import 'dart:async';
import 'dart:convert';

import 'package:animate_do/animate_do.dart';
import 'package:centurion/common.dart';
import 'package:centurion/config.dart';
import 'package:centurion/services/communication/http/index.dart';

import 'package:centurion/utils/utils.dart';
import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

class RemusPayment extends StatefulWidget {
  final String shoppingtoken;

  const RemusPayment({Key key, this.shoppingtoken = "tfghjk"})
      : super(key: key);
  @override
  _RemusPaymentState createState() => _RemusPaymentState();
}

class _RemusPaymentState extends State<RemusPayment> {
  // Map<String, dynamic> pageDetails = {};
  Future<String> _temp;
  double width;
  String error;
  bool isMobile = false;
  int id;
  String paymentid = '';
  double amt;
  String hash = '';
  String availshoppingamt = '';
  int mtrans;
  int atrans;

  Future<String> tokensepartor() async {
    var decodeurl = utf8.decode(base64.decode(widget.shoppingtoken));
    print('DECODEURL: $decodeurl}');
    var splitval = decodeurl.toString().split('&');
    print('SPLITVAL: $splitval}');
    for (var i = 0; i < splitval.length; i++) {
      if (splitval[i].split('=')[0] == "id") {
        id = int.parse(splitval[i].split('=')[1]);
        print('ID: $id}');
      } else if (splitval[i].split('=')[0] == "paymentid") {
        paymentid = splitval[i].split('=')[1];
        print('PAYMENTID: paymentid}');
      } else if (splitval[i].split('=')[0] == "amount") {
        amt = double.parse(splitval[i].split('=')[1]);
        print('AMT: $amt}');
      } else if (splitval[i].split('=')[0] == "hash") {
        hash = splitval[i].split('=')[1];
        print('HASH: $hash}');
      }
    }

    var map = {
      "id": id,
      "payment_id": paymentid,
      "hash": hash,
      "amount": amt,
    };
    return validatetoken(map);
  }

  Future<String> validatetoken(_map) async {
    // Map<String, dynamic> _map = {
    //   "link_expired": widget.shoppingtoken,
    // };
    var _result =
        await HttpRequest.Post('remuspayment', Utils.constructPayload(_map));
    print('_MAP: $_map');
    print(_result);
    if (Utils.isServerError(_result)) {
      print("ddddddddddddddd");
      var err = await Utils.getMessage(_result['response']['error']);
      if (err == 'Invalid Merchant Request') {
        snack.snack(title: 'Invalid Merchant Request');
      } else {}
      throw (await Utils.getMessage(_result['response']['error']));
    } else {
      print("sssssssssssssssss");
      availshoppingamt =
          _result['response']['data']['shopping_account_balance'].toString();
      print('AVAILSHOPPINGAMT: $availshoppingamt}');
      mtrans = int.parse(_result['response']['data']['m_trans_id']);
      atrans = int.parse(_result['response']['data']['a_trans_id']);
      return 'start';
    }
  }

  proceedpayment(_map) async {
    var _result = await HttpRequest.Post(
        'remuspaymentconfirm', Utils.constructPayload(_map));
    print('_MAP: $_map');
    print(_result);
    if (Utils.isServerError(_result)) {
      Navigator.pop(context);

      var err = await Utils.getMessage(_result['response']['error']);

      if (err == 'Insufficient Shopping Account Balance') {
        snack.snack(title: 'Insufficient Shopping Account Balance');
      } else {
        snack.snack(title: 'Payment Failed');
      }

      throw (await Utils.getMessage(_result['response']['error']));
    } else {
      var url = _result['response']['data']['url'];
      Navigator.pop(context);
      if (_map['status'] == 'success') {
        snack.snack(title: 'Payment Success Please Wait...');
      } else if (_map['status'] == 'failed') {
        snack.snack(title: 'Payment Cancelled Please Wait...');
      } else {}

      urlredirector(url);

      Navigator.pushReplacementNamed(context, '/');
    }
  }

  urlredirector(String url) async {
    if (await canLaunch(url)) {
      await launch(
        url,
        webOnlyWindowName: '_self',
      );
    } else {
      throw 'Could not launch $url';
    }
  }
  // void setPageDetails() async {
  //   String data = await Utils.getPageDetails('settings');
  //   setState(() {
  //     pageDetails = Utils.fromJSONString(data);
  //   });
  // }

  @override
  void initState() {
    // setPageDetails();
    super.initState();
    _temp = tokensepartor();
  }

  @override
  Widget build(BuildContext context) {
    width = MediaQuery.of(context).size.width;

    return Scaffold(
      body: FutureBuilder(
        future: _temp,
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            return Scaffold(
              backgroundColor: Pallet.popupcontainerback,
              body: LayoutBuilder(
                builder: (context, constraints) {
                  if (constraints.maxWidth < 800) {
                    isMobile = true;
                    if (constraints.maxHeight < 595) {
                      return SingleChildScrollView(
                        child: passwordchange(),
                      );
                    } else {
                      return passwordchange();
                    }
                  } else {
                    isMobile = false;
                    return passwordchange();
                  }
                },
              ),
            );
          } else if (snapshot.hasError) {
            return Scaffold(
              backgroundColor: Colors.white,
              body:
                  // error == 'Invalid Merchant Request'
                  //     ? Container(
                  //         child: Center(
                  //           child: Column(
                  //             mainAxisSize: MainAxisSize.min,
                  //             children: [
                  //               Container(
                  //                 padding: EdgeInsets.all(7),
                  //                 decoration: BoxDecoration(
                  //                   shape: BoxShape.circle,
                  //                   color: Colors.red,
                  //                 ),
                  //                 child: Icon(
                  //                   Icons.close,
                  //                   size: 40,
                  //                   color: Colors.white,
                  //                 ),
                  //               ),
                  //               SizedBox(
                  //                 height: 10,
                  //               ),
                  //               Text(
                  //                 '',
                  //                 style: TextStyle(
                  //                   fontSize: Pallet.heading2,
                  //                   fontWeight: Pallet.font600,
                  //                   color: Pallet.fontcolornew,
                  //                 ),
                  //               ),
                  //               SizedBox(
                  //                 height: 20,
                  //               ),
                  //               ElevatedButton(
                  //                 onPressed: () async {
                  //                   urlredirector("https://");
                  //                   // Navigator.pushReplacementNamed(context, '/');
                  //                 },
                  //                 child: Text(
                  //                   'Go Back',
                  //                 ),
                  //               ),
                  //             ],
                  //           ),
                  //         ),
                  //       )
                  //     :
                  SomethingWentWrongMessage(),
            );
          } else {
            return Loader();
          }
        },
      ),
    );
  }

  passwordchange() {
    return Container(
      child: isMobile == false
          ? Stack(
              children: [
                isMobile == false
                    ? SlideInRight(
                        child: Container(
                          alignment: Alignment.centerLeft,
                          width: MediaQuery.of(context).size.height * 0.5,
                          child: Image.asset("centurion-logo.png"),
                        ),
                      )
                    : Container(),
                FadeIn(child: centercard()),
              ],
            )
          : Stack(
              children: [
                SlideInDown(
                  child: Container(
                    alignment: Alignment.topCenter,
                    child: Image.asset(
                      "centurion-logo.png",
                      height: MediaQuery.of(context).size.height * 0.3,
                    ),
                  ),
                ),
                // : Container(),
                FadeIn(child: centercard()),
              ],
            ),
    );
  }

  Widget centercard() {
    return Center(
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Container(
            padding: EdgeInsets.all(20),
            margin: EdgeInsets.all(20),
            width: isMobile == true ? MediaQuery.of(context).size.width : 500,
            // height:
            //     isMobile == true ? MediaQuery.of(context).size.width : 500,
            decoration: BoxDecoration(
              color: Colors.white,
              border: Border.all(
                color: Colors.black,
                width: 2,
              ),
              borderRadius: BorderRadius.circular(12),
            ),
            child: Column(
              children: [
                Text(
                  "Shopping Payment",
                  style: TextStyle(
                    color: Pallet.fontcolornew,
                    fontSize: isMobile == true ? Pallet.heading3 : 27,
                    fontWeight: Pallet.font600,
                  ),
                ),
                SizedBox(height: 20),
                Row(
                  children: [
                    Expanded(
                      child: Center(
                        child: Text(
                          "Shopping Balance",
                          style: TextStyle(
                            color: Pallet.fontcolornew,
                            fontSize: isMobile == true
                                ? Pallet.heading4
                                : Pallet.heading3,
                            fontWeight: Pallet.font500,
                          ),
                        ),
                      ),
                    ),
                    Text(
                      ":",
                      style: TextStyle(
                        color: Pallet.fontcolornew,
                        fontSize: isMobile == true
                            ? Pallet.heading4
                            : Pallet.heading3,
                        fontWeight: Pallet.font500,
                      ),
                    ),
                    Expanded(
                      child: Center(
                        child: Text(
                          availshoppingamt.toString(),
                          style: TextStyle(
                            color: Pallet.fontcolornew,
                            fontSize: isMobile == true
                                ? Pallet.heading4
                                : Pallet.heading3,
                            fontWeight: Pallet.font500,
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
                SizedBox(height: 20),
                Row(
                  children: [
                    Expanded(
                      child: Center(
                        child: Text(
                          "Deduct Shopping Balance",
                          style: TextStyle(
                            color: Pallet.fontcolornew,
                            fontSize: isMobile == true
                                ? Pallet.heading4
                                : Pallet.heading3,
                            fontWeight: Pallet.font500,
                          ),
                        ),
                      ),
                    ),
                    Text(
                      ":",
                      style: TextStyle(
                        color: Pallet.fontcolornew,
                        fontSize: isMobile == true
                            ? Pallet.heading4
                            : Pallet.heading3,
                        fontWeight: Pallet.font500,
                      ),
                    ),
                    Expanded(
                      child: Center(
                        child: Text(
                          amt.toString(),
                          style: TextStyle(
                            color: Pallet.fontcolornew,
                            fontSize: isMobile == true
                                ? Pallet.heading4
                                : Pallet.heading3,
                            fontWeight: Pallet.font500,
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
                SizedBox(height: 20),
                Row(
                  children: [
                    Expanded(
                      child: Center(
                        child: Row(
                          mainAxisSize: MainAxisSize.min,
                          children: [
                            CustomButton(
                              text: 'Cancel',
                              onpress: () async {
                                waiting.waitpopup(context);

                                var map = {
                                  "id": id,
                                  "payment_id": paymentid,
                                  "hash": hash,
                                  "amount": amt,
                                  "status": "failed",
                                  "m_trans_id": mtrans,
                                  "a_trans_id": atrans,
                                };
                                await proceedpayment(map);
                              },
                              vpadding: 10,
                              buttoncolor: Colors.red,
                              textcolor: Pallet.fontcolor,
                            ),
                            SizedBox(width: isMobile == true ? 15 : 25),
                            CustomButton(
                              text: 'Confirm',
                              vpadding: 10,
                              onpress: () async {
                                waiting.waitpopup(context);

                                var map = {
                                  "id": id,
                                  "payment_id": paymentid,
                                  "hash": hash,
                                  "amount": amt,
                                  "status": "success",
                                  "m_trans_id": mtrans,
                                  "a_trans_id": atrans,
                                };
                                await proceedpayment(map);
                              },
                              buttoncolor: Colors.green,
                              textcolor: Pallet.fontcolor,
                            ),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  changepass() async {
    waiting.waitpopup(context);
    Map<String, dynamic> _map = {
      "account_id": '',
      "link_code": '',
      'password': '',
    };
    var _result = await HttpRequest.Post(
        'link_resetpassword', Utils.constructPayload(_map));
    // print("zzzzzzzzzzzzzzzzzzzzzzzzzzzz");
    print(_result);
    if (Utils.isServerError(_result)) {
      snack.snack(title: 'Password Reset Failed');
    } else {
      snack.snack(title: 'Successfully Password Reseted');
      Navigator.pushReplacementNamed(context, '/');
    }
  }
}
