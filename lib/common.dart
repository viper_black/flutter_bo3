import 'package:centurion/services/business/cart.dart';
import 'package:centurion/animations/Bouncing_button.dart';
import 'package:centurion/utils/utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:get/get.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'config.dart';
// import 'package:dropdownfield/dropdownfield.dart';
import 'package:flutter/services.dart';
import 'package:webview_flutter/webview_flutter.dart';
import 'dart:math' as math;
import 'config/app_settings.dart';
import 'home.dart';
import 'animations/dropdown.dart';
import 'package:intl/intl.dart' as intl;

// String capitalize(String string) {
//   if (string.isEmpty) {
//     return string;
//   }

//   return string[0].toUpperCase() + string.substring(1);
// }

class MyText extends StatelessWidget {
  @required
  final text;
  final TextStyle style;
  final int maxLines;
  final TextOverflow overflow;
  final bool isselectable;
  final TextAlign textAlign;
  final softWrap;
  const MyText({
    this.text,
    this.style,
    this.maxLines,
    this.overflow,
    this.isselectable,
    this.textAlign,
    this.softWrap,
  });

  @override
  Text build(BuildContext context) {
    return isselectable == true
        ? SelectableText(
            text == null ? '' : text.toString(),
            style: style,
            maxLines: maxLines,
            textAlign: textAlign,
            // softWrap:softWrap,
          )
        : Text(
            text == null ? '' : text.toString(),
            style: style,
            maxLines: maxLines,
            overflow: overflow,
            textAlign: textAlign,
            softWrap: softWrap,
          );
  }
}

// -----------------------------don't delete or change this part -----------------------------

extension addamtextens on String {
  String addamt({int roundofvalue, bool issubract}) {
    var val = roundofvalue - (int.parse((this)) % roundofvalue);
    // print('VAL: ${val}');

    if (int.parse((this)) % roundofvalue == roundofvalue - val) {
      // print(
      //     'INT.PARSE((THIS)) % ROUNDOFVALUE: ${int.parse((this)) % roundofvalue}');
      var cal = roundofvalue - val;
      var finalval = roundofvalue - cal;

      var returnval = issubract == true
          ? cal == 0
              ? 5.toString()
              : cal.toString()
          : finalval.toString();
      return returnval;
    } else {
      return roundofvalue.toString();
    }
  }
}

extension date on DateTime {
  String utcweekend({int day}) {
    var date = this.toUtc();
    var calsunday = day - date.weekday;
    var sunday = date.add(Duration(days: calsunday)).toString().split(' ');
    var value = sunday[0].split('-');
    String finalsundayval =
        value[2] + '-' + value[1] + '-' + value[0].toString();

    return finalsundayval;
  }
}

extension CapExtension on String {
  String get inCaps => '${this[0].toUpperCase()}${this.substring(1)}';
  String get allInCaps => this.toUpperCase();
  String get capitalizeFirstofEach =>
      this.split(" ").map((str) => str.capitalize).join(" ");
}

// -------------------------------------------------------------------------

// String extensions

extension StringExtension on String {
  String firstLetterUpperCase() => length > 1
      ? "${this[0].toUpperCase()}${substring(1).toLowerCase()}"
      : this;
  String get eliminateFirst => length > 1 ? "${substring(1, length)}" : "";
  String get eliminateLast => length > 1 ? "${substring(0, length - 1)}" : "";
  bool get isEmptyOrNull => isEmpty;

  String suffixconcat(String word) => '$this' + ' ' + '$word';
  String prefixconcat(String word) => '$word' + ' ' + '$this';
  String get adddollarsymbol => "\$ $this";
  String get addducsymbol => "$this DUC";

  String nullcheck() {
    if (this == null) {
      return '';
    } else if (this == 'null') {
      return '';
    }
    return this;
  }

  String customnullcheck({String ifnullreturnval}) {
    if (this == null) {
      return '$ifnullreturnval';
    } else if (this == 'null') {
      return '$ifnullreturnval';
    }
    return this;
  }

  int toint() => int.parse(this);
  double todouble() => double.parse(this);

  bool validateEmail() => RegExp(
          r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+")
      .hasMatch(this);
  String get orEmpty => this;
  String ifEmpty(Function action) => isEmpty ? action() : this;
  String removeAllWhiteSpace() => replaceAll(RegExp(r"\s+\b|\b\s"), "");
  bool get isNotBlank => trim().isNotEmpty;
  String get numCurrency =>
      intl.NumberFormat.currency(customPattern: "#,##0.00")
          .format(double.tryParse(this))
          .toString();
  String numCurrencyWithLocale({String locale = "en_US"}) =>
      intl.NumberFormat.currency(
        locale: locale,
      ).format(double.tryParse(this)).toString();
  String allWordsCapitilize() {
    return toLowerCase().split(' ').map((word) {
      final String leftText =
          (word.length > 1) ? word.substring(1, word.length) : '';
      return word[0].toUpperCase() + leftText;
    }).join(' ');
  }

  String reverse() {
    final stringBuffer = StringBuffer();
    for (var i = length - 1; i >= 0; i--) {
      stringBuffer.write(this[i]);
    }
    return stringBuffer.toString();
  }

  bool isNumber() {
    final isMatch = RegExp("[1-9]").hasMatch(this);
    return isMatch;
  }

  bool isLetter() {
    final isMatch = RegExp("[A-Za-z]").hasMatch(this);
    return isMatch;
  }

  bool isSymbol() {
    const String pattern =
        "[`~!@#\$%^&*()_\-+=<>?:\"{}|,.//\/;'\\[\]·~！@#￥%……&*（）——\-+={}|《》？：“”【】、；‘’，。、]";
    for (int i = 0; i < length; i++) {
      if (pattern.contains(this[i])) {
        return true;
      }
    }
    return false;
  }
}

// ignore: must_be_immutable
class Dlist extends StatelessWidget {
  @required
  final String headtitle;
  @required
  final String title1;
  @required
  final String title2;
  @required
  final String footer1;

  final String img1;

  final String img2;
  @required
  final String footer2;
  @required
  final List listofvalues;
  @required
  final String listvalkey1;
  @required
  final String listvalkey2;

  final bool firsticonenable;
  final bool secondiconenable;

  Dlist({
    this.title1,
    this.title2,
    this.listofvalues,
    this.listvalkey1,
    this.footer1,
    this.footer2,
    this.headtitle,
    this.listvalkey2,
    this.img1,
    this.img2,
    this.firsticonenable,
    this.secondiconenable,
  });
  final ScrollController _scrollController = ScrollController();
  double width;
  @override
  Widget build(BuildContext context) {
    width = MediaQuery.of(context).size.width;
    return Container(
      child: Column(
        children: [
          Padding(
            padding: const EdgeInsets.only(left: 20.0, bottom: 20.0, top: 20),
            child: Row(
              children: [
                Text(
                  headtitle,
                  overflow: TextOverflow.ellipsis,
                  style: TextStyle(
                    fontSize: width > 600 ? Pallet.heading3 : Pallet.heading5,
                    fontWeight: Pallet.font500,
                    color: Pallet.fontcolor,
                  ),
                ),
              ],
            ),
          ),
          Container(
            padding: EdgeInsets.symmetric(vertical: 8),
            decoration: BoxDecoration(
                color: Pallet.lastpackheadingback,
                borderRadius: BorderRadius.circular(5)),
            child: Row(
              children: [
                Expanded(
                  child: Center(
                    child: Text(
                      title1,
                      style: TextStyle(
                        fontSize:
                            width > 600 ? Pallet.heading5 : Pallet.heading7,
                        fontWeight: Pallet.font500,
                        color: Pallet.fontcolor,
                      ),
                    ),
                  ),
                ),
                Expanded(
                  child: Center(
                    child: Text(
                      title2,
                      overflow: TextOverflow.ellipsis,
                      style: TextStyle(
                        fontSize:
                            width > 600 ? Pallet.heading5 : Pallet.heading7,
                        fontWeight: Pallet.font500,
                        color: Pallet.fontcolor,
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
          SizedBox(
            height: 5,
          ),
          Expanded(
            child: listofvalues.length == 0
                ? Center(
                    child: Text("No Data Found",
                        style: TextStyle(
                          fontSize:
                              width > 600 ? Pallet.heading5 : Pallet.heading7,
                          fontWeight: Pallet.font500,
                          color: Pallet.fontcolor,
                        )),
                  )
                : RawScrollbar(
                    thumbColor: Colors.white,
                    thickness: 4,
                    radius: Radius.circular(3),
                    controller: _scrollController,
                    fadeDuration: Duration(seconds: 1),
                    isAlwaysShown:
                        MediaQuery.of(context).size.height > 800 ? false : true,
                    child: SingleChildScrollView(
                      controller: _scrollController,
                      child: Column(
                        children: [
                          for (var item in listofvalues)
                            Container(
                              decoration: BoxDecoration(
                                  color: Pallet.lastpackback,
                                  borderRadius: BorderRadius.circular(5)),
                              padding: EdgeInsets.symmetric(vertical: 5),
                              margin: MediaQuery.of(context).size.height > 800
                                  ? EdgeInsets.symmetric(vertical: 5)
                                  : EdgeInsets.fromLTRB(0, 5, 5, 5),
                              child: Row(
                                // mainAxisSize: MainAxisSize.min,
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Expanded(
                                    child: Padding(
                                      padding: EdgeInsets.only(
                                          left: width > 600 ? 20 : 7),
                                      child: Container(
                                        // color: Colors.amber,
                                        child: firsticonenable == true
                                            ? Row(
                                                children: [
                                                  CircleAvatar(
                                                    backgroundColor:
                                                        Colors.transparent,
                                                    backgroundImage:
                                                        NetworkImage(appSettings[
                                                                'SERVER_URL'] +
                                                            '/' +
                                                            item[img2]),
                                                    radius: 13,
                                                  ),
                                                  SizedBox(
                                                    width: 10,
                                                  ),
                                                  Expanded(
                                                    child: Row(
                                                      children: [
                                                        Expanded(
                                                          child: Text(
                                                            item[listvalkey1]
                                                                .toString()
                                                                .nullcheck(),
                                                            overflow:
                                                                TextOverflow
                                                                    .ellipsis,
                                                            style: TextStyle(
                                                              fontSize: width > 600
                                                                  ? Pallet
                                                                      .heading5
                                                                  : Pallet
                                                                      .heading7,
                                                              fontWeight: Pallet
                                                                  .font500,
                                                              color: Pallet
                                                                  .fontcolor,
                                                            ),
                                                          ),
                                                        ),
                                                      ],
                                                    ),
                                                  ),
                                                ],
                                              )
                                            : Row(
                                                children: [
                                                  Expanded(
                                                    child: Container(),
                                                  ),
                                                  Expanded(
                                                    flex: 2,
                                                    child: Text(
                                                      item[listvalkey1]
                                                          .toString()
                                                          .nullcheck(),
                                                      overflow:
                                                          TextOverflow.ellipsis,
                                                      style: TextStyle(
                                                        fontSize: width > 600
                                                            ? Pallet.heading5
                                                            : Pallet.heading7,
                                                        fontWeight:
                                                            Pallet.font500,
                                                        color: Pallet.fontcolor,
                                                      ),
                                                    ),
                                                  ),
                                                ],
                                              ),
                                      ),
                                    ),
                                  ),
                                  // Expanded(child:Container()),
                                  Expanded(
                                    child: Padding(
                                      padding: const EdgeInsets.symmetric(
                                          horizontal: 7),
                                      child: secondiconenable == true
                                          ? Container(
                                              // color: Colors.amber,
                                              child: Center(
                                                child: Row(
                                                  children: [
                                                    Expanded(
                                                        child: Container()),
                                                    Expanded(
                                                      flex: 2,
                                                      child: Row(
                                                        // mainAxisAlignment: MainAxisAlignment.center,
                                                        mainAxisSize:
                                                            MainAxisSize.min,
                                                        children: [
                                                          Container(
                                                            width: 25,
                                                            height: 25,
                                                            child: Image.network(
                                                                appSettings[
                                                                        'SERVER_URL'] +
                                                                    '/' +
                                                                    item[img2]),
                                                          ),
                                                          SizedBox(
                                                            width: 10,
                                                          ),
                                                          Expanded(
                                                            child: Text(
                                                              item[listvalkey2]
                                                                  .toString()
                                                                  .nullcheck(),
                                                              overflow:
                                                                  TextOverflow
                                                                      .ellipsis,
                                                              style: TextStyle(
                                                                fontSize: width > 600
                                                                    ? Pallet
                                                                        .heading5
                                                                    : Pallet
                                                                        .heading7,
                                                                fontWeight:
                                                                    Pallet
                                                                        .font500,
                                                                color: Pallet
                                                                    .fontcolor,
                                                              ),
                                                            ),
                                                          )
                                                        ],
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                              ),
                                            )
                                          : Row(
                                              children: [
                                                Expanded(
                                                  child: Container(),
                                                ),
                                                Expanded(
                                                  flex: 2,
                                                  child: Text(
                                                    item[listvalkey2]
                                                        .toString()
                                                        .nullcheck(),
                                                    overflow:
                                                        TextOverflow.ellipsis,
                                                    style: TextStyle(
                                                      fontSize: width > 600
                                                          ? Pallet.heading5
                                                          : Pallet.heading7,
                                                      fontWeight:
                                                          Pallet.font500,
                                                      color: Pallet.fontcolor,
                                                    ),
                                                  ),
                                                ),
                                              ],
                                            ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                        ],
                      ),
                    ),
                  ),
          ),
          SizedBox(
            height: 5,
          ),
          Container(
            padding: EdgeInsets.symmetric(vertical: 6),
            decoration: BoxDecoration(
                color: Pallet.lastpackfooterback,
                borderRadius: BorderRadius.circular(5)),
            child: Row(
              children: [
                Expanded(
                  child: Center(
                    child: Text(
                      footer1,
                      overflow: TextOverflow.ellipsis,
                      style: TextStyle(
                        fontSize:
                            width > 600 ? Pallet.heading3 : Pallet.heading5,
                        fontWeight: Pallet.font500,
                        color: Pallet.fontcolornew,
                      ),
                    ),
                  ),
                ),
                Expanded(
                  child: Center(
                    child: Text(
                      footer2.nullcheck(),
                      overflow: TextOverflow.ellipsis,
                      style: TextStyle(
                        fontSize:
                            width > 600 ? Pallet.heading3 : Pallet.heading5,
                        fontWeight: Pallet.font500,
                        color: Pallet.fontcolornew,
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}

class Textbox extends StatelessWidget {
  const Textbox(
      {Key key,
      this.autofocus,
      this.focusnode,
      this.tabpress,
      this.onsubmit,
      this.icon,
      this.label,
      this.validation,
      this.controller,
      this.header,
      this.enabled,
      this.errorText})
      : super(key: key);
  final IconData icon;
  final bool autofocus, enabled;
  final String label, errorText, header;
  final Function validation, tabpress, onsubmit;
  final TextEditingController controller;
  final FocusNode focusnode;

  @override
  Column build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(header == null ? '' : header,
            style: TextStyle(
                color: Pallet.fontcolornew,
                fontSize: Pallet.heading4,
                fontWeight: FontWeight.w300)),
        SizedBox(height: 5),
        RawKeyboardListener(
          focusNode: focusnode,
          onKey: tabpress,
          child: TextFormField(
              textAlign: TextAlign.left,
              onEditingComplete: onsubmit,
              autofocus: autofocus == true ? true : false,
              style: TextStyle(color: Pallet.dashcontainerback),
              controller: controller,
              enabled: enabled == false ? false : true,
              onChanged: validation,
              cursorColor: Pallet.dashcontainerback,
              keyboardType: TextInputType.numberWithOptions(decimal: true),
              // inputFormatters: <TextInputFormatter>[
              //   FilteringTextInputFormatter.digitsOnly,
              // ],
              inputFormatters: [NumberRemoveExtraDotFormatter()],
              decoration: InputDecoration(
                errorText: errorText,
                border: OutlineInputBorder(
                  borderSide:
                      BorderSide(color: Pallet.dashcontainerback, width: 2),
                ),
                // labelText: label,
                focusedBorder: OutlineInputBorder(
                    borderSide:
                        BorderSide(color: Pallet.dashcontainerback, width: 2),
                    borderRadius: BorderRadius.circular(Pallet.radius)),
                // fillColor: Pallet.dashcontainerback,
                enabledBorder: OutlineInputBorder(
                    borderSide:
                        BorderSide(color: Pallet.dashcontainerback, width: 2),
                    borderRadius: BorderRadius.circular(Pallet.radius)),
                errorBorder: OutlineInputBorder(
                    borderSide: BorderSide(color: Colors.red, width: 2),
                    borderRadius: BorderRadius.circular(Pallet.radius)),
                focusedErrorBorder: OutlineInputBorder(
                    borderSide: BorderSide(color: Colors.red, width: 2),
                    borderRadius: BorderRadius.circular(Pallet.radius)),
                // labelStyle: TextStyle(
                //   color: Pallet.fontcolor,
                //   fontSize: Pallet.heading2,
                // ),
                filled: true,
              ),
              validator: validation),
        ),
      ],
    );
  }
}

// mydialge(context) {
//   return showDialog(
//       context: context,
//       barrierDismissible: false,
//       builder: (BuildContext context) {
//         return AlertDialog(
//           scrollable: true,
//           elevation: 24.0,
//           backgroundColor: Pallet.fontcolornew,
//           shape: RoundedRectangleBorder(
//               borderRadius: BorderRadius.all(Radius.circular(15.0))),
//           title: Container(
//             width: 450,
//             child: Column(
//               mainAxisSize: MainAxisSize.min,
//               crossAxisAlignment: CrossAxisAlignment.start,
//               children: [
//                 Text('New Lottry',
//                     style: TextStyle(
//                         fontWeight: Pallet.font500,
//                         fontSize: Pallet.heading3,
//                         color: Pallet.fontcolor)),
//                 SizedBox(height: 10),
//                 Text('Ticklet No : 123456',
//                     style: TextStyle(
//                         fontSize: Pallet.heading5, color: Pallet.fontcolor)),
//                 SizedBox(height: 10),
//                 Text('Special Number',
//                     style: TextStyle(
//                         fontWeight: Pallet.font500,
//                         fontSize: Pallet.heading4,
//                         color: Pallet.fontcolor)),
//                 SizedBox(height: 15),
//                 Container(
//                   width: double.infinity,
//                   // height: 60,
//                   decoration: BoxDecoration(
//                       // boxShadow: [Pallet.shadowEffect],
//                       color: Colors.transparent,
//                       // border: Border.all(width: 1, color: Pallet.fontcolor),
//                       borderRadius: BorderRadius.circular(6)),
//                   child: Wrap(
//                     spacing: 5,
//                     runSpacing: 5,
//                     children: [
//                       Container(
//                         width: 50,
//                         height: 50,
//                         decoration: BoxDecoration(
//                             // boxShadow: [Pallet.shadowEffect],
//                             color: Pallet.inner1,
//                             borderRadius: BorderRadius.circular(50)),
//                         child: Center(
//                           child: Text('1',
//                               style: TextStyle(
//                                   fontWeight: Pallet.font500,
//                                   fontSize: Pallet.heading4,
//                                   color: Pallet.fontcolor)),
//                         ),
//                       ),
//                       Container(
//                         width: 50,
//                         height: 50,
//                         decoration: BoxDecoration(
//                             // boxShadow: [Pallet.shadowEffect],
//                             color: Pallet.inner1,
//                             borderRadius: BorderRadius.circular(50)),
//                         child: Center(
//                           child: Text('2',
//                               style: TextStyle(
//                                   fontWeight: Pallet.font500,
//                                   fontSize: Pallet.heading4,
//                                   color: Pallet.fontcolor)),
//                         ),
//                       ),
//                     ],
//                   ),
//                 ),
//                 SizedBox(height: 10),
//                 Text('Normal Number',
//                     style: TextStyle(
//                         fontWeight: Pallet.font500,
//                         fontSize: Pallet.heading4,
//                         color: Pallet.fontcolor)),
//                 SizedBox(height: 15),
//                 Container(
//                   width: double.infinity,
//                   // height: 80,
//                   decoration: BoxDecoration(
//                       // boxShadow: [Pallet.shadowEffect],
//                       color: Colors.transparent,
//                       // border: Border.all(width: 1, color: Pallet.fontcolor),
//                       borderRadius: BorderRadius.circular(6)),
//                   child: Wrap(
//                     spacing: 5,
//                     runSpacing: 5,
//                     children: [
//                       Container(
//                         width: 50,
//                         height: 50,
//                         decoration: BoxDecoration(
//                             // boxShadow: [Pallet.shadowEffect],
//                             color: Pallet.fontcolor,
//                             borderRadius: BorderRadius.circular(50)),
//                         child: Center(
//                           child: Text('1',
//                               style: TextStyle(
//                                   fontWeight: Pallet.font500,
//                                   fontSize: Pallet.heading4,
//                                   color: Pallet.fontcolornew)),
//                         ),
//                       ),
//                       Container(
//                         width: 50,
//                         height: 50,
//                         decoration: BoxDecoration(
//                             // boxShadow: [Pallet.shadowEffect],
//                             color: Pallet.fontcolor,
//                             borderRadius: BorderRadius.circular(50)),
//                         child: Center(
//                           child: Text('8',
//                               style: TextStyle(
//                                   fontWeight: Pallet.font500,
//                                   fontSize: Pallet.heading4,
//                                   color: Pallet.fontcolornew)),
//                         ),
//                       ),
//                     ],
//                   ),
//                 ),
//               ],
//             ),
//           ),
//           actions: [
//             PopupButton(
//               text: 'Close',
//               onpress: () {
//                 Navigator.of(context).pop();
//               },
//             ),
//             SizedBox(height: 10),
//           ],
//         );
//       });
// }

class NumberRemoveExtraDotFormatter extends TextInputFormatter {
  NumberRemoveExtraDotFormatter({this.decimalRange = 2})
      : assert(decimalRange == null || decimalRange > 0);

  final int decimalRange;

  @override
  TextEditingValue formatEditUpdate(
      TextEditingValue oldValue, TextEditingValue newValue) {
    String nValue = newValue.text;
    TextSelection nSelection = newValue.selection;

    Pattern p = RegExp(r'(\d+\.?)|(\.?\d+)|(\.?)');
    nValue = p
        .allMatches(nValue)
        .map<String>((Match match) => match.group(0))
        .join();

    if (nValue.startsWith('.')) {
      nValue = '0.';
    } else if (nValue.contains('.')) {
      if (nValue.substring(nValue.indexOf('.') + 1).length > decimalRange) {
        nValue = oldValue.text;
      } else {
        if (nValue.split('.').length > 2) {
          List<String> split = nValue.split('.');
          nValue = split[0] + '.' + split[1];
        }
      }
    }

    nSelection = newValue.selection.copyWith(
      baseOffset: math.min(nValue.length, nValue.length + 1),
      extentOffset: math.min(nValue.length, nValue.length + 1),
    );

    return TextEditingValue(
        text: nValue, selection: nSelection, composing: TextRange.empty);
  }
}

class Option extends StatelessWidget {
  const Option({
    Key key,
    this.title,
    this.icon,
    this.amt,
    this.subtitle,
    this.selected,
    this.onselect,
    this.enable,
    this.wdgtWidth,
  }) : super(key: key);
  final bool selected;
  final double wdgtWidth;
  final bool enable;
  final String amt;
  final Function onselect;
  final String title, icon, subtitle;
  @override
  InkWell build(BuildContext context) {
    return InkWell(
      onTap: onselect,
      child: Container(
        decoration: BoxDecoration(
            boxShadow: [Pallet.shadowEffect],
            // border: selected
            //     ? Border.all(
            //         color: Pallet.activebordercolor,
            //       )
            //     : Border.all(color: Colors.transparent),
            borderRadius: BorderRadius.circular(Pallet.radius),
            color: Pallet.fontcolor),
        // padding: EdgeInsets.symmetric(
        //   horizontal: 10.0,
        //    vertical: 20 + wdgtWidth * 0.01
        // ),
        padding: EdgeInsets.only(left: 10, bottom: 20 + wdgtWidth * 0.01),
        width: 250 + wdgtWidth * 0.05,
// color: Colors.blue[900],
        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.only(top: 3),
              child: Container(
                width: 250 + wdgtWidth * 0.05,
                height: 20,
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    if (selected)
                      CircleAvatar(
                          backgroundColor: Pallet.dashcontainerback,
                          child: Center(
                            child: Icon(
                              Icons.done,
                              color: Pallet.fontcolor,
                              size: 14,
                            ),
                          )),

                    // SizedBox(width: 10)
                  ],
                ),
              ),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Container(
                    child: ImageIcon(
                  NetworkImage(icon),
                  color: Pallet.dashcontainerback,
                  size: 40,
                )),
                SizedBox(width: 15.0),
                Container(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        title,
                        style: TextStyle(
                            fontSize: 18.0, color: Pallet.dashcontainerback),
                      ),
                      SizedBox(height: 5),
                      enable == false || enable == null
                          ? Text(
                              subtitle,
                              style: TextStyle(
                                  fontSize: 16.0,
                                  color: Pallet.dashcontainerback,
                                  fontWeight: FontWeight.w600),
                            )
                          : Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  subtitle,
                                  style: TextStyle(
                                      fontSize: 16.0,
                                      color: Pallet.dashcontainerback,
                                      fontWeight: FontWeight.w600),
                                ),
                                Text(
                                  amt,
                                  style: TextStyle(
                                      fontSize: 16.0,
                                      color: Pallet.dashcontainerback,
                                      fontWeight: FontWeight.w600),
                                ),
                              ],
                            ),
                    ],
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}

// ignore: must_be_immutable
class DropDown extends StatelessWidget {
  DropDown(
      {Key key,
      this.label,
      this.onChange,
      this.items,
      this.labeltext,
      this.value,
      this.controller,
      this.errortext})
      : super(key: key);
  final String label, labeltext;
  final Function onChange;
  final List items;
  final String errortext;
  TextEditingController controller = TextEditingController();

  final String value;
  @override
  Column build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        // SizedBox(
        //   height: 10,
        // ),
        Text(label,
            style: TextStyle(
                color: Pallet.fontcolornew,
                fontSize: Pallet.heading4,
                fontWeight: FontWeight.w300)),
        SizedBox(height: 5),
        Container(
          width: double.infinity,
          decoration: BoxDecoration(
              border: Border.all(
                  color:
                      errortext == '' ? Pallet.dashcontainerback : Colors.red,
                  width: 2.0),
              color: Pallet.fontcolor,
              borderRadius: BorderRadius.circular(Pallet.radius)),
          child: DropDownField(
            divcolor: Pallet.dashcontainerback,
            // isize: 35,
            controller: controller,
            // labelText: labeltext,
            // labelStyle: TextStyle(color: Colors.white, fontSize: 12),
            value: value,
            enabled: true,
            strict: true,
            items: items,

            onValueChanged: onChange,
            itemsVisibleInDropdown: items.length,
            textStyle: TextStyle(color: Pallet.dashcontainerback),
          ),
        ),
        Padding(
          padding: EdgeInsets.symmetric(vertical: Pallet.defaultPadding / 3),
          child: Container(
              margin: EdgeInsets.only(left: 10),
              child: Text(
                errortext,
                style: TextStyle(color: Colors.red, fontSize: 12),
              )),
        ),
      ],
    );
  }
}

class SomethingWentWrongMessage extends StatelessWidget {
  @override
  Center build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Center(
      child: Container(
        width: size.width,
        height: size.height * 0.9,
        child: Center(
          child: Image.asset(
            'oops1.gif',
            // width: size.width,
            // height: size.width * 0.50,
          ),
        ),
        // child: Text('oh oh'),
      ),
    );
  }
}

class Error extends StatelessWidget {
  const Error({Key key, this.message}) : super();
  final String message;
  @override
  Widget build(BuildContext context) {
    // Size size = MediaQuery.of(context).size;
    return SnackBar(
      content: Text(message),
    );
  }
}

class MultilineTextBox extends StatelessWidget {
  const MultilineTextBox(
      {Key key,
      this.label,
      this.value,
      this.controller,
      this.onsubmit,
      this.errortext,
      this.validator,
      this.onchanged})
      : super(key: key);
  final String label;
  final TextEditingController value;
  final Function onchanged;
  final String errortext;
  final Function validator;
  final TextEditingController controller;
  final Function onsubmit;
  @override
  Column build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        // SizedBox(
        //   height: 20,
        // ),
        Text(label,
            style: TextStyle(
                color: Pallet.dashcontainerback,
                fontSize: Pallet.heading4,
                fontWeight: FontWeight.w300)),
        SizedBox(height: 5),
        TextFormField(
            validator: validator,
            onChanged: onchanged,
            onEditingComplete: onsubmit,
            maxLines: 2,
            textAlign: TextAlign.left,
            style: TextStyle(color: Pallet.dashcontainerback),
            cursorColor: Pallet.dashcontainerback,
            keyboardType: TextInputType.text,
            controller: controller,
            decoration: InputDecoration(
              errorText: errortext,
              border: OutlineInputBorder(
                  borderSide: BorderSide(
                    color: Pallet.dashcontainerback,
                    width: 2.0,
                  ),
                  borderRadius: BorderRadius.circular(Pallet.radius)),
              enabledBorder: OutlineInputBorder(
                  borderSide:
                      BorderSide(color: Pallet.dashcontainerback, width: 2.0),
                  borderRadius: BorderRadius.circular(Pallet.radius)),
              fillColor: Pallet.fontcolor,
              focusedBorder: OutlineInputBorder(
                  borderSide:
                      BorderSide(color: Pallet.dashcontainerback, width: 2.0),
                  borderRadius: BorderRadius.circular(Pallet.radius)),
              labelStyle: TextStyle(
                color: Color(0xFFb6c3db),
                fontSize: 14,
              ),
              filled: true,
            )),
      ],
    );
  }
}

class SucessPopup extends StatelessWidget {
  const SucessPopup({Key key, this.message, this.route}) : super();
  final String message;
  final String route;
  @override
  AlertDialog build(BuildContext context) {
    return AlertDialog(
      elevation: 24.0,
      backgroundColor: Pallet.inner1,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.all(Radius.circular(15.0)),
      ),
      title: CircleAvatar(
          backgroundColor: Pallet.fontcolor,
          radius: 40,
          child: Icon(
            Icons.done_outline_rounded,
            color: Pallet.inner1,
            size: 30,
          )),
      content: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text(
            message,
            maxLines: 1,
            style: TextStyle(
                fontWeight: FontWeight.w500,
                fontSize: 15.0,
                color: Pallet.fontcolor),
          ),
        ],
      ),
      actions: [
        // ignore: deprecated_member_use
        FlatButton(
            child: Text(
              "Continue",
              style: TextStyle(color: Pallet.component),
            ),
            onPressed: () {
              // Navigator.of(context).pop();
              // Navigator.of(context).pop();
              Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => Home(
                          route: route,
                        )),
              );
            })
      ],
    );
  }
}

// ignore: must_be_immutable
class SignupTextBox extends StatefulWidget {
  SignupTextBox({
    Key key,
    this.label,
    this.onFieldSubmitted,
    this.focusnode,
    this.onChanged,
    this.onsubmit,
    this.autofocus,
    this.icon,
    this.controller,
    this.validation,
    this.errorText,
    this.tabpress,
    this.myimage,
    this.isPrefixIcon,
    this.isSuffixIcon,
    this.digitsOnlyPhone,
    this.readonly,
    this.isPassword,
    this.doubleonly,
    this.regexallow,
  }) : super(key: key) {
    shState = isPassword;
    hintText = label;
  }
  final String label;
  final FocusNode focusnode;
  final IconData icon;
  final String myimage;
  final bool autofocus;
  final Function onsubmit;
  final bool readonly;
  final TextEditingController controller;
  final Function validation;
  final Function onFieldSubmitted;

  final String errorText;
  final bool isPassword;
  final bool isPrefixIcon;
  final bool isSuffixIcon;
  final List<TextInputFormatter> regexallow;
  final bool doubleonly;
  final bool digitsOnlyPhone;
  final Function onChanged;
  final Function tabpress;
  bool shState;
  String hintText;

  @override
  _SignupTextBoxState createState() => _SignupTextBoxState();
}

class _SignupTextBoxState extends State<SignupTextBox> {
  // FocusNode focusNode = FocusNode();
  @override
// unFocus(context){
//   FocusNode focus =  FocusScope.of(context).unfocus();
// }
  _SignupTextBoxState() {
    test();
  }
  test() {}

  void initState() {
    super.initState();
    // focusNode.addListener(() {
    //   if (focusNode.hasFocus) {
    //     widget.hintText = '';
    //   } else {
    //     widget.hintText = widget.label;
    //   }
    //   setState(() {});
    // });
  }

  @override
  Widget build(BuildContext context) {
    // FocusNode unfocus;
    return
        // new RawKeyboardListener(
        //   focusNode: widget.focusnode,
        //   onKey: widget.tabpress,
        //   child:
        TextFormField(
      //focusNode: widget.node,
      controller: widget.controller,
      onChanged: widget.validation,
      obscureText: widget.shState,
      textAlign: TextAlign.left,
      autofocus: widget.autofocus == true ? true : false,
      readOnly: widget.readonly == true ? true : false,
      onFieldSubmitted: widget.onFieldSubmitted,
      validator: widget.validation,
      onEditingComplete: widget.onsubmit,

      style: TextStyle(
          color: Pallet.fontcolornew,
          fontSize: Pallet.heading3,
          fontWeight: Pallet.font500),
      cursorColor: Pallet.fontcolornew,
      keyboardType: TextInputType.emailAddress,
      inputFormatters: widget.digitsOnlyPhone == true
          ? <TextInputFormatter>[FilteringTextInputFormatter.digitsOnly]
          : widget.doubleonly == true
              ? [FilteringTextInputFormatter.allow(RegExp('[0-9.]'))]
              : widget.regexallow != null
                  ? widget.regexallow
                  : [],
      decoration: InputDecoration(
        errorText: widget.errorText,
        errorStyle: TextStyle(
          fontSize: Pallet.heading4,
          color: Pallet.errortxt,
        ),
        border: OutlineInputBorder(
            borderSide: const BorderSide(color: Color(0XFF1034a6), width: 1.0),
            borderRadius: BorderRadius.circular(8.0)),
// labelText: widget.label,
        hintText: widget.hintText,
        hintStyle: TextStyle(
            fontSize: Pallet.heading5,
            fontWeight: Pallet.font500,
            color: Pallet.fontcolornew),
        fillColor: Pallet.fontcolor,
        focusedBorder: OutlineInputBorder(
            borderSide: const BorderSide(color: Color(0XFF1034a6), width: 1.0),
            borderRadius: BorderRadius.circular(8.0)),
        enabledBorder: OutlineInputBorder(
            borderSide: BorderSide(color: Color(0xFF4570ae), width: 0.0),
            borderRadius: BorderRadius.circular(8.0)),
// labelStyle: TextStyle(
// color: Pallet.fontcolornew,
// fontSize: 25,
// ),
        filled: true,

        suffixIcon: widget.isSuffixIcon == true
            ? IconButton(
                // focusNode: null,
                icon: Icon(
                  widget.shState == true
                      ? Icons.visibility_off
                      : Icons.visibility,
                  size: 22,
                  color: Pallet.fontcolornew,
                ),
                onPressed: () {
                  FocusScope.of(context).unfocus();
                  setState(() {
                    widget.shState = !widget.shState;
                    // == true
                    //     ? widget.shState = false
                    //     : widget.shState = true;
                  });
                },
              )
            : null,
        //  widget.isSuffixIcon == true
        //     ? InkWell(
        //         onTap: () {
        // setState(() {
        //   widget.shState == true
        //       ? widget.shState = false
        //       : widget.shState = true;
        // });
        //         },
        //         child: widget.isPassword == true
        // ? Icon(
        //     widget.shState == true
        //         ? Icons.visibility_off
        //         : Icons.visibility,
        //     size: 22,
        //     color: Pallet.fontcolornew,
        //   )
        //             : Text('')

        // Column(
        //   mainAxisAlignment: MainAxisAlignment.center,
        //   children: [
        //     if (widget.isPassword)
        //       Icon(
        //         widget.shState == true
        //             ? Icons.visibility_off
        //             : Icons.visibility,
        //         size: 22,
        //         color: Pallet.fontcolornew,
        //       )
        //     else
        //       Container(
        //         width: 0,
        //       )
        //   ],
        // )
        // )
        // : null,
        // prefixIcon: widget.isPrefixIcon == false
        //     ? (Image.asset(
        //         widget.myimage,
        //         width: 10,
        //         height: 10,
        //       ))
        //     : Icon(widget.icon, size: 22, color: Pallet.fontcolornew)
        //

        prefixIcon: widget.isPrefixIcon == true
            ? (Icon(widget.icon, size: 22, color: Pallet.fontcolornew))
            : null,
      ),
      // ),
    );
// else
// return TextFormField();
  }
}

class Popup extends StatelessWidget {
  const Popup({Key key, this.witems}) : super(key: key);
  final List<Widget> witems;
  @override
  Widget build(BuildContext context) {
    return Dialog(
      elevation: 24.0,
      backgroundColor: Color(0xFF1034a6),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.all(Radius.circular(15.0)),
      ),
      child: Container(
        padding: EdgeInsets.all(20.0),
        width: 450,
        height: 320,
        child: Column(
            crossAxisAlignment: CrossAxisAlignment.start, children: witems),
      ),
    );
  }
}

class Loader1 extends StatefulWidget {
  Loader1({Key key}) : super(key: key);

  @override
  _Loader1State createState() => _Loader1State();
}

class _Loader1State extends State<Loader1> with TickerProviderStateMixin {
  AnimationController controller;
  AnimationController controller2;

  Animation imageAnimation;
  Animation textAnimation;

  @override
  void initState() {
    super.initState();
    controller =
        AnimationController(vsync: this, duration: Duration(milliseconds: 700));
    controller2 =
        AnimationController(vsync: this, duration: Duration(seconds: 1));

    controller.addListener(() {
      if (controller.isCompleted) {
        controller.reverse();
      } else if (controller.isDismissed) {
        controller.forward();
      }
      setState(() {});
    });
    controller.forward();
    controller2.repeat();
  }

  void dispose() {
    controller.dispose();
    controller2.dispose();

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;

    imageAnimation =
        CurvedAnimation(parent: controller, curve: Curves.decelerate);

    imageAnimation = Tween(begin: size.width * 0.7, end: size.width * 0.72)
        .animate(imageAnimation);

    textAnimation = CurvedAnimation(
        parent: controller2, curve: Curves.fastLinearToSlowEaseIn);

    textAnimation = Tween(begin: 0.0, end: 1.0).animate(textAnimation);

    return Container(
        width: size.width,
        height: size.height * 0.9,
        color: Colors.transparent,
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              CircularProgressIndicator(),
              SizedBox(height: 10),
              Text(
                'Loading...',
                style: TextStyle(
                  fontSize: Pallet.heading2,
                  color: Pallet.fontcolornew,
                  fontWeight: Pallet.font500,
                ),
              ),
            ],
          ),
        ));
  }
}

class Loader extends StatelessWidget {
  final Color indicatorcolor;
  final String text;
  final FontWeight textweight;
  final Color textcolor;
  final double textsize;
  final double sizedbox;

  const Loader(
      {Key key,
      this.indicatorcolor,
      this.sizedbox,
      this.text,
      this.textweight,
      this.textcolor,
      this.textsize})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          CircularProgressIndicator(
            valueColor: AlwaysStoppedAnimation(
                indicatorcolor != null ? indicatorcolor : Pallet.fontcolornew),
          ),
          SizedBox(height: sizedbox == null ? 10 : sizedbox),
          Text(
            text == null ? 'Loading...' : text,
            style: TextStyle(
              fontSize: textsize == null ? Pallet.heading2 : textsize,
              color: textcolor == null ? Pallet.fontcolornew : textcolor,
              fontWeight: textweight == null ? Pallet.font500 : textweight,
            ),
          ),
        ],
      ),
    );
  }
}

class Website extends StatelessWidget {
  Website({Key key, this.url}) : super(key: key);
  final String url;
  // var _controller;
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Container(
      width: size.width,
      height: size.height,
      child: WebView(
        initialUrl: url.toString(),
        javascriptMode: JavascriptMode.unrestricted,
        // onWebViewCreated: (WebViewController webcontroller) {
        // _controller.complete(webcontroller);
        // },
      ),
    );
  }
}

class KycPopUp extends StatefulWidget {
  @override
  _KycPopUpState createState() => _KycPopUpState();
}

class _KycPopUpState extends State<KycPopUp> {
  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      backgroundColor: Pallet.popupcontainerback,
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(Pallet.radius)),
      title: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Image.asset("c-logo.png", width: 30),
          SizedBox(width: 10),
          Expanded(
            child: Text('KYC Not Confirmed!',
                style: TextStyle(
                    fontSize: Pallet.heading3,
                    color: Pallet.fontcolor,
                    fontWeight: Pallet.font500)),
          ),
        ],
      ),
      content: Container(
        height: 200,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            Image.asset('kycimage.png',
                width: 100, height: 100, color: Pallet.fontcolor),

            // Text('Please Confirm KYC',
            //     style: TextStyle(
            //         fontSize: Pallet.normalfont,
            //         color: Pallet.fontcolor,
            //         fontWeight: Pallet.font500)),
            Text("Your KYC has not verified",
                style: TextStyle(
                  fontSize: Pallet.heading5,
                  color: Pallet.fontcolor,
                )),
            // Row(
            //   mainAxisAlignment: MainAxisAlignment.spaceAround,
            //   children: [
            //     PopupButton(
            //         text: 'Go KYC',
            //         onpress: () {
            // Navigator.push(
            //     context,
            //     MaterialPageRoute(
            //         builder: (context) => Home(
            //               route: 'kyc',
            //             )));
            //         }),
            //     PopupButton(
            //       onpress: () {
            //         Navigator.of(context).pop();
            //       },
            //       text: 'Back',
            //     )
            //   ],
            // )
          ],
        ),
      ),
      actions: [
        // PopupButton(
        //     text: 'Go KYC',
        //     onpress: () {
        //       Navigator.push(
        //           context,
        //           MaterialPageRoute(
        //               builder: (context) => Home(
        //                     route: 'kyc',
        //                   )));
        //     }),
        PopupButton(
          onpress: () {
            Navigator.of(context).pop();
          },
          text: 'Close',
        )
      ],
    );
  }
}

class Insufficientfunds extends StatelessWidget {
  Insufficientfunds({Key key, this.content, this.onpress1, this.onpress})
      : super(key: key);
  // final Widget title;
  final Widget content;
  final Function onpress;
  final Function onpress1;

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      backgroundColor: Pallet.popupcontainerback,
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(Pallet.radius)),
      title: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Image.asset("c-logo.png", width: 30),
          SizedBox(width: 10),
          Expanded(
            child: Text('Insufficient Funds',
                style: TextStyle(
                    fontSize: Pallet.normalfont + 5,
                    color: Pallet.fontcolor,
                    fontWeight: Pallet.font500)),
          ),
        ],
      ),
      content: content,
      actions: [
        PopupButton(
          onpress: onpress,
          text: 'Add Fund',
        ),
        PopupButton(
          onpress: onpress1,
          text: 'Close',
        )
      ],
    );
  }
}

class CustomButton extends StatelessWidget {
  final Color textcolor;
  final String text;
  final double textsize;
  final double width;
  final double height;
  final Color buttoncolor;
  final double vpadding;
  final double bradius;
  final bool bshadow;
  final double radius;
  final double sradius;
  final double hpadding;
  final Function onpress;
  final Widget child;
  final FontWeight fontweight;
  final TextDecoration textDecoration;
  const CustomButton(
      {Key key,
      this.textcolor,
      this.text,
      this.textsize,
      this.width,
      this.height,
      this.buttoncolor,
      this.vpadding,
      this.hpadding,
      this.onpress,
      this.fontweight,
      this.child,
      this.textDecoration,
      this.bradius,
      this.bshadow,
      this.sradius,
      this.radius})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Bouncing(
      onPress: onpress,
      child: MouseRegion(
        cursor: SystemMouseCursors.click,
        child: Container(
          height: height,
          width: width,
          padding: EdgeInsets.symmetric(
            vertical: vpadding == null ? 5 : vpadding,
            horizontal: hpadding == null ? 10 : hpadding,
          ),
          decoration: BoxDecoration(
            boxShadow: [
              bshadow == true
                  ? BoxShadow(
                      color: Pallet.fontcolornew.withOpacity(1),
                      offset: Offset(4, 6),
                      spreadRadius: sradius,
                      blurRadius: bradius)
                  : bshadow == null
                      ? BoxShadow()
                      : BoxShadow(),
            ],
            color: buttoncolor == null ? Pallet.buttonback : buttoncolor,
            borderRadius: BorderRadius.circular(radius == null ? 7.0 : radius),
          ),
          child: child == null
              ? Center(
                  child: Text(
                    text,
                    style: TextStyle(
                      decoration: textDecoration,
                      fontWeight:
                          fontweight == null ? Pallet.font500 : fontweight,
                      color:
                          textcolor == null ? Pallet.fontcolornew : textcolor,
                      fontSize: textsize == null ? Pallet.heading4 : textsize,
                    ),
                  ),
                )
              : child,
        ),
      ),
    );
  }
}

class PopupButton extends StatelessWidget {
  final Color textcolor;
  final String text;
  final double textsize;
  final double width;
  final double height;
  final Color buttoncolor;
  final double vpadding;
  final double hpadding;
  final Function onpress;
  final Widget child;
  final FontWeight fontweight;
  const PopupButton(
      {Key key,
      this.textcolor,
      this.text,
      this.textsize,
      this.width,
      this.height,
      this.buttoncolor,
      this.vpadding,
      this.hpadding,
      this.onpress,
      this.fontweight,
      this.child})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Bouncing(
      onPress: onpress,
      child: MouseRegion(
        cursor: SystemMouseCursors.click,
        child: Container(
          height: height,
          width: width,
          padding: EdgeInsets.symmetric(
            vertical: vpadding == null ? 5 : vpadding,
            horizontal: hpadding == null ? 10 : hpadding,
          ),
          decoration: BoxDecoration(
            color: buttoncolor == null ? Pallet.buttonback : buttoncolor,
            borderRadius: BorderRadius.circular(5.0),
          ),
          child: child == null
              ? Center(
                  child: Text(
                    text,
                    style: TextStyle(
                      fontWeight: fontweight,
                      color:
                          textcolor == null ? Pallet.fontcolornew : textcolor,
                      fontSize: textsize == null ? Pallet.heading6 : textsize,
                    ),
                  ),
                )
              : child,
        ),
      ),
    );
  }
}

class AddressProcessPopUp extends StatefulWidget {
  AddressProcessPopUp(
      {Key key,
      this.setData,
      this.index,
      this.isAdd,
      this.shopContext,
      this.notifyParent})
      : super(key: key);

  final setData;
  final shopContext;
  final index;
  final String isAdd;
  final Function() notifyParent;

  @override
  _AddressProcessPopUpState createState() => _AddressProcessPopUpState();
}

class _AddressProcessPopUpState extends State<AddressProcessPopUp> {
  Map<String, dynamic> pageDetails = {};

  GlobalKey<FormState> addressform = GlobalKey<FormState>();
  TextEditingController addressline1 = TextEditingController();
  TextEditingController addressline2 = TextEditingController();
  TextEditingController addressname = TextEditingController();
  TextEditingController phonenumber = TextEditingController();
  TextEditingController country = TextEditingController();
  TextEditingController state = TextEditingController();
  TextEditingController district = TextEditingController();
  TextEditingController postalcode = TextEditingController();
  String nameError;
  String addressline1Error;
  String addressline2Error;
  String phonenumberError;
  String countryError;
  String stateError;
  String districtError;

  String postalcodeError;

  String dropDownCountry = '';
  String dropDownCity = '';
  String dropDownZip = '';

  FocusNode statefocus = FocusNode();
  FocusNode postalcodefocus = FocusNode();

  FocusNode districtfocus = FocusNode();
  FocusNode countryfocus = FocusNode();
  FocusNode phonenumberfocus = FocusNode();
  FocusNode addressline1focus = FocusNode();
  FocusNode addressline2focus = FocusNode();
  FocusNode addressnamefocus = FocusNode();
  String isMobile;
  // String isAdd;
  bool isRequired = false;
  bool enablecity = false;
  bool enablezipcode = false;
  FocusNode continuenode = FocusNode();
  Fund fund = Fund();
  Map<String, dynamic> getRegionsMap = {
    'system_product_id': 1,
    'region_level': 2,
    'parent_region': 'null'
  };
  void editaddres(index) {
    setState(() {
      addressname.text = cart.address[index]['address_name'];
      addressline1.text = cart.address[index]['address_line1'];
      addressline2.text = cart.address[index]['address_line2'];
      phonenumber.text = cart.address[index]['address_phone'];
      dropDownCountry = cart.address[index]['country'];
      state.text = cart.address[index]['region'];
      district.text = cart.address[index]['city'];
      postalcode.text = cart.address[index]['postal_code'];
    });
  }

  validateAddress() {
    addressform.currentState.validate();
  }

  void initState() {
    setPageDetails();

    cart.getAddressInfo(setState);
    cart.getRegions(getRegionsMap);

    if (widget.index != null) {
      editaddres(widget.index);
    }
    print('________________________');
    print(widget.index);
    super.initState();
  }

  void setPageDetails() async {
    String data = await Utils.getPageDetails('common_address');
    setState(() {
      pageDetails = Utils.fromJSONString(data);
    });
  }

  List<DropdownMenuItem<String>> countryList = [];
  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      backgroundColor: Pallet.fontcolor,
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(Pallet.radius)),
      title: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Image.asset("c-logo.png", width: 30, color: Pallet.fontcolornew),
          SizedBox(width: 10),
          Expanded(
            child: Text(
                widget.isAdd == 'true'
                    ? pageDetails["text1"]
                    : pageDetails["text2"],
                style: TextStyle(
                    fontSize: Pallet.heading2,
                    color: Pallet.fontcolornew,
                    fontWeight: Pallet.font500)),
          ),
        ],
      ),
      content: ConstrainedBox(
        constraints: BoxConstraints(maxWidth: isMobile == 'true' ? 450 : 650),
        child: Container(
          // width: 400,
          // height: 400,
          child: SingleChildScrollView(
            child: Form(
              key: addressform,
              child: Wrap(
                // mainAxisAlignment: MainAxisAlignment.spaceEvenly,

                spacing: Pallet.defaultPadding / 2,
                runSpacing: Pallet.defaultPadding,
                children: [
                  Container(
                    width: isMobile == 'true' ? 200 : 300,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(pageDetails["text3"],
                            style: TextStyle(
                                fontSize: Pallet.normalfont,
                                color: Pallet.fontcolornew,
                                fontWeight: Pallet.font500)),
                        SizedBox(height: 5),
                        SignupTextBox(
                            // tabpress: (event) {
                            //   if (event.isKeyPressed(LogicalKeyboardKey.tab)) {
                            //     FocusScope.of(context)
                            //         .requestFocus(addressline1focus);
                            //   }
                            // },
                            // onsubmit: () {
                            //   FocusScope.of(context)
                            //       .requestFocus(addressline1focus);
                            // },
                            // focusnode: addressnamefocus,
                            errorText: nameError,
                            // onChanged: (value) => validateAddress(),
                            onFieldSubmitted: (value) => submitAddress(),
                            autofocus: true,
                            isPassword: false,
                            digitsOnlyPhone: false,

                            // header: 'llllllllll',
                            // label: ' Addressname',
                            controller: addressname,
                            validation: (value) {
                              setState(() {
                                if (value.trim().isEmpty) {
                                  nameError = pageDetails["text12"];
                                } else {
                                  nameError = null;
                                }
                              });
                            }),
                      ],
                    ),
                  ),
                  SizedBox(height: 10),
                  Container(
                    width: isMobile == 'true' ? 200 : 300,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(pageDetails["text4"],
                            style: TextStyle(
                                fontSize: Pallet.normalfont,
                                color: Pallet.fontcolornew,
                                fontWeight: Pallet.font500)),
                        SizedBox(height: 5),
                        SignupTextBox(
                            // tabpress: (event) {
                            //   if (event.isKeyPressed(LogicalKeyboardKey.tab)) {
                            //     FocusScope.of(context)
                            //         .requestFocus(phonenumberfocus);
                            //   }
                            // },
                            // focusnode: addressline1focus,
                            // onsubmit: () {
                            //   FocusScope.of(context)
                            //       .requestFocus(phonenumberfocus);
                            // },
                            errorText: addressline1Error,
                            // onChanged: (value) => validateAddress(),
                            onFieldSubmitted: (value) => submitAddress(),
                            isPassword: false,
                            digitsOnlyPhone: false,

                            // header: 'llllllllll',
                            // label: ' Addressline1',
                            controller: addressline1,
                            validation: (value) {
                              setState(() {
                                if (value.trim().isEmpty) {
                                  addressline1Error = pageDetails["text12"];
                                } else {
                                  addressline1Error = null;
                                }
                              });
                            }),
                      ],
                    ),
                  ),
                  // SizedBox(height: 10),
                  // Container(
                  //   width: isMobile == 'true' ? 200 : 300,
                  //   child: Column(
                  //     crossAxisAlignment: CrossAxisAlignment.start,
                  //     children: [
                  //       Text('Address Line2',
                  //           style: TextStyle(
                  //               fontSize: Pallet.normalfont,
                  //               color: Pallet.fontcolornew,
                  //               fontWeight: Pallet.font500)),
                  //       SizedBox(height: 5),
                  //       SignupTextBox(
                  //           tabpress: (event) {
                  //             if (event
                  //                 .isKeyPressed(LogicalKeyboardKey.tab)) {
                  //               FocusScope.of(context)
                  //                   .requestFocus(phonenumberfocus);
                  //             }
                  //           },
                  //           focusnode: addressline2focus,
                  //           onsubmit: () {
                  //             FocusScope.of(context)
                  //                 .requestFocus(phonenumberfocus);
                  //           },
                  //           errorText: addressline2Error,
                  //           isPassword: false,
                  //           digitsOnlyPhone: false,

                  //           // header: 'llllllllll',
                  //           // label: ' addressline2',
                  //           controller: addressline2,
                  //           validation: (value) {
                  //             setState(() {
                  //               if (value.isEmpty) {
                  //                 addressline2Error = 'Required';
                  //               } else {
                  //                 addressline2Error = null;
                  //               }
                  //             });
                  //           }),
                  //     ],
                  //   ),
                  // ),
                  SizedBox(height: 10),
                  Container(
                    width: isMobile == 'true' ? 200 : 300,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(pageDetails["text5"],
                            style: TextStyle(
                                fontSize: Pallet.normalfont,
                                color: Pallet.fontcolornew,
                                fontWeight: Pallet.font500)),
                        SizedBox(height: 5),
                        SignupTextBox(
                            // tabpress: (event) {
                            //   if (event.isKeyPressed(LogicalKeyboardKey.tab)) {
                            //     FocusScope.of(context)
                            //         .requestFocus(countryfocus);
                            //   }
                            // },
                            // onsubmit: () {
                            //   FocusScope.of(context).requestFocus(countryfocus);
                            // },
                            // focusnode: phonenumberfocus,
                            errorText: phonenumberError,
                            // onChanged: (value) => validateAddress(),
                            onFieldSubmitted: (value) => submitAddress(),
                            isPassword: false,
                            digitsOnlyPhone: true,
                            // header: 'llllllllll',
                            // label: 'addressLine12',
                            controller: phonenumber,
                            validation: (value) {
                              setState(() {
                                if (value.trim().isEmpty) {
                                  phonenumberError = pageDetails["text12"];
                                } else {
                                  phonenumberError = null;
                                }
                              });
                            }),
                      ],
                    ),
                  ),
                  SizedBox(height: 10),
                  Container(
                    width: isMobile == 'true' ? 200 : 300,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(pageDetails["text6"],
                            style: TextStyle(
                                fontSize: Pallet.normalfont,
                                color: Pallet.fontcolornew,
                                fontWeight: Pallet.font500)),
                        SizedBox(height: 5),
                        // DropDown(
                        //     label: 'Select Country',
                        //     labeltext: 'Select Country',
                        //     items: cart.countries,
                        //     value: dropDownCountry,
                        //     // controller: country,
                        //     onChange: (newValue) {
                        //       setState(() {
                        //         dropDownCountry = newValue;
                        //         getRegionsMap['region_level'] = 4;
                        //         getRegionsMap['parent_region'] =
                        //             dropDownCountry.toUpperCase();

                        //         cart.getCities(getRegionsMap);
                        //       });
                        //     },
                        //     errortext: countryError),

                        // RawKeyboardListener(
                        //   focusNode: countryfocus,
                        //   onKey: (event) {
                        //     if (event.isKeyPressed(LogicalKeyboardKey.tab)) {
                        //       FocusScope.of(context)
                        //           .requestFocus(districtfocus);
                        //     }
                        //   },
                        // child:
                        DropDownField(
                            // hintText: 'Please choose country',
                            required: isRequired,
                            textStyle: TextStyle(color: Pallet.fontcolornew),
                            value: dropDownCountry,
                            onValueChanged: (newValue) {
                              setState(() {
                                // cart.cities = [];
                                // cart.zipCode = [];
                                // district.text = '';
                                // postalcode.text = '';
                                // enablecity = true;
                                dropDownCountry = newValue;
                                // getRegionsMap['region_level'] = 4;
                                // getRegionsMap['parent_region'] =
                                //     dropDownCountry.toUpperCase();

                                // cart.getCities(getRegionsMap);
                              });
                            },
                            items: cart.countries),
                        // ),
                        // SignupTextBox(
                        //     tabpress: (event) {2S22w2s21 2q2 `w2wc.
                        //             .requestFocus(statefocus);
                        //       }
                        //     },
                        //     focusnode: countryfocus,
                        //     onsubmit: () {
                        //       FocusScope.of(context)
                        //           .requestFocus(statefocus);
                        //     },
                        //     errorText: countryError,
                        //     isPassword: false,
                        //     digitsOnlyPhone: false,
                        //     // header: 'llllllllll',1dw`2Q
                        //     // label: 'addressLine12',
                        //     controller: country,
                        //     validation: (value) {
                        //       setState(() {
                        //         if (value.isEmpty) {
                        //           countryError = 'Required';
                        //         } else {
                        //           countryError = null;
                        //         }12
                        //       });
                        //     }),
                      ],
                    ),
                  ),
                  // SizedBox(height: 10),
                  // Container(
                  //   width: isMobile == 'true' ? 200 : 300,
                  //   child: Column(
                  //     crossAxisAlignment: CrossAxisAlignment.start,
                  //     children: [
                  //       Text('State',
                  //           style: TextStyle(
                  //               fontSize: Pallet.normalfont,
                  //               color: Pallet.fontcolornew,
                  //               fontWeight: Pallet.font500)),
                  //       SizedBox(height: 5),
                  //       SignupTextBox(
                  //           tabpress: (event) {
                  //             if (event
                  //                 .isKeyPressed(LogicalKeyboardKey.tab)) {
                  //               FocusScope.of(context)
                  //                   .requestFocus(districtfocus);
                  //             }
                  //           },
                  //           focusnode: statefocus,
                  //           onsubmit: () {
                  //             FocusScope.of(context)
                  //                 .requestFocus(districtfocus);
                  //           },
                  //           errorText: stateError,
                  //           isPassword: false,
                  //           digitsOnlyPhone: false,
                  //           // header: 'llllllllll',
                  //           // label: 'addressLine12',
                  //           controller: state,
                  //           validation: (value) {
                  //             setState(() {
                  //               if (value.isEmpty) {
                  //                 stateError = 'Required';
                  //               } else {
                  //                 stateError = null;
                  //               }
                  //             });
                  //           }),
                  //     ],
                  //   ),
                  // ),
                  SizedBox(height: 10),
                  Container(
                    width: isMobile == 'true' ? 200 : 300,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(pageDetails["text7"],
                            style: TextStyle(
                                fontSize: Pallet.normalfont,
                                color: Pallet.fontcolornew,
                                fontWeight: Pallet.font500)),
                        SizedBox(height: 5),

                        SignupTextBox(
                            // focusnode: districtfocus,
                            // tabpress: (event) {
                            //   if (event.isKeyPressed(LogicalKeyboardKey.tab)) {
                            //     FocusScope.of(context)
                            //         .requestFocus(postalcodefocus);
                            //   }
                            // },
                            // onsubmit: () {
                            //   FocusScope.of(context)
                            //       .requestFocus(postalcodefocus);
                            // },
                            // onChanged: (value) => validateAddress(),
                            onFieldSubmitted: (value) => submitAddress(),
                            errorText: districtError,
                            isPassword: false,
                            digitsOnlyPhone: false,
                            // header: 'llllllllll',
                            // label: 'addressLine12',
                            regexallow: [
                              FilteringTextInputFormatter.allow(
                                  RegExp("[0-9a-zA-Z ]"))
                            ],
                            controller: district,
                            validation: (value) {
                              setState(() {
                                if (value.trim().isEmpty) {
                                  districtError = pageDetails["text12"];
                                } else {
                                  districtError = null;
                                }
                              });
                            }),

                        // AbsorbPointer(
                        //   absorbing: !enablecity ? true : false,
                        //   child: TypeAheadFormField(
                        //     errortext: districtError,
                        //     textFieldConfiguration: TextFieldConfiguration(
                        //       cursorColor: Pallet.fontcolornew,
                        //       style: TextStyle(
                        //           color: Pallet.fontcolornew,
                        //           fontSize: Pallet.heading3,
                        //           fontWeight: Pallet.font500),
                        //       controller: district,
                        //     ),
                        //     suggestionsCallback: (pattern) {
                        //       return cart.cities;
                        //     },
                        //     itemBuilder: (context, suggestion) {
                        //       return ListTile(
                        //         title: Text(
                        //           suggestion,
                        //           style: TextStyle(color: Pallet.fontcolornew),
                        //         ),
                        //       );
                        //     },
                        //     transitionBuilder:
                        //         (context, suggestionsBox, controller) {
                        //       return suggestionsBox;
                        //     },
                        //     onSuggestionSelected: (suggestion) {
                        //       setState(() {
                        //         print('lllllllllllllllllllllllllllll');
                        //         cart.zipCode.clear();
                        //         this.district.text = suggestion;
                        //         districtError = null;
                        //         postalcode.text = '';
                        //         enablezipcode = true;
                        //         // postalcodeError = "Required";
                        //         getRegionsMap['region_level'] = 5;
                        //         getRegionsMap['parent_region'] = district.text;
                        //         // isRequired = false;

                        //         // print(isRequired);
                        //         print(
                        //             'VVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVV');

                        //         cart.getZipCode(getRegionsMap);
                        //       });
                        //     },
                        //     // ignore: missing_return
                        //     onchanged: (value) {
                        //       if (value.isEmpty) {
                        //         setState(() {
                        //           districtError = 'Required';
                        //         });
                        //       } else {
                        //         setState(() {
                        //           enablezipcode = true;
                        //           districtError = null;
                        //         });
                        //       }
                        //     },
                        //     onSaved: (value) {
                        //       setState(() {
                        //         if (value.isEmpty) {
                        //           districtError = 'Required';
                        //         } else {
                        //           districtError = null;
                        //         }
                        //       });
                        //     },
                        //     // ignore: missing_return
                        //     validator: (value) {
                        //       if (value.isEmpty) {
                        //         setState(() {
                        //           districtError = 'Required';
                        //         });
                        //       } else {
                        //         setState(() {
                        //           districtError = null;
                        //         });
                        //       }
                        //     },
                        //   ),
                        // ),

                        // this.district.text == ''
                        //     ? Padding(
                        //         padding:
                        //             EdgeInsets.all(Pallet.defaultPadding),
                        //         child: Container(
                        //             child: isRequired == true
                        //                 ? Text(
                        //                     'Required',
                        //                     style: TextStyle(
                        //                         color: Pallet.failed,
                        //                         fontSize:
                        //                             Pallet.normalfont),
                        //                   )
                        //                 : Text(
                        //                     '',
                        //                   )),
                        //       )
                        //     : Container()
                        // InkWell(
                        //   onTap: () {
                        //     print(district.text);
                        //   },
                        //   child: Container(
                        //     child: districtError != null
                        //         ? Text('Required')
                        //         : Text(''),
                        //   ),
                        // )
                        // DropDown(
                        //     label: 'Select City',
                        //     labeltext: 'Select City',
                        //     items: cart.cities,
                        //     value: dropDownCity,
                        //     controller: district,
                        //     onChange: (value) {
                        // setState(() {
                        //   dropDownCity = value;

                        //   getRegionsMap['region_level'] = 5;
                        //   getRegionsMap['parent_region'] =
                        //       dropDownCity;

                        //   cart.getZipCode(getRegionsMap);
                        // });
                        //     },
                        //     errortext: districtError),
                        // DropDownField(
                        //     required: true,
                        //     hintText: 'Please choose city',
                        //     value: dropDownCity,
                        //     onValueChanged: (value) {
                        //       setState(() {
                        //         dropDownCity = value;

                        //         getRegionsMap['region_level'] = 5;
                        //         getRegionsMap['parent_region'] =
                        //             dropDownCity;

                        //         cart.getZipCode(getRegionsMap);
                        //       });
                        //     },
                        //     items: cart.cities),
                        // SignupTextBox(
                        //     tabpress: (event) {
                        //       if (event
                        //           .isKeyPressed(LogicalKeyboardKey.tab)) {
                        //         FocusScope.of(context)
                        //             .requestFocus(postalcodefocus);
                        //       }
                        //     },
                        //     focusnode: districtfocus,
                        //     onsubmit: () {
                        //       FocusScope.of(context)
                        //           .requestFocus(postalcodefocus);
                        //     },
                        //     errorText: districtError,
                        //     isPassword: false,
                        //     digitsOnlyPhone: false,
                        //     // header: 'llllllllll',
                        //     // label: 'addressLine12',
                        //     controller: district,
                        //     validation: (value) {
                        //       setState(() {
                        //         if (value.isEmpty) {
                        //           districtError = 'Required';
                        //         } else {
                        //           districtError = null;
                        //         }
                        //       });
                        //     }),
                      ],
                    ),
                  ),
                  SizedBox(height: 10),
                  Container(
                    width: isMobile == 'true' ? 200 : 300,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(pageDetails["text8"],
                            style: TextStyle(
                                fontSize: Pallet.normalfont,
                                color: Pallet.fontcolornew,
                                fontWeight: Pallet.font500)),
                        SizedBox(height: 5),

                        SignupTextBox(
                            // focusnode: postalcodefocus,
                            errorText: postalcodeError,
                            // onChanged: (value) => validateAddress(),
                            onFieldSubmitted: (value) => submitAddress(),
                            isPassword: false,
                            digitsOnlyPhone: false,
                            regexallow: [
                              FilteringTextInputFormatter.allow(
                                  RegExp("[a-zA-Z0-9 ]"))
                            ],
                            // header: 'llllllllll',
                            // label: 'addressLine12',
                            controller: postalcode,
                            validation: (value) {
                              setState(() {
                                if (value.trim().isEmpty) {
                                  postalcodeError = pageDetails["text12"];
                                } else {
                                  postalcodeError = null;
                                }
                              });
                            }),

                        // Container(
                        //   // decoration: BoxDecoration(
                        //   //     border:
                        //   //         Border.all(color: Pallet.fontcolornew)),
                        //   child: AbsorbPointer(
                        //     absorbing: !enablezipcode ? true : false,
                        //     child: TypeAheadFormField(
                        //       // ignore: missing_return
                        //       validator: (val) {
                        //         if (val.isEmpty) {}
                        //       },
                        //       errortext: postalcodeError,
                        //       textFieldConfiguration: TextFieldConfiguration(
                        //           cursorColor: Pallet.fontcolornew,
                        //           style: TextStyle(
                        //               color: Pallet.fontcolornew,
                        //               fontSize: Pallet.heading3,
                        //               fontWeight: Pallet.font500),
                        //           controller: postalcode,
                        //           decoration: InputDecoration(
                        //             // labelText: 'City',
                        //             border: OutlineInputBorder(
                        //                 borderSide: const BorderSide(
                        //                     color: Color(0XFF1034a6), width: 1.0),
                        //                 borderRadius: BorderRadius.circular(8.0)),
                        //             errorBorder: OutlineInputBorder(
                        //                 borderSide: BorderSide(
                        //                     color: Colors.red, width: 2),
                        //                 borderRadius:
                        //                     BorderRadius.circular(Pallet.radius)),
                        //             focusedErrorBorder: OutlineInputBorder(
                        //                 borderSide: BorderSide(
                        //                     color: Colors.red, width: 2),
                        //                 borderRadius:
                        //                     BorderRadius.circular(Pallet.radius)),
                        //             focusedBorder: OutlineInputBorder(
                        //                 borderSide: const BorderSide(
                        //                     color: Color(0XFF1034a6), width: 1.0),
                        //                 borderRadius: BorderRadius.circular(8.0)),
                        //             enabledBorder: OutlineInputBorder(
                        //                 borderSide: BorderSide(
                        //                     color: Color(0xFF4570ae), width: 0.0),
                        //                 borderRadius: BorderRadius.circular(8.0)),
                        //           )),
                        //       suggestionsCallback: (pattern) {
                        //         return cart.zipCode;
                        //       },

                        //       itemBuilder: (context, suggestion) {
                        //         return ListTile(
                        //           title: Text(
                        //             suggestion,
                        //             style: TextStyle(color: Pallet.fontcolornew),
                        //           ),
                        //         );
                        //       },
                        //       transitionBuilder:
                        //           (context, suggestionsBox, controller) {
                        //         return suggestionsBox;
                        //       },
                        //       onSuggestionSelected: (suggestion) {
                        //         setState(() {
                        //           // isRequired = false;
                        //           postalcodeError = null;

                        //           this.postalcode.text = suggestion;
                        //         });
                        //       },

                        //       // autovalidateMode: AutovalidateMode.always,
                        //       // hideOnError: true,
                        //       // noItemsFoundBuilder: (BuildContext context) {
                        //       //   return Container(
                        //       //       color: Pallet.fontcolor,
                        //       //       child: Text(
                        //       //         'Required',
                        //       //         style: TextStyle(
                        //       //             color: Pallet.failed,
                        //       //             fontSize: Pallet.normalfont),
                        //       //       ));
                        //       // },
                        //     ),
                        //   ),
                        // ),
                        // this.postalcode.text == ''
                        //     ? Padding(
                        //         padding:
                        //             EdgeInsets.all(Pallet.defaultPadding),
                        //         child: Container(
                        //             child: isRequired == true
                        //                 ? Text(
                        //                     'Required',
                        //                     style: TextStyle(
                        //                         color: Pallet.failed,
                        //                         fontSize:
                        //                             Pallet.normalfont),
                        //                   )
                        //                 : Text(
                        //                     '',
                        //                   )),
                        //       )
                        //     : Container()
                        // DropDown(
                        //     label: 'Select ZipCode',
                        //     labeltext: 'Select ZipCode',
                        //     items: cart.zipCode,
                        //     value: dropDownZip,
                        //     controller: postalcode,
                        //     onChange: (value) {
                        //       setState(() {
                        //         dropDownZip = value;
                        //       });
                        //     },
                        //     errortext: postalcodeError),

                        // DropDownField(
                        //     hintText: 'Please choose ZipCode',
                        //     value: dropDownZip,
                        //     required: true,
                        //     onValueChanged: (value) {
                        //       setState(() {
                        //         dropDownZip = value;

                        //         // getRegionsMap['region_level'] = 5;
                        //         // getRegionsMap['parent_region'] =
                        //         //     dropDownCity;

                        //         // cart.getRegions(getRegionsMap);
                        //       });
                        //     },
                        //     items: cart.zipCode),

                        // SignupTextBox(
                        //     tabpress: (event) {
                        //       if (event
                        //           .isKeyPressed(LogicalKeyboardKey.tab)) {
                        //         FocusScope.of(context)
                        //             .requestFocus(continuenode);
                        //       }
                        //     },
                        //     focusnode: postalcodefocus,
                        //     onsubmit: () {
                        //       FocusScope.of(context)
                        //           .requestFocus(continuenode);
                        //     },
                        //     errorText: postalcodeError,
                        //     isPassword: false,
                        //     digitsOnlyPhone: true,
                        //     // header: 'llllllllll',
                        //     // label: 'addressLine12',
                        //     controller: postalcode,
                        //     validation: (value) {
                        //       setState(() {
                        //         if (value.isEmpty) {
                        //           postalcodeError = 'Required';
                        //         } else {
                        //           postalcodeError = null;
                        //         }
                        //       });
                        //     }),
                        //
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
      actions: [
        PopupButton(
            textcolor: Pallet.fontcolor,
            buttoncolor: Pallet.fontcolornew,
            text: pageDetails["text9"],
            onpress: () {
              nameError = null;
              addressline1Error = null;
              addressline2Error = null;
              phonenumberError = null;
              countryError = null;
              stateError = null;
              districtError = null;

              postalcodeError = null;

              Navigator.of(context).pop();
            }),
        PopupButton(
            textcolor: Pallet.fontcolor,
            buttoncolor: Pallet.fontcolornew,
            text: widget.isAdd == 'true'
                ? pageDetails["text10"]
                : pageDetails["text11"],
            onpress: () {
              submitAddress();
              // setState(() {
              //   isRequired = true;
              // });
//               SharedPreferences prefs = await SharedPreferences.getInstance();

//               if (addressline1.text.trim().isEmpty ||
//                   // addressline2.text.isEmpty ||
//                   addressname.text.trim().isEmpty ||
//                   phonenumber.text.trim().isEmpty ||
//                   // state.text.isEmpty ||
//                   dropDownCountry.trim().isEmpty ||
//                   district.text.trim().isEmpty ||
//                   postalcode.text.trim().isEmpty) {
//                 if (district.text.trim().length < 1) {
//                   setState(() {
//                     districtError = pageDetails["text12"];
//                   });
//                 }
//                 if (addressline1.text.trim().length < 1) {
//                   setState(() {
//                     addressline1Error = pageDetails["text12"];
//                   });
//                 }
//                 // if (addressline2.text.length < 1) {
//                 //   setState(() {
//                 //     addressline2Error = 'Required';
//                 //   });
//                 // }
//                 if (addressname.text.trim().length < 1) {
//                   setState(() {
//                     nameError = pageDetails["text12"];
//                   });
//                 }
//                 if (phonenumber.text.trim().length < 1) {
//                   setState(() {
//                     phonenumberError = pageDetails["text12"];
//                   });
//                 }
//                 // if (state.text.length < 1) {
//                 //   setState(() {
//                 //     stateError = 'Required';
//                 //   });
//                 // }
//                 if (postalcode.text.trim().length < 1) {
//                   setState(() {
//                     postalcodeError = pageDetails["text12"];
//                   });
//                 }
//                 if (dropDownCountry.trim().length < 1) {
//                   setState(() {
//                     countryError = pageDetails["text12"];
//                   });
//                 }
//               } else {
//                 setState(() {
//                   // cart.countries = [];
//                   cart.cities = [];
//                   cart.zipCode = [];
//                   enablecity = false;
//                   enablezipcode = false;
//                   if (widget.isAdd == 'true') {
//                     // if (addressform.currentState.validate()) {
//                     Map<String, dynamic> _map = {
//                       "product_id": 1,
//                       "address_id": 0,
//                       "address_name": addressname.text.trim(),
//                       "address_line1": addressline1.text.trim(),
//                       "address_line2": 'null',
//                       "country": dropDownCountry,
//                       "region": 'null',
//                       "city": district.text.trim(),
//                       "postal_code": postalcode.text.trim(),
//                       "address_phone": phonenumber.text,
//                       "delete": false
//                     };
//                     cart.addAddress(_map, widget.setData);
//                     cart.getAddressInfo(setState);
//                     // cart.getAddressInfo(setState);
//                     Navigator.of(context).pop();
//                     cart.askpopup = true;
//                     widget.notifyParent();
//                     print("mdhgfgdyuisjlknmbhgjdkxc");
//                     print(cart.address);
//                     print(cart.address.length);
//                     if (prefs.getString('reload') == 'individual_profile') {
//                       Navigator.pushReplacement(
//                           widget.shopContext,
//                           MaterialPageRoute(
//                               builder: (context) => Home(
//                                     route: 'individual_profile',
//                                   )));
//                     } else {
//                       if (prefs.getString('reload') == "checkout") {
//                         // cart.addAddress(_map, widget.setData);
//                         cart.myCart(setState, widget.setData);
//                         Navigator.pushReplacement(
//                             widget.shopContext,
//                             MaterialPageRoute(
//                                 builder: (context) => Home(
//                                       route: 'checkout',
//                                     )));
//                       } else {
//                         // if (widget.shopContext != null) {
//                         Navigator.pushReplacement(
//                             widget.shopContext,
//                             MaterialPageRoute(
//                                 builder: (context) => Home(
//                                       route: 'centurion_shop',
//                                       param: 'true',
//                                     )));

// //
//                         // }
//                       }
//                     }
//                     // print(_map);
//                     // print(cart.address);
//                     // }
//                   } else {
//                     // if (addressform.currentState.validate()) {
//                     print('========================');
//                     Map<String, dynamic> _map = {
//                       "product_id": 1,
//                       "address_id": cart.address[widget.index]['address_id'],
//                       "address_name": addressname.text.trim(),
//                       "address_line1": addressline1.text.trim(),
//                       "address_line2": 'null',
//                       "country": dropDownCountry,
//                       "region": 'null',
//                       "city": district.text,
//                       "postal_code": postalcode.text.trim(),
//                       "address_phone": phonenumber.text.trim(),
//                       "delete": false
//                     };
//                     print('.........................');
//                     cart.updateAddress(
//                         _map, cart.address[widget.index], widget.setData);
//                     // cart.getAddressInfo(setState);
//                     Navigator.of(context).pop();
//                     // if (prefs.getString('reload') == 'individual_profile') {
//                     //   Navigator.push(
//                     //       widget.shopContext,
//                     //       MaterialPageRoute(
//                     //           builder: (context) => Home(
//                     //                 route: 'individual_profile',
//                     //               )));
//                     // } else {
//                     //   Navigator.push(
//                     //       context,
//                     //       MaterialPageRoute(
//                     //           builder: (context) => Home(
//                     //                 route: 'checkout',
//                     //               )));
//                     //   // }
//                     // }
//                     widget.notifyParent();
//                   }
//                 });
//               }
            }),
      ],
    );
  }

  submitAddress() async {
    validateAddress();
    setState(() {
      isRequired = true;
    });
    SharedPreferences prefs = await SharedPreferences.getInstance();

    if (addressline1.text.trim().isEmpty ||
        // addressline2.text.isEmpty ||
        addressname.text.trim().isEmpty ||
        phonenumber.text.trim().isEmpty ||
        // state.text.isEmpty ||
        dropDownCountry.trim().isEmpty ||
        district.text.trim().isEmpty ||
        postalcode.text.trim().isEmpty) {
      if (district.text.trim().length < 1) {
        setState(() {
          districtError = pageDetails["text12"];
        });
      }
      if (addressline1.text.trim().length < 1) {
        setState(() {
          addressline1Error = pageDetails["text12"];
        });
      }
      // if (addressline2.text.length < 1) {
      //   setState(() {
      //     addressline2Error = 'Required';
      //   });
      // }
      if (addressname.text.trim().length < 1) {
        setState(() {
          nameError = pageDetails["text12"];
        });
      }
      if (phonenumber.text.trim().length < 1) {
        setState(() {
          phonenumberError = pageDetails["text12"];
        });
      }
      // if (state.text.length < 1) {
      //   setState(() {
      //     stateError = 'Required';
      //   });
      // }
      if (postalcode.text.trim().length < 1) {
        setState(() {
          postalcodeError = pageDetails["text12"];
        });
      }
      if (dropDownCountry.trim().length < 1) {
        setState(() {
          countryError = pageDetails["text12"];
        });
      }
    } else {
      setState(() {
        // cart.countries = [];
        cart.cities = [];
        cart.zipCode = [];
        enablecity = false;
        enablezipcode = false;
        if (widget.isAdd == 'true') {
          // if (addressform.currentState.validate()) {
          Map<String, dynamic> _map = {
            "product_id": 1,
            "address_id": 0,
            "address_name": addressname.text.trim(),
            "address_line1": addressline1.text.trim(),
            "address_line2": 'null',
            "country": dropDownCountry,
            "region": 'null',
            "city": district.text.trim(),
            "postal_code": postalcode.text.trim(),
            "address_phone": phonenumber.text,
            "delete": false
          };
          cart.addAddress(_map, widget.setData);
          cart.getAddressInfo(setState);
          // cart.getAddressInfo(setState);
          Navigator.of(context).pop();
          cart.askpopup = true;
          widget.notifyParent();
          print("mdhgfgdyuisjlknmbhgjdkxc");
          print(cart.address);
          print(cart.address.length);
          if (prefs.getString('reload') == 'individual_profile') {
            Navigator.pushReplacement(
                widget.shopContext,
                MaterialPageRoute(
                    builder: (context) => Home(
                          route: 'individual_profile',
                        )));
          } else {
            if (prefs.getString('reload') == "checkout") {
              // cart.addAddress(_map, widget.setData);
              cart.myCart(setState, widget.setData);
              Navigator.pushReplacement(
                  widget.shopContext,
                  MaterialPageRoute(
                      builder: (context) => Home(
                            route: 'checkout',
                          )));
            } else {
              // if (widget.shopContext != null) {
              Navigator.pushReplacement(
                  widget.shopContext,
                  MaterialPageRoute(
                      builder: (context) => Home(
                            route: 'centurion_shop',
                            param: 'true',
                          )));

//
              // }
            }
          }
          // print(_map);
          // print(cart.address);
          // }
        } else {
          // if (addressform.currentState.validate()) {
          print('========================');
          Map<String, dynamic> _map = {
            "product_id": 1,
            "address_id": cart.address[widget.index]['address_id'],
            "address_name": addressname.text.trim(),
            "address_line1": addressline1.text.trim(),
            "address_line2": 'null',
            "country": dropDownCountry,
            "region": 'null',
            "city": district.text,
            "postal_code": postalcode.text.trim(),
            "address_phone": phonenumber.text.trim(),
            "delete": false
          };
          print('.........................');
          cart.updateAddress(_map, cart.address[widget.index], widget.setData);
          // cart.getAddressInfo(setState);
          Navigator.of(context).pop();
          // if (prefs.getString('reload') == 'individual_profile') {
          //   Navigator.push(
          //       widget.shopContext,
          //       MaterialPageRoute(
          //           builder: (context) => Home(
          //                 route: 'individual_profile',
          //               )));
          // } else {
          //   Navigator.push(
          //       context,
          //       MaterialPageRoute(
          //           builder: (context) => Home(
          //                 route: 'checkout',
          //               )));
          //   // }
          // }
          widget.notifyParent();
        }
      });
    }
  }
}

class Waiting {
  waitpopup(context) {
    showDialog(
        barrierDismissible: false,
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            elevation: 24.0,
            backgroundColor: Pallet.popupcontainerback,
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(15.0))),
            title: Container(
              width: 50,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  CircularProgressIndicator(
                      valueColor: AlwaysStoppedAnimation<Color>(Colors.white)),
                  SizedBox(
                    height: 10,
                  ),
                  Text("Please Wait...",
                      style: TextStyle(
                          fontSize: Pallet.heading3,
                          color: Pallet.fontcolor,
                          fontWeight: Pallet.font500)),
                ],
              ),
            ),
          );
        });
  }
}

class Snack {
  snack({@required String title}) {
    Get.snackbar('', '',
        maxWidth: 600,
        margin: EdgeInsets.only(top: 10),
        boxShadows: [Pallet.shadowEffect],
        progressIndicatorBackgroundColor: Pallet.fontcolornew,
        titleText: Text(
          title,
          textAlign: TextAlign.center,
          style: TextStyle(
            color: Pallet.fontcolor,
          ),
        ),
        backgroundColor: Pallet.snackback,
        snackPosition: SnackPosition.TOP);
  }
}

class NullRemover {
  String nullremove(
      {@required dynamic text,
      @required String ifnullreturnval,
      bool enableemptyvalcheck}) {
    if (text.toString() == null) {
      return ifnullreturnval;
    } else if (text.toString() == 'null') {
      return ifnullreturnval;
    } else {
      if (enableemptyvalcheck == true) {
        if (text.toString() == '') {
          return ifnullreturnval;
        }
      } else {
        return text.toString();
      }
      return text.toString();
    }
  }
}

class Custompopup {
  showdialog({
    @required BuildContext context,
    String title,
    Widget content,
    List<Widget> actions,
    Color backgroundcolor,
    Widget customtitle,
    Color barrierColor,
    bool titlelogo,
    bool scrollable,
    double popupwidth,
    EdgeInsetsGeometry titlepadding,
    bool barrierDismissible,
    double elevation,
  }) {
    return showDialog(
      context: context,
      barrierColor: barrierColor == null ? null : barrierColor,
      barrierDismissible:
          barrierDismissible == null ? false : barrierDismissible,
      builder: (BuildContext context) {
        return AlertDialog(
          elevation: elevation == null ? 24.0 : elevation,
          scrollable: scrollable == null ? false : scrollable,
          backgroundColor: backgroundcolor == null
              ? Pallet.popupcontainerback
              : backgroundcolor,
          titlePadding:
              titlepadding == null ? EdgeInsets.all(24) : titlepadding,
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(15.0))),
          title: customtitle != null
              ? customtitle
              : Row(
                  children: [
                    if (titlelogo == null || titlelogo == true)
                      Image.asset("c-logo.png", width: 40),
                    if (titlelogo == null || titlelogo == true)
                      SizedBox(width: 10),
                    Expanded(
                      child: MyText(
                          text: title,
                          maxLines: 2,
                          style: TextStyle(
                              fontSize: Pallet.heading4,
                              color: Pallet.fontcolor,
                              fontWeight: Pallet.font500)),
                    ),
                  ],
                ),
          content: Container(
            width: popupwidth == null ? 455.0 : popupwidth,
            child: content == null ? Container() : content,
          ),
          actions: actions == null ? [] : actions,
        );
      },
    );
  }
}

class Maintainance extends StatelessWidget {
  const Maintainance({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        // height: 40,
        // padding: EdgeInsets.symmetric(vertical: 5, horizontal: 8),
        // color: Colors.red,
        // child: Text(
        //   'For the kind attention of all our valued partners, there will be a Scheduled Maintenance of our Back office during which the system will not be available on the following times, starting from the 06th of Aug 08:30 p.m. to 07th of Aug 01:30 a.m. Singapore time (SGT).',
        //   style: TextStyle(
        //     color: Colors.white,
        //   ),
        // ),
        );
  }
}

Custompopup custompopup = Custompopup();
NullRemover nullremover = NullRemover();
Snack snack = Snack();
Waiting waiting = new Waiting();
