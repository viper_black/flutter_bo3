import 'dart:async';
import 'package:blinking_text/blinking_text.dart';
// import 'package:flutter_animated_button/flutter_animated_button.dart';
import 'dart:math';
import 'package:centurion/screens/2fa.dart';
import 'package:centurion/screens/cataegory.dart';
import 'package:centurion/screens/help.dart';
import 'package:centurion/screens/kyc_page.dart';
import 'package:centurion/screens/myorders.dart';
import 'package:confetti/confetti.dart';
// import 'package:centurion/screens/TreeViewPage.dart';
import 'lottery/all_lottery_winners.dart';
import 'lottery/common.dart';
import 'package:centurion/screens/news.dart';
import 'package:centurion/screens/payout.dart';
import 'package:centurion/screens/profile_page.dart';
import 'package:centurion/screens/settings.dart';
import 'package:centurion/screens/invite_member.dart';
import 'package:centurion/services/business/cart.dart';
import 'lottery/winners_announcement.dart';
import 'package:centurion/utils/utils.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'common.dart';
import 'config.dart';
import 'config/app_settings.dart';
import 'login.dart';
import 'screens/commissions.dart';
import 'screens/corporatekyc.dart';
import 'screens/TreeViewPage.dart';
import 'screens/individualprofile.dart';
import 'screens/kyc.dart';
import 'screens/dashboard.dart';
import 'package:badges/badges.dart';
import 'screens/cart.dart';
import 'screens/cataegory.dart';
import 'screens/bonus.dart';
import 'screens/settings.dart';
import 'screens/accounts.dart';
import 'screens/add_funding.dart';
import 'screens/get_dsv.dart';
import 'screens/document.dart';
import 'lottery/lottery.dart';
import 'lottery/lotteryHistory.dart';
import 'screens/checkout.dart';
import 'services/business/account.dart';
import 'animations/drop.dart';
import 'services/communication/index.dart' show HttpRequest;

// ignore: must_be_immutable
class Home extends StatefulWidget {
  final bool twofapopup;
  Home({
    Key key,
    this.bankBool,

    //  this.newshow,
    this.route,
    this.bottomsheet,
    this.param,
    this.intialPage,
    this.sessionstart,
    this.twofapopup,
    this.amt,
    this.manageaddressonly,
    // this.mynetworktreeinvite,
  }) : super(key: key);
  bool bottomsheet;
  // String mynetworktreeinvite;
  String param;
  int intialPage;
  bool bankBool;

  // bool newshow;
  String amt;
  bool sessionstart;
  bool manageaddressonly;
  String route;

  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  ConfettiController lotteryConfettiController = ConfettiController();

  GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey();
  List navigations = [];
  String countryError;
  String dropDownCountry = '';
  bool isMobile = false;
  bool isNetworkBack = false;
  FocusNode countryfocus = FocusNode();
  Map<String, dynamic> setAddressMap = {
    'product_id': 1,
    'address_id': 'String',
  };
  Map<String, dynamic> pageDetails = {};
  List navigationsNetWork = <Map>[
    {
      "nav_name": "Dashboard",
      "image": Image.asset(
        "dashboard.png",
        width: 20.0,
      ),
      "route": "dashboard"
    },
    // {
    //   "nav_name": "Invite Member",
    //   "image": Image.asset(
    //    "invite_member.png",
    //     width: 25.0,
    //   ),
    //   "route": "invite_member"
    // },
    // {"nav_name": "Customer Dashboard", "icon": Icons.dashboard},
    {
      "nav_name": "Add Funding",
      "image": Image.asset(
        "digitalasset.png",
        width: 20.0,
      ),
      "route": "add_funding"
    },
    {
      "nav_name": "Buy DSV",
      "image": Image.asset(
        "DSV.png",
        width: 20.0,
      ),
      "route": "buy_dsv"
    },
    {
      "nav_name": "Centurion Shop",
      "image": Image.asset(
        "centurion_shop.png",
        width: 20.0,
      ),
      "route": "centurion_shop"
    },
    {
      "nav_name": "My Shop Orders",
      "image": Image.asset(
        "myorders.png",
        width: 20.0,
      ),
      "route": "my_orders"
    },
    {
      "nav_name": "My Accounts",
      "image": Image.asset(
        "account.png",
        width: 20.0,
      ),
      "route": "my_accounts"
    },
    // {
    //   "nav_name": "Reports",
    //   "image": Image.asset(
    //     "account.png",
    //     width: 20.0,
    //   ),
    //   "route": "commission_report"
    // },

    // {
    //   "nav_name": "Lottry History",
    //   "image": Image.asset(
    //     "account.png",
    //     width: 20.0,
    //   ),
    //   "route": "lottery_history"
    // },
    //  {
    //   "nav_name": "My Bonuses",
    //   "image": Image.asset(
    //     "bonus.png",
    //     width: 20.0,
    //   ),
    //   "route": "my_bonuses"
    // },
    // {
    //   "nav_name": "Request Payout",
    //   "image": Image.asset(
    //     "payout.png",
    //     width: 20.0,
    //   ),
    //   "route": "request_payout"
    // },
    {
      "nav_name": "My Office",
      "image": Image.asset(
        "documentation.png",
        width: 20.0,
      ),
      "route": "my_office"
    },
    // {
    //   "nav_name": "KYC",
    //   "image": Image.asset(
    //     "documentation.png",
    //     width: 20.0,
    //   ),
    //   "route": "kyc"
    // },
    {
      "nav_name": "My News",
      "image": Image.asset(
        "newspaper.png",
        width: 20.0,
      ),
      "route": "my_news"
    },
    {
      "nav_name": "My Settings",
      "image": Image.asset(
        "settings.png",
        width: 20.0,
      ),
      "route": "my_settings"
    },
    {
      "nav_name": "Help",
      "image": Image.asset(
        "help.png",
        width: 20.0,
      ),
      "route": "help"
    },
    {
      "nav_name": "Logout",
      "image": Image.asset(
        "logout.png",
        width: 20.0,
      ),
      "route": ""
    },
    // {
    //   "nav_name": "DSV-P",
    //   "image": Image.asset(
    //     "DSV.png",
    //     width: 20.0,
    //   ),
    //   "route": "dsv_p"
    // },

    // {
    //   "nav_name": "ApplyCCT",
    //   "image": Image.asset(
    //     "CCT.png",
    //     width: 20.0,
    //   ),
    //   "route": "applycct"
    // },
  ];
  List navigationsCustomer = <Map>[
    {
      "nav_name": "Dashboard",
      "image": Image.asset(
        "dashboard.png",
        width: 20.0,
      ),
      "route": "dashboard"
    },
    {
      "nav_name": "Invite Member",
      "image": Image.asset(
        "invite_member.png",
        width: 25.0,
      ),
      "route": "invite_member"
    },
    {
      "nav_name": "Add Funding",
      "image": Image.asset(
        "digitalasset.png",
        width: 20.0,
      ),
      "route": "add_funding"
    },
    {
      "nav_name": "Buy DSV",
      "image": Image.asset(
        "DSV.png",
        width: 20.0,
      ),
      "route": "buy_dsv"
    },
    {
      "nav_name": "Centurion Shop",
      "image": Image.asset(
        "centurion_shop.png",
        width: 20.0,
      ),
      "route": "centurion_shop"
    },
    {
      "nav_name": "My Network",
      "image": Image.asset(
        "mynetwork.png",
        width: 25.0,
      ),
      "route": "my_network"
    },
    // {
    //   "nav_name": "Lottry History",
    //   "image": Image.asset(
    //     "account.png",
    //     width: 20.0,
    //   ),
    //   "route": "lottery_history"
    // },
    {
      "nav_name": "My Accounts",
      "image": Image.asset(
        "account.png",
        width: 20.0,
      ),
      "route": "my_accounts"
    },
    {
      "nav_name": "My Reports",
      "image": Image.asset(
        "account.png",
        width: 20.0,
      ),
      "route": "commission_report"
    },
    {
      "nav_name": "My Bonuses",
      "image": Image.asset(
        "bonus.png",
        width: 20.0,
      ),
      "route": "my_bonuses"
    },
    {
      "nav_name": "My Shop Orders",
      "image": Image.asset(
        "myorders.png",
        width: 20.0,
      ),
      "route": "my_orders"
    },
    {
      "nav_name": "My Office",
      "image": Image.asset(
        "documentation.png",
        width: 20.0,
      ),
      "route": "my_office"
    },
    {
      "nav_name": "My Settings",
      "image": Image.asset(
        "settings.png",
        width: 20.0,
      ),
      "route": "my_settings"
    },
    // {
    //   "nav_name": "KYC",
    //   "image": Image.asset(
    //     "documentation.png",
    //     width: 20.0,
    //   ),
    //   "route": "kyc"
    // },
    {
      "nav_name": "My News",
      "image": Image.asset(
        "newspaper.png",
        width: 20.0,
      ),
      "route": "my_news"
    },
    {
      "nav_name": "Request Payout",
      "image": Image.asset(
        "payout.png",
        width: 20.0,
      ),
      "route": "request_payout"
    },
    {
      "nav_name": "Help",
      "image": Image.asset(
        "help.png",
        width: 20.0,
      ),
      "route": "help"
    },
    {
      "nav_name": "Logout",
      "image": Image.asset(
        "logout.png",
        width: 20.0,
      ),
      "route": ""
    },
    // {
    //   "nav_name": "ApplyCCT",
    //   "image": Image.asset(
    //     "CCT.png",
    //     width: 20.0,
    //   ),
    //   "route": "applycct"
    // },
  ];
  Map<String, dynamic> getRegionsMap = {
    'system_product_id': 1,
    'region_level': 2,
    'parent_region': 'null'
  };

  String mynetworkinvit;
  var cancel = true;
  Future<String> temp;

  reload() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    if (widget.route == 'lottery') {
    } else {
      prefs.setString("reload", widget.route);
      String stringValue = prefs.getString('reload');
      print(stringValue);
      print("sssssssssssss");
    }
  }

  redirectlogin() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.remove("shopbannershow");
    prefs.remove("newshow");

    prefs.remove("reload");
    prefs.remove("sessiontime");
    Navigator.pushReplacement(
      context,
      MaterialPageRoute(builder: (context) => Login()),
    );
  }

  sessionexpire() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    final now = DateTime.now();
    DateTime endTime =
        DateTime(now.year, now.month, now.day, now.hour, now.minute);

    if (widget.sessionstart == true) {
      widget.sessionstart = false;
      print("zzzzzzzzzzzzzzaaaaaaaaaaaaaaaaaa");
      if (prefs.getString('sessiontime') != null) {
        if (prefs.getString('sessiontime') != null) {
          String storedtime = prefs.getString('sessiontime');
          var startTime = DateTime.parse(storedtime);
          print(startTime);
          print("xxxxxxxxxxxxxxxxxxxxxx");
          var timerval = endTime.difference(startTime).inMinutes;
          print(timerval);
          Timer(
            Duration(minutes: 30 - timerval),
            redirectlogin,
          );
        } else {
          print("hvhvhvhvhkv,hkv");
          Timer(
            Duration(minutes: 30),
            redirectlogin,
          );
        }
      } else {
        print("hvhvhvhvhkv,hkv");
        Timer(
          Duration(minutes: 30),
          redirectlogin,
        );
      }
    } else {
      Timer(
        Duration(minutes: 30),
        redirectlogin,
      );
    }
  }

  @override
  void initState() {
    super.initState();
    lotteryConfettiController.play();
    sessionexpire();
    temp = settingskycdata.getKycStatus();
    print('TTTEEEE');
    print(temp);
    cart.getAddressInfo(setState);
    // cart.getRegions(getRegionsMap);
    setSlide();
    WidgetsBinding.instance.addPostFrameCallback((_) => test());
    setPageDetails();
    cart.myCart(setState, setState);
  }

  void setPageDetails() async {
    String data = await Utils.getPageDetails('home');
    setState(() {
      pageDetails = Utils.fromJSONString(data);

      navigationsNetWork = <Map>[
        {
          "nav_name": pageDetails['text1'],
          "image": Image.asset(
            "dashboard.png",
            width: 20.0,
          ),
          "route": "dashboard"
        },
        // {
        //   "nav_name": "Invite Member",
        //   "image": Image.asset(
        //    "invite_member.png",
        //     width: 25.0,
        //   ),
        //   "route": "invite_member"
        // },
        // {"nav_name": "Customer Dashboard", "icon": Icons.dashboard},
        {
          "nav_name": pageDetails['text2'],
          "image": Image.asset(
            "digitalasset.png",
            width: 20.0,
          ),
          "route": "add_funding"
        },
        {
          "nav_name": pageDetails['text3'],
          "image": Image.asset(
            "DSV.png",
            width: 20.0,
          ),
          "route": "buy_dsv"
        },
        {
          "nav_name": pageDetails['text13'],
          "image": Image.asset(
            "centurion_shop.png",
            width: 20.0,
          ),
          "route": "centurion_shop"
        },
        {
          "nav_name": pageDetails['text4'],
          "image": Image.asset(
            "account.png",
            width: 20.0,
          ),
          "route": "my_accounts"
        },
        // {
        //   "nav_name": "Reports",
        //   "image": Image.asset(
        //     "account.png",
        //     width: 20.0,
        //   ),
        //   "route": "commission_report"
        // },
        // {
        //   "nav_name": "Lottry History",
        //   "image": Image.asset(
        //     "account.png",
        //     width: 20.0,
        //   ),
        //   "route": "lottery_history"
        // },
        {
          "nav_name": pageDetails['text15'],
          "image": Image.asset(
            "myorders.png",
            width: 20.0,
          ),
          "route": "my_orders"
        },

        //  {
        //   "nav_name": "My Bonuses",
        //   "image": Image.asset(
        //     "bonus.png",
        //     width: 20.0,
        //   ),
        //   "route": "my_bonuses"
        // },
        // {
        //   "nav_name": "Request Payout",
        //   "image": Image.asset(
        //     "payout.png",
        //     width: 20.0,
        //   ),
        //   "route": "request_payout"
        // },
        {
          "nav_name": pageDetails['text6'],
          "image": Image.asset(
            "documentation.png",
            width: 20.0,
          ),
          "route": "my_office"
        },
        {
          "nav_name": pageDetails['text5'],
          "image": Image.asset(
            "settings.png",
            width: 20.0,
          ),
          "route": "my_settings"
        },
        {
          "nav_name": pageDetails['text8'],
          "image": Image.asset(
            "newspaper.png",
            width: 20.0,
          ),
          "route": "my_news"
        },
        {
          "nav_name": pageDetails['text7'],
          "image": Image.asset(
            "help.png",
            width: 20.0,
          ),
          "route": "help"
        },
        {
          "nav_name": pageDetails['text9'],
          "image": Image.asset(
            "logout.png",
            width: 20.0,
          ),
          "route": ""
        },
        // {
        //   "nav_name": "ApplyCCT",
        //   "image": Image.asset(
        //     "CCT.png",
        //     width: 20.0,
        //   ),
        //   "route": "applycct"
        // },
      ];
      navigationsCustomer = [
        {
          "nav_name": pageDetails['text1'],
          "image": Image.asset(
            "dashboard.png",
            width: 20.0,
          ),
          "route": "dashboard"
        },
        {
          "nav_name": pageDetails['text12'],
          "image": Image.asset(
            "invite_member.png",
            width: 25.0,
          ),
          "route": "invite_member"
        },
        {
          "nav_name": pageDetails['text2'],
          "image": Image.asset(
            "digitalasset.png",
            width: 20.0,
          ),
          "route": "add_funding"
        },

        {
          "nav_name": pageDetails['text3'],
          "image": Image.asset(
            "DSV.png",
            width: 20.0,
          ),
          "route": "buy_dsv"
        },
        {
          "nav_name": pageDetails['text13'],
          "image": Image.asset(
            "centurion_shop.png",
            width: 20.0,
          ),
          "route": "centurion_shop"
        },
        {
          "nav_name": pageDetails['text22'],
          "image": Image.asset(
            "mynetwork.png",
            width: 25.0,
          ),
          "route": "my_network"
        },
        // {
        //   "nav_name": "Lottry History",
        //   "image": Image.asset(
        //     "account.png",
        //     width: 20.0,
        //   ),
        //   "route": "lottery_history"
        // },
        {
          "nav_name": pageDetails['text4'],
          "image": Image.asset(
            "account.png",
            width: 20.0,
          ),
          "route": "my_accounts"
        },
        {
          "nav_name": "My Reports",
          "image": Image.asset(
            "account.png",
            width: 20.0,
          ),
          "route": "commission_report"
        },
        {
          "nav_name": pageDetails['text10'],
          "image": Image.asset(
            "bonus.png",
            width: 20.0,
          ),
          "route": "my_bonuses"
        },

        {
          "nav_name": pageDetails['text15'],
          "image": Image.asset(
            "myorders.png",
            width: 20.0,
          ),
          "route": "my_orders"
        },
        {
          "nav_name": pageDetails['text6'],
          "image": Image.asset(
            "documentation.png",
            width: 20.0,
          ),
          "route": "my_office"
        },
        {
          "nav_name": pageDetails['text5'],
          "image": Image.asset(
            "settings.png",
            width: 20.0,
          ),
          "route": "my_settings"
        },
        {
          "nav_name": pageDetails['text8'],
          "image": Image.asset(
            "newspaper.png",
            width: 20.0,
          ),
          "route": "my_news"
        },
        {
          "nav_name": pageDetails['text11'],
          "image": Image.asset(
            "payout.png",
            width: 20.0,
          ),
          "route": "request_payout"
        },

        {
          "nav_name": pageDetails['text7'],
          "image": Image.asset(
            "help.png",
            width: 20.0,
          ),
          "route": "help"
        },
        {
          "nav_name": pageDetails['text9'],
          "image": Image.asset(
            "logout.png",
            width: 20.0,
          ),
          "route": ""
        },
        // {
        //   "nav_name": "ApplyCCT",
        //   "image": Image.asset(
        //     "CCT.png",
        //     width: 20.0,
        //   ),
        //   "route": "applycct"
        // },
      ];
    });
    setSlide();
  }

  setSlide() async {
    var accountType = int.parse(Utils.fromJSONString(
            await Utils.getSharedPrefereces('data'))['account_type']
        .toString());
    setState(() {
      navigations = accountType == 1 ? navigationsNetWork : navigationsCustomer;
    });
  }

  static String option = 'red';

  test() {
    if (widget.bottomsheet) {
      // ignore: unused_local_variable
      var bottomsheet = showModalBottomSheet(
          context: context,
          builder: (BuildContext bc) {
            return Container(
                height: 40,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    ClipRRect(
                      borderRadius: BorderRadius.circular(5),
                      // ignore: deprecated_member_use
                      child: RaisedButton(
                        child: Text(pageDetails['text21']),
                        textColor: Pallet.applybuttontxtclr,
                        color: Pallet.applybuttonclr,
                        onPressed: () {
                          Navigator.of(context).pop();
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => Home(
                                        route: 'dashboard',
                                      )));
                        },
                      ),
                    ),
                    SizedBox(width: 15.0),
                    ClipRRect(
                      borderRadius: BorderRadius.circular(5),
                      // ignore: deprecated_member_use
                      child: RaisedButton(
                        child: Text(pageDetails['text20']),
                        color: Pallet.cancelbuttonclr,
                        textColor: Pallet.cancelbtntxtclr,
                        onPressed: () {
                          setState(() {
                            Pallet.changetheme(Pallet.previous, context);
                            Navigator.of(context).pop();
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) =>
                                        Home(route: 'documentation')));
                          });
                        },
                      ),
                    )
                  ],
                ));
          });
    }
  }

  test2() {
    Pallet.changetheme(option, context);
  }

  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;

    return LayoutBuilder(builder: (context, constraints) {
      if (constraints.maxWidth < ScreenSize.verysmall) {
        isMobile = true;
        isNetworkBack = true;
        return Scaffold(
            drawer: Drawer(
              child: sidebar(wdgtRadius: 0, isphone: true),
            ),
            appBar: topbar(true),
            body: Scaffold(
              backgroundColor: Pallet.inner2,
              key: _scaffoldKey,
              endDrawer: Newcart(
                  wdgtWidth: ScreenSize.iphone,
                  wdgtHeight: size.height,
                  setData: setState,
                  wdgtRadius: 0),
              body: main(wdgtWidth: size.width, wdgtHeight: size.height),
              // bottomNavigationBar:
              //     footer(size.width * 0.92 - 135, size.height * 0.12),
            ));
      } else if (constraints.maxWidth < ScreenSize.iphone) {
        isMobile = true;
        isNetworkBack = true;
        return Scaffold(
            drawer: Drawer(
              child: sidebar(wdgtRadius: 0, isphone: true),
            ),
            appBar: topbar(true),
            body: Scaffold(
              backgroundColor: Pallet.inner2,
              key: _scaffoldKey,
              endDrawer: Newcart(
                  wdgtWidth: ScreenSize.iphone,
                  wdgtHeight: size.height,
                  setData: setState,
                  wdgtRadius: 0),
              body: main(wdgtWidth: size.width, wdgtHeight: size.height),
              // bottomNavigationBar:
              //     footer(size.width * 0.92 - 135, size.height * 0.1),
            ));
      } else if (constraints.maxWidth > ScreenSize.iphone &&
          constraints.maxWidth < ScreenSize.ipad) {
        isNetworkBack = true;
        return Scaffold(
            drawer: Drawer(
              child: sidebar(wdgtRadius: 0, isphone: true),
            ),
            appBar: topbar(true),
            body: Scaffold(
              backgroundColor: Pallet.inner2,
              key: _scaffoldKey,
              endDrawer: Newcart(
                  wdgtWidth: ScreenSize.iphone,
                  wdgtHeight: size.height,
                  setData: setState,
                  wdgtRadius: 0),
              body: main(wdgtWidth: size.width, wdgtHeight: size.height),
              // bottomNavigationBar:
              //     footer(size.width * 0.92 - 135, size.height * 0.099),
            ));
      } else if (constraints.maxWidth > ScreenSize.ipad &&
          constraints.maxWidth < ScreenSize.tab) {
        isNetworkBack = true;
        return Scaffold(
            drawer: Drawer(
              child: sidebar(wdgtRadius: 0, isphone: true),
            ),
            appBar: topbar(true),
            body: Scaffold(
              backgroundColor: Pallet.inner2,
              key: _scaffoldKey,
              endDrawer: Newcart(
                  wdgtWidth: ScreenSize.iphone,
                  wdgtHeight: size.height,
                  setData: setState,
                  wdgtRadius: 0),
              body: main(wdgtWidth: size.width, wdgtHeight: size.height),
              // bottomNavigationBar:
              //     footer(size.width * 0.92 - 135, size.height * 0.05),
            ));
      } else {
        return Scaffold(
            backgroundColor: Pallet.inner2,
            body: Row(
              children: [
                sidebar(
                    wdgtWidth: size.width * 0.08 + 135,
                    wdgtHeight: size.height,
                    wdgtRadius: 35,
                    isphone: false),
                Container(
                    width: size.width * 0.92 - 135,
                    child: Scaffold(
                      appBar: topbar(false),
                      body: Scaffold(
                        backgroundColor: Pallet.inner2,
                        key: _scaffoldKey,
                        endDrawer: Newcart(
                          wdgtWidth: 500,
                          wdgtHeight: size.height,
                          wdgtRadius: 10,
                          setData: setState,
                        ),
                        body: main(
                            wdgtWidth: size.width * 0.92 - 135,
                            wdgtHeight: size.height),
                      ),
                      // bottomNavigationBar:
                      //     footer(size.width * 0.92 - 135, size.height * 0.05),
                    ))
              ],
            ));
      }
    });
  }

//0.Dashbaorad | 1.CustomerDashbaorad | 2.AddFund | 3.Dsv | 4.Accounts | 5.Bonus | 6.Settings | 7.Documents | 8.Payout
//  switch(dashboard.accountType ) {
//    case 4: {
//       // statements;
//    }
//    break;

//    case 5: {
//       //statements;
//    }
//    break;

//    default: {
//       //statements;
//    }
//    break;
// }

  screen({double wdgtWidth, wdgtHeight}) {
    reload();
    if (widget.route == 'dashboard')
      return Dashboard(
        // newshow: widget.newshow,
        twofapopup: widget.twofapopup,
        //kycstatus: widget.kycststus,
        wdgtWidth: wdgtWidth,
        wdgtHeight: wdgtHeight,
      );
    else if (widget.route == 'kyc')
      return KYC(
        wdgtWidth: wdgtWidth,
        wdgtHeight: wdgtHeight,
      );
    else if (widget.route == 'invite_member')
      return Invitemember(
        // mynetworkinviter: mynetworkinvit,
        wdgtWidth: wdgtWidth,
        wdgtHeight: wdgtHeight,
      );
    else if (widget.route == 'individual_profile')
      return IndividualProfile(
        wdgtWidth: wdgtWidth,
        wdgtHeight: wdgtHeight,
        bank: widget.bankBool,
        manageaddressonly: widget.manageaddressonly,
        setData: setState,
      );
    else if (widget.route == 'my_network')
      return TreeViewPage(
        wdgtWidth: wdgtWidth,
        wdgtHeight: wdgtHeight,
      );
    else if (widget.route == 'add_funding')
      return AddFund(
        wdgtWidth: wdgtWidth,
        wdgtHeight: wdgtHeight,
      );
    else if (widget.route == 'lottery')
      return RedeemLottery(
        wdgtWidth: wdgtWidth,
        wdgtHeight: wdgtHeight,
        amt: widget.amt,
      );
    else if (widget.route == 'checkout')
      return CheckoutPage(
        wdgtWidth: wdgtWidth,
        wdgtHeight: wdgtHeight,
        setData: setState,
      );
    else if (widget.route == 'winner_Announcement')
      return WinnerAnnouncement(
        wdgtWidth: wdgtWidth,
        wdgtHeight: wdgtHeight,
      );
    else if (widget.route == 'buy_dsv')
      return Dsv(wdgtWidth: wdgtWidth, wdgtHeight: wdgtHeight);
    // else if (widget.route == 'category_page')
    //   return Products(
    //     wdgtWidth: wdgtWidth,
    //     wdgtHeight: wdgtHeight,
    //     storeId: widget.param,
    //     setData: setState,
    //   );
    else if (widget.route == 'my_accounts')
      return Accounts(
          wdgtWidth: wdgtWidth,
          wdgtHeight: wdgtHeight,
          assetType: widget.param,
          intialPage: widget.intialPage);
    else if (widget.route == 'centurion_shop')
      return Categories(
        wdgtWidth: wdgtWidth,
        wdgtHeight: wdgtHeight,
        setData: setState,
        goToShop: widget.param,
      );
    else if (widget.route == 'my_bonuses')
      return Bonus(
        wdgtWidth: wdgtWidth,
        wdgtHeight: wdgtHeight,
      );
    else if (widget.route == 'request_payout')
      return Payout(wdgtWidth: wdgtWidth, wdgtHeight: wdgtHeight);
    else if (widget.route == 'my_news')
      return News(wdgtWidth: wdgtWidth, wdgtHeight: wdgtHeight);
    else if (widget.route == 'corporate_kyc')
      return CorporateKyc(wdgtWidth: wdgtWidth, wdgtHeight: wdgtHeight);
    else if (widget.route == 'my_settings')
      return Settings(wdgtWidth: wdgtWidth, wdgtHeight: wdgtHeight);
    else if (widget.route == 'help')
      return Help(wdgtWidth: wdgtWidth, wdgtHeight: wdgtHeight);
    else if (widget.route == 'my_office')
      return Documents(wdgtWidth: wdgtWidth, wdgtHeight: wdgtHeight);
    else if (widget.route == 'my_orders')
      return MyOrders(wdgtWidth: wdgtWidth, wdgtHeight: wdgtHeight);
    else if (widget.route == 'commission_report')
      return Commissions(wdgtWidth: wdgtWidth, wdgtHeight: wdgtHeight);
    // else if (widget.route == 'applycct')
    //   return ApplyCCT(wdgtWidth: wdgtWidth, wdgtHeight: wdgtHeight);

    // else if (widget.activeLink == 11)
    //   return Help(wdgtWidth: wdgtWidth, wdgtHeight: wdgtHeight);
    // else if (widget.activeLink == 12)
    //   return News(wdgtWidth: wdgtWidth, wdgtHeight: wdgtHeight);
    else if (widget.route == 'two_factor_auth')
      return TwoFactorAuth(
          wdgtWidth: wdgtWidth, wdgtHeight: wdgtHeight, code: widget.param);
    else if (widget.route == 'account_pwd2')
      return AccountPwd2(wdgtWidth: wdgtWidth, wdgtHeight: wdgtHeight);
    else if (widget.route == 'lottery_history')
      return LotteryHistory(wdgtWidth: wdgtWidth, wdgtHeight: wdgtHeight);
    else if (widget.route == 'all_lottery_history')
      return AllLotteryHistory(wdgtWidth: wdgtWidth, wdgtHeight: wdgtHeight);
    // else if (widget.route == 'product_details')
    //   return NewProducts(
    //     wdgtWidth: wdgtWidth,
    //     wdgtHeight: wdgtHeight,
    //     productId: int.parse(widget.param),
    //     setData: setState,
    //   );
    else if (widget.route == 'profile')
      return Profile(wdgtWidth: wdgtWidth, wdgtHeight: wdgtHeight);
    else if (widget.route == 'kyc_page')
      return KycPage(wdgtWidth: wdgtWidth, wdgtHeight: wdgtHeight);
  }

  Widget main({double wdgtWidth, double wdgtHeight}) {
    return LayoutBuilder(builder: (context, constraints) {
      if (constraints.maxWidth < ScreenSize.iphone) {
        return screen(wdgtWidth: wdgtWidth, wdgtHeight: wdgtHeight);
// } else if (constraints.maxWidth > ScreenSize.iphone &&
// constraints.maxWidth < ScreenSize.tab) {
// return Container(
// height: wdgtHeight,
// width: wdgtWidth,
// child: screen(wdgtWidth: wdgtWidth, wdgtHeight: wdgtHeight),
// );
      } else {
        return screen(wdgtWidth: wdgtWidth - 20, wdgtHeight: wdgtHeight);
      }
    });
  }

  // int counter = 0;
  // Cart cart = Cart();
  Widget topbar(bool showMenu) {
    int counter = cart.totalCount.toInt();
    return AppBar(
        // leading: isNetworkBack == true
        //     ? widget.route == 'my_network'
        //         ? IconButton(
        //             icon: Icon(Icons.arrow_back),
        //             onPressed: () {
        //               Navigator.pushReplacement(
        //                   context,
        //                   MaterialPageRoute(
        //                       builder: (context) => Home(
        //                             route: 'dashboard',
        //                           )));
        //             })
        //         : null
        //     : null,
        automaticallyImplyLeading: showMenu,
        backgroundColor: Pallet.inner2,
        iconTheme: IconThemeData(
          color: Colors.black,
          //change your color here
        ),
        elevation: 0,
        actions: [
          Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              widget.route == 'centurion_shop'
                  ? Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Container(
                          // constraints: (),s
                          // color: Colors.pink,
                          width: isMobile == true ? 150 : 200,
                          // height: 40,

                          child: TypeAheadFormField(
                            hideKeyboard: true,

                            // readonly:false,
                            prefixicon: Icon(
                              Icons.location_on,
                              color: Pallet.fontcolornew,
                              size: 18,
                            ),
                            // ignore: missing_return
                            validator: (val) {
                              if (val.isEmpty) {}
                            },
                            // errortext: postalcodeError,
                            textFieldConfiguration: TextFieldConfiguration(
                                overflow: TextOverflow.ellipsis,
                                // maxLength: 5,
                                // maxLengthEnforced: true,
                                style: TextStyle(
                                    color: Pallet.fontcolornew,
                                    fontSize: Pallet.heading3,
                                    fontWeight: Pallet.font500),
                                controller: cart.country,
                                decoration: InputDecoration(
                                  // labelText: 'City',
                                  border: OutlineInputBorder(
                                      borderSide: const BorderSide(
                                          color: Color(0XFF1034a6), width: 1.0),
                                      borderRadius: BorderRadius.circular(8.0)),
                                  errorBorder: OutlineInputBorder(
                                      borderSide: BorderSide(
                                          color: Colors.red, width: 2),
                                      borderRadius:
                                          BorderRadius.circular(Pallet.radius)),
                                  focusedErrorBorder: OutlineInputBorder(
                                      borderSide: BorderSide(
                                          color: Colors.red, width: 2),
                                      borderRadius:
                                          BorderRadius.circular(Pallet.radius)),
                                  focusedBorder: OutlineInputBorder(
                                      borderSide: const BorderSide(
                                          color: Color(0XFF1034a6), width: 1.0),
                                      borderRadius: BorderRadius.circular(8.0)),
                                  enabledBorder: OutlineInputBorder(
                                      borderSide: BorderSide(
                                          color: Color(0xFF4570ae), width: 0.0),
                                      borderRadius: BorderRadius.circular(8.0)),
                                )),
                            suggestionsCallback: (pattern) {
                              print('AAAAAAAAA');
                              print(cart.addressNames);
                              return cart.addressNames;
                            },

                            itemBuilder: (context, dynamic suggestion) {
                              return ListTile(
                                title: Text(
                                  suggestion,
                                  overflow: TextOverflow.ellipsis,
                                  // minLines: 1,
                                  style: TextStyle(color: Pallet.fontcolornew),
                                ),
                              );
                            },
                            transitionBuilder:
                                (context, suggestionsBox, controller) {
                              return suggestionsBox;
                            },
                            onSuggestionSelected: (suggestion) {
                              if (suggestion == '+ Add New Address') {
                                showDialog(
                                    barrierDismissible: false,
                                    context: context,
                                    builder: (BuildContext context) {
                                      return AddressProcessPopUp(
                                        isAdd: 'true',
                                        // index: 0,
                                        setData: setState,
                                        shopContext: context,
                                      );
                                    });
                              }
//                               else {
//                                 int length = isMobile == true ? 5 : 15;
//                                 setState(() {
//                                   if (suggestion.toString().length >= length) {
//                                     var val = suggestion
//                                         .toString()
//                                         .substring(0, length);
//                                     cart.country.text = val.toString() + "...";
//                                   } else {
//                                     cart.country.text = suggestion;
//                                   }
//                                   cart.country.text = suggestion;
//                                 });
// // getProductDetail(productId, 1, true);
//                                 Navigator.pushReplacement(
//                                     context,
//                                     MaterialPageRoute(
//                                         builder: (context) => Home(
//                                               route: 'centurion_shop',
//                                               param: 'true',
//                                             )));
//                                 cart.setDefaultAddress(cart.country.text);
//                               }

                              else {
                                setState(() {
                                  cart.country.text = suggestion;
                                });
                                // getProductDetail(productId, 1, true);
                                Navigator.pushReplacement(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) => Home(
                                              route: 'centurion_shop',
                                              param: 'true',
                                            )));
                                cart.setDefaultAddress(cart.country.text);
                              }
                            },

                            // autovalidateMode: AutovalidateMode.always,
                            // hideOnError: true,
                            // noItemsFoundBuilder: (BuildContext context) {
                            //   return Container(
                            //       color: Pallet.fontcolor,
                            //       child: Text(
                            //         'Required',
                            //         style: TextStyle(
                            //             color: Pallet.failed,
                            //             fontSize: Pallet.normalfont),
                            //       ));
                            // },
                          )),
                    )
                  : Container(),
              //  DropDownField(
              //     // hintText: 'Please choose country',
              //     required: true,
              //     textStyle: TextStyle(color: Pallet.fontcolornew),
              //     value: dropDownCountry,
              //     onValueChanged: (newValue) {
              //       setState(() {
              //         // cart.cities = [];
              //         // cart.zipCode = [];
              //         // district.text = '';
              //         // postalcode.text = '';
              //         // enablecity = true;
              //         dropDownCountry = newValue;
              //         // getRegionsMap['region_level'] = 4;
              //         // getRegionsMap['parent_region'] =
              //         //     dropDownCountry.toUpperCase();

              //         // cart.getCities(getRegionsMap);
              //       });
              //     },
              //     items: cart.countries))

              widget.route != 'individual_profile'
                  ? FutureBuilder(
                      future: temp,
                      builder: (context, snapshot) {
                        if (snapshot.hasData) {
                          print('snapshot.hasData');
                          print(snapshot.data);
                          return PopupMenuButton(
                            onSelected: (result) async {
                              if (result == 0) {
                                Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) =>
                                          Home(route: 'my_settings')),
                                );
                              }
                              if (result == 1) {
                                showDialog(
                                    barrierDismissible: false,
                                    context: context,
                                    builder: (BuildContext context) {
                                      return AlertDialog(
                                        backgroundColor:
                                            Pallet.popupcontainerback,
                                        shape: RoundedRectangleBorder(
                                            borderRadius: BorderRadius.circular(
                                                Pallet.radius)),
                                        title: Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.start,
                                          children: [
                                            Image.asset("c-logo.png",
                                                width: 40),
                                            SizedBox(width: 10),
                                            Expanded(
                                              child: Text(pageDetails['text16'],
                                                  style: TextStyle(
                                                      fontSize: Pallet.heading3,
                                                      color: Pallet.fontcolor,
                                                      fontWeight:
                                                          Pallet.font500)),
                                            ),
                                          ],
                                        ),
                                        content: Container(
                                          height: 150,
                                          child: Column(
                                            mainAxisAlignment:
                                                MainAxisAlignment.spaceEvenly,
                                            children: [
                                              CircleAvatar(
                                                backgroundColor: Colors.white,
                                                radius: 30,
                                                child: Icon(Icons.logout,
                                                    color: Pallet
                                                        .dashcontainerback,
                                                    size: 40),
                                              ),
                                              Text(pageDetails['text17'],
                                                  style: TextStyle(
                                                      fontSize: Pallet.heading2,
                                                      color: Pallet.fontcolor,
                                                      fontWeight:
                                                          Pallet.font500)),
                                            ],
                                          ),
                                        ),
                                        actions: [
                                          PopupButton(
                                            text: pageDetails['text18'],
                                            textsize: Pallet.heading2,
                                            width: 100,
                                            onpress: () {
                                              Navigator.of(context).pop();
                                            },
                                          ),
                                          PopupButton(
                                            text: pageDetails['text19'],
                                            textsize: Pallet.heading2,
                                            width: 100,
                                            onpress: () async {
                                              SharedPreferences prefs =
                                                  await SharedPreferences
                                                      .getInstance();

                                              prefs.remove("newshow");

                                              Navigator.push(
                                                context,
                                                MaterialPageRoute(
                                                    builder: (context) =>
                                                        Login()),
                                              );
                                            },
                                          ),
                                        ],
                                      );
                                    });
                                // Utils.removeSharedPrefrences('data');
                                SharedPreferences prefs =
                                    await SharedPreferences.getInstance();
                                prefs.remove("shopbannershow");
                                prefs.remove("newshow");
                              }
                            },
                            color: Pallet.inner1,
                            tooltip: 'My Account',
                            icon: CircleAvatar(
                              child: ClipRRect(
                                borderRadius: BorderRadius.circular(50),
                                child: dashboard.userprofile != null
                                    ? Image.network(
                                        appSettings['SERVER_URL'] +
                                            '/' +
                                            dashboard.userprofile,
                                        fit: BoxFit.contain,
                                      )
                                    : Image.asset(
                                        'temp-profile.png',
                                        fit: BoxFit.contain,
                                      ),
                              ),
                              backgroundColor: Colors.transparent,
                              radius: 20,
                            ),
                            itemBuilder: (BuildContext context) {
                              return [
                                PopupMenuItem(
                                    child: Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        children: [
                                      InkWell(
                                        onTap: () async {
// ------------------------------lottery part---------------------------------
                                          print('lnjknhnlkn');
                                          if (settingskycdata.settingskyc ==
                                              'verified') {
                                            Navigator.push(
                                                context,
                                                MaterialPageRoute(
                                                    builder: (BuildContext
                                                            context) =>
                                                        Home(
                                                          route:
                                                              'individual_profile',
                                                          manageaddressonly:
                                                              false,
                                                        )));
                                          }
                                          // String kycstatus;
                                          // kycstatus = settingskycdata.settingskyc;
                                        },
                                        child: Row(
                                          children: [
                                            CircleAvatar(
                                              backgroundColor:
                                                  Colors.transparent,
                                              child: ClipRRect(
                                                borderRadius:
                                                    BorderRadius.circular(16),
                                                child: dashboard.userprofile !=
                                                        null
                                                    ? Image.network(
                                                        appSettings[
                                                                'SERVER_URL'] +
                                                            '/' +
                                                            dashboard
                                                                .userprofile,
                                                        fit: BoxFit.contain,
                                                      )
                                                    : Image.asset(
                                                        'temp-profile.png',
                                                        fit: BoxFit.contain,
                                                      ),
                                                // child: Image.network(
                                                //   dashboard.userprofile == null
                                                //       ? 'https://profiles.onshape.com/62ed0e33daab89201831c978581fce85e765efb88b09d62d88d1bf278480a499.png'
                                                //       : appSettings['SERVER_URL'] +
                                                //           '/' +
                                                //           dashboard.userprofile,
                                                //   fit: BoxFit.contain,
                                                // ),
                                              ),
                                              radius: 16,
                                            ),
                                            SizedBox(width: 10),
                                            Expanded(
                                              child: Text(
                                                // 'Jagan Jagadhess Rajendran Jagan Jagadhess Rajendran',

                                                dashboard.username == null
                                                    ? ""
                                                    : dashboard.username
                                                        .toString()
                                                        .capitalizeFirstofEach,
                                                // overflow: TextOverflow.ellipsis,
                                                style: TextStyle(
                                                    fontSize: Pallet.heading3,
                                                    color: Pallet.fontcolor),
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ])),
                                PopupMenuItem(
                                  value: 0,
                                  child: Row(
                                    children: [
                                      Image.asset(
                                        'settings.png',
                                        width: 18,
                                        // color: Pallet.fontcolor,
                                      ),
                                      SizedBox(width: 10.0),
                                      Text(
                                        pageDetails['text5'],
                                        style: TextStyle(
                                            color: Pallet.fontcolor,
                                            fontSize: Pallet.heading5),
                                      ),
                                    ],
                                  ),
                                ),
                                PopupMenuItem(
                                  value: 1,
                                  child: Column(
                                    children: [
                                      Row(
                                        children: [
                                          Image.asset(
                                            'logout.png',
                                            width: 18,
                                            // color: Pallet.fontcolor,
                                          ),
                                          SizedBox(width: 10.0),
                                          Text(
                                            pageDetails['text9'],
                                            style: TextStyle(
                                                color: Pallet.fontcolor,
                                                fontSize: Pallet.heading4),
                                          ),
                                        ],
                                      ),
                                      SizedBox(
                                        height: 20,
                                      ),
                                      Divider(
                                        color: Pallet.fontcolor,
                                        height: 8,
                                      ),
                                    ],
                                  ),
                                ),
                                for (var i = 0;
                                    i < dashboard.multipleAccounts.length;
                                    i++)
                                  PopupMenuItem(
                                      child: InkWell(
                                    onTap: () async {
                                      print("&&&&&&&&&&&");
                                      print(dashboard.multipleAccounts[i]
                                          ['account_id']);
                                      print(await Utils.getSharedPrefereces(
                                          'for_acount'));
                                      print("________________________");
                                      String _temp =
                                          await Utils.getSharedPrefereces(
                                              'for_acount');
                                      Map<String, dynamic> _map = {
                                        "system_product_id": 1,
                                        "account_id": dashboard
                                            .multipleAccounts[i]['account_id'],
                                        "token": _temp,
                                      };
                                      print('MMMMMMMM');
                                      print(_map);

                                      var _result = await HttpRequest.Post(
                                          'use_account',
                                          Utils.constructPayload(_map));
                                      if (Utils.isServerError(_result)) {
                                        print(_result);
                                        // ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                                        //     content: Text("Something Went Wrong")));
                                        Navigator.of(context).pop();
                                        //print(Utils.isServerError(
                                        // result));
                                      } else {
                                        await Utils.setSharedPrefereces(
                                            Utils.toJSONString(
                                                _result['response']['data']),
                                            'data');
                                        print(
                                            '************************************************');
                                        print(_result);
                                        // setState(() {
                                        // });
                                        Navigator.pushReplacement(
                                            context,
                                            MaterialPageRoute(
                                                builder:
                                                    (BuildContext context) =>
                                                        Home(
                                                          route: 'dashboard',
                                                        )));
                                      }
                                    },
                                    child: Container(
                                      child: Row(
                                        children: [
                                          CircleAvatar(
                                            backgroundColor: Colors.transparent,
                                            child: ClipRRect(
                                              borderRadius:
                                                  BorderRadius.circular(16),
                                              child: dashboard.userprofile !=
                                                      null
                                                  ? Image.network(
                                                      appSettings[
                                                              'SERVER_URL'] +
                                                          '/' +
                                                          dashboard.multipleAccounts[
                                                                  i]
                                                              ['profile_image'],
                                                      fit: BoxFit.contain,
                                                    )
                                                  : Image.asset(
                                                      'temp-profile.png',
                                                      fit: BoxFit.contain,
                                                    ),
                                              // child: Image.network(
                                              //   dashboard.userprofile == null
                                              //       ? 'https://profiles.onshape.com/62ed0e33daab89201831c978581fce85e765efb88b09d62d88d1bf278480a499.png'
                                              //       : appSettings['SERVER_URL'] +
                                              //           '/' +
                                              //           dashboard.userprofile,
                                              //   fit: BoxFit.contain,
                                              // ),
                                            ),
                                            radius: 16,
                                          ),
                                          // Image.network(
                                          //   appSettings['SERVER_URL'] +
                                          //       '/' +
                                          //       item['profile_image'],
                                          //   width: 18,
                                          // ),
                                          SizedBox(width: 10),
                                          Expanded(
                                            child: Text(
                                              // 'Jagan Jagadhess Rajendran Jagan Jagadhess Rajendran',
                                              // item['screen_name'],
                                              dashboard.multipleAccounts[i]
                                                          ['screen_name'] ==
                                                      null
                                                  ? ""
                                                  : dashboard
                                                      .multipleAccounts[i]
                                                          ['screen_name']
                                                      .toString()
                                                      .capitalizeFirstofEach,
                                              overflow: TextOverflow.ellipsis,
                                              style: TextStyle(
                                                  fontSize: Pallet.heading3,
                                                  color: Pallet.fontcolor),
                                            ),
                                          ),
                                          // Text(
                                          //   item['screen_name'],
                                          //   style: TextStyle(
                                          //       color: Pallet.fontcolor,
                                          //       fontSize: Pallet.heading4),
                                          // ),
                                        ],
                                      ),
                                    ),
                                  )),
                              ];
                            },
                          );
                        }
                        return Container();
                      })
                  : Container(),

              counter != 0
                  ? Stack(
                      children: <Widget>[
                        IconButton(
                            icon: Icon(
                              Icons.shopping_cart_rounded,
                              color:
// cart.animate == false
                                  Colors.black,
// : Colors.orange,
                              size: 25,
                            ),
                            onPressed: () {
                              setState(() {
                                if (_scaffoldKey.currentState.isEndDrawerOpen ==
                                    false) {
                                  _scaffoldKey.currentState.openEndDrawer();
                                } else {
                                  _scaffoldKey.currentState.openDrawer();
                                }
                              });
                            }),
                        counter != 0 && counter != null
                            ? Positioned(
                                top: cart.animate == true ? -5 : -1,
                                right: cart.animate == true ? 0 : 3,
                                child: Badge(
// toAnimate: true,
// animationType: BadgeAnimationType.scale,
// animationDuration: Duration(
// milliseconds: 250,
// ),
                                  shape: BadgeShape.circle,
                                  showBadge: true,
                                  badgeColor: Colors.red,
                                  borderRadius: BorderRadius.circular(8),
                                  badgeContent: Text(
                                    "$counter",
                                    style: TextStyle(
                                      color: Colors.white,
                                      fontSize: cart.animate == true ? 14 : 7,
                                    ),
                                  ),
                                ),
                              )
                            : new Container(),
                      ],
                    )
                  : IconButton(
                      icon: Icon(
                        Icons.shopping_cart_rounded,
                        color:
// cart.animate == false
                            Colors.black,
// : Colors.orange,
                        size: 25,
                      ),
                      onPressed: () {
                        setState(() {
                          if (_scaffoldKey.currentState.isEndDrawerOpen ==
                              false) {
                            _scaffoldKey.currentState.openEndDrawer();
                          } else {
                            _scaffoldKey.currentState.openDrawer();
                          }
                        });
                      }),
              SizedBox(width: 5),
              // widget.route == 'centurion_shop'
              //     ? Container()
              //     : PopupMenuButton(
              //         onSelected: (result) async {
              //           if (result == 0) {
              //             var val = await lottery.lotterypopup(context);
              //             if (val = null) {
              //               print("hello its null");
              //             } else {
              //               print(val);
              //             }
              //           }
              //           if (result == 1) {
              //             Navigator.push(
              //                 context,
              //                 MaterialPageRoute(
              //                     builder: (context) =>
              //                         Home(route: 'lottery_history')));
              //           }
              //           if (result == 2) {
              //             Navigator.push(
              //                 context,
              //                 MaterialPageRoute(
              //                     builder: (BuildContext context) => Home(
              //                           route: 'winner_Announcement',
              //                         )));
              //           }
              //         },
              //         color: Pallet.inner1,
              //         tooltip: 'Lottery Ticket',
              //         child: Container(
              //           margin: EdgeInsets.all(Pallet.defaultPadding),
              //           child: CustomButton(
              //             child: Center(
              //               child: BlinkText(
              //                 'Lottery',
              //                 beginColor: Colors.orange,
              //                 endColor: Pallet.fontcolor,
              //               ),
              //             ),
              //             textcolor: Pallet.fontcolor,
              //             buttoncolor: Pallet.dashcontainerback,
              //             fontweight: Pallet.font600,
              //           ),
              //         ),
              //         itemBuilder: (BuildContext context) {
              //           return [
              //             PopupMenuItem(
              //               value: 0,
              //               child: Text(
              //                 "Join Now",
              //                 style: TextStyle(
              //                     color: Pallet.fontcolor,
              //                     fontSize: Pallet.heading5),
              //               ),
              //             ),
              //             PopupMenuItem(
              //               value: 1,
              //               child: Text(
              //                 "My Tickets",
              //                 style: TextStyle(
              //                     color: Pallet.fontcolor,
              //                     fontSize: Pallet.heading4),
              //               ),
              //             ),
              //             PopupMenuItem(
              //               value: 2,
              //               child: Text(
              //                 "The Winners",
              //                 style: TextStyle(
              //                     color: Pallet.fontcolor,
              //                     fontSize: Pallet.heading4),
              //               ),
              //             ),
              //           ];
              //         },
              //       ),
              // AnimatedButton(
              // onPress: () async {
              //   var val = await lottery.lotterypopup(context);
              //   if (val = null) {
              //     print("hello its null");
              //   } else {
              //     print(val);
              //   }
              // },
              //   height: 70,
              //   width: 200,
              //   text: 'Lottery',
              //   isReverse: true,
              //   selectedTextColor: Colors.black,
              //   transitionType: TransitionType.LEFT_TO_RIGHT,
              //   textStyle: TextStyle(color: Colors.pink),
              //   backgroundColor: Colors.black,
              //   borderColor: Colors.white,
              //   borderRadius: 50,
              //   borderWidth: 2,
              // ),
              // IconButton(
              //     icon: Icon(Icons.attach_money),
              //     onPressed: () async {
              //       // Navigator.push(
              //       //     context,
              //       //     MaterialPageRoute(
              //       //         builder: (context) =>
              //       //             Home(route: 'lottery_history')));

              //       var val = await lottery.lotterypopup(context);
              //       if (val = null) {
              //         print("hello its null");
              //       } else {
              //         print(val);
              //       }
              //     })
            ],
          ),
        ]);
  }

  Path drawStar(Size size) {
    // Method to convert degree to radians
    double degToRad(double deg) => deg * (pi / 180.0);

    const numberOfPoints = 5;
    final halfWidth = size.width / 2;
    final externalRadius = halfWidth;
    final internalRadius = halfWidth / 2.5;
    final degreesPerStep = degToRad(360 / numberOfPoints);
    final halfDegreesPerStep = degreesPerStep / 2;
    final path = Path();
    final fullAngle = degToRad(360);
    path.moveTo(size.width, halfWidth);

    for (double step = 0; step < fullAngle; step += degreesPerStep) {
      path.lineTo(halfWidth + externalRadius * cos(step),
          halfWidth + externalRadius * sin(step));
      path.lineTo(halfWidth + internalRadius * cos(step + halfDegreesPerStep),
          halfWidth + internalRadius * sin(step + halfDegreesPerStep));
    }
    path.close();
    return path;
  }
  // Widget footer(double wdgtWidth, double wdgtHeight) {
  //   Size size = MediaQuery.of(context).size;
  //   return Container(
  //     decoration: BoxDecoration(
  //         color: Pallet.fontcolor,
  //         // boxShadow: [Pallet.shadowEffect],
  //         border: Border(top: BorderSide(color: Colors.grey, width: 2))),
  //     padding: EdgeInsets.all(10),
  //     width: wdgtWidth,
  //     height: wdgtHeight,
  //     child: Column(
  //       crossAxisAlignment: CrossAxisAlignment.center,
  //       mainAxisAlignment: MainAxisAlignment.center,
  //       children: [
  //         Row(
  //           mainAxisAlignment: MainAxisAlignment.start,
  //           crossAxisAlignment: CrossAxisAlignment.start,
  //           children: [
  //             Expanded(
  //               child: Wrap(
  //                 spacing: 10,
  //                 runSpacing: 10,
  //                 children: [
  //                   Text(
  //                     '2021 © Centurion Global Limited',
  //                     style: TextStyle(
  //                       color: Pallet.fontcolornew,
  //                       fontSize: Pallet.heading6,
  //                       fontWeight: Pallet.font500,
  //                     ),
  //                   ),
  //                   Container(
  //                     height: wdgtHeight / 2.7,
  //                     width: 1.5,
  //                     color: Pallet.fontcolornew,
  //                   ),
  //                   InkWell(
  //                     onTap: () {
  //                       launch(
  //                           "https://network.ducatus.net/backoffice/static/files/documents/Terms_and_Conditions.pdf");
  //                     },
  //                     child: Text(
  //                       'Terms & Conditions',
  //                       style: TextStyle(
  //                         color: Pallet.fontcolornew,
  //                         fontSize: Pallet.heading6,
  //                         // fontWeight: Pallet.font500,
  //                       ),
  //                     ),
  //                   ),
  //                   Container(
  //                     height: wdgtHeight / 2.7,
  //                     width: 1.5,
  //                     color: Pallet.fontcolornew,
  //                   ),
  //                   InkWell(
  //                     onTap: () {
  //                       launch(
  //                           "https://network.ducatus.net/backoffice/static/files/documents/Rules_of_Conduct.pdf");
  //                     },
  //                     child: Text(
  //                       ' Rules of conduct',
  //                       style: TextStyle(
  //                         color: Pallet.fontcolornew,
  //                         fontSize: Pallet.heading6,
  //                         // fontWeight: Pallet.font500,
  //                       ),
  //                     ),
  //                   ),
  //                   Container(
  //                     height: wdgtHeight / 2.7,
  //                     width: 1.5,
  //                     color: Pallet.fontcolornew,
  //                   ),
  //                   InkWell(
  //                     onTap: () {
  //                       launch(
  //                           "https://network.ducatus.net/backoffice/static/files/documents/Privacy_Policy.pdf");
  //                     },
  //                     child: Text(
  //                       'Privacy Policy',
  //                       style: TextStyle(
  //                         color: Pallet.fontcolornew,
  //                         fontSize: Pallet.heading6,
  //                         // fontWeight: Pallet.font500,
  //                       ),
  //                     ),
  //                   ),
  //                   Container(
  //                     height: wdgtHeight / 2.7,
  //                     width: 1.5,
  //                     color: Pallet.fontcolornew,
  //                   ),
  //                   Text(
  //                     'Network Member Portal v3.0',
  //                     style: TextStyle(
  //                       color: Pallet.fontcolornew,
  //                       fontSize: Pallet.heading6,
  //                       // fontWeight: Pallet.font500,
  //                     ),
  //                   ),
  //                 ],
  //               ),
  //             )
  //           ],
  //         )
  //       ],
  //     ),
  //   );
  // }

  Widget sidebar(
      {double wdgtWidth, double wdgtHeight, double wdgtRadius, bool isphone}) {
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.only(
          topRight: Radius.circular(wdgtRadius),
        ),
        color: Pallet.inner1,
      ),
      padding: EdgeInsets.all(18),
      width: wdgtWidth,
      height: wdgtHeight,
      child: Container(
        child: ListView(
          children: [
            Image.asset(
              'final.png',
              width: wdgtWidth,
              alignment: Alignment.centerLeft,
            ),
            SizedBox(
              height: 15,
            ),
            for (var i = 0; i < navigations.length; i++)
              if (navigations[i]["route"].length > 1)
                Container(
                    margin: EdgeInsets.only(top: 5),
                    decoration: BoxDecoration(
                        color: widget.route == navigations[i]["route"]
                            ? Pallet.activelinkcolor
                            : Colors.transparent,
                        borderRadius: BorderRadius.circular(10)),
                    child: ListTile(
                        title: Row(
                          children: [
                            navigations[i]["image"],
                            // Icon(navigations[i]["image"],
                            //     color: Pallet.fontcolor),
                            Expanded(
                              child: Padding(
                                padding: EdgeInsets.only(left: 8),
                                child: Text(
                                  navigations[i]["nav_name"],
                                  style: TextStyle(
                                      color: Pallet.fontcolor,
                                      fontSize: Pallet.heading4),
                                ),
                              ),
                            ),
                          ],
                        ),
                        trailing: Icon(Icons.arrow_forward_ios,
                            size: 10,
                            color: widget.route == navigations[i]["route"]
                                ? Pallet.fontcolor
                                : Colors.transparent),
                        onTap: () {
                          // Navigator.push(
                          //   context,
                          //   MaterialPageRoute(
                          //       builder: (context) => Home(activeLink: i)),
                          // );
                          if (isphone == true) {
                            Navigator.of(context).pop();
                          }

                          setState(() {
                            widget.param = null;
                            widget.route = navigations[i]["route"];

                            // (wdgtRadius == 0) Navigator.pop(context);
                          });
                        }))
              else
                Container(
                    margin: EdgeInsets.only(top: 5),
                    decoration: BoxDecoration(
                        color: widget.route == navigations[i]
                            ? Pallet.activelinkcolor
                            : Colors.transparent,
                        borderRadius: BorderRadius.circular(10)),
                    child: ListTile(
                        title: Row(
                          children: [
                            navigations[i]["image"],
                            // Icon(navigations[i]["image"],
                            //     color: Pallet.fontcolor),
                            Padding(
                              padding: EdgeInsets.only(left: 8),
                              child: Text(
                                navigations[i]["nav_name"],
                                style: TextStyle(
                                    color: Pallet.fontcolor,
                                    fontSize: Pallet.heading4),
                              ),
                            ),
                          ],
                        ),
                        trailing: Icon(Icons.arrow_forward_ios,
                            size: 10,
                            color: widget.route == navigations[i]
                                ? Pallet.fontcolor
                                : Colors.transparent),
                        onTap: () async {
                          // Utils.removeSharedPrefrences('data');

                          showDialog(
                              barrierDismissible: false,
                              context: context,
                              builder: (BuildContext context) {
                                return AlertDialog(
                                  backgroundColor: Pallet.popupcontainerback,
                                  shape: RoundedRectangleBorder(
                                      borderRadius:
                                          BorderRadius.circular(Pallet.radius)),
                                  title: Row(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    children: [
                                      Image.asset("c-logo.png", width: 40),
                                      SizedBox(width: 10),
                                      Expanded(
                                        child: Text(pageDetails['text16'],
                                            style: TextStyle(
                                                fontSize: Pallet.heading3,
                                                color: Pallet.fontcolor,
                                                fontWeight: Pallet.font500)),
                                      ),
                                    ],
                                  ),
                                  content: Container(
                                    height: 150,
                                    child: Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceEvenly,
                                      children: [
                                        CircleAvatar(
                                          backgroundColor: Colors.white,
                                          radius: 30,
                                          child: Icon(Icons.logout,
                                              color: Pallet.dashcontainerback,
                                              size: 40),
                                        ),
                                        Text(pageDetails['text17'],
                                            style: TextStyle(
                                                fontSize: Pallet.heading2,
                                                color: Pallet.fontcolor,
                                                fontWeight: Pallet.font500)),
                                      ],
                                    ),
                                  ),
                                  actions: [
                                    PopupButton(
                                      text: pageDetails['text18'],
                                      textsize: Pallet.heading2,
                                      width: 100,
                                      onpress: () {
                                        Navigator.of(context).pop();
                                      },
                                    ),
                                    PopupButton(
                                      text: pageDetails['text19'],
                                      textsize: Pallet.heading2,
                                      width: 100,
                                      onpress: () async {
                                        SharedPreferences prefs =
                                            await SharedPreferences
                                                .getInstance();
                                        prefs.remove("shopbannershow");
                                        prefs.remove("reload");
                                        prefs.remove("newshow");

                                        prefs.remove("sessiontime");
                                        Navigator.push(
                                          context,
                                          MaterialPageRoute(
                                              builder: (context) => Login()),
                                        );
                                      },
                                    ),
                                  ],
                                );
                              });

                          // setState(() {
                          //   widget.activeLink = i;
                          //   if (wdgtRadius == 0) Navigator.pop(context);
                          // });
                        }))
          ],
        ),
      ),
    );
  }
}
