import 'package:logger/logger.dart' as log;

import '../../../config/index.dart';

class Logger {
  static final logger = log.Logger(
    printer: log.PrettyPrinter(
        methodCount: 2, // number of method calls to be displayed
        errorMethodCount: 8, // number of method calls if stacktrace is provided
        lineLength: 120, // width of the output
        colors: true, // Colorful log messages
        printEmojis: true, // Print an emoji for each log message
        printTime: true // Should each log print contain a timestamp
        ),
  );
  static void level() {
    logger.v('Verbose log');

    logger.d('Debug log');

    logger.i('Info log');

    logger.w('Warning log');

    logger.e('Error log');

    logger.wtf('What a terrible failure log');
  }

  static void verbose(dynamic message, [dynamic error, StackTrace stackTrace]) {
    if (appSettings['IS_LOGGER_ENABLED']) {
      logger.v(message, error, stackTrace);
    }
  }

  static void debug(dynamic message, [dynamic error, StackTrace stackTrace]) {
    if (appSettings['IS_LOGGER_ENABLED']) {
      logger.d(message, error, stackTrace);
    }
  }

  static void info(dynamic message, [dynamic error, StackTrace stackTrace]) {
    if (appSettings['IS_LOGGER_ENABLED']) {
      logger.i(message, error, stackTrace);
    }
  }

  static void warning(dynamic message, [dynamic error, StackTrace stackTrace]) {
    if (appSettings['IS_LOGGER_ENABLED']) {
      logger.w(message, error, stackTrace);
    }
  }

  static void error(dynamic message, [dynamic error, StackTrace stackTrace]) {
    if (appSettings['IS_LOGGER_ENABLED']) {
      logger.e(message, error, stackTrace);
    }
  }

  static void wtf(dynamic message, [dynamic error, StackTrace stackTrace]) {
    if (appSettings['IS_LOGGER_ENABLED']) {
      logger.wtf(message, error, stackTrace);
    }
  }
}
