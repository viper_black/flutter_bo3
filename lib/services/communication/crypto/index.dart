import 'dart:math';
import 'package:encrypt/encrypt.dart' as crypto;

import '../../common/index.dart';

class Encryption {
  String encode(String string) {
    try {
      var k1, k2;
      var b1, b2;

      //randomkey password
      final mKey = random().toString();
      final key = crypto.Key.fromUtf8(mKey);
      final iv = crypto.IV.fromUtf8(mKey);

      //find key
      k1 = mKey.substring(0, 8);
      k2 = mKey.substring(8, 16);

      //find bit
      b1 = (k1.length).toString();
      b2 = (k2.length).toString();

      //process
      final e = crypto.Encrypter(crypto.AES(key, mode: crypto.AESMode.cbc));
      final encryptedData = e.encrypt(string, iv: iv);
      //set format
      final encrypted = k2 + b1 + k1 + b2 + encryptedData.base64;

      return encrypted;
    } catch (e, trace) {

      Logger.error('Encryption Failed', e, trace);
      return 'Encode Failed';
    }
  }

  String decode(String encodedString) {
    try {
      var data = encodedString;
      var bit;
      var mKey, k1, k2;
      var b1, b2;
      var min, max;
      var cryptodata;

      //find bit
      b1 = data.substring(8, 9);
      b2 = data.substring(17, 18);

      //find key
      k2 = data.substring(0, int.parse(b2));
      k1 = data.substring(
          (k2.length + b1.length), (k2.length + b1.length + int.parse(b1)));
      mKey = k1.toString() + k2.toString();

      //find cryptodata
      bit = int.parse(b1) + int.parse(b2);
      min = bit + b1.length + b2.length;
      max = data.length - min;
      cryptodata = data.substring(min, min + max);

      //process
      final key = crypto.Key.fromUtf8(mKey);
      final iv = crypto.IV.fromUtf8(mKey);
      final e = crypto.Encrypter(crypto.AES(key, mode: crypto.AESMode.cbc));
      final decryptedData =
          e.decrypt(crypto.Encrypted.fromBase64(cryptodata), iv: iv);

      return decryptedData;
    } catch (e, trace) {
      Logger.error('Decryption Failed', e, trace);
      return 'Decode Failed';
    }
  }

  int random() {
    const MaxNumericDigits = 16;
    const digitCount = 16;
    final _random = Random();

    if (digitCount > MaxNumericDigits || digitCount < 1) {
      throw RangeError.range(
          0, 1, MaxNumericDigits, 'Invalid Rage of Count'.toString());
    }
    dynamic digit = _random.nextInt(9) + 1;
    int n = digit;

    for (var i = 0; i < digitCount - 1; i++) {
      digit = _random.nextInt(10);
      n *= 10;
      n += digit;
    }
    return n;
  }
}
