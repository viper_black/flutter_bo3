import '../../../utils/utils.dart';
import '../../../config/index.dart';
import '../crypto/index.dart';
import '../../common/index.dart';
import 'package:http/io_client.dart';
import 'dart:io';
import 'package:http/http.dart' as http;
import 'dart:convert';

// Future<http.Response> getApplicationsAPICall(CurrencyRequestModel post) async {
bool trustSelfSigned = true;
HttpClient httpClient = new HttpClient()
  ..badCertificateCallback =
  ((X509Certificate cert, String host, int port) => trustSelfSigned);
IOClient ioClient = new IOClient(httpClient);

// final response = await ioClient.post('$url',
// headers: {
// HttpHeaders.contentTypeHeader: 'application/json',
// //HttpHeaders.authorizationHeader: '',
// },
// body: currencyRequestToJson(post));
// return response;
// }

class HttpRequest {
  static Map<String, String> customHeaders = {
    'content-type': 'application/json',
    "Access-Control-Allow-Origin": "*", // Required for CORS support to work
    "Access-Control-Allow-Credentials":
    "true", // Required for cookies, authorization headers with HTTPS
    "Access-Control-Allow-Headers":
    "Origin,Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
    "Access-Control-Allow-Methods": "POST, OPTIONS",
    'Authorization': '',
  };
  static String getRequestBody(Map<String, dynamic> body) {
    final crypto = Encryption();
    String input;

    if (appSettings['IS_PRODUCTION']) {
      input = crypto.encode(json.encode(body['payload']));
      input = json.encode({'payload': input});
    } else {
      input = json.encode(body);
    }
    return input;
  }

  static String getResponseBody(http.Response response) {
    final crypto = Encryption();
    String output;
    if (appSettings['IS_PRODUCTION']) {
      if (json.decode(response.body)['response'] != null) {
        output = crypto.decode(json.decode(response.body)['response']);
        output = '{"response": $output}';
      } else {
        output = '{"response": {"error" : ${response.reasonPhrase}}}';
      }
    } else {
      output = response.body;
    }
    return output;
  }

  static Future<void> updateHeader() async {
    customHeaders['Authorization'] =
    Utils.fromJSONString(await Utils.getSharedPrefereces('data'))['token'];
  }

// ignore: non_constant_identifier_names
  static Future<Map<String, dynamic>> Get(String url) async {
    try {
      if (requestConfig[url]['requestHeader'].keys.contains('Authorization')) {
        await updateHeader();
      }
      String path = appSettings['SERVER_URL'] + requestConfig[url]['path'];
      var paths = Uri.parse(path);
// ignore: omit_local_variable_types
      http.Response response = await ioClient
          .get(paths, headers: customHeaders)
          .timeout(Duration(seconds: 50));

// ignore: omit_local_variable_types
      String output = getResponseBody(response);

// //print('Response status: ${response.statusCode}');
// //print("Response body: ${output}");
// //print('Response Headers: ${response.headers}');
// //print('Response Reason: ${response.reasonPhrase}');
// //print('Response RequestURI: ${response.request}');

// client.badCertificateCallback =
// ((X509Certificate cert, String host, int port) => true);
      return json.decode(output);
    } catch (e, trace) {
      Logger.error('ERROR in httpRequest Get', e, trace);
      return json.decode(
          '{"response": {"error":"Something Went Wrong","data":"null"}}');
    }
  }

// ignore: non_constant_identifier_names
  static Future<Map<String, dynamic>> Post(
      String url, Map<String, dynamic> body) async {
    try {
      if (requestConfig[url]['requestHeader'].keys.contains('Authorization')) {
        await updateHeader();
      }
// ignore: omit_local_variable_types
//print('intput: $body');
//print(url);
//print(customHeaders);

      String input = getRequestBody(body);
      String path = appSettings['SERVER_URL'] + requestConfig[url]['path'];
      var paths2 = Uri.parse(path);
// ignore: omit_local_variable_types
      print(path);
      print(input);
      http.Response response = await ioClient
          .post(paths2, headers: customHeaders, body: input)
          .timeout(Duration(seconds: 50));
// ignore: omit_local_variable_types
      String output = getResponseBody(response);

      print(" lkhjlkhj $output");
// //print('Response status: ${response.statusCode}');
// //print("Response body: ${output}");
// //print('Response Headers: ${response.headers}');
// //print('Response Reason: ${response.reasonPhrase}');
// //print('Response RequestURI: ${response.request}');

      return json.decode(output);
    } catch (e, trace) {
      Logger.error('ERROR in httpRequest Post', e, trace);
      return json.decode(
          '{"response": {"error":"Something Went Wrong","data":"null"}}');
    }
  }
}