import 'dart:convert';

import '../../../config/index.dart';
import '../crypto/index.dart';
import './connectivity.dart';
import '../../common/index.dart';

class SocketRequest {
  SocketRequest() {
    if (!socket.connected) {
      socket.connect();
    }
  }

  static void responseHandler(data, Function callback) {
    try {
      String output;
      final crypto = Encryption();

      if (appSettings['IS_PRODUCTION']) {
        if (json.decode(data)['response'] != null) {
          output = crypto.decode(json.decode(data)['response']);
          output = '{"response": $output}';
        } else {
          output = '{"response": "No Response From Server"}';
        }
      } else {
        output = data;
      }
      callback(json.decode(output));
    } catch (e, trace) {
      Logger.error('ERROR in SocketRequest responseHandler', e, trace);
      callback(json.decode(
          '{"response": {"error":"Something Went Wrong","data":"null"}}'));
    }
  }

  static Future<void> emit(String eventname, Map<String, dynamic> body,
      [Function callback]) async {
    try {
      final crypto = Encryption();
      String input;

      if (appSettings['IS_PRODUCTION']) {
        input = crypto.encode(json.encode(body['payload']));
      } else {
        input = json.encode(body['payload']);
      }

      socket.emit(eventname, {'payload': input});
      socket.off(eventname);
      socket.on(eventname, (data) => {responseHandler(data, callback)});
    } catch (e, trace) {
      Logger.error('ERROR in SocketRequest emit', e, trace);
      callback(json.decode(
          '{"response": {"error":"Something Went Wrong","data":"null"}}'));
    }
  }
}
