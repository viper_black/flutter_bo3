// ignore: library_prefixes
import 'package:socket_io_client/socket_io_client.dart' as IO;
import '../../../config/app_settings.dart';

IO.Socket socket = IO.io(
    appSettings['SERVER_URL'],
    IO.OptionBuilder()
        .setTransports(['websocket']) // for Flutter or Dart VM
        .disableAutoConnect() // disable auto-connection
        .setExtraHeaders({}) // optional
        .build());
