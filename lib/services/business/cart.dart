// import './product.dart';
// import '../screens/myshop.dart';

// import 'package:get/get_core/src/get_main.dart';
import 'package:centurion/screens/myshop.dart';
import 'package:get/get.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../config.dart';
import '../../utils/utils.dart' show Utils;
import '../communication/index.dart' show HttpRequest;
import 'package:flutter/material.dart';
// ignore: unused_import
import 'package:url_launcher/url_launcher.dart';
import '../../common.dart';
// ignore: duplicate_import
import '../../screens/maintenancepage.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import '../../home.dart';
// import 'account.dart';
import '../../config/app_settings.dart';
import 'package:qr_flutter/qr_flutter.dart';

Map<String, dynamic> pageDetails = {};

class Cart {
  int totalCount = 0;
  double total = 0;
  double dsvpBalance;
  double cashBalance;
  String isKycStatus;
  double shippingCosts = 0;
  bool animate = false;
  bool userActiveStatus = false;
  String negativeAmount;

  TextEditingController country = TextEditingController();
  int selectRadio = 0;
  String categoryName;
  // bool isAvailable;
  List cart = [];
  List address = [];
  List addressNames = []; // isExistInCart(data){
  // isExistInCart(data){
  //   for (var temp in cart) {
  //     if(temp['product_id'])
  //   }
  // }

  bool askpopup = false;
  List<String> countries = [];
  List<String> cities = [];
  List<String> zipCode = [];

  setPageDetails1() async {
    print("called");
    String data = await Utils.getPageDetails1('businesscart');
    pageDetails = Utils.fromJSONString(data);
  }

  getRegions(
    Map<String, dynamic> data,
  ) async {
    Map<String, dynamic> result =
        await HttpRequest.Post('getRegions', Utils.constructPayload(data));
    print('$result, getRegions');
    if (Utils.isServerError(result)) {
      throw (await Utils.getMessage(result['response']['error']));
    } else {
      countries = [];
      for (var tempcountries in result['response']['data']) {
        countries.add(tempcountries['country_name']);
      }

      return Utils.toJSONString(
          await Utils.convertMessage(result['response']['data']));
    }
  }

  getCities(
    Map<String, dynamic> data,
  ) async {
    cities.clear();
    Map<String, dynamic> result =
        await HttpRequest.Post('getRegions', Utils.constructPayload(data));
    print('$result, getRegions');
    if (Utils.isServerError(result)) {
      throw (await Utils.getMessage(result['response']['error']));
    } else {
      for (var tempcities in result['response']['data']) {
        cities.add(tempcities['city']);
      }

      return Utils.toJSONString(
          await Utils.convertMessage(result['response']['data']));
    }
  }

  getZipCode(
    Map<String, dynamic> data,
  ) async {
    zipCode.clear();
    Map<String, dynamic> result =
        await HttpRequest.Post('getRegions', Utils.constructPayload(data));
    print('$result, getRegions');
    if (Utils.isServerError(result)) {
      throw (await Utils.getMessage(result['response']['error']));
    } else {
      for (var tempZipCode in result['response']['data']) {
        zipCode.add(tempZipCode['postal_code']);
      }

      return Utils.toJSONString(
          await Utils.convertMessage(result['response']['data']));
    }
  }

  getDsvPData() async {
    Map<String, dynamic> _map;
    Map<String, dynamic> _result;

    _map = {'system_product_id': 1};
    _result = await HttpRequest.Post('getdsv_p', Utils.constructPayload(_map));
    if (Utils.isServerError(_result)) {
      print('I**********************I');

      // _result['response']['error'] == 'negative_amount'
      //     ? snack.snack(
      //         title:
      //             "Blocked because of Negative balance in any of your account")
      //     :
      snack.snack(title: "Something went wrong");

      return await Utils.getMessage(_result['response']['error']);
    } else {
      print('***********************check*_*********************');
      print(_result);
      negativeAmount =
          _result['response']['data']['user_data']['negative_amount_status'];
      userActiveStatus =
          _result['response']['data']['user_data']['user_active_status'];
      cashBalance = double.parse(
          _result['response']['data']['user_data']['cash_balance']);
      dsvpBalance =
          double.parse(_result['response']['data']['user_data']['dsv_p']);
      isKycStatus =
          (_result['response']['data']['user_data']['kyc_status']).toString();
      return "start";
    }
  }

  addAddress(Map<String, dynamic> data, setState) async {
    Map<String, dynamic> result =
        await HttpRequest.Post('addressProcess', Utils.constructPayload(data));

    print('$result, addressProcess');
    if (Utils.isServerError(result)) {
      throw (await Utils.getMessage(result['response']['error']));
    } else {
      getAddressInfo(setState);

      // Navigator.push(context,
      //     MaterialPageRoute(builder: (context) => Home(route: 'checkout')));
      return Utils.toJSONString(
          await Utils.convertMessage(result['response']['data']));
    }
  }

  static Future<bool> networkpartner(context, accounttype) async {
    Map<String, dynamic> _map;
    Map<String, dynamic> _result;

    _map = {'system_product_id': 1, 'account_type_id': accounttype};
    _result =
        await HttpRequest.Post('networkpartner', Utils.constructPayload(_map));

    if (Utils.isServerError(_result)) {
      throw (await Utils.getMessage(_result['response']['error']));
    } else {
      return true;
    }
  }

  // updateAddress(
  //     Map<String, dynamic> data, Map<String, dynamic> olddata, setState) async {
  //   Map<String, dynamic> result =
  //       await HttpRequest.Post('addressProcess', Utils.constructPayload(data));
  //   //print('$result, addressProcess');
  //   if (Utils.isServerError(result)) {
  //     throw (await Utils.getMessage(result['response']['error']));
  //   } else {
  //     setState(() {
  //       address.remove(olddata);
  //       address.add(data);
  //     });
  //     // Navigator.push(context,
  //     //     MaterialPageRoute(builder: (context) => Home(route: 'checkout')));
  //     return Utils.toJSONString(
  //         await Utils.convertMessage(result['response']['data']));
  //   }
  // }
  updateAddress(
      Map<String, dynamic> data, Map<String, dynamic> olddata, setData) async {
    print('_____________IIIIII_____________');
    print(data);
    print(olddata);

    print('________0_______');
    Map<String, dynamic> result =
        await HttpRequest.Post('addressProcess', Utils.constructPayload(data));
//print('$result, addressProcess');
    if (Utils.isServerError(result)) {
      throw (await Utils.getMessage(result['response']['error']));
    } else {
      setData(() {
        address.remove(olddata);
        address.add(data);
      });

      print(address);
      print(address.length);

      getAddressInfo(setData);
// Navigator.push(context,
// MaterialPageRoute(builder: (context) => Home(route: 'checkout')));
      return Utils.toJSONString(
          await Utils.convertMessage(result['response']['data']));
    }
  }
  // updateAddress(Map<String, dynamic> data, setState, context) async {
  //   Map<String, dynamic> result =
  //       await HttpRequest.Post('updateAddress', Utils.constructPayload(data));
  //   //print('$result, updateAddress');

  //   if (Utils.isServerError(result)) {
  //     throw (await Utils.getMessage(result['response']['error']));
  //   } else {
  //     Navigator.push(context,
  //         MaterialPageRoute(builder: (context) => Home(route: 'checkout')));
  //     return Utils.toJSONString(
  //         await Utils.convertMessage(result['response']['data']));
  //   }
  // }
  getCountries() async {
    Map<String, dynamic> _map = {
      "product_id": 3,
      "option": 3,
      "find": "Ireland"
    };
    Map<String, dynamic> result =
        await HttpRequest.Post('region', Utils.constructPayload(_map));
    //print('$result, region');
    if (Utils.isServerError(result)) {
      throw (await Utils.getMessage(result['response']['error']));
    } else {
      return Utils.toJSONString(
          await Utils.convertMessage(result['response']['data']));
    }
  }

  removeAll(setState, setData, _map, context) async {
    for (var idd = 0; idd < cart.length; idd++) {
      print(idd);
      print('&&&&&&&&&&&&&&&&&&&&&&&&&&&');
      cart[idd]['qty'] = 1;
      print(cart[idd]);
      Map<String, dynamic> _map = {};
      _map = {
        "product_id": 1,
        "variant_id": cart[idd]['variant_id'],
        "product_id_fs": cart[idd]['product_id'],
        "qty": 0
      };
      // ignore: unused_local_variable
      Map<String, dynamic> result =
          await HttpRequest.Post('addToCart', Utils.constructPayload(_map));
    }
    myCart(setState, setData);
    addToCart(_map, setState, setData, context);
    startAnimate(setState, setData);
  }

  buyProducts(BuildContext context) async {
    Map<String, dynamic> _map = {
      'product_id': 1,
      'coupon_code': [
        'null',
      ], //  ------------------->>>>>>>>>>>>>>>>>> need to check why couponcode is in Array format
      'overall_total': total,
      'address_id': 1,
    };
    Map<String, dynamic> result =
        await HttpRequest.Post('buyProduct', Utils.constructPayload(_map));
    //print('$result, buyProduct');

    if (Utils.isServerError(result)) {
      if (result['response']['error'] == 'kyc_not_verified') {
        snack.snack(title: 'KYC Not Verified');
      } else {
        if (result['response']['error'] == 'negative_amount') {
          snack.snack(
              title:
                  "Blocked because of Negative balance in any of your account");
        } else {
          snack.snack(title: pageDetails["text1"]);
        }
      }

      // final snackBar = SnackBar(content: Text(pageDetails["text1"]));
      // ScaffoldMessenger.of(context).showSnackBar(snackBar);
      throw (await Utils.getMessage(result['response']['error']));
    } else {
      snack.snack(title: 'Payment Success');
      paymentsucess(context);
      // SharedPreferences prefs = await SharedPreferences.getInstance();
      // Get.to(Home(
      //   route: prefs.getString('reload'),
      // ));
    }
  }

  getpagenavigator() {
    Get.to(Maintenancepage());
  }

  proceedcheckout(context) {
    return showDialog(
        barrierDismissible: false,
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            backgroundColor: Pallet.popupcontainerback,
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(Pallet.radius)),
            title: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Image.asset("c-logo.png", width: 30),
                SizedBox(width: 10),
                Text(pageDetails["text43"],
                    style: TextStyle(
                      fontSize: Pallet.heading3,
                      color: Pallet.fontcolor,
                    )),
              ],
            ),
            content: Container(
              width: 200,
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  RichText(
                    textAlign: TextAlign.justify,
                    text: TextSpan(
                      text: pageDetails["text44"] + ' ',
                      style: TextStyle(
                          height: 1.5,
                          fontSize: Pallet.heading4,
                          fontFamily: 'Montserrat',
                          color: Pallet.fontcolor),
                      children: <TextSpan>[
                        TextSpan(
                          text: pageDetails["text45"] + ' ',
                          style: TextStyle(
                              height: 1.5,
                              fontSize: Pallet.heading4,
                              fontWeight: FontWeight.bold,
                              fontFamily: 'Montserrat',
                              color: Pallet.fontcolor),
                        ),
                        TextSpan(
                            text: pageDetails["text46"] + ' ',
                            style: TextStyle(
                                height: 1.5,
                                fontSize: Pallet.heading4,
                                fontFamily: 'Montserrat',
                                color: Pallet.fontcolor)),
                        TextSpan(
                            text: total.toString() +
                                ' ' +
                                pageDetails["text47"] +
                                ' ' +
                                '\$ ' +
                                shippingCosts.toString() +
                                ' ' +
                                pageDetails["text48"],
                            style: TextStyle(
                                fontSize: Pallet.heading4,
                                height: 1.5,
                                fontFamily: 'Montserrat',
                                color: Pallet.fontcolor)),
                      ],
                    ),
                  ),
                  // Text(
                  //     'By Clicking on "Confirm" button you agree to deduct' +
                  //         ' ' +
                  //         total.toString() +
                  //         ' ' +
                  //         'from your DSV-P balance for the product Cost and' +
                  //         ' ' +
                  //         '\$ ' +
                  //         shippingCosts.toString() +
                  //         ' ' +
                  //         'from your Cash Balance for paying the shipping cost.',
                  //     textAlign: TextAlign.justify,
                  //     style: TextStyle(
                  //       height: 1.4,
                  //       fontSize: Pallet.heading4,
                  //       color: Pallet.fontcolor,
                  //       // fontWeight: Pallet.font500
                  //     )),
                ],
              ),
            ),
            actions: [
              PopupButton(
                text: pageDetails["text36"],
                onpress: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (BuildContext context) => Home(
                                route: 'dashboard',
                              )));
                  // Navigator.of(context).pop();
                },
              ),
              PopupButton(
                text: pageDetails["text49"],
                onpress: () {
                  buyProducts(context);
                  // Navigator.of(context).pop();
                },
              ),
            ],
          );
        });
  }

  paymentsucess(context) {
    return showDialog(
        barrierDismissible: false,
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            backgroundColor: Pallet.inner1,
            title: Row(
              children: [
                Image.asset(
                  "c-logo.png",
                  height: 30,
                ),
                SizedBox(
                  width: 10,
                ),
                Text(
                  pageDetails["text39"],
                  style: TextStyle(color: Pallet.fontcolor),
                ),
              ],
            ),
            content: Text(
              pageDetails["text2"],
              style: TextStyle(color: Pallet.fontcolor),
            ),
            actions: [
              PopupButton(
                text: pageDetails["text3"],
                onpress: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (BuildContext context) => Home(
                                route: 'my_orders',
                              )));
                },
              )
            ],
          );
        });
  }

  deleteAddress(Map<String, dynamic> data, setState) async {
    Map<String, dynamic> result =
        await HttpRequest.Post('addressProcess', Utils.constructPayload(data));
    //print('$result, addressProcess');

    if (Utils.isServerError(result)) {
      throw (await Utils.getMessage(result['response']['error']));
    } else {
      getAddressInfo(setState);
      return Utils.toJSONString(
          await Utils.convertMessage(result['response']['data']));
    }
  }

  setDefaultAddress(String addresName) async {
    int addressId;
    Map<String, dynamic> data = {};
    for (var tempId in address) {
      if (tempId['address_name'] == addresName) {
        addressId = tempId['address_id'];
      }
    }
    print(addressId);
    print('addressId printed');
    data = {'product_id': 1, 'address_id': addressId};

    Map<String, dynamic> result = await HttpRequest.Post(
        'setDefaultAddress', Utils.constructPayload(data));
    print('$result, setDefaultAddress');

    if (Utils.isServerError(result)) {
      throw (await Utils.getMessage(result['response']['error']));
    } else {
      return 'done';
      // return Utils.toJSONString(
      //     await Utils.convertMessage(result['response']['data']));
    }
  }

  Future<String> getAddressInfo(setState) async {
    Map<String, dynamic> result =
        await HttpRequest.Post('addressInfo', Utils.getSiteID());
//print('$result, addressInfo');

    if (Utils.isServerError(result)) {
      throw (await Utils.getMessage(result['response']['error']));
    } else {
      print(result['response']['data']);
      print('address Response');
      var val = false;
      print('asdfasdf');
      addressNames = [];
      address = [];
      setState(() {
        address = result['response']['data'];
      });
      print('adddresss');
      if (address.length == 0) {
        country.text = '';
      }
      print('lkjdfasdflasdjf');
      print(address.length);
      for (var i = 0; i < address.length; i++) {
        addressNames.add(address[i]['address_name']);
// await setDefaultAddress(country.text);

// if (address.length == 1) {
// print("cccccccccczzdddssss");
// setState(() {
// selectRadio = i;
// print(selectRadio);
// });
// country.text = await address[i]["address_name"];
// var val = await setDefaultAddress(address[i]["address_name"]);
// print(val);
// }
        if (address[i]['is_main'] == true) {
          setState(() {
            selectRadio = i;
          });
// country.text = await address[i]["address_name"];
// await setDefaultAddress(country.text);
// country.text = address[i]['address_name'];
        }
      }
      if (address.length > 0) {
        print("cccccccccczzdddssss");
// setState(() {
// selectRadio = 0;
// print(selectRadio);
// });
        if (address[selectRadio]["address_name"].toString().length >= 10) {
          // ignore: await_only_futures
          var val = await address[selectRadio]["address_name"]
              .toString()
              .substring(0, 10);
          country.text = val.toString() + "...";
        } else {
          country.text = await address[selectRadio]["address_name"];
        }
        var val = await setDefaultAddress(address[selectRadio]["address_name"]);
        print(val);
      }

      for (var i = 0; i < addressNames.length; i++) {
        if (addressNames[i] == '+ Add New Address') {
          val = true;
        } else {
          val = false;
        }
      }
      if (val == false) {
        addressNames.add('+ Add New Address');
      }

      return 'start';
    }
  }

  // Future<String> getAddressInfo(setState) async {
  //   Map<String, dynamic> result =
  //       await HttpRequest.Post('addressInfo', Utils.getSiteID());
  //   //print('$result, addressInfo');

  //   if (Utils.isServerError(result)) {
  //     throw (await Utils.getMessage(result['response']['error']));
  //   } else {
  //     print(result['response']['data']);
  //     print('address Response');
  //     var val = false;
  //     print('asdfasdf');
  //     addressNames = [];
  //     address = [];
  //     setState(() {
  //       address = result['response']['data'];
  //     });
  //     print('adddresss');
  //     if (address.length == 0) {
  //       country.text = '';
  //     }
  //     print('lkjdfasdflasdjf');
  //     print(address.length);
  //     for (var i = 0; i < address.length; i++) {
  //       addressNames.add(address[i]['address_name']);
  //       // await setDefaultAddress(country.text);

  //       // if (address.length == 1) {
  //       //   print("cccccccccczzdddssss");
  //       //   setState(() {
  //       //     selectRadio = i;
  //       //     print(selectRadio);
  //       //   });
  //       //   country.text = await address[i]["address_name"];
  //       //   var val = await setDefaultAddress(address[i]["address_name"]);
  //       //   print(val);
  //       // }
  //       if (address[i]['is_main'] == true) {
  //         setState(() {
  //           selectRadio = i;
  //         });
  //         // country.text = await address[i]["address_name"];
  //         // await setDefaultAddress(country.text);
  //         // country.text = address[i]['address_name'];
  //       }
  //     }
  //     if (address.length == 1) {
  //       print("cccccccccczzdddssss");
  //       setState(() {
  //         selectRadio = 0;
  //         print(selectRadio);
  //       });
  //       country.text = await address[0]["address_name"];
  //       var val = await setDefaultAddress(address[0]["address_name"]);
  //       print(val);
  //     }

  //     for (var i = 0; i < addressNames.length; i++) {
  //       if (addressNames[i] == '+ Add New Address') {
  //         val = true;
  //       } else {
  //         val = false;
  //       }
  //     }
  //     if (val == false) {
  //       addressNames.add('+ Add New Address');
  //     }

  //     return 'start';
  //   }
  // }

  addToCart(Map<String, dynamic> data, setState, setData, context) {
    bool isNotExist = true;

    for (var temp in cart) {
      if (temp['product_id'] == data['product_id'] &&
          temp['variant_id'] == data['variant_id']) {
        isNotExist = false;
        temp['qty'] += 1;
        updateCart(temp, setState, setData);
        startAnimate(setState, setData);
      }
    }
    //print(isNotExist);
    if (isNotExist) {
      data['qty'] = 1;
      cart.add(data);
      updateCart(data, setState, setData);
      startAnimate(setState, setData);
    }
  }

  startAnimate(setState, setData) {
    setData(() {
      setState(() {
        animate = true;
        //print('======');
        //print(animate);
      });
    });

    Future.delayed(
        Duration(milliseconds: 500),
        () => setData(() {
              setState(() {
                animate = false;
                //print('======');
                //print(animate);
              });
            }));
  }

  Future<bool> profiledetails(Map<String, dynamic> map) async {
    print(map);
    Map<String, dynamic> result =
        await HttpRequest.Post('profiledetails', Utils.constructPayload(map));
    //print('$result, region');
    if (Utils.isServerError(result)) {
      throw (await Utils.getMessage(result['response']['error']));
    } else {
      print(result['response']['error']);
      print("---------00000000000000000----------------");
      return true;
    }
  }

  redeemDSVP(context, setState, setData) async {
    // getDsvPData();
    String dsvpError;
    double reqCash = total - dsvpBalance;
    TextEditingController dsvpcontroller =
        TextEditingController(text: reqCash.toString());
    print('__________________________');
    print(cashBalance);
    print(dsvpBalance);
    print(shippingCosts);
    // if (dashboard.accountType == 2) {
    //   // if (isKycStatus == 'not_verified') {
    //   //   showDialog(
    //   //       barrierDismissible: false,
    //   //       context: context,
    //   //       builder: (BuildContext context) {
    //   //         return KycPopUp();
    //   //       });
    //   // }
    // } else {
    //   //print('KYC');
    if (total <= dsvpBalance) {
      print('dddd__________________________');

      if (shippingCosts > cashBalance) {
        print('1dddd__________________________');

        showDialog(
            context: context,
            builder: (BuildContext context) {
              return Insufficientfunds(
                  content: Text(pageDetails['text4'],
                      style: TextStyle(
                          fontSize: Pallet.normalfont,
                          color: Pallet.fontcolor,
                          fontWeight: Pallet.font500)),
                  onpress: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => Home(
                                  route: 'add_funding',
                                )));
                  },
                  onpress1: () {
                    Navigator.of(context).pop();
                  });
            });
      } else {
        await myCart(setState, setData);
        await proceedcheckout(context);
      }
    } else {
      print('2dddd__________________________');

      if ((shippingCosts + total) - dsvpBalance >= cashBalance) {
        print('3dddd__________________________');

        showDialog(
            context: context,
            builder: (BuildContext context) {
              return Insufficientfunds(
                  content: Text(pageDetails['text5'],
                      style: TextStyle(
                          fontSize: Pallet.normalfont,
                          color: Pallet.fontcolor,
                          fontWeight: Pallet.font500)),
                  onpress: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => Home(
                                  route: 'add_funding',
                                )));
                  },
                  onpress1: () {
                    Navigator.of(context).pop();
                  });
            });
      } else {
        print('4dddd__________________________');

        print("dddddddddddddd");
        await showDialog(
            context: context,
            barrierDismissible: false,
            builder: (context) {
              return StatefulBuilder(builder: (BuildContext context,
                  void Function(void Function()) setState1) {
                return Container(
                  decoration: BoxDecoration(
                      border: Border.all(color: Pallet.fontcolor)),
                  child: AlertDialog(
                    backgroundColor: Pallet.fontcolor,
                    title: Row(
                      children: [
                        Image.asset("c-logo.png",
                            height: 30, color: Pallet.dashcontainerback),
                        SizedBox(
                          width: 10,
                        ),
                        Text(
                          pageDetails['text6'],
                          style: TextStyle(color: Pallet.dashcontainerback),
                        ),
                      ],
                    ),
                    content: Container(
                      width: 400,
                      // height: 340,
                      child: SingleChildScrollView(
                        child: ListBody(
                          children: [
                            Container(
                              width: double.infinity,
                              padding: EdgeInsets.symmetric(
                                vertical: 10,
                                horizontal: 5,
                              ),
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(5),
                                  color: Pallet.fontcolor),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    pageDetails['text7'],
                                    style: TextStyle(
                                      fontSize: 15,
                                      color: Pallet.dashcontainerback,
                                      fontWeight: Pallet.font600,
                                    ),
                                  ),
                                  SizedBox(
                                    height: 3,
                                  ),
                                  Container(
                                    width: 170,
                                    decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(
                                            Pallet.radius),
                                        border: Border.all(
                                            color: Pallet.dashcontainerback)),
                                    child: Padding(
                                      padding:
                                          EdgeInsets.all(Pallet.defaultPadding),
                                      child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.end,
                                        children: [
                                          Text(
                                            dsvpBalance.toString(),
                                            style: TextStyle(
                                              color: Pallet.fontcolornew,
                                              fontWeight: Pallet.font600,
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            SizedBox(
                              height: 10,
                            ),
                            Container(
                              width: double.infinity,
                              padding: EdgeInsets.symmetric(
                                vertical: 10,
                                horizontal: 5,
                              ),
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(5),
                                  color: Pallet.fontcolor),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    pageDetails['text8'],
                                    style: TextStyle(
                                      fontSize: 15,
                                      color: Pallet.dashcontainerback,
                                      fontWeight: Pallet.font600,
                                    ),
                                  ),
                                  Container(
                                    width: 240,
                                    decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(
                                            Pallet.radius),
                                        border: Border.all(
                                            color: Pallet.dashcontainerback)),
                                    child: Padding(
                                      padding:
                                          EdgeInsets.all(Pallet.defaultPadding),
                                      child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.end,
                                        children: [
                                          Text(
                                            cashBalance.toString(),
                                            style: TextStyle(
                                              color: Pallet.fontcolornew,
                                              fontWeight: Pallet.font600,
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                  // Container(
                                  //   child: Center(
                                  //     child: Text(
                                  //       cashBalance.toString(),
                                  //       style: TextStyle(
                                  //         color: Pallet.dashcontainerback,
                                  //         fontWeight: Pallet.font600,
                                  //       ),
                                  //     ),
                                  //   ),
                                  // ),
                                  SizedBox(
                                    height: 20,
                                  ),
                                  Row(
                                    children: [
                                      Expanded(
                                        child: Text(pageDetails['text9'],
                                            style: TextStyle(
                                              color: Pallet.dashcontainerback,
                                            )),
                                      ),
                                      Text(pageDetails['text10'],
                                          style: TextStyle(
                                            color: Pallet.dashcontainerback,
                                            fontSize: 12,
                                          )),
                                    ],
                                  ),
                                  // Container(
                                  //   child: Center(
                                  //     child: Text(
                                  //         (total - dsvpBalance).toString() +
                                  //             'DSV-P',
                                  //         style: TextStyle(
                                  //           color: Pallet.dashcontainerback,
                                  //           fontSize: Pallet.normalfont,
                                  //         )),
                                  //   ),
                                  // ),
                                  SizedBox(height: 5),
                                  TextFormField(
                                    controller: dsvpcontroller,
                                    textAlign: TextAlign.right,
                                    // initialValue:
                                    //     (total - dsvpBalance).toString(),
                                    style: TextStyle(
                                        color: Pallet.dashcontainerback),
                                    cursorColor: Pallet.dashcontainerback,
                                    onChanged: (value) {
                                      if (value.isEmpty) {
                                        setState1(() {
                                          dsvpError = pageDetails['text11'];
                                        });
                                      } else if (double.parse((value)) >
                                          (cashBalance)) {
                                        setState1(() {
                                          dsvpError = pageDetails['text12'];
                                        });
                                      } else if (double.parse((value)) <=
                                          (cashBalance)) {
                                        setState1(() {
                                          dsvpError = null;
                                        });
                                      }
                                    },
                                    keyboardType:
                                        TextInputType.numberWithOptions(
                                            decimal: true),
                                    inputFormatters: <TextInputFormatter>[
                                      FilteringTextInputFormatter.digitsOnly,
                                    ],
                                    decoration: InputDecoration(
                                      errorText: dsvpError,
                                      border: OutlineInputBorder(
                                        borderSide: BorderSide(
                                            color: Pallet.dashcontainerback,
                                            width: 2),
                                      ),
                                      focusedBorder: OutlineInputBorder(
                                          borderSide: BorderSide(
                                              color: Pallet.dashcontainerback,
                                              width: 2),
                                          borderRadius: BorderRadius.circular(
                                              Pallet.radius)),
                                      enabledBorder: OutlineInputBorder(
                                          borderSide: BorderSide(
                                              color: Pallet.dashcontainerback,
                                              width: 2),
                                          borderRadius: BorderRadius.circular(
                                              Pallet.radius)),
                                      errorBorder: OutlineInputBorder(
                                          borderSide: BorderSide(
                                              color: Colors.red, width: 2),
                                          borderRadius: BorderRadius.circular(
                                              Pallet.radius)),
                                      focusedErrorBorder: OutlineInputBorder(
                                          borderSide: BorderSide(
                                              color: Colors.red, width: 2),
                                          borderRadius: BorderRadius.circular(
                                              Pallet.radius)),
                                      filled: true,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            SizedBox(
                              height: 15,
                            ),
                            Text(pageDetails['text13'],
                                style: TextStyle(
                                  color: Colors.red,
                                  fontSize: Pallet.heading7,
                                )),
                            // Center(
                            //   child: Text('(From Cash Account)',
                            //       style: TextStyle(
                            //         color: Pallet.dashcontainerback,
                            //         fontSize: 12,
                            //       )),
                            // ),
                            SizedBox(
                              height: 15,
                            ),
                            AbsorbPointer(
                              absorbing: (cashBalance) <= 0 ? true : false,
                              child: CustomButton(
                                vpadding: 10,
                                buttoncolor: Pallet.dashcontainerback,
                                text: pageDetails['text14'],
                                textcolor: Pallet.fontcolor,
                                onpress: () async {
                                  if (dsvpcontroller.text == null ||
                                      dsvpcontroller.text == '') {
                                    setState1(() {
                                      dsvpError = pageDetails['text15'];
                                    });
                                  } else if (dsvpError == null) {
                                    if (dsvpcontroller.text != null &&
                                        (cashBalance) > 0) {
                                      showDialog(
                                          barrierDismissible: false,
                                          context: context,
                                          builder: (BuildContext context) {
                                            return AlertDialog(
                                              backgroundColor:
                                                  Pallet.popupcontainerback,
                                              shape: RoundedRectangleBorder(
                                                  borderRadius:
                                                      BorderRadius.circular(
                                                          Pallet.radius)),
                                              title: Row(
                                                mainAxisAlignment:
                                                    MainAxisAlignment.start,
                                                children: [
                                                  Image.asset("c-logo.png",
                                                      width: 30),
                                                  SizedBox(width: 10),
                                                  Text('Confirm DSV-P',
                                                      style: TextStyle(
                                                        fontSize:
                                                            Pallet.heading3,
                                                        color: Pallet.fontcolor,
                                                      )),
                                                ],
                                              ),
                                              content: Container(
                                                width: 200,
                                                child: Column(
                                                  mainAxisSize:
                                                      MainAxisSize.min,
                                                  children: [
                                                    Text(
                                                        'By Clicking on "Confirm" button you agree to deduct' +
                                                            '  \$' +
                                                            (total -
                                                                    dsvpBalance)
                                                                .toString() +
                                                            ' ' +
                                                            'from your Cash balance for the DSV-P redeemption',
                                                        //  +
                                                        // ' ' +
                                                        // '\$ ' +
                                                        // shippingCosts.toString() +
                                                        // ' ' +
                                                        // 'from your Cash Balance for paying the shipping cost.',
                                                        textAlign:
                                                            TextAlign.justify,
                                                        style: TextStyle(
                                                          height: 1.4,
                                                          fontSize:
                                                              Pallet.heading4,
                                                          color:
                                                              Pallet.fontcolor,
                                                          // fontWeight: Pallet.font500
                                                        )),
                                                  ],
                                                ),
                                              ),
                                              actions: [
                                                PopupButton(
                                                  text: pageDetails["text36"],
                                                  onpress: () {
                                                    Navigator.of(context).pop();
                                                  },
                                                ),
                                                PopupButton(
                                                  text: pageDetails["text49"],
                                                  onpress: () async {
                                                    double amount =
                                                        double.parse(
                                                            dsvpcontroller
                                                                .text);
                                                    //print(amount);

                                                    setState1(() {
                                                      cashBalance =
                                                          (cashBalance -
                                                              amount);

                                                      dsvpBalance =
                                                          (dsvpBalance +
                                                              amount);

                                                      // dsvpcontroller.text = "";
                                                    });
                                                    Map<String, dynamic> _map =
                                                        {
                                                      "product_id": 1,
                                                      "split_type": 'none',
                                                      "is_dsv_p": true,
                                                      "total": amount,
                                                      "cash_account_used":
                                                          amount,
                                                      "trading_account_used": 0,
                                                      "packages": [],
                                                    };
                                                    // Map<String, dynamic> result =
                                                    //     await HttpRequest.Post(
                                                    //         'createPayment',
                                                    //         Utils.constructPayload(data));
                                                    // Map<String, dynamic> _map = {
                                                    //   "payload": {
                                                    //     "product_id": 1,
                                                    //     "split_type": 'none',
                                                    //     "is_dsv_p": true,
                                                    //     "total": amount,
                                                    //     "cash_account_used": amount,
                                                    //     "trading_account_used": 0,
                                                    //     "packages": [],
                                                    //   }
                                                    // };

                                                    waiting.waitpopup(context);
                                                    Map<String, dynamic>
                                                        _result =
                                                        await HttpRequest.Post(
                                                            'buyPackages',
                                                            Utils
                                                                .constructPayload(
                                                                    _map));
                                                    // Map<String, dynamic> _result =
                                                    //     await HttpRequest.Post(
                                                    //         'buyPackages', _map);
                                                    if (Utils.isServerError(
                                                        _result)) {
                                                      Navigator.pop(context);

                                                      Navigator.of(context)
                                                          .pop();
                                                      print(
                                                          'I=====================I');
                                                      print(_result['response']
                                                          ['error']);
                                                      _result['response']
                                                                  ['error'] ==
                                                              'negative_amount'
                                                          ? snack.snack(
                                                              title:
                                                                  "Blocked because of Negative balance in any of your account")
                                                          : snack.snack(
                                                              title: pageDetails[
                                                                  'text16']);

                                                      // final snackBar = SnackBar(
                                                      //     content:
                                                      //         Text(pageDetails['text16']));
                                                      // ScaffoldMessenger.of(context)
                                                      //     .showSnackBar(snackBar);
                                                      return await Utils
                                                          .getMessage(_result[
                                                                  'response']
                                                              ['error']);
                                                    } else {
                                                      Navigator.pop(context);

                                                      getDsvPData();
                                                      Navigator.of(context)
                                                          .pop();
                                                      return showDialog(
                                                          barrierDismissible:
                                                              false,
                                                          context: context,
                                                          builder: (BuildContext
                                                              context) {
                                                            return AlertDialog(
                                                              backgroundColor:
                                                                  Pallet
                                                                      .popupcontainerback,
                                                              shape: RoundedRectangleBorder(
                                                                  borderRadius:
                                                                      BorderRadius.circular(
                                                                          Pallet
                                                                              .radius)),
                                                              title: Row(
                                                                mainAxisAlignment:
                                                                    MainAxisAlignment
                                                                        .start,
                                                                children: [
                                                                  Image.asset(
                                                                      "c-logo.png",
                                                                      width:
                                                                          30),
                                                                  SizedBox(
                                                                      width:
                                                                          10),
                                                                  Expanded(
                                                                    child: Text(
                                                                        pageDetails[
                                                                            'text17'],
                                                                        style: TextStyle(
                                                                            fontSize: Pallet.normalfont +
                                                                                10,
                                                                            color:
                                                                                Pallet.fontcolor,
                                                                            fontWeight: Pallet.font500)),
                                                                  ),
                                                                ],
                                                              ),
                                                              content:
                                                                  Container(
                                                                height: 150,
                                                                child: Column(
                                                                  mainAxisAlignment:
                                                                      MainAxisAlignment
                                                                          .spaceEvenly,
                                                                  children: [
                                                                    CircleAvatar(
                                                                      backgroundColor:
                                                                          Colors
                                                                              .white,
                                                                      radius:
                                                                          30,
                                                                      child: Icon(
                                                                          Icons
                                                                              .done,
                                                                          color: Pallet
                                                                              .dashcontainerback,
                                                                          size:
                                                                              40),
                                                                    ),
                                                                    Text(
                                                                        pageDetails['text18'] +
                                                                            ' ' +
                                                                            dsvpcontroller
                                                                                .text,
                                                                        style: TextStyle(
                                                                            fontSize:
                                                                                Pallet.normalfont,
                                                                            color: Pallet.fontcolor,
                                                                            fontWeight: Pallet.font500)),
                                                                    Text(
                                                                        pageDetails['text19'] +
                                                                            ' ' +
                                                                            dsvpBalance
                                                                                .toString(),
                                                                        style: TextStyle(
                                                                            fontSize:
                                                                                Pallet.normalfont,
                                                                            color: Pallet.fontcolor,
                                                                            fontWeight: Pallet.font500))
                                                                  ],
                                                                ),
                                                              ),
                                                              actions: [
                                                                PopupButton(
                                                                  text: pageDetails[
                                                                      'text20'],
                                                                  onpress:
                                                                      () async {
                                                                    dsvpcontroller
                                                                        .text = "";
                                                                    // snack.snack(title: 'DSV-P ')
                                                                    await myCart(
                                                                        setState,
                                                                        setData);
                                                                    await proceedcheckout(
                                                                        context);
                                                                    Navigator.of(
                                                                            context)
                                                                        .pop();

                                                                    // setState(() {
                                                                    //   add({
                                                                    //     'product_id':
                                                                    //         data['products']
                                                                    //             ['product_id'],
                                                                    //     'product_name': data[
                                                                    //             'products']
                                                                    //         ['product_name'],
                                                                    //     'new_price':
                                                                    //         data['products']
                                                                    //             ['new_price'],
                                                                    //     'price':
                                                                    //         data['products']
                                                                    //             ['price'],
                                                                    //     'shipping_cost': data[
                                                                    //             'products']
                                                                    //         ['shipping_cost'],
                                                                    //     'variant_id':
                                                                    //         data['products']
                                                                    //             ['variant_id'],
                                                                    //     // 'image_url': data['products']['images'][0]['image_url']
                                                                    //   }, setState, setData,
                                                                    //       context);
                                                                    // });
                                                                  },
                                                                ),
                                                              ],
                                                            );
                                                          });
                                                    }
                                                    // buyProducts(context);
                                                    // Navigator.of(context).pop();
                                                  },
                                                ),
                                              ],
                                            );
                                          });
                                    } else if ((cashBalance) <= 0) {
                                      setState1(() {
                                        dsvpError = pageDetails['text12'];
                                      });
                                    }
                                  }
                                },
                                fontweight: Pallet.font600,
                              ),
                            ),
                            SizedBox(
                              height: 15,
                            ),
                            // Text(
                            //   "Insufficient Cash Balance to Get DSV-P?",
                            //   style: TextStyle(
                            //     color: Pallet.dashcontainerback,
                            //     fontSize: 11,
                            //   ),
                            // ),
                            // SizedBox(
                            //   height: 9,
                            // ),
                            // AbsorbPointer(
                            //   absorbing: (cashBalance) <= 0 ? false : true,
                            //   child: CustomButton(
                            //     vpadding: 10,
                            //     text: "ADD FUND",
                            //     buttoncolor: Pallet.dashcontainerback,
                            //     textcolor: Pallet.fontcolor,
                            //     onpress: () {
                            //       Navigator.push(
                            //         context,
                            //         MaterialPageRoute(
                            //             builder: (context) =>
                            //                 Home(route: 'add_funding')),
                            //       );
                            //     },
                            //     fontweight: Pallet.font600,
                            //   ),
                            // ),
                          ],
                        ),
                      ),
                    ),
                    actions: [
                      PopupButton(
                        buttoncolor: Pallet.dashcontainerback,
                        text: pageDetails['text3'],
                        textcolor: Pallet.fontcolor,
                        onpress: () {
                          Navigator.of(context).pop();
                          return "closed";
                          // Navigator.of(context).pop();
                        },
                      ),
                      SizedBox(height: 10),
                    ],
                  ),
                );
              });
            });
      }
    }
    // }
  }

  add(Map<String, dynamic> data, setState, setData, context) async {
    // bool isNotExist = true;
    String dsvpError;
    TextEditingController dsvpcontroller = TextEditingController();
    //print(data);
    //print('..........................................................');
    if (total + data["new_price"] <= dsvpBalance) {
      //print('__________________________');
      //print(cashBalance);
      //print(dsvpBalance);
      //print(shippingCosts);
      //print(data['shipping_cost']);
      //print(data);
      //print(data["new_price"]);
      //print('__________________________');

      if ((shippingCosts + data['shipping_cost']) >= cashBalance) {
        //print('-----------------------------------');
        //print(data['shipping_cost']);
        showDialog(
            context: context,
            builder: (BuildContext context) {
              return Insufficientfunds(
                  content: Text(pageDetails['text4'],
                      style: TextStyle(
                          fontSize: Pallet.normalfont,
                          color: Pallet.fontcolor,
                          fontWeight: Pallet.font500)),
                  onpress: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => Home(
                                  route: 'add_funding',
                                )));
                  },
                  onpress1: () {
                    Navigator.of(context).pop();
                  });
            });
      }
      //  else {
      //   for (var temp in cart) {
      //     if (temp['product_id'] == data['product_id'] &&
      //         temp['variant_id'] == data['variant_id']) {
      //       isNotExist = false;
      //       temp['qty'] += 1;
      //       updateCart(temp, setState, setData);
      //     }
      //   }
      //   //print(isNotExist);
      //   if (isNotExist) {
      //     data['qty'] = 1;
      //     cart.add(data);
      //     updateCart(data, setState, setData);
      //   }
      // }
    } else {
      if (((shippingCosts + data['shipping_cost']) +
              ((total + data["new_price"]) - dsvpBalance)) >=
          cashBalance) {
        //print('.............');
        showDialog(
            context: context,
            builder: (BuildContext context) {
              return Insufficientfunds(
                  content: Text(pageDetails['text5'],
                      style: TextStyle(
                          fontSize: Pallet.normalfont,
                          color: Pallet.fontcolor,
                          fontWeight: Pallet.font500)),
                  onpress: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => Home(
                                  route: 'add_funding',
                                )));
                  },
                  onpress1: () {
                    Navigator.of(context).pop();
                  });
            });
      } else {
        await showDialog(
            context: context,
            barrierDismissible: false,
            builder: (context) {
              return StatefulBuilder(builder: (BuildContext context,
                  void Function(void Function()) setState1) {
                return AlertDialog(
                  backgroundColor: Pallet.inner1,
                  title: Row(
                    children: [
                      Image.asset(
                        "c-logo.png",
                        height: 30,
                      ),
                      SizedBox(
                        width: 10,
                      ),
                      Text(
                        pageDetails['text21'],
                        style: TextStyle(color: Pallet.fontcolor),
                      ),
                    ],
                  ),
                  content: Container(
                    width: 400,
                    // height: 340,
                    child: SingleChildScrollView(
                      child: ListBody(
                        children: [
                          Container(
                            width: double.infinity,
                            padding: EdgeInsets.symmetric(
                              vertical: 10,
                              horizontal: 5,
                            ),
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(5),
                                color: Pallet.fontcolor),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  pageDetails['text7'],
                                  style: TextStyle(
                                    fontSize: 15,
                                    color: Pallet.fontcolornew,
                                    fontWeight: Pallet.font600,
                                  ),
                                ),
                                SizedBox(
                                  height: 3,
                                ),
                                Text(
                                  dsvpBalance.toString(),
                                  style: TextStyle(
                                    color: Pallet.fontcolornew,
                                    fontWeight: Pallet.font600,
                                  ),
                                ),
                              ],
                            ),
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          Container(
                            width: double.infinity,
                            padding: EdgeInsets.symmetric(
                              vertical: 10,
                              horizontal: 5,
                            ),
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(5),
                                color: Pallet.fontcolor),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  pageDetails['text22'],
                                  style: TextStyle(
                                    fontSize: 15,
                                    color: Pallet.fontcolornew,
                                    fontWeight: Pallet.font600,
                                  ),
                                ),
                                Text(
                                  cashBalance.toString(),
                                  style: TextStyle(
                                    color: Pallet.fontcolornew,
                                    fontWeight: Pallet.font600,
                                  ),
                                ),
                                Text(pageDetails['text23'],
                                    style: TextStyle(
                                      color: Pallet.fontcolornew,
                                    )),
                                SizedBox(height: 5),
                                TextFormField(
                                  controller: dsvpcontroller,
                                  textAlign: TextAlign.left,
                                  style: TextStyle(
                                      color: Pallet.dashcontainerback),
                                  cursorColor: Pallet.dashcontainerback,
                                  onChanged: (value) {
                                    if (value.isEmpty) {
                                      setState1(() {
                                        dsvpError = pageDetails['text11'];
                                      });
                                    } else if (double.parse((value)) >
                                        (cashBalance)) {
                                      setState1(() {
                                        dsvpError = pageDetails['text12'];
                                      });
                                    } else if (double.parse((value)) <=
                                        (cashBalance)) {
                                      setState1(() {
                                        dsvpError = null;
                                      });
                                    }
                                  },
                                  keyboardType: TextInputType.numberWithOptions(
                                      decimal: true),
                                  inputFormatters: <TextInputFormatter>[
                                    FilteringTextInputFormatter.digitsOnly,
                                  ],
                                  decoration: InputDecoration(
                                    errorText: dsvpError,
                                    border: OutlineInputBorder(
                                      borderSide: BorderSide(
                                          color: Pallet.dashcontainerback,
                                          width: 2),
                                    ),
                                    focusedBorder: OutlineInputBorder(
                                        borderSide: BorderSide(
                                            color: Pallet.dashcontainerback,
                                            width: 2),
                                        borderRadius: BorderRadius.circular(
                                            Pallet.radius)),
                                    enabledBorder: OutlineInputBorder(
                                        borderSide: BorderSide(
                                            color: Pallet.dashcontainerback,
                                            width: 2),
                                        borderRadius: BorderRadius.circular(
                                            Pallet.radius)),
                                    errorBorder: OutlineInputBorder(
                                        borderSide: BorderSide(
                                            color: Colors.red, width: 2),
                                        borderRadius: BorderRadius.circular(
                                            Pallet.radius)),
                                    focusedErrorBorder: OutlineInputBorder(
                                        borderSide: BorderSide(
                                            color: Colors.red, width: 2),
                                        borderRadius: BorderRadius.circular(
                                            Pallet.radius)),
                                    filled: true,
                                  ),
                                ),
                              ],
                            ),
                          ),
                          SizedBox(
                            height: 30,
                          ),
                          AbsorbPointer(
                            absorbing: (cashBalance) <= 0 ? true : false,
                            child: CustomButton(
                              vpadding: 10,
                              text: pageDetails['text24'],
                              onpress: () async {
                                if (dsvpcontroller.text == null ||
                                    dsvpcontroller.text == '') {
                                  setState1(() {
                                    dsvpError = pageDetails['text15'];
                                  });
                                } else if (dsvpError == null) {
                                  if (dsvpcontroller.text != null &&
                                      (cashBalance) > 0) {
                                    double amount =
                                        double.parse(dsvpcontroller.text);
                                    //print(amount);

                                    setState1(() {
                                      cashBalance = (cashBalance - amount);

                                      dsvpBalance = (dsvpBalance + amount);

                                      dsvpcontroller.text = "";
                                    });
                                    Map<String, dynamic> _map = {
                                      "payload": {
                                        "product_id": 1,
                                        "is_dsv_p": true,
                                        // "split_type": 'none',
                                        "total": amount,
                                        "cash_account_used": amount,
                                        "trading_account_used": 0,
                                        "packages": [],
                                      }
                                    };
                                    print('EEEEEEE');
                                    print(_map);
                                    Map<String, dynamic> _result =
                                        await HttpRequest.Post(
                                            'buyPackages', _map);
                                    if (Utils.isServerError(_result)) {
                                      Navigator.of(context).pop();
                                      if (_result['response']['error'] ==
                                          'kyc_not_verified') {
                                        snack.snack(title: 'KYC Not Verified');
                                      } else {
                                        snack.snack(
                                            title: pageDetails['text16']);
                                      }
                                      // final snackBar = SnackBar(
                                      //     content: Text(pageDetails['text16']));
                                      // ScaffoldMessenger.of(context)
                                      //     .showSnackBar(snackBar);
                                      return await Utils.getMessage(
                                          _result['response']['error']);
                                    } else {
                                      getDsvPData();
                                      Navigator.of(context).pop();
                                      return showDialog(
                                          barrierDismissible: false,
                                          context: context,
                                          builder: (BuildContext context) {
                                            return AlertDialog(
                                              backgroundColor:
                                                  Pallet.popupcontainerback,
                                              shape: RoundedRectangleBorder(
                                                  borderRadius:
                                                      BorderRadius.circular(
                                                          Pallet.radius)),
                                              title: Row(
                                                mainAxisAlignment:
                                                    MainAxisAlignment.start,
                                                children: [
                                                  Image.asset("c-logo.png",
                                                      width: 30),
                                                  SizedBox(width: 10),
                                                  Expanded(
                                                    child: Text(
                                                        pageDetails['text17'],
                                                        style: TextStyle(
                                                            fontSize: Pallet
                                                                    .normalfont +
                                                                10,
                                                            color: Pallet
                                                                .fontcolor,
                                                            fontWeight: Pallet
                                                                .font500)),
                                                  ),
                                                ],
                                              ),
                                              content: Container(
                                                height: 150,
                                                child: Column(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment
                                                          .spaceEvenly,
                                                  children: [
                                                    CircleAvatar(
                                                      backgroundColor:
                                                          Colors.white,
                                                      radius: 30,
                                                      child: Icon(Icons.done,
                                                          color: Pallet
                                                              .dashcontainerback,
                                                          size: 40),
                                                    ),
                                                    Text(
                                                        pageDetails['text18'] +
                                                            ': ' +
                                                            dsvpcontroller.text,
                                                        style: TextStyle(
                                                            fontSize: Pallet
                                                                    .normalfont +
                                                                5,
                                                            color: Pallet
                                                                .fontcolor,
                                                            fontWeight: Pallet
                                                                .font500)),
                                                    Text(
                                                        pageDetails['text19'] +
                                                            ': ' +
                                                            dsvpBalance
                                                                .toString(),
                                                        style: TextStyle(
                                                            fontSize: Pallet
                                                                    .normalfont +
                                                                5,
                                                            color: Pallet
                                                                .fontcolor,
                                                            fontWeight:
                                                                Pallet.font500))
                                                  ],
                                                ),
                                              ),
                                              actions: [
                                                PopupButton(
                                                  text: pageDetails['text25'],
                                                  onpress: () {
                                                    setState(() {
                                                      add({
                                                        'product_id':
                                                            data['products']
                                                                ['product_id'],
                                                        'product_name': data[
                                                                'products']
                                                            ['product_name'],
                                                        'new_price':
                                                            data['products']
                                                                ['new_price'],
                                                        'price':
                                                            data['products']
                                                                ['price'],
                                                        'shipping_cost': data[
                                                                'products']
                                                            ['shipping_cost'],
                                                        'variant_id':
                                                            data['products']
                                                                ['variant_id'],
                                                        // 'image_url': data['products']['images'][0]['image_url']
                                                      }, setState, setData,
                                                          context);
                                                    });
                                                  },
                                                ),
                                              ],
                                            );
                                          });
                                    }
                                  } else if ((cashBalance) <= 0) {
                                    setState1(() {
                                      dsvpError = pageDetails['text12'];
                                    });
                                  }
                                }
                              },
                              fontweight: Pallet.font600,
                            ),
                          ),
                          SizedBox(
                            height: 15,
                          ),
                          Text(
                            pageDetails['text26'],
                            style: TextStyle(
                              color: Pallet.fontcolor,
                              fontSize: 11,
                            ),
                          ),
                          SizedBox(
                            height: 9,
                          ),
                          AbsorbPointer(
                            absorbing: (cashBalance) <= 0 ? false : true,
                            child: CustomButton(
                              vpadding: 10,
                              text: pageDetails['text27'],
                              onpress: () {
                                Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) =>
                                          Home(route: 'add_funding')),
                                );
                              },
                              fontweight: Pallet.font600,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  actions: [
                    PopupButton(
                      text: pageDetails['text3'],
                      onpress: () {
                        Navigator.of(context).pop();
                        // Navigator.of(context).pop();
                      },
                    ),
                    SizedBox(height: 10),
                  ],
                );
              });
            });
        // }
        // else {
        //   //print('yyyyyyyyyyyyyyyy');
        //   showDialog(
        //       context: context,
        //       builder: (BuildContext context) {
        //         return AlertDialog(
        //             title: Text('Addfund'),
        //             content: Text('AddFunding'),
        //             actions: [
        //               PopupButton(
        //                   text: 'addfund',
        //                   onpress: () {
        //                     Navigator.push(
        //                         context,
        //                         MaterialPageRoute(
        //                             builder: (context) => Home(
        //                                   route: 'add_funding',
        //                                 )));
        //                   })
        //             ]);
        //       });
        // }
      }
    }
  }

  dismiss(int idx, setState, setData) {
    Map<String, dynamic> _map = cart[idx];
    cart[idx]['qty'] = 0;
    cart.removeAt(idx);
    updateCart(_map, setState, setData);
  }

  sub(int idx, setState, setData) async {
    Map<String, dynamic> _map = cart[idx];
    if (_map['qty'] > 1) {
      cart[idx]['qty'] -= 1;
      updateCart(cart[idx], setState, setData);
    } else {
      cart.removeAt(idx);
      _map['qty'] = 0;
      updateCart(_map, setState, setData);
    }
  }

  updateCart(Map<String, dynamic> data, setState, setData) async {
    Map<String, dynamic> _map = {};
    _map = {
      "product_id": 1,
      "variant_id": data['variant_id'],
      "product_id_fs": data['product_id'],
      "qty": data['qty']
    };
    Map<String, dynamic> result =
        await HttpRequest.Post('addToCart', Utils.constructPayload(_map));
    myCart(setState, setData);
    startAnimate(setState, setData);
    if (Utils.isServerError(result)) {
    } else {}
  }

  Future<String> myCart(setState, setData) async {
    Map<String, dynamic> result =
        await HttpRequest.Post('myCart', Utils.getSiteID());
    if (Utils.isServerError(result)) {
      throw (await Utils.getMessage(result['response']['error']));
    } else {
      setData(() {
        setState(() {
          totalCount = result['response']['data']['overall_count'];
          total = result['response']['data']['total'];
          shippingCosts = result['response']['data']['shipping_costs'];
          cart = result['response']['data']['products'];
        });
      });

      return "start";
    }
  }
}

Cart cart = Cart();

class Fund {
  List<String> _paymentmode = [
    "advcash",
    "asiapay",
    "aaplus",
    "quantumClearance",
    "bankwire"
  ];
  // Map<String, dynamic> notemap = {
  //   'quantumClearance':
  //       '''You can pay to your cash wallet through Credit card payment. When your payment has been received by the company and fully confirmed we will credit your E-Wallet with the amount you transferred Supported Currencies : USD, EUR, GBP, CHF''',
  //   'aaplus':
  //       '''Your payment will be deferred and incomplete if you make incorrect payment. You can simply copy and paste the address & Crypto amount to avoid errors. Please do not close the page until the payment process is finished. Once the payment is complete, you will automatically see the confirmation message.''',
  //   'asiapay':
  //       '''You can pay to your cash wallet through Asiapay. When your payment has been received by the company and fully confirmed we will credit your E-Wallet with the amount you transferred.Supported Currencies : MYR, THB, VND, IDR''',
  //   'advcash':
  //       ''' You can pay to your cash wallet through Advcash. When your payment has been received by the company and fully confirmed we will credit your E-Wallet with the amount you transferred.''',
  //   'bankwire':
  //       '''Please email finance@ducatus.net with your confirmation info. When your payment has been received by the company and fully confirmed we will credit your E-Wallet with the amount you transferred.''',
  // };

  String selectedGeteway = "quantumClearance";
  String selectedDisplayName = "Credit Card";
  String code;
  String selectedCurrency = "";
  String selectedBank = "";
  String selectedCode = "";
  Map<String, dynamic> pmDetails = {};
  List bankMap = [];
  Map<String, dynamic> pDetails = {};
  List<Map> paymentGateway = [];
  List<Map> paymentMethods = [];

  List<String> currencies = <String>[];
  List<String> banks = <String>[];
  bool value123 = false;
  TextEditingController amount = TextEditingController();
  // ignore: non_constant_identifier_names
  TextEditingController convertion_amount = TextEditingController();
  TextEditingController currency = TextEditingController();
  TextEditingController bank = TextEditingController();
  String amountError;
  // ignore: non_constant_identifier_names
  String currency_after_selection;
  TextEditingController note = TextEditingController();

  String errortext = '';

  selectGateway(String gateway) {
    //print(gateway);
    currencies = [];
    banks = [];
    convertion_amount.text = '';
    currency.text = '';
    bank.text = '';
    selectedCurrency = "";
    selectedBank = "";
    selectedCode = "";
    note.text = "";
    amountError = null;
    currency_after_selection = '';
    errortext = '';

    for (var pmdata in paymentMethods) {
      if (pmdata['payment_gateway_name'] == gateway) {
        pmDetails = pmdata;
        selectedGeteway = pmdata['payment_gateway_name'];
        selectedDisplayName = pmdata['display_name'];
        if (pmdata['payment_gateway_name'] != "bankwire") {
          for (var currency in pmdata['currencies']) {
            currencies.add(currency['currency_code']);
          }
        }
        if (selectedGeteway == "asiapay") {
          print("object123d");
          print(currencies);
          selectedCurrency = currencies[0];
          currency_after_selection = selectedCurrency;
          print(selectedCurrency);
          selectCurrency(selectedCurrency);
          print(banks);
          selectedBank = banks[0];
          selectBank(selectedBank);
        } else if (selectedGeteway == "quantumClearance") {
          print("object123d");
          print(currencies);
          selectedCurrency = currencies[0];
          currency_after_selection = selectedCurrency;
          print(selectedCurrency);
          selectCurrency_convertion(selectedCurrency);
        } else if (selectedGeteway == "aaplus") {
          print("select currencies");
          print(currencies);
          selectedCurrency = currencies[0];
          currency_after_selection = selectedCurrency;
          print(selectedCurrency);
          selectCurrency(selectedCurrency);
        }
      }
    }
  }

  selectCurrency(String currency) {
    banks = [];
    errortext = '';
    selectedCurrency = currency;
    bank.text = '';
    if (selectedGeteway == "asiapay") {
      for (var pmdata in pmDetails["currencies"]) {
        if (pmdata['currency_code'] == selectedCurrency) {
          bankMap = pmdata["banks"];
          for (var bank in bankMap) {
            banks.add(bank["bank_name"]);
          }
          selectedBank = banks[0];
        }
      }
    }
  }

  // ignore: non_constant_identifier_names
  selectCurrency_convertion(String currency) {
    if (selectedGeteway == "asiapay" || selectedGeteway == "quantumClearance") {
      print("hello");
      print(currency);
      print(pmDetails["currencies"]);
      selectedCurrency = currency;
      for (var pmdata in pmDetails["currencies"]) {
        if (pmdata['currency_code'] == selectedCurrency) {
          print(pmdata['conversion_price']);
          print(amount.text);
          print(amount.text.isEmpty);
          if (amount.text.isNotEmpty) {
            double temp = double.parse(amount.text);
            double temp2 = double.parse(pmdata['conversion_price']);
            // ignore: non_constant_identifier_names
            double temp_final = temp * temp2;
            convertion_amount.text = temp_final.toString();
            print(temp_final);
          } else {
            print('else');
            double temp = 0;
            double temp2 = double.parse(pmdata['conversion_price']);
            // ignore: non_constant_identifier_names
            double temp_final = temp * temp2;
            convertion_amount.text = temp_final.toString();
            print('*********************************');
            print(convertion_amount.text);
          }
        }
      }
    }
  }

  selectBank(String bank) {
    selectedBank = bank;
    errortext = '';
    if (selectedGeteway == "asiapay") {
      for (var data in bankMap) {
        if (data["bank_name"] == bank) {
          selectedCode = data["bank_code"];
        }
      }
    }
  }

  // selectBankCrypto(String bank) {
  //   selectedBank = bank;
  //   errortext = '';
  //   if (selectedGeteway == "aaplus") {
  //     for (var data in bankMap) {
  //       if (data["bank_name"] == bank) {
  //         selectedCode = data["bank_code"];
  //       }
  //     }
  //   }
  // }

  createPayment(context) async {
    print('*********************************');
    /**aaplus
asiapay
advcash
bankwire */
    //print("selected gateway : " + selectedGeteway);
    //print("amount : " + amount.text);
    //print("selectedCode : " + selectedCode);
    //print("selectedCurrency : " + selectedCurrency);
    //print("note.text : " + note.text);
    bool isValid = true;
    amountError = null;

    if (selectedGeteway == "quantumClearance") {
      print('**---------------------***');

      if (amount.text.length < 1) {
        isValid = false;
        amountError = pageDetails['text28'];
      } else if (selectedCurrency.length < 1) {
        isValid = false;
        errortext = pageDetails['text28'];
      }
      //  else {
      //   BuildContext dialogContext;
      //   showDialog(
      //       context: context,
      //       builder: (BuildContext context) {
      //         dialogContext = context;
      //         return AlertDialog(
      //           elevation: 24.0,
      //           backgroundColor: Pallet.popupcontainerback,
      //           shape: RoundedRectangleBorder(
      //               borderRadius: BorderRadius.all(Radius.circular(15.0))),
      //           title: Column(
      //             children: [
      //               Center(
      //                 child: Text("Don't Close the Browser Window..!",
      //                     style: TextStyle(
      //                         fontSize: Pallet.heading1,
      //                         color: Pallet.fontcolor,
      //                         fontWeight: Pallet.font500)),
      //               ),
      //             ],
      //           ),
      //           content: Container(
      //             width: 450.0,
      //             child: SingleChildScrollView(
      //               child: ListBody(
      //                 children: <Widget>[
      //                   // cryptopopup(),
      //                 ],
      //               ),
      //             ),
      //           ),
      //         );
      //       });
      // }
    } else if (selectedGeteway == "aaplus") {
      print('**0000000000000000000***');

      if (amount.text.length < 1) {
        isValid = false;
        amountError = pageDetails['text28'];
      } else if (selectedCurrency.length < 1) {
        isValid = false;
        errortext = pageDetails['text28'];
      }
    } else if (selectedGeteway == "asiapay") {
      if (amount.text.length < 1) {
        isValid = false;
        amountError = pageDetails['text28'];
      } else if (selectedCurrency.length < 1) {
        isValid = false;
        errortext = pageDetails['text28'];
      }
    } else if (selectedGeteway == "advcash") {
      if (amount.text.length < 1) {
        isValid = false;
        amountError = pageDetails['text28'];
      }
    }
    if (isValid) {
      print('#######################');

      Map<String, dynamic> data = {
        "product_id": 1,
        "paymentMethod": selectedGeteway,
        "amount": amount.text,
        "order_id": 0,
        "bankCode": selectedCode,
        "type": "null",
        "paymentCurrencyCode": selectedCurrency,
        "txnNote": note.text
      };
// Map<String, dynamic> result =
//           await HttpRequest.Post('createPayment', Utils.constructPayload(data));
      BuildContext dialogContext;
      if (selectedGeteway == "advcash" ||
          selectedGeteway == "quantumClearance" ||
          selectedGeteway == "asiapay" ||
          selectedGeteway == "aaplus") {
        showDialog(
            barrierDismissible: false,
            context: context,
            builder: (BuildContext context) {
              dialogContext = context;
              return AlertDialog(
                elevation: 24.0,
                backgroundColor: Pallet.popupcontainerback,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.all(Radius.circular(15.0))),
                title: Container(
                  width: 50,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      CircularProgressIndicator(
                          valueColor:
                              AlwaysStoppedAnimation<Color>(Colors.white)),
                      SizedBox(
                        height: 10,
                      ),
                      Text("Please Wait...",
                          style: TextStyle(
                              fontSize: Pallet.heading3,
                              color: Pallet.fontcolor,
                              fontWeight: Pallet.font500)),
                    ],
                  ),
                ),
              );
            });
      }
      Map<String, dynamic> result =
          await HttpRequest.Post('createPayment', Utils.constructPayload(data));
      print('DATA: ${data}');
      if (Utils.isServerError(result)) {
        Navigator.pop(dialogContext);
        snack.snack(title: pageDetails['text16']);
        // snack.snack(title: result['response']['error']);

        // ScaffoldMessenger.of(context).showSnackBar(
        //   SnackBar(
        //       content: Text(
        //     pageDetails['text16'],
        //   )),
        // );
        return await Utils.getMessage(result['response']['error']);
      } else {
        pDetails = result['response']['data'];
        if (selectedGeteway == "advcash" ||
            selectedGeteway == "quantumClearance") {
          if (await Utils.fromJSONString(
                  await Utils.getSharedPrefereces('data'))['ismobile'] ==
              true) {
            Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) => Website(url: pDetails["url"])),
            );
          } else {
            //print('object123467');

            Navigator.pop(dialogContext);
            launch(pDetails["url"]);

            // dialogContext
            // html.window.location.href = pDetails["url"];
          }
        } else if (selectedGeteway == "asiapay") {
          String url = appSettings['SERVER_URL'] +
              "/static/asiapay?Merchant=" +
              pDetails['paymentDetails']['Merchant'] +
              "&Currency=" +
              pDetails['paymentDetails']['Currency'] +
              "&Customer=" +
              pDetails['paymentDetails']['Customer'] +
              "&Reference=" +
              pDetails['paymentDetails']['Reference'] +
              "&Key=" +
              pDetails['paymentDetails']['Key'] +
              "&Amount=" +
              pDetails['paymentDetails']['Amount'] +
              "&Note=" +
              pDetails['paymentDetails']['Note'] +
              "&Datetime=" +
              pDetails['paymentDetails']['Datetime'] +
              "&FrontURI=" +
              pDetails['paymentDetails']['FrontURI'] +
              "&BackURI=" +
              pDetails['paymentDetails']['BackURI'] +
              "&Language=" +
              pDetails['paymentDetails']['Language'] +
              "&Bank=" +
              pDetails['paymentDetails']['Bank'] +
              "&ClientIP=" +
              pDetails['paymentDetails']['ClientIP'] +
              "&url=" +
              pDetails['url'] +
              "";
          if (await Utils.fromJSONString(
                  await Utils.getSharedPrefereces('data'))['ismobile'] ==
              true) {
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => Website(url: url)),
            );
          } else {
            Navigator.pop(dialogContext);
            launch(url);
          }
        } else if (selectedGeteway == "aaplus") {
          Navigator.pop(dialogContext);
          showDialog(
              context: context,
              builder: (BuildContext context) {
                return AlertDialog(
                  elevation: 24.0,
                  backgroundColor: Pallet.popupcontainerback,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.all(Radius.circular(15.0))),
                  title: Row(
                    children: [
                      Image.asset("c-logo.png", width: 40),
                      SizedBox(width: 10),
                      Text("Crypto",
                          style: TextStyle(
                              fontSize: 15,
                              color: Pallet.fontcolor,
                              fontWeight: Pallet.font500)),
                    ],
                  ),
                  content: Container(
                    width: 450.0,
                    child: SingleChildScrollView(
                      child: ListBody(
                        children: <Widget>[
                          cryptopopup(),
                        ],
                      ),
                    ),
                  ),
                  actions: [
                    PopupButton(
                      onpress: () {
                        Navigator.of(context).pop();
                      },
                      text: 'Close',
                      buttoncolor: Pallet.fontcolor,
                      textcolor: Pallet.fontcolornew,
                    ),
                    SizedBox(height: 10),
                  ],
                );
              });
        }
        return Utils.toJSONString(
            await Utils.convertMessage(result['response']['data']));
      }
    }
  }

  Widget cryptopopup({double popheading, context}) {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Text('Amount' + ' ' + selectedCurrency,
              style: TextStyle(color: Colors.white, fontSize: 15)),
          SizedBox(height: 10),
          Container(
              decoration: BoxDecoration(
                  color: Pallet.fontcolor,
                  borderRadius: BorderRadius.circular(5)),
              padding: EdgeInsets.symmetric(horizontal: 15, vertical: 10),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Expanded(
                    flex: 8,
                    child: Container(
                      width: 100,
                      child: Text(pDetails['paymentDetails']["amount"],
                          style: TextStyle(
                              color: Pallet.fontcolornew, fontSize: 14)),
                    ),
                  ),
                  Expanded(
                    flex: 2,
                    child: InkWell(
                      child: Icon(
                        Icons.copy,
                        color: Pallet.fontcolornew,
                      ),
                      onTap: () {
                        print('HHHHHHHH');
                        snack.snack(title: 'Message Copied');

                        // Get.snackbar('Copied', '',
                        //     colorText: Pallet.fontcolor,
                        //     backgroundColor: Pallet.snackback,
                        //     snackPosition: SnackPosition.TOP);

                        // final snackBar = SnackBar(content: Text('Copied'));
                        // ScaffoldMessenger.of(context).showSnackBar(snackBar);
                        // ScaffoldMessenger.of(context)
                        //     .showSnackBar(SnackBar(content: Text("Copied")));
                        Clipboard.setData(new ClipboardData(
                            text: pDetails['paymentDetails']["amount"]));
                      },
                    ),
                  )
                ],
              )),
          SizedBox(height: 10),
          Text('Scan QR Code',
              style: TextStyle(color: Colors.white, fontSize: 15)),
          SizedBox(height: 10),
          Center(
            child: Container(
              width: 150,
              height: 150,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(3),
                color: Pallet.fontcolor,
              ),
              child: QrImage(
                data: pDetails['paymentDetails']["walletAddress"],
                version: QrVersions.auto,
                size: 150.0,
              ),
            ),
          ),
          SizedBox(height: 10),
          Container(
            width: double.infinity,
            padding: EdgeInsets.all(5),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(3),
              color: Pallet.fontcolor,
            ),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text('Disclaimer:',
                    style: TextStyle(
                        color: Pallet.fontcolornew,
                        fontSize: 15,
                        fontWeight: Pallet.heading1wgt)),
                SizedBox(height: 10),
                Text(
                    'Send only' +
                        ' ' +
                        selectedCurrency +
                        ' ' +
                        'to this address. Sending any other digital asset, would result in permanent loss.',
                    style: TextStyle(color: Pallet.fontcolornew, fontSize: 15)),
                SizedBox(height: 10),
              ],
            ),
          ),
          SizedBox(height: 10),
          Text('Wallet Address',
              style: TextStyle(color: Colors.white, fontSize: 15)),
          SizedBox(height: 10),
          Container(
              decoration: BoxDecoration(
                  color: Pallet.fontcolor,
                  borderRadius: BorderRadius.circular(5)),
              padding: EdgeInsets.symmetric(horizontal: 15, vertical: 10),
              child: Row(
                // mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Expanded(
                    flex: 9,
                    child: Text(pDetails['paymentDetails']["walletAddress"],
                        // maxLines: 1,
                        // overflow: TextOverflow.ellipsis,
                        style: TextStyle(
                            color: Pallet.fontcolornew, fontSize: 14)),
                  ),
                  Expanded(
                    flex: 1,
                    child: InkWell(
                      child: Icon(
                        Icons.copy,
                        color: Pallet.fontcolornew,
                      ),
                      onTap: () {
                        Clipboard.setData(
                          new ClipboardData(
                              text: pDetails['paymentDetails']
                                  ["walletAddress"]),
                        );
                        snack.snack(title: 'Message Copied');

                        // final snackBar = SnackBar(content: Text("Copied"));
                        // ScaffoldMessenger.of(context).showSnackBar(snackBar);
                      },
                    ),
                  ),
                  // SizedBox(width: 5),
                  // Row(
                  //   mainAxisAlignment: MainAxisAlignment.end,
                  //   children: [

                  //   ],
                  // )
                ],
              ))
        ],
      ),
    );
  }

  Future<String> confirmPayment() async {
    Map<String, dynamic> data = Utils.getSiteID();
    data['payload']['referenceId'] = pDetails['referenceId'];
    Map<String, dynamic> result =
        await HttpRequest.Post('createPayment', Utils.constructPayload(data));
    if (Utils.isServerError(result)) {
      return await Utils.getMessage(result['response']['error']);
    } else {
      return Utils.toJSONString(
          await Utils.convertMessage(result['response']['data']));
    }
  }

  Future<String> getPaymetMethods() async {
    Map<String, dynamic> result =
        await HttpRequest.Post('paymentMethods', Utils.getSiteID());

    if (Utils.isServerError(result)) {
      return throw (await Utils.getMessage(result['response']['error']));
    } else {
      print('addddddddddddddddddddd');
      print(result['response']['data']);
      for (var p_method in result['response']['data']['paymentMethods']) {
        if (_paymentmode.contains(p_method['payment_gateway_name'])) {
          paymentMethods.add(p_method);
          paymentGateway.add({
            "dname": p_method["display_name"],
            "name": p_method["payment_gateway_name"],
            "logo": p_method["payment_gateway_logo"]
          });
        }
      }
      selectGateway('bankwire');
      if (selectedGeteway == "quantumClearance") {
        print("1wqeqwe23");
        print(currencies);
        selectedCurrency = currencies[0];
      }
      if (selectedGeteway == "asiapay") {
        print("1sdfsdf3");
        print(currencies);
        selectedCurrency = currencies[0];
      } else {
        print("1sddfdsfsdf3");
      }
      return Utils.toJSONString(
          await Utils.convertMessage(result['response']['data']));
    }
  }

  Future<void> getCurrencies() async {
    for (var pmdata in paymentMethods) {
      if (pmdata['payment_gateway_name'] == selectedGeteway) {
        for (var item in pmdata['currencies']) {
          currencies.add(item['currency_code']);
        }
      }
    }
  }
}

Fund fund = Fund();

class Packages {
  List cart = [];
  int total = 0;

  List packages;
  List tempList = [];

  double cashBal;
  double tradeBal;

  TextEditingController cashAccount = TextEditingController();
  TextEditingController tradAccount = TextEditingController();

  bool cashSelected = true;
  bool tradeSelected = false;

  autofill({String value, bool isCash}) {
    try {
      if (cashBal + tradeBal >= total) {
        // ignore: unused_local_variable
        double cashUsed = 0;
        // ignore: unused_local_variable
        double tradeUsed = 0;
        double adder = 0;
        if (cashAccount.text.length > 0) {
          cashUsed = double.parse(cashAccount.text);
        }
        if (tradAccount.text.length > 0) {
          tradeUsed = double.parse(tradAccount.text);
        }

        if (value == null) {
          cashAccount.text = "";
          tradAccount.text = "";
          if (cashSelected && tradeSelected) {
            if (cashBal >= total) {
              cashAccount.text = total.toString();
            } else {
              cashAccount.text = cashBal.toString();
              adder = total - cashBal;
              tradAccount.text = adder.toString();
            }
          } else if (cashSelected) {
            if (cashBal >= total) {
              cashAccount.text = total.toString();
            } else {
              tradeSelected = true;
              cashAccount.text = cashBal.toString();
              adder = total - cashBal;
              tradAccount.text = adder.toString();
            }
          } else if (tradeSelected) {
            if (tradeBal >= total) {
              tradAccount.text = total.toString();
            } else {
              cashSelected = true;
              tradAccount.text = tradeBal.toString();
              adder = total - tradeBal;
              cashAccount.text = adder.toString();
            }
          }
        } else {
          double amount = 0;
          if (value.length > 0) {
            amount = double.parse(value);
          }
          //print('amount : $amount');
          //print('cash used : $cashUsed');
          //print('trade used: $tradeUsed');
          //print('total : $total');
          // if (cashUsed + tradeUsed <= total) {
          //print('check');
          if (isCash == true) {
            if (tradeBal >= total - amount) {
              if (cashBal >= amount) {
                adder = total - amount;
                if (adder < 0) {
                  //print('chance of negative funds in trade');
                } else {
                  tradAccount.text = adder.toString();
                }
              } else {
                //print("amount is greater than ur cash balance");
              }
            } else {
              //print("amount is greater than ur trade balance");
            }
          } else {
            if (cashBal >= total - amount) {
              if (tradeBal >= amount) {
                adder = total - amount;
                if (adder < 0) {
                  //print('chance of negative funds in cash');
                } else {
                  cashAccount.text = adder.toString();
                }
              } else {
                //print("amount is greater than ur trade balance");
              }
            } else {
              //print("amount is greater than ur cash balance");
            }
          }
          // }
        }
      }
      // ignore: unused_catch_stack
    } catch (e, trace) {
      //print('here');
      //print(e);
      //print(trace);
    }
  }

  Future<String> getPackages() async {
    Map<String, dynamic> result =
        await HttpRequest.Post('addFundingInfo', Utils.getSiteID());
    if (Utils.isServerError(result)) {
      return throw (await Utils.getMessage(result['response']['error']));
    } else {
      Map response = result['response']['data'];
      //print(response);
      packages = response["packages"];

      tempList = packages.sublist(0, 3);
      cashBal = double.parse(response["cash_balance"]);
      tradeBal = double.parse(response["trading_balance"]);

      return Utils.toJSONString(
          await Utils.convertMessage(result['response']['data']));
    }
  }

  add(Map package, context) {
    bool exists = false;
    int rt = total + package["price"];
    if (cashBal + tradeBal >= rt) {
      if (cart.length > 0)
        for (var i = 0; i < cart.length; i++) {
          if (cart[i]["package_id"].toString() ==
              package["package_id"].toString()) {
            cart[i]["qty"] += 1;
            exists = true;
            total += package["price"];
          }
        }
      if (exists == false) {
        package["qty"] == null
            ? package["qty"] = 1
            : package["qty"] = package["qty"];

        cart.insert(cart.length, package);
        total += package["price"];
      }
      autofill(value: null);
    } else {
      //print(
      // '*******************************************************************ASDFASDFASDFASDFAS*******************************************************************************');
      showAlertDialog(context);
    }
  }

  showAlertDialog(BuildContext context) {
    // set up the button
    // ignore: deprecated_member_use
    Widget okButton = PopupButton(
      onpress: () {
        Navigator.push(context,
            MaterialPageRoute(builder: (context) => Home(route: 'Get DSV')));
      },
      text: "Add Fund",
      buttoncolor: Pallet.fontcolor,
      textcolor: Pallet.fontcolornew,
    );
    // ignore: deprecated_member_use
    Widget cancl = PopupButton(
      onpress: () {
        Navigator.of(context).pop();
      },
      text: "Cancel",
      buttoncolor: Pallet.fontcolor,
      textcolor: Pallet.fontcolornew,
    );
    AlertDialog alert = AlertDialog(
      title: Text("Insufficient Funds"),
      content: Text("Please Add Funds..."),
      actions: [okButton, cancl],
    );

    // show the dialog
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }

  sub(Map package) {
    for (var i = 0; i < cart.length; i++) {
      if (cart[i]["package_id"] == package["package_id"]) {
        if (cart[i]["qty"] <= 1) {
          cart.remove(package);
          total -= package["price"];
        } else {
          cart[i]["qty"] -= 1;
          total -= package["price"];
        }
      }
    }
    autofill(value: null);
  }

  buyPackages(context) async {
    List tempCart = [];
    // Find the Scaffold in the widget tree and use it to show a SnackBar.
    double cashUsed = 0;
    double tradeUsed = 0;
    if (cashAccount.text.length > 0) {
      cashUsed = double.parse(cashAccount.text);
    }
    if (tradAccount.text.length > 0) {
      tradeUsed = double.parse(tradAccount.text);
    }

    if (cashUsed + tradeUsed >= total) {
      for (var temp in cart) {
        var ram = {"package_id": temp["package_id"], "qty": temp["qty"]};
        tempCart.add(ram);
      }

      var map = {
        "payload": {
          "product_id": 1,
          "cash_account_used": cashUsed,
          "trading_account_used": tradeUsed,
          "packages": tempCart,
        }
      };

      Map<String, dynamic> result = await HttpRequest.Post('buyPackages', map);
      if (Utils.isServerError(result)) {
        snack.snack(title: result['response']['error'].toString());

        // final snackBar = SnackBar(content: Text(result['response']['error']));
        // ScaffoldMessenger.of(context).showSnackBar(snackBar);
        return await Utils.getMessage(result['response']['error']);
      } else {
        cart = [];
        total = 0;
        cashAccount.text = '0';
        tradAccount.text = '0';
        showDialog(
            context: context,
            builder: (_) => SucessPopup(message: 'Get Dsv Successful'));
      }
    }
  }
}
