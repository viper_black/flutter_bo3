import '../../utils/utils.dart' show Utils;
import '../communication/index.dart' show HttpRequest;

class Product {
  Map<String, dynamic> _productDetails = {};
  Map<int, Product> _productCollection = {};
  Product();

  // maintain product related thiings here
  Product setProductDetail(Map<String, dynamic> details) {
    for (var key in details.keys) {
      _productDetails[key] = details[key];
    }
    return this;
  }

  Map<String, dynamic> details() {
    return _productDetails;
  }

  Map<int, Product> all() {
    return _productCollection;
  }

  Product instance(int productId) {
    return _productCollection[productId];
  }

  bool addPackages(List data) {
    for (var package in data) {
      _productCollection[package['package_id']] = setProductDetail(package);
    }
    return true;
  }

  Future<String> getAll(Map<String, dynamic> details) async {
    Map<String, dynamic> result =
     await HttpRequest.Post('productList', Utils.constructPayload(details));
    if (Utils.isServerError(result)) {
      return await Utils.getMessage(result['response']['error']);
    } else {
      for (var prod in result['response']['data']['products']) {
        for (var product in prod['products']) {
          _productCollection[product['product_id']] = setProductDetail(product);
        }
      }
      return Utils.toJSONString(
          await Utils.convertMessage(result['response']['data']));
    }
  }

  Future<String> getPackages() async {
     Map<String, dynamic> result =
        await HttpRequest.Post('addFundingInfo', Utils.getSiteID());
     if (Utils.isServerError(result)) {
      return await Utils.getMessage(result['response']['error']);
    } else {
      // for (var prod in result['response']['data']['products']) {
      //   for (var product in prod['products']) {
      //     _productCollection[product['product_id']] = setProductDetail(product);
      //   }
      // }
      return Utils.toJSONString(
          await Utils.convertMessage(result['response']['data']));
    }
  }

  Future<String> getProduct(int productId) async {
    var data = Utils.getSiteID();
    data['payload']['product_id_fs'] = productId;
    Map<String, dynamic> result = await HttpRequest.Post('singleProduct', data);
    if (Utils.isServerError(result)) {
      return await Utils.getMessage(result['response']['error']);
    } else {
      return Utils.toJSONString(
          await Utils.convertMessage(result['response']['data']));
    }
  }
}
