import 'dart:async';

import 'package:flutter/material.dart';

import '../../config.dart';
import './cart.dart';
import './product.dart';
import '../../utils/utils.dart' show Utils;
import '../communication/index.dart' show HttpRequest;
import 'package:intl/intl.dart';

class Account {
  Map<String, dynamic> _accountDetails = {};

  // have to maintain diff cart for both product and package
  final _packageCart = Cart();
  final _productCart = Cart();
  final _products = Product();
  final _packages = Product();
  Account();

  // handle account details like account balance , account product
  bool setAccountDetail(Map<String, dynamic> details) {
    for (var key in details.keys) {
      _accountDetails[key] = details[key];
    }
    return true;
  }

  getbalance() {}
  setbalance() {}
  Product product() {
    return _products;
  }

  Product package() {
    return _packages;
  }

  Cart productCart() {
    return _productCart;
  }

  Cart pakageCart() {
    return _packageCart;
  }

  Future<String> dashboard() async {
    Map<String, dynamic> result =
        await HttpRequest.Post('dashboard', Utils.getSiteID());
    if (Utils.isServerError(result)) {
      return await Utils.getMessage(result['response']['error']);
    } else {
      setAccountDetail(result['response']['data']);
      return Utils.toJSONString(
          await Utils.convertMessage(result['response']['data']));
    }
  }

  Future<String> statement() async {
    Map<String, dynamic> result =
        await HttpRequest.Post('accountStatement', Utils.getSiteID());
    if (Utils.isServerError(result)) {
      return await Utils.getMessage(result['response']['error']);
    } else {
      return Utils.toJSONString(
          await Utils.convertMessage(result['response']['data']));
    }
  }

  Future<String> bonusStatement(String type) async {
    var data = {
      "payload": {
        "product_id": 1,
        "trans_type": type,
        "limit": 100,
        "off_set": 0
      }
    };
    Map<String, dynamic> result =
        await HttpRequest.Post('bonusStatement', data);
    if (Utils.isServerError(result)) {
      return await Utils.getMessage(result['response']['error']);
    } else {
      return Utils.toJSONString(
          await Utils.convertMessage(result['response']['data']));
    }
  }

  Future<String> network() async {
    Map<String, dynamic> result =
        await HttpRequest.Post('myNetwork', Utils.getSiteID());
    if (Utils.isServerError(result)) {
      return await Utils.getMessage(result['response']['error']);
    } else {
      return Utils.toJSONString(
          await Utils.convertMessage(result['response']['data']));
    }
  }

  Future<String> resetPassword(Map<String, dynamic> data) async {
    Map<String, dynamic> result =
        await HttpRequest.Post('resetPassword', Utils.constructPayload(data));
    if (Utils.isServerError(result)) {
      return await Utils.getMessage(result['response']['error']);
    } else {
      return Utils.toJSONString(
          await Utils.convertMessage(result['response']['data']));
    }
  }

  Future<String> addFundingInfo() async {
    Map<String, dynamic> result =
        await HttpRequest.Post('addFundingInfo', Utils.getSiteID());
    if (Utils.isServerError(result)) {
      return await Utils.getMessage(result['response']['error']);
    } else {
      setAccountDetail(result['response']['data']);
      _packages.addPackages(result['response']['data']['packages']);
      return Utils.toJSONString(
          await Utils.convertMessage(result['response']['data']));
    }
  }
}

class DashboardData {
  int accountType;
  String rankValue;
  String rankName;

  Map<String, dynamic> response;
  double totalPartnersCount = 0,
      totalPackagesCount = 0,
      totalDsvpCount = 0,
      ducprice;
  String username, time, finalDate, userprofile, zoomlink, createdate;
  List<dynamic> assets = [];
  List<dynamic> myteam = [];
  List<dynamic> myteam1 = [], myteam2 = [];

  List<dynamic> myperformance = [];
  List<dynamic> latestmember = [];
  List<dynamic> lastPackages = [];
  List<dynamic> lastPPackages = [];
  List<dynamic> stores = [];
  List<String> typelist = [];
  List month = [
    'jan',
    'feb',
    'mar',
    'apr',
    'may',
    'jun',
    'jul',
    'aug',
    'sep',
    'oct',
    'nov',
    'dec'
  ];
  List monthnumber = [
    {
      'jan': '01',
      'feb': '02',
      'mar': '03',
      'apr': '04',
      'may': '05',
      'jun': '06',
      'jul': '07',
      'aug': '08',
      'sep': '09',
      'oct': '10',
      'nov': '11',
      'dec': '12'
    }
  ];
  List<dynamic> ducmessages = [];
  List<dynamic> myproducts = [];
  List<dynamic> lockinCoins = [];
  Map<String, dynamic> mypriceofcoin = {};
  double mypriceofcoinvalue;

  List<dynamic> mybonus2 = [];
  // String mybonus3;

  List<Map<String, dynamic>> bonuschart = [];
  List<dynamic> multipleAccounts = [];
  List<Map<String, dynamic>> performancechart1 = [];
  List<Map<String, dynamic>> performancechart2 = [];
  List<Map<String, dynamic>> performancechart3 = [];
  DateTime dateParse;
  Map<String, dynamic> dashboardMap = {}, myteamstatus = {}, mybonus = {};

  // ignore: non_constant_identifier_names
  Map<String, dynamic> mybonus3 = {}, team_bv = {};

  Map<String, dynamic> pageDetails = {};
  rankLogo(rankValue) {
    print('RRRRRRRR');
    print(rankValue);
    if (rankValue == '1') {
      return Image.asset('iron.png', width: 50);
    } else if (rankValue == '2') {
      return Image.asset('aluminium.png', width: 50);
    } else if (rankValue == '3') {
      return Image.asset('zinc.png', width: 50);
    } else if (rankValue == '4') {
      return Image.asset('nickel.png', width: 50);
    } else if (rankValue == '5') {
      return Image.asset('copper.png', width: 50);
    } else if (rankValue == '6') {
      return Image.asset('bronze.png', width: 50);
    } else if (rankValue == '7') {
      return Image.asset('silver.png', width: 50);
    } else if (rankValue == '8') {
      return Image.asset('gold.png', width: 50);
    } else if (rankValue == '9') {
      return Image.asset('platinum.png', width: 50);
    } else if (rankValue == '10') {
      return Image.asset('titanium.png', width: 50);
    } else if (rankValue == '11') {
      return Image.asset('senator.png', width: 50);
    } else if (rankValue == '12') {
      return Image.asset('cenurion.png', width: 50);
    } else if (rankValue == '13') {
      return Image.asset('empror.png', width: 50);
    } else {
      Container();
    }
  }

  rankColor(rankValue) {
    print('RRRRRRRR');
    print(rankValue);
    if (rankValue == '1') {
      return Pallet.iron;
    } else if (rankValue == '2') {
      return Pallet.aluminium;
    } else if (rankValue == '3') {
      return Pallet.zinc;
    } else if (rankValue == '4') {
      return Pallet.nickel;
    } else if (rankValue == '5') {
      return Pallet.copper;
    } else if (rankValue == '6') {
      return Pallet.bronze;
    } else if (rankValue == '7') {
      return Pallet.silver;
    } else if (rankValue == '8') {
      return Pallet.gold;
    } else if (rankValue == '9') {
      return Pallet.platinum;
    } else if (rankValue == '10') {
      return Pallet.titanium;
    } else if (rankValue == '11') {
      return Pallet.senator;
    } else if (rankValue == '12') {
      return Pallet.cenurion;
    } else if (rankValue == '13') {
      return Pallet.empror;
    } else if (rankValue == '0') {
      return Colors.blue[100];
    } else {
      return Colors.transparent;
    }
  }

  rankCurrentName(rankName) {
    print('RRRRRRRR');
    print(rankName);
    if (rankName == 'iron'.toUpperCase()) {
      return 'iron rank';
    } else if (rankName == 'aluminium'.toUpperCase()) {
      return 'aluminium rank';
    } else if (rankName == 'zinc'.toUpperCase()) {
      return 'zinc rank';
    } else if (rankName == 'nickel'.toUpperCase()) {
      return 'nickel rank';
    } else if (rankName == 'copper'.toUpperCase()) {
      return 'copper rank';
    } else if (rankName == 'bronze'.toUpperCase()) {
      return 'bronze rank';
    } else if (rankName == 'silver'.toUpperCase()) {
      return 'silver rank';
    } else if (rankName == 'gold'.toUpperCase()) {
      return 'gold rank';
    } else if (rankName == 'platinum'.toUpperCase()) {
      return 'platinum rank';
    } else if (rankName == 'titanium'.toUpperCase()) {
      return 'titanium rank';
    } else if (rankName == 'senator'.toUpperCase()) {
      return 'senator rank';
    } else if (rankName == 'cenurion'.toUpperCase()) {
      return 'cenurion rank';
    } else if (rankName == 'empror'.toUpperCase()) {
      return 'empror rank';
    } else {
      return '';
    }
  }

  dropval(createdate) {
    print("gggggggggggggggg");
    print(month[0]);
    print(month[1]);

    DateTime date = DateTime.now();

    var currentyear = date.year;
    var currentmonth = date.month;

    DateTime createddate = DateTime.parse(createdate);
    typelist.clear();
    while (currentyear >= createddate.year) {
      if (currentmonth > 0) {
        if (currentyear > createddate.year) {
          typelist.add(currentyear.toString() +
              ' ' +
              month[currentmonth - 1].toString().toUpperCase());
        } else if (currentyear == createddate.year) {
          if (currentmonth >= createddate.month) {
            typelist.add(currentyear.toString() +
                ' ' +
                month[currentmonth - 1].toString().toString().toUpperCase());
          }
        } else {
          print(currentmonth >= createddate.month &&
              currentyear <= createddate.year);
        }

        currentmonth--;
      } else if (currentmonth == 0) {
        currentyear -= 1;
        currentmonth = 12;
        print(typelist);
        print("sssssssssss");
      }
    }
  }

  var nxtweekjackpot;
  String todaydate;
  String nextweeksunday;
  Future<String> dashboard() async {
    accountType = int.parse(Utils.fromJSONString(
            await Utils.getSharedPrefereces('data'))['account_type']
        .toString());
    String data = await Utils.getPageDetails('dashboard');
    pageDetails = Utils.fromJSONString(data);

    Map<String, dynamic> result =
        await HttpRequest.Post('dashboard', Utils.getSiteID());
    if (Utils.isServerError(result)) {
      return throw (await Utils.getMessage(result['response']['error']));
    } else {
      response = result['response']['data'];
      await settingskycdata.getKycStatus();
      print('KKKKKKKKKKKKKK');
      print(settingskycdata.settingskyc);
      print('DDDDDDDDDDDD');
      print(response);
      ducprice = response['DUC_PRICE'];
      // print('DUCSSSSSSSSS');
      // print(ducprice);
      nxtweekjackpot = response['jackpot_percentage'];
      todaydate = response['current_date'].toString();
      nextweeksunday = response['next_sunday_date'].toString();
      username = response['user_name'];
      createdate = response['created_at'];
      dropval(response['created_at'].toString());

      zoomlink = response['zoom_link'];
      userprofile = response['profile_image'];
      //print('Profile Inmage');
      //print(userprofile);
      assets = response["assets"];

      myteam = response['team_info']['members'];
      print('QQQQQQQQQQQQQQ');
      print(myteam);
      team_bv = response['team_info'];
      //print('sssssssssss');
      //print(team_bv);
      myteam1 = response["team_info"]['boxes'];
      print('MMMMMMMMMMMMMMMMMMMMMMM');
      print(myteam1);
      myteam2 = response["team_info"]['data'];
      print('FFFFFFFFFFF');
      print(myteam2);
      ducmessages = response["messages"];
      stores = response["stores"];

      print('My Stores');
      print(stores);

      myteamstatus = response['status'];
      print('JGGGGGLLLLLLLL');
      print(myteamstatus);
      multipleAccounts = response['accounts'];

      myteamstatus['rank'] =
          myteamstatus['rank'] == "" ? "0" : myteamstatus['rank'];
      //print('Mypriceofcoin Values');

      //print(response['price_of_coins']['price_of_coin']);

      // mypriceofcoinvalue = response['price_of_coins']['price_of_coin'];
      mypriceofcoin = response['price_of_coins'];

      mybonus = response['bonus_statement']['charts'];
      mybonus2 = response['bonus_statement']['boxes'];
      mybonus3 = response['bonus_statement'];
      //print('mybonus3');
      //print(mybonus3);

      myperformance = response['performance'];

      myproducts = response['products'];

      latestmember = response["last_members"];
      lastPackages = response["last_packages"];
      lastPPackages = response["last_p_packages"];

      lockinCoins = response["duc_in_vault"];
      dateParse = DateTime.now();
      time = DateFormat.yMMMEd().format(dateParse);
      // //print(accountType );
      totalPackagesCount = response["total_packages_count"];
      totalPartnersCount = response["total_partners_count"];
      totalDsvpCount = response["total_dsv_p"];
      //print("1234321");
      //print(totalPackagesCount);

      bonuschart = [];

      for (var item in mybonus.keys) {
        bonuschart.add({
          "bonuscategory": item,
          "data": mybonus[item] > 0 ? mybonus[item] : 0
        });
      }
      performancechart1 = [];
      performancechart2 = [];
      performancechart3 = [];
      // var i = 1, j = 1, k = 1;
      // for (var item in myperformance[0]['wave1']) {
      //   performancechart1.add({"category": i, "data": item > 0 ? item : 0});
      //   i++;
      // }
      // for (var item in myperformance[0]['wave2']) {
      //   performancechart2.add({"category": j, "data": item > 0 ? item : 0});
      //   j++;
      // }
      // for (var item in myperformance[0]['wave3']) {
      //   performancechart3.add({"category": k, "data": item > 0 ? item : 0});
      //   k++;
      // }

      // performancechart1 = [
      //   {
      //     "category": 1,
      //     "data": 0,
      //   },
      //   {
      //     "category": 2,
      //     "data": 40,
      //   },
      //   {
      //     "category": 3,
      //     "data": 45,
      //   },
      //   {
      //     "category": 4,
      //     "data": 48,
      //   },
      //   {
      //     "category": 5,
      //     "data": 50,
      //   },
      //   {
      //     "category": 6,
      //     "data": 45,
      //   },
      //   {
      //     "category": 7,
      //     "data": 52,
      //   },
      //   // {
      //   //   "category": 8,
      //   //   "data": 60,
      //   // },
      //   // {
      //   //   "category": 9,
      //   //   "data": 65,
      //   // },
      //   // {
      //   //   "category": 10,
      //   //   "data": 70,
      //   // },
      // ];
      // performancechart2 = [
      //   {
      //     "category": 1,
      //     "data": 0,
      //   },
      //   {
      //     "category": 2,
      //     "data": 25,
      //   },
      //   {
      //     "category": 3,
      //     "data": 20,
      //   },
      //   {
      //     "category": 4,
      //     "data": 25,
      //   },
      //   {
      //     "category": 5,
      //     "data": 20,
      //   },
      //   {
      //     "category": 6,
      //     "data": 30,
      //   },
      //   {
      //     "category": 7,
      //     "data": 20,
      //   },
      //   // {
      //   //   "category": 8,
      //   //   "data": 25,
      //   // },
      //   // {
      //   //   "category": 9,
      //   //   "data": 35,
      //   // },
      //   // {
      //   //   "category": 10,
      //   //   "data": 20,
      //   // },
      // ];
      // performancechart3 = [
      //   {
      //     "category": 1,
      //     "data": 0,
      //   },
      //   {
      //     "category": 2,
      //     "data": 2,
      //   },
      //   {
      //     "category": 3,
      //     "data": 5,
      //   },
      //   {
      //     "category": 4,
      //     "data": 15,
      //   },
      //   {
      //     "category": 5,
      //     "data": 10,
      //   },
      //   {
      //     "category": 6,
      //     "data": 5,
      //   },
      //   {
      //     "category": 7,
      //     "data": 13,
      //   },
      //   // {
      //   //   "category": 8,
      //   //   "data": 16,
      //   // },
      //   // {
      //   //   "category": 9,
      //   //   "data": 5,
      //   // },
      //   // {
      //   //   "category": 10,
      //   //   "data": 20,
      //   // },
      // ];
      // latestnews(context);

      return "Start";
    }
  }

  // void latestnews(BuildContext context) async {
  //   await showDialog(
  //     context: context,
  //     builder: (BuildContext context) {
  //       return AlertDialog(
  //         elevation: 24.0,
  //         backgroundColor: Pallet.popupcontainerback,
  //         shape: RoundedRectangleBorder(
  //             borderRadius: BorderRadius.all(Radius.circular(15.0))),
  //         title: Row(
  //           children: [
  //             Image.asset("c-logo.png", width: 40),
  //             SizedBox(width: 10),
  //             Expanded(
  //               child: Text(ducmessages[0]["header"],
  //                   maxLines: 2,
  //                   style: TextStyle(
  //                       fontSize: Pallet.heading3,
  //                       color: Pallet.fontcolor,
  //                       fontWeight: Pallet.font500)),
  //             ),
  //           ],
  //         ),
  //         content: Container(
  //           width: 450.0,
  //           child: SingleChildScrollView(
  //             child: ListBody(
  //               children: <Widget>[
  //                 Center(
  //                     child: Text(ducmessages[0]["body"],
  //                         style: TextStyle(
  //                           fontSize: Pallet.heading2 / 1.03,
  //                           height: 1.5,
  //                           color: Pallet.fontcolor,
  //                         ))),
  //               ],
  //             ),
  //           ),
  //         ),
  //         actions: [
  //           PopupButton(
  //             text: 'Close',
  //             onpress: () {
  //               Navigator.of(context).pop();
  //             },
  //           ),
  //           SizedBox(height: 10),
  //         ],
  //       );
  //     },
  //   );
  // }
}

class AccountsData {
  List accountTrans = [];
  List accounts = [];
  Future<String> statement(assetType, setState) async {
    var map = {
      "payload": {
        "product_id": 1,
        "trans_mode": "null",
        "asset_type": assetType == null ? "null" : assetType,
        "limit": 100,
        "off_set": 0
      }
    };

    Map<String, dynamic> result =
        await HttpRequest.Post('accountStatement', map);
    if (Utils.isServerError(result)) {
      return throw (await Utils.getMessage(result['response']['error']));
    } else {
      if (assetType != null) {
        setState(() {
          accountTrans = result['response']['data'];
        });
      } else {
        setState(() {
          accounts = result['response']['data'];
        });
      }

      // return result['response'].toString();
      return Utils.toJSONString(await Utils.convertMessage(result['response']));
    }
  }
}

class NewsData {
  List<dynamic> news = [];
  Future<String> getNews(setState) async {
    Map<String, dynamic> result =
        await HttpRequest.Post('c_rms_get_news_data', Utils.getSiteID());
    if (Utils.isServerError(result)) {
      // //print(result['response']['error']);
      return throw (await Utils.getMessage(result['response']['error']));
    } else {
      setState(() {
        news = result['response']['data'];
      });

      return "start";
      // return Utils.toJSONString(await Utils.convertMessage(result['response']));
    }
  }
}

class SettingData {
  Map<String, dynamic> settings;
  bool twoEnabled;
  // Future<String> response;
  Future<String> getSettings() async {
    Map<String, dynamic> result =
        await HttpRequest.Post('c_my_setting', Utils.getSiteID());
    if (Utils.isServerError(result)) {
      return throw (await Utils.getMessage(result['response']['error']));
    } else {
      await settingskycdata.getKycStatus();
      print('IIIIIIIIIIIIIIIII');
      print(settingskycdata.getKycStatus());

      settings = result['response']['data'];
      twoEnabled = result['response']['data']['info']['two_fa_enabled'];
      //print(result['response']['data']);
      return "start";
    }
  }

  Future<String> setup2FA() async {
    Map<String, dynamic> map = Utils.getSiteID();
    map['payload']['username'] = Utils.fromJSONString(
        await Utils.getSharedPrefereces('data'))['user_name'];
    //print(
    // '********************************************************************');
    //print(map);

    Map<String, dynamic> result = await HttpRequest.Post('setup2FA', map);
    //print(result);
    //print(result['response']['error']);
    //print(Utils.isServerError(result));
    if (Utils.isServerError(result)) {
      //print(await Utils.getMessage(result['response']['error']));
      return throw (await Utils.getMessage(result['response']['error']));
    } else {
      //print(result);
      return Utils.toJSONString(
          await Utils.convertMessage(result['response']['data']));
    }
  }
}

class SettingKycData {
  String settingskyc;
  // bool twoEnabled;
  // Future<String> response;
  Future<String> getKycStatus() async {
    var map = {
      'payload': {
        "system_product_id": 1,
      },
    };

    Map<String, dynamic> result =
        await HttpRequest.Post('kyc_status_check', map);
    print('MYMAPSSSSSSSSS');
    print(result);
    if (Utils.isServerError(result)) {
      print('SERVER ERROR');
      print(result['response']['error']);
      return throw (await Utils.getMessage(result['response']['error']));
    } else {
      settingskyc = result['response']['data'];

      // twoEnabled = result['response']['data']['info']['two_fa_enabled'];
      print('##################################');
      print(settingskyc);
      print(result['response']['data']);
      return "start";
    }
  }
}

class DocumentsData {
  List<dynamic> documents = [];
  Future<String> getDocumentsdata(setState) async {
    try {
      Map<String, dynamic> result =
          await HttpRequest.Post('documentsData', Utils.getSiteID());
      if (Utils.isServerError(result)) {
        // //print(result['response']['error']);
        return throw (await Utils.getMessage(result['response']['error']));
      } else {
        setState(() {
          documents = result['response']['data'];
          print('MMMMMMMYYYYYYYYY');
          print(documents);
        });
        return Utils.toJSONString(
            await Utils.convertMessage(result['response']));
      }
      // ignore: unused_catch_stack
    } catch (e, trace) {
      //print(e);
      //print(trace);
      //print('error in getdocumentsdaya');
      return throw ('Something Went Wrong');
    }
  }
}

// class PayoutData {
//   Map<String, dynamic> payoutSystemDetails = {};
//   Map<String, dynamic> payoutUserDetails = {};
//   double totalDucAmount = 0, totalCashAmount = 0;
//   TextEditingController ducAmount = TextEditingController();
//   TextEditingController cashAmount = TextEditingController();

//   validdateDuc(setState) {
//     //print(ducAmount.text);
//     if (ducAmount.text.length > 0) {
//       // double temp = double.parse(ducAmount.text);
//       setState(() {
//         totalDucAmount =
//             double.parse(ducAmount.text) / payoutSystemDetails['duc_price'];
//       });
//     } else {
//       setState(() {
//         totalDucAmount = 0;
//       });
//     }
//   }

//   validdateCash(setState) {
//     //print(cashAmount.text);
//     if (cashAmount.text.length > 0) {
//       // double temp = double.parse(cashAmount.text);
//       setState(() {
//         totalCashAmount = double.parse(cashAmount.text);
//       });
//     } else {
//       setState(() {
//         totalCashAmount = 0;
//       });
//     }
//   }

//   Future<String> getDetails(setState) async {
//     Map<String, dynamic> result =
//         await HttpRequest.Post('getPayoutDetails', Utils.getSiteID());
//     if (Utils.isServerError(result)) {
//       return throw (await Utils.getMessage(result['response']['error']));
//     } else {
//       setState(() {
//         payoutSystemDetails = result['response']['data']['system_data'];
//         payoutUserDetails = result['response']['data']['user_data'];
//       });
//       return Utils.toJSONString(
//           await Utils.convertMessage(result['response']['data']));
//     }
//   }
// }

class BonusData {
  List<dynamic> bonusTrans = [];
  List<dynamic> bonusState = [];
  String name;
  Future<String> response;
  double total = 0;
  var setData;
  Future<String> statement(String assetType, setState) async {
    var data = {
      "payload": {
        "product_id": 1,
        "trans_type": assetType,
        "limit": 10,
        "off_set": 0
      }
    };
    Map<String, dynamic> result =
        await HttpRequest.Post('bonusStatement', data);
    if (Utils.isServerError(result)) {
      //print(result['response']['error']);
      return throw (await Utils.getMessage(result['response']['error']));
    } else {
      if (assetType == 'null') {
        setData = setState;
        setData(() {
          // total = double.parse(result['response']['data']['total'].toString());
          bonusTrans = result['response']['data']['trans'];
        });
      } else {
        setData(() {
          print('TTTTfsdfsTTTTTTTTT');
          total = double.parse(result['response']['data']['total'].toString());
          print(total);

          if (total == 0) {
            print('TTTTTTTTTTTTT');
            print(total);
            total =
                double.parse(result['response']['data']['total'].toString());
          } else {
            total =
                double.parse(result['response']['data']['total'].toString());
            total = double.parse(total.toStringAsFixed(2));
          }

          // for (var temp in bonusTrans) {
          //   if (temp['trans_type'] == assetType) {
          //     name = temp['trans_name'];
          //   }
          // }
          name = assetType;
        });
        //print('**************************************************************');
        //print(name);

        setState(() {
          bonusState = result['response']['data']['trans'];
          //print('bonusState Statement');
          //print(bonusState);
        });
      }

      return Utils.toJSONString(
          await Utils.convertMessage(result['response']['data']));
    }
  }
}

class InvoicesData {
  // List<dynamic> invoicehistory = [];
  // setState() {
  //   invoicehistory = statement() as List;
  //   print(invoicehistory);
  // }
  List<dynamic> indata = [];
  bool type = false;

  StreamController invoicecontroller = StreamController.broadcast();
  StreamSink get invoicesink => invoicecontroller.sink;
  Stream get invoicestream => invoicecontroller.stream;

  Future statement(
    setState,
  ) async {
    var data = {
      "payload": {
        "is_dsvp": type,
      }
    };
    // invoicesink.add(null);
    Map<String, dynamic> result =
        await HttpRequest.Post('invoiceStatement', data);
    if (Utils.isServerError(result)) {
      return throw (await Utils.getMessage(result['response']['error']));
    } else {
      setState(() {
        indata = result['response']['data'];
        // print('indata Statement');
        // print(indata);
      });
      invoicesink.add(result['response']['data']);
      return result['response']['data'];
    }
  }
}

SettingData settingdata = SettingData();
SettingKycData settingskycdata = SettingKycData();

NewsData newsData = NewsData();
DocumentsData documentsData = DocumentsData();

AccountsData accounts = AccountsData();
BonusData bonus = BonusData();

InvoicesData invoices = InvoicesData();

// PayoutData payoutss = PayoutData();
DashboardData dashboard = DashboardData();
