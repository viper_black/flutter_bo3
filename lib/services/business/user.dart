// import 'dart:math';
// import '../../home.dart';
// import '../../login.dart';

import '../../utils/utils.dart' show Utils;
import '../communication/index.dart' show HttpRequest;
import './account.dart';
import 'package:flutter/material.dart';
import '../../data.dart';
// import '../../config.dart';
// ignore: unused_import
import '../../config/app_settings.dart';
// import '../communication/crypto/index.dart';

class User {
  Map<String, dynamic> _userDetails;
  static bool _isAuthorized = false;
  static bool _gotProfile = false;
  Map<int, Account> _userAccounts = {1: new Account()};
  User() {
    // setLanguage(1);
  }
  Future<bool> setLanguage(int id) async {
    if (id == 1) {
      await Utils.setSharedPrefereces(
        Utils.toJSONString({'language': 'English'}),
        "language",
      );
      return true;
    }
    return false;
  }

  bool setUserDetail(Map<String, dynamic> details) {
    for (var key in details.keys) {
      _userDetails[key] = details[key];
    }
    _gotProfile = true;
    return true;
  }

  Map<String, dynamic> getUserDetails() {
    return _userDetails;
  }

  String username() {
    return _userDetails['username'];
  }

  String userEmail() {
    return _userDetails['email'];
  }

  Account account() {
    if (_isAuthorized) {
      return _userAccounts[1];
    } else {
      return _userAccounts[1];
      // return throw new UnsupportedError('UnAuthorized Access');
    }
  }

  Future<String> getDocuments() async {
    // List documents = [];
    Map<String, dynamic> result =
        await HttpRequest.Post('getDocuments', Utils.getSiteID());
    if (Utils.isServerError(result)) {
      //print(result['response']['error']);
      return throw (await Utils.getMessage(result['response']['error']));
    } else {
      return Utils.toJSONString(
          await Utils.convertMessage(result['response']['data']));
    }
  }

  Future<String> verifyEmail(String token) async {
    Map<String, dynamic> payload = {"token": token, 'product_id': 1};
    Map<String, dynamic> result =
        await HttpRequest.Post('verifyEmail', Utils.constructPayload(payload));
    //print('$result, verify email');
    if (Utils.isServerError(result)) {
      print('RESULT: ${result['response']['error']}');

      return result['response']['error'];
    } else {
      return 'null';
    }
  }

  Future<String> checkEmail(String data) async {
    Map<String, dynamic> payload = {"email": data};
// ignore: omit_local_variable_types
    Map<String, dynamic> result =
        await HttpRequest.Post('email', Utils.constructPayload(payload));
    if (Utils.isServerError(result)) {
      //print(result);
      return await Utils.getMessage(result['response']['error']);
    } else {
      return result['response']['data'].toString();
    }
  }

  // ignore: non_constant_identifier_names
  Future<String> resetPassword(String old_password, String new_password) async {
    Map<String, dynamic> data = Utils.getSiteID();
    data['payload']['old_password'] = old_password;
    data['payload']['new_password'] = new_password;
    Map<String, dynamic> result = await HttpRequest.Post('resetPassword', data);
    if (Utils.isServerError(result)) {
      return await Utils.getMessage(result['response']['error']);
    } else {
      return Utils.toJSONString(
          await Utils.convertMessage(result['response']['data']));
    }
  }

//   Future<String> checkUsername(String data) async {
//     Map<String, dynamic> payload = {'username': data};
// // ignore: omit_local_variable_types
//     Map<String, dynamic> result = await HttpRequest.Post(
//         'checkUsername', Utils.constructPayload(payload));
//     if (Utils.isServerError(result)) {
//       //print('request 400');
//       return await Utils.getMessage(result['response']['error']);
//     } else {
//       //print('request 200');
//       return result['response']['data'].toString();
//     }
//   }
  Future<String> checkUsername(String data) async {
    Map<String, dynamic> payload = {'username': data};
    // ignore: omit_local_variable_types
    Map<String, dynamic> result = await HttpRequest.Post(
        'checkUsername', Utils.constructPayload(payload));
    if (Utils.isServerError(result)) {
      return result['response']['error'].toString();
    } else {
      return result['response']['data'].toString();
    }
  }

  Future<String> checkPassword(String data) async {
    Map<String, dynamic> payload = {'password': data};
// ignore: omit_local_variable_types
    Map<String, dynamic> result = await HttpRequest.Post(
        'checkPassword', Utils.constructPayload(payload));
    if (Utils.isServerError(result)) {
      return result['response']['error'].toString();
    } else {
      print('12343456789');
      return '';
    }
  }

  Future<String> isSponser(String data) async {
    Map<String, dynamic> payload = {'checkname': data};
    Map<String, dynamic> result =
        await HttpRequest.Post('checkSponsor', Utils.constructPayload(payload));
    if (Utils.isServerError(result)) {
      return await Utils.getMessage(result['response']['error']);
    } else {
      //print(result['response']['data']);
      return Utils.toJSONString(
          await Utils.convertMessage(result['response']['data']));
    }
  }

  // Future<String> checkEmail(String data) async {
  //   Map<String, dynamic> payload = {"email": data};
  //   // ignore: omit_local_variable_types
  //   Map<String, dynamic> result =
  //       await HttpRequest.Post('email', Utils.constructPayload(payload));
  //   if (Utils.isServerError(result)) {
  //     //print(result);
  //     return await Utils.getMessage(result['response']['error']);
  //   } else {
  //     return await Utils.getMessage(result['response']['data'].toString());
  //   }
  // }

  Future<String> setup2FA(String username) async {
    //print(
    // '++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++');
    Map<String, dynamic> data = Utils.getSiteID();
    data['payload']['username'] = username;
    Map<String, dynamic> result = await HttpRequest.Post('setup2FA', data);

    //print(result);
    //print(
    // '===========================================================================================');
    if (Utils.isServerError(result)) {
      return await Utils.getMessage(result['response']['error']);
    } else {
      return Utils.toJSONString(
          await Utils.convertMessage(result['response']['data']));
    }
  }

  Future<String> disable2FA() async {
    Map<String, dynamic> data = Utils.getSiteID();

    Map<String, dynamic> result = await HttpRequest.Post('disable2FA', data);
    if (Utils.isServerError(result)) {
      return await Utils.getMessage(result['response']['error']);
    } else {
      return result['response']['data'] == true
          ? await Utils.getMessage('2FA_disabled')
          : "";
    }
  }

  Future<String> check2FA(String username, secret) async {
    Map<String, dynamic> data = Utils.getSiteID();
    data['payload']['username'] = username;
    data['payload']['is_setup'] = true;
    data['payload']['secret'] = secret.toString();
    Map<String, dynamic> result = await HttpRequest.Post('check2FA', data);
    if (Utils.isServerError(result)) {
      return await Utils.getMessage(result['response']['error']);
    } else {
      return result['response']['data'] == true
          ? await Utils.getMessage('valid_code')
          : "";
    }
  }

  // Future<String> isSponser(String data) async {
  //   Map<String, dynamic> payload = {'checkname': data};
  //   Map<String, dynamic> result =
  //       await HttpRequest.Post('checkSponsor', Utils.constructPayload(payload));
  //   if (Utils.isServerError(result)) {
  //     return await Utils.getMessage(result['response']['error']);
  //   } else {
  //     //print(result['response']['data']);
  //     return await Utils.getMessage(result['response']['data'].toString());
  //   }
  // }

  Future<List> getCountires(Map<String, dynamic> details) async {
    Map<String, dynamic> result =
        await HttpRequest.Post('country', Utils.constructPayload(details));
    return result['response']['data']['countries'];
  }

  Future<String> login(Map<String, dynamic> details) async {
    Map<String, dynamic> result =
        await HttpRequest.Post('login', Utils.constructPayload(details));
    if (Utils.isServerError(result)) {
      return await Utils.getMessage(result['response']['error']);
    } else {
      await Utils.setSharedPrefereces(
          Utils.toJSONString(result['response']['data']), 'data');
      _isAuthorized = true;

      //print(result['response']['data']);
      return await Utils.getMessage('login_sucess');
    }
  }

  Future<String> logout() async {
    Map<String, dynamic> result =
        await HttpRequest.Post('logout', Utils.getSiteID());
    //print(result);
    if (Utils.isServerError(result)) {
      return await Utils.getMessage(result['response']['error']);
    } else {
      _isAuthorized = false;
      return result['response']['data'].toString();
    }
  }

  Future<bool> setFingerPrint(bool value) async {
    Map<String, dynamic> data = {'isEnabled': value};
    await Utils.setSharedPrefereces(Utils.toJSONString(data), 'fingerprint');
    return true;
  }

  Future<Map<String, dynamic>> loginPreferences() async {
    Map<String, dynamic> result = {};
    var login = await Utils.getSharedPrefereces('data');
    //print(login);
    //print('login');
    if (login == 'Something Went Wrong') {
      result['username'] = null;
    } else {
      result['username'] = Utils.fromJSONString(
          await Utils.getSharedPrefereces('data'))['user_name'];
    }
    var finger = await Utils.getSharedPrefereces('fingerprint');
    //print(finger);
    //print('finger');

    if (finger == 'Something Went Wrong') {
      result['fingerprint'] = null;
    } else {
      result['fingerprint'] = Utils.fromJSONString(
          await Utils.getSharedPrefereces('fingerprint'))['isEnabled'];
    }

    return result;
  }

  Future<String> profile() async {
    if (_gotProfile) {
      return Utils.toJSONString(_userDetails);
    } else {
      Map<String, dynamic> result =
          await HttpRequest.Post('profile', Utils.getSiteID());
      //print(result);
      if (Utils.isServerError(result)) {
        return await Utils.getMessage(result['response']['error']);
      } else {
        setUserDetail(result['response']['data']);
        return Utils.toJSONString(
            await Utils.convertMessage(result['response']['data']));
      }
    }
  }

  Future<String> sendOTP(String email) async {
    Map<String, dynamic> payload = {'email_id': email};
    Map<String, dynamic> result =
        await HttpRequest.Post('sendOtp', Utils.constructPayload(payload));
    //print(result);
    if (Utils.isServerError(result)) {
      return await Utils.getMessage(result['response']['error']);
    } else {
      return Utils.toJSONString(
          await Utils.convertMessage(result['response']['data']));
    }
  }

  Future<String> checkOTP(String email, int otp) async {
    Map<String, dynamic> payload = {'email': email, 'otp': otp};
    Map<String, dynamic> result =
        await HttpRequest.Post('checkOtp', Utils.constructPayload(payload));
    //print(result);
    if (Utils.isServerError(result)) {
      return await Utils.getMessage(result['response']['error']);
    } else {
      return Utils.toJSONString(
          await Utils.convertMessage(result['response']['data']));
    }
  }
}

class Auth {
  int pageidx = 0;
  double headerLineLeft = 0;
  List dots = [
    {"color": Colors.white, "used": true},
    {"color": Colors.white, "used": false},
    {"color": Colors.white, "used": false},
    {"color": Colors.white, "used": false},
    {"color": Colors.white, "used": false},
    {"color": Colors.white, "used": false},
    {"color": Colors.white, "used": false},
    {"color": Colors.white, "used": false}
  ];
  int accountType = 1;

  int userType = 4;
  TextEditingController username = TextEditingController();
  String userError;

  TextEditingController companyName = TextEditingController();
  String companyError;

  TextEditingController email = TextEditingController();
  String emailError;

  TextEditingController password = TextEditingController();
  String passwordError;

  TextEditingController passwordconfirm = TextEditingController();
  String confirmError;

  TextEditingController sponsor = TextEditingController(text: 'dcfounders');
  String sponsorError;

  TextEditingController retypepwdCon = TextEditingController();

  next({double width}) {
    pageidx += 1;
    headerLineLeft = width * pageidx / 100 * 15;
    dots[pageidx]["used"] = true;
    //print(headerLineLeft);
  }

  back({double width, int index}) {
    dots[pageidx]["used"] = false;
    pageidx -= 1;
    headerLineLeft = width * pageidx / 100 * 15;
  }

  checSponsor(value, setState) {
    if (value.trim().isEmpty) {
      sponsorError = "Sponsor Required";
    } else if (value.length < 6) {
      sponsorError = "Sponsor must be of minimum 6 char";
    } else {
      output(value) {
        //print(value);
        if (value == "Sponsor Not Found") {
          setState(() {
            sponsorError = "Sponsor Now Found";
          });
        } else {
          setState(() {
            sponsorError = null;
          });
        }
      }

      SampleClass().checksponsor(value, (output));
    }
  }

  checkUsername(value) {
    if (value.trim().isEmpty) {
      userError = "Username Required";
    } else if (value.length < 6) {
      userError = "Username must be of minimum 6 char";
    } else {
      output(value) {
        //print(value);
        if (value == "usernameexists") {
          userError = "Username allready exists";
        } else {
          userError = null;
        }
      }

      SampleClass().checkusername(value, (output));
    }
  }

  checkPassword(value) {
    if (value.trim().isEmpty) {
      passwordError = "Password Required";
    } else if (RegExp(".*[0-9].*").hasMatch(value.substring(0, 1))) {
      passwordError = "cannot start with numeric";
    } else if (value.length < 8) {
      passwordError = "must be of minimum 6 char ";
    } else if (!RegExp("^(?=.*[!@#\$%\^&\*])").hasMatch(value)) {
      passwordError = "must contain atleast one special char";
    } else if (!RegExp(".*[0-9].*").hasMatch(value)) {
      passwordError = "must contain atleast one numeric";
    } else if (!RegExp(".*[A-Z].*").hasMatch(value)) {
      passwordError = "must contain atleast one Alpha numeric";
    } else if (!RegExp(".*[a-z].*").hasMatch(value)) {
      passwordError = "must contain atleast one lower char";
    } else {
      passwordError = null;
    }
  }

  checkReTypePassword(value) {
    if (value.trim().isEmpty) {
      confirmError = "Re-Type Password Required";
    } else if (password.text != passwordconfirm.text) {
      confirmError = "Password dont match";
    } else {
      confirmError = null;
    }
  }

  checkEmail(value) {
    if (value.trim().isEmpty) {
      emailError = "Email Required";
    } else if (!RegExp(
            r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+")
        .hasMatch(value)) {
      emailError = "Enter a valid email";
    } else {
      output(value) {
        //print(value);
        if (value.toString() == "Email Already Exists") {
          emailError = "Email Already Exists";
        } else {
          emailError = null;
        }
      }

      SampleClass().checkemail(value, (output));
    }
  }

  checkCompany(value) {
    if (value.trim().isEmpty) {
      companyError = "Company Name Required";
    } else {
      companyError = null;
    }
  }

  Future<String> signup() async {
    Map<String, dynamic> details = {
      "sponsor": 1,
      "account_type": accountType,
      "username": username.text.toString(),
      "password": password.text.toString(),
      "email": email.text.toString(),
      "country": "IN",
      "product_id": 1,
      "company_name": "null",
      "user_type": 4
    };
    //print('lk;lk' + Utils.constructPayload(details).toString());
    Map<String, dynamic> result =
        await HttpRequest.Post('signup', Utils.constructPayload(details));
    //print(result);
    if (Utils.isServerError(result)) {
      return await Utils.getMessage(result['response']['error']);
    } else {
      return Utils.toJSONString(
          await Utils.convertMessage(result['response']['data']));
    }
  }
}

// class Loginn {
//   rndmtest() {
//     var test = getRandomString(32);
//     //print(test);
//   }

//   final username = TextEditingController();
//   final password = TextEditingController();

//   TextEditingController otp = TextEditingController();
//   Future<String> fingerPrintSet;
//   bool fingerPrintAuth;
//   final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
//   final loginfailed = SnackBar(
//     content: Text('Username or Password incorrect !'),
//   );
//   final otpRegistered = SnackBar(
//     content: Text('email successfully verified'),
//   );

//   String otpError;

//   String _chars =
//       'AaBbCcDdEeFfGgHhIiJjKkLlMmNnOoPpQqRrSsTtUuVvWwXxYyZz1234567890';
//   Random _rnd = Random();
//   String getRandomString(int length) => String.fromCharCodes(Iterable.generate(
//       length, (_) => _chars.codeUnitAt(_rnd.nextInt(_chars.length))));

//   void resendOtp({String username, email}) async {
//     Map<String, dynamic> _map = {
//       "product_id": 1,
//       "username": username,
//       "otp": 0,
//       "email": email,
//       "s_flag": true
//     };
//     //print("lkjkjlkj $_map");
//     Map<String, dynamic> result =
//         await HttpRequest.Post('email_otp', Utils.constructPayload(_map));
//     if (Utils.isServerError(result)) {
//       //print(result['response']['error']);
//     } else {
//       await Utils.setSharedPrefereces(
//           Utils.toJSONString(result['response']['data']), 'data');
//     }

//     //print(result['response']['data']);
//     // return await Utils.getMessage('login_sucess');
//   }

//   void checkOtp({String username, String email, int otp, context}) async {
//     Map<String, dynamic> map = {
//       "product_id": 1,
//       "username": username,
//       "otp": otp,
//       "email": email,
//       "s_flag": false
//     };
//     Map<String, dynamic> result =
//         await HttpRequest.Post('email_otp', Utils.constructPayload(map));
//     if (Utils.isServerError(result)) {
//       otpError = "Invalid otp!";
//     } else {
//       await Utils.setSharedPrefereces(
//           Utils.toJSONString(result['response']['data']), 'data');
//       otpError = null;
//       Navigator.of(context).pop();
//       ScaffoldMessenger.of(context).showSnackBar(loginfailed);
//     }

//     _formKey.currentState.validate();
//   }

//   setUpFingerprint() async {
//     Map<String, dynamic> hashkey = {
//       "username": "test12345",
//       "product_id": 1,
//       "device_model": "iphone6",
//       "android_id": "d5d0f7d4f37d8541",
//       "mac_address": "E3:7D:89:J0:F4:Q0:11:A3",
//       "time_snap": "1970-01-01 00:00:00"
//     };

//     String key = Encryption().encode(hashkey.toString());
//     //print("llkjlj $key");

//     Map<String, dynamic> map = {
//       "username": "test12345",
//       "password": "Eg@email.com1",
//       "product_id": 1,
//       "hash_key": key.toString()
//     };
//     Map<String, dynamic> result = await HttpRequest.Post(
//         'c_fingerprint_setup', Utils.constructPayload(map));

//     if (Utils.isServerError(result)) {
//     } else {
//       //print(result['response']['data']);
//     }
//   }

//   login(context) async {
//     Map<String, dynamic> map = {
//       "username": 'testing1',
//       "password": 'Eg@email.com1',
//       "password_future": "null",
//       "hardware_id": "null",
//       "product_id": 1
//     };
//     Map<String, dynamic> result =
//         await HttpRequest.Post('login', Utils.constructPayload(map));
//     if (Utils.isServerError(result)) {
//       ScaffoldMessenger.of(context).showSnackBar(loginfailed);
//     } else {
//       //print(result['response']['data']['email_status']);
//       await Utils.setSharedPrefereces(
//           Utils.toJSONString(result['response']['data']), 'data');
//       if (result['response']['data']['email_status'] == false) {
//         showDialog(
//             context: context,
//             builder: (_) => AlertDialog(
//                   elevation: 24.0,
//                   backgroundColor: Pallet.inner1,
//                   shape: RoundedRectangleBorder(
//                     borderRadius: BorderRadius.all(Radius.circular(15.0)),
//                   ),
//                   title: Icon(
//                     Icons.email_rounded,
//                     color: Pallet.fontcolor,
//                     size: 60,
//                   ),
//                   content: Row(
//                     mainAxisAlignment: MainAxisAlignment.center,
//                     children: [
//                       Text('Email not verified!',
//                           style: TextStyle(
//                             color: Pallet.fontcolor,
//                           )),
//                     ],
//                   ),
//                   actions: [
//                     FlatButton(
//                       child: Text("Send OTP"),
//                       onPressed: () {
//                         resendOtp(
//                             username: result['response']['data']['username'],
//                             email: result['response']['data']['email_id']);
//                         Navigator.of(context, rootNavigator: true)
//                             .pop('dialog');
//                         showDialog(
//                             context: context,
//                             builder: (_) => AlertDialog(
//                                   title: Text('Enter your otp'),
//                                   content: Form(
//                                     child: Form(
//                                       key: _formKey,
//                                       child: TextFormField(
//                                           controller: otp,
//                                           decoration: const InputDecoration(
//                                               labelText: 'content'),
//                                           keyboardType: TextInputType.text,
//                                           validator: (value) {
//                                             if (value.isEmpty) {
//                                               return "Invalid otp!";
//                                             } else {
//                                               return otpError;
//                                             }
//                                           }),
//                                     ),
//                                   ),
//                                   actions: [
//                                     FlatButton(
//                                       child: Text("Send OTP"),
//                                       onPressed: () {
//                                         checkOtp(
//                                             username: result['response']['data']
//                                                     ['username']
//                                                 .toString(),
//                                             email: result['response']['data']
//                                                     ['email_id']
//                                                 .toString(),
//                                             otp: int.parse(otp.text),
//                                             context: context);
//                                         // //print(temp);
//                                       },
//                                     )
//                                   ],
//                                 ));
//                       },
//                     ),
//                   ],
//                 ));
//       } else if (result['response']['data']['two_fa_enabled'] == true) {
//         showDialog(
//             context: context,
//             builder: (_) => Dialog(
//                   elevation: 24.0,
//                   backgroundColor: Color(0xFF1034a6),
//                   shape: RoundedRectangleBorder(
//                     borderRadius: BorderRadius.all(Radius.circular(15.0)),
//                   ),
//                   child: Container(
//                     padding: EdgeInsets.all(20.0),
//                     // child: Wrap(children:[]),
//                   ),
//                 ));
//       } else {
//         Navigator.push(
//           context,
//           MaterialPageRoute(builder: (context) => Home(activeLink: 0)),
//         );
//         await Utils.setSharedPrefereces(
//             Utils.toJSONString(result['response']['data']), 'data');
//       }
//     }
//   }
// }

// Loginn loginn = Loginn();

Auth signup = Auth();
User user = User();
