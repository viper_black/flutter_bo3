// import 'package:centurion/animations/dropdown.dart';
import 'package:centurion/login.dart';
import 'package:centurion/lottery/common.dart';
import 'package:centurion/lottery/dropdown/drop.dart';
import 'package:centurion/utils/utils.dart';
// import 'package:dropdownfield/dropdownfield.dart';
import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';
import 'config.dart';
import 'services/communication/index.dart' show HttpRequest;

// import 'common.dart';
// import 'config.dart';
// import 'data.dart';

class ResetAccount extends StatefulWidget {
  final String email;
  final String password;

  const ResetAccount({Key key, this.email, this.password}) : super(key: key);
  @override
  _ResetAccountState createState() => _ResetAccountState();
}

class _ResetAccountState extends State<ResetAccount> {
  TextEditingController _emailcontroller = TextEditingController();
  Future<String> _temp;
  List<String> emaillist = ['kk'];
  String primaryacc = '';
  List secondaryacc = [];
  var result;
  Future<String> getdata() async {
    print('ffffffffffssssssddddddfffff');
    // var templist = [
    //   {"username": "deepakrgty1", "email": "ragu@gmail.com"},
    //   {"username": "grth", "email": "sggtrfd@gmail.com"},
    //   {"username": "wsdefrgtyh", "email": "sggtrfd@gmail.com"}
    // ];
    // for (var i = 0; i < templist.length; i++) {
    //   emaillist.add(
    //     templist[i]['username'] +
    //         ' - ' +
    //         templist[i]['email'].toString().split('@')[0].substring(
    //             0,
    //             templist[i]['email'].toString().split('@')[0].length -
    //                         templist[i]['email']
    //                             .toString()
    //                             .split('@')[0]
    //                             .length >
    //                     3
    //                 ? 3
    //                 : 1) +
    //         '***@' +
    //         templist[i]['email'].toString().split('@')[1],
    //   );
    // }
    var map = {
      'username': widget.email,
      'password': widget.password,
    };
    Map<String, dynamic> result =
        await HttpRequest.Post('resetaccount', Utils.constructPayload(map));
    print('RESULT: $result');
    if (Utils.isServerError(result)) {
      throw (await Utils.getMessage(result['response']['error']));
    } else {
      result = result['response']['data'];
      primaryacc = result['response']['data']['primary_account'];
      secondaryacc = result['response']['data']['secondary_account'];
      var templist = result['response']['data']['in_active_accounts'];
      for (var i = 0; i < templist.length; i++) {
        emaillist.add(
          templist[i]['username'] +
              ' - ' +
              templist[i]['email'].toString().split('@')[0].substring(
                  0,
                  templist[i]['email'].toString().split('@')[0].length -
                              templist[i]['email']
                                  .toString()
                                  .split('@')[0]
                                  .length >
                          3
                      ? 3
                      : 1) +
              '***@' +
              templist[i]['email'].toString().split('@')[1],
        );
      }
      return 'start';
    }
  }

  @override
  void initState() {
    print(widget.email);
    print(widget.password);
    super.initState();
    _temp = getdata();
  }

  double height;
  double width;
  @override
  Widget build(BuildContext context) {
    width = MediaQuery.of(context).size.width;
    height = MediaQuery.of(context).size.height;
    return Scaffold(
      body: FutureBuilder(
        future: _temp,
        builder: (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
          if (snapshot.hasData) {
            return LayoutBuilder(
              builder: (BuildContext context, BoxConstraints constraints) {
                if (constraints.maxWidth > 1024) {
                  return SingleChildScrollView(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        SizedBox(
                          height: height > 880 ? 50 : 0,
                        ),
                        Center(
                          child: Container(
                            width: height > 850
                                ? width * 0.30
                                : height <= 850 && height > 800
                                    ? width * 0.37
                                    : height <= 800 && height > 700
                                        ? width * 0.40
                                        : width * 0.55,
                            padding: EdgeInsets.all(25),
                            decoration: BoxDecoration(
                              color: Pallet.dashcontainerback,
                              borderRadius: BorderRadius.circular(10),
                            ),
                            child: Column(
                              children: [
                                title(fontsize: 25),
                                SizedBox(
                                  height: 15,
                                ),
                                accountdetail(),
                                SizedBox(
                                  height: 15,
                                ),
                                acclist(),
                                SizedBox(
                                  height: 15,
                                ),
                                info1(),
                                SizedBox(
                                  height: 15,
                                ),
                                info2(),
                                SizedBox(
                                  height: 15,
                                ),
                                info3(),
                                SizedBox(
                                  height: 20,
                                ),
                                buttons(),
                              ],
                            ),
                          ),
                        ),
                      ],
                    ),
                  );
                } else if (constraints.maxWidth > 450 &&
                    constraints.maxWidth <= 1024) {
                  return SingleChildScrollView(
                    child: Container(
                      width: width,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Container(
                            width: width * 0.7,
                            padding: EdgeInsets.all(25),
                            decoration: BoxDecoration(
                              color: Pallet.dashcontainerback,
                              borderRadius: BorderRadius.circular(10),
                            ),
                            child: Column(
                              children: [
                                title(),
                                SizedBox(
                                  height: 15,
                                ),
                                accountdetail(),
                                SizedBox(
                                  height: 15,
                                ),
                                acclist(),
                                SizedBox(
                                  height: 15,
                                ),
                                info1(),
                                SizedBox(
                                  height: 15,
                                ),
                                info2(),
                                SizedBox(
                                  height: 15,
                                ),
                                info3(),
                                SizedBox(
                                  height: 20,
                                ),
                                buttons(),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                  );
                } else {
                  return SingleChildScrollView(
                    child: Container(
                      width: width,
                      padding: EdgeInsets.all(10),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Container(
                            width: width,
                            padding: EdgeInsets.all(15),
                            decoration: BoxDecoration(
                              color: Pallet.dashcontainerback,
                              borderRadius: BorderRadius.circular(10),
                            ),
                            child: Column(
                              children: [
                                title(fontsize: Pallet.heading2),
                                SizedBox(
                                  height: 10,
                                ),
                                accountdetail(fontsize: Pallet.heading4),
                                SizedBox(
                                  height: 10,
                                ),
                                acclist(fontsize: Pallet.heading4),
                                SizedBox(
                                  height: 10,
                                ),
                                info1(fontsize: Pallet.heading4),
                                SizedBox(
                                  height: 10,
                                ),
                                info2(fontsize: Pallet.heading4),
                                SizedBox(
                                  height: 10,
                                ),
                                info3(fontsize: Pallet.heading4),
                                SizedBox(
                                  height: 15,
                                ),
                                buttons(),
                              ],
                            ),
                          ),
                          SizedBox(
                            height: 30,
                          ),
                        ],
                      ),
                    ),
                  );
                }
              },
            );
          } else if (snapshot.hasError) {
            return SomethingWentWrongMessage();
          } else {
            return Loader();
          }
        },
      ),
    );
  }

  Widget title({double fontsize}) {
    return Text(
      "Important Information!!",
      style: TextStyle(
        color: Pallet.fontcolor,
        fontSize: fontsize != null ? fontsize : Pallet.heading2,
        fontWeight: Pallet.font600,
      ),
    );
  }

  Widget accountdetail({double fontsize}) {
    return Text(
      "       Hello partner, this is to inform you that our system has identified that you were maintaining more than 3 accounts in the Backoffice which is against the rules mentioned in the T & C. Hence the system has deactivated all the other accounts keeping only the first three accounts active and moved all the account balances of the deactivated accounts to the Topmost First account for your information. To Simplify the process of Login & to access all the three accounts with a single login we will be only allowing single mail ID (The First & topmost one) and a Single password associated with the main account.",
      style: TextStyle(
        color: Pallet.fontcolor,
        fontSize: fontsize != null ? fontsize : Pallet.heading3,
      ),
    );
  }

  Widget info1({double fontsize}) {
    return Text(
      "You can now login to any of the above accounts using the same password and swap between accounts in the same login session without having to Log out.",
      style: TextStyle(
        color: Pallet.fontcolor,
        fontSize: fontsize != null ? fontsize : Pallet.heading3,
      ),
    );
  }

  Widget info2({double fontsize}) {
    return Text(
      "This is a one-time confirmation process & if you agree to the above condition, please show your consent “I Agree” and continue to use you Backoffice account normally.",
      style: TextStyle(
        color: Pallet.fontcolor,
        fontSize: fontsize != null ? fontsize : Pallet.heading3,
      ),
    );
  }

  Widget info3({double fontsize}) {
    return Text(
      "In case you want to amend your main email ID which is different from what is mentioned above then you are allowed only once to choose another email ID from any of your other existing mail ID’s in Backoffice. Choose “Contact Support” Button to change your Main Email ID (support@centuriongm.com)",
      style: TextStyle(
        color: Pallet.fontcolor,
        fontSize: fontsize != null ? fontsize : Pallet.heading4,
      ),
    );
  }

  String dropval = '';
  Widget acclist({double fontsize}) {
    return Column(
      children: [
        // for (var i = 0; i < 3; i++)
        usercard(
            fontsize: fontsize,
            type: 'Primary Account',
            email: primaryacc.toString().split('@')[0].substring(
                    0, primaryacc.toString().split('@')[0].length - 3) +
                '***@' +
                primaryacc.toString().split('@')[1]),
        usercard(
          fontsize: fontsize,
          type: 'Secondary Account',
          // list: 'deepak - xvbnm@vb.com,\ndeepak2 - ghf@sdfghjj.cvb'
          list: secondaryacc,
        ),

        Padding(
          padding: const EdgeInsets.symmetric(vertical: 4),
          child: Row(
            children: [
              Expanded(
                child: Container(
                  padding: const EdgeInsets.symmetric(horizontal: 8),
                  child: Center(
                    child: Text(
                      'InActive Accounts',
                      style: TextStyle(
                        color: Pallet.fontcolor,
                        fontSize: fontsize != null ? fontsize : Pallet.heading4,
                      ),
                    ),
                  ),
                ),
              ),
              Text(
                '-',
                style: TextStyle(
                  color: Pallet.fontcolor,
                  fontSize: fontsize != null ? fontsize : Pallet.heading4,
                ),
              ),
              Expanded(
                child: Container(
                  padding: const EdgeInsets.symmetric(horizontal: 8),
                  child: Container(
                    decoration: BoxDecoration(
                      color: Pallet.popupcontainerback,
                      borderRadius: BorderRadius.circular(10.0),
                      // color: Pallet.component,
                    ),
                    child: DropDownField(
                      isclosedenabled: false,
                      controller: _emailcontroller,
                      onValueChanged: (value) {
                        setState(() {
                          dropval = '';
                          _emailcontroller.text = '';
                        });
                      },
                      items: emaillist,
                      divcolor: Pallet.fontcolor,
                      labelStyle: TextStyle(
                          color: Pallet.fontcolor,
                          fontSize: 12.0,
                          fontWeight: Pallet.font500),
                      enabled: true,
                      value: dropval,
                      // itemsVisibleInDropdown: 3,
                      textStyle: TextStyle(
                          color: Pallet.fontcolor, fontSize: Pallet.heading5),
                      required: false,
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }

  Widget usercard({double fontsize, String type, List list, String email}) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 4),
      child: Row(
        children: [
          Expanded(
            child: Container(
              padding: const EdgeInsets.symmetric(horizontal: 8),
              child: Center(
                child: Text(
                  type,
                  style: TextStyle(
                    color: Pallet.fontcolor,
                    fontSize: fontsize != null ? fontsize : Pallet.heading4,
                  ),
                ),
              ),
            ),
          ),
          Text(
            '-',
            style: TextStyle(
              color: Pallet.fontcolor,
              fontSize: fontsize != null ? fontsize : Pallet.heading4,
            ),
          ),
          Expanded(
            child: Container(
              padding: const EdgeInsets.symmetric(horizontal: 8),
              child: email != null
                  ? Center(
                      child: Text(
                        email.toString(),
                        style: TextStyle(
                          color: Pallet.fontcolor,
                          fontSize:
                              fontsize != null ? fontsize : Pallet.heading4,
                        ),
                      ),
                    )
                  : Column(
                      children: [
                        for (var i = 0; i < list.length; i++)
                          Text(
                            list[i]['username'] +
                                ' - ' +
                                list[i]['email']
                                    .toString()
                                    .split('@')[0]
                                    .substring(
                                        0,
                                        list[i]['email']
                                                        .toString()
                                                        .split('@')[0]
                                                        .length -
                                                    list[i]['email']
                                                        .toString()
                                                        .split('@')[0]
                                                        .length >
                                                3
                                            ? 3
                                            : 1) +
                                '***@' +
                                list[i]['email'].toString().split('@')[1],
                            style: TextStyle(
                              color: Pallet.fontcolor,
                              fontSize:
                                  fontsize != null ? fontsize : Pallet.heading4,
                            ),
                          ),
                      ],
                    ),
            ),
          ),
        ],
      ),
    );
  }

  Widget buttons({double fontsize}) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        CustomButton1(
          text: "Contact Support",
          vpadding: 8,
          hpadding: 10,
          onpress: () {
            launch("mailto:support@centuriongm.com");
          },
        ),
        CustomButton1(
          text: "Yes,I Agree",
          vpadding: 8,
          hpadding: 10,
          onpress: () {
            Navigator.pushReplacement(
              context,
              MaterialPageRoute(
                builder: (BuildContext context) => Login(),
              ),
            );
          },
        )
      ],
    );
  }
}
