import 'package:centurion/lottery/common.dart';
import 'package:centurion/lottery/config.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';

class Intropopups {
  popupwaiter() async {
    await Future.delayed(Duration(milliseconds: 1000));
    return 'start';
  }

  intropopup(BuildContext c1) async {
    await showDialog(
        barrierDismissible: false,
        context: c1,
        builder: (BuildContext context) {
          double width = MediaQuery.of(context).size.width;
          double height = MediaQuery.of(context).size.height;

          return AlertDialog(
            scrollable: true,
            elevation: 0,
            titlePadding: EdgeInsets.all(0),
            backgroundColor: Colors.transparent,
            title: GestureDetector(
              onTap: () async {
                Navigator.pop(context);
                var val = await lottery.lotterypopup(context);
                if (val = null) {
                  print("hello its null");
                } else {
                  print(val);
                }
              },
              child: MouseRegion(
                cursor: SystemMouseCursors.click,
                child: Container(
                  // margin: EdgeInsets.fromLTRB(30, 0, 0, 20),
                  // width: width > 500 ? 700 : 400,
                  // height: width > 500 ? 450 : 250,
                  height: height / 1.5,
                  width: width / 1.8,
                  decoration: BoxDecoration(
                    image: DecorationImage(
                      image: AssetImage('lottery_intro.gif'),
                      fit: BoxFit.fill,
                    ),
                  ),
                  child: FutureBuilder(
                      future: popupwaiter(),
                      builder: (context, snapshot) {
                        if (snapshot.hasData) {
                          return Stack(
                            children: [
                              Positioned(
                                // top: width > 500 ? 80 : 30,
                                // right: width > 500
                                //     ? 130
                                //     : width <= 500 && width > 350
                                //         ? 60
                                //         : 40,
                                top: height > 940
                                    ? 130
                                    : height > 630 && height <= 768
                                        ? 80
                                        : height > 840 && height <= 940
                                            ? 110
                                            : height > 768 && height <= 840
                                                ? 90
                                                : 60,
                                right: width > 1450
                                    ? 190
                                    : width > 1100 && width <= 1450
                                        ? 150
                                        : width > 950 && width <= 1100
                                            ? 120
                                            : width > 700 && width <= 950
                                                ? 90
                                                : width > 425 && width <= 700
                                                    ? 60
                                                    : 40,
                                child: IconButton(
                                    icon: Icon(
                                      Icons.close,
                                      size: 25,
                                      color: Colors.grey[200],
                                    ),
                                    onPressed: () {
                                      Navigator.pop(context);
                                    }),
                              )
                            ],
                          );
                        } else {
                          return Container();
                        }
                      }),
                  // child: Row(
                  //   children: [
                  //     Column(
                  //       crossAxisAlignment: CrossAxisAlignment.start,
                  //       mainAxisAlignment: MainAxisAlignment.center,
                  //       children: [
                  //         Container(
                  //           padding: EdgeInsets.only(
                  //               left:
                  //                   MediaQuery.of(context).size.width > 500 ? 30 : 30),
                  //           child: Image.asset(
                  //             'introtitle.png',
                  //             width:
                  //                 MediaQuery.of(context).size.width > 500 ? 150 : 100,
                  //             height: MediaQuery.of(context).size.width > 500 ? 60 : 40,
                  //           ),
                  //         ),
                  //         // SizedBox(
                  //         //   height: 3,
                  //         // ),
                  //         Container(
                  //           padding: EdgeInsets.only(
                  //               left:
                  //                   MediaQuery.of(context).size.width > 500 ? 60 : 30),
                  //           width: MediaQuery.of(context).size.width > 500 ? 210 : 170,
                  //           child: Wrap(
                  //             children: [
                  //               Text(
                  //                 "Win Jackpot After Jackpot And Super Prizes in the Centurion Lottery",
                  //                 style: TextStyle(
                  //                   color: Colors.white,
                  //                   fontSize: MediaQuery.of(context).size.width > 500
                  //                       ? Pallet1.heading1
                  //                       : Pallet1.heading4,
                  //                 ),
                  //               ),
                  //             ],
                  //           ),
                  //         ),
                  //         SizedBox(
                  //           height: 10,
                  //         ),
                  //         Padding(
                  //           padding: EdgeInsets.only(
                  //               left:
                  //                   MediaQuery.of(context).size.width > 500 ? 60 : 30),
                  //           child: Row(
                  //             children: [
                  //               PopupButton1(
                  //                 buttoncolor: Color(0xFFed9b39),
                  //                 textcolor: Colors.white,
                  //                 borderradius: 10,
                  //                 onpress: () async {
                  //                   Navigator.pop(context);
                  //                   var val = await lottery.lotterypopup(context);
                  //                   if (val = null) {
                  //                     print("hello its null");
                  //                   } else {
                  //                     print(val);
                  //                   }
                  //                   // Navigator.push(
                  //                   //     context,
                  //                   //     MaterialPageRoute(
                  //                   //         builder: (BuildContext context)=>));
                  //                 },
                  //                 text: 'Play Now',
                  //               ),
                  //             ],
                  //           ),
                  //         ),
                  //       ],
                  //     ),
                  //     Expanded(
                  //       child: Column(
                  //         crossAxisAlignment: CrossAxisAlignment.end,
                  //         children: [
                  //           Padding(
                  //             padding: EdgeInsets.only(
                  //                 top: MediaQuery.of(context).size.width > 500 ? 20 : 5,
                  //                 right:
                  //                     MediaQuery.of(context).size.width > 500 ? 30 : 5),
                  //             child: InkWell(
                  //               onTap: () {
                  //                 Navigator.pop(context);
                  //               },
                  //               child: Container(
                  //                 padding: EdgeInsets.all(4),
                  //                 decoration: BoxDecoration(
                  //                   borderRadius: BorderRadius.circular(5),
                  //                   // border: Border.all(color: Colors.white),
                  //                 ),
                  //                 child: Icon(
                  //                   Icons.close,
                  //                   color: Colors.white,
                  //                   size: MediaQuery.of(context).size.width > 500
                  //                       ? 25
                  //                       : 18,
                  //                 ),
                  //               ),
                  //             ),
                  //           ),
                  //         ],
                  //       ),
                  //     ),
                  //   ],
                  // ),
                ),
              ),
            ),
          );
        });
  }
}

Intropopups lotteryintro = Intropopups();
