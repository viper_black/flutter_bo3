import 'package:centurion/config/app_settings.dart';
import 'package:centurion/lottery/common.dart';
import 'package:flutter/material.dart';
// import 'package:graphview/GraphView.dart';
// import '../common.dart';
import '../services/communication/http/index.dart';
import '../utils/utils.dart';
import 'config.dart';

List splNumbers = [0, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89];

class LotteryHistory extends StatefulWidget {
  final double wdgtWidth, wdgtHeight;

  const LotteryHistory({Key key, this.wdgtWidth, this.wdgtHeight})
      : super(key: key);
  @override
  _LotteryHistoryState createState() => _LotteryHistoryState();
}

class _LotteryHistoryState extends State<LotteryHistory> {
  bool isMobile = false;
  // AssetImage dummycall;

  int selectedIndex;
  Map<String, dynamic> map = {
    'product_id': 1,
  };
  List data = [];
  List unProcessed = [];
  Future<String> temp;

  void initState() {
    temp = lotteryHistory(map);
    super.initState();
  }

  Future<String> lotteryHistory(Map map) async {
    Map<String, dynamic> result =
        await HttpRequest.Post('lottery_History', Utils.constructPayload(map));
    if (Utils.isServerError(result)) {
      print('wowowowowwowowowwowow');
      return throw (await Utils.getMessage(result['response']['error']));
    } else {
      print('fff');

      data = result['response']['data']['history'];
      unProcessed = result['response']['data']['unprocessed'];
      print('DDDDDDDDDDDDDDDDDDDDDDDDDDDD');
      print(result['response']['data']['history']);
      print('.............................');
      print(unProcessed);
      return 'starrt';
    }
  }

  popupwaiter() async {
    await Future.delayed(Duration(milliseconds: 600));
    return 'start';
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<Object>(
        future: temp,
        builder: (context, snapshot) {
          if (snapshot.hasError) {
            return SomethingWentWrongMessage();
          } else if (snapshot.hasData) {
            return LayoutBuilder(builder: (context, constraints) {
              if (constraints.maxWidth <= 600) {
                isMobile = true;
                return Padding(
                  padding: EdgeInsets.all(Pallet1.defaultPadding),
                  child: historyList(widget.wdgtWidth),
                );
              } else if (constraints.maxWidth > 600 &&
                  constraints.maxWidth <= 950) {
                isMobile = false;

                return Padding(
                  padding: EdgeInsets.all(Pallet1.defaultPadding),
                  child: historyList(widget.wdgtWidth),
                );
              } else if (constraints.maxWidth > 950 &&
                  constraints.maxWidth <= 1200) {
                isMobile = false;

                return Padding(
                  padding: EdgeInsets.only(left: Pallet1.leftPadding),
                  child: historyList(widget.wdgtWidth * .6),
                );
              } else {
                isMobile = false;

                return Padding(
                  padding: EdgeInsets.all(Pallet1.leftPadding),
                  child: historyList(widget.wdgtWidth * .5),
                );
              }
            });
          } else {
            return Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Container(
                  height: 1,
                  width: 1,
                  decoration: BoxDecoration(
                      image: DecorationImage(
                          image: AssetImage(
                            isMobile == true
                                ? 'historyLotteryMobile.png'
                                : 'lotteryHistory.png',
                          ),
                          fit: BoxFit.fill)),
                ),
                Loader(),
              ],
            );
          }
        });
  }

  Widget historyList(wdgtWidth) {
    return Container(
      width: wdgtWidth,
      child: ListView(
        children: [
          Text('My Lottery Tickets',
              style: TextStyle(
                  color: Pallet1.fontcolornew,
                  fontSize: Pallet1.heading1,
                  fontWeight: Pallet1.font600)),
          SizedBox(
            height: 20,
          ),
          unProcessed.length == 0
              ? Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text("No Lottery Tickets Found",
                        style: TextStyle(
                            color: Pallet1.fontcolornew,
                            fontSize: Pallet1.heading4,
                            fontWeight: Pallet1.font500)),
                  ],
                )
              : Container(),
          Column(
            children: [
              for (var i = 0; i < unProcessed.length; i++)
                SingleChildScrollView(
                  // scrollDirection: Axis.horizontal,
                  child: Container(
                    decoration: BoxDecoration(
                        image: DecorationImage(
                            image: AssetImage(
                              isMobile == true
                                  ? 'historyLotteryMobile.png'
                                  : 'lotteryHistory.png',
                            ),
                            fit: BoxFit.fill)),
                    child: FutureBuilder(
                        future: popupwaiter(),
                        builder: (context, snapshot) {
                          if (snapshot.hasData) {
                            return Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Container(
                                    padding: EdgeInsets.all(
                                        Pallet1.defaultPadding * 2),
                                    width: wdgtWidth,
                                    child: Padding(
                                      padding: EdgeInsets.all(
                                          Pallet1.defaultPadding),
                                      child: Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          Container(
                                            padding: EdgeInsets.only(
                                              right: 10,
                                            ),
                                            child: Row(
                                              mainAxisAlignment:
                                                  MainAxisAlignment
                                                      .spaceBetween,
                                              children: [
                                                Text(
                                                    'Ticket Id : ' +
                                                        'LOT' +
                                                        unProcessed[i]['id']
                                                            .toString(),
                                                    style: TextStyle(
                                                        color:
                                                            Pallet1.fontcolor,
                                                        fontSize:
                                                            Pallet1.heading4)),
                                                Text(
                                                    'Date : ' +
                                                        unProcessed[i]
                                                                ['created_at']
                                                            .toString()
                                                            .split('T')[0]
                                                            .toString(),
                                                    style: TextStyle(
                                                        color:
                                                            Pallet1.fontcolor,
                                                        fontSize:
                                                            Pallet1.heading4)),
                                              ],
                                            ),
                                          ),
                                          SizedBox(height: 10),
                                          rowDailyProcess(unProcessed[i], true),
                                          SizedBox(height: 10),
                                          rowDailyProcess(
                                              unProcessed[i], false),
                                          SizedBox(height: 10),
                                          Text("Spcial Number",
                                              style: TextStyle(
                                                  color: Colors.white,
                                                  fontWeight: FontWeight.w600)),
                                          SizedBox(height: 7),
                                          Wrap(
                                            runSpacing: 10,
                                            spacing: 10,
                                            children: [
                                              for (var number in unProcessed[i]
                                                  ['numbers'])
                                                if (splNumbers
                                                        .contains(number) ==
                                                    true)
                                                  Container(
                                                    // padding: EdgeInsets.symmetric(vertical: Pallet1.defaultPadding),
                                                    width: 40,
                                                    height: 35,
                                                    padding: EdgeInsets.all(3),

                                                    decoration: BoxDecoration(
                                                        color:
                                                            Color(0xFF3a3a7a),
                                                        // border: Border.all(
                                                        //     color: Pallet1
                                                        //         .dashsmallcontainerback),
                                                        borderRadius:
                                                            BorderRadius
                                                                .circular(Pallet1
                                                                    .radius)),
                                                    child: CustomPaint(
                                                      size: Size(40,
                                                          (40 * 1).toDouble()),
                                                      painter:
                                                          StarCustomPainter(),
                                                      child: Center(
                                                        child: Text(
                                                          number.toString(),
                                                          style: TextStyle(
                                                              color: Pallet1
                                                                  .fontcolor,
                                                              fontSize: Pallet1
                                                                  .heading4),
                                                        ),
                                                      ),
                                                    ),
                                                  ),
                                            ],
                                          ),
                                          SizedBox(height: 10),
                                          Text("Basic Number",
                                              style: TextStyle(
                                                  color: Colors.white,
                                                  fontWeight: FontWeight.w600)),
                                          SizedBox(height: 7),
                                          Wrap(
                                            runSpacing: 10,
                                            spacing: 10,
                                            children: [
                                              for (var number in unProcessed[i]
                                                  ['numbers'])
                                                if (splNumbers
                                                        .contains(number) !=
                                                    true)
                                                  Container(
                                                    // padding: EdgeInsets.symmetric(vertical: Pallet1.defaultPadding),
                                                    width: 40,
                                                    height: 35,
                                                    decoration: BoxDecoration(
                                                        color:
                                                            Color(0xFFf3a03a),
                                                        border: Border.all(
                                                            color: Colors.grey),
                                                        borderRadius:
                                                            BorderRadius
                                                                .circular(Pallet1
                                                                    .radius)),
                                                    child: Center(
                                                      child: Text(
                                                        number.toString(),
                                                        style: TextStyle(
                                                            color: Pallet1
                                                                .fontcolor,
                                                            fontSize: Pallet1
                                                                .heading4),
                                                      ),
                                                    ),
                                                  ),
                                            ],
                                          ),
                                          // for (var number in item['numbers'])
                                          //   Container(
                                          //     // padding: EdgeInsets.symmetric(vertical: Pallet1.defaultPadding),
                                          //     width: 40,
                                          //     height: 35,
                                          //     decoration: BoxDecoration(
                                          //         color: (splNumbers
                                          //                     .contains(number)) ==
                                          //                 true
                                          //             ? Pallet1.dashsmallcontainerback
                                          //             : Color(0xFFf3a03a),
                                          //         border: Border.all(
                                          //             color: (splNumbers
                                          //                         .contains(number)) ==
                                          //                     true
                                          //                 ? Pallet1
                                          //                     .dashsmallcontainerback
                                          //                 : Colors.grey),
                                          //         borderRadius: BorderRadius.circular(
                                          //             Pallet1.radius)),
                                          //     child: (splNumbers.contains(number)) ==
                                          //             true
                                          //         ? CustomPaint(
                                          //             size:
                                          //                 Size(40, (40 * 1).toDouble()),
                                          //             painter: StarCustomPainter(),
                                          //             child: Center(
                                          //               child: Text(
                                          //                 number.toString(),
                                          //                 style: TextStyle(
                                          //                     color: Pallet1.fontcolor,
                                          //                     fontSize:
                                          //                         Pallet1.heading4),
                                          //               ),
                                          //             ),
                                          //           )
                                          //         : Center(
                                          //             child: Text(
                                          //               number.toString(),
                                          //               style: TextStyle(
                                          //                   color: Pallet1.fontcolor,
                                          //                   fontSize: Pallet1.heading4),
                                          //             ),
                                          //           ),
                                          //   ),
                                          // UnprocessedTickets(
                                          //   item: unProcessed[i],
                                          // ),
                                          SizedBox(height: 10),
                                          if (unProcessed[i]['lucky_numbers'] !=
                                              null)
                                            Text("Lucky Numbers :",
                                                style: TextStyle(
                                                    color: Colors.white,
                                                    fontWeight:
                                                        FontWeight.w600)),
                                          if (unProcessed[i]['lucky_numbers'] !=
                                              null)
                                            SizedBox(height: 5),
                                          if (unProcessed[i]['lucky_numbers'] !=
                                              null)
                                            for (var items in unProcessed[i]
                                                ['lucky_numbers'])
                                              Padding(
                                                padding:
                                                    const EdgeInsets.symmetric(
                                                        vertical: 5),
                                                child: Row(
                                                  children: [
                                                    // for (var i = 0;
                                                    //     i <
                                                    //         items['lucky_numbers']
                                                    //             .length;
                                                    //     i++)
                                                    if (items['day']
                                                            .toString()
                                                            .trim()
                                                            .toLowerCase() ==
                                                        'sunday')
                                                      Text(
                                                        'weekly Draw : ',
                                                        style: TextStyle(
                                                            color: Colors.white,
                                                            fontWeight:
                                                                FontWeight
                                                                    .w600),
                                                      )
                                                    else
                                                      Text(
                                                        'Daily Draw : ',
                                                        style: TextStyle(
                                                            color: Colors.white,
                                                            fontWeight:
                                                                FontWeight
                                                                    .w600),
                                                      ),

                                                    Expanded(
                                                      child: Wrap(
                                                        spacing: 10,
                                                        runSpacing: 10,
                                                        children: [
                                                          for (var i = 0;
                                                              i <
                                                                  items['lucky_numbers']
                                                                      .length;
                                                              i++)
                                                            // Text(items['lucky_numbers'][i]
                                                            //     .toString())

                                                            LuckyNumbers(
                                                                items: items[
                                                                        'lucky_numbers'][i]
                                                                    .toString()),
                                                        ],
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                              ),
                                          // if (unProcessed[i]['lucky_numbers'] != null)
                                          //   Column(
                                          //     crossAxisAlignment:
                                          //         CrossAxisAlignment.start,
                                          //     children: [
                                          //       Container(
                                          //         padding: EdgeInsets.symmetric(
                                          //             horizontal: 5),
                                          //         child: Text('Lucky Numbers :',
                                          //             style: TextStyle(
                                          //                 color: Pallet1.fontcolor,
                                          //                 fontSize: Pallet1.heading4)),
                                          //       ),
                                          //       SizedBox(height: 10),
                                          //       // Wrap(
                                          //       //   spacing: 10,
                                          //       //   runSpacing: 10,
                                          //       //   children: [
                                          //       //     if (unProcessed[i]
                                          //       //             ['lucky_numbers'] !=
                                          //       //         null)
                                          //       //       for (var items in unProcessed[i]
                                          //       //           ['lucky_numbers'])
                                          //       //         LuckyNumbers(items: items),
                                          //       //   ],
                                          //       // ),
                                          //     ],
                                          //   )
                                          // JackpotPercent(
                                          //     data:
                                          //         data,
                                          //     i: i)
                                        ],
                                      ),
                                    ),
                                  ),
                                ]);
                          } else {
                            return Container();
                          }
                        }),
                  ),
                )
            ],
          ),
          SizedBox(
            height: 10,
          ),
          data.length == 0
              ? Container()
              : Text('Purchase History',
                  style: TextStyle(
                      color: Pallet1.fontcolornew,
                      fontSize: Pallet1.heading1,
                      fontWeight: Pallet1.font600)),
          if (data.length == 0)
            SizedBox(
              height: 20,
            ),
          for (var i = 0; i < data.length; i++)
            InkWell(
              onTap: () {
                setState(() {
                  if (selectedIndex == i) {
                    selectedIndex = null;
                  } else {
                    selectedIndex = i;
                  }
                });
              },
              child: Padding(
                padding: EdgeInsets.symmetric(vertical: Pallet1.defaultPadding),
                child: Container(
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(Pallet1.radius)),
                  child: Column(
                    children: [
                      Container(
                        padding: EdgeInsets.all(Pallet1.defaultPadding),
                        decoration: BoxDecoration(
                            color: selectedIndex == i
                                ? Colors.blue[100]
                                : Colors.grey[200],
                            border: Border.all(color: Colors.black),
                            borderRadius:
                                BorderRadius.circular(Pallet1.radius)),
                        child: Row(
                          // mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Expanded(
                              child: Text(
                                  data[i]['created_at']
                                      .toString()
                                      .split('T')[0]
                                      //      +
                                      // " " +
                                      // data[i]['created_at']
                                      //     .toString()
                                      //     .split('T')[1]
                                      //     .toString()
                                      //     .split('.')[0]
                                      .toString(),
                                  style: TextStyle(
                                      color: Pallet1.fontcolornew,
                                      fontSize: Pallet1.heading4)),
                            ),
                            Expanded(
                              child: Image.asset(
                                'shadowTicket.png',
                                color: Pallet1.fontcolornew,
                                height: 30,
                                width: 30,
                              ),
                            ),
                            Expanded(
                              child: Text('LOT' + data[i]['id'].toString(),
                                  style: TextStyle(
                                      color: Pallet1.fontcolornew,
                                      fontSize: Pallet1.heading4)),
                            ),
                          ],
                        ),
                      ),
                      selectedIndex == i
                          ? SingleChildScrollView(
                              // scrollDirection: Axis.horizontal,
                              child: Container(
                                decoration: BoxDecoration(
                                    image: DecorationImage(
                                        image: AssetImage(
                                          isMobile == true
                                              ? 'historyLotteryMobile.png'
                                              : 'lotteryHistory.png',
                                        ),
                                        fit: BoxFit.fill)),
                                child:
                                    //  FutureBuilder(
                                    //     future: popupwaiter(),
                                    //     builder: (context, snapshot) {
                                    //       if (snapshot.hasData) {
                                    //         return
                                    Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Container(
                                      padding: EdgeInsets.all(
                                          Pallet1.defaultPadding * 2),
                                      width: wdgtWidth * .9,
                                      child: Padding(
                                        padding: EdgeInsets.all(
                                            Pallet1.defaultPadding),
                                        child: Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            Text(
                                                'Ticket Id : ' +
                                                    'LOT' +
                                                    data[i]['id'].toString(),
                                                style: TextStyle(
                                                    color: Pallet1.fontcolor,
                                                    fontSize:
                                                        Pallet1.heading4)),
                                            SizedBox(height: 10),
                                            rowDailyProcess(data[i], true),
                                            SizedBox(height: 10),
                                            rowDailyProcess(data[i], false),
                                            SizedBox(height: 10),
                                            UnprocessedTickets(
                                              item: data[i],
                                            ),
                                            SizedBox(height: 10),

                                            if (data[i]['lucky_numbers'] !=
                                                null)
                                              Column(
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.start,
                                                children: [
                                                  Container(
                                                    // padding:
                                                    //     EdgeInsets.symmetric(
                                                    //         horizontal: 5),
                                                    child: Text(
                                                        'Lucky Numbers :',
                                                        style: TextStyle(
                                                            color: Pallet1
                                                                .fontcolor,
                                                            fontSize: Pallet1
                                                                .heading4)),
                                                  ),
                                                  SizedBox(height: 5),
                                                  if (data[i]
                                                          ['lucky_numbers'] !=
                                                      null)
                                                    for (var items in data[i]
                                                        ['lucky_numbers'])
                                                      Padding(
                                                        padding:
                                                            const EdgeInsets
                                                                    .symmetric(
                                                                vertical: 5),
                                                        child: Row(
                                                          children: [
                                                            // for (var i = 0;
                                                            //     i <
                                                            //         items['lucky_numbers']
                                                            //             .length;
                                                            //     i++)
                                                            if (items['day']
                                                                    .toString()
                                                                    .trim()
                                                                    .toLowerCase() ==
                                                                'sunday')
                                                              Text(
                                                                'weekly Draw : ',
                                                                style: TextStyle(
                                                                    color: Colors
                                                                        .white,
                                                                    fontWeight:
                                                                        FontWeight
                                                                            .w600),
                                                              )
                                                            else
                                                              Text(
                                                                'Daily Draw : ',
                                                                style: TextStyle(
                                                                    color: Colors
                                                                        .white,
                                                                    fontWeight:
                                                                        FontWeight
                                                                            .w600),
                                                              ),

                                                            Expanded(
                                                              child: Wrap(
                                                                spacing: 10,
                                                                runSpacing: 10,
                                                                children: [
                                                                  for (var i =
                                                                          0;
                                                                      i <
                                                                          items['lucky_numbers']
                                                                              .length;
                                                                      i++)
                                                                    // Text(items['lucky_numbers'][i]
                                                                    //     .toString())

                                                                    LuckyNumbers(
                                                                        items: items['lucky_numbers'][i]
                                                                            .toString()),
                                                                ],
                                                              ),
                                                            ),
                                                          ],
                                                        ),
                                                      ),
                                                ],
                                              )
                                            // JackpotPercent(
                                            //     data:
                                            //         data,
                                            //     i: i)
                                          ],
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                                // } else {
                                //   return Container(
                                //     height: 250,
                                //     child: Center(
                                //       child: Loader(),
                                //     ),
                                //   );
                                // }
                                // }),
                              ),
                            )
                          : Container(
                              color: Colors.yellow,
                            )
                    ],
                  ),
                ),
              ),
            )
        ],
      ),
    );
  }

  Container rowDailyProcess(item, bool isDaily) {
    return Container(
      width: 200,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(isDaily == true ? 'Daily Draw :' : 'Weekly Draw :',
              style: TextStyle(
                  color: Pallet1.fontcolor, fontSize: Pallet1.heading4)),
          SizedBox(width: 10),
          Container(
            width: 35,
            height: 35,
            padding: EdgeInsets.all(Pallet1.defaultPadding),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(Pallet1.radius),
              color: Colors.grey[200],
            ),
            child: isDaily == true
                ? item['daily_processed'] == true &&
                        item['daily_winner'] == true
                    ? Center(
                        child: Container(
                            // width: 35,
                            // height: 35,
                            decoration: BoxDecoration(
                              // borderRadius: BorderRadius.circular(Pallet1.radius),
                              shape: BoxShape.circle,
                              color: Pallet1.fontcolornew,
                            ),
                            child: Icon(
                              Icons.check,
                              size: 15,
                              color: Pallet1.fontcolor,
                            )),
                      )
                    : Center(child: Text('--'))
                : item['weekly_processed'] == true &&
                        item['weekly_winner'] == true
                    ? Center(
                        child: Container(
                            // width: 35,
                            // height: 35,
                            decoration: BoxDecoration(
                              // borderRadius: BorderRadius.circular(Pallet1.radius),
                              shape: BoxShape.circle,

                              color: Pallet1.fontcolornew,
                            ),
                            child: Icon(
                              Icons.check,
                              size: 15,
                              color: Pallet1.fontcolor,
                            )),
                      )
                    : Center(child: Text('--')),
          )
        ],
      ),
    );
  }
}

class JackpotPercent extends StatelessWidget {
  const JackpotPercent({
    Key key,
    @required this.data,
    @required this.i,
  }) : super(key: key);

  final List data;
  final int i;

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Text('Next Week Jackpot %(Cash : DUC)',
            style: TextStyle(
                color: Pallet1.fontcolornew, fontSize: Pallet1.normalfont)),
        SizedBox(width: 10),
        Text(
            data[i]["jackpot_percentage"].toString() +
                '% ' +
                ': ' +
                (100 - num.parse(data[i]["jackpot_percentage"])).toString() +
                '%',
            style: TextStyle(
                color: Pallet1.fontcolornew, fontSize: Pallet1.normalfont)),
      ],
    );
  }
}

class LuckyNumbers extends StatelessWidget {
  const LuckyNumbers({
    Key key,
    @required this.items,
  }) : super(key: key);

  final items;

  @override
  Widget build(BuildContext context) {
    return items == null
        ? Row(
            children: [
              for (var i = 0; i < 5; i++)
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Container(
                    width: 40,
                    height: 35,
                    decoration: BoxDecoration(
                      color: Color(0xFFf3a03a),
                      shape: BoxShape.circle,
                      border: Border.all(color: Colors.grey),
                    ),
                  ),
                )
            ],
          )
        : Container(
            // padding: EdgeInsets.symmetric(vertical: Pallet1.defaultPadding/5),
            width: 40,
            height: 35,
            padding: EdgeInsets.all(3),
            decoration: BoxDecoration(
                color: (splNumbers.contains(int.parse(items))) == true
                    ? Color(0xFF3a3a7a)
                    : Color(0xFFf3a03a),
                border: Border.all(
                    color: (splNumbers.contains(int.parse(items))) == true
                        ? Color(0xFF3a3a7a)
                        : Colors.grey),
                borderRadius: BorderRadius.circular(Pallet1.radius)),
            child: (splNumbers.contains(int.parse(items))) == true
                ? CustomPaint(
                    size: Size(40, (40 * 1).toDouble()),
                    painter: StarCustomPainter(),
                    child: Center(
                      child: Text(
                        items.toString(),
                        style: TextStyle(
                            color: Pallet1.fontcolor,
                            fontSize: Pallet1.heading4),
                      ),
                    ),
                  )
                : Center(
                    child: Text(
                      items.toString(),
                      style: TextStyle(
                          color: Pallet1.fontcolor, fontSize: Pallet1.heading4),
                    ),
                  ),
          );
  }
}

class Ticket extends StatelessWidget {
  const Ticket({
    Key key,
    @required this.item,
  }) : super(key: key);

  final item;

  @override
  Widget build(BuildContext context) {
    return Text('LOT' + item['id'].toString(),
        style: TextStyle(
            color: Pallet1.fontcolor,
            fontSize: Pallet1.heading4,
            fontWeight: Pallet1.bold));
  }
}

class TextCreatedAt extends StatelessWidget {
  const TextCreatedAt({
    Key key,
    @required this.item,
  }) : super(key: key);

  final item;

  @override
  Widget build(BuildContext context) {
    return Text(
        item['created_at'].toString().split('T')[0] +
            " " +
            item['created_at']
                .toString()
                .split('T')[1]
                .toString()
                .split('.')[0]
                .toString(),
        style:
            TextStyle(color: Pallet1.fontcolornew, fontSize: Pallet1.heading4));
  }
}

class UnprocessedTickets extends StatelessWidget {
  const UnprocessedTickets({
    Key key,
    @required this.item,
  }) : super(key: key);

  final item;

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Wrap(
        spacing: 10,
        runSpacing: 10,
        children: [
          for (var number in item['numbers'])
            Container(
              // padding: EdgeInsets.symmetric(vertical: Pallet1.defaultPadding),
              width: 40,
              height: 35,
              padding: EdgeInsets.all(3),

              decoration: BoxDecoration(
                  color: (splNumbers.contains(number)) == true
                      ? Color(0xFF3a3a7a)
                      : Color(0xFFf3a03a),
                  border: Border.all(
                      color: (splNumbers.contains(number)) == true
                          ? Color(0xFF3a3a7a)
                          : Colors.grey),
                  borderRadius: BorderRadius.circular(Pallet1.radius)),
              child: (splNumbers.contains(number)) == true
                  ? CustomPaint(
                      size: Size(40, (40 * 1).toDouble()),
                      painter: StarCustomPainter(),
                      child: Center(
                        child: Text(
                          number.toString(),
                          style: TextStyle(
                              color: Pallet1.fontcolor,
                              fontSize: Pallet1.heading4),
                        ),
                      ),
                    )
                  : Center(
                      child: Text(
                        number.toString(),
                        style: TextStyle(
                            color: Pallet1.fontcolor,
                            fontSize: Pallet1.heading4),
                      ),
                    ),
            ),
        ],
      ),
    );
  }
}
