// import 'dart:math';
import 'dart:async';
import 'package:centurion/config/index_backup.dart';
import 'package:centurion/lottery/common.dart';
// import 'package:centurion/lottery/custompath.dart';
// import 'package:clippy_flutter/clippy_flutter.dart';
import 'package:confetti/confetti.dart';
import 'package:audio_session/audio_session.dart';
// import 'package:centurion/config.dart';
import 'package:centurion/services/business/account.dart';
import 'package:flutter/foundation.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'package:step_progress_indicator/step_progress_indicator.dart';
import 'package:just_audio/just_audio.dart';
import '../home.dart';
import '../services/communication/index.dart' show HttpRequest;
import '../services/business/account.dart';
import 'package:centurion/utils/utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter_countdown_timer/current_remaining_time.dart';
import 'package:flutter_countdown_timer/flutter_countdown_timer.dart';
import 'config.dart';
import 'package:bordered_text/bordered_text.dart';
import 'package:universal_html/html.dart' as html;

// class CurrentRemaininggTime {
//   final int days;
//   final int hours;
//   final int min;
//   final int sec;
//   final Animation<double> milliseconds;

//   CurrentRemaininggTime.utc(
//       {this.days, this.hours, this.min, this.sec, this.milliseconds});

//   @override
//   String toString() {
//     return 'CurrentRemainingTime{days: $days, hours: $hours, min: $min, sec: $sec, milliseconds: ${milliseconds?.value}';
//   }
// }

class WinnerAnnouncement extends StatefulWidget {
  final double wdgtWidth, wdgtHeight;

  const WinnerAnnouncement({Key key, this.wdgtWidth, this.wdgtHeight})
      : super(key: key);
  @override
  _WinnerAnnouncementState createState() => _WinnerAnnouncementState();
}

class _WinnerAnnouncementState extends State<WinnerAnnouncement>
    with TickerProviderStateMixin {
  // ignore: unused_field
  Future<bool> _temp;
  List<dynamic> data = [];
  List<dynamic> winners = [];
  List<dynamic> splNumber = [];
  bool iscalled = false;
  bool timer = true;
  bool isMobile = false;
  bool iswinner = false;
  String wintype = '';

  int endTime3;
  final AudioPlayer _player = AudioPlayer();
  bool startreveal = false;
  bool dontShow = false;
  bool isanimcalled = false;

  bool startanimation = false;

  StreamController<List> controller;
  Stream lottryDataStream;
  Sink lottryDataSink;
  ConfettiController controllerCenter =
      ConfettiController(duration: const Duration(seconds: 10));

  // functions and api calls
  Future<bool> lotteryWinner(Map map, [bool drawtimecall]) async {
    // final DateFormat formatter = DateFormat('yyyy-MM-dd');

    Map<String, dynamic> result =
        await HttpRequest.Post('lotteryWinner', Utils.constructPayload(map));
    if (Utils.isServerError(result)) {
      print(result);
      return throw (await Utils.getMessage(result['response']['error']));
    } else {
      lucky_num = [];
      print('000000|_|++++++++++++++++');
      print(result['response']['data']);
      // setState(() {
      data = result['response']['data']['config'];
      winners = result['response']['data']['winners'];
      // });
      if (result['response']['data']['isBlast'] == true) {
        setState(() {
          iswinner = true;
          wintype = result['response']['data']['winnerType'];
          // wintype = 'special';
        });
        if (drawtimecall == true) {
        } else {
          SharedPreferences prefs = await SharedPreferences.getInstance();
          var val = prefs.getBool('winnerpopupshowed');
          if (val != true) {
            winnerpopup();
          }
        }
      }
      print('STARTREVEAL: $startreveal}');

      lottryDataSink.add(result['response']['data']['config']);

      await luckynumberreveal();
      print(data);
      // controllerCenter.stop();
      // setState(() {});

      return true;
    }
  }

  bool hidetimer = false;

  Future<bool> isdrawhappened() async {
    hidetimer = true;
    animsink.add('');

    Map<String, dynamic> result =
        await HttpRequest.Post('drawishappened', Utils.constructPayload(map));
    if (Utils.isServerError(result)) {
      isdrawhappened();
      return false;
    } else {
      SharedPreferences prefs = await SharedPreferences.getInstance();
      prefs.remove('winnerpopupshowed');
      startanimation = true;
      animsink.add('');
      await Future.delayed(Duration(seconds: 7), () async {
        startreveal = true;
        luckysink.add('');
        animsink.add('');
        await lotteryWinner(map, true);

        await Future.delayed(Duration(seconds: 30), () async {
          startanimation = false;
          animsink.add('');
          startreveal = false;
          luckysink.add('');
          controllerCenter.stop();

          setState(() {
            wait = false;
            hidetimer = false;
          });
        });
      });
      return true;
    }
  }

  bool wait = false;
  bool firsttime = true;
  // GifController gifcontroller;
  // GifController numbercontroller1;
  // GifController numbercontroller2;
  // GifController numbercontroller3;
  // GifController numbercontroller4;
  // GifController numbercontroller5;
  // GifController numbercontroller6;

  Future<void> _init() async {
    final session = await AudioSession.instance;
    await session.configure(AudioSessionConfiguration.speech());
    _player.playbackEventStream.listen((event) {},
        onError: (Object e, StackTrace stackTrace) {
      print('A stream error occurred: $e');
    });
    try {
      await _player.setAudioSource(AudioSource.uri(
          Uri.parse(appSettings['SERVER_URL'] + '/lottery_audios/blast.mp3')));
    } catch (e) {
      print("Error loading audio source: $e");
    }
  }

  // ignore: missing_return
  Future<bool> testLottry(Map map) async {
    // Timer(Duration(seconds: 1), () {
    setState(() {
      wait = true;
    });
    //   setState(() {
    //     gifcontroller.repeat(min: 0, max: 120, period: Duration(seconds: 8));
    //   });
    // });

    // Timer(Duration(seconds: 8), () {
    //   setState(() {
    //     gifcontroller.stop();
    //     gifcontroller.value = 120;
    //   });
    // });
    Map<String, dynamic> result =
        await HttpRequest.Post('daily_draw', Utils.constructPayload(map));
    if (Utils.isServerError(result)) {
      print(result);
      return throw (await Utils.getMessage(result['response']['error']));
    } else {
      // controllerCenter.stop();
      // lionwaiting.evict(
      //   configuration: ImageConfiguration.empty,
      // );

      startanimation = true;
      animsink.add('');
      await Future.delayed(Duration(seconds: 7), () async {
        startreveal = true;
        luckysink.add('');
        animsink.add('');
        await lotteryWinner(map);

        await Future.delayed(Duration(seconds: 30), () async {
          startanimation = false;
          animsink.add('');
          startreveal = false;
          luckysink.add('');
          controllerCenter.stop();

          setState(() {
            wait = false;
          });
        });
      });
      return true;
    }
  }

  // ignore: missing_return
  Future<bool> testLottry2(Map map) async {
    setState(() {
      wait = true;
    });
    Map<String, dynamic> result =
        await HttpRequest.Post('weekly_draw', Utils.constructPayload(map));
    if (Utils.isServerError(result)) {
      print(result);
      return throw (await Utils.getMessage(result['response']['error']));
    } else {
      // await lotteryWinner(map);
      // setState(() {
      //   wait = false;
      // });

      // return true;

      startanimation = true;
      // lionwaiting.evict();

      animsink.add('');
      await Future.delayed(Duration(seconds: 7), () async {
        startreveal = true;
        luckysink.add('');
        animsink.add('');
        await lotteryWinner(map);

        await Future.delayed(Duration(seconds: 30), () async {
          startanimation = false;
          animsink.add('');
          startreveal = false;
          luckysink.add('');
          controllerCenter.stop();

          setState(() {
            wait = false;
          });
        });
      });
      return true;
    }
  }

  caltimer() {
    var now = new DateTime.now().toString();

    var dateParse = DateTime.parse(now);

    // var currentUTC = DateTime.now().toUtc().toString();
    // var parseCurrentUTC = DateTime.parse(currentUTC);

    endTime3 =
        DateTime.utc(dateParse.year, dateParse.month, dateParse.day, 12, 00)
            .millisecondsSinceEpoch;

    // -1620648-18000000-60000

    // endTime3=20000;

    print("xxxxxxxxxxx");
    print(endTime3);
    print(
        (DateTime.utc(dateParse.year, dateParse.month, dateParse.day, 12, 00)));
  }

  Map<String, dynamic> map = {'product_id': 1};
  // CountdownTimerController controller1;
  @override
  void initState() {
    caltimer();
    // controller1 = CountdownTimerController(endTime: endTime3, onEnd: caltimer);

    // controllerCenter =
    //     ConfettiController(duration: const Duration(seconds: 10));
    // gifcontroller = GifController(vsync: this, duration: Duration(seconds: 8));
    // numbercontroller1 =
    //     GifController(vsync: this, duration: Duration(seconds: 5));
    // numbercontroller2 =
    //     GifController(vsync: this, duration: Duration(seconds: 5));
    // numbercontroller3 =
    //     GifController(vsync: this, duration: Duration(seconds: 5));
    // numbercontroller4 =
    //     GifController(vsync: this, duration: Duration(seconds: 5));
    // numbercontroller5 =
    //     GifController(vsync: this, duration: Duration(seconds: 5));
    // numbercontroller6 =
    //     GifController(vsync: this, duration: Duration(seconds: 5));

    print('sssssssssssssssssssssssssssssssssssssssssss');
    print(dashboard.username);

    _temp = lotteryWinner(map);
    controller = StreamController<List>();
    lottryDataStream = controller.stream;
    lottryDataSink = controller.sink;

    super.initState();
  }

  stop() {
    // controllerCenter.stop();
    // timer = false;
    var now = new DateTime.now().toString();

    var dateParse = DateTime.parse(now);
    if (endTime3 == endTime3) {
      endTime3 = DateTime.utc(
              dateParse.year, dateParse.month, dateParse.day + 1, 12, 00)
          .millisecondsSinceEpoch;
      // -1620648-18000000-60000
    }
  }
// AppLifecycleState appLifecycleState = AppLifecycleState.detached;

//  @override
// void didChangeAppLifecycleState(AppLifecycleState state) {
//   appLifecycleState = state;
// }

// if (appLifecycleState == AppLifecycleState.paused) {
//   // in background
// }

  NetworkImage lionwaiting = new NetworkImage(
    appSettings['SERVER_URL'] + '/lottery_numbers/lion_waiting.gif',
  );

  @override
  void dispose() {
    animcontroller.close();
    luckycontroller.close();

    _player.stop();
    _player.dispose();
    controller.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return StreamBuilder(
        stream: lottryDataStream,
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            // controllerCenter.play();
            return LayoutBuilder(builder: (context, constraints) {
              if (constraints.maxWidth <= 425) {
                isMobile = true;
                return Padding(
                  padding: EdgeInsets.all(Pallet1.defaultPadding),
                  child: Center(
                    child: SingleChildScrollView(
                      child: Column(
                        children: [
                          winnerAnnouncement(width: widget.wdgtWidth),
                          SizedBox(
                            height: 20,
                          ),
                          recentWinners(width: widget.wdgtWidth),
                          SizedBox(
                            height: 20,
                          ),
                          luckyNumbers(width: widget.wdgtWidth)
                        ],
                      ),
                    ),
                  ),
                );
              } else if (constraints.maxWidth >= 425 &&
                  constraints.maxWidth <= 1024) {
                isMobile = false;
                return Center(
                  child: SingleChildScrollView(
                    child: Column(
                      children: [
                        winnerAnnouncement(width: widget.wdgtWidth),
                        SizedBox(
                          height: 20,
                        ),
                        recentWinners(width: widget.wdgtWidth),
                        SizedBox(
                          height: 20,
                        ),
                        luckyNumbers(width: widget.wdgtWidth)
                      ],
                    ),
                  ),
                );
              } else if (constraints.maxWidth >= 1024 &&
                  constraints.maxWidth <= 1450) {
                isMobile = false;
                return SingleChildScrollView(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Container(
                        width: widget.wdgtWidth,
                        child: Column(
                          //        mainAxisAlignment: MainAxisAlignment.center,
                          // crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            // wait == false
                            //     ? InkWell(
                            //         onTap: () async {
                            //           print('WAIT: $wait}');
                            //           if (firsttime == true) {
                            //             if (wait == false) {
                            //               await testLottry(map);
                            //               firsttime = false;
                            //             }
                            //           } else {
                            //             html.window.location.href =
                            //                 appSettings["CLIENT_URL"];
                            //           }
                            //         },
                            //         child: Text('Daily Draw'))
                            //     : Container(),
                            // wait == false
                            //     ? InkWell(
                            //         onTap: () async {
                            //           print('WAIT: $wait}');
                            //           if (firsttime == true) {
                            //             if (wait == false) {
                            //               await testLottry2(map);
                            //               firsttime = false;
                            //             }
                            //           } else {
                            //             html.window.location.href =
                            //                 appSettings["CLIENT_URL"];
                            //           }
                            //         },
                            //         child: Text('Weekly Draw'))
                            //     : Container(),
                            winnerAnnouncement(
                                width: widget.wdgtWidth * .7,
                                imgsize: widget.wdgtHeight * 0.3),
                            SizedBox(
                              height: 20,
                            ),
                            Container(
                              width: widget.wdgtWidth * 0.7,
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  recentWinners(width: widget.wdgtWidth * .34),
                                  luckyNumbers(width: widget.wdgtWidth * .34)
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                );
              } else {
                isMobile = false;
                return SingleChildScrollView(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Container(
                        width: widget.wdgtWidth,
                        child: Column(
                          //        mainAxisAlignment: MainAxisAlignment.center,
                          // crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            // wait == false
                            //     ? TextButton(
                            //         onPressed: () async {
                            //           print('WAIT: $wait}');

                            //           if (firsttime == true) {
                            //             if (wait == false) {
                            //               await testLottry(map);
                            //               firsttime = false;
                            //             }
                            //           } else {
                            //             html.window.location.href =
                            //                 appSettings["CLIENT_URL"];
                            //           }

                            //           setState(() {});
                            //         },
                            //         child: Text('Daily Draw'))
                            //     : Container(),
                            // wait == false
                            //     ? TextButton(
                            //         onPressed: () async {
                            //           if (firsttime == true) {
                            //             if (wait == false) {
                            //               await testLottry2(map);
                            //               firsttime = false;
                            //             }
                            //           } else {
                            //             html.window.location.href =
                            //                 appSettings["CLIENT_URL"];
                            //           }

                            //           setState(() {});
                            //         },
                            //         child: Text('Weekly Draw'))
                            //     : Container(),
                            winnerAnnouncement(width: widget.wdgtWidth * .7),
                            SizedBox(
                              height: 10,
                            ),
                            Container(
                              width: widget.wdgtWidth * 0.7,
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  recentWinners(width: widget.wdgtWidth * .34),
                                  luckyNumbers(width: widget.wdgtWidth * .34)
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                );
              }
            });
          } else if (snapshot.hasError) {
            return SomethingWentWrongMessage();
          } else {
            return Loader();
          }
        });
  }

  AssetImage loadingcup = AssetImage("Lottery_cup_anim.gif");

//winner page container(1st box)

  Widget winnerAnnouncement({double width, double imgsize}) {
    double swidth = MediaQuery.of(context).size.width;
    return Container(
      padding: EdgeInsets.all(Pallet1.defaultPadding),
      decoration: BoxDecoration(
          color: Color(0xFF100F31), borderRadius: BorderRadius.circular(10)),
      width: width,
      child: Stack(
        children: [
          Column(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              swidth >= 770
                  ? Container(
                      width: width * 0.85,
                      padding: EdgeInsets.symmetric(horizontal: 15),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          SizedBox(height: 15),
                          Container(
                            padding: EdgeInsets.symmetric(
                                horizontal: 20, vertical: 15),
                            decoration: BoxDecoration(
                                color: Color(0xFF3a3a7a),
                                borderRadius: BorderRadius.circular(30)),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              // mainAxisSize: MainAxisSize.min,
                              children: [
                                Expanded(
                                  flex: 40,
                                  child: Center(
                                    child: Container(
                                      // padding: EdgeInsets.only(top: 5),
                                      child: Row(
                                        mainAxisSize: MainAxisSize.min,
                                        children: [
                                          Image.asset(
                                            'super.png',
                                            width: 40,
                                          ),
                                          SizedBox(width: 5),
                                          Column(
                                            children: [
                                              Text('Jackpot Fund :',
                                                  style: TextStyle(
                                                    color: Color(0xFFed9b39),
                                                    fontSize:
                                                        Pallet1.normalfont,
                                                  )),
                                              SizedBox(height: 5),
                                              for (var item in data)
                                                Text(
                                                    "\$ " +
                                                        item['jackpot_fund']
                                                            .toString(),
                                                    style: TextStyle(
                                                      color: Pallet1.fontcolor,
                                                      fontSize: 20,
                                                    )),
                                            ],
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                ),
                                Container(
                                  width: 3,
                                  decoration: BoxDecoration(
                                      color: Color(0xFF100F31),
                                      borderRadius: BorderRadius.circular(10)),
                                  height: 30,
                                ),
                                Expanded(
                                  flex: 30,
                                  child: Center(
                                    child: Container(
                                      child: Text(
                                          ((DateTime.now().toUtc().weekday ==
                                                          6 &&
                                                      DateTime.now()
                                                              .toUtc()
                                                              .hour >
                                                          12) ||
                                                  (DateTime.now()
                                                              .toUtc()
                                                              .weekday ==
                                                          7 &&
                                                      DateTime.now()
                                                              .toUtc()
                                                              .hour <
                                                          12))
                                              ? 'Weekly Draw'
                                              : 'Daily Draw',
                                          style: TextStyle(
                                            color: Color(0xFFed9b39),
                                            fontSize: Pallet1.heading2,
                                          )),
                                    ),
                                  ),
                                ),
                                Container(
                                  width: 3,
                                  decoration: BoxDecoration(
                                      color: Color(0xFF100F31),
                                      borderRadius: BorderRadius.circular(10)),
                                  height: 30,
                                ),
                                Expanded(
                                  flex: 40,
                                  child: Center(
                                    child: Container(
                                      // padding: EdgeInsets.only(top: 5),
                                      child: Row(
                                        mainAxisSize: MainAxisSize.min,
                                        children: [
                                          Image.asset(
                                            'grandprize.png',
                                            width: 40,
                                          ),
                                          SizedBox(width: 5),
                                          Column(
                                            children: [
                                              Text('Super Jackpot Fund :',
                                                  style: TextStyle(
                                                    color: Color(0xFFed9b39),
                                                    fontSize:
                                                        Pallet1.normalfont,
                                                  )),
                                              SizedBox(height: 5),
                                              for (var item in data)
                                                Text(
                                                  "\$ " +
                                                      item['weekly_fund']
                                                          .toString(),
                                                  style: TextStyle(
                                                    color: Pallet1.fontcolor,
                                                    fontSize: 20,
                                                  ),
                                                ),
                                            ],
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          SizedBox(height: 15),

                          StreamBuilder(
                              stream: animstream,
                              builder: (context, snapshot) {
                                return imgsize == null
                                    ? startanimation == true
                                        ? Container(
                                            height: widget.wdgtHeight * 0.25,
                                            decoration: BoxDecoration(
                                                image: DecorationImage(
                                                    image: loadingcup)),
                                            // child: GifImage(
                                            //   controller: gifcontroller,
                                            //   repeat: ImageRepeat.noRepeat,
                                            //   image: AssetImage(
                                            //       "Lottery_cup_anim.gif"),
                                            // ),
                                          )
                                        : Container(
                                            height: widget.wdgtHeight * 0.25,

                                            decoration: BoxDecoration(
                                                image: DecorationImage(
                                                    image: lionwaiting)),
                                            // child: lionwaiting,
                                          )
                                    : startanimation == true
                                        ? Container(
                                            height: imgsize,
                                            decoration: BoxDecoration(
                                                image: DecorationImage(
                                                    image: loadingcup)),
                                            // child: GifImage(
                                            //   controller: gifcontroller,
                                            //   image: AssetImage(
                                            //       "Lottery_cup_anim.gif"),
                                            // ),
                                          )
                                        : Container(
                                            height: imgsize,
                                            decoration: BoxDecoration(
                                                image: DecorationImage(
                                                    image: lionwaiting)),
                                          );
                              }),
                          SizedBox(
                            height: 20,
                          ),
                          // StreamBuilder<Object>(
                          //     stream: luckystream,
                          //     builder: (context, snapshot) {
                          //       return startreveal == true
                          //           ? revealluckynumbers()
                          //           : Container();
                          //     }),
                        ],
                      ),
                    )
                  : Container(
                      width: width,
                      padding: EdgeInsets.symmetric(horizontal: 8),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          SizedBox(height: 15),
                          Container(
                            padding: EdgeInsets.symmetric(
                                horizontal: 8, vertical: 5),
                            decoration: BoxDecoration(
                                color: Color(0xFF3a3a7a),
                                borderRadius: BorderRadius.circular(20)),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              // mainAxisSize: MainAxisSize.min,
                              children: [
                                Expanded(
                                  child: Center(
                                    child: Container(
                                      // padding: EdgeInsets.only(top: 5),
                                      child: Row(
                                        mainAxisSize: MainAxisSize.min,
                                        children: [
                                          Image.asset(
                                            'super.png',
                                            width: 30,
                                          ),
                                          SizedBox(width: 2),
                                          Expanded(
                                            child: Center(
                                              child: Column(
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.start,
                                                children: [
                                                  Text('Jackpot Fund :',
                                                      style: TextStyle(
                                                        color:
                                                            Color(0xFFed9b39),
                                                        fontSize:
                                                            Pallet1.heading7,
                                                      )),
                                                  SizedBox(height: 5),
                                                  for (var item in data)
                                                    Text(
                                                        "\$ " +
                                                            item['jackpot_fund']
                                                                .toString(),
                                                        style: TextStyle(
                                                          color:
                                                              Pallet1.fontcolor,
                                                          fontSize:
                                                              Pallet1.heading7,
                                                        )),
                                                ],
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                ),
                                Container(
                                  width: 3,
                                  decoration: BoxDecoration(
                                      color: Color(0xFF100F31),
                                      borderRadius: BorderRadius.circular(10)),
                                  height: 30,
                                ),
                                Expanded(
                                  child: Center(
                                    child: Container(
                                      child: Text(
                                          ((DateTime.now().toUtc().weekday ==
                                                          6 &&
                                                      DateTime.now()
                                                              .toUtc()
                                                              .hour >
                                                          12) ||
                                                  (DateTime.now()
                                                              .toUtc()
                                                              .weekday ==
                                                          7 &&
                                                      DateTime.now()
                                                              .toUtc()
                                                              .hour <=
                                                          12))
                                              ? 'Weekly Draw'
                                              : 'Daily Draw',
                                          style: TextStyle(
                                            color: Color(0xFFed9b39),
                                            fontSize: Pallet1.heading5,
                                          )),
                                    ),
                                  ),
                                ),
                                Container(
                                  width: 3,
                                  decoration: BoxDecoration(
                                      color: Color(0xFF100F31),
                                      borderRadius: BorderRadius.circular(10)),
                                  height: 25,
                                ),
                                Expanded(
                                  child: Center(
                                    child: Container(
                                      // padding: EdgeInsets.only(top: 5),
                                      child: Row(
                                        mainAxisSize: MainAxisSize.min,
                                        children: [
                                          Image.asset(
                                            'grandprize.png',
                                            width: 30,
                                          ),
                                          SizedBox(width: 5),
                                          Expanded(
                                            child: Center(
                                              child: Column(
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.start,
                                                children: [
                                                  Text('Super Jackpot Fund :',
                                                      style: TextStyle(
                                                        color:
                                                            Color(0xFFed9b39),
                                                        fontSize:
                                                            Pallet1.heading7,
                                                      )),
                                                  SizedBox(height: 5),
                                                  for (var item in data)
                                                    Text(
                                                        "\$ " +
                                                            item['weekly_fund']
                                                                .toString(),
                                                        style: TextStyle(
                                                          color:
                                                              Pallet1.fontcolor,
                                                          fontSize:
                                                              Pallet1.heading7,
                                                        )),
                                                ],
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          SizedBox(height: 15),
                          StreamBuilder<Object>(
                              stream: animstream,
                              builder: (context, snapshot) {
                                return imgsize == null
                                    ? startanimation == true
                                        ? Container(
                                            height: widget.wdgtHeight * 0.25,
                                            decoration: BoxDecoration(
                                                image: DecorationImage(
                                                    image: loadingcup)),
                                            // child: GifImage(
                                            //   controller: gifcontroller,
                                            //   image: AssetImage(
                                            //       "Lottery_cup_anim.gif"),
                                            // ),
                                          )
                                        : Container(
                                            height: widget.wdgtHeight * 0.25,

                                            decoration: BoxDecoration(
                                                image: DecorationImage(
                                                    image: lionwaiting)),
                                            // child: lionwaiting,
                                          )
                                    : startanimation == true
                                        ? Container(
                                            height: imgsize,
                                            decoration: BoxDecoration(
                                                image: DecorationImage(
                                                    image: loadingcup)),
                                            // child: GifImage(
                                            //   controller: gifcontroller,
                                            //   image: AssetImage(
                                            //       "Lottery_cup_anim.gif"),
                                            // ),
                                          )
                                        : Container(
                                            height: imgsize,
                                            decoration: BoxDecoration(
                                                image: DecorationImage(
                                                    image: lionwaiting)),
                                          );
                              }),
                        ],
                      ),
                    ),
              SizedBox(
                height: 20,
              ),
              StreamBuilder<Object>(
                  stream: luckystream,
                  builder: (context, snapshot) {
                    return startreveal == true
                        ? Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              revealluckynumbers(),
                            ],
                          )
                        : Container();
                  }),
              SizedBox(
                height: 20,
              ),
              StreamBuilder(
                  stream: animstream,
                  builder: (context, snapshot) {
                    return hidetimer == true
                        ? Container()
                        : startreveal == true
                            ? Container()
                            : Text('Time Left for Next Lucky Draw',
                                style: TextStyle(
                                    color: Pallet1.fontcolor,
                                    fontSize: Pallet1.normalfont + 5,
                                    fontWeight: Pallet1.font600));
                  }),
              StreamBuilder<Object>(
                  stream: animstream,
                  builder: (context, snapshot) {
                    return hidetimer == true
                        ? Container()
                        : startreveal == true
                            ? Container()
                            : SizedBox(
                                height: 20,
                              );
                  }),
              Center(
                child: StreamBuilder(
                    stream: animstream,
                    builder: (context, snapshot) {
                      return hidetimer == true
                          ? Container()
                          : startreveal == true
                              ? Container()
                              : Container(
                                  // width: 250,
                                  child: CountdownTimer(
                                    // controller: controller1,
                                    endTime: endTime3,
                                    onEnd: () {
                                      stop();

                                      lotteryWinner(map);
                                    },
                                    widgetBuilder:
                                        (_, CurrentRemainingTime time) {
                                      if (time == null) {
                                        // lotteryWinner(map);
                                        // print("ddddddddddddddcccccccc");
                                        // startreveal = true;

                                        return Text('Time Out',
                                            style: TextStyle(
                                                color: Pallet1.fontcolor,
                                                fontSize: Pallet1.normalfont,
                                                fontWeight: Pallet1.bold));
                                      }
                                      if (time.days == null &&
                                          time.hours == null &&
                                          time.min == null &&
                                          time.sec == 1) {
                                        isdrawhappened();
                                        // startreveal = true;
                                        // animsink.add('');
                                      }

                                      if (isanimcalled == false) {
                                        // print('ISANIMCALLED: $isanimcalled}');
                                        // print('TIME.DAYS: ${time.hours}');
                                        // print('TIME.MIN: ${time.min}');
                                        // print('TIME.SEC: ${time.sec}');
                                        if ((time.days == null &&
                                                time.hours == 23 &&
                                                time.min == 59 &&
                                                time.sec >= 20) &&
                                            time.sec <= 59) {
                                          isanimcalled = true;
                                          lottryDataSink.add(data);
                                          isdrawhappened();
                                          print(
                                              'ISDRAWHAPPENED   true: $isdrawhappened}');
                                        }
                                      }
                                      // if (time.days == null &&
                                      //     time.hours == null &&
                                      //     time.min == null &&
                                      //     time.sec <= 9) {
                                      //   Timer(Duration(seconds: 1), () {
                                      //     setState(() {
                                      //       wait = true;
                                      //     });
                                      //     // setState(() {
                                      //     //   gifcontroller.repeat(
                                      //     //       min: 0, max: 120, period: Duration(seconds: 8));
                                      //     // });
                                      //   });

                                      //   // Timer(Duration(seconds: 8), () {
                                      //   //   setState(() {
                                      //   //     gifcontroller.stop();
                                      //   //     gifcontroller.value = 120;
                                      //   //   });
                                      //   // });
                                      //   startanimation = true;
                                      //   print('STARTANIMATION: $startanimation}');

                                      //   animsink.add('');
                                      // }

                                      // if ((time.days == null &&
                                      //         time.hours == 23 &&
                                      //         time.min == 59 &&
                                      //         time.sec >= 25) &&
                                      //     (time.days == null &&
                                      //         time.hours == 23 &&
                                      //         time.min == 59 &&
                                      //         time.sec <= 59)) {
                                      //   startanimation = true;
                                      //   // print('STARTANIMATION: $startanimation}');
                                      //   print('deepak$time}');
                                      //   animsink.add('');
                                      //   if (startreveal == false) {
                                      //     lotteryWinner(map);
                                      //   }
                                      //   startreveal = true;

                                      //   luckysink.add('');
                                      // }
                                      // if (time.days == null &&
                                      //     time.hours == 23 &&
                                      //     time.min == 59 &&
                                      //     time.sec <= 01) {
                                      //   startanimation = false;
                                      //   // print('ccccccccccccccccc$time}');

                                      //   animsink.add('');
                                      //   startreveal = false;
                                      //   controllerCenter.stop();

                                      //   luckysink.add('');
                                      // }
                                      return Column(
                                        children: [
                                          Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.center,
                                            children: [
                                              time.days == null
                                                  ? Container()
                                                  : Row(
                                                      children: [
                                                        CircularStepProgressIndicator(
                                                          totalSteps: 365,
                                                          currentStep:
                                                              time.days,
                                                          stepSize: 1,
                                                          selectedColor:
                                                              Color(0xFF356BAD),
                                                          unselectedColor:
                                                              Colors.white,
                                                          padding: 0,
                                                          width: 70,
                                                          selectedStepSize: 1,
                                                          roundedCap: (_, __) =>
                                                              true,
                                                          // gradientColor: LinearGradient(
                                                          //   colors: [
                                                          //     Color(0xFF273b89),
                                                          //     Color(0xFF265ea1),
                                                          //     Color(0xFF18e3e5),
                                                          //   ],
                                                          // ),
                                                          height: 70,
                                                          child: Center(
                                                            child: Column(
                                                              mainAxisSize:
                                                                  MainAxisSize
                                                                      .min,
                                                              children: [
                                                                Text(
                                                                    time.days.toString().length ==
                                                                            1
                                                                        ? '0' +
                                                                            '${time.days}'
                                                                        : '${time.days}',
                                                                    style: TextStyle(
                                                                        color: Pallet1
                                                                            .fontcolor,
                                                                        fontSize:
                                                                            Pallet1
                                                                                .heading3,
                                                                        fontWeight:
                                                                            Pallet1.bold)),
                                                                Text('Days',
                                                                    style: TextStyle(
                                                                        color: Pallet1
                                                                            .fontcolor,
                                                                        fontSize:
                                                                            Pallet1
                                                                                .normalfont,
                                                                        fontWeight:
                                                                            Pallet1.bold))
                                                              ],
                                                            ),
                                                          ),
                                                        ),
                                                      ],
                                                    ),
                                              SizedBox(
                                                width:
                                                    time.days == null ? 0 : 20,
                                              ),
                                              Row(
                                                children: [
                                                  CircularStepProgressIndicator(
                                                    totalSteps: 24,
                                                    currentStep:
                                                        time.hours == null
                                                            ? 0
                                                            : time.hours,
                                                    stepSize: 1,
                                                    selectedColor:
                                                        Color(0xFF356BAD),
                                                    unselectedColor:
                                                        Colors.white,
                                                    padding: 0,
                                                    width: 70,
                                                    selectedStepSize: 1,
                                                    roundedCap: (_, __) => true,
                                                    // gradientColor: LinearGradient(
                                                    //   colors: [
                                                    //     Color(0xFF273b89),
                                                    //     Color(0xFF265ea1),
                                                    //     Color(0xFF18e3e5),
                                                    //   ],
                                                    // ),
                                                    height: 70,
                                                    child: Center(
                                                      child: Column(
                                                        mainAxisSize:
                                                            MainAxisSize.min,
                                                        children: [
                                                          Text(
                                                              time.hours == null
                                                                  ? '00'
                                                                  : time.hours
                                                                              .toString()
                                                                              .length ==
                                                                          1
                                                                      ? '0' +
                                                                          '${time.hours}'
                                                                      : '${time.hours}',
                                                              style: TextStyle(
                                                                  color: Pallet1
                                                                      .fontcolor,
                                                                  fontSize: Pallet1
                                                                      .heading2,
                                                                  fontWeight:
                                                                      Pallet1
                                                                          .bold)),
                                                          Text('Hrs',
                                                              style: TextStyle(
                                                                color: Pallet1
                                                                    .fontcolor,
                                                                fontSize: Pallet1
                                                                    .normalfont,
                                                              ))
                                                        ],
                                                      ),
                                                    ),
                                                  ),
                                                ],
                                              ),
                                              SizedBox(
                                                width: 17,
                                              ),
                                              Row(
                                                children: [
                                                  CircularStepProgressIndicator(
                                                    totalSteps: 60,
                                                    currentStep:
                                                        time.min == null
                                                            ? 0
                                                            : time.min,
                                                    stepSize: 1,
                                                    selectedColor:
                                                        Color(0xFF356BAD),
                                                    unselectedColor:
                                                        Colors.white,
                                                    padding: 0,
                                                    width: 70,
                                                    selectedStepSize: 1,
                                                    roundedCap: (_, __) => true,
                                                    // gradientColor: LinearGradient(
                                                    //   colors: [
                                                    //     Color(0xFF273b89),
                                                    //     Color(0xFF265ea1),
                                                    //     Color(0xFF18e3e5),
                                                    //   ],
                                                    // ),
                                                    height: 70,
                                                    child: Center(
                                                      child: Column(
                                                        mainAxisSize:
                                                            MainAxisSize.min,
                                                        children: [
                                                          Text(
                                                              time.min == null
                                                                  ? "00"
                                                                  : time.min.toString().length ==
                                                                          1
                                                                      ? '0' +
                                                                          '${time.min}'
                                                                      : '${time.min}',
                                                              style: TextStyle(
                                                                color: Pallet1
                                                                    .fontcolor,
                                                                fontSize: Pallet1
                                                                    .heading2,
                                                              )),
                                                          Text('Mins',
                                                              style: TextStyle(
                                                                color: Pallet1
                                                                    .fontcolor,
                                                                fontSize: Pallet1
                                                                    .normalfont,
                                                              ))
                                                        ],
                                                      ),
                                                    ),
                                                  ),
                                                ],
                                              ),
                                              SizedBox(
                                                width: 17,
                                              ),
                                              Row(
                                                children: [
                                                  CircularStepProgressIndicator(
                                                    totalSteps: 60,
                                                    currentStep:
                                                        time.sec == null
                                                            ? 0
                                                            : time.sec,
                                                    stepSize: 1,
                                                    selectedColor:
                                                        Color(0xFF356BAD),
                                                    unselectedColor:
                                                        Colors.white,
                                                    padding: 0,
                                                    width: 70,
                                                    selectedStepSize: 1,
                                                    roundedCap: (_, __) => true,
                                                    // gradientColor: LinearGradient(
                                                    //   colors: [
                                                    //     Color(0xFF273b89),
                                                    //     Color(0xFF265ea1),
                                                    //     Color(0xFF18e3e5),
                                                    //   ],
                                                    // ),
                                                    height: 70,
                                                    child: Center(
                                                      child: Column(
                                                        mainAxisSize:
                                                            MainAxisSize.min,
                                                        children: [
                                                          Text(
                                                              time.sec == null
                                                                  ? "00"
                                                                  : time.sec.toString().length ==
                                                                          1
                                                                      ? '0' +
                                                                          '${time.sec}'
                                                                      : '${time.sec}',
                                                              style: TextStyle(
                                                                  color: Pallet1
                                                                      .fontcolor,
                                                                  fontSize: Pallet1
                                                                      .heading2,
                                                                  fontWeight:
                                                                      Pallet1
                                                                          .bold)),
                                                          Text('Secs ',
                                                              style: TextStyle(
                                                                color: Pallet1
                                                                    .fontcolor,
                                                                fontSize: Pallet1
                                                                    .normalfont,
                                                              )),
                                                        ],
                                                      ),
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            ],
                                          ),
                                          SizedBox(
                                            height: 10,
                                          ),
                                          text(),
                                          SizedBox(
                                            height: 10,
                                          ),
                                          // isMobile == true ? text() : Container()
                                        ],
                                      );
                                    },
                                  ),
                                );
                    }),
              ),
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              // for (var item in data)

              Stack(
                children: [
                  ConfettiWidget(
                    particleDrag: 0.05,
                    emissionFrequency: 0.15,
                    numberOfParticles: 3,
                    gravity: 0.1,
                    createParticlePath: createPath,
                    confettiController: controllerCenter,
                    blastDirectionality: BlastDirectionality.explosive,
                    shouldLoop: false,
                    colors: const [
                      Color(0xFFeac600),
                      Colors.red,
                      Colors.green,
                      Colors.yellow,
                      Colors.blue,
                    ],
                  ),
                  ConfettiWidget(
                    particleDrag: 0.05,
                    emissionFrequency: 0.15,
                    // blastDirection: pi,
                    numberOfParticles: 4,
                    gravity: 0.1,

                    confettiController: controllerCenter,

                    blastDirectionality: BlastDirectionality.explosive,
                    shouldLoop: false,
                    colors: const [
                      Color(0xFFeac600),
                      Colors.red,
                      Colors.green,
                      Colors.yellow,
                      Colors.blue,
                    ],
                  ),
                ],
              ),
              Stack(
                children: [
                  ConfettiWidget(
                    particleDrag: 0.05,
                    emissionFrequency: 0.15,
                    numberOfParticles: 3,
                    gravity: 0.1,
                    createParticlePath: createPath,
                    confettiController: controllerCenter,
                    blastDirectionality: BlastDirectionality.explosive,
                    shouldLoop: false,
                    colors: const [
                      Color(0xFFeac600),
                      Colors.red,
                      Colors.green,
                      Colors.yellow,
                      Colors.blue,
                    ],
                  ),
                  ConfettiWidget(
                    particleDrag: 0.05,
                    emissionFrequency: 0.15,
                    // blastDirection: pi,
                    numberOfParticles: 4,
                    gravity: 0.1,
                    confettiController: controllerCenter,
                    blastDirectionality: BlastDirectionality.explosive,
                    shouldLoop: false,
                    colors: const [
                      Color(0xFFeac600),
                      Colors.red,
                      Colors.green,
                      Colors.yellow,
                      Colors.blue,
                    ],
                  ),
                ],
              ),
            ],
          ),
        ],
      ),
    );
  }

  revealdelay() async {
    await Future.delayed(
      Duration(milliseconds: 3000),
    );
    return 'start';
  }

  Widget revealluckynumbers() {
    return StreamBuilder(
        stream: luckystream,
        builder: (context, snapshot) {
          for (var i = 0; i < lucky_num.length; i++) {
            print(appSettings['SERVER_URL'] +
                '/lottery_numbers/lotteryno_${lucky_num[i]}.gif');
          }
          return Row(
            children: [
              for (var i = 0; i < lucky_num.length; i++)
                Container(
                  height: 120,
                  width: 120,
                  padding: EdgeInsets.all(3),
                  child: Image.network(
                    appSettings['SERVER_URL'] +
                        '/lottery_numbers/lotteryno_${lucky_num[i]}.gif',
                    fit: BoxFit.fill,
                  ),
                )
              // ([0, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89]
              //             .contains(lucky_num[i])) ==
              //         true
              //     ?
              // Container(
              //   height: 120,
              //   width: 120,
              //   padding: EdgeInsets.all(3),
              //   child: Stack(
              //     children: [
              //       GifImage(
              //         controller: i == 0
              //             ? numbercontroller1
              //             : i == 1
              //                 ? numbercontroller2
              //                 : i == 2
              //                     ? numbercontroller3
              //                     : i == 3
              //                         ? numbercontroller4
              //                         : i == 4
              //                             ? numbercontroller5
              //                             : numbercontroller6,
              //         repeat: ImageRepeat.noRepeat,
              //         image: AssetImage("normalnumber.gif"),
              //       ),
              //       FutureBuilder(
              //           future: revealdelay(),
              //           builder: (context, snapshot) {
              //             if (snapshot.hasData) {
              //               return Center(
              //                 child: Text(
              //                   i.toString(),
              //                   style: TextStyle(
              //                     fontSize: 16,
              //                     fontWeight: FontWeight.w600,
              //                   ),
              //                 ),
              //               );
              //             } else {
              //               return Container();
              //             }
              //           }),
              //     ],
              //   ),

              //   // Image.network(
              //   //   appSettings['SERVER_URL'] +
              //   //       '/lottery_numbers/lotteryno_${lucky_num[i]}.gif',
              //   //   fit: BoxFit.fill,
              //   // ),
              // )
              // : Container(
              //     height: 80,
              //     width: 80,
              //     padding: EdgeInsets.all(3),
              //     child: Image.network(
              //      appSettings['SERVER_URL'] +
              //           '/lottery_numbers/lotteryno_${lucky_num[i]}.gif',
              //       fit: BoxFit.fill,
              //     ),
              //   ),
            ],
          );
        });
  }

  Widget text() {
    return Text('LEFT UNTIL TODAY\'S DRAW',
        style: TextStyle(
          color: Pallet1.fontcolor,
          fontSize: Pallet1.heading3,
        ));
  }

// recent winner container(2nd box)

  Widget recentWinners({double width}) {
    return Container(
      height: 273,
      padding: EdgeInsets.all(Pallet1.defaultPadding),
      width: width,
      decoration: BoxDecoration(
          color: Color(0xFF100F31), borderRadius: BorderRadius.circular(10)),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Row(
            children: [
              Text('Today Winners',
                  style: TextStyle(
                      color: Color(0xFFf59f3a),
                      fontSize: Pallet1.normalfont + 3,
                      fontWeight: Pallet1.font600)),
            ],
          ),
          SizedBox(height: 7),
          MediaQuery.of(context).size.width > 550
              ? Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                      legend("Special\n ", 'super.png', Color(0xFF585858),
                          'twgrey.png'),
                      legend("Jackpot\n ", 'jackpot.png', Color(0xFFc62ce6),
                          'twpurple.png'),
                      legend("Grand\nPrize", 'superjackpot.png',
                          Color(0xFFd10d0a), 'twred.png'),
                      legend("Super\nJackpot", '', Color(0xFF0e9400),
                          'twgreen.png'),
                    ])
              : SingleChildScrollView(
                  scrollDirection: Axis.horizontal,
                  child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        legend("Special\n ", 'super.png', Color(0xFF585858),
                            'twgrey.png'),
                        SizedBox(
                          width: 10,
                        ),
                        legend("Jackpot\n ", 'jackpot.png', Color(0xFFc62ce6),
                            'twpurple.png'),
                        SizedBox(
                          width: 10,
                        ),
                        legend("Grand\nPrize", 'superjackpot.png',
                            Color(0xFFd10d0a), 'twred.png'),
                        SizedBox(
                          width: 10,
                        ),
                        legend("Super\nJackpot", '', Color(0xFF0e9400),
                            'twgreen.png'),
                        // 'grandprize.png'
                      ]),
                ),
          SizedBox(height: 3),
          StreamBuilder<Object>(
              stream: null,
              builder: (context, snapshot) {
                return startreveal == true
                    ? Expanded(
                        child: Center(
                          child: Text("Loading...",
                              style: TextStyle(
                                color: Pallet1.fontcolor,
                                fontSize: Pallet1.normalfont,
                              )),
                        ),
                      )
                    : winners.length != 0
                        ? Expanded(
                            child: ListView.builder(
                                itemCount: winners.length,
                                itemBuilder: (context, index) {
                                  var time = winners[index]['created_at']
                                      .toString()
                                      .split('T');

                                  var a = time[1].toString().split('.');
                                  return Padding(
                                    padding:
                                        EdgeInsets.all(Pallet1.defaultPadding),
                                    child: Container(
                                      padding: EdgeInsets.all(
                                          Pallet1.defaultPadding),
                                      decoration: BoxDecoration(
                                        // color: winners[index]['winner_type'] == 'normal'
                                        //     ? Color(0xFF585858)
                                        //     : winners[index]['winner_type'] == 'special'
                                        //         ? Color(0xFF0e9400)
                                        //         : winners[index]['winner_type'] ==
                                        //                 'weekly'
                                        //             ? Color(0xFFc62ce6)
                                        //             : Color(0xFFd10d0a),

                                        color: Colors.transparent,
                                        image: DecorationImage(
                                            image: winners[index]
                                                        ['winner_type'] ==
                                                    'special'
                                                ? AssetImage('ttgrey.png')
                                                : winners[index]
                                                            ['winner_type'] ==
                                                        'normal'
                                                    ? AssetImage('ttgreen.png')
                                                    : winners[index][
                                                                'winner_type'] ==
                                                            'both'
                                                        ? AssetImage(
                                                            'ttred.png')
                                                        : AssetImage(
                                                            'ttpurple.png'),
                                            fit: BoxFit.fill),
                                        borderRadius: BorderRadius.circular(10),
                                      ),
                                      child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: [
                                          Expanded(
                                            child: Center(
                                              child: Text(
                                                  time[0] +
                                                      ' ' +
                                                      a[0].toString(),
                                                  style: TextStyle(
                                                    color: Pallet1.fontcolor,
                                                    fontSize:
                                                        Pallet1.normalfont,
                                                  )),
                                            ),
                                          ),
                                          Expanded(
                                            child: Center(
                                              child: Text(
                                                  'LOT' +
                                                      winners[index]
                                                              ['lottery_id']
                                                          .toString(),
                                                  style: TextStyle(
                                                    color: Pallet1.fontcolor,
                                                    fontSize:
                                                        Pallet1.normalfont,
                                                  )),
                                            ),
                                          ),
                                          Expanded(
                                            child: Center(
                                              child: Text(
                                                  winners[index]['screen_name']
                                                      .toString(),
                                                  // overflow: TextOverflow.ellipsis,
                                                  style: TextStyle(
                                                    color: Pallet1.fontcolor,
                                                    fontSize:
                                                        Pallet1.normalfont,
                                                  )),
                                            ),
                                          ),
                                          Expanded(
                                            child: Center(
                                              child: Text(
                                                  winners[index]
                                                              ['particulars'] ==
                                                          'Dollar'
                                                      ? '\$ ' +
                                                          (double.parse(winners[
                                                                          index]
                                                                      ['amount']
                                                                  .toString()))
                                                              .toStringAsFixed(
                                                                  2)
                                                      : ((double.parse(winners[
                                                                              index]
                                                                          [
                                                                          'amount']
                                                                      .toString()))
                                                                  .toStringAsFixed(
                                                                      2) +
                                                              ' DUC')
                                                          .toString(),
                                                  style: TextStyle(
                                                    color: Pallet1.fontcolor,
                                                    fontSize:
                                                        Pallet1.normalfont,
                                                  )),
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  );
                                }))
                        : Expanded(
                            child: Center(
                              child: Text("No Recent Winners Found",
                                  style: TextStyle(
                                    color: Pallet1.fontcolor,
                                    fontSize: Pallet1.normalfont,
                                  )),
                            ),
                          );
              }),
        ],
      ),
    );
  }

  Widget legend(String name, String imgpath, Color color, String bgimg) {
    return Container(
      width: 80,
      height: 90,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10),
        image: DecorationImage(image: AssetImage(bgimg), fit: BoxFit.fill),
        color: Colors.transparent,
      ),
      padding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
      child: Column(
        // mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          SizedBox(
            width: 30,
            height: 25,
            child: imgpath == ''
                ? Container()
                : Container(
                    child: Image.asset(imgpath),
                  ),
          ),
          SizedBox(height: 5),
          bgimg == 'twgreen.png'
              ? Center(
                  child: Container(
                    // width: 22,
                    // height: 7,
                    child: Image.asset('superjackpottext.png'),
                  ),
                )
              : Text(
                  name,
                  style: TextStyle(
                    color: Color(0xFFfaf3fc),
                  ),
                ),
        ],
      ),
    );
  }

  StreamController animcontroller = StreamController.broadcast();
  StreamSink get animsink => animcontroller.sink;
  Stream get animstream => animcontroller.stream;

  StreamController luckycontroller = StreamController.broadcast();
  StreamSink get luckysink => luckycontroller.sink;
  Stream get luckystream => luckycontroller.stream;
// Last lucky draw container(3rd box)
  // ignore: non_constant_identifier_names
  List lucky_num = [];

  winnerpopup() async {
    if (dontShow == false) {
      SharedPreferences prefs = await SharedPreferences.getInstance();
      prefs.setBool('winnerpopupshowed', true);
      setState(() {
        dontShow = true;
      });
      await showDialog(
        context: context,
        builder: (BuildContext context) => AlertDialog(
          backgroundColor: Colors.transparent,
          content: Container(
            height: MediaQuery.of(context).size.height * 0.5,
            width: MediaQuery.of(context).size.height * 0.5,
            decoration: BoxDecoration(
              image: DecorationImage(
                image: AssetImage(
                  wintype == 'special'
                      ? 'special_v2.png'
                      : wintype == 'normal'
                          ? 'jackpot_v2.png'
                          : wintype == 'both'
                              ? 'grand_win.png'
                              : 'super_jackpot_v2.png',
                ),
                fit: BoxFit.fill,
              ),
            ),
          ),
          // content: Container(
          //   child: Text(
          //     'You Won' + wintype,
          //     style: TextStyle(color: Pallet1.fontcolor),
          //   ),
          // ),
          // actions: [
          //   PopupButton1(
          //     text: 'Close',
          //     onpress: () {
          //       Navigator.pop(context);
          //     },
          //   )
          // ],
        ),
      );
    } else {}
  }

  luckynumberreveal() async {
    if (startreveal == true) {
      lucky_num = [];
      setState(() {
        wait = true;
      });
      _init();
      controllerCenter.play();
      _player.play();
      for (var item in data) {
        if (item['last_lucky_numbers'] != null) {
          for (var i = 0; i < item['last_lucky_numbers'].length; i++) {
            if (i == 0) {
              lucky_num.add(item['last_lucky_numbers'][i]);
            } else {
              await Future.delayed(
                const Duration(seconds: 4),
                () {
                  if (lucky_num.contains(item['last_lucky_numbers'][i]) ==
                      true) {
                  } else {
                    lucky_num.add(item['last_lucky_numbers'][i]);
                    print('LUCKY_NUM added: $lucky_num}');
                    luckysink.add('');
                  }
                },
              );
            }
          }
        }
      }
      // setState(() {
      //   wait = false;
      // });
      if (iswinner == true) {
        winnerpopup();
      }
      print("fhgjnkkkkkkkkkkkkkkkkkkkkkkkjjjjjj$wait");
    } else {}
  }

  // luckynumberreveal() async {
  //   if (startreveal == true) {
  //     lucky_num = [];
  //     setState(() {
  //       wait = true;
  //     });
  //     _init();
  //     controllerCenter.play();
  //     _player.play();
  //     for (var item in data) {
  //       if (item['last_lucky_numbers'] != null) {
  //         for (var i = 0; i < item['last_lucky_numbers'].length; i++) {
  //           if (i == 0) {
  //             lucky_num.add(item['last_lucky_numbers'][i]);
  //             numbercontroller1.repeat(
  //                 min: 0, max: 140, period: Duration(milliseconds: 5000));
  //             Timer(Duration(milliseconds: 5000), () {
  //               // numbercontroller1.stop();
  //               numbercontroller1.repeat(
  //                   min: 115, max: 140, period: Duration(milliseconds: 2038));
  //               // numbercontroller1.value = 140;
  //             });
  //           } else if (i == 1) {
  //             await Future.delayed(
  //               const Duration(seconds: 4),
  //               () {
  //                 if (lucky_num.contains(item['last_lucky_numbers'][i]) ==
  //                     true) {
  //                 } else {
  //                   lucky_num.add(item['last_lucky_numbers'][i]);
  //                   print('LUCKY_NUM added: $lucky_num}');
  //                   luckysink.add('');
  //                   numbercontroller2.repeat(
  //                       min: 0, max: 140, period: Duration(milliseconds: 5000));
  //                   Timer(Duration(milliseconds: 5000), () {
  //                     // numbercontroller2.stop();
  //                     numbercontroller2.repeat(
  //                         min: 115,
  //                         max: 140,
  //                         period: Duration(milliseconds: 2036));
  //                     // numbercontroller2.value = 140;
  //                   });
  //                 }
  //               },
  //             );
  //           } else if (i == 2) {
  //             await Future.delayed(
  //               const Duration(seconds: 4),
  //               () {
  //                 if (lucky_num.contains(item['last_lucky_numbers'][i]) ==
  //                     true) {
  //                 } else {
  //                   lucky_num.add(item['last_lucky_numbers'][i]);
  //                   print('LUCKY_NUM added: $lucky_num}');
  //                   luckysink.add('');
  //                   numbercontroller3.repeat(
  //                       min: 0, max: 140, period: Duration(milliseconds: 5000));
  //                   Timer(Duration(milliseconds: 5000), () {
  //                     // numbercontroller3.stop();
  //                     numbercontroller3.repeat(
  //                         min: 115,
  //                         max: 140,
  //                         period: Duration(milliseconds: 2030));
  //                     // numbercontroller3.value = 140;
  //                   });
  //                 }
  //               },
  //             );
  //           } else if (i == 3) {
  //             await Future.delayed(
  //               const Duration(seconds: 4),
  //               () {
  //                 if (lucky_num.contains(item['last_lucky_numbers'][i]) ==
  //                     true) {
  //                 } else {
  //                   lucky_num.add(item['last_lucky_numbers'][i]);
  //                   print('LUCKY_NUM added: $lucky_num}');
  //                   luckysink.add('');
  //                   numbercontroller4.repeat(
  //                       min: 0, max: 140, period: Duration(milliseconds: 5000));
  //                   Timer(Duration(milliseconds: 5000), () {
  //                     // numbercontroller4.stop();
  //                     numbercontroller4.repeat(
  //                         min: 115,
  //                         max: 140,
  //                         period: Duration(milliseconds: 2032));
  //                     // numbercontroller4.value = 140;
  //                   });
  //                 }
  //               },
  //             );
  //           } else if (i == 4) {
  //             await Future.delayed(
  //               const Duration(seconds: 4),
  //               () {
  //                 if (lucky_num.contains(item['last_lucky_numbers'][i]) ==
  //                     true) {
  //                 } else {
  //                   lucky_num.add(item['last_lucky_numbers'][i]);
  //                   print('LUCKY_NUM added: $lucky_num}');
  //                   luckysink.add('');
  //                   numbercontroller5.repeat(
  //                       min: 0, max: 140, period: Duration(milliseconds: 5000));
  //                   Timer(Duration(milliseconds: 5000), () {
  //                     // numbercontroller5.stop();
  //                     numbercontroller5.repeat(
  //                         min: 115,
  //                         max: 140,
  //                         period: Duration(milliseconds: 2033));
  //                     // numbercontroller5.value = 140;
  //                   });
  //                 }
  //               },
  //             );
  //           } else if (i == 5) {
  //             await Future.delayed(
  //               const Duration(seconds: 4),
  //               () {
  //                 if (lucky_num.contains(item['last_lucky_numbers'][i]) ==
  //                     true) {
  //                 } else {
  //                   lucky_num.add(item['last_lucky_numbers'][i]);
  //                   print('LUCKY_NUM added: $lucky_num}');
  //                   luckysink.add('');
  //                   numbercontroller6.repeat(
  //                       min: 0, max: 140, period: Duration(milliseconds: 5000));
  //                   Timer(Duration(milliseconds: 5000), () {
  //                     numbercontroller6.stop();
  //                     //  numbercontroller6.repeat(
  //                     //     min: 115,
  //                     //     max: 140,
  //                     //     period: Duration(milliseconds: 1800));
  //                     numbercontroller6.value = 140;
  //                   });
  //                 }
  //               },
  //             );
  //           }
  //         }
  //       }
  //     }
  //     setState(() {
  //       wait = false;
  //     });

  //     // if (iswinner == true) {
  //     //   await showDialog(
  //     //     context: context,
  //     //     builder: (BuildContext context) => AlertDialog(
  //     //       backgroundColor: Pallet1.popupcontainerback,
  //     //       content: Container(
  //     //         child: Text(
  //     //           'You Won' + wintype,
  //     //           style: TextStyle(color: Pallet1.fontcolor),
  //     //         ),
  //     //       ),
  //     //       actions: [
  //     //         PopupButton1(
  //     //           text: 'Close',
  //     //           onpress: () {
  //     //             Navigator.pop(context);
  //     //           },
  //     //         )
  //     //       ],
  //     //     ),
  //     //   );
  //     // }

  //     print("fhgjnkkkkkkkkkkkkkkkkkkkkkkkjjjjjj$wait");
  //   } else {}
  // }

  Widget luckyNumbers({double width}) {
    print('DATA.LENGTH: ${data.length}');
    return Container(
        width: width,
        height: 273,
        padding: EdgeInsets.all(Pallet1.defaultPadding),
        decoration: BoxDecoration(
            color: Color(0xFF100F31), borderRadius: BorderRadius.circular(10)),
        child: Column(
          // mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            // for (var item in data)
            //   item['last_lucky_numbers'].length == 0
            //       ? Container()
            //       :
            Text('Last Draw of Winning Numbers',
                style: TextStyle(
                    color: Color(0xFFf59f3a),
                    fontSize: Pallet1.normalfont + 3,
                    fontWeight: Pallet1.font600)),
            SizedBox(height: 7),
            Container(
              height: 210,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  // Text('Last Lucky Draw Numbers',
                  //     style: TextStyle(
                  //       color: Pallet1.fontcolor,
                  //       fontSize: Pallet1.normalfont,
                  //     )),
                  StreamBuilder(
                      stream: luckystream,
                      builder: (context, snapshot) {
                        return startreveal == true
                            ? Container()
                            : Expanded(
                                flex: 40,
                                child: Container(
                                  padding: EdgeInsets.symmetric(
                                      vertical: 3, horizontal: 7),
                                  decoration: BoxDecoration(
                                    // color: Color(0xFF13125e),
                                    color: Colors.transparent,
                                    borderRadius: BorderRadius.circular(30),
                                  ),
                                  child: SingleChildScrollView(
                                    scrollDirection: Axis.horizontal,
                                    child: Row(
                                      children: [
                                        for (var item in data)
                                          if (item['last_lucky_numbers'] !=
                                              null)
                                            for (var newItem
                                                in item['last_lucky_numbers'])
                                              //   luckynumberreveal(newItem)

                                              Padding(
                                                padding: EdgeInsets.all(
                                                    Pallet1.defaultPadding),
                                                child: ([
                                                          0,
                                                          1,
                                                          2,
                                                          3,
                                                          5,
                                                          8,
                                                          13,
                                                          21,
                                                          34,
                                                          55,
                                                          89
                                                        ].contains(newItem)) ==
                                                        true
                                                    ? Container(
                                                        width: 40,
                                                        height: 35,
                                                        padding:
                                                            EdgeInsets.all(3),
                                                        decoration:
                                                            BoxDecoration(
                                                          borderRadius:
                                                              BorderRadius
                                                                  .circular(10),
                                                          color:
                                                              Color(0xFF3a3a7a),
                                                        ),
                                                        child: CustomPaint(
                                                          size: Size(
                                                              40,
                                                              (40 * 1)
                                                                  .toDouble()),
                                                          painter:
                                                              StarCustomPainter(),
                                                          child: Center(
                                                            child: Text(
                                                              newItem
                                                                  .toString(),
                                                              style: TextStyle(
                                                                color: Colors
                                                                    .white,
                                                              ),
                                                            ),
                                                          ),
                                                        ))
                                                    : Container(
                                                        width: 40,
                                                        height: 35,
                                                        decoration:
                                                            BoxDecoration(
                                                          borderRadius:
                                                              BorderRadius
                                                                  .circular(10),
                                                          color:
                                                              Color(0xFFf59f3a),
                                                        ),
                                                        child: Center(
                                                          child: Text(
                                                            newItem.toString(),
                                                            style: TextStyle(
                                                                color: Pallet1
                                                                    .fontcolor),
                                                          ),
                                                        ),
                                                      ),
                                              )
                                      ],
                                    ),
                                  ),
                                ),
                              );
                      }),
                  SizedBox(
                    height: 5,
                  ),
                  Expanded(
                    flex: 45,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text('Next Week Super Jackpot %(Cash : DUC)',
                            style: TextStyle(
                              color: Pallet1.fontcolor,
                              fontSize: Pallet1.normalfont,
                            )),
                        Padding(
                            padding: EdgeInsets.all(10),
                            child: Text(
                              data[0]["next_jackpot_percentage"].toString() +
                                  '% ' +
                                  ': ' +
                                  (100 - data[0]["next_jackpot_percentage"])
                                      .toString() +
                                  '%',
                              style: TextStyle(
                                color: Colors.grey,
                                fontSize: 24,
                                fontWeight: FontWeight.bold,
                              ),
                            )),
                      ],
                    ),
                  ),

                  Expanded(
                    flex: 25,
                    child: Container(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              CustomButton1(
                                vpadding: 9,
                                hpadding: 8,
                                text: 'View History',
                                buttoncolor: Colors.white,
                                //  Color(0xFF65667b),
                                textcolor: Color(0xFF2a2663),
                                onpress: () {
                                  Navigator.pushReplacement(
                                    context,
                                    MaterialPageRoute(
                                      builder: (context) => Home(
                                        route: 'all_lottery_history',
                                      ),
                                    ),
                                  );
                                },
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                  ),
                  // SizedBox(width: 10),

                  // dashboard.username != null
                  //     ?

                  // SizedBox(height: 10),
                  // Column(
                  //   children: [
                  // Row(
                  //   children: [
                  //     Text('Jackpot Fund :',
                  //         style: TextStyle(
                  //           color: Pallet1.fontcolor,
                  //           fontSize: Pallet1.normalfont,
                  //         )),
                  //     SizedBox(width: 10),
                  //     for (var item in data)
                  //       Text(item['jackpot_fund'].toString(),
                  //           style: TextStyle(
                  //             color: Pallet1.fontcolor,
                  //             fontSize: Pallet1.normalfont,
                  //           )),
                  //   ],
                  // ),
                  // Row(
                  //   children: [
                  //     Text('Weekly Fund :',
                  //         style: TextStyle(
                  //           color: Pallet1.fontcolor,
                  //           fontSize: Pallet1.normalfont,
                  //         )),
                  //     SizedBox(width: 10),
                  //     for (var item in data)
                  //       Text(item['weekly_fund'].toString(),
                  //           style: TextStyle(
                  //             color: Pallet1.fontcolor,
                  //             fontSize: Pallet1.normalfont,
                  //           )),
                  //   ],
                  // ),
                  // Row(
                  //   children: [
                  //     Text('Company Fund :',
                  //         style: TextStyle(
                  //           color: Pallet1.fontcolor,
                  //           fontSize: Pallet1.normalfont,
                  //         )),
                  //     SizedBox(width: 10),
                  //     for (var item in data)
                  //       Text(item['company_fund'].toString(),
                  //           style: TextStyle(
                  //             color: Pallet1.fontcolor,
                  //             fontSize: Pallet1.normalfont,
                  //           )),
                  //   ],
                  // ),
                  //   ],
                  // )
                  // : Container()

                  // Center(child: Image.asset('Ticketimg.png')),
                  // Center(
                  //   child: Container(
                  //     decoration: BoxDecoration(
                  //         color: Pallet1.fontcolor,
                  //         borderRadius: BorderRadius.circular(5)),
                  //     child: TextButton(
                  //       onPressed: () {},
                  //       child: Text('Invite',
                  //           style: TextStyle(
                  //             color: Pallet1.dashcontainerback,
                  //             fontSize: Pallet1.normalfont,
                  //           )),
                  //     ),
                  //   ),
                  // )
                ],
              ),
            ),
          ],
        ));
  }

// confetti spring shape

  Path createPath(Size size) {
    Path path_0 = Path();
    path_0.moveTo(size.width * 0.2412963, size.height * 0.6356481);
    path_0.cubicTo(
        size.width * 0.2413889,
        size.height * 0.6356481,
        size.width * 0.2415741,
        size.height * 0.6357407,
        size.width * 0.2418519,
        size.height * 0.6357407);
    path_0.cubicTo(
        size.width * 0.2413889,
        size.height * 0.6358333,
        size.width * 0.2409259,
        size.height * 0.6358333,
        size.width * 0.2407407,
        size.height * 0.6358333);
    path_0.cubicTo(
        size.width * 0.2404630,
        size.height * 0.6357407,
        size.width * 0.2405556,
        size.height * 0.6357407,
        size.width * 0.2412963,
        size.height * 0.6356481);
    path_0.moveTo(size.width * 0.2433333, size.height * 0.6355556);
    path_0.cubicTo(
        size.width * 0.2431481,
        size.height * 0.6355556,
        size.width * 0.2430556,
        size.height * 0.6356481,
        size.width * 0.2429630,
        size.height * 0.6356481);
    path_0.cubicTo(
        size.width * 0.2430556,
        size.height * 0.6355556,
        size.width * 0.2431481,
        size.height * 0.6355556,
        size.width * 0.2433333,
        size.height * 0.6355556);
    path_0.moveTo(size.width * 0.1733333, size.height * 0.5983333);
    path_0.cubicTo(
        size.width * 0.1735185,
        size.height * 0.5983333,
        size.width * 0.1736111,
        size.height * 0.5983333,
        size.width * 0.1737963,
        size.height * 0.5982407);
    path_0.cubicTo(
        size.width * 0.1758333,
        size.height * 0.5983333,
        size.width * 0.1770370,
        size.height * 0.5984259,
        size.width * 0.1761111,
        size.height * 0.5984259);
    path_0.cubicTo(
        size.width * 0.1756481,
        size.height * 0.5984259,
        size.width * 0.1748148,
        size.height * 0.5984259,
        size.width * 0.1733333,
        size.height * 0.5983333);
    path_0.moveTo(size.width * 0.1185185, size.height * 0.5925000);
    path_0.cubicTo(
        size.width * 0.1186111,
        size.height * 0.5925000,
        size.width * 0.1186111,
        size.height * 0.5925000,
        size.width * 0.1186111,
        size.height * 0.5925926);
    path_0.cubicTo(
        size.width * 0.1191667,
        size.height * 0.5927778,
        size.width * 0.1197222,
        size.height * 0.5930556,
        size.width * 0.1202778,
        size.height * 0.5932407);
    path_0.cubicTo(
        size.width * 0.1200000,
        size.height * 0.5931481,
        size.width * 0.1194444,
        size.height * 0.5929630,
        size.width * 0.1185185,
        size.height * 0.5925000);
    path_0.moveTo(size.width * 0.1179630, size.height * 0.5922222);
    path_0.cubicTo(
        size.width * 0.1180556,
        size.height * 0.5923148,
        size.width * 0.1181481,
        size.height * 0.5923148,
        size.width * 0.1182407,
        size.height * 0.5924074);
    path_0.cubicTo(
        size.width * 0.1181481,
        size.height * 0.5924074,
        size.width * 0.1180556,
        size.height * 0.5923148,
        size.width * 0.1179630,
        size.height * 0.5922222);
    path_0.moveTo(size.width * 0.5027778, size.height * 0.5403704);
    path_0.cubicTo(
        size.width * 0.5026852,
        size.height * 0.5403704,
        size.width * 0.5025926,
        size.height * 0.5404630,
        size.width * 0.5025926,
        size.height * 0.5404630);
    path_0.cubicTo(
        size.width * 0.5025000,
        size.height * 0.5404630,
        size.width * 0.5024074,
        size.height * 0.5405556,
        size.width * 0.5022222,
        size.height * 0.5405556);
    path_0.cubicTo(
        size.width * 0.5001852,
        size.height * 0.5403704,
        size.width * 0.4984259,
        size.height * 0.5401852,
        size.width * 0.5027778,
        size.height * 0.5403704);
    path_0.moveTo(size.width * 0.4262963, size.height * 0.4796296);
    path_0.lineTo(size.width * 0.4261111, size.height * 0.4796296);
    path_0.cubicTo(
        size.width * 0.4278704,
        size.height * 0.4794444,
        size.width * 0.4278704,
        size.height * 0.4795370,
        size.width * 0.4262963,
        size.height * 0.4796296);
    path_0.moveTo(size.width * 0.3553704, size.height * 0.4696296);
    path_0.cubicTo(
        size.width * 0.3558333,
        size.height * 0.4698148,
        size.width * 0.3562963,
        size.height * 0.4699074,
        size.width * 0.3566667,
        size.height * 0.4701852);
    path_0.cubicTo(
        size.width * 0.3562037,
        size.height * 0.4700000,
        size.width * 0.3557407,
        size.height * 0.4698148,
        size.width * 0.3553704,
        size.height * 0.4696296);
    path_0.moveTo(size.width * 0.7548148, size.height * 0.2450000);
    path_0.cubicTo(
        size.width * 0.7427778,
        size.height * 0.2500000,
        size.width * 0.7331481,
        size.height * 0.2585185,
        size.width * 0.7260185,
        size.height * 0.2693519);
    path_0.cubicTo(
        size.width * 0.7160185,
        size.height * 0.2847222,
        size.width * 0.7108333,
        size.height * 0.3030556,
        size.width * 0.7075000,
        size.height * 0.3210185);
    path_0.cubicTo(
        size.width * 0.7038889,
        size.height * 0.3406481,
        size.width * 0.7037037,
        size.height * 0.3606481,
        size.width * 0.7009259,
        size.height * 0.3804630);
    path_0.cubicTo(
        size.width * 0.6993519,
        size.height * 0.3908333,
        size.width * 0.6969444,
        size.height * 0.4031481,
        size.width * 0.6915741,
        size.height * 0.4133333);
    path_0.cubicTo(
        size.width * 0.6841667,
        size.height * 0.4088889,
        size.width * 0.6765741,
        size.height * 0.4047222,
        size.width * 0.6687037,
        size.height * 0.4010185);
    path_0.cubicTo(
        size.width * 0.6603704,
        size.height * 0.3971296,
        size.width * 0.6518519,
        size.height * 0.3941667,
        size.width * 0.6428704,
        size.height * 0.3915741);
    path_0.cubicTo(
        size.width * 0.6331481,
        size.height * 0.3888889,
        size.width * 0.6231481,
        size.height * 0.3868519,
        size.width * 0.6132407,
        size.height * 0.3849074);
    path_0.cubicTo(
        size.width * 0.5938889,
        size.height * 0.3812037,
        size.width * 0.5741667,
        size.height * 0.3781481,
        size.width * 0.5543519,
        size.height * 0.3769444);
    path_0.cubicTo(
        size.width * 0.5430556,
        size.height * 0.3762963,
        size.width * 0.5287037,
        size.height * 0.3750926,
        size.width * 0.5207407,
        size.height * 0.3850926);
    path_0.cubicTo(
        size.width * 0.5145370,
        size.height * 0.3930556,
        size.width * 0.5134259,
        size.height * 0.4047222,
        size.width * 0.5128704,
        size.height * 0.4144444);
    path_0.cubicTo(
        size.width * 0.5120370,
        size.height * 0.4337037,
        size.width * 0.5149074,
        size.height * 0.4526852,
        size.width * 0.5173148,
        size.height * 0.4717593);
    path_0.cubicTo(
        size.width * 0.5190741,
        size.height * 0.4859259,
        size.width * 0.5204630,
        size.height * 0.5006481,
        size.width * 0.5185185,
        size.height * 0.5149074);
    path_0.cubicTo(
        size.width * 0.5055556,
        size.height * 0.5065741,
        size.width * 0.4925926,
        size.height * 0.4985185,
        size.width * 0.4789815,
        size.height * 0.4913889);
    path_0.cubicTo(
        size.width * 0.4601852,
        size.height * 0.4814815,
        size.width * 0.4403704,
        size.height * 0.4750000,
        size.width * 0.4196296,
        size.height * 0.4703704);
    path_0.cubicTo(
        size.width * 0.3988889,
        size.height * 0.4657407,
        size.width * 0.3778704,
        size.height * 0.4622222,
        size.width * 0.3567593,
        size.height * 0.4600926);
    path_0.cubicTo(
        size.width * 0.3476852,
        size.height * 0.4591667,
        size.width * 0.3384259,
        size.height * 0.4583333,
        size.width * 0.3292593,
        size.height * 0.4584259);
    path_0.cubicTo(
        size.width * 0.3227778,
        size.height * 0.4585185,
        size.width * 0.3158333,
        size.height * 0.4589815,
        size.width * 0.3097222,
        size.height * 0.4615741);
    path_0.cubicTo(
        size.width * 0.2805556,
        size.height * 0.4738889,
        size.width * 0.2789815,
        size.height * 0.5139815,
        size.width * 0.2787037,
        size.height * 0.5409259);
    path_0.cubicTo(
        size.width * 0.2784259,
        size.height * 0.5607407,
        size.width * 0.2792593,
        size.height * 0.5804630,
        size.width * 0.2744444,
        size.height * 0.5998148);
    path_0.cubicTo(
        size.width * 0.2723148,
        size.height * 0.6085185,
        size.width * 0.2689815,
        size.height * 0.6174074,
        size.width * 0.2632407,
        size.height * 0.6243519);
    path_0.cubicTo(
        size.width * 0.2552778,
        size.height * 0.6203704,
        size.width * 0.2473148,
        size.height * 0.6164815,
        size.width * 0.2393519,
        size.height * 0.6126852);
    path_0.cubicTo(
        size.width * 0.2275000,
        size.height * 0.6070370,
        size.width * 0.2152778,
        size.height * 0.6017593,
        size.width * 0.2026852,
        size.height * 0.5976852);
    path_0.cubicTo(
        size.width * 0.1874074,
        size.height * 0.5927778,
        size.width * 0.1718519,
        size.height * 0.5890741,
        size.width * 0.1561111,
        size.height * 0.5861111);
    path_0.cubicTo(
        size.width * 0.1312963,
        size.height * 0.5813889,
        size.width * 0.1059259,
        size.height * 0.5775000,
        size.width * 0.08055556,
        size.height * 0.5770370);
    path_0.cubicTo(
        size.width * 0.06944444,
        size.height * 0.5768519,
        size.width * 0.05907407,
        size.height * 0.5781481,
        size.width * 0.04953704,
        size.height * 0.5840741);
    path_0.cubicTo(
        size.width * 0.04157407,
        size.height * 0.5890741,
        size.width * 0.03629630,
        size.height * 0.5973148,
        size.width * 0.03287037,
        size.height * 0.6059259);
    path_0.cubicTo(
        size.width * 0.02740741,
        size.height * 0.6196296,
        size.width * 0.02574074,
        size.height * 0.6347222,
        size.width * 0.02518519,
        size.height * 0.6492593);
    path_0.cubicTo(
        size.width * 0.02416667,
        size.height * 0.6723148,
        size.width * 0.02601852,
        size.height * 0.6956481,
        size.width * 0.02870370,
        size.height * 0.7186111);
    path_0.cubicTo(
        size.width * 0.02953704,
        size.height * 0.7257407,
        size.width * 0.03037037,
        size.height * 0.7328704,
        size.width * 0.03138889,
        size.height * 0.7399074);
    path_0.cubicTo(
        size.width * 0.03185185,
        size.height * 0.7432407,
        size.width * 0.04675926,
        size.height * 0.7463889,
        size.width * 0.04712963,
        size.height * 0.7464815);
    path_0.cubicTo(
        size.width * 0.05990741,
        size.height * 0.7499074,
        size.width * 0.07333333,
        size.height * 0.7525000,
        size.width * 0.08648148,
        size.height * 0.7545370);
    path_0.cubicTo(
        size.width * 0.09962963,
        size.height * 0.7565741,
        size.width * 0.1128704,
        size.height * 0.7587037,
        size.width * 0.1262963,
        size.height * 0.7592593);
    path_0.cubicTo(
        size.width * 0.1280556,
        size.height * 0.7593519,
        size.width * 0.1437037,
        size.height * 0.7603704,
        size.width * 0.1433333,
        size.height * 0.7579630);
    path_0.cubicTo(
        size.width * 0.1389815,
        size.height * 0.7269444,
        size.width * 0.1354630,
        size.height * 0.6951852,
        size.width * 0.1373148,
        size.height * 0.6637963);
    path_0.cubicTo(
        size.width * 0.1383333,
        size.height * 0.6469444,
        size.width * 0.1405556,
        size.height * 0.6283333,
        size.width * 0.1500000,
        size.height * 0.6137963);
    path_0.cubicTo(
        size.width * 0.1510185,
        size.height * 0.6122222,
        size.width * 0.1522222,
        size.height * 0.6106481,
        size.width * 0.1534259,
        size.height * 0.6092593);
    path_0.cubicTo(
        size.width * 0.1668519,
        size.height * 0.6160185,
        size.width * 0.1802778,
        size.height * 0.6226852,
        size.width * 0.1941667,
        size.height * 0.6284259);
    path_0.cubicTo(
        size.width * 0.2213889,
        size.height * 0.6399074,
        size.width * 0.2500000,
        size.height * 0.6462963,
        size.width * 0.2790741,
        size.height * 0.6510185);
    path_0.cubicTo(
        size.width * 0.2992593,
        size.height * 0.6542593,
        size.width * 0.3205556,
        size.height * 0.6577778,
        size.width * 0.3411111,
        size.height * 0.6568519);
    path_0.cubicTo(
        size.width * 0.3475926,
        size.height * 0.6564815,
        size.width * 0.3541667,
        size.height * 0.6560185,
        size.width * 0.3600926,
        size.height * 0.6534259);
    path_0.cubicTo(
        size.width * 0.3866667,
        size.height * 0.6418519,
        size.width * 0.3897222,
        size.height * 0.6079630,
        size.width * 0.3904630,
        size.height * 0.5825926);
    path_0.cubicTo(
        size.width * 0.3910185,
        size.height * 0.5612037,
        size.width * 0.3897222,
        size.height * 0.5399074,
        size.width * 0.3943519,
        size.height * 0.5187963);
    path_0.cubicTo(
        size.width * 0.3961111,
        size.height * 0.5110185,
        size.width * 0.3986111,
        size.height * 0.5027778,
        size.width * 0.4028704,
        size.height * 0.4957407);
    path_0.cubicTo(
        size.width * 0.4178704,
        size.height * 0.5052778,
        size.width * 0.4325926,
        size.height * 0.5150926,
        size.width * 0.4478704,
        size.height * 0.5242593);
    path_0.cubicTo(
        size.width * 0.4597222,
        size.height * 0.5312963,
        size.width * 0.4719444,
        size.height * 0.5376852,
        size.width * 0.4849074,
        size.height * 0.5425000);
    path_0.cubicTo(
        size.width * 0.5063889,
        size.height * 0.5503704,
        size.width * 0.5297222,
        size.height * 0.5540741,
        size.width * 0.5523148,
        size.height * 0.5574074);
    path_0.cubicTo(
        size.width * 0.5675926,
        size.height * 0.5597222,
        size.width * 0.5832407,
        size.height * 0.5620370,
        size.width * 0.5987963,
        size.height * 0.5617593);
    path_0.cubicTo(
        size.width * 0.6025926,
        size.height * 0.5616667,
        size.width * 0.6062963,
        size.height * 0.5615741,
        size.width * 0.6099074,
        size.height * 0.5608333);
    path_0.cubicTo(
        size.width * 0.6348148,
        size.height * 0.5557407,
        size.width * 0.6321296,
        size.height * 0.5209259,
        size.width * 0.6306481,
        size.height * 0.5023148);
    path_0.cubicTo(
        size.width * 0.6287963,
        size.height * 0.4800926,
        size.width * 0.6242593,
        size.height * 0.4582407,
        size.width * 0.6248148,
        size.height * 0.4358333);
    path_0.cubicTo(
        size.width * 0.6249074,
        size.height * 0.4319444,
        size.width * 0.6250926,
        size.height * 0.4275926,
        size.width * 0.6256481,
        size.height * 0.4233333);
    path_0.lineTo(size.width * 0.6283333, size.height * 0.4246296);
    path_0.cubicTo(
        size.width * 0.6462037,
        size.height * 0.4332407,
        size.width * 0.6650926,
        size.height * 0.4383333,
        size.width * 0.6845370,
        size.height * 0.4424074);
    path_0.cubicTo(
        size.width * 0.7107407,
        size.height * 0.4477778,
        size.width * 0.7379630,
        size.height * 0.4525926,
        size.width * 0.7648148,
        size.height * 0.4525000);
    path_0.cubicTo(
        size.width * 0.7702778,
        size.height * 0.4525000,
        size.width * 0.7761111,
        size.height * 0.4523148,
        size.width * 0.7812963,
        size.height * 0.4505556);
    path_0.cubicTo(
        size.width * 0.8032407,
        size.height * 0.4437037,
        size.width * 0.8098148,
        size.height * 0.4189815,
        size.width * 0.8128704,
        size.height * 0.3987963);
    path_0.cubicTo(
        size.width * 0.8144444,
        size.height * 0.3879630,
        size.width * 0.8149074,
        size.height * 0.3770370,
        size.width * 0.8159259,
        size.height * 0.3661111);
    path_0.cubicTo(
        size.width * 0.8173148,
        size.height * 0.3511111,
        size.width * 0.8195370,
        size.height * 0.3360185,
        size.width * 0.8236111,
        size.height * 0.3213889);
    path_0.cubicTo(
        size.width * 0.8283333,
        size.height * 0.3042593,
        size.width * 0.8356481,
        size.height * 0.2863889,
        size.width * 0.8490741,
        size.height * 0.2740741);
    path_0.cubicTo(
        size.width * 0.8516667,
        size.height * 0.2760185,
        size.width * 0.8543519,
        size.height * 0.2780556,
        size.width * 0.8568519,
        size.height * 0.2800926);
    path_0.cubicTo(
        size.width * 0.8596296,
        size.height * 0.2823148,
        size.width * 0.8623148,
        size.height * 0.2845370,
        size.width * 0.8649074,
        size.height * 0.2869444);
    path_0.cubicTo(
        size.width * 0.8679630,
        size.height * 0.2884259,
        size.width * 0.8712037,
        size.height * 0.2896296,
        size.width * 0.8746296,
        size.height * 0.2903704);
    path_0.cubicTo(
        size.width * 0.8797222,
        size.height * 0.2920370,
        size.width * 0.8849074,
        size.height * 0.2933333,
        size.width * 0.8900926,
        size.height * 0.2943519);
    path_0.cubicTo(
        size.width * 0.9042593,
        size.height * 0.2975000,
        size.width * 0.9187963,
        size.height * 0.3000926,
        size.width * 0.9331481,
        size.height * 0.3019444);
    path_0.cubicTo(
        size.width * 0.9442593,
        size.height * 0.3033333,
        size.width * 0.9557407,
        size.height * 0.3052778,
        size.width * 0.9669444,
        size.height * 0.3048148);
    path_0.cubicTo(
        size.width * 0.9688889,
        size.height * 0.3046296,
        size.width * 0.9708333,
        size.height * 0.3045370,
        size.width * 0.9728704,
        size.height * 0.3043519);
    path_0.cubicTo(
        size.width * 0.9756481,
        size.height * 0.3037963,
        size.width * 0.9757407,
        size.height * 0.3027778,
        size.width * 0.9733333,
        size.height * 0.3012963);
    path_0.cubicTo(
        size.width * 0.9648148,
        size.height * 0.2935185,
        size.width * 0.9553704,
        size.height * 0.2866667,
        size.width * 0.9455556,
        size.height * 0.2804630);
    path_0.cubicTo(
        size.width * 0.9353704,
        size.height * 0.2738889,
        size.width * 0.9246296,
        size.height * 0.2686111,
        size.width * 0.9134259,
        size.height * 0.2640741);
    path_0.cubicTo(
        size.width * 0.9057407,
        size.height * 0.2609259,
        size.width * 0.8978704,
        size.height * 0.2586111,
        size.width * 0.8899074,
        size.height * 0.2563889);
    path_0.cubicTo(
        size.width * 0.8637963,
        size.height * 0.2494444,
        size.width * 0.8365741,
        size.height * 0.2448148,
        size.width * 0.8097222,
        size.height * 0.2421296);
    path_0.cubicTo(
        size.width * 0.8010185,
        size.height * 0.2412963,
        size.width * 0.7915741,
        size.height * 0.2402778,
        size.width * 0.7823148,
        size.height * 0.2402778);
    path_0.cubicTo(
        size.width * 0.7726852,
        size.height * 0.2404630,
        size.width * 0.7633333,
        size.height * 0.2415741,
        size.width * 0.7548148,
        size.height * 0.2450000);

    return path_0;
  }
}
