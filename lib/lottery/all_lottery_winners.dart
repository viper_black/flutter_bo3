import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

import '../common.dart';
import '../services/communication/http/index.dart';
import '../utils/utils.dart';
import 'config.dart';

class AllLotteryHistory extends StatefulWidget {
  final double wdgtWidth, wdgtHeight;

  const AllLotteryHistory({Key key, this.wdgtWidth, this.wdgtHeight})
      : super(key: key);
  @override
  _AllLotteryHistoryState createState() => _AllLotteryHistoryState();
}

class _AllLotteryHistoryState extends State<AllLotteryHistory> {
  bool isMobile = false;

  int selectedIndex;
  Map<String, dynamic> map = {
    'product_id': 1,
  };
  List data = [];
  List unProcessed = [];
  Future<String> temp;
  void initState() {
    temp = lotteryHistory(map);
    super.initState();
  }

  Future<String> lotteryHistory(Map map) async {
    Map<String, dynamic> result = await HttpRequest.Post(
        'lottery_all_history', Utils.constructPayload(map));
    if (Utils.isServerError(result)) {
      print('wowowowowwowowowwowow');
      print(result);
      print("LLLLLLLLAAAAAAAAAAAAAA");
      return throw (await Utils.getMessage(result['response']['error']));
    } else {
      print('fff');

      data = result['response']['data'];
      print('fffzzzzzzzzzzzzzzzzzz');

      return 'starrt';
    }
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<Object>(
        future: temp,
        builder: (context, snapshot) {
          if (snapshot.hasError) {
            return SomethingWentWrongMessage();
          } else if (snapshot.hasData) {
            return LayoutBuilder(builder: (context, constraints) {
              if (constraints.maxWidth <= 600) {
                isMobile = true;
                return Padding(
                  padding: EdgeInsets.all(Pallet1.defaultPadding),
                  child: historyList(widget.wdgtWidth),
                );
              } else if (constraints.maxWidth > 600 &&
                  constraints.maxWidth <= 950) {
                isMobile = false;

                return Padding(
                  padding: EdgeInsets.only(left: Pallet1.leftPadding),
                  child: historyList(widget.wdgtWidth * .8),
                );
              } else if (constraints.maxWidth > 950 &&
                  constraints.maxWidth <= 1200) {
                isMobile = false;

                return Padding(
                  padding: EdgeInsets.only(left: Pallet1.leftPadding),
                  child: historyList(widget.wdgtWidth * .7),
                );
              } else {
                isMobile = false;

                return Padding(
                  padding: EdgeInsets.all(Pallet1.leftPadding),
                  child: historyList(widget.wdgtWidth * .7),
                );
              }
            });
          } else {
            return Loader();
          }
        });
  }

  Widget historyList(wdgtWidth) {
    // var formatter = new DateFormat('dd-MM-yyyy');
    // String formattedDate;

    return Container(
      width: wdgtWidth,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            children: [
              Text('Lottery Winners History',
                  style: TextStyle(
                      color: Pallet1.fontcolornew,
                      fontSize: Pallet1.heading1,
                      fontWeight: Pallet1.font600)),
            ],
          ),
          SizedBox(
            height: 20,
          ),

          // --------------------------------------------------------------------------------interchange here--------
          Container(
            padding: EdgeInsets.only(left: Pallet1.defaultPadding),
            child: Wrap(runSpacing: 10, spacing: 10, children: [
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 3),
                child: Row(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    SizedBox(
                      child: Container(
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(Pallet1.radius),
                            color: Color(0xFF585858)),
                        width: 25,
                        height: 20,
                      ),
                    ),
                    SizedBox(width: 5),
                    Text("Special",
                        style: TextStyle(color: Pallet1.fontcolornew)),
                  ],
                ),
              ),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 3),
                child: Row(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    SizedBox(
                      child: Container(
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(Pallet1.radius),
                            color: Color(0xFF00ABFF)),
                        width: 25,
                        height: 20,
                      ),
                    ),
                    SizedBox(width: 5),
                    Text("Jackpot",
                        style: TextStyle(color: Pallet1.fontcolornew)),
                  ],
                ),
              ),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 3),
                child: Row(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    SizedBox(
                      child: Container(
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(Pallet1.radius),
                            color: Color(0xFFd10d0a)),
                        width: 25,
                        height: 20,
                      ),
                    ),
                    SizedBox(width: 5),
                    Text("Grand Prize",
                        style: TextStyle(color: Pallet1.fontcolornew)),
                  ],
                ),
              ),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 3),
                child: Row(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    SizedBox(
                      child: Container(
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(Pallet1.radius),
                            color: Color(0xFFFFD712)),
                        width: 25,
                        height: 20,
                      ),
                    ),
                    SizedBox(width: 5),
                    Text("Super Jackpot",
                        style: TextStyle(color: Pallet1.fontcolornew)),
                  ],
                ),
              ),
            ]),
          ),

          // ----------------------------------------------------------------------end----------------------
          SizedBox(
            height: 20,
          ),
          data.length != 0
              ? Expanded(
                  child: ListView.builder(
                      itemCount: data.length,
                      itemBuilder: (context, index) {
                        var time =
                            data[index]['created_at'].toString().split('T');

                        var a = time[1].toString().split('.');
                        return Padding(
                          padding: EdgeInsets.all(Pallet1.defaultPadding),
                          child: Container(
                            height: isMobile == true ? 80 : 40,
                            // padding: EdgeInsets.all(Pallet1.defaultPadding),
                            decoration: BoxDecoration(
                                boxShadow: [
                                  BoxShadow(
                                    color: Colors.black12,
                                    spreadRadius: 5,
                                    blurRadius: 5,
                                  )
                                ],
                                color: Color(0xFF100f2f),
                                borderRadius: BorderRadius.circular(10)),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                // Padding(
                                //   padding:
                                //       EdgeInsets.all(Pallet1.defaultPadding),
                                // child: Row(
                                //   children: [
                                Expanded(
                                  child: Center(
                                    child: Text(time[0] + ' ' + a[0].toString(),
                                        style: TextStyle(
                                          color: Pallet1.fontcolor,
                                          fontSize: Pallet1.normalfont,
                                        )),
                                  ),
                                ),
                                Expanded(
                                  child: Center(
                                    child: Text(
                                        'LOT' +
                                            data[index]['lottery_id']
                                                .toString(),
                                        style: TextStyle(
                                          color: Pallet1.fontcolor,
                                          fontSize: Pallet1.normalfont,
                                        )),
                                  ),
                                ),
                                Expanded(
                                  child: Center(
                                    child: Text(
                                        data[index]['screen_name'].toString(),
                                        // overflow: TextOverflow.ellipsis,
                                        style: TextStyle(
                                          color: Pallet1.fontcolor,
                                          fontSize: Pallet1.normalfont,
                                        )),
                                  ),
                                  //   ),
                                  // ],
                                ),
                                // ),
                                Expanded(
                                  child: Container(
                                    decoration: BoxDecoration(
                                      borderRadius:
                                          BorderRadius.circular(Pallet1.radius),
                                      image: DecorationImage(
                                          image: AssetImage(
                                            data[index]['winner_type'] ==
                                                    'special'
                                                ? 'Hgrey.png'
                                                : data[index]['winner_type'] ==
                                                        'normal'
                                                    ? 'Hpurple.png'
                                                    : data[index][
                                                                'winner_type'] ==
                                                            'both'
                                                        ? 'Hred.png'
                                                        : 'Hgreen.png',
                                          ),
                                          fit: BoxFit.fill),
                                      // color: data[index]['winner_type'] ==
                                      //         'normal'
                                      //     ? Color(0xFF585858)
                                      //     : data[index]['winner_type'] ==
                                      //             'special'
                                      //         ? Color(0xFFc62ce6)
                                      //         : data[index]['winner_type'] ==
                                      //                 'weekly'
                                      //             ? Color(0xFFd10d0a)
                                      //             : Color(0xFF0e9400),
                                    ),
                                    child: Center(
                                      child: Text(
                                          data[index]['particulars'] == 'Dollar'
                                              ? ('\$ ' +
                                                      double.parse(data[index]
                                                                  ['amount']
                                                              .toString())
                                                          .toStringAsFixed(2))
                                                  .toString()
                                              : ((double.parse(data[index]
                                                                  ['amount']
                                                              .toString()))
                                                          .toStringAsFixed(2) +
                                                      ' DUC')
                                                  .toString(),
                                          style: TextStyle(
                                            color: Pallet1.fontcolor,
                                            fontSize: Pallet1.normalfont,
                                            fontWeight: FontWeight.w700,
                                          )),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        );
                      }),
                )
              : Expanded(
                  child: Center(
                    child: Text("No Recent Winners Found",
                        style: TextStyle(
                          color: Pallet1.fontcolor,
                          fontSize: Pallet1.normalfont,
                        )),
                  ),
                ),
        ],
      ),
    );
  }
}
