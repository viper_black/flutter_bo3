// import 'dart:math';

import 'package:centurion/lottery/common.dart';
import 'package:centurion/utils/utils.dart';
import 'package:flutter/material.dart';
import '../home.dart';
import 'config.dart';
import '../services/communication/index.dart' show HttpRequest;

class RedeemLottery extends StatefulWidget {
  final String amt;
  final double wdgtWidth, wdgtHeight;

  const RedeemLottery({Key key, this.amt, this.wdgtWidth, this.wdgtHeight})
      : super(key: key);
  @override
  _RedeemLotteryState createState() => _RedeemLotteryState();
}

class _RedeemLotteryState extends State<RedeemLottery> {
  int amtval;
  bool isMobile;
  List selecttickets = [];
  List<int> fab;
  bool specialselected;
  List specialticketselected = [];
  bool special = false;
  List total = [];
  int data;
  // ignore: unused_field
  Future<dynamic> _temp;

  List serverval = [
    {'amt': '5', 'special': '1', 'normal': '8'},
    {'amt': '10', 'special': '2', 'normal': '13'}
  ];
// ------------------------------------apply this to server values----------------------

  int maxnarmalselect;
  int maxspecialselect;

// ------------------------------------------------------------------------------------
  void initState() {
    super.initState();
    findtkenlength();
    fab = fibonacci1(12);
    print(fab);
    print("mmmmmmmmmmmm");
    print(widget.amt);
    amtval = int.parse(widget.amt);
    print(amtval.runtimeType);
    // test();
  }

  // ignore: missing_return
  Future<bool> savelotterytoken(Map map) async {
    print(map);
    Map<String, dynamic> result =
        await HttpRequest.Post('lottery_token', Utils.constructPayload(map));
    print(map);
    if (Utils.isServerError(result)) {
      snack.snack(title: 'Something Went Wrong!');
      // ScaffoldMessenger.of(context)
      //     .showSnackBar(SnackBar(content: Text("Something Went Wrong!")));
    } else {
      data = result['response']['data']['id'];
      print(result['response']['data']);
      return true;
    }
  }

  findtkenlength() {
    print("lllllllllddddddddddddd");
    for (var i = 0; i < serverval.length; i++) {
      if (widget.amt == serverval[i]['amt']) {
        maxnarmalselect = int.parse(serverval[i]['normal']);
        maxspecialselect = int.parse(serverval[i]['special']);
      }
    }
    print("wwwwwwwwwwwwwwwwww");
  }

  List<int> fibonacci1(int n) {
    int last = 1;
    int last2 = 0;
    return List<int>.generate(n, (int i) {
      if (i < 2) return i;
      int curr = last + last2;
      last2 = last;
      last = curr;
      return curr;
    });
  }

  List testlist = [];
  List temptestlist = [];

  test() async {
//   var limit=0;
// while (
//   limit<=100

// ) {
//   temptestlist=[];
//     final _random = new Random();

// testlist=[];
//     var element = fab[_random.nextInt(fab.length)];

//     for (var i = 0; i < 100; i++) {
//       if (fab.contains(i)) {
//       } else {
//         testlist.add(i);
//       }
//     }

// for (var i = 0; i < 6; i++) {
//   temptestlist.add(testlist[_random.nextInt(testlist.length)]);
// }

// temptestlist.add(element);
// List finallist=temptestlist;
//    print(finallist);
// //  var map = {
// //             'product_id': 1,
// //             'amount': int.parse(widget.amt),
// //             'numbers': finallist,
// //           };
// //           bool result = await savelotterytoken(map);
// }
  }
  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
      builder: (context, constraints) {
        if (constraints.maxWidth < 320) {
          isMobile = true;
          return Padding(
            padding: EdgeInsets.fromLTRB(Pallet1.defaultPadding,
                Pallet1.defaultPadding, Pallet1.defaultPadding, 0),
            child: SingleChildScrollView(
              child: Column(
                children: [
                  profile(
                    width: widget.wdgtWidth,
                  ),
                  SizedBox(
                    height: 15,
                  ),
                  slots(
                    width: widget.wdgtWidth,
                  ),
                  SizedBox(
                    height: total.length == 0 ? 0 : 15,
                  ),
                  // tickets(
                  //   width: widget.wdgtWidth,
                  // ),
                  SizedBox(
                    height: 15,
                  ),
                  count(width: widget.wdgtWidth),
                  SizedBox(
                    height: 15,
                  ),
                  Row(
                    children: [
                      cancel(),
                      SizedBox(
                        width: 20,
                      ),
                      submit(
                        width: widget.wdgtWidth,
                      )
                    ],
                  ),
                  SizedBox(
                    height: 15,
                  ),
                ],
              ),
            ),
          );
        } else if (constraints.maxWidth >= 320 && constraints.maxWidth < 443) {
          isMobile = true;
          return Padding(
            padding: EdgeInsets.fromLTRB(Pallet1.defaultPadding,
                Pallet1.defaultPadding, Pallet1.defaultPadding, 0),
            child: SingleChildScrollView(
              child: Column(
                children: [
                  profile(
                    width: widget.wdgtWidth,
                  ),
                  SizedBox(
                    height: 15,
                  ),
                  slots(
                    width: widget.wdgtWidth,
                  ),
                  SizedBox(
                    height: total.length == 0 ? 0 : 15,
                  ),
                  // tickets(
                  //   width: widget.wdgtWidth,
                  // ),
                  SizedBox(
                    height: 15,
                  ),
                  count(width: widget.wdgtWidth),
                  SizedBox(
                    height: 15,
                  ),
                  Row(
                    children: [
                      cancel(),
                      SizedBox(
                        width: 20,
                      ),
                      submit(
                        width: widget.wdgtWidth,
                      )
                    ],
                  ),
                  SizedBox(
                    height: 15,
                  ),
                ],
              ),
            ),
          );
        } else if (constraints.maxWidth >= 443 && constraints.maxWidth <= 703) {
          isMobile = true;
          return Padding(
            padding: EdgeInsets.fromLTRB(Pallet1.defaultPadding,
                Pallet1.defaultPadding, Pallet1.defaultPadding, 0),
            child: SingleChildScrollView(
              child: Column(
                children: [
                  profile(
                    width: widget.wdgtWidth,
                  ),
                  SizedBox(
                    height: 15,
                  ),
                  slots(
                    width: widget.wdgtWidth,
                  ),
                  // SizedBox(
                  //   height: total.length == 0 ? 0 : 15,
                  // ),
                  // tickets(
                  //   width: widget.wdgtWidth,
                  // ),
                  SizedBox(
                    height: 15,
                  ),
                  count(width: widget.wdgtWidth),
                  SizedBox(
                    height: 15,
                  ),
                  Row(
                    children: [
                      cancel(),
                      SizedBox(
                        width: 20,
                      ),
                      submit(
                        width: widget.wdgtWidth,
                      )
                    ],
                  ),
                  SizedBox(
                    height: 15,
                  ),
                ],
              ),
            ),
          );
        } else if (constraints.maxWidth > 703 && constraints.maxWidth <= 1024) {
          isMobile = false;

          return Padding(
            padding: EdgeInsets.fromLTRB(Pallet1.defaultPadding,
                Pallet1.defaultPadding, Pallet1.defaultPadding, 0),
            child: SingleChildScrollView(
              child: Column(
                children: [
                  profile(
                    width: widget.wdgtWidth,
                  ),
                  SizedBox(
                    height: 15,
                  ),
                  slots(
                    width: widget.wdgtWidth,
                  ),
                  // SizedBox(
                  //   height: total.length == 0 ? 0 : 15,
                  // ),
                  // tickets(
                  //   width: widget.wdgtWidth,
                  // ),
                  SizedBox(
                    height: 15,
                  ),
                  count(
                    width: widget.wdgtWidth,
                  ),
                  SizedBox(
                    height: 15,
                  ),
                  Row(
                    children: [
                      cancel(),
                      SizedBox(
                        width: 20,
                      ),
                      submit(
                        width: widget.wdgtWidth,
                      )
                    ],
                  ),
                  SizedBox(
                    height: 15,
                  ),
                ],
              ),
            ),
          );
        } else {
          isMobile = false;
          return Padding(
            padding: EdgeInsets.symmetric(
              horizontal: Pallet1.leftPadding,
            ),
            child: SingleChildScrollView(
              child: Container(
                // color: Colors.green,
                child: Column(
                  children: [
                    profile(
                      width: widget.wdgtWidth,
                    ),
                    SizedBox(
                      height: 15,
                    ),
                    slots(
                      width: widget.wdgtWidth,
                    ),
                    // SizedBox(
                    //   height: total.length == 0 ? 0 : 15,
                    // ),
                    // tickets(
                    //   width: widget.wdgtWidth,
                    // ),
                    SizedBox(
                      height: 15,
                    ),
                    Wrap(
                      children: [
                        count(
                          width: (constraints.maxWidth > 1024 &&
                                  constraints.maxWidth <= 1500)
                              ? widget.wdgtWidth * .85
                              : widget.wdgtWidth * .6,
                        )
                      ],
                    ),
                    SizedBox(
                      height: 15,
                    ),
                    Row(children: [
                      cancel(),
                      SizedBox(
                        width: 20,
                      ),
                      submit(
                        width: widget.wdgtWidth,
                      ),
                    ]),
                    SizedBox(
                      height: 15,
                    ),
                  ],
                ),
              ),
            ),
          );
        }
      },
    );
  }

  Widget profile({double width}) {
    return Container(
      width: width,
      child: Text(
        "Lottery",
        style: TextStyle(
            color: Pallet1.fontcolornew,
            fontSize: Pallet1.heading1,
            fontWeight: Pallet1.bold),
      ),
    );
  }

  Widget tickets({double width}) {
    return Container(
      width: width,
      child: Text(
        "Lottery Tickets:",
        style: TextStyle(
            color: Pallet1.fontcolornew,
            fontSize: Pallet1.heading2,
            fontWeight: Pallet1.bold),
      ),
    );
  }

  Widget slots({double width}) {
    return Container(
      width: width,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          if (isMobile == true)
            Container(
              padding: EdgeInsets.symmetric(horizontal: 4),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  legends(),
                  SizedBox(height: 6),
                ],
              ),
            ),
          Padding(
            padding: EdgeInsets.all(10.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            "Special Numbers",
                            style: TextStyle(
                              color: Pallet1.fontcolornew,
                              fontWeight: Pallet1.font600,
                              fontSize: Pallet1.heading3,
                            ),
                          ),
                          SizedBox(
                            // height: specialticketselected.length == 0 ? 0 : 10,
                            height: 10,
                          ),
                          Wrap(
                            spacing: 5,
                            runSpacing: 5,
                            children: [
                              for (var i = 0; i < maxspecialselect; i++)
                                Opacity(
                                  opacity: specialticketselected.length - 1 >= i
                                      ? 1
                                      : 0.6,
                                  child: myContainer(
                                      specialticketselected.length - 1 >= i
                                          ? specialticketselected[i]
                                          : ''),
                                ),
                              // : myContainer(specialticketselected[i]),
                            ],
                          ),
                        ],
                      ),
                    ),
                    isMobile == true
                        ? Container()
                        : Padding(
                            padding: EdgeInsets.symmetric(
                                horizontal: Pallet1.defaultPadding),
                            child: Container(
                                width: 2,
                                height: 80,
                                // (selecttickets.length == 0 &&
                                //         specialticketselected.length == 0)
                                //     ? 25
                                //     : 80,
                                color: Pallet1.fontcolornew),
                          ),
                    isMobile == true ? Container() : columnNormalNumbers(),
                  ],
                ),
              ],
            ),
          ),
          SizedBox(
            height: 10,
          ),
          isMobile == true ? columnNormalNumbers() : Container(),
        ],
      ),
    );
  }

  Widget legends() {
    return Column(
      children: [
        Row(
          mainAxisSize: MainAxisSize.min,
          children: [
            SizedBox(
              width: 10,
              height: 10,
              child: Container(
                decoration: BoxDecoration(
                    color: Colors.white,
                    border: Border.all(
                      color: Colors.black,
                    )),
              ),
            ),
            SizedBox(width: 5),
            Text("Normal Numbers",
                style: TextStyle(color: Pallet1.fontcolornew)),
          ],
        ),
        Padding(
          padding: EdgeInsets.symmetric(horizontal: 3),
          child: Row(
            mainAxisSize: MainAxisSize.min,
            children: [
              SizedBox(
                width: 10,
                height: 10,
                child: Container(
                  color: Pallet1.dashcontainerback,
                ),
              ),
              SizedBox(width: 5),
              Text("Special Numbers",
                  style: TextStyle(color: Pallet1.fontcolornew)),
            ],
          ),
        ),
      ],
    );
  }

  Widget columnNormalNumbers() {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            "Normal Numbers",
            style: TextStyle(
              color: Pallet1.fontcolornew,
              fontWeight: Pallet1.font600,
              fontSize: Pallet1.heading3,
            ),
          ),
          SizedBox(
            // height: selecttickets.length == 0 ? 0 : 10,
            height: 10,
          ),
          Wrap(
            spacing: 5,
            runSpacing: 5,
            children: [
              for (var i = 0; i < maxnarmalselect; i++)
                Opacity(
                  opacity: selecttickets.length - 1 >= i ? 1 : 0.6,
                  child: myContainer(
                      selecttickets.length - 1 >= i ? selecttickets[i] : ''),
                ),
              // for (var i = 0; i < selecttickets.length; i++)
              //   myContainer(selecttickets[i]),
            ],
          ),
        ],
      ),
    );
  }

  myContainer(text) {
    return Container(
      width: 50,
      height: 50,
      padding: EdgeInsets.all(5),
      decoration: BoxDecoration(
          color: Pallet1.inner1,
          shape: BoxShape.circle,
          boxShadow: [
            BoxShadow(
              color: Colors.black12,
            )
          ]),
      child: Center(
        child: Padding(
          padding: EdgeInsets.all(Pallet1.defaultPadding),
          child: Text(text.toString(),
              style: TextStyle(
                  fontSize: Pallet1.normalfont, color: Pallet1.fontcolor)),
        ),
      ),
    );
  }

  mydialge() {
    return showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return AlertDialog(
            scrollable: true,
            elevation: 24.0,
            backgroundColor: Pallet1.fontcolornew,
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(15.0))),
            title: Container(
              child: Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text('New Lottery',
                      style: TextStyle(
                          fontWeight: Pallet1.font500,
                          fontSize: Pallet1.heading3,
                          color: Pallet1.fontcolor)),
                  Text('Ticket No : LOT' + data.toString(),
                      style: TextStyle(
                          fontSize: Pallet1.heading5,
                          color: Pallet1.fontcolor)),
                  SizedBox(height: 10),
                  Text('Special Number',
                      style: TextStyle(
                          fontWeight: Pallet1.font500,
                          fontSize: Pallet1.heading4,
                          color: Pallet1.fontcolor)),
                  SizedBox(height: 15),
                  Container(
                    width: double.infinity,
                    decoration: BoxDecoration(
                        color: Colors.transparent,
                        borderRadius: BorderRadius.circular(6)),
                    child: Wrap(
                      spacing: 5,
                      runSpacing: 5,
                      children: [
                        for (var i = 0; i < specialticketselected.length; i++)
                          Container(
                            width: 50,
                            height: 50,
                            decoration: BoxDecoration(
                                color: Pallet1.inner1,
                                borderRadius: BorderRadius.circular(50)),
                            child: Center(
                              child: Text(specialticketselected[i].toString(),
                                  style: TextStyle(
                                      fontWeight: Pallet1.font500,
                                      fontSize: Pallet1.heading4,
                                      color: Pallet1.fontcolor)),
                            ),
                          ),
                      ],
                    ),
                  ),
                  SizedBox(height: 10),
                  Text('Normal Number',
                      style: TextStyle(
                          fontWeight: Pallet1.font500,
                          fontSize: Pallet1.heading4,
                          color: Pallet1.fontcolor)),
                  SizedBox(height: 15),
                  Container(
                    width: double.infinity,
                    decoration: BoxDecoration(
                        color: Colors.transparent,
                        borderRadius: BorderRadius.circular(6)),
                    child: Wrap(
                      spacing: 5,
                      runSpacing: 5,
                      children: [
                        for (var i = 0; i < selecttickets.length; i++)
                          Container(
                            width: 50,
                            height: 50,
                            decoration: BoxDecoration(
                                // boxShadow: [Pallet1.shadowEffect],
                                color: Pallet1.fontcolor,
                                borderRadius: BorderRadius.circular(50)),
                            child: Center(
                              child: Text(selecttickets[i].toString(),
                                  style: TextStyle(
                                      fontWeight: Pallet1.font500,
                                      fontSize: Pallet1.heading4,
                                      color: Pallet1.fontcolornew)),
                            ),
                          ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
            actions: [
              PopupButton1(
                text: 'Continue',
                onpress: () async {
                  Navigator.of(context).pop();
                  lotterySuccess();
                },
              ),
              SizedBox(height: 10),
            ],
          );
        });
  }

  lotterySuccess() {
    return showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return AlertDialog(
            scrollable: true,
            elevation: 24.0,
            backgroundColor: Pallet1.fontcolor,
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(15.0))),
            title: Text('Success',
                maxLines: 2,
                style: TextStyle(
                    fontSize: Pallet1.heading4,
                    color: Pallet1.fontcolornew,
                    fontWeight: Pallet1.font500)),
            content: Container(
              width: 350,
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  ClipOval(
                    child: Container(
                      padding: EdgeInsets.all(15),
                      color: Colors.green,
                      child: Icon(
                        Icons.done,
                        color: Colors.white,
                      ),
                    ),
                  ),
                  SizedBox(height: 10),
                  Text("Lottery Ticket is Bought Successfully with",
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          height: 1.4,
                          fontWeight: Pallet1.font500,
                          fontSize: Pallet1.heading4,
                          color: Pallet1.fontcolornew)),
                  SizedBox(height: 15),
                  Text('Ticket No : LOT' + data.toString(),
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          fontSize: Pallet1.heading5,
                          fontWeight: Pallet1.subheading1wgt,
                          color: Pallet1.fontcolornew)),
                  SizedBox(height: 15),
                  Row(
                    children: [
                      Text('Special Number',
                          style: TextStyle(
                              fontWeight: Pallet1.font500,
                              fontSize: Pallet1.heading4,
                              color: Pallet1.fontcolornew)),
                    ],
                  ),
                  SizedBox(height: 15),
                  Container(
                    width: double.infinity,
                    decoration: BoxDecoration(
                        color: Colors.transparent,
                        borderRadius: BorderRadius.circular(6)),
                    child: Wrap(
                      spacing: 5,
                      runSpacing: 5,
                      children: [
                        for (var i = 0; i < specialticketselected.length; i++)
                          Container(
                            width: 50,
                            height: 50,
                            decoration: BoxDecoration(
                                color: Pallet1.inner1,
                                borderRadius: BorderRadius.circular(50)),
                            child: Center(
                              child: Text(specialticketselected[i].toString(),
                                  style: TextStyle(
                                      fontWeight: Pallet1.font500,
                                      fontSize: Pallet1.heading4,
                                      color: Pallet1.fontcolor)),
                            ),
                          ),
                      ],
                    ),
                  ),
                  SizedBox(height: 15),
                  Row(
                    children: [
                      Text('Normal Number',
                          style: TextStyle(
                              fontWeight: Pallet1.font500,
                              fontSize: Pallet1.heading4,
                              color: Pallet1.fontcolornew)),
                    ],
                  ),
                  SizedBox(height: 15),
                  Container(
                    width: double.infinity,
                    decoration: BoxDecoration(
                        color: Colors.transparent,
                        borderRadius: BorderRadius.circular(6)),
                    child: Wrap(
                      spacing: 5,
                      runSpacing: 5,
                      children: [
                        for (var i = 0; i < selecttickets.length; i++)
                          Container(
                            width: 50,
                            height: 50,
                            decoration: BoxDecoration(
                                // boxShadow: [Pallet1.shadowEffect],
                                color: Pallet1.fontcolor,
                                border: Border.all(
                                  color: Colors.black,
                                ),
                                borderRadius: BorderRadius.circular(50)),
                            child: Center(
                              child: Text(selecttickets[i].toString(),
                                  style: TextStyle(
                                      fontWeight: Pallet1.font500,
                                      fontSize: Pallet1.heading4,
                                      color: Pallet1.fontcolornew)),
                            ),
                          ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
            actions: [
              PopupButton1(
                text: 'Close',
                buttoncolor: Pallet1.fontcolornew,
                textcolor: Pallet1.fontcolor,
                onpress: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (BuildContext context) => Home(
                                route: 'winner_Announcement',
                              )));
                },
              ),
              SizedBox(height: 10),
            ],
          );
        });
  }

  Widget submit({double width}) {
    return PopupButton1(
      text: "Submit",
      onpress: () async {
        if (specialticketselected.length < maxspecialselect) {
          snack.snack(title: 'You Haven\'t Selected all the Special Numbers');
        } else if (selecttickets.length < maxnarmalselect) {
          snack.snack(title: 'You Haven\'t Selected all the Normal Numbers');
        } else {
          var map = {
            'product_id': 1,
            'amount': int.parse(widget.amt),
            'numbers': total,
          };
          bool result = await savelotterytoken(map);
          print("mmmmmmmmmmmmmmmmmmmmmmmmmmmnnnnnnnnnnnn");
          print(result);
          if (result == true) {
            lotterySuccess();
          } else {}
        }
      },
      vpadding: 10,
      hpadding: 10,
      buttoncolor: Pallet1.fontcolornew,
      textcolor: Pallet1.fontcolor,
      fontweight: Pallet1.font600,
    );
  }

  Widget cancel() {
    return Container(
        child: Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        PopupButton1(
          text: "Cancel",
          onpress: () {
            Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (BuildContext context) => Home(
                          route: 'dashboard',
                        )));
          },
          vpadding: 10,
          hpadding: 10,
          buttoncolor: Pallet1.fontcolornew,
          textcolor: Pallet1.fontcolor,
          fontweight: Pallet1.font600,
        ),
      ],
    ));
  }

  Widget count({width}) {
    return Container(
      width: width,
      child: Column(
        children: [
          if (isMobile == false)
            Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                Padding(
                  padding: const EdgeInsets.only(right: 18.0),
                  child: legends(),
                ),
              ],
            ),
          SizedBox(
            height: 15,
          ),
          Wrap(
            runSpacing: 5,
            spacing: 5,
            verticalDirection: VerticalDirection.down,
            children: [
              for (var index = 0; index < 100; index++)
                InkWell(
                  onTap: () {
                    setState(() {
                      if (fab.contains(index) == true) {
                        if (specialticketselected.contains(index) == true) {
                          special = false;
                          specialticketselected.remove(index);
                        } else if (specialticketselected.length + 1 <=
                            maxspecialselect) {
                          specialticketselected.add(index);
                          // specialticketselected.length == 1
                          //     ? special = true
                          //     : special = false;
                        } else if (specialticketselected.length + 1 >
                            maxspecialselect) {
                          // ScaffoldMessenger.of(context).removeCurrentSnackBar();

                          // ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                          //   content: Text(
                          //       "You already Selected $maxspecialselect Special Lottery"),
                          // ));

                          snack.snack(
                              title:
                                  'You already Selected $maxspecialselect Special Numbers');
                        }
                      } else {
                        if (selecttickets.contains(index) == true) {
                          // print("bbbbbvvvvvvvvvvvv");

                          // print("eeeeeeeeeeeeeeeeeeeeeee");
                          for (var i = 0; i < selecttickets.length; i++) {
                            if (index == selecttickets[i]) {
                              // print("eeeeeeeeeeeeeeeeeeeeeee");
                              // print(i);
                              selecttickets.removeAt(i);
                            }
                          }
                          print("eeeeeeeeeeeeeeeeeeeeeee");
                        } else {
                          // print("ggggggggggggggggg");
                          // print(specialticketselected.length);
                          // print(special);
                          if (selecttickets.length + 1 > maxnarmalselect) {
                            // ScaffoldMessenger.of(context)
                            //     .removeCurrentSnackBar();

                            // ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                            //   content: Text(
                            //       "You already Selected $maxnarmalselect Lottery"),
                            // ));

                            snack.snack(
                                title:
                                    'You already Selected $maxnarmalselect Normal Numbers');
                          } else if (selecttickets.length + 1 <=
                              maxnarmalselect) {
                            if (fab.contains(index) == true) {
                            } else {
                              selecttickets.add(index);
                            }
                            // print("vvvvvvvvvvvvvvvvvvvvvvvv");
                          }
                        }
                      }
                    });

                    // -----------------------find the total selected tickets-----------------------------------
                    total = selecttickets + specialticketselected;
                    // ----------------------------------------------------------------------------------
                    // print(specialticketselected);
                    // print(specialticketselected.length);
                    // print("ddddddddddd");
                    // print(selecttickets);
                    // print(selecttickets.length);
                    // print("eeeeeeeeeeeeee");
                    // print(selecttickets + specialticketselected);
                  },
                  child: Container(
                    width: 70,
                    padding: EdgeInsets.all(3),
                    decoration: BoxDecoration(
                      // color: Colors.white,
                      color:
                          // Colors.pink,
                          total.contains(index) == true
                              ? specialticketselected.contains(index) == true
                                  ? Colors.green
                                  : Colors.red
                              : Pallet1.fontcolor,
                      shape: BoxShape.circle,
                      border: Border.all(
                        color: total.contains(index) == true
                            ? specialticketselected.contains(index) == true
                                ? Colors.green
                                : Colors.red
                            : Pallet1.fontcolor,
                      ),
                    ),
                    child: Container(
                      // width: 200,
                      padding: EdgeInsets.all(5),
                      decoration: BoxDecoration(
                          color: total.contains(index) == true
                              ? Pallet1.popupcontainerback
                              : fab.contains(index) == true
                                  ? Pallet1.dashsmallcontainerback
                                  : Colors.white,
                          shape: BoxShape.circle,
                          border: Border.all(
                            color: Pallet1.fontcolornew,
                          ),
                          boxShadow: [
                            BoxShadow(
                              color: Colors.black12,
                            )
                          ]),
                      child: Center(
                        child: Padding(
                          padding: EdgeInsets.all(Pallet1.defaultPadding),
                          child: Text(
                            index.toString(),
                            style: TextStyle(
                              fontSize: Pallet1.normalfont,
                              color: total.contains(index) == true
                                  ? Colors.grey
                                  : fab.contains(index) == true
                                      ? Colors.white
                                      : Pallet1.fontcolornew,
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
            ],
          ),
        ],
      ),
    );
    // )
    // );
  }
}
