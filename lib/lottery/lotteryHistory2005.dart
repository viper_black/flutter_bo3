import 'package:flutter/material.dart';
import '../common.dart';
import '../services/communication/http/index.dart';
import '../utils/utils.dart';
import 'config.dart';

class LotteryHistory extends StatefulWidget {
  final double wdgtWidth, wdgtHeight;

  const LotteryHistory({Key key, this.wdgtWidth, this.wdgtHeight})
      : super(key: key);
  @override
  _LotteryHistoryState createState() => _LotteryHistoryState();
}

class _LotteryHistoryState extends State<LotteryHistory> {
  bool isMobile = false;

  int selectedIndex;
  Map<String, dynamic> map = {
    'product_id': 1,
  };
  List data = [];
  List unProcessed = [];
  Future<String> temp;
  void initState() {
    temp = lotteryHistory(map);
    super.initState();
  }

  Future<String> lotteryHistory(Map map) async {
    Map<String, dynamic> result =
        await HttpRequest.Post('lottery_History', Utils.constructPayload(map));
    if (Utils.isServerError(result)) {
      print('wowowowowwowowowwowow');
      return throw (await Utils.getMessage(result['response']['error']));
    } else {
      print('fff');

      data = result['response']['data']['history'];
      unProcessed = result['response']['data']['unprocessed'];
      print('.............................');
      print(unProcessed);
      return 'starrt';
    }
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<Object>(
        future: temp,
        builder: (context, snapshot) {
          if (snapshot.hasError) {
            return SomethingWentWrongMessage();
          } else if (snapshot.hasData) {
            return LayoutBuilder(builder: (context, constraints) {
              if (constraints.maxWidth <= 600) {
                isMobile = true;
                return Padding(
                  padding: EdgeInsets.all(Pallet1.defaultPadding),
                  child: historyList(widget.wdgtWidth),
                );
              } else if (constraints.maxWidth > 600 &&
                  constraints.maxWidth <= 950) {
                isMobile = true;

                return Padding(
                  padding: EdgeInsets.only(left: Pallet1.leftPadding),
                  child: historyList(widget.wdgtWidth * .8),
                );
              } else if (constraints.maxWidth > 950 &&
                  constraints.maxWidth <= 1200) {
                isMobile = false;

                return Padding(
                  padding: EdgeInsets.only(left: Pallet1.leftPadding),
                  child: historyList(widget.wdgtWidth * .7),
                );
              } else {
                isMobile = false;

                return Padding(
                  padding: EdgeInsets.all(Pallet1.leftPadding),
                  child: historyList(widget.wdgtWidth * .7),
                );
              }
            });
          } else {
            return Loader();
          }
        });
  }

  Widget historyList(wdgtWidth) {
    // var formatter = new DateFormat('dd-MM-yyyy');
    // String formattedDate;
    double width = MediaQuery.of(context).size.width;
    return Container(
      width: wdgtWidth,
      child: ListView(
        children: [
          Text('My Lottery Tickets',
              style: TextStyle(
                  color: Pallet1.fontcolornew,
                  fontSize: Pallet1.heading1,
                  fontWeight: Pallet1.font600)),
          SizedBox(
            height: 20,
          ),
          unProcessed.length == 0
              ? Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text("No Lottery Tickets Found",
                        style: TextStyle(
                            color: Pallet1.fontcolornew,
                            fontSize: Pallet1.heading4,
                            fontWeight: Pallet1.font500)),
                  ],
                )
              : Container(),
          Column(
            // crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              // for (var item in unProcessed)

              for (var i = unProcessed.length - 1; i >= 0; i--)
                Padding(
                  padding: EdgeInsets.all(Pallet1.defaultPadding),
                  child: Container(
                    decoration: BoxDecoration(
                        color: Colors.grey[200],
                        border: Border.all(color: Colors.black),
                        borderRadius: BorderRadius.circular(Pallet1.radius)),
                    child: Padding(
                      padding: EdgeInsets.all(Pallet1.defaultPadding),
                      child: width < 1095
                          ? Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                unprocessedTickets(
                                  item: unProcessed[i],
                                ),
                                Text(
                                    'Ticket Id : ' +
                                        'LOT' +
                                        unProcessed[i]['id'].toString(),
                                    style: TextStyle(
                                        color: Pallet1.fontcolornew,
                                        fontSize: Pallet1.heading4)),
                                SizedBox(height: 10),
                                rowDailyProcess(unProcessed[i]),
                                SizedBox(height: 10),
                                rowweeklyProcess(unProcessed[i]),
                                if (unProcessed[i]['lucky_numbers'] != null)
                                  Container(
                                    padding:
                                        EdgeInsets.symmetric(horizontal: 5),
                                    child: Text('Lucky Numbers',
                                        style: TextStyle(
                                            color: Pallet1.fontcolornew,
                                            fontSize: Pallet1.heading4)),
                                  ),
                                Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Wrap(
                                      children: [
                                        if (unProcessed[i]['lucky_numbers'] !=
                                            null)
                                          for (var items in unProcessed[i]
                                              ['lucky_numbers'])
                                            luckyNumbers(items: items),
                                      ],
                                    ),
                                  ],
                                )
                              ],
                            )
                          : Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Expanded(
                                      flex: 3,
                                      child: unprocessedTickets(
                                        item: unProcessed[i],
                                      ),
                                    ),
                                    Expanded(
                                      flex: 1,
                                      child: Column(
                                        // crossAxisAlignment: CrossAxisAlignment.start,
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          Text(
                                              'Ticket Id : ' +
                                                  'LOT' +
                                                  unProcessed[i]['id']
                                                      .toString(),
                                              style: TextStyle(
                                                  color: Pallet1.fontcolornew,
                                                  fontSize: Pallet1.heading4)),
                                          SizedBox(height: 5),
                                          rowDailyProcess(unProcessed[i]),
                                          SizedBox(height: 5),
                                          rowweeklyProcess(unProcessed[i]),
                                        ],
                                      ),
                                    ),
                                  ],
                                ),
                                if (unProcessed[i]['lucky_numbers'] != null)
                                  Container(
                                    padding:
                                        EdgeInsets.symmetric(horizontal: 10),
                                    child: Text('Lucky Numbers',
                                        style: TextStyle(
                                            color: Pallet1.fontcolornew,
                                            fontSize: Pallet1.heading4)),
                                  ),
                                Padding(
                                  padding: EdgeInsets.symmetric(horizontal: 5),
                                  child: Row(
                                    // mainAxisAlignment:
                                    //     MainAxisAlignment
                                    //         .spaceBetween,
                                    children: [
                                      if (unProcessed[i]['lucky_numbers'] !=
                                          null)
                                        for (var items in unProcessed[i]
                                            ['lucky_numbers'])
                                          luckyNumbers(items: items),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                    ),
                  ),
                ),
            ],
          ),
          SizedBox(
            height: 10,
          ),
          data.length == 0
              ? Container()
              : Text('Purchase History',
                  style: TextStyle(
                      color: Pallet1.fontcolornew,
                      fontSize: Pallet1.heading1,
                      fontWeight: Pallet1.font600)),
          if (data.length == 0)
            SizedBox(
              height: 20,
            ),
          // data.length == 0
          //     ? Expanded(
          //         child: Center(
          //           child: Text("No Purchases Found",
          //               style: TextStyle(
          //                 color: Pallet1.fontcolornew,
          //                 fontSize: Pallet1.normalfont,
          //               )),
          //         ),
          //       )
          //     : Container(),
          for (var i = 0; i < data.length; i++)
            if (data[i]['purchase'] != null)
              InkWell(
                onTap: () {
                  setState(() {
                    if (selectedIndex == i) {
                      selectedIndex = null;
                    } else {
                      selectedIndex = i;
                    }
                  });
                },
                child: Padding(
                  padding: EdgeInsets.all(Pallet1.defaultPadding),
                  child: Container(
                    decoration: BoxDecoration(
                        color: selectedIndex == i
                            ? Colors.blue[100]
                            : Colors.grey[200],
                        border: Border.all(color: Colors.black),
                        borderRadius: BorderRadius.circular(Pallet1.radius)),
                    child: Column(
                      children: [
                        Padding(
                          padding: EdgeInsets.all(Pallet1.defaultPadding),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Expanded(
                                child: Text(
                                    data[i]['created_at']
                                            .toString()
                                            .split('T')[0] +
                                        " " +
                                        data[i]['created_at']
                                            .toString()
                                            .split('T')[1]
                                            .toString()
                                            .split('.')[0]
                                            .toString(),
                                    style: TextStyle(
                                        color: Pallet1.fontcolornew,
                                        fontSize: Pallet1.heading4)),
                              ),
                              Text(data[i]['day'].toString(),
                                  style: TextStyle(
                                      color: Pallet1.fontcolornew,
                                      fontSize: Pallet1.heading4)),
                            ],
                          ),
                        ),
                        selectedIndex == i
                            ? SingleChildScrollView(
                                child: Column(
                                  children: [
                                    Container(
                                      // color: Colors.green,
                                      child: Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          if (data[i]['purchase'] != null)
                                            for (var item in data[i]
                                                ['purchase'])
                                              Padding(
                                                padding: EdgeInsets.all(
                                                    Pallet1.defaultPadding),
                                                child: Container(
                                                    decoration: BoxDecoration(
                                                        borderRadius:
                                                            BorderRadius.circular(
                                                                Pallet1.radius),
                                                        border: Border.all(
                                                            color:
                                                                Colors.black),
                                                        color: (item['daily_processed'] ==
                                                                        true &&
                                                                    item['weekly_processed'] ==
                                                                        true) ==
                                                                true
                                                            ? Colors.white
                                                            : (item['daily_processed'] ==
                                                                            false &&
                                                                        item['weekly_processed'] ==
                                                                            false) ==
                                                                    true
                                                                ? Colors.pink
                                                                : (item['daily_processed'] == false && item['weekly_processed'] == true) ==
                                                                        true
                                                                    ? Colors
                                                                        .yellow
                                                                    : (item['daily_processed'] == true && item['weekly_processed'] == false) ==
                                                                            true
                                                                        ? Colors.green
                                                                        : Colors.white),
                                                    child: Padding(
                                                      padding: EdgeInsets.all(
                                                          Pallet1
                                                              .defaultPadding),
                                                      child: isMobile == true
                                                          ? Column(
                                                              crossAxisAlignment:
                                                                  CrossAxisAlignment
                                                                      .start,
                                                              children: [
                                                                Ticket(
                                                                  item: item,
                                                                ),
                                                                textCreatedat(
                                                                    item: item),
                                                                wrappp(
                                                                    item: item),
                                                                rowDailyProcess(
                                                                    item),
                                                                rowweeklyProcess(
                                                                    item),
                                                              ],
                                                            )
                                                          : Row(
                                                              // mainAxisAlignment:
                                                              //     MainAxisAlignment.spaceBetween,
                                                              children: [
                                                                Expanded(
                                                                  flex: 3,
                                                                  child: wrappp(
                                                                      item:
                                                                          item),
                                                                ),
                                                                Expanded(
                                                                  flex: 1,
                                                                  child: Column(
                                                                    crossAxisAlignment:
                                                                        CrossAxisAlignment
                                                                            .start,
                                                                    children: [
                                                                      Ticket(
                                                                        item:
                                                                            item,
                                                                      ),
                                                                      SizedBox(
                                                                          height:
                                                                              5),
                                                                      textCreatedat(
                                                                          item:
                                                                              item),
                                                                      SizedBox(
                                                                          height:
                                                                              5),
                                                                      rowDailyProcess(
                                                                          item),
                                                                      SizedBox(
                                                                          height:
                                                                              5),
                                                                      rowweeklyProcess(
                                                                          item),
                                                                    ],
                                                                  ),
                                                                ),
                                                              ],
                                                            ),
                                                    )),
                                              ),
                                          if (data[i]['purchase'] == null)
                                            // SizedBox(
                                            //   height: 10,
                                            // ),
                                            Text('No purchases found',
                                                style: TextStyle(
                                                    color: Pallet1.fontcolornew,
                                                    fontWeight: Pallet1.bold,
                                                    fontSize:
                                                        Pallet1.heading4)),
                                          SizedBox(
                                            height: 10,
                                          ),
                                        ],
                                      ),
                                    ),
                                    // for (var item in )
                                    Padding(
                                      padding: EdgeInsets.all(
                                          Pallet1.defaultPadding),
                                      child: Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          Container(
                                            padding: EdgeInsets.symmetric(
                                                horizontal: 5),
                                            child: Text('Lucky Numbers',
                                                style: TextStyle(
                                                    color: Pallet1.fontcolornew,
                                                    fontSize:
                                                        Pallet1.heading4)),
                                          ),
                                          isMobile == true
                                              ? Column(
                                                  crossAxisAlignment:
                                                      CrossAxisAlignment.start,
                                                  children: [
                                                    Wrap(
                                                      children: [
                                                        for (var items in data[
                                                            i]['lucky_numbers'])
                                                          luckyNumbers(
                                                              items: items),
                                                      ],
                                                    ),
                                                    SizedBox(
                                                      height: 10,
                                                    ),
                                                    // Spacer(flex: 1),
                                                    jackpotPercent(
                                                        data: data, i: i)
                                                  ],
                                                )
                                              : Row(
                                                  // mainAxisAlignment:
                                                  //     MainAxisAlignment
                                                  //         .spaceBetween,
                                                  children: [
                                                    for (var items in data[i]
                                                        ['lucky_numbers'])
                                                      luckyNumbers(
                                                          items: items),
                                                    SizedBox(
                                                      height: 10,
                                                    ),
                                                    Spacer(flex: 2),
                                                    jackpotPercent(
                                                        data: data, i: i)
                                                  ],
                                                ),
                                        ],
                                      ),
                                    ),
                                  ],
                                ),
                              )
                            : Container(
                                color: Colors.yellow,
                              )
                      ],
                    ),
                  ),
                ),
              )
        ],
      ),
    );
  }

  Row rowweeklyProcess(item) {
    return Row(
      children: [
        Text('Weekly Draw : ',
            style: TextStyle(
                color: Pallet1.fontcolornew, fontSize: Pallet1.heading4)),
        SizedBox(width: 5),
        item['weekly_processed'] == true && item['weekly_winner'] == true
            ? Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(Pallet1.radius),
                  color: Pallet1.fontcolornew,
                ),
                child: Icon(
                  Icons.check,
                  size: 10,
                  color: Pallet1.fontcolor,
                ))
            : Text('--')
      ],
    );
  }

  Row rowDailyProcess(item) {
    return Row(
      children: [
        Text('Daily Draw : ',
            style: TextStyle(
                color: Pallet1.fontcolornew, fontSize: Pallet1.heading4)),
        SizedBox(width: 5),
        item['daily_processed'] == true && item['daily_winner'] == true
            ? Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(Pallet1.radius),
                  color: Pallet1.fontcolornew,
                ),
                child: Icon(
                  Icons.check,
                  size: 15,
                  color: Pallet1.fontcolor,
                ))
            : Text('--')
      ],
    );
  }
}

// ignore: camel_case_types
class jackpotPercent extends StatelessWidget {
  const jackpotPercent({
    Key key,
    @required this.data,
    @required this.i,
  }) : super(key: key);

  final List data;
  final int i;

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.end,
      children: [
        Text('Next Week Jackpot %(Cash : DUC)',
            style: TextStyle(
                color: Pallet1.fontcolornew, fontSize: Pallet1.heading2)),
        Text(
            data[i]["jackpot_percentage"].toString() +
                '% ' +
                ': ' +
                (100 - num.parse(data[i]["jackpot_percentage"])).toString() +
                '%',
            style: TextStyle(
                color: Pallet1.fontcolornew, fontSize: Pallet1.heading4)),
      ],
    );
  }
}

// ignore: camel_case_types
class luckyNumbers extends StatelessWidget {
  const luckyNumbers({
    Key key,
    @required this.items,
  }) : super(key: key);

  final items;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.all(Pallet1.defaultPadding / 2),
      child: Container(
        width: 40,
        decoration: BoxDecoration(
            color: ([0, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89]
                        .contains(int.parse(items))) ==
                    true
                ? Pallet1.dashsmallcontainerback
                : Colors.white,
            border: Border.all(
                color: ([0, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89]
                            .contains(int.parse(items))) ==
                        true
                    ? Colors.transparent
                    : Colors.grey),
            // color:
            //     ([0, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89].contains(items)) == true
            //         ? Colors.red
            //         :
            //         Colors.green,
            borderRadius: BorderRadius.circular(Pallet1.radius)),
        child: Padding(
          padding: EdgeInsets.symmetric(vertical: Pallet1.defaultPadding),
          child: Center(
            child: Text(items.toString(),
                style: TextStyle(
                    color: ([0, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89]
                                .contains(int.parse(items))) ==
                            true
                        ? Pallet1.fontcolor
                        : Pallet1.dashsmallcontainerback,
                    fontSize: Pallet1.heading4)),
          ),
        ),
      ),
    );
  }
}

class Ticket extends StatelessWidget {
  const Ticket({
    Key key,
    @required this.item,
  }) : super(key: key);

  final item;

  @override
  Widget build(BuildContext context) {
    return Text('LOT' + item['id'].toString(),
        style: TextStyle(
            color: Pallet1.fontcolornew,
            fontSize: Pallet1.heading4,
            fontWeight: Pallet1.bold));
  }
}

// ignore: camel_case_types
class textCreatedat extends StatelessWidget {
  const textCreatedat({
    Key key,
    @required this.item,
  }) : super(key: key);

  final item;

  @override
  Widget build(BuildContext context) {
    return Text(
        item['created_at'].toString().split('T')[0] +
            " " +
            item['created_at']
                .toString()
                .split('T')[1]
                .toString()
                .split('.')[0]
                .toString(),
        style:
            TextStyle(color: Pallet1.fontcolornew, fontSize: Pallet1.heading4));
  }
}

// ignore: camel_case_types
class unprocessedTickets extends StatelessWidget {
  const unprocessedTickets({
    Key key,
    @required this.item,
  }) : super(key: key);

  final item;

  @override
  Widget build(BuildContext context) {
    return Container(
      // color: Colors.black,
      child: Wrap(
        children: [
          for (var number in item['numbers'])
            Padding(
              padding: EdgeInsets.all(Pallet1.defaultPadding),
              child: Container(
                width: 45,
                decoration: BoxDecoration(
                    color: ([0, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89]
                                .contains(number)) ==
                            true
                        ? Pallet1.dashsmallcontainerback
                        : Pallet1.fontcolor,
                    border: Border.all(
                        color: ([0, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89]
                                    .contains(number)) ==
                                true
                            ? Colors.transparent
                            : Colors.grey),
                    borderRadius: BorderRadius.circular(Pallet1.radius)),
                child: Center(
                  child: Padding(
                    padding: EdgeInsets.all(Pallet1.defaultPadding),
                    child: Text(
                      number.toString(),
                      style: TextStyle(
                          color: ([0, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89]
                                      .contains(number)) ==
                                  true
                              ? Pallet1.fontcolor
                              : Pallet1.dashsmallcontainerback,
                          fontSize: Pallet1.heading4),
                    ),
                  ),
                ),
              ),
            ),
        ],
      ),
    );
  }
}

// ignore: camel_case_types
class wrappp extends StatelessWidget {
  const wrappp({
    Key key,
    @required this.item,
  }) : super(key: key);

  final item;

  @override
  Widget build(BuildContext context) {
    return Container(
      // color: Colors.black,
      child: Wrap(
        children: [
          for (var number in item['numbers'])
            Padding(
              padding: EdgeInsets.all(Pallet1.defaultPadding),
              child: Container(
                width: 40,
                decoration: BoxDecoration(
                    color: ([0, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89]
                                .contains(number)) ==
                            true
                        ? Pallet1.dashsmallcontainerback
                        : Pallet1.fontcolor,
                    border: Border.all(
                        color: ([0, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89]
                                    .contains(number)) ==
                                true
                            ? Colors.transparent
                            : Colors.grey),
                    borderRadius: BorderRadius.circular(Pallet1.radius)),
                child: Center(
                  child: Padding(
                    padding: EdgeInsets.symmetric(vertical: 10),
                    child: Text(
                      number.toString(),
                      style: TextStyle(
                          color: ([0, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89]
                                      .contains(number)) ==
                                  true
                              ? Pallet1.fontcolor
                              : Pallet1.dashsmallcontainerback,
                          fontSize: Pallet1.heading4),
                    ),
                  ),
                ),
              ),
            ),
        ],
      ),
    );
  }
}
