import 'dart:async';
import 'dart:math';

import 'package:centurion/lottery/common.dart';
import 'package:centurion/utils/utils.dart';
// import 'package:clippy_flutter/star.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import '../home.dart';
import 'config.dart';
import '../services/communication/index.dart' show HttpRequest;

class RedeemLottery extends StatefulWidget {
  final String amt;
  final double wdgtWidth, wdgtHeight;

  const RedeemLottery({Key key, this.amt, this.wdgtWidth, this.wdgtHeight})
      : super(key: key);
  @override
  _RedeemLotteryState createState() => _RedeemLotteryState();
}

class _RedeemLotteryState extends State<RedeemLottery> {
  StreamController normalcont = StreamController.broadcast();
  StreamSink get normalsink => normalcont.sink;
  Stream get normalstream => normalcont.stream;

  StreamController specialcont = StreamController.broadcast();
  StreamSink get specialsink => normalcont.sink;
  Stream get specialstream => normalcont.stream;

  // StreamController totalcont = StreamController.broadcast();
  // StreamSink get totalsink => normalcont.sink;
  // Stream get totalstream => normalcont.stream;

  int amtval;
  bool isMobile;
  bool isTab = false;

  List selecttickets = [];
  List<int> fab;
  bool specialselected;
  List specialticketselected = [];
  bool special = false;
  List total = [];
  int data;
  // ignore: unused_field
  Future<dynamic> _temp;

  List serverval = [
    {'amt': '5', 'special': '1', 'normal': '8'},
    {'amt': '10', 'special': '2', 'normal': '13'}
  ];
// ------------------------------------apply this to server values----------------------

  int maxnarmalselect;
  int maxspecialselect;

// ------------------------------------------------------------------------------------
  void initState() {
    super.initState();
    findtkenlength();
    fab = fibonacci1(12);
    specialsink.add(specialticketselected);
    // print(fab);
    // print("mmmmmmmmmmmm");
    // print(widget.amt);
    amtval = int.parse(widget.amt);
    // print(amtval.runtimeType);
    // test();
  }

  // ignore: missing_return
  Future<bool> savelotterytoken(Map map, BuildContext context) async {
    waiting1.waitpopup(context);

    print(map);
    Map<String, dynamic> result =
        await HttpRequest.Post('lottery_token', Utils.constructPayload(map));
    print(map);
    if (Utils.isServerError(result)) {
      print(result['response']['error']);
      Navigator.pop(context);
      snack.snack(title: result['response']['error'], isgrey: true);
      // ScaffoldMessenger.of(context)
      //     .showSnackBar(SnackBar(content: Text("Something Went Wrong!")));
    } else {
      data = result['response']['data']['id'];
      print(result['response']['data']);
      Navigator.pop(context);

      return true;
    }
  }

  findtkenlength() {
    // print("lllllllllddddddddddddd");
    for (var item in serverval) {
      if (widget.amt == item['amt']) {
        maxnarmalselect = int.parse(item['normal']);
        maxspecialselect = int.parse(item['special']);
      }
    }
    // print("wwwwwwwwwwwwwwwwww");
  }

  List<int> fibonacci1(int n) {
    int last = 1;
    int last2 = 0;
    return List<int>.generate(n, (int i) {
      if (i < 2) return i;
      int curr = last + last2;
      last2 = last;
      last = curr;
      return curr;
    });
  }

  List testlist = [];
  List temptestlist = [];

  test() async {
    // var limit = 0;

    // temptestlist = [];

    selecttickets = [];
    specialticketselected = [];
    final _random = new Random();

    testlist = [];
    for (var i = 0; i < maxspecialselect; i++) {
      // temptestlist.add(testlist[_random.nextInt(testlist.length)]);
      specialticketselected.add(fab[_random.nextInt(fab.length)]);
    }
    var element = fab[_random.nextInt(fab.length)];

    for (var i = 0; i < 100; i++) {
      if (fab.contains(i)) {
      } else {
        testlist.add(i);
      }
    }

    for (var i = 0; i < maxnarmalselect; i++) {
      // temptestlist.add(testlist[_random.nextInt(testlist.length)]);
      selecttickets.add(testlist[_random.nextInt(testlist.length)]);
    }
    normalsink.add(selecttickets);
    specialsink.add(specialticketselected);
    temptestlist.add(element);
    total = selecttickets + specialticketselected;

    print(total);
    // setState(() {});
//  var map = {
//             'product_id': 1,
//             'amount': int.parse(widget.amt),
//             'numbers': finallist,
//           };
//           bool result = await savelotterytoken(map);
  }

  @override
  void dispose() {
    normalcont.close();
    specialcont.close();
    // totalcont.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
      builder: (context, constraints) {
        if (constraints.maxWidth <= 703) {
          isMobile = true;
          isTab = false;
          return Container(
            color: Color(0xFF1d1b40),
            padding: EdgeInsets.fromLTRB(Pallet1.defaultPadding,
                Pallet1.defaultPadding, Pallet1.defaultPadding, 0),
            child: SingleChildScrollView(
              child: Column(
                children: [
                  profile(
                    width: widget.wdgtWidth,
                  ),
                  SizedBox(
                    height: 15,
                  ),
                  slots(
                    width: widget.wdgtWidth,
                  ),
                  SizedBox(
                    height: 15,
                  ),
                  // tickets(
                  //   width: widget.wdgtWidth,
                  // ),
                  SizedBox(
                    height: 15,
                  ),
                  count(width: widget.wdgtWidth),
                  SizedBox(
                    height: 15,
                  ),
                  Column(
                    children: [
                      submit(
                        width: widget.wdgtWidth,
                      ),
                      // SizedBox(
                      //   height: 10,
                      // ),
                      // cancel(),
                    ],
                  ),
                  SizedBox(
                    height: 15,
                  ),
                ],
              ),
            ),
          );
        }
        // else if (constraints.maxWidth >= 320 && constraints.maxWidth < 443) {
        //   isMobile = true;
        //   isTab = false;
        //   return Padding(
        //     padding: EdgeInsets.fromLTRB(Pallet1.defaultPadding,
        //         Pallet1.defaultPadding, Pallet1.defaultPadding, 0),
        //     child: SingleChildScrollView(
        //       child: Column(
        //         children: [
        //           profile(
        //             width: widget.wdgtWidth,
        //           ),
        //           SizedBox(
        //             height: 15,
        //           ),
        //           slots(
        //             width: widget.wdgtWidth,
        //           ),
        //           SizedBox(
        //             height: 15,
        //           ),
        //           // tickets(
        //           //   width: widget.wdgtWidth,
        //           // ),
        //           SizedBox(
        //             height: 15,
        //           ),
        //           count(width: widget.wdgtWidth),
        //           SizedBox(
        //             height: 15,
        //           ),
        //           Column(
        //             children: [
        //               cancel(),
        //               SizedBox(
        //                 height: 10,
        //               ),
        //               submit(
        //                 width: widget.wdgtWidth,
        //               )
        //             ],
        //           ),
        //           SizedBox(
        //             height: 15,
        //           ),
        //         ],
        //       ),
        //     ),
        //   );
        // } else if (constraints.maxWidth >= 443 && constraints.maxWidth <= 703) {
        //   isMobile = true;
        //   isTab = false;
        //   return Padding(
        //     padding: EdgeInsets.fromLTRB(Pallet1.defaultPadding,
        //         Pallet1.defaultPadding, Pallet1.defaultPadding, 0),
        //     child: SingleChildScrollView(
        //       child: Column(
        //         children: [
        //           profile(
        //             width: widget.wdgtWidth,
        //           ),
        //           SizedBox(
        //             height: 15,
        //           ),
        //           slots(
        //             width: widget.wdgtWidth,
        //           ),
        //           SizedBox(
        //             height: 15,
        //           ),
        //           // tickets(
        //           //   width: widget.wdgtWidth,
        //           // ),
        //           SizedBox(
        //             height: 15,
        //           ),
        //           count(width: widget.wdgtWidth),
        //           SizedBox(
        //             height: 15,
        //           ),
        //           Row(
        //             children: [
        //               cancel(),
        //               SizedBox(
        //                 width: 20,
        //               ),
        //               submit(
        //                 width: widget.wdgtWidth,
        //               )
        //             ],
        //           ),
        //           SizedBox(
        //             height: 15,
        //           ),
        //         ],
        //       ),
        //     ),
        //   );
        // }
        else if (constraints.maxWidth > 703 && constraints.maxWidth <= 1024) {
          isMobile = false;
          isTab = true;

          return Container(
            color: Color(0xFF1d1b40),
            padding: EdgeInsets.fromLTRB(Pallet1.defaultPadding,
                Pallet1.defaultPadding, Pallet1.defaultPadding, 0),
            child: SingleChildScrollView(
              child: Column(
                children: [
                  profile(
                    width: widget.wdgtWidth,
                  ),
                  SizedBox(
                    height: 15,
                  ),
                  slots(
                    width: widget.wdgtWidth,
                  ),
                  SizedBox(
                    height: 15,
                  ),
                  // tickets(
                  //   width: widget.wdgtWidth,
                  // ),
                  SizedBox(
                    height: 15,
                  ),
                  count(
                    width: widget.wdgtWidth,
                  ),
                  SizedBox(
                    height: 15,
                  ),
                  Row(
                    children: [
                      // cancel(),
                      // SizedBox(
                      //   width: 20,
                      // ),
                      submit(
                        width: widget.wdgtWidth,
                      )
                    ],
                  ),
                  SizedBox(
                    height: 15,
                  ),
                ],
              ),
            ),
          );
        } else {
          isMobile = false;
          isTab = false;
          return Container(
            color: Color(0xFF1d1b40),
            padding: EdgeInsets.fromLTRB(50, 20, 50, 0),
            height: widget.wdgtHeight,
            child: SingleChildScrollView(
              child: Column(
                children: [
                  profile(
                    width: widget.wdgtWidth,
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  slots(
                    width: widget.wdgtWidth,
                  ),
                  SizedBox(
                    height: 15,
                  ),
                  // tickets(
                  //   width: widget.wdgtWidth,
                  // ),
                  // SizedBox(
                  //   height: 15,
                  // ),
                  Wrap(
                    children: [
                      count(
                        width: (constraints.maxWidth > 1024 &&
                                constraints.maxWidth <= 1500)
                            ? widget.wdgtWidth * .85
                            : widget.wdgtWidth * .60,
                      )
                    ],
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Container(
                    width: (constraints.maxWidth > 1024 &&
                            constraints.maxWidth <= 1500)
                        ? widget.wdgtWidth * .82
                        : widget.wdgtWidth * .57,
                    child: Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: [
                          submit(
                            width: widget.wdgtWidth,
                          ),
                        ]),
                  ),
                  SizedBox(
                    height: 15,
                  ),
                ],
              ),
            ),
          );
        }
      },
    );
  }

  Container profile({double width}) {
    return Container(
      width: width,
      child: Text(
        "Lottery",
        style: TextStyle(
            color: Pallet1.fontcolor,
            fontSize: Pallet1.heading1,
            fontWeight: Pallet1.bold),
      ),
    );
  }

  // Widget tickets({double width}) {
  //   return Container(
  //     width: width,
  //     child: Text(
  //       "Lottery Tickets:",
  //       style: TextStyle(
  //           color: Pallet1.fontcolornew,
  //           fontSize: Pallet1.heading2,
  //           fontWeight: Pallet1.bold),
  //     ),
  //   );
  // }

  Column legends() {
    return Column(
      children: [
        rowLegend(Pallet1.fontcolornew, "Special Numbers"),
        SizedBox(height: 5),
        rowLegend(Pallet1.fontcolor, "Basic Numbers")
      ],
    );
  }

  Row rowLegend(Color c1, String text) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        Container(
          padding: EdgeInsets.all(Pallet1.defaultPadding),
          decoration: BoxDecoration(
              color: c1,
              shape: BoxShape.circle,
              border: Border.all(color: Pallet1.fontcolornew)),
          child: Text(''),
        ),
        SizedBox(width: 10),
        Text(
          text,
          style: TextStyle(
            color: Pallet1.fontcolornew,
            fontWeight: Pallet1.font600,
            fontSize: Pallet1.normalfont,
          ),
        ),
      ],
    );
  }

  Container slots({double width}) {
    return Container(
      width: width,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          // isMobile == true ? legends() : Container(),
          columnSlot(),
          // isMobile == true
          //     ? (selecttickets.length == 0 && specialticketselected.length == 0)
          //         ? Container()
          //         : columnSlot()
          //     : columnSlot(),
        ],
      ),
    );
  }

  Column columnSlot() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            rowSlot(),
            // isMobile == true ? Container() : legends(),
          ],
        ),
        SizedBox(
          height: 10,
        ),
        isMobile == true ? columnNormalNumbers() : Container(),
      ],
    );
  }

  Row rowSlot() {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Container(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                "Special Numbers",
                style: TextStyle(
                  color: Pallet1.fontcolor,
                  fontWeight: Pallet1.font600,
                  fontSize: Pallet1.heading3,
                ),
              ),
              SizedBox(
                height: 10,
              ),
              StreamBuilder(
                  stream: specialstream,
                  builder: (context, snapshot) {
                    return Container(
                      width: 140,
                      padding:
                          EdgeInsets.symmetric(vertical: 15, horizontal: 15),
                      decoration: BoxDecoration(
                          color: Color(0xFF13125e),
                          borderRadius: BorderRadius.circular(10)),
                      child: Wrap(
                        spacing: 5,
                        runSpacing: 5,
                        children: [
                          for (var i = 0; i < maxspecialselect; i++)
                            Opacity(
                              opacity: specialticketselected.length - 1 >= i
                                  ? 1
                                  : 0.6,
                              child: myContainer(
                                  specialticketselected.length - 1 >= i
                                      ? specialticketselected[i]
                                      : '',
                                  specialticketselected.length - 1 >= i
                                      ? Color(0xFFf59f3a)
                                      : Color(0xFF3a3a7a),
                                  true),
                            ),

                          // for (var i = 0; i < specialticketselected.length; i++)
                          //   myContainer(i, specialticketselected[i]),
                        ],
                      ),
                    );
                  }),
            ],
          ),
        ),
        isMobile == true
            ? Container()
            : Padding(
                padding:
                    EdgeInsets.symmetric(horizontal: Pallet1.defaultPadding),
                child: Container(
                    width: 2,
                    height:
                        //  (selecttickets.length == 0 &&
                        //         specialticketselected.length == 0)
                        //     ? 25
                        //     :
                        95,
                    color: Pallet1.fontcolor),
              ),
        isMobile == true ? Container() : columnNormalNumbers(),
      ],
    );
  }

  Container columnNormalNumbers() {
    return Container(
      width: isMobile == true
          ? widget.wdgtWidth
          : isTab == true
              ? widget.wdgtWidth * .68
              : widget.wdgtWidth * .7,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            "Basic Numbers",
            style: TextStyle(
              color: Pallet1.fontcolor,
              fontWeight: Pallet1.font600,
              fontSize: Pallet1.heading3,
            ),
          ),
          SizedBox(
            height: 10,
          ),
          StreamBuilder(
              stream: normalstream,
              builder: (context, snapshot) {
                return Container(
                  padding: EdgeInsets.symmetric(vertical: 15, horizontal: 15),
                  decoration: BoxDecoration(
                      color: Color(0xFF13125e),
                      borderRadius: BorderRadius.circular(10)),
                  child: Wrap(
                    spacing: 5,
                    runSpacing: 5,
                    children: [
                      for (var i = 0; i < maxnarmalselect; i++)
                        Opacity(
                          opacity: selecttickets.length - 1 >= i ? 1 : 0.6,
                          child: myContainer(
                              selecttickets.length - 1 >= i
                                  ? selecttickets[i]
                                  : '',
                              selecttickets.length - 1 >= i
                                  ? Color(0xFFf59f3a)
                                  : Color(0xFF3a3a7a),
                              false),
                        ),
                      // for (var i = 0; i < selecttickets.length; i++)
                      //   myContainer(i, selecttickets[i]),
                    ],
                  ),
                );
              }),
        ],
      ),
    );
  }

  Container myContainer(text, Color color, bool special) {
    return special == true
        ? Container(
            width: 50,
            height: 45,
            padding: EdgeInsets.all(3),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(10),
              color: Color(0xFF3a3a7a),
            ),
            child: CustomPaint(
              size: Size(40, (40 * 1).toDouble()),
              painter: StarCustomPainter(),
              child: Center(
                child: Text(text.toString(),
                    style: TextStyle(
                        fontSize: Pallet1.heading5, color: Pallet1.fontcolor)),
              ),
            ),
          )
        : Container(
            width: 50,
            height: 45,
            decoration: BoxDecoration(
                color: color,
                // shape: BoxShape.rectangle,
                borderRadius: BorderRadius.circular(7),
                boxShadow: [
                  BoxShadow(
                    color: Colors.black12,
                  )
                ]),
            child: Center(
              child: Padding(
                padding: EdgeInsets.all(Pallet1.defaultPadding),
                child: Text(text.toString(),
                    style: TextStyle(
                        fontSize: Pallet1.heading5, color: Pallet1.fontcolor)),
              ),
            ),
          );
  }

  Row submit({double width}) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        // PopupButton1(
        //   text: "Random Select",
        //   onpress: () {
        //     test();
        //   },
        //   vpadding: 10,
        //   hpadding: 10,
        //   buttoncolor: Pallet1.fontcolornew,
        //   textcolor: Pallet1.fontcolor,
        //   fontweight: Pallet1.font600,
        // ),
        // SizedBox(width: 20),
        // PopupButton1(
        //   text: "Clear",
        //   onpress: () {
        //     total = [];
        //     selecttickets = [];
        //     specialticketselected = [];

        //     normalsink.add(selecttickets);
        //     specialsink.add(specialselected);
        //   },
        //   vpadding: 10,
        //   hpadding: 10,
        //   buttoncolor: Pallet1.fontcolornew,
        //   textcolor: Pallet1.fontcolor,
        //   fontweight: Pallet1.font600,
        // ),
        // SizedBox(width: 20),
        PopupButton1(
          text: "Cancel",
          onpress: () {
            Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (BuildContext context) => Home(
                          route: 'dashboard',
                        )));
          },
          vpadding: 10,
          hpadding: 10,
          bordercolor: Colors.white,
          buttoncolor: Color(0xFF1d1b40),
          //  Color(0xFF1d1b40),
          textcolor: Colors.white,
          borderradius: 10,
          fontweight: Pallet1.font600,
        ),
        SizedBox(width: 20),
        PopupButton1(
          text: "Submit",
          onpress: () async {
            if (specialticketselected.length < maxspecialselect) {
              snack.snack(
                  title:
                      'You haven\'t selected all the special lottery numbers',
                  isgrey: true);
            } else if (selecttickets.length < maxnarmalselect) {
              snack.snack(
                  title: 'You haven\'t selected all the normal lottery numbers',
                  isgrey: true);
            } else {
              var map = {
                'product_id': 1,
                'amount': int.parse(widget.amt),
                'numbers': total,
              };
              bool result = await savelotterytoken(map, context);
              // print("mmmmmmmmmmmmmmmmmmmmmmmmmmmnnnnnnnnnnnn");
              // print(result);
              // Navigator.pop(context);

              if (result == true) {
                mydialge();
              } else {}
            }
          },
          vpadding: 10,
          hpadding: 10,
          borderradius: 10,
          buttoncolor: Colors.white,
          // Color(0xFFa5a4b4),
          textcolor: Color(0xFF1d1b40),
          fontweight: Pallet1.font600,
        ),
      ],
    );
  }

  // Container cancel() {
  //   return Container(
  //       child: Row(
  //     mainAxisAlignment: MainAxisAlignment.center,
  //     children: [
  //       PopupButton1(
  //         text: "Clear",
  //         onpress: () {
  //           total = [];
  //           selecttickets = [];
  //           specialticketselected = [];

  //           normalsink.add(selecttickets);
  //           specialsink.add(specialselected);
  //         },
  //         vpadding: 10,
  //         hpadding: 10,
  //         buttoncolor: Pallet1.fontcolornew,
  //         textcolor: Pallet1.fontcolor,
  //         fontweight: Pallet1.font600,
  //       ),
  //       SizedBox(width: 20),
  //       PopupButton1(
  //         text: "Cancel",
  //         onpress: () {
  //           Navigator.push(
  //               context,
  //               MaterialPageRoute(
  //                   builder: (BuildContext context) => Home(
  //                         route: 'dashboard',
  //                       )));
  //         },
  //         vpadding: 10,
  //         hpadding: 10,
  //         buttoncolor: Pallet1.fontcolornew,
  //         textcolor: Pallet1.fontcolor,
  //         fontweight: Pallet1.font600,
  //       ),
  //     ],
  //   ));
  // }

  Container count({width}) {
    return Container(
      width: width,
      child: Wrap(
        runSpacing: 13,
        spacing: 13,
        verticalDirection: VerticalDirection.down,
        children: [
          for (var index = 0; index < 100; index++)
            GestureDetector(
              onTap: () {
                if (fab.contains(index) == true) {
                  if (specialticketselected.contains(index) == true) {
                    special = false;
                    specialticketselected.remove(index);
                  } else if (specialticketselected.length + 1 <=
                      maxspecialselect) {
                    specialticketselected.add(index);
                  } else if (specialticketselected.length + 1 >
                      maxspecialselect) {
                    snack.snack(
                        title:
                            'You already Selected $maxspecialselect Special Lottery',
                        isgrey: true);
                  }
                  specialsink.add(specialticketselected);
                } else {
                  if (selecttickets.contains(index) == true) {
                    // for (var i = 0; i < selecttickets.length; i++) {
                    //   if (index == selecttickets[i]) {
                    //     selecttickets.removeAt(i);
                    //   }
                    // }
                    selecttickets.remove(index);
                  } else {
                    if (selecttickets.length + 1 > maxnarmalselect) {
                      snack.snack(
                          title:
                              'You already Selected $maxnarmalselect Lottery',
                          isgrey: true);
                    } else if (selecttickets.length + 1 <= maxnarmalselect) {
                      if (fab.contains(index) == true) {
                      } else {
                        selecttickets.add(index);
                      }
                    }
                  }
                  normalsink.add(selecttickets);
                }
                // setState(() {});

                // -----------------------find the total selected tickets-----------------------------------
                total = selecttickets + specialticketselected;
                // ----------------------------------------------------------------------------------
                // print(specialticketselected);
                // print(specialticketselected.length);
                // print("ddddddddddd");
                // print(selecttickets);
                // print(selecttickets.length);
                // print("eeeeeeeeeeeeee");
                // print(selecttickets + specialticketselected);
              },
              child: MouseRegion(
                cursor: SystemMouseCursors.click,
                child: StreamBuilder(
                    stream: specialstream,
                    builder: (context, snapshot) {
                      return StreamBuilder(
                          stream: normalstream,
                          builder: (context, snapshot) {
                            bool specialticket =
                                specialticketselected.contains(index);
                            bool totalticket = total.contains(index);
                            bool fabslot = fab.contains(index);
                            return Container(
                              // width: 200,
                              width: 50,
                              height: 45,
                              // decoration: BoxDecoration(
                              //     color: Color(0xFF3a3a7a),
                              //     // shape: BoxShape.rectangle,
                              //     borderRadius: BorderRadius.circular(7),
                              //     boxShadow: [
                              //       BoxShadow(
                              //         color: Colors.black12,
                              //       )
                              //     ]),
                              // padding: EdgeInsets.all(5),
                              decoration: BoxDecoration(
                                  color: totalticket == true
                                      ? Color(0xFFf59f3a)
                                      : Color(0xFF3a3a7a),
                                  // shape: BoxShape.circle,
                                  borderRadius: BorderRadius.circular(10),
                                  // border: Border.all(
                                  //   color: Pallet1.fontcolornew,
                                  // ),
                                  boxShadow: [
                                    BoxShadow(
                                      color: Colors.black12,
                                    ),
                                  ]),
                              child: fabslot == true
                                  ? Container(
                                      width: 40,
                                      height: 35,
                                      padding: EdgeInsets.all(3),
                                      decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(10),
                                        color: Color(0xFF3a3a7a),
                                      ),
                                      child: CustomPaint(
                                        size: Size(40, (40 * 1).toDouble()),
                                        painter: StarCustomPainter(
                                            color: specialticket == true
                                                ? Colors.white
                                                : Color(0xFFf59f3a)),
                                        child: Center(
                                          child: Text(index.toString(),
                                              style: TextStyle(
                                                fontSize: Pallet1.heading5,
                                                color: specialticket == true
                                                    ? Colors.black
                                                    : Colors.white,
                                              )),
                                        ),
                                      ),
                                    )
                                  : Center(
                                      child: Padding(
                                        padding: EdgeInsets.all(
                                            Pallet1.defaultPadding),
                                        child: Text(
                                          index.toString(),
                                          style: TextStyle(
                                            fontSize: Pallet1.normalfont,
                                            color: Colors.white,
                                          ),
                                        ),
                                      ),
                                    ),
                            );
                          });
                    }),
              ),
            ),
        ],
      ),
    );
  }

  // myContainer(i, text) {
  //   return Container(
  //     width: 50,
  //     height: 50,
  //     padding: EdgeInsets.all(5),
  //     decoration: BoxDecoration(
  //         color: Pallet1.inner1,
  //         shape: BoxShape.circle,
  //         boxShadow: [
  //           BoxShadow(
  //             color: Colors.black12,
  //           )
  //         ]),
  //     child: Center(
  //       child: Padding(
  //         padding: EdgeInsets.all(Pallet1.defaultPadding),
  //         child: Text(text.toString(),
  //             style: TextStyle(
  //                 fontSize: Pallet1.normalfont, color: Pallet1.fontcolor)),
  //       ),
  //     ),
  //   );
  // }

  popupwaiter() async {
    await Future.delayed(Duration(milliseconds: 500));
    return 'start';
  }

  mydialge() {
    return showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return AlertDialog(
            scrollable: true,
            // elevation: 24.0,
            backgroundColor: Colors.transparent,
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(15.0))),
            title: Container(
              padding: EdgeInsets.all(30),
              decoration: BoxDecoration(
                  image: DecorationImage(
                      image: AssetImage('lottery_success1.png'),
                      fit: BoxFit.fill)),
              width: 450,
              child: FutureBuilder(
                  future: popupwaiter(),
                  builder: (context, snapshot) {
                    if (snapshot.hasData) {
                      return Column(
                        mainAxisSize: MainAxisSize.min,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text('New Lottery',
                              style: TextStyle(
                                  fontWeight: Pallet1.font500,
                                  fontSize: Pallet1.heading3,
                                  color: Pallet1.fontcolor)),
                          SizedBox(height: 10),
                          Text('Ticket No : LOT' + data.toString(),
                              style: TextStyle(
                                  fontSize: Pallet1.heading5,
                                  color: Pallet1.fontcolor)),
                          SizedBox(height: 10),
                          Text('Special Number',
                              style: TextStyle(
                                  fontWeight: Pallet1.font500,
                                  fontSize: Pallet1.heading4,
                                  color: Pallet1.fontcolor)),
                          SizedBox(height: 15),
                          Container(
                            width: double.infinity,
                            decoration: BoxDecoration(
                                color: Colors.transparent,
                                borderRadius: BorderRadius.circular(6)),
                            child: Wrap(
                              spacing: 5,
                              runSpacing: 5,
                              children: [
                                for (var i = 0;
                                    i < specialticketselected.length;
                                    i++)
                                  // myContainer(specialticketselected[i],
                                  //     Color(0xFFf59f3a), true),

                                  Container(
                                    width: 47,
                                    height: 42,
                                    padding: EdgeInsets.all(3),
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(10),
                                      color: Color(0xFF3a3a7a),
                                    ),
                                    child: CustomPaint(
                                      size: Size(40, (40 * 1).toDouble()),
                                      painter: StarCustomPainter(),
                                      child: Center(
                                        child: Text(
                                            specialticketselected[i].toString(),
                                            style: TextStyle(
                                                fontSize: Pallet1.heading5,
                                                color: Pallet1.fontcolor)),
                                      ),
                                    ),
                                  )
                              ],
                            ),
                          ),
                          SizedBox(height: 10),
                          Text('Basic Number',
                              style: TextStyle(
                                  fontWeight: Pallet1.font500,
                                  fontSize: Pallet1.heading4,
                                  color: Pallet1.fontcolor)),
                          SizedBox(height: 15),
                          Container(
                            width: double.infinity,
                            decoration: BoxDecoration(
                                color: Colors.transparent,
                                borderRadius: BorderRadius.circular(6)),
                            child: Wrap(
                              spacing: 5,
                              runSpacing: 5,
                              children: [
                                for (var i = 0; i < selecttickets.length; i++)
                                  // myContainer(
                                  //     selecttickets[i], Color(0xFFf59f3a), false),
                                  Container(
                                    width: 47,
                                    height: 42,
                                    decoration: BoxDecoration(
                                        color: Color(0xFFf59f3a),
                                        borderRadius:
                                            BorderRadius.circular(10)),
                                    child: Center(
                                      child: Text(selecttickets[i].toString(),
                                          style: TextStyle(
                                              fontWeight: Pallet1.font500,
                                              fontSize: Pallet1.heading4,
                                              color: Pallet1.fontcolor)),
                                    ),
                                  ),
                              ],
                            ),
                          ),
                          SizedBox(height: 15),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: [
                              PopupButton1(
                                text: 'Continue',
                                buttoncolor: Color(0xFFa5a4b4),
                                textcolor: Color(0xFF1d1b40),
                                borderradius: 10,
                                onpress: () async {
                                  Navigator.of(context).pop();
                                  lotterySuccess();
                                },
                              ),
                            ],
                          ),
                          // SizedBox(height: 10),
                        ],
                      );
                    } else {
                      return Container();
                    }
                  }),
            ),
            // actions: [
            //   PopupButton1(
            //     text: 'Continue',
            //     onpress: () async {
            //       Navigator.of(context).pop();
            //       lotterySuccess();
            //     },
            //   ),
            //   SizedBox(height: 10),
            // ],
          );
        });
  }

  lotterySuccess() {
    return showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return AlertDialog(
            scrollable: true,
            // elevation: 24.0,
            backgroundColor: Colors.transparent,
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(15.0))),
            title: Container(
              padding: EdgeInsets.all(30),
              decoration: BoxDecoration(
                  image: DecorationImage(
                      image: AssetImage('lottery_success2.png'),
                      fit: BoxFit.fill)),
              width: 400,
              child: FutureBuilder(
                  future: popupwaiter(),
                  builder: (context, snapshot) {
                    if (snapshot.hasData) {
                      return Column(
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          Row(
                            children: [
                              Image.asset("c-logo.png", width: 35),
                              SizedBox(width: 10),
                              Text('Lottery Ticket Confirmation',
                                  maxLines: 2,
                                  style: TextStyle(
                                      fontSize: Pallet1.heading4,
                                      color: Pallet1.fontcolor,
                                      fontWeight: Pallet1.font500)),
                            ],
                          ),
                          ClipOval(
                            child: Container(
                              padding: EdgeInsets.all(10),
                              color: Colors.white,
                              child: Icon(
                                Icons.done,
                                color: Color(0xFF0902ad),
                              ),
                            ),
                          ),
                          SizedBox(height: 10),
                          Text("Lottery Ticket is Bought Successfully with",
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                  height: 1.4,
                                  fontWeight: Pallet1.font500,
                                  fontSize: Pallet1.heading4,
                                  color: Pallet1.fontcolor)),
                          SizedBox(height: 15),
                          Text('Ticket No : LOT' + data.toString(),
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                  fontSize: Pallet1.heading5,
                                  fontWeight: Pallet1.subheading1wgt,
                                  color: Pallet1.fontcolor)),
                          SizedBox(height: 15),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: [
                              PopupButton1(
                                text: 'Continue',
                                buttoncolor: Color(0xFFa5a4b4),
                                textcolor: Color(0xFF1d1b40),
                                borderradius: 10,
                                onpress: () {
                                  Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (BuildContext context) =>
                                              Home(
                                                route: 'winner_Announcement',
                                              )));
                                },
                              ),
                            ],
                          ),
                        ],
                      );
                    } else {
                      return Container();
                    }
                  }),
            ),
            // actions: [
            //   SizedBox(height: 10),
            // ],
          );
        });

    // successdialog() {
    //   double height = MediaQuery.of(context).size.height;
    //   double width = MediaQuery.of(context).size.width;
    //   return showDialog(
    //       context: context,
    //       barrierDismissible: false,
    //       barrierColor: Color(0xFF0027a6).withOpacity(0.4),
    //       builder: (BuildContext context) {
    //         return AlertDialog(
    //           elevation: 0,
    //           scrollable: true,
    //           backgroundColor: Colors.transparent,
    //           title: Stack(
    //             children: [
    //               Container(
    //                 color: Colors.transparent,
    //                 height: width > 310 && width < 373
    //                     ? 830
    //                     : height > 970
    //                         ? height * 0.7
    //                         : height > 765 && height <= 970
    //                             ? height * 0.8
    //                             : width < 450 && width >= 373
    //                                 ? 680
    //                                 : 600,
    //                 width: width > 650
    //                     ? 350
    //                     : width > 473 && width <= 650
    //                         ? 350
    //                         : width > 423 && width <= 473
    //                             ? 300
    //                             : width > 373 && width <= 423
    //                                 ? 250
    //                                 : 200,
    //                 child: Image.asset(
    //                   "s1.png",
    //                   fit: BoxFit.fill,
    //                 ),
    //               ),
    //               Positioned(
    //                 right: 0,
    //                 top: 10,
    //                 child: Container(
    //                   decoration: BoxDecoration(
    //                     color: Colors.red,
    //                     shape: BoxShape.circle,
    //                   ),
    //                   child: IconButton(
    //                     icon: Icon(Icons.close),
    //                     onPressed: () {
    //                       // Navigator.pop(context);
    //                       Navigator.pushReplacement(
    //                           context,
    //                           MaterialPageRoute(
    //                               builder: (BuildContext context) => Home(
    //                                     route: 'winner_Announcement',
    //                                   )));
    //                     },
    //                   ),
    //                 ),
    //               ),
    //               Positioned(
    //                 top: height > 970
    //                     ? height * 0.17
    //                     : height > 765 && height <= 970
    //                         ? height * 0.20
    //                         : width < 450 && width >= 373
    //                             ? 165
    //                             : width > 310 && width < 373
    //                                 ? 200
    //                                 : 150,
    //                 child: Container(
    //                   padding: EdgeInsets.all(20),
    //                   width: width > 650
    //                       ? 350
    //                       : width > 473 && width <= 650
    //                           ? 350
    //                           : width > 423 && width <= 473
    //                               ? 300
    //                               : width > 373 && width <= 423
    //                                   ? 250
    //                                   : 200,
    //                   // color: Colors.amber,
    //                   child: Column(
    //                     crossAxisAlignment: CrossAxisAlignment.center,
    //                     children: [
    //                       Container(
    //                         // padding: EdgeInsets.all(20),
    //                         color: Colors.transparent,
    //                         height: width > 460 ? height * 0.16 : 100,
    //                         width: width > 650
    //                             ? 360
    //                             : width > 473 && width <= 650
    //                                 ? 310
    //                                 : width > 423 && width <= 473
    //                                     ? 260
    //                                     : width > 373 && width <= 423
    //                                         ? 210
    //                                         : 170,
    //                         child: Image.asset(
    //                           "s2.png",
    //                           fit: BoxFit.fill,
    //                         ),
    //                       ),
    //                       SizedBox(height: 15),
    //                       Wrap(
    //                         children: [
    //                           Text("Lottery Ticket is Bought Successfully with",
    //                               textAlign: TextAlign.center,
    //                               style: TextStyle(
    //                                   height: 1.4,
    //                                   fontWeight: Pallet1.font500,
    //                                   fontSize: Pallet1.heading4,
    //                                   color: Pallet1.fontcolornew)),
    //                         ],
    //                       ),
    //                       SizedBox(height: 15),
    //                       Wrap(
    //                         children: [
    //                           Text('Ticket No : LOT' + data.toString(),
    //                               textAlign: TextAlign.center,
    //                               style: TextStyle(
    //                                   fontSize: Pallet1.heading5,
    //                                   fontWeight: Pallet1.subheading1wgt,
    //                                   color: Pallet1.fontcolornew)),
    //                         ],
    //                       ),
    //                       SizedBox(height: 15),
    //                       Row(
    //                         crossAxisAlignment: CrossAxisAlignment.start,
    //                         children: [
    //                           Container(
    //                             child: Text('Special Number',
    //                                 style: TextStyle(
    //                                     fontWeight: Pallet1.font500,
    //                                     fontSize: Pallet1.heading4,
    //                                     color: Pallet1.fontcolornew)),
    //                           ),
    //                         ],
    //                       ),
    //                       SizedBox(height: 10),
    //                       Row(
    //                         crossAxisAlignment: CrossAxisAlignment.start,
    //                         children: [
    //                           Container(
    //                             // width: double.infinity,
    //                             // decoration: BoxDecoration(
    //                             //     color: Colors.transparent,
    //                             //     borderRadius: BorderRadius.circular(6)),
    //                             child: Wrap(
    //                               spacing: 5,
    //                               runSpacing: 5,
    //                               children: [
    //                                 for (var i = 0;
    //                                     i < specialticketselected.length;
    //                                     i++)
    //                                   Container(
    //                                     width: 38,
    //                                     height: 38,
    //                                     decoration: BoxDecoration(
    //                                       color: Pallet1.inner1,
    //                                       shape: BoxShape.circle,
    //                                     ),
    //                                     child: Center(
    //                                       child: Text(
    //                                           specialticketselected[i].toString(),
    //                                           style: TextStyle(
    //                                               fontWeight: Pallet1.font500,
    //                                               fontSize: Pallet1.heading5,
    //                                               color: Pallet1.fontcolor)),
    //                                     ),
    //                                   ),
    //                               ],
    //                             ),
    //                           ),
    //                         ],
    //                       ),
    //                       SizedBox(height: 15),
    //                       Row(
    //                         crossAxisAlignment: CrossAxisAlignment.start,
    //                         children: [
    //                           Text('Basic Number',
    //                               style: TextStyle(
    //                                   fontWeight: Pallet1.font500,
    //                                   fontSize: Pallet1.heading4,
    //                                   color: Pallet1.fontcolornew)),
    //                         ],
    //                       ),
    //                       SizedBox(height: 10),
    //                       Container(
    //                         // width: double.infinity,
    //                         // decoration: BoxDecoration(
    //                         //     color: Colors.transparent,
    //                         //     borderRadius: BorderRadius.circular(6)),
    //                         child: Wrap(
    //                           spacing: 5,
    //                           runSpacing: 5,
    //                           children: [
    //                             for (var i = 0; i < selecttickets.length; i++)
    //                               Container(
    //                                 width: 38,
    //                                 height: 38,
    //                                 decoration: BoxDecoration(
    //                                   // boxShadow: [Pallet1.shadowEffect],
    //                                   color: Pallet1.fontcolor,
    //                                   border: Border.all(
    //                                     color: Colors.black,
    //                                   ),
    //                                   shape: BoxShape.circle,
    //                                 ),
    //                                 child: Center(
    //                                   child: Text(selecttickets[i].toString(),
    //                                       style: TextStyle(
    //                                           fontWeight: Pallet1.font500,
    //                                           fontSize: Pallet1.heading5,
    //                                           color: Pallet1.fontcolornew)),
    //                                 ),
    //                               ),
    //                           ],
    //                         ),
    //                       ),
    //                     ],
    //                   ),
    //                 ),
    //               ),
    //             ],
    //           ),
    //         );
    //       });
    // }
  }
}
