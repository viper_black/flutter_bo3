// import 'dart:math';
import 'dart:async';

import 'package:centurion/lottery/common.dart';
import 'package:confetti/confetti.dart';

import 'package:centurion/config.dart';
import 'package:centurion/services/business/account.dart';
// import 'package:get/get.dart';

import '../home.dart';
import '../services/communication/index.dart' show HttpRequest;
import '../services/business/account.dart';
import 'package:centurion/utils/utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter_countdown_timer/current_remaining_time.dart';
import 'package:flutter_countdown_timer/flutter_countdown_timer.dart';
import 'config.dart';
// import 'package:flutter/animation.dart';
import 'package:intl/intl.dart';

// class CurrentRemaininggTime {
//   final int days;
//   final int hours;
//   final int min;
//   final int sec;
//   final Animation<double> milliseconds;

//   CurrentRemaininggTime.utc(
//       {this.days, this.hours, this.min, this.sec, this.milliseconds});

//   @override
//   String toString() {
//     return 'CurrentRemainingTime{days: $days, hours: $hours, min: $min, sec: $sec, milliseconds: ${milliseconds?.value}';
//   }
// }

class WinnerAnnouncement extends StatefulWidget {
  final double wdgtWidth, wdgtHeight;

  const WinnerAnnouncement({Key key, this.wdgtWidth, this.wdgtHeight})
      : super(key: key);
  @override
  _WinnerAnnouncementState createState() => _WinnerAnnouncementState();
}

class _WinnerAnnouncementState extends State<WinnerAnnouncement> {
  Future<bool> _temp;
  List<dynamic> data = [];
  List<dynamic> winners = [];

  List<dynamic> splNumber = [];

  Future<bool> lotteryWinner(Map map) async {
    // ignore: unused_local_variable
    final DateFormat formatter = DateFormat('yyyy-MM-dd');
    var now = new DateTime.now().toString();
    var dateParse = DateTime.parse(now);
    endTime3 =
        DateTime.utc(dateParse.year, dateParse.month, dateParse.day, 14, 30)
            .millisecondsSinceEpoch;
    Map<String, dynamic> result =
        await HttpRequest.Post('lotteryWinner', Utils.constructPayload(map));
    if (Utils.isServerError(result)) {
      print(result);
      return throw (await Utils.getMessage(result['response']['error']));
    } else {
      print('000000|_|++++++++++++++++');
      print(result['response']['data']);
      // setState(() {
      data = result['response']['data']['config'];
      winners = result['response']['data']['winners'];
      // });
      lottryDataSink.add(result['response']['data']['config']);
      // splNumber = result['response']['data']['spl_numbers'];
      print('MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM');
      // print(splNumber);
      print(result['response']['data']['last_lucky_numbers']);
      print('UUUUUUUUUUUUUUUUUUUUUUUUU');

      print(data);

      return true;
    }
  }

  Future<bool> testLottry(Map map) async {
    Map<String, dynamic> result =
        await HttpRequest.Post('Testlottery', Utils.constructPayload(map));
    if (Utils.isServerError(result)) {
      print(result);
      return throw (await Utils.getMessage(result['response']['error']));
    } else {
      lotteryWinner(map);
      return true;
    }
  }

  Future<bool> testLottry2(Map map) async {
    Map<String, dynamic> result =
        await HttpRequest.Post('Testlottery2', Utils.constructPayload(map));
    if (Utils.isServerError(result)) {
      print(result);
      return throw (await Utils.getMessage(result['response']['error']));
    } else {
      lotteryWinner(map);
      return true;
    }
  }

  StreamController<List> controller;
  Stream lottryDataStream;
  Sink lottryDataSink;

  ConfettiController controllerCenter;

  // int diffhr;
  // int diffmn;
  // int diff_sc;

  // void main() {
  //   final startTime = DateTime(2021, 04, 28, 14, 10);
  //   final currentTime = DateTime.now();

  //   final diff_dy = currentTime.difference(startTime).inDays;
  //   diffhr = startTime.difference(currentTime).inHours;
  //   diffmn = startTime.difference(currentTime).inMinutes;
  //   diff_sc = startTime.difference(currentTime).inSeconds;
  //   print('d______------datedatedatedate');

  //   print(diff_dy);
  //   print(diffhr);
  //   print("${diffmn}minnnnnndndndnd");
  //   print(diff_sc);
  // }

  Map<String, dynamic> map = {'product_id': 1};
  @override
  void initState() {
    controllerCenter =
        ConfettiController(duration: const Duration(seconds: 10));
    print('sssssssssssssssssssssssssssssssssssssssssss');
    print(dashboard.username);
    _temp = lotteryWinner(map);
    controller = StreamController<List>();
    lottryDataStream = controller.stream;
    lottryDataSink = controller.sink;
    super.initState();
  }

  @override
  // ignore: override_on_non_overriding_member
  // List<Map> winnerList = [
  //   {'name': 'winner', 'price': '100'},
  //   {'name': 'winfner', 'price': 'd100'},
  //   {'name': 'winnere', 'price': '100'},
  //   {'name': 'winnere', 'price': '100'},
  //   {'name': 'winnere', 'price': '100'},
  //   {'name': 'winnere', 'price': '100'},
  //   {'name': 'winnere', 'price': '100'},
  //   {'name': 'winnere', 'price': '100'},
  // ];

//
  // ignore: override_on_non_overriding_member
  int endTime3;
  // int endTime = DateTime.now().millisecondsSinceEpoch + 1000 * (300 * 30000);
  // int endTime2 = DateTime.now().second;
  Widget build(BuildContext context) {
    print('OOOOOOOOOOOOOOOOOOOOOOOOOO');
    // print(endTime2);
    // print(DateTime(endTime3));

    // print(endTime);

    return StreamBuilder(
        stream: lottryDataStream,
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            return LayoutBuilder(builder: (context, constraints) {
              if (constraints.maxWidth <= 425) {
                return Padding(
                  padding: EdgeInsets.all(Pallet1.defaultPadding),
                  child: Center(
                    child: SingleChildScrollView(
                      child: Column(
                        children: [
                          winnerAnnouncement(width: widget.wdgtWidth),
                          SizedBox(
                            height: 20,
                          ),
                          recentWinners(width: widget.wdgtWidth),
                          SizedBox(
                            height: 20,
                          ),
                          luckyNumbers(width: widget.wdgtWidth)
                        ],
                      ),
                    ),
                  ),
                );
              } else if (constraints.maxWidth >= 425 &&
                  constraints.maxWidth <= 968) {
                return Center(
                  child: SingleChildScrollView(
                    child: Column(
                      children: [
                        winnerAnnouncement(width: widget.wdgtWidth),
                        SizedBox(
                          height: 20,
                        ),
                        recentWinners(width: widget.wdgtWidth),
                        SizedBox(
                          height: 20,
                        ),
                        luckyNumbers(width: widget.wdgtWidth)
                      ],
                    ),
                  ),
                );
              } else {
                return Center(
                  child: Container(
                    width: widget.wdgtWidth * .7,
                    child: Column(
                      children: [
                        TextButton(
                            onPressed: () {
                              testLottry(map);
                            },
                            child: Text('Test')),
                        TextButton(
                            onPressed: () {
                              testLottry2(map);
                            },
                            child: Text('Test2')),
                        winnerAnnouncement(width: widget.wdgtWidth * .7),
                        SizedBox(
                          height: 20,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            recentWinners(width: widget.wdgtWidth * .33),
                            luckyNumbers(width: widget.wdgtWidth * .33)
                          ],
                        ),
                      ],
                    ),
                  ),
                );
              }
            });
          } else if (snapshot.hasError) {
            return SomethingWentWrongMessage();
          } else {
            return Loader();
          }
        });
  }

  Widget winnerAnnouncement({double width}) {
    return Container(
        padding: EdgeInsets.all(Pallet1.defaultPadding),
        decoration: BoxDecoration(
            color: Pallet1.dashcontainerback,
            borderRadius: BorderRadius.circular(10)),
        width: width,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                ConfettiWidget(
                  particleDrag: 0.05, // apply drag to the confetti
                  emissionFrequency: 0.05, // how often it should emit
                  // blastDirection: 0,
                  numberOfParticles: 20, // number of particles to emit
                  gravity: 0.01, // gravity - or fall speed

                  confettiController: controllerCenter,
                  blastDirectionality: BlastDirectionality
                      .explosive, // don't specify a direction, blast randomly
                  shouldLoop:
                      false, // start again as soon as the animation is finished
                  colors: const [
                    Colors.green,
                    Colors.blue,
                    Colors.pink,
                    Colors.orange,
                    Colors.purple
                  ], // manually specify the colors to be used
                  // createParticlePath: drawStar, // define a custom shape/path.
                ),
                ConfettiWidget(
                  particleDrag: 0.05, // apply drag to the confetti
                  emissionFrequency: 0.05, // how often it should emit
                  // blastDirection: pi,
                  numberOfParticles: 20, // number of particles to emit
                  gravity: 0.01, // gravity - or fall speed

                  confettiController: controllerCenter,
                  blastDirectionality: BlastDirectionality
                      .explosive, // don't specify a direction, blast randomly
                  shouldLoop:
                      false, // start again as soon as the animation is finished
                  colors: const [
                    Colors.green,
                    Colors.blue,
                    Colors.pink,
                    Colors.orange,
                    Colors.purple
                  ], // manually specify the colors to be used
                  // createParticlePath: drawStar, // define a custom shape/path.
                ),
              ],
            ),
            Image.asset('winnerAnnouncement.png'),
            SizedBox(
              height: 20,
            ),
            Text('Winner will be announced on ',
                style: TextStyle(
                    color: Pallet1.fontcolor,
                    fontSize: Pallet1.normalfont + 5,
                    fontWeight: Pallet1.bold)),
            SizedBox(
              height: 20,
            ),
            Center(
              child: Container(
                width: 250,
                child: CountdownTimer(
                  endTime: endTime3,
                  widgetBuilder: (_, CurrentRemainingTime time) {
                    if (time == null) {
                      lotteryWinner(map);
                      controllerCenter.play();

                      return Text('Time Out',
                          style: TextStyle(
                              color: Pallet1.fontcolor,
                              fontSize: Pallet1.normalfont + 5,
                              fontWeight: Pallet1.bold));
                    }
                    return Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        time.days == null
                            ? Container()
                            : Column(
                                children: [
                                  Container(
                                    decoration: BoxDecoration(
                                        color: Pallet1.fontcolor,
                                        borderRadius: BorderRadius.circular(2)),
                                    child: Padding(
                                      padding: EdgeInsets.all(
                                          Pallet1.defaultPadding / 2),
                                      child: Text('${time.days}',
                                          style: TextStyle(
                                              color: Pallet1.dashcontainerback,
                                              fontSize: Pallet1.normalfont + 5,
                                              fontWeight: Pallet1.bold)),
                                    ),
                                  ),
                                  Text('days',
                                      style: TextStyle(
                                          color: Pallet1.fontcolor,
                                          fontSize: Pallet1.normalfont,
                                          fontWeight: Pallet1.bold))
                                ],
                              ),
                        SizedBox(
                          width: 20,
                        ),
                        time.hours == null
                            ? Container()
                            : Column(
                                children: [
                                  Container(
                                    decoration: BoxDecoration(
                                        color: Pallet1.fontcolor,
                                        borderRadius: BorderRadius.circular(2)),
                                    child: Padding(
                                      padding: EdgeInsets.all(
                                          Pallet1.defaultPadding / 2),
                                      child: Text('${time.hours}',
                                          style: TextStyle(
                                              color: Pallet1.dashcontainerback,
                                              fontSize: Pallet1.normalfont + 5,
                                              fontWeight: Pallet1.bold)),
                                    ),
                                  ),
                                  Text('Hrs',
                                      style: TextStyle(
                                        color: Pallet1.fontcolor,
                                        fontSize: Pallet1.normalfont,
                                      ))
                                ],
                              ),
                        SizedBox(
                          width: 20,
                        ),
                        time.min == null
                            ? Container()
                            : Column(
                                children: [
                                  Container(
                                    decoration: BoxDecoration(
                                        color: Pallet1.fontcolor,
                                        borderRadius: BorderRadius.circular(2)),
                                    child: Padding(
                                      padding: EdgeInsets.all(
                                          Pallet1.defaultPadding / 2),
                                      child: Text('${time.min}',
                                          style: TextStyle(
                                            color: Pallet1.dashcontainerback,
                                            fontSize: Pallet1.normalfont + 5,
                                          )),
                                    ),
                                  ),
                                  Text('Mins',
                                      style: TextStyle(
                                        color: Pallet1.fontcolor,
                                        fontSize: Pallet1.normalfont,
                                      ))
                                ],
                              ),
                        SizedBox(
                          width: 20,
                        ),
                        time.sec == null
                            ? Container()
                            : Column(
                                children: [
                                  Container(
                                    decoration: BoxDecoration(
                                        color: Pallet1.fontcolor,
                                        borderRadius: BorderRadius.circular(2)),
                                    child: Padding(
                                      padding: EdgeInsets.all(
                                          Pallet1.defaultPadding / 2),
                                      child: Text('${time.sec}',
                                          style: TextStyle(
                                              color: Pallet1.dashcontainerback,
                                              fontSize: Pallet1.normalfont + 5,
                                              fontWeight: Pallet1.bold)),
                                    ),
                                  ),
                                  Text('Secs',
                                      style: TextStyle(
                                        color: Pallet1.fontcolor,
                                        fontSize: Pallet1.normalfont,
                                      ))
                                ],
                              ),
                      ],
                    );
                  },
                ),
              ),
            ),
          ],
        ));
  }

  Widget recentWinners({double width}) {
    return Container(
      height: 250,
      padding: EdgeInsets.all(Pallet1.defaultPadding),
      width: width,
      decoration: BoxDecoration(
          color: Pallet1.dashcontainerback,
          borderRadius: BorderRadius.circular(10)),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text('Recent winners',
              style: TextStyle(
                  color: Pallet1.fontcolor,
                  fontSize: Pallet1.normalfont + 5,
                  fontWeight: Pallet1.bold)),
          Container(
            height: 200,
            child: ListView.builder(
                itemCount: winners.length,
                itemBuilder: (context, index) {
                  return Padding(
                    padding: EdgeInsets.all(Pallet1.defaultPadding),
                    child: Container(
                      padding: EdgeInsets.all(Pallet1.defaultPadding),
                      decoration: BoxDecoration(
                          color: Pallet1.dashsmallcontainerback,
                          borderRadius: BorderRadius.circular(10)),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(winners[index]['screen_name'],
                              style: TextStyle(
                                color: Pallet1.fontcolor,
                                fontSize: Pallet1.normalfont,
                              )),
                          Text('\$ ' + winners[index]['amount'],
                              style: TextStyle(
                                color: Pallet1.fontcolor,
                                fontSize: Pallet1.normalfont,
                              )),
                        ],
                      ),
                    ),
                  );
                }),
          ),
        ],
      ),
    );
  }

  Widget luckyNumbers({double width}) {
    return Container(
        width: width,
        height: 250,
        padding: EdgeInsets.all(Pallet1.defaultPadding),
        decoration: BoxDecoration(
            color: Pallet1.dashcontainerback,
            borderRadius: BorderRadius.circular(10)),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text('Lucky Numbers',
                style: TextStyle(
                    color: Pallet1.fontcolor,
                    fontSize: Pallet1.normalfont + 5,
                    fontWeight: Pallet1.bold)),
            Container(
              height: 200,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  // Text(
                  //     'Lorem ipsum dolor sit amet conscutar adipising elit sed do eiusmod tempor incididunt ut labore et dalore',
                  //     style: TextStyle(
                  //       color: Pallet1.fontcolor,
                  //       fontSize: Pallet1.normalfont,
                  //     )),
                  Text('Last Lucky Numbers',
                      style: TextStyle(
                        color: Pallet1.fontcolor,
                        fontSize: Pallet1.normalfont,
                      )),
                  SingleChildScrollView(
                    scrollDirection: Axis.horizontal,
                    child: Row(
                      children: [
                        for (var item in data)
                          for (var newItem in item['last_lucky_numbers'])
                            Padding(
                              padding: EdgeInsets.all(Pallet.defaultPadding),
                              child: Container(
                                  decoration: BoxDecoration(
                                      borderRadius:
                                          BorderRadius.circular(Pallet.radius),
                                      color: Pallet.fontcolor,
                                      border: Border.all(
                                          color: Pallet.fontcolornew)),
                                  child: Padding(
                                    padding:
                                        EdgeInsets.all(Pallet.defaultPadding),
                                    child: Text(
                                      newItem.toString(),
                                      style:
                                          TextStyle(color: Pallet.fontcolornew),
                                    ),
                                  )),
                            )
                      ],
                    ),
                  ),
                  Text('Next Week jackpot percent',
                      style: TextStyle(
                        color: Pallet1.fontcolor,
                        fontSize: Pallet1.normalfont,
                      )),
                  Padding(
                    padding: EdgeInsets.all(10),
                    child: Text(
                        data[0]["next_jackpot_percentage"].toString() + '%',
                        style: TextStyle(
                            color: Colors.grey,
                            fontSize: 21,
                            fontWeight: FontWeight.w800)),
                  ),

                  Row(
                    children: [
                      CustomButton1(
                        vpadding: 10,
                        hpadding: 8,
                        text: 'View History',
                        buttoncolor: Pallet1.fontcolor,
                        textcolor: Pallet1.fontcolornew,
                        onpress: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) =>
                                      Home(route: 'lottery_History')));
                        },
                      ),
                    ],
                  ),

                  dashboard.username != null
                      ? Column(
                          children: [
                            Row(
                              children: [
                                Text('Jackpot Fund :',
                                    style: TextStyle(
                                      color: Pallet1.fontcolor,
                                      fontSize: Pallet1.normalfont,
                                    )),
                                SizedBox(width: 10),
                                for (var item in data)
                                  Text(item['jackpot_fund'],
                                      style: TextStyle(
                                        color: Pallet1.fontcolor,
                                        fontSize: Pallet1.normalfont,
                                      )),
                              ],
                            ),
                            Row(
                              children: [
                                Text('Weekly Fund :',
                                    style: TextStyle(
                                      color: Pallet1.fontcolor,
                                      fontSize: Pallet1.normalfont,
                                    )),
                                SizedBox(width: 10),
                                for (var item in data)
                                  Text(item['weekly_fund'],
                                      style: TextStyle(
                                        color: Pallet1.fontcolor,
                                        fontSize: Pallet1.normalfont,
                                      )),
                              ],
                            ),
                            Row(
                              children: [
                                Text('Company Fund :',
                                    style: TextStyle(
                                      color: Pallet1.fontcolor,
                                      fontSize: Pallet1.normalfont,
                                    )),
                                SizedBox(width: 10),
                                for (var item in data)
                                  Text(item['company_fund'],
                                      style: TextStyle(
                                        color: Pallet1.fontcolor,
                                        fontSize: Pallet1.normalfont,
                                      )),
                              ],
                            ),
                          ],
                        )
                      : Container()

                  // Center(child: Image.asset('Ticketimg.png')),
                  // Center(
                  //   child: Container(
                  //     decoration: BoxDecoration(
                  //         color: Pallet1.fontcolor,
                  //         borderRadius: BorderRadius.circular(5)),
                  //     child: TextButton(
                  //       onPressed: () {},
                  //       child: Text('Invite',
                  //           style: TextStyle(
                  //             color: Pallet1.dashcontainerback,
                  //             fontSize: Pallet1.normalfont,
                  //           )),
                  //     ),
                  //   ),
                  // )
                ],
              ),
            ),
          ],
        ));
  }
}
