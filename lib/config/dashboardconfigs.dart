Map dashconfig = const {
  'assets': {
    'duc_available': 'AAP',
    'dsv_available': 'Cash Account',
    'cash_ewallet': 'Shopping Account',
  },
  'my_stats': {
    'rank_level': 'AAP',
    'total_purchases': 'Cash Account',
    'next_rank': 'Shopping Account',
    'bonus': 'Shopping Account',
    'cct_awarded': 'Shopping Account',
  },
};
