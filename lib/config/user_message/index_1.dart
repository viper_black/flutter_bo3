Map messageConfig = const {
  'english': {
    'user_name_exists': 'Username Already Exists',
    'user_name_available': 'Username Available',
    'email_allready_found': 'Email Already Exists',
    'email_available': 'Email Available',
    'payload_validation_failed': 'Something Went Wrong',
    'wrong_combination': 'Invalid Username Or Password',
    'login_sucess': 'Login Success',
    'missing_token': 'Something Went Wrong',
    'Signup_Success': 'Signup Success',
    'email_exists': 'Email Already Exists',
    'user_not_found': 'Sponsor Not Found',
    'try_again_error_0': 'Something Went Wrong',
    'email_does_not_exsists': 'Email Not Available',
    'invalid_access': 'Invalid OTP',
    'auth_error': 'Authentication Failed',
  },
  'mandarin': {
    'login_sucess': '登录成功',
  }
};
