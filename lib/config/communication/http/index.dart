Map<String, dynamic> requestConfig = const {
  'shareHolders': {
    'path': '/bov3015/4205413',
    'method': 'POST',
    'requestBody': {
      'payload': {
        "account_id": "integer",
        "share_holders": [
          {
            "share_holder_id": {
              "type": "string",
              "minimum": 0,
              "maxLength": 50
            },
            "first_name": {"type": "string", "minimum": 0},
            "last_name": {"type": "string", "minimum": 0},
            "shares": {"type": "string", "minimum": 0},
            "Nationality": {"type": "string", "minimum": 0},
            "delete_flag": {"type": "boolean"},
          }
        ],
      }
    },
    'requestHeader': {},
  },

  'FAQ': {
    'path': '/bov4003/487324',
    'method': 'POST',
    'requestBody': {
      'payload': {
        "faq_heading_id": {"type": "string", "minimum": 0, "maximum": 999999},
        "faq_data_id": {"type": "string", "minimum": 0, "maximum": 999999},
        "language": {"type": "string", "minimum": 0, "maximum": 999999}
      }
    },
    'requestHeader': {'Authorization': 'String'}
  },

  'onSaveCorporate': {
    'path': '/bov3015/4205412',
    'method': 'POST',
    'requestBody': {
      'payload': {
        "profile_id": {"type": "string", "minLength": 1, "maxLength": 50},
        "account_id": {"type": "string", "minLength": 5, "maxLength": 50},
        "company_name": {"type": "string", "minLength": 6, "maxLength": 50},
        "tax_number": {"type": "string", "minLength": 6, "maxLength": 50},
        "user_name": {"type": "string", "minLength": 5, "maxLength": 50},
        // "last_name": {"type": "string", "minLength": 0, "maxLength": 50},
        "date_of_incorporation": {
          "type": "string",
          "minLength": 0,
          "maxLength": 50
        },
        "field_of_business": {
          "type": "string",
          "minLength": 0,
          "maxLength": 50
        },
        "referal_link": {"type": "string", "minLength": 0, "maxLength": 50},
        "address_1": {"type": "string", "minLength": 5, "maxLength": 50},
        "address_2": {"type": "string", "minLength": 0, "maxLength": 50},
        "mobile_number_": {"type": "number", "minLength": 10, "maxLength": 50},
        "country_": {"type": "string", "minLength": 0, "maxLength": 50},
        "state_": {"type": "string", "minLength": 0, "maxLength": 50},
        "city_": {"type": "string", "minLength": 0, "maxLength": 50},
        "postal_code": {"type": "number", "minLength": 0, "maxLength": 50},
        "email_id": {"type": "string", "minLength": 0, "maxLength": 50},
        "identity_type": {"type": "string", "minLength": 0, "maxLength": 50},
        "date_of_birth": {"type": "string", "minLength": 0, "maxLength": 50},
        "nationality_": {"type": "string", "minLength": 0, "maxLength": 50},
        "company_certificate": {
          "type": "string",
          "minLength": 0,
          "maxLength": 50
        },
        "tax_certificate": {"type": "string", "minLength": 0, "maxLength": 50},
        "is_company": {
          "type": "boolean",
        },
      }
    },
    'requestHeader': {},
  },

  'corporate': {
    'path': '/bov3015/4205414',
    'method': 'POST',
    'requestBody': {
      'payload': {
        "account_id": {"type": "string", "minimum": 0},
        "share_holer": {"type": "boolean"},
      }
    },
    'requestHeader': {},
  },

  'signup': {
    'path': '/bov3002/88314',
    'method': 'POST',
    'requestBody': {
      'payload': {
        'sponsor': 'int',
        'account_type': 'int',
        'username': 'String',
        'password': 'String',
        'email': 'String',
        'country': 'String',
        'product_id': 'int',
        'company_name': 'String',
        'user_type': 'int'
      }
    },
    'requestHeader': {},
  },
  'invoiceStatement': {
    'path': '/bov3014/78328',
    'method': 'POST',
    'requestBody': {
      'payload': {"is_dsvp": 'bool'},
    },
    'requestHeader': {'Authorization': 'String'}
  },

  'drawishappened': {
    'path': '/bov3014/78329',
    'method': 'POST',
    'requestBody': {
      'payload': {
        "product_id": "integer",
      },
    },
    'requestHeader': {'Authorization': 'String'}
  },

  'remuspayment': {
    'path': '/bov60001/100001',
    'method': 'POST',
    'requestBody': {
      'payload': {
        "id": "integer",
        "payment_id": "string",
        "hash": "string",
        "amount": "number",
      },
    },
    'requestHeader': {'Authorization': 'String'}
  },

  'remuspaymentconfirm': {
    'path': '/bov60001/100002',
    'method': 'POST',
    'requestBody': {
      'payload': {
        "id": "integer",
        "payment_id": "string",
        "hash": "string",
        "amount": "number",
        "status": "string",
        "m_trans_id": "integer",
        "a_trans_id": "integer",
      },
    },
    'requestHeader': {'Authorization': 'String'}
  },
  'checkPassword': {
    'path': '/bov3002/88318',
    'method': 'POST',
    'requestBody': {
      'payload': {
        "password": {"type": "string", "minLength": 8, "maxLength": 50}
      },
    },
    'requestHeader': {}
  },
  'lottery_token': {
    'path': '/bov3003/78321',
    'method': 'POST',
    'requestBody': {
      'payload': {
        'product_id': 'int',
        'amount': 'int',
        'numbers': 'List',
      }
    },
    'requestHeader': {
      'Authorization': 'String',
    }
  },
  'lotteryWinner': {
    'path': '/bov3003/78322',
    'method': 'POST',
    'requestBody': {
      'payload': {
        'product_id': 'int',
      }
    },
    'requestHeader': {
      'Authorization': 'String',
    }
  },
  'daily_draw': {
    'path': '/bov3003/78323',
    'method': 'POST',
    'requestBody': {
      'payload': {
        'product_id': 'int',
      }
    },
    'requestHeader': {
      'Authorization': 'String',
    }
  },

  'weekly_draw': {
    'path': '/bov3003/78324',
    'method': 'POST',
    'requestBody': {
      'payload': {
        'product_id': 'int',
      }
    },
    'requestHeader': {
      'Authorization': 'String',
    }
  },
  'gettreedetail': {
    'path': '/bov3012/28362',
    'method': 'POST',
    'requestBody': {
      'payload': {
        "system_product_id": 'int',
        "account_id": 'int',
      }
    },
    'requestHeader': {
      'Authorization': 'String',
    }
  },
  'change_placement': {
    'path': '/bov3002/80001',
    'method': 'POST',
    'requestBody': {
      'payload': {
        "system_product_id": "int",
        "account_id": "int",
        "placement_id": "int"
      }
    },
    'requestHeader': {
      'Authorization': 'String',
    }
  },
  'rms_get_network_data': {
    'path': '/bov3012/78312',
    'method': 'POST',
    'requestBody': {
      'payload': {'system_product_id': 'int'}
    },
    'requestHeader': {'Authorization': 'String'}
  },
  'rms_get_network_data_search': {
    'path': "/bov3012/78330",
    'method': 'POST',
    'requestBody': {
      'payload': {"user_name": "string"},
    },
    'requestHeader': {
      'Authorization': 'String',
    }
  },
  'rms_get_network_data_search_select': {
    'path': "/bov3012/78331",
    'method': 'POST',
    'requestBody': {
      'payload': {"user_name": "string"},
    },
    'requestHeader': {
      'Authorization': 'String',
    }
  },

  'profiledetails': {
    'path': '/bov3002/88317',
    'method': 'POST',
    'requestBody': {
      'payload': {
        "profile_id": 'int',
        "account_id": 'int',
        "screen_name": 'string',
        "profile_image": 'string',
        "phone_no": 'string',
        "first_name": 'string',
        "last_name": 'string',
        "date_of_birth": 'string',
        "gender": 'string',
        "address_line1": 'string',
        "address_line2": 'string',
        "nationality": 'string',
        "country": 'string',
        "region": 'string',
        "city": 'string',
        "postal_code": 'string',
        "account_number": "string",
        "beneficiary": "string",
        "bank_name": "string",
        "bank_identification_code": "string",
      }
    },
    'requestHeader': {},
  },

  'findusertype': {
    'path': '/bov3002/80002',
    'method': 'POST',
    'requestBody': {
      'payload': {
        "system_product_id": 'int',
        "user_name": 'string',
      },
    },
    'requestHeader': {}
  },

  'individualprofile': {
    'path': '/bov3003/78318',
    'method': 'POST',
    'requestBody': {
      "payload": {
        "system_product_id": 'int',
      }
    },
    'requestHeader': {'Authorization': 'String'}
  },
  'profileupload': {
    'path': '/bov3006/48310',
    'method': 'POST',
    'requestBody': {
      "payload": {
        "product_name": "string",
        "image_name": "string",
        "is_main": 'bool',
        "folder_name": "string",
        "file_base64": "string",
      }
    },
    'requestHeader': {'Authorization': 'String'}
  },

  'c_check_signup_otp': {
    'path': '/bov530002/530012',
    'method': 'POST',
    'requestBody': {
      "payload": {
        "product_id": 'int',
        "user_name": 'String',
        "otp": 'String',
        "email": 'String',
        "s_flag": 'bool',
      }
    },
    'requestHeader': {}
  },
  'networkpartner': {
    'path': '/bov3012/28352',
    'method': 'POST',
    'requestBody': {
      'payload': {
        'system_product_id': 'int',
        'account_type_id': 'int',
      },
    },
    'requestHeader': {'Authorization': 'String'}
  },
  'redeemduc': {
    'path': '/bov3011/800005',
    'method': 'POST',
    'requestBody': {
      'payload': {'product_id': 'int', 'amount': 'double'},
    },
    'requestHeader': {
      'Authorization': 'String',
    }
  },

  'redeemaaplus': {
    'path': '/bov3011/800006',
    'method': 'POST',
    'requestBody': {
      'payload': {'product_id': 'int', 'amount': 'double'},
    },
    'requestHeader': {
      'Authorization': 'String',
    }
  },
  'getRegions': {
    'path': '/bov3013/797314',
    'method': 'POST',
    'requestBody': {
      'payload': {
        "system_product_id": {
          "type": "integer",
          "minimum": 0,
          "maximum": 999999
        },
        "region_level": {"type": "integer", "minimum": 0},
        "parent_region": {"type": "string", "minLength": 1}
      }
    },
    'requestHeader': {'Authorization': 'String'}
  },
  'verifyEmail': {
    'path': '/bov3002/88316',
    'method': 'POST',
    'requestBody': {
      'payload': {
        'token': 'String',
      },
    },
    'requestHeader': {}
  },
  'resetaccount': {
    'path': '/bov3001/10003',
    'method': 'POST',
    'requestBody': {
      'payload': {
        "username": "String",
        "password": "String",
        'isupdate': "bool",
      },
    },
    'requestHeader': {}
  },

  'link_resetpassword': {
    'path': '/bov3001/10004',
    'method': 'POST',
    'requestBody': {
      'payload': {
        "account_id": "integer",
        "link_code": "string",
        "password": "string",
      },
    },
    'requestHeader': {}
  },

  'check_token_valid': {
    'path': '/bov3001/10005',
    'method': 'POST',
    'requestBody': {
      'payload': {
        "link_expired": "string",
      },
    },
    'requestHeader': {}
  },

  'login': {
    'path': '/bov3001/10001',
    'method': 'POST',
    'requestBody': {
      'payload': {
        "is_finger_print": "bool",
        "username": "String",
        "password": "String",
        "password_future": "String",
        "hardware_id": "String",
        "system_product_id": "int"
      }
    },
    'requestHeader': {},
  },
  'use_account': {
    'path': '/BOV3001/10002',
    'method': 'POST',
    'requestBody': {
      'payload': {
        "system_product_id": "int",
        "account_id": "int",
        "token": "String",
      }
    },
    'requestHeader': {},
  },
  'logout': {
    'path': '/bov3005/58310',
    'method': 'POST',
    'requestBody': {
      'payload': {
        'product_id': 'int',
      },
    },
    'requestHeader': {'Authorization': 'String'},
  },
  'dsvtodsvp': {
    'path': '/bov3012/78320',
    'method': 'POST',
    'requestBody': {
      'payload': {'system_product_id': 'int', 'amount': 'int'},
    },
    'requestHeader': {'Authorization': 'String'},
  },
  'checkSponsor': {
    'path': '/bov3002/88310',
    'method': '',
    'requestBody': {
      'payload': {
        'checkname': 'String',
      },
    },
    'requestHeader': {}
  },
  'checkUsername': {
    'path': '/bov3002/88311',
    'method': 'POST',
    'requestBody': {
      'payload': {
        'username': 'String',
      },
    },
    'requestHeader': {}
  },
  'email': {
    'path': '/bov3002/88312',
    'method': 'POST',
    'requestBody': {
      'payload': {
        'email': 'String',
      },
    },
    'requestHeader': {}
  },
  'country': {
    'path': '/bov3002/88313',
    'method': 'POST',
    'requestBody': {
      'payload': {
        'product_id': 'int',
        "option": 'int',
        "find": "String",
      },
    },
    'requestHeader': {}
  },
  'dashboard': {
    'path': '/bov430001/430011',
    'method': 'POST',
    'requestBody': {
      'payload': {
        'product_id': 'int',
      },
    },
    'requestHeader': {'Authorization': 'String'}
  },
  'eWallet': {
    'path': '/bov3003/78311',
    'method': 'POST',
    'requestBody': {
      'payload': {
        'product_id': 'int',
      },
    },
    'requestHeader': {'Authorization': 'String'}
  },
  'myNetwork': {
    'path': '/bov3003/78312',
    'method': 'POST',
    'requestBody': {
      'payload': {
        'product_id': 'int',
      },
    },
    'requestHeader': {'Authorization': 'String'}
  },
  'accountStatement': {
    'path': '/bov3012/430016',
    'method': 'POST',
    'requestBody': {
      'payload': {
        'product_id': 'int',
      },
    },
    'requestHeader': {'Authorization': 'String'}
  },
  'bonusStatement': {
    'path': '/bov430001/430015',
    'method': 'POST',
    'requestBody': {
      'payload': {
        'product_id': 'int',
        "trans_type": "String",
        "limit": 'int',
        "off_set": 'int'
      },
    },
    'requestHeader': {'Authorization': 'String'}
  },
  'productList': {
    'path': '/bov3004/68310',
    'method': 'POST',
    'requestBody': {
      'payload': {
        "product_id": 'int',
        "category_id": 'int',
        "seller_id": 'int',
        "purchase_id": 'int',
        "warehouse_id": 'int',
        "enable_filter_protocol": 'bool'
      }
    },
    'requestHeader': {'Authorization': 'String'}
  },
  'singleProduct': {
    'path': '/bov3004/68313',
    'method': 'POST',
    'requestBody': {
      'payload': {
        'product_id': 'int',
        'product_id_fs': 'int', // products -> product_id
      },
    },
    'requestHeader': {'Authorization': 'String'}
  },
  'addToCart': {
    'path': '/bov3004/68311',
    'method': 'POST',
    'requestBody': {
      'payload': {
        'product_id': 'int',
        'product_id_fs': 'int', // products -> product_id
        'qty': 'int',
      },
    },
    'requestHeader': {'Authorization': 'String'}
  },
  'myCart': {
    'path': '/bov3004/68314',
    'method': 'POST',
    'requestBody': {
      'payload': {
        'product_id': 'int',
      },
    },
    'requestHeader': {'Authorization': 'String'}
  },
  'buyProduct': {
    'path': '/bov3004/68312',
    'method': 'POST',
    'requestBody': {
      'payload': {
        'product_id': 'int',
        'coupon_code': [
          'String',
        ], //  ------------------->>>>>>>>>>>>>>>>>> need to check why couponcode is in Array format
        'overall_total': 'double',
        'address_id': 'int',
      },
    },
    'requestHeader': {'Authorization': 'String'}
  },
  'directBuyProduct': {
    'path': '/bov3004/68318',
    'method': 'POST',
    'requestBody': {
      'payload': {
        'product_id': 'int',
        'product_id_fs': 'int',
        'coupon_code': [
          'String',
        ],
        'overall_total': 'int',
        'address_id': 'int'
      },
    },
    'requestHeader': {'Authorization': 'String'}
  },
  'sendOtp': {
    'path': '/bov3001/98311',
    'method': 'POST',
    'requestBody': {
      'payload': {
        'email_id': 'String',
        'system_product_id': 'int',
      },
    },
    'requestHeader': {}
  },
  'resendEmail': {
    'path': '/bov3001/98322',
    'method': 'POST',
    'requestBody': {
      'payload': {
        "system_product_id": {
          "type": "integer",
          "minimum": 0,
          "maximum": 999999
        },
        "email_id": {"type": "string", "format": "email"},
        "user_name": {"type": "string"},
      },
    },
    'requestHeader': {}
  },
  'checkOtp': {
    'path': '/bov3001/98312',
    'method': 'POST',
    'requestBody': {
      'payload': {
        'email': 'String',
        'otp': 'String',
      },
    },
    'requestHeader': {}
  },
  'resetPassword': {
    'path': '/bov3003/78316',
    'method': 'POST',
    'requestBody': {
      'payload': {
        'old_password': 'String',
        'new_password': 'String',
        'product_id': 'int',
      },
    },
    'requestHeader': {'Authorization': 'String'}
  },
  'forgotPassword': {
    'path': '/bov3001/98313',
    'method': 'POST',
    'requestBody': {
      'payload': {
        'email': 'String',
        'otp': 'String',
        'new_password': 'String',
      },
    },
    'requestHeader': {}
  },
  'uploadProductFile': {
    'path': '/bov3006/48310',
    'method': 'POST',
    'requestBody': {
      'payload': {
        'product_name': 'String',
        'file_name': 'String',
        'image_name': 'String',
        'is_main': 'bool',
        'file_base64': 'String',
      },
    },
    'requestHeader': {}
  },
  'getImageFile': {
    'path': '/bov3006/48311',
    'method': 'POST',
    'requestBody': {
      'payload': {
        'file_src': 'String',
        'width': 'int',
        'height': 'int',
        'quality': 'int',
        'format': 'String',
      },
    },
    'requestHeader': {}
  },
  'getFile': {
    'path': '/bov3006/48312',
    'method': 'POST',
    'requestBody': {
      'payload': {'file_src': 'String'},
    },
    'requestHeader': {}
  },
  'addFundingInfo': {
    'path': '/bov430001/430017',
    'method': 'POST',
    'requestBody': {
      'payload': {
        'product_id': 'int',
      },
    },
    'requestHeader': {'Authorization': 'String'}
  },
  'c_fingerprint_setup': {
    'path': '/bov530002/530013',
    'method': 'POST',
    'requestBody': {
      'payload': {
        'username': 'String',
        'password': "String",
        'product_id': 'int',
        'hash_key': 'String'
      }
    },
    'requestHeader': {}
  },
  'c_rms_payout': {
    'path': '/bov430001/430013',
    'method': 'POST',
    'requestBody': {
      'payload': {
        'product_id': 'int',
        'amount': 'int',
        'asset_type': 'String',
        'address_line': 'String'
      }
    },
    'requestHeader': {}
  },
  'buyPackages': {
    'path': '/bov430001/430018',
    'method': 'POST',
    'requestBody': {
      'payload': {
        'is_dsv_p': 'bool',
        'total': 'int',
        "split_type": 'String',
        'product_id': 'int',
        'packages': 'List',
        'cash_account_used': 'int',
        'trading_account_used': 'int',
      },
    },
    'requestHeader': {'Authorization': 'String'}
  },

  'c_rms_get_news_data': {
    'path': '/bov430001/430020',
    'method': 'POST',
    'requestBody': {
      'payload': {
        "payload": {
          "product_id": 'int',
        }
      },
    },
    'requestHeader': {'Authorization': 'String'}
  },
  'c_ps_get_categories': {
    'path': '/bov430001/430023',
    'method': 'POST',
    'requestBody': {
      "payload": {
        "product_id": 'int',
      }
    },
    'requestHeader': {'Authorization': 'String'}
  },
  'c_my_setting': {
    'path': '/bov430001/430021',
    'method': 'POST',
    'requestBody': {
      'payload': {
        "payload": {
          "product_id": 'int',
        }
      },
    },
    'requestHeader': {'Authorization': 'String'}
  },
  'kyc_status_check': {
    'path': '/bov430001/430027',
    'method': 'POST',
    'requestBody': {
      'payload': {
        "system_product_id": {
          "type": "integer",
          "minimum": 0,
          "maximum": 999999
        },
      },
    },
    'requestHeader': {'Authorization': 'String'}
  },
  'fingerprintSetup': {
    'path': '/bov3002/88315',
    'method': 'POST',
    'requestBody': {
      'payload': {
        'username': 'String',
        'password': 'String',
        'product_id': 'int',
        'hash_key': 'String',
      },
    },
    'requestHeader': {}
  },
  'fingerprintLogin': {
    'path': '/bov3001/98314',
    'method': 'POST',
    'requestBody': {
      'payload': {
        'product_id': 'int',
        'auth': 'String',
        'hash_key': 'String',
      },
    },
    'requestHeader': {}
  },
  'addressInfo': {
    'path': '/bov3004/68315',
    'method': 'POST',
    'requestBody': {
      'payload': {
        'product_id': 'int',
      },
    },
    'requestHeader': {'Authorization': 'String'}
  },
  'my_cart': {
    'path': '/bov3004/68314',
    'method': 'POST',
    'requestBody': {
      'payload': {
        'product_id': 'int',
      },
    },
    'requestHeader': {'Authorization': 'String'}
  },
  'c_rms_get_cct_data': {
    'path': '/bov430001/430024',
    'method': 'POST',
    'requestBody': {
      'payload': {
        'product_id': 'int',
      },
    },
    'requestHeader': {'Authorization': 'String'}
  },
  'getdsv_p': {
    'path': '/bov3012/130001',
    'method': 'POST',
    'requestBody': {
      'payload': {'system_product_id': 'int'}
    },
    'requestHeader': {'Authorization': 'String'}
  },
  'addressProcess': {
    ///// ---------------->>>>>>>>>>>>>>  need to check whether the address id returned back
    'path': '/bov3004/68316',
    'method': 'POST',
    'requestBody': {
      'payload': {
        "product_id": {"type": "integer", "minimum": 0, "maximum": 999999},
        "address_id": {"type": "integer", "minimum": 0, "maximum": 999999},
        "address_name": {"type": "string", "minLength": 1, "maxLength": 50},
        "address_line1": {"type": "string", "minLength": 1, "maxLength": 50},
        "address_line2": {"type": "string"},
        "country": {"type": "string", "minLength": 1},
        "region": {"type": "string", "minLength": 1},
        "city": {"type": "string", "minLength": 1},
        "postal_code": {"type": "string", "minLength": 1},
        "address_phone": {"type": "integer", "minimum": 0},
        "delete": {"type": "boolean"}
      },
    },
    'requestHeader': {'Authorization': 'String'}
  },
  // 'updateAddress': {
  //   'path': '/bov3004/68316',
  //   'method': 'POST',
  //   'requestBody': {
  //     'payload': {
  //       "product_id": {"type": "integer", "minimum": 0, "maximum": 999999},
  //       "address_id": {"type": "integer", "minimum": 0, "maximum": 999999},
  //       "address_name": {"type": "string", "minLength": 1, "maxLength": 50},
  //       "address1": {"type": "string", "minLength": 1, "maxLength": 50},
  //       "address2": {"type": "string"},
  //       "country": {"type": "string", "minLength": 1},
  //       "region": {"type": "string", "minLength": 1},
  //       "city": {"type": "string", "minLength": 1},
  //       "postal_code": {"type": "string", "minLength": 1},
  //       "phone_no": {"type": "integer", "minimum": 0},
  //       "delete": {"type": "boolean"}
  //     },
  //   },
  //   'requestHeader': {'Authorization': 'String'}
  // },
  'c_ps_get_product_details': {
    'path': '/bov430001/430022',
    'method': 'POST',
    'requestBody': {
      "payload": {
        "system_product_id": 'int',
        "product_id": 'int',
        "variant_id": 'int',
        "enable_filter_protocol": 'bool'
      }
    },
    'requestHeader': {'Authorization': 'String'}
  },
  // 'deleteAddress': {
  //   'path': '/bov3004/68316',
  //   'method': 'POST',
  //   'requestBody': {
  //     'payload': {
  //       "product_id": {"type": "integer", "minimum": 0, "maximum": 999999},
  //       "address_id": {"type": "integer", "minimum": 0, "maximum": 999999},
  //       "address_name": {"type": "string", "minLength": 1, "maxLength": 50},
  //       "address1": {"type": "string", "minLength": 1, "maxLength": 50},
  //       "address2": {"type": "string"},
  //       "country": {"type": "string", "minLength": 1},
  //       "region": {"type": "string", "minLength": 1},
  //       "city": {"type": "string", "minLength": 1},
  //       "postal_code": {"type": "string", "minLength": 1},
  //       "phone_no": {"type": "integer", "minimum": 0},
  //       "delete": {"type": "boolean"}
  //     },
  //   },
  //   'requestHeader': {'Authorization': 'String'}
  // },
  'setDefaultAddress': {
    'path': '/bov3004/68317',
    'method': 'POST',
    'requestBody': {
      'payload': {
        'product_id': 'int',
        'address_id': 'String',
      },
    },
    'requestHeader': {'Authorization': 'String'}
  },
  'c_ps_get_product_list': {
    'path': '/bov430001/430014',
    'method': 'POST',
    'requestBody': {
      'payload': {
        "product_id": 'int',
        "store_id": 'int',
        "seller_id": 'int',
        "purchase_id": 'int',
        "warehouse_id": 'int',
        "enable_filter_protocol": 'bool'
      },
    },
    'requestHeader': {'Authorization': 'String'}
  },
  'getCategories': {
    'path': '/bov3004/68319',
    'method': 'POST',
    'requestBody': {
      'payload': {
        'product_id': 'int',
      },
    },
    'requestHeader': {'Authorization': 'String'}
  },
  'getPayoutDetails': {
    'path': '/bov430001/430012',
    'method': 'POST',
    'requestBody': {
      'payload': {
        'product_id': 'int',
      },
    },
    'requestHeader': {'Authorization': 'String'}
  },
  'documentsData': {
    'path': '/bov3003/78314',
    'method': 'POST',
    'requestBody': {
      'payload': {
        'product_id': 'int',
      },
    },
    'requestHeader': {'Authorization': 'String'}
  },
  'getNewsData': {
    'path': '/bov430001/4300202',
    'method': 'POST',
    'requestBody': {
      'payload': {
        'product_id': 'int',
      },
    },
    'requestHeader': {'Authorization': 'String'}
  },
  'disable2FA': {
    'path': '/bov3003/28312',
    'method': 'POST',
    'requestBody': {
      'payload': {
        'product_id': 'int',
      }
    },
    'requestHeader': {'Authorization': 'String'}
  },
  'region': {
    'path': '/bov5001/797314',
    'method': 'POST',
    'requestBody': {
      'payload': {'product_id': 'int', 'option': 'int', 'find': 'String'}
    },
    'requestHeader': {'Authorization': 'String'}
  },
  'setup2FA': {
    'path': '/bov3003/530014',
    'method': 'POST',
    'requestBody': {
      'payload': {
        'product_id': 'int',
        'username': 'String',
      }
    },
    'requestHeader': {'Authorization': 'String'}
  },
  'paymentMethods': {
    'path': '/bov3011/800002',
    'method': '',
    'requestBody': {
      'payload': {'product_id': 'int', 'paymentMethod': 'String'},
    },
    'requestHeader': {'Authorization': 'String'}
  },
  'createPayment': {
    'path': '/bov3011/800000',
    'method': '',
    'requestBody': {
      'payload': {
        'product_id': 2,
        'paymentMethod': 'aaplus',
        'order_id': 1,
        'bankCode': '',
        'type': 'payment',
        'paymentCurrencyCode': 'BTC',
        'txnNote': ''
      }
    },
    'requestHeader': {'Authorization': 'String'}
  },
  'lottery_History': {
    'path': '/bov3003/78325',
    'method': 'POST',
    'requestBody': {
      'payload': {
        'product_id': 'int',
      },
    },
    'requestHeader': {'Authorization': 'String'}
  },

  'lottery_all_history': {
    'path': '/bov3003/78326',
    'method': 'POST',
    'requestBody': {
      'payload': {
        'product_id': 'int',
      },
    },
    'requestHeader': {'Authorization': 'String'}
  },
  'monthlycommissions': {
    'path': '/bov3003/78327',
    'method': 'POST',
    'requestBody': {
      'payload': {
        'product_id': 'int',
        'time_period': 'String',
      },
    },
    'requestHeader': {'Authorization': 'String'}
  },
  'check2FA': {
    'path': '/bov3003/28311',
    'method': 'POST',
    'requestBody': {
      'payload': {
        'product_id': 'int',
        'username': 'String',
        'secret': 'String',
      },
    },
    'requestHeader': {'Authorization': 'String'}
  },
  // 'validate2FA': {
  //   'path': '/bov3008/28311',
  //   'method': 'POST',
  //   'requestBody': {
  //     'payload': {
  //       'product_id': 'int',
  //       'username': 'String',
  //       'secret': 'number',
  //     },
  //   },
  //   'requestHeader': {'Authorization': 'String'}
  // },

  'getScreeningToken': {
    'path': '/bov3009/18310',
    'method': 'POST',
    'requestBody': {
      'payload': {
        'system_product_id': 'int',
      },
    },
    'requestHeader': {'Authorization': 'String'}
  },
  'profile': {
    'path': '/bov3003/78318',
    'method': 'POST',
    'requestBody': {
      'payload': {
        'product_id': 'int',
      },
    },
    'requestHeader': {'Authorization': 'String'}
  },
  'getDocuments': {
    'path': '/bov3003/78314',
    'method': 'POST',
    'requestBody': {
      'payload': {
        'product_id': 'int',
      },
    },
    'requestHeader': {'Authorization': 'String'}
  },
};
Map<String, dynamic> responseConfig = {
  'responseBody': {
    'response': {
      'data':
          'InternalLinkedHashMap<String, dynamic>', //{'username': 'bool', 'password': 'bool', 'status': 'String'},
      'error': 'null'
    },
    'requestHeader': {'Authorization': 'String'}
  },
};

Map<String, dynamic> errorResponseConfig = {
  'errorBody': {
    'response':
        'InternalLinkedHashMap<String, dynamic>', //{'data': 'null', 'error': 'user name exists'}
  }
};
