Map<String, dynamic> appSettings = const {
  // 'isdevelopment': true,
  'IS_PRODUCTION': true,
  'IS_LOGGER_ENABLED': true,
  'SITE_ID': 1,
  'ROOT': 'dcfounders',
  //Network Tree
  // 'NETWORK_TREE_URL': 'https://iqnetwork.ddns.net:5030',
  // App Share Link
  'OPEN_PLAY':
      'https://play.google.com/store/apps/details?id=com.google.android.apps.authenticator2',
  'OPEN_APP': 'https://apps.apple.com/in/app/google-authenticator/id388497605',

//Use Testing Only
  'SERVER_URL': 'https://iqnetwork.iqgeneral.com:5210',
  'CLIENT_URL': 'https://iqnetwork.iqgeneral.com:5201',
  'NETWORK_TREE_URL': 'https://iqnetwork.ddns.net:5030',
  'KYC_URL': 'https://kycverification.centuriongm.com/verification',
};
