import 'dart:ui';

import 'package:carousel_slider/carousel_slider.dart';
import 'package:centurion/animations/Bouncing_button.dart';
import 'package:centurion/resetaccount.dart';
import 'package:centurion/services/business/user.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'config.dart';
import 'config/app_settings.dart';
import 'data.dart';
import 'signup.dart';
import 'home.dart';
//import 'package:web_browser_detect/web_browser_detect.dart';

//------------------------------------------------------------
//web supported import
//this import is won't support android if you build apk please comment this one.............................................
// ignore: avoid_web_libraries_in_flutter
import 'package:universal_html/html.dart' as html;
//---------------------------------------------------

import 'utils/utils.dart';
import 'package:flutter/services.dart';
import 'package:local_auth/local_auth.dart';
import 'common.dart';
import 'utils/utils.dart' show Utils;
import 'services/communication/index.dart' show HttpRequest;
import 'services/communication/crypto/index.dart';

// ignore: must_be_immutable

// ignore: unused_element
var _formKey5 = GlobalKey<FormState>();
final usernamenode = FocusNode();

final password = TextEditingController();
final password1 = TextEditingController();
final password2 = TextEditingController();
// ignore: non_constant_identifier_names
TextEditingController otp_valid = TextEditingController();
String passwordError;
String passwordError1;
String passwordError2;
String passwordError12;
bool twofastatus;
// ignore: non_constant_identifier_names
bool language_change;
FocusNode confirmnode = FocusNode();
var snackBar;

// ignore: must_be_immutable
class Login extends StatefulWidget {
  final String activatekey;
  final bool accountswitchstatus;
  final bool winnerpage;
  Login({Key key, this.activatekey, this.accountswitchstatus, this.winnerpage})
      : super(key: key);

  bool isLogin = true;

  TextEditingController pwdCon = TextEditingController();
  var test;
  bool secureText1 = true;
  // ignore: unused_field
  var _formKey = GlobalKey<FormState>();

  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<Login> {
  GlobalKey<FormState> _loginFormKey = GlobalKey<FormState>();

  Map<String, dynamic> pageDetails = {};
  _LoginState() {
    setPageDetails();
  }
  void setPageDetails() async {
    String data = await Utils.getPageDetails('login');
    setState(() {
      pageDetails = Utils.fromJSONString(data);
    });
    // snack.snack(title: otpRegistered);
    otpRegistered = SnackBar(
      content: Text(pageDetails["text1"]),
    );
  }

  LocalAuthentication localAuth = LocalAuthentication();
  TextEditingController otp = TextEditingController();
  var _formKey2 = GlobalKey<FormState>();
  var _formKey4 = GlobalKey<FormState>();
  TextEditingController emailverify = TextEditingController();
  String emailError;
  TextEditingController fotp = TextEditingController();
  String otperror;
  TextEditingController pwd = TextEditingController();
  String pwderror;
  TextEditingController cnfrmpwd = TextEditingController();
  String cnfrmpwderror;
  String popconfirm = '';
  Future<bool> hasBiometric;
  bool fingerPrintAuth;
  bool isSetUp = false;
  bool ismobile = false;
  final username = TextEditingController();
  String usernameError;

  final password = TextEditingController();
  String passwordError;
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  SnackBar otpRegistered;

  String deviceId = 'null';
  String deviceModel;
  String macAddress;
  String otpError;

  Future<String> loaclStorage;
  Future<String> fingerPrintSet;
  //final browser = Browser();
  reload() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    // var browsername = browser.browser + browser.version.toString();
    // final now = DateTime.now();
    // if (prefs.getString('browsername') == browsername) {
    //   print("ccccccccccaaaaaaaaaaaaaaaaaaaaaa");
    //   print(prefs.getString('sessiontime'));
    //   if (prefs.getString('sessiontime') == null) {
    //     prefs.setString(
    //         "sessiontime",
    //         DateTime(now.year, now.month, now.day, now.hour, now.minute)
    //             .toString());
    //   }
    // }
    prefs.setString("reload", "login");
    print("sssssssssssss");
  }

  void initState() {
    twofastatus = false;
    language_change = false;
    //print(widget.activatekey);
    loaclStorage = Utils.getSharedPrefereces('data');
    fingerPrintSet = Utils.getSharedPrefereces('fingerprint');
    reload();
    // fingerPrintSet.then((value) => print('dsafdsfadsfasdfdasf$value'));
    super.initState();
    // if (promotion == true) {
    //   Future.delayed(
    //     Duration.zero,
    //     () {
    //       promotion = false;
    //       WidgetsBinding.instance.addPostFrameCallback((_) async {
    //         // promotionpopup(context);

    //       });
    //     },
    //   );
    // }
    // test();
  }

  List cart = [];
  int total = 0;

  List packages;
  List tempList = [];

  FocusNode cashaccnode = FocusNode();
  FocusNode tradeaccNode = FocusNode();
  FocusNode continuenode = FocusNode();
  double cashBal;
  double tradeBal;

  TextEditingController cashAccount = TextEditingController();
  String cashError;
  TextEditingController tradAccount = TextEditingController();
  String tradError;
  CarouselController _carouselController = new CarouselController();
  int _current = 1;
  int selectedoption = 1;
  IconData icon = Icons.arrow_drop_down;
  Future<String> temp;
  double totalAmount;

  // ignore: non_constant_identifier_names
  AccountSlider(context, width) async {
    //print(width);
  }

  accountactivationsuccesspopup(
      BuildContext context, bool verify, String res) async {
    ////print("ssssssssssssssssssssssssssddddddddddddddddddddddddddddddddd");
    await showDialog(
        context: context,
        barrierDismissible: false,
        // barrierColor: Colors.red,
        builder: (context) => AlertDialog(
              backgroundColor: Pallet.inner1,
              title: CircleAvatar(
                //backgroundColor: Pallet.fontcolor,
                radius: 25,
                backgroundColor: verify == false ? Pallet.fontcolor : null,
                backgroundImage:
                    verify == true ? AssetImage("success.gif") : null,
                child: verify == false
                    ? Icon(
                        Icons.close,
                        color: Pallet.errortxt,
                        size: 20,
                      )
                    : null,
              ),
              content: Container(
                child: verify == true
                    ? Text(pageDetails["text41"],
                        textAlign: TextAlign.center,
                        style: TextStyle(color: Pallet.fontcolor))
                    : Text(
                        res == 'wrong_otp' || res == 'invalid token'
                            ? 'Wrong Activation Mail'
                            : res == 'jwt_expired' || res == 'opt_expired'
                                ? 'Link has Expired\nPlease Try Login Again to Regenerate Activation Code link'
                                : pageDetails["text42"],
                        textAlign: TextAlign.center,
                        style: TextStyle(color: Pallet.errortxt)),
              ),
              actions: [
                verify == true
                    ? PopupButton(
                        text: pageDetails['text46'],
                        onpress: () {
                          //------------------------------------------------------------
                          //web supported import
                          //this import is won't support android if you build apk please comment this one.............................................
                          html.window.location.href = appSettings["CLIENT_URL"];
                          //----------------------------------------------------------------------

                          //android support

                          //Navigator.of(context).pop();
                          // Navigator.of(context).pop();
                        },
                      )
                    : PopupButton(
                        text: pageDetails["text40"],
                        onpress: () {
                          //------------------------------------------------------------
                          //web supported import
                          //this import is won't support android if you build apk please comment this one.............................................
                          html.window.location.href = appSettings["CLIENT_URL"];
                          //----------------------------------------------------------------------

                          //android support
                          // Navigator.of(context).pop();
                          // Navigator.of(context).pop();
                        },
                      ),
                SizedBox(height: 10),
              ],
            ));
  }

  accountswitchpopup(BuildContext context) async {
    ////print("ssssssssssssssssssssssssssddddddddddddddddddddddddddddddddd");
    await showDialog(
        context: context,
        barrierDismissible: false,
        // barrierColor: Colors.red,
        builder: (context) => AlertDialog(
              backgroundColor: Pallet.inner1,
              title: CircleAvatar(
                radius: 25,
                backgroundImage: AssetImage("success.gif"),
              ),
              content: Text(pageDetails["text47"],
                  textAlign: TextAlign.center,
                  style: TextStyle(color: Pallet.fontcolor)),
              actions: [
                PopupButton(
                  text: pageDetails["text46"],
                  onpress: () {
                    //------------------------------------------------------------
                    //web supported import
                    //this import is won't support android if you build apk please comment this one.............................................

                    var link = html.window.location.pathname;
                    html.window.location.href = link;
                  },
                ),
                SizedBox(height: 10),
              ],
            ));
  }

  promotionpopup(context) {
    return showDialog(
      barrierDismissible: false,
      context: context,
      builder: (BuildContext context) {
        return SingleChildScrollView(
          child: Container(
            padding: MediaQuery.of(context).size.width > 900
                ? EdgeInsets.only(
                    left: 50,
                    top: 20,
                    bottom: 20,
                  )
                : EdgeInsets.all(10),
            // width: MediaQuery.of(context).size.width,
            // height: MediaQuery.of(context).size.height,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Container(
                  child: Stack(
                    children: [
                      Container(
                        height: MediaQuery.of(context).size.width > 900
                            ? MediaQuery.of(context).size.height +
                                MediaQuery.of(context).size.height * 0.3 -
                                40
                            : MediaQuery.of(context).size.height - 20,
                        child: MediaQuery.of(context).size.width > 630
                            ? Image.network(
                                appSettings['SERVER_URL'] +
                                    "/assets/promotion.png",
                                // fit: BoxFit.fill,
                              )
                            : Container(
                                width: MediaQuery.of(context).size.width - 20,
                                child: Image.network(
                                  appSettings['SERVER_URL'] +
                                      "/assets/promotion.png",
                                  fit: BoxFit.fill,
                                ),
                              ),
                      ),
                      Positioned(
                        top: 5,
                        right: 5,
                        child: Container(
                          decoration: BoxDecoration(
                              shape: BoxShape.circle, color: Pallet.fontcolor),
                          width: 30,
                          height: 30,
                          // ignore: deprecated_member_use
                          child: FlatButton(
                            onPressed: () {
                              Navigator.of(context).pop();
                            },
                            child:
                                Icon(Icons.close, color: Colors.red, size: 15),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        );
      },
    );
  }

  // void promotionpopups(BuildContext context) async {
  //   await showDialog(
  //       context: context,
  //       barrierDismissible: false,
  //       // barrierColor: Colors.red,
  //       builder: (context) => AlertDialog(
  //             backgroundColor: Colors.transparent,
  //             elevation: 0,

  //             // title: Text("Wifi"),

  //             // title: Row(
  //             //   mainAxisAlignment: MainAxisAlignment.end,
  //             //   children: [
  //             //     Positioned(
  //             //       child: CircleAvatar(
  //             //         backgroundColor: Pallet.lastmembercircleavatarback,
  //             //         radius: 13,
  //             //         child: Icon(
  //             //           Icons.supervised_user_circle,
  //             //           color: Pallet.lastpackback,
  //             //           size: 16.0,
  //             //         ),
  //             //       ),
  //             //     ),
  //             //   ],
  //             // ),
  //             content: Image.asset(
  //               "promotion.jpg",
  //               // width: 400,
  //               // height: 800,
  //             ),
  //             // onPressed: () {
  //             //   Navigator.of(context).pop();
  //             //   Navigator.of(context).pop();
  //             // },
  //             actions: [
  //               PopupButton(
  //                 onpress: () {
  //                   Navigator.of(context).pop();
  //                 },
  //                 text: pageDetails["text40"],
  //               ),
  //             ],
  //           ));
  // }

  // test() async {
  //   DeviceInfoPlugin deviceInfo = DeviceInfoPlugin();
  //   if (Platform.isAndroid) {
  //     ismobile = true;
  //     AndroidDeviceInfo androidInfo = await deviceInfo.androidInfo;
  //     //print('Running on ${androidInfo.androidId}');
  //     deviceId = androidInfo.androidId;
  //     deviceModel = androidInfo.model;
  //     macAddress = androidInfo.type;
  //     hasBiometric = _checkBiometric();
  //   }
  //   if (Platform.isIOS) {
  //     ismobile = true;
  //     IosDeviceInfo iosInfo = await deviceInfo.iosInfo;
  //     //print('Running on ${iosInfo.identifierForVendor}');
  //     deviceId = iosInfo.identifierForVendor;
  //     deviceModel = iosInfo.model;
  //     macAddress = iosInfo.systemName;
  //     hasBiometric = _checkBiometric();
  //   }
  // }

  // ignore: unused_element
  Future<bool> _checkBiometric() async {
    try {
      return await localAuth.canCheckBiometrics;
      // ignore: unused_catch_clause
    } on PlatformException catch (e) {
      //print(e);
      return false;
    }
  }

  Future<void> _authenticate() async {
    try {
      // ignore: deprecated_member_use
      fingerPrintAuth = await localAuth.authenticateWithBiometrics(
          localizedReason: "Scan your finger print to authenticate",
          useErrorDialogs: true,
          stickyAuth: false);
      if (!fingerPrintAuth) {
        setState(() {
          isSetUp = false;
        });
      }
      // ignore: unused_catch_clause
    } on PlatformException catch (e) {
      //print(e);
    }
  }

  // DateTime _date = DateTime.now();
  // TextEditingController _dateController = TextEditingController();
  // _handleDatePicker() async {
  //   final DateFormat _dateFormatter = DateFormat('MMM dd, yyyy');
  //   @override
  //   void initState() {
  //     super.initState();
  //     _dateController.text = _dateFormatter.format(_date);
  //   }

  //   final DateTime date = await showDatePicker(
  //     context: context,
  //     initialDate: _date,
  //     firstDate: DateTime(2000),
  //     lastDate: DateTime(2100),
  //   );
  //   if (date != null && date != _date) {
  //     setState(() {
  //       _date = date;
  //     });
  //     _dateController.text = _dateFormatter.format(date);
  //   }
  // }
  // List specialticketselected = [30, 50];
  // List selecttickets = [30, 1, 40, 30, 40, 50, 60, 70, 50, 10, 10, 10, 10];

  final usernamenode = FocusNode();
  final passnode = FocusNode();
  final loginnode = FocusNode();
  final login2node = FocusNode();
  bool showBanner = true;
  bool verify;

  verifyresult(BuildContext context) async {
    //print("ssssssssssssssssssssssssssssssss");
    var res = await user.verifyEmail(widget.activatekey);
    if (res == 'null') {
      verify = true;
    } else {
      verify = false;
    }
    Future.delayed(Duration.zero,
        () => accountactivationsuccesspopup(context, verify, res));
  }

  bool promotion = true;
  @override
  Scaffold build(BuildContext context) {
    // successdialog();

    if (widget.accountswitchstatus == true) {
      accountswitchpopup(context);
    }
    Size size = MediaQuery.of(context).size;
    if (widget.activatekey != null) {
      verifyresult(context);
    } else if (widget.activatekey == null) {
      if (showBanner == true) {
        // Future.delayed(Duration.zero, () => twofasetuppopup(context));
        setState(() {
          showBanner = false;
        });
      }
    } else {
      setState(() {
        showBanner = false;
      });
    }

    return Scaffold(
      backgroundColor: Colors.transparent,
      body: LayoutBuilder(builder: (context, constraints) {
        if (constraints.maxWidth < ScreenSize.ipad) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              // Maintainance(),
              Expanded(
                child: Container(
                  padding: EdgeInsets.only(top: 15),
                  width: size.width,
                  height: size.height,
                  color: Pallet.inner2,
                  child: SingleChildScrollView(
                    child: Column(
                      children: [
                        SizedBox(
                          height: 60,
                        ),
                        Align(
                          alignment: Alignment.center,
                          child: Image.asset(
                            'mobile_logo.png',
                            height: 100,
                          ),
                        ),
                        SizedBox(
                          height: 30,
                        ),
                        Container(
                            padding: EdgeInsets.all(15),
                            width: size.width,
                            height: size.height - 205,
                            decoration: BoxDecoration(
                                color: Pallet.inner1,
                                borderRadius: BorderRadius.only(
                                    topRight: Radius.circular(30),
                                    topLeft: Radius.circular(30))),
                            child: screen(wdgtWidth: 350)),
                      ],
                    ),
                  ),
                ),
              ),
            ],
          );
        } else {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              // Maintainance(),
              Expanded(
                child: Row(
                  children: [
                    Container(
                      padding: EdgeInsets.only(top: 15),
                      width: 420,
                      height: size.height,
                      color: Pallet.inner1,
                      child: Center(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            SizedBox(
                              height: 20,
                            ),
                            Image.asset(
                              'logo.png',
                              height: 100,
                            ),
                            SizedBox(
                              height: 30,
                            ),
                            Container(
                                // padding: EdgeInsets.all(15),
                                height: size.height - 180,
                                child: screen(wdgtWidth: 350)),
                          ],
                        ),
                      ),
                    ),
                    Center(
                      child: Container(
                        width: size.width - 420,
                        height: size.height - 205,
                        color: Pallet.inner2,
                        child: Image.asset(
                          "centurion-logo.png",
                        ),
                      ),
                    )
                  ],
                ),
              ),
            ],
          );
        }
      }),
    );
  }

  FutureBuilder screen({double wdgtWidth, wdgtHeight}) {
    return FutureBuilder(
        future: loaclStorage,
        builder: (context, snapshot) {
          if (snapshot.hasError) {
            return SomethingWentWrongMessage();
          } else if (snapshot.hasData) {
            var ram = Utils.fromJSONString(snapshot.data);
            return signin2(
                wdgtWidth: wdgtWidth,
                wdgtHeight: wdgtHeight,
                username: ram["user_name"]);
          } else {
            return signin1(wdgtWidth: wdgtWidth, wdgtHeight: wdgtHeight);
          }
        });
  }

  validateSignIn1() {
    _loginFormKey.currentState.validate();
  }

  submitSignIn1() {
    validateSignIn1();

    if (usernameError == null && passwordError == null) {
      language_change = true;
      perflogin();
    }
  }

  Container signin1({double wdgtWidth, wdgtHeight}) {
    return Container(
        width: wdgtWidth,
        // height: wdgtHeight,
        child: Form(
            key: _loginFormKey,
            child: ListView(
              // crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(height: 40.0),
                Text(
                  pageDetails["text2"],
                  style: TextStyle(
                    // fontSize: getpr,
                    fontSize: Pallet.heading3,
                    color: Pallet.fontcolor,
                    fontWeight: Pallet.font500,
                  ),
                ),
                SizedBox(height: 10.0),
                Text(
                  pageDetails["text3"],
                  style: TextStyle(
                    fontSize: Pallet.heading1,
                    color: Pallet.fontcolor,
                    fontWeight: Pallet.font500,
                  ),
                ),
                SizedBox(height: 40.0),
                Text(
                  pageDetails["text4"],
                  style: TextStyle(
                    fontSize: Pallet.heading3,
                    color: Pallet.fontcolor,
                    fontWeight: Pallet.font500,
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
                SignupTextBox(
                    // focusnode: usernamenode,
                    // tabpress: (event) {
                    //   if (event.isKeyPressed(LogicalKeyboardKey.tab)) {
                    //     FocusScope.of(context).requestFocus(passnode);
                    //   }
                    //   if (event.isKeyPressed(LogicalKeyboardKey.enter) ||
                    //       event.isKeyPressed(LogicalKeyboardKey.numpadEnter)) {
                    //     FocusScope.of(context).requestFocus(passnode);
                    //   }
                    // },
                    onFieldSubmitted: (value) => submitSignIn1(),
                    // onsubmit: () {
                    //   FocusScope.of(context).requestFocus(passnode);
                    // },
                    isPrefixIcon: true,
                    isSuffixIcon: false,
                    isPassword: false,
                    digitsOnlyPhone: false,
                    icon: Icons.mail,
                    label: pageDetails["text4"],
                    errorText: usernameError,
                    autofocus: true,
                    // onsubmit: () {
                    //   FocusScope.of(context).requestFocus(passnode);
                    // },
                    controller: username,
                    // onChanged: (value) => validateSignIn1(),
                    validation: (value) {
                      if (value.isEmpty) {
                        setState(() {
                          usernameError = pageDetails["text6"];
                        });
                      } else {
                        setState(() {
                          usernameError = null;
                        });
                      }
                    }),
                SizedBox(
                  height: 10,
                ),
                Text(
                  pageDetails["text7"],
                  style: TextStyle(
                    fontSize: Pallet.heading3,
                    color: Pallet.fontcolor,
                    fontWeight: Pallet.font500,
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
                SignupTextBox(
                    isPassword: true,
                    // tabpress: (event) {
                    // if (event.isKeyPressed(LogicalKeyboardKey.tab)) {
                    //   FocusScope.of(context).requestFocus(loginnode);
                    // }
                    // },
                    icon: Icons.lock,
                    isSuffixIcon: true,
                    // onChanged: (value) => validateSignIn1(),

                    // focusnode: passnode,
                    // onsubmit: () {
                    //   FocusScope.of(context).requestFocus(loginnode);
                    // },
                    onFieldSubmitted: (value) => submitSignIn1(),
                    isPrefixIcon: true,
                    label: pageDetails["text7"],
                    errorText: passwordError,
                    digitsOnlyPhone: false,
                    controller: password,
                    validation: (value) {
                      if (value.isEmpty) {
                        setState(() {
                          passwordError = pageDetails["text8"];
                        });
                        // return "Required";
                      } else {
                        setState(() {
                          passwordError = null;
                        });
                        // return null;
                      }
                    }),
                SizedBox(
                  height: 20,
                ),
                // RawKeyboardListener(
                //     focusNode: loginnode,
                //     onKey: (event) {
                //       if (event.isKeyPressed(LogicalKeyboardKey.enter) ||
                //           event.isKeyPressed(LogicalKeyboardKey.numpadEnter)) {
                //         widget._formKey.currentState.validate();
                //         if (usernameError == null && passwordError == null) {
                //           language_change = true;
                //           perflogin();
                //         }
                //       }
                //     },
                //     child:
                Bouncing(
                    onPress: () {
                      submitSignIn1();
                      // FocusScope.of(context).unfocus();

                      // widget._formKey.currentState.validate();

                      // if (usernameError == null && passwordError == null) {
                      //   language_change = true;
                      //   perflogin();
                      // }
                    },
                    // child: MouseRegion(
                    // cursor: SystemMouseCursors.click,
                    child: Container(
                      width: 400,

                      height: 40,

                      decoration: BoxDecoration(
                        color: Pallet.fontcolor,

                        //shape: RoundedRectangleBorder(

                        borderRadius: BorderRadius.circular(8.0),

                        //side: BorderSide(color: Colors.red),

                        //),
                      ),

                      // padding: const EdgeInsets.fromLTRB(0, 18, 0, 18),

                      child: Center(
                        child: Text(
                          pageDetails["text9"],
                          style: TextStyle(
                              color: Pallet.fontcolornew,
                              fontSize: Pallet.heading2,
                              fontWeight: FontWeight.w700),
                        ),
                      ),
                      // ),
                      // ),
                    )),
                SizedBox(
                  height: 20,
                ),
                Align(
                  alignment: Alignment.center,
                  child: InkWell(
                    child: Text(
                      pageDetails["text10"],
                      style: TextStyle(
                        fontSize: Pallet.heading4,
                        color: Pallet.fontcolor,
                        fontWeight: Pallet.font500,
                      ),
                    ),
                    onTap: () async {
                      // //print("1st");
                      await showDialog(
                          barrierDismissible: false,
                          context: context,
                          builder: (BuildContext context) {
                            return StatefulBuilder(
                              builder: (context, setStatee) {
                                return AlertDialog(
                                  elevation: 24.0,
                                  backgroundColor: Pallet.popupcontainerback,
                                  shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.all(
                                          Radius.circular(15.0))),
                                  title: Row(
                                    children: [
                                      Image.asset("c-logo.png",
                                          width: wdgtWidth * 0.1),
                                      SizedBox(width: 10),
                                      Text(pageDetails['text37'],
                                          style: TextStyle(
                                              fontSize: 14,
                                              color: Pallet.fontcolor,
                                              fontWeight: Pallet.font500)),
                                    ],
                                  ),
                                  content: Container(
                                    width: 450.0,
                                    child: SingleChildScrollView(
                                      child: ListBody(
                                        children: <Widget>[
                                          emanilverify(setStatee),
                                        ],
                                      ),
                                    ),
                                  ),
                                  actions: [
                                    PopupButton(
                                      text: pageDetails['text48'],
                                      onpress: () {
                                        checkEmail(emailverify.text, setStatee);
                                        // //print("Send OTPSSSSSSSSS");
                                        // Logger.info("Send OTP" + emailError);
                                        // //print("555" + emailError);
                                        // //print("555123" + popconfirm);
                                        // //print("123asdf");
                                        // Logger.info(emailError);
                                        if (emailError == null) {
                                          Navigator.of(context).pop();
                                          fsendOtp(email: emailverify.text);
                                          return showDialog(
                                              barrierDismissible: false,
                                              context: context,
                                              builder: (BuildContext context) {
                                                return StatefulBuilder(builder:
                                                    (context, setStatee) {
                                                  return AlertDialog(
                                                    elevation: 24.0,
                                                    backgroundColor: Pallet
                                                        .popupcontainerback,
                                                    shape: RoundedRectangleBorder(
                                                        borderRadius:
                                                            BorderRadius.all(
                                                                Radius.circular(
                                                                    15.0))),
                                                    title: Row(
                                                      children: [
                                                        Image.asset(
                                                            "c-logo.png",
                                                            width: wdgtWidth *
                                                                0.1),
                                                        SizedBox(width: 10),
                                                        Text(
                                                            pageDetails[
                                                                'text49'],
                                                            style: TextStyle(
                                                                fontSize: 14,
                                                                color: Pallet
                                                                    .fontcolor,
                                                                fontWeight: Pallet
                                                                    .font500)),
                                                      ],
                                                    ),
                                                    content: Container(
                                                      width: 450.0,
                                                      child:
                                                          SingleChildScrollView(
                                                        child: ListBody(
                                                          children: <Widget>[
                                                            saveforgotpassword(),
                                                          ],
                                                        ),
                                                      ),
                                                    ),
                                                    actions: [
                                                      PopupButton(
                                                        text: pageDetails[
                                                            "text27"],
                                                        onpress: () {
                                                          //print("object12");

                                                          try {
                                                            if (otp_valid
                                                                .text.isEmpty) {
                                                              setStatee(() {
                                                                passwordError12 =
                                                                    pageDetails[
                                                                        "text34"];
                                                              });
                                                            } else {
                                                              setStatee(() {
                                                                passwordError12 =
                                                                    null;
                                                              });
                                                            }
                                                            if (password1
                                                                .text.isEmpty) {
                                                              setStatee(() {
                                                                passwordError1 =
                                                                    pageDetails[
                                                                        "text34"];
                                                              });
                                                            } else if (passwordError1 !=
                                                                null) {
                                                              //print("object");
                                                              setStatee(() {
                                                                checkpassword(
                                                                    password1
                                                                        .text,
                                                                    setStatee);
                                                              });
                                                            }
                                                            if (password2
                                                                .text.isEmpty) {
                                                              //print("value1");
                                                              setStatee(() {
                                                                passwordError2 =
                                                                    pageDetails[
                                                                        "text34"];
                                                              });
                                                            }
                                                            if (password1
                                                                    .text !=
                                                                password2
                                                                    .text) {
                                                              //print("value2");
                                                              setStatee(() {
                                                                passwordError2 =
                                                                    pageDetails[
                                                                        "text50"];
                                                              });
                                                            } else {
                                                              setStatee(() {
                                                                passwordError2 =
                                                                    null;
                                                              });
                                                            }

                                                            //print("object123");

                                                            if (passwordError12 == null &&
                                                                passwordError1 ==
                                                                    null &&
                                                                passwordError2 ==
                                                                    null) {
                                                              setStatee(() {
                                                                //print(
                                                                // "Everything works fine");

                                                                forgetpassword_save(
                                                                    new_password:
                                                                        password1
                                                                            .text,
                                                                    email:
                                                                        emailverify
                                                                            .text,
                                                                    otp_val:
                                                                        otp_valid
                                                                            .text);
                                                                Navigator.push(
                                                                  context,
                                                                  MaterialPageRoute(
                                                                      builder:
                                                                          (context) =>
                                                                              Login()),
                                                                );
                                                              });
                                                            }
                                                          } catch (e) {
                                                            //print(e);
                                                          }
                                                        },
                                                      ),
                                                      PopupButton(
                                                        text: pageDetails[
                                                            'text51'],
                                                        onpress: () {
                                                          fsendOtp(
                                                              email: emailverify
                                                                  .text);
                                                          snack.snack(
                                                              title: pageDetails[
                                                                  "text52"]);

                                                          // snackBar = SnackBar(
                                                          //     content: Text(
                                                          //         pageDetails[
                                                          //             'text52']));
                                                          // ScaffoldMessenger.of(
                                                          //         context)
                                                          //     .showSnackBar(
                                                          //         snackBar);
                                                        },
                                                      ),
                                                      PopupButton(
                                                          text: pageDetails[
                                                              'text40'],
                                                          onpress: () {
                                                            Navigator.push(
                                                              context,
                                                              MaterialPageRoute(
                                                                  builder:
                                                                      (context) =>
                                                                          Login()),
                                                            );
                                                          }),
                                                    ],
                                                  );
                                                });
                                              });
                                        }
                                      },
                                    ),
                                    SizedBox(height: 10),
                                    PopupButton(
                                        text: pageDetails['text40'],
                                        onpress: () {
                                          Navigator.push(
                                              context,
                                              MaterialPageRoute(
                                                  builder: (context) =>
                                                      Login()));
                                        }),
                                  ],
                                );
                              },
                            );
                          });
                    },
                  ),
                ),
                SizedBox(
                  height: 20,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      pageDetails["text11"],
                      style: TextStyle(
                          color: Color(0xFFbfc7db),
                          fontSize: Pallet.heading6,
                          fontWeight: Pallet.font500),
                    ),
                    SizedBox(width: 10),
                    InkWell(
                      child: Text(
                        pageDetails["text12"],
                        style: TextStyle(
                          color: Pallet.fontcolor,
                          fontSize: Pallet.heading6,
                          decoration: TextDecoration.underline,
                          fontWeight: Pallet.font500,
                        ),
                      ),
                      onTap: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(builder: (context) => Signup()),
                        );
                        // showDialog(
                        //     // barrierDismissible: false,
                        //     context: context,
                        //     builder: (_) => AlertDialog(
                        //           backgroundColor: Pallet.inner1,
                        //           title: CircleAvatar(
                        //               backgroundColor: Pallet.fontcolor,
                        //               radius: 25,
                        //               child: Icon(
                        //                 Icons.done_outline_rounded,
                        //                 color: Pallet.inner1,
                        //                 size: 20,
                        //               )),
                        //           content: Text("Registration Completed",
                        //               textAlign: TextAlign.center,
                        //               style:
                        //                   TextStyle(color: Pallet.fontcolor)),
                        //           actions: [],
                        //         ));
                        // showDialog(
                        //     barrierDismissible: false,
                        //     context: context,
                        //     builder: (_) => AlertDialog(
                        //           backgroundColor: Pallet.inner1,
                        //           title: CircleAvatar(
                        //               backgroundColor: Pallet.fontcolor,
                        //               radius: 25,
                        //               child: Icon(
                        //                 Icons.close,
                        //                 color: Pallet.errortxt,
                        //                 size: 20,
                        //               )),
                        //           content: Text("Registration Cancelled",
                        //               textAlign: TextAlign.center,
                        //               style: TextStyle(color: Pallet.errortxt)),
                        //           actions: [],
                        //         ));
                      },
                    ),
                  ],
                ),
              ],
            )));
  }

  void checkEmail(value, setStatee) {
    //print("AAAAAAAAAAAAAAAAAAA" + value);
    // SampleClass().checkemail(value, "");
    if (value.trim().isEmpty || value == null) {
      setStatee(() {
        emailError = pageDetails['text34'];
        //print("1" + emailError);
      });
    } else if (!RegExp(
            r"^[a-zA-Z-0-9.a-zA-Z0-9.!#$%&'*+/=?^_`{|}~]+@[a-zA-Z-0-9]+\.[a-zA-Z]+")
        .hasMatch(value)) {
      setStatee(() {
        emailError = pageDetails['text39'];
        //print("2" + emailError);
      });
    } else {
      // //print(value);
      output(value) {
        // //print("kkkkkkkkkkkk" + value);
        // //print(value.toString() == "Email Already Exists".toString());
        if (value.toString() == "Email Already Exists".toString()) {
          // Logger.info("165435413546846");
          setStatee(() {
            emailError = null;
            // popconfirm = 'test';
            // //print("dfgdfg" + popconfirm);
          });
        } else {
          // Logger.error("jdfgbhfdhdebhjcb");
          setStatee(() {
            //print("sixth");
            emailError = pageDetails['text53'];
          });
        }
      }

      SampleClass().checkemail(value, output);
    }
  }

  void checkpassword(value, setStatee) {
    //print("12AAAAAAAAAA" + value);

    if (value.trim().isEmpty) {
      setStatee(() {
        //print("123");
        passwordError1 = pageDetails['text34'];
      });
    } else if (value.length < 8) {
      setStatee(() {
        //print("1234");
        passwordError1 = "Must Contain Atleast 8 Char";
      });
    } else if (!RegExp("^(?=.*[!@#\$%\^&\*])").hasMatch(value)) {
      setStatee(() {
        //print("12345");
        passwordError1 = pageDetails['text54'];
      });
    } else if (!RegExp(".*[0-9].*").hasMatch(value)) {
      setStatee(() {
        //print("123456");
        passwordError1 = pageDetails['text55'];
      });
    } else if (!RegExp(".*[A-Z].*").hasMatch(value)) {
      setStatee(() {
        //print("1234567");
        passwordError1 = pageDetails['text56'];
      });
    } else if (!RegExp(".*[a-z].*").hasMatch(value)) {
      setStatee(() {
        //print("12345678");
        passwordError1 = pageDetails['text57'];
      });
    } else {
      setStatee(() {
        //print("123456789");
        passwordError1 = null;
      });
    }

    // SampleClass().checkemail(value, "");
    if (value.trim().isEmpty || value == null) {
      setStatee(() {
        //print("Second");
        emailError = pageDetails['text34'];
        //print("1" + emailError);
      });
    } else if (!RegExp(
            r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+")
        .hasMatch(value)) {
      setStatee(() {
        //print("Third");

        emailError = pageDetails['text39'];
        //print("2" + emailError);
      });
    } else {
      // //print(value);
      //print("Fourth");
      output(value) {
        // //print("kkkkkkkkkkkk" + value);
        // //print(value.toString() == "Email Already Exists".toString());
        if (value.toString() == "Email Already Exists".toString()) {
          // Logger.info("165435413546846");
          setStatee(() {
            emailError = null;
            //print("fifth");
            // popconfirm = 'test';
            // //print("dfgdfg" + popconfirm);
          });
        } else {
          // Logger.error("jdfgbhfdhdebhjcb");
          setStatee(() {
            //print("sixth");
            emailError = pageDetails['text53'];
          });
        }
      }

      SampleClass().checkemail(value, output);
    }
  }

  Widget emanilverify(setStatee) {
    // ignore: unused_local_variable
    FocusNode emailnode = FocusNode();
    return Form(
      key: _formKey4,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SignupTextBox(
            // autofocus: true,
            // focusnode: emailnode,
            // tabpress: (event) {
            //   // if (event.isKeyPressed(LogicalKeyboardKey.enter)||event.isKeyPressed(LogicalKeyboardKey.numpadEnter)) {
            //   //   FocusScope.of(context).requestFocus(next2);
            //   // }
            // },
            isPrefixIcon: true,
            label: pageDetails['text58'],
            icon: Icons.mail,
            errorText: emailError,
            isSuffixIcon: true,
            digitsOnlyPhone: false,
            isPassword: false,
            controller: emailverify,
            validation: (value) {
              setStatee(() {
                // emailError = "teswt";

                checkEmail(value, setStatee);
                // emailError = "test";
              });
              // return checkEmail(value).toString();
            },
          ),
          SizedBox(height: 10.0),
          Text(
            pageDetails['text59'],
            style:
                TextStyle(color: Pallet.fontcolor, fontSize: Pallet.heading5),
          ),
          SizedBox(height: 10.0),
        ],
      ),
    );
  }

  Widget saveforgotpassword({setStatee}) {
    var _formKey = GlobalKey<FormState>();

    // void _adddsv() {
    //   final isValid = _formKey.currentState.validate();
    //   if (!isValid) {
    //     return;
    //   }
    //   _formKey.currentState.save();
    // }

    return Form(
      key: _formKey,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SignupTextBox(
            // focusnode: usernamenode,
            // tabpress: (event) {
            //   if (event.isKeyPressed(LogicalKeyboardKey.tab)) {
            //     // FocusScope.of(context).requestFocus(confirmnode);
            //   }
            // },
            label: pageDetails['text60'],
            isPrefixIcon: true,
            icon: Icons.contact_mail_outlined,
            controller: otp_valid,
            isSuffixIcon: true,
            digitsOnlyPhone: true,
            isPassword: false,
            errorText: passwordError12,
            validation: (value) {
              setState(() {
                // checkPassword(value);
              });
            },
          ),
          SizedBox(height: 20.0),
          SignupTextBox(
            // focusnode: usernamenode,
            // tabpress: (event) {
            //   if (event.isKeyPressed(LogicalKeyboardKey.tab)) {
            //     // FocusScope.of(context).requestFocus(confirmnode);
            //   }
            // },
            label: pageDetails['text32'],
            isSuffixIcon: true,
            isPrefixIcon: true,
            icon: Icons.lock,
            controller: password1,
            isPassword: true,
            digitsOnlyPhone: false,
            errorText: passwordError1,
            // validation: (value) {
            //   setStatee(() {
            //     checkpassword(value, setStatee);
            //   });
            // },
          ),
          SizedBox(height: 20.0),
          SignupTextBox(
            // focusnode: usernamenode,
            // tabpress: (event) {
            //   if (event.isKeyPressed(LogicalKeyboardKey.tab)) {
            //     // FocusScope.of(context).requestFocus(confirmnode);
            //   }
            // },
            label: pageDetails['text33'],
            icon: Icons.lock,
            controller: password2,
            isPrefixIcon: true,
            isSuffixIcon: true,
            isPassword: true,
            digitsOnlyPhone: false,
            errorText: passwordError2,
            validation: (value) {
              setState(() {
                // checkPassword(value);
              });
            },
          ),
        ],
      ),
    );
  }

  validateSignIn2() {
    _formKey.currentState.validate();
  }

  submitSignIn2() async {
    validateSignIn2();

    if (passwordError == null) {
      if (isSetUp) {
        setUpFingerprint();
      } else {
        perflogin();
      }
    }
    Utils.removeSharedPrefrences('data');
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.remove("shopbannershow");
  }

  Container signin2({double wdgtWidth, wdgtHeight, String username}) {
    return Container(
        width: wdgtWidth,
        // height: wdgtHeight,
        child: Form(
            key: _formKey,
            child: Column(children: [
              if (fingerPrintAuth == null)
                Align(
                  alignment: Alignment.topLeft,
                  child: Text(
                    pageDetails["text71"].toString() +
                        ' ' +
                        //username.toString(),
                        username.toString().capitalizeFirstofEach,
                    style: TextStyle(
                      fontSize: Pallet.heading1,
                      color: Pallet.fontcolor,
                    ),
                  ),
                )
              else if (fingerPrintAuth == true)
                Container(
                  child: Image.asset(
                    "fingerF.png",
                    width: wdgtWidth * .6,
                  ),
                )
              else
                Container(
                  child: Image.asset(
                    "fingerS.png",
                    width: wdgtWidth * .6,
                  ),
                ),
              SizedBox(
                height: 30,
              ),
              SignupTextBox(
                // focusnode: passnode,
                // tabpress: (event) {
                //   if (event.isKeyPressed(LogicalKeyboardKey.tab)) {
                //     FocusScope.of(context).requestFocus(login2node);
                //   }
                // },
                // onsubmit: () {
                //   FocusScope.of(context).requestFocus(login2node);
                // },
                autofocus: true,
                isPassword: true,
                isSuffixIcon: true,
                // autofocus: true,
                isPrefixIcon: true,
                icon: Icons.lock,
                onFieldSubmitted: (value) => submitSignIn2(),
                // onChanged: (value) => validateSignIn2(),
                digitsOnlyPhone: false,
                label: pageDetails["text66"],
                controller: password,
                errorText: passwordError,
                validation: (value) {
                  if (value.isEmpty) {
                    setState(() {
                      passwordError = pageDetails["text67"];
                    });
                    // return "Required";
                  } else {
                    setState(() {
                      passwordError = null;
                    });
                    // return null;
                  }
                },
              ),
              SizedBox(
                height: 30,
              ),
              Container(
                width: double.infinity,
                child: Bouncing(
                  onPress: () {
                    submitSignIn2();
                    // _formKey.currentState.validate();
                    // if (passwordError == null) {
                    //   if (isSetUp) {
                    //     setUpFingerprint();
                    //   } else {
                    //     perflogin();
                    //   }
                    // }
                    // Utils.removeSharedPrefrences('data');
                    // SharedPreferences prefs =
                    //     await SharedPreferences.getInstance();
                    // prefs.remove("shopbannershow");
                  },
                  // child: RawKeyboardListener(
                  //   focusNode: login2node,
                  //   onKey: (event) {
                  //     if (event.isKeyPressed(LogicalKeyboardKey.enter) ||
                  //         event.isKeyPressed(LogicalKeyboardKey.numpadEnter)) {
                  //       _formKey.currentState.validate();
                  //       if (passwordError == null) {
                  //         if (isSetUp) {
                  //           setUpFingerprint();
                  //         } else {
                  //           perflogin();
                  //         }
                  //       }
                  //     }
                  //   },
                  // child: MouseRegion(
                  // cursor: SystemMouseCursors.click,
                  child: Container(
                    width: 400,

                    height: 40,

                    decoration: BoxDecoration(
                      color: Pallet.fontcolor,

                      //shape: RoundedRectangleBorder(

                      borderRadius: BorderRadius.circular(8.0),
                      //side: BorderSide(color: Colors.red),

                      //),
                    ),

                    // padding: const EdgeInsets.fromLTRB(0, 18, 0, 18),

                    child: Center(
                      child: Text(
                        pageDetails["text68"],
                        style: TextStyle(
                            color: Pallet.fontcolornew,
                            fontSize: Pallet.heading2,
                            fontWeight: FontWeight.w700),
                      ),
                    ),
                  ),
                ),
              ),
              // ),
              // ),
              SizedBox(
                height: 20,
              ),
              InkWell(
                onTap: () async {
                  //print("1st");
                  await showDialog(
                      barrierDismissible: false,
                      context: context,
                      builder: (BuildContext context) {
                        return StatefulBuilder(
                          builder: (context, setStatee) {
                            return AlertDialog(
                              elevation: 24.0,
                              backgroundColor: Pallet.popupcontainerback,
                              shape: RoundedRectangleBorder(
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(15.0))),
                              title: Row(
                                children: [
                                  Image.asset("c-logo.png",
                                      width: wdgtWidth * 0.1),
                                  SizedBox(width: 10),
                                  Text(pageDetails['text23'],
                                      style: TextStyle(
                                          fontSize: 14,
                                          color: Pallet.fontcolor,
                                          fontWeight: Pallet.font500)),
                                ],
                              ),
                              content: Container(
                                width: 450.0,
                                child: SingleChildScrollView(
                                  child: ListBody(
                                    children: <Widget>[
                                      emanilverify(setStatee),
                                    ],
                                  ),
                                ),
                              ),
                              actions: [
                                PopupButton(
                                  text: pageDetails['text48'],
                                  onpress: () {
                                    //print("555" + emailError);
                                    //print("555123" + popconfirm);
                                    //print("123asdf");
                                    // Logger.info(emailError);
                                    if (popconfirm == 'test') {
                                      return showDialog(
                                          barrierDismissible: false,
                                          context: context,
                                          builder: (BuildContext context) {
                                            return StatefulBuilder(
                                                builder: (context, setStatee) {
                                              return AlertDialog(
                                                elevation: 24.0,
                                                backgroundColor:
                                                    Pallet.popupcontainerback,
                                                shape: RoundedRectangleBorder(
                                                    borderRadius:
                                                        BorderRadius.all(
                                                            Radius.circular(
                                                                15.0))),
                                                title: Row(
                                                  children: [
                                                    Image.asset("c-logo.png",
                                                        width: wdgtWidth * 0.1),
                                                    SizedBox(width: 10),
                                                    Text(pageDetails['text49'],
                                                        style: TextStyle(
                                                            fontSize: 14,
                                                            color: Pallet
                                                                .fontcolor,
                                                            fontWeight: Pallet
                                                                .font500)),
                                                  ],
                                                ),
                                                content: Container(
                                                  width: 450.0,
                                                  child: SingleChildScrollView(
                                                    child: ListBody(
                                                      children: <Widget>[
                                                        saveforgotpassword(),
                                                        Text("sdfnsjfj"),
                                                      ],
                                                    ),
                                                  ),
                                                ),
                                                actions: [
                                                  PopupButton(
                                                      text:
                                                          pageDetails['text61'],
                                                      onpress: () {
                                                        Navigator.push(
                                                          context,
                                                          MaterialPageRoute(
                                                              builder:
                                                                  (context) =>
                                                                      Login()),
                                                        );
                                                      }),
                                                  PopupButton(
                                                    text: pageDetails['text51'],
                                                    onpress: () {
                                                      Navigator.push(
                                                        context,
                                                        MaterialPageRoute(
                                                            builder:
                                                                (context) =>
                                                                    Login()),
                                                      );
                                                    },
                                                  ),
                                                  PopupButton(
                                                    text: pageDetails['text51'],
                                                    onpress: () {
                                                      Navigator.push(
                                                        context,
                                                        MaterialPageRoute(
                                                            builder:
                                                                (context) =>
                                                                    Login()),
                                                      );
                                                    },
                                                  ),
                                                  PopupButton(
                                                    text: pageDetails['text40'],
                                                    onpress: () {
                                                      Navigator.push(
                                                        context,
                                                        MaterialPageRoute(
                                                            builder:
                                                                (context) =>
                                                                    Login()),
                                                      );
                                                    },
                                                  ),
                                                ],
                                              );
                                            });
                                          });
                                    } else {
                                      //print("123" + emailError);
                                      //print("555321" + popconfirm);
                                    }
                                  },
                                ),
                                SizedBox(height: 10),
                                PopupButton(
                                  text: pageDetails['text40'],
                                  onpress: () {
                                    Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) => Login()),
                                    );
                                  },
                                )
                              ],
                            );
                          },
                        );
                      });
                },
                child: InkWell(
                  child: Text(
                    pageDetails["text69"],
                    style: TextStyle(
                      fontSize: Pallet.heading4,
                      color: Pallet.fontcolor,
                      fontWeight: Pallet.font500,
                    ),
                  ),
                  onTap: () async {
                    // //print("1st");
                    await showDialog(
                        barrierDismissible: false,
                        context: context,
                        builder: (BuildContext context) {
                          return StatefulBuilder(
                            builder: (context, setStatee) {
                              return AlertDialog(
                                elevation: 24.0,
                                backgroundColor: Pallet.popupcontainerback,
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.all(
                                        Radius.circular(15.0))),
                                title: Row(
                                  children: [
                                    Image.asset("c-logo.png",
                                        width: wdgtWidth * 0.1),
                                    SizedBox(width: 10),
                                    Text(pageDetails['text37'],
                                        style: TextStyle(
                                            fontSize: 14,
                                            color: Pallet.fontcolor,
                                            fontWeight: Pallet.font500)),
                                  ],
                                ),
                                content: Container(
                                  width: 450.0,
                                  child: SingleChildScrollView(
                                    child: ListBody(
                                      children: <Widget>[
                                        emanilverify(setStatee),
                                      ],
                                    ),
                                  ),
                                ),
                                actions: [
                                  PopupButton(
                                    text: pageDetails['text48'],
                                    onpress: () {
                                      checkEmail(emailverify.text, setStatee);
                                      // //print("Send OTPSSSSSSSSS");
                                      // Logger.info("Send OTP" + emailError);
                                      // //print("555" + emailError);
                                      // //print("555123" + popconfirm);
                                      // //print("123asdf");
                                      // Logger.info(emailError);
                                      if (emailError == null) {
                                        Navigator.of(context).pop();
                                        fsendOtp(email: emailverify.text);
                                        return showDialog(
                                            barrierDismissible: false,
                                            context: context,
                                            builder: (BuildContext context) {
                                              return StatefulBuilder(builder:
                                                  (context, setStatee) {
                                                return AlertDialog(
                                                  elevation: 24.0,
                                                  backgroundColor:
                                                      Pallet.popupcontainerback,
                                                  shape: RoundedRectangleBorder(
                                                      borderRadius:
                                                          BorderRadius.all(
                                                              Radius.circular(
                                                                  15.0))),
                                                  title: Row(
                                                    children: [
                                                      Image.asset("c-logo.png",
                                                          width:
                                                              wdgtWidth * 0.1),
                                                      SizedBox(width: 10),
                                                      Text(
                                                          pageDetails['text49'],
                                                          style: TextStyle(
                                                              fontSize: 14,
                                                              color: Pallet
                                                                  .fontcolor,
                                                              fontWeight: Pallet
                                                                  .font500)),
                                                    ],
                                                  ),
                                                  content: Container(
                                                    width: 450.0,
                                                    child:
                                                        SingleChildScrollView(
                                                      child: ListBody(
                                                        children: <Widget>[
                                                          saveforgotpassword(),
                                                        ],
                                                      ),
                                                    ),
                                                  ),
                                                  actions: [
                                                    PopupButton(
                                                      text:
                                                          pageDetails['text27'],
                                                      onpress: () {
                                                        //print("object12");

                                                        try {
                                                          if (otp_valid
                                                              .text.isEmpty) {
                                                            setStatee(() {
                                                              passwordError12 =
                                                                  pageDetails[
                                                                      "text34"];
                                                            });
                                                          } else {
                                                            setStatee(() {
                                                              passwordError12 =
                                                                  null;
                                                            });
                                                          }
                                                          if (password1
                                                              .text.isEmpty) {
                                                            setStatee(() {
                                                              passwordError1 =
                                                                  pageDetails[
                                                                      "text34"];
                                                            });
                                                          } else if (passwordError1 !=
                                                              null) {
                                                            //print("object");
                                                            setStatee(() {
                                                              checkpassword(
                                                                  password1
                                                                      .text,
                                                                  setStatee);
                                                            });
                                                          }
                                                          if (password2
                                                              .text.isEmpty) {
                                                            //print("value1");
                                                            setStatee(() {
                                                              passwordError2 =
                                                                  pageDetails[
                                                                      "text34"];
                                                            });
                                                          }
                                                          if (password1.text !=
                                                              password2.text) {
                                                            //print("value2");
                                                            setStatee(() {
                                                              passwordError2 =
                                                                  pageDetails[
                                                                      'text50'];
                                                            });
                                                          } else {
                                                            setStatee(() {
                                                              passwordError2 =
                                                                  null;
                                                            });
                                                          }

                                                          //print("object123");

                                                          if (passwordError12 ==
                                                                  null &&
                                                              passwordError1 ==
                                                                  null &&
                                                              passwordError2 ==
                                                                  null) {
                                                            setStatee(() {
                                                              //print(
                                                              // "Everything works fine");

                                                              forgetpassword_save(
                                                                  new_password:
                                                                      password1
                                                                          .text,
                                                                  email:
                                                                      emailverify
                                                                          .text,
                                                                  otp_val:
                                                                      otp_valid
                                                                          .text);
                                                              Navigator.push(
                                                                context,
                                                                MaterialPageRoute(
                                                                    builder:
                                                                        (context) =>
                                                                            Login()),
                                                              );
                                                            });
                                                          }
                                                        } catch (e) {
                                                          //print(e);
                                                        }
                                                      },
                                                    ),
                                                    PopupButton(
                                                      text:
                                                          pageDetails['text51'],
                                                      onpress: () {
                                                        fsendOtp(
                                                            email: emailverify
                                                                .text);
                                                        snack.snack(
                                                            title: pageDetails[
                                                                "text52"]);

                                                        // snackBar = SnackBar(
                                                        //     content: Text(
                                                        //         pageDetails[
                                                        //             'text52']));
                                                        // ScaffoldMessenger.of(
                                                        //         context)
                                                        //     .showSnackBar(
                                                        //         snackBar);
                                                      },
                                                    ),
                                                    PopupButton(
                                                        text: pageDetails[
                                                            'text40'],
                                                        onpress: () {
                                                          Navigator.push(
                                                            context,
                                                            MaterialPageRoute(
                                                                builder:
                                                                    (context) =>
                                                                        Login()),
                                                          );
                                                        }),
                                                  ],
                                                );
                                              });
                                            });
                                      }
                                    },
                                  ),
                                  SizedBox(height: 10),
                                  PopupButton(
                                      text: pageDetails['text40'],
                                      onpress: () {
                                        Navigator.push(
                                            context,
                                            MaterialPageRoute(
                                                builder: (context) => Login()));
                                      }),
                                ],
                              );
                            },
                          );
                        });
                  },
                ),
              ),
              SizedBox(
                height: 20,
              ),
              Container(
                child: TextButton(
                  onPressed: () {
                    Utils.removeSharedPrefrences('data');
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => Login()),
                    );
                  },
                  child: Text(
                    pageDetails["text70"],
                    style: TextStyle(
                      decoration: TextDecoration.underline,
                      fontSize: Pallet.heading4,
                      color: Pallet.fontcolor,
                      fontWeight: Pallet.font500,
                    ),
                  ),
                ),
              ),

              SizedBox(
                height: 30,
              ),
              FutureBuilder(
                  future: hasBiometric,
                  builder: (context, snapshot) {
                    if (snapshot.hasError) {
                      return SomethingWentWrongMessage();
                    } else if (snapshot.hasData) {
                      return FutureBuilder(
                          future: fingerPrintSet,
                          builder: (context, snapshot) {
                            if (!snapshot.hasData && snapshot.data == null) {
                              return GestureDetector(
                                child: setUpContainer(),
                                onTap: () {
                                  setUpFingerprint();
                                  setState(() {
                                    isSetUp = true;
                                  });
                                },
                              );
                            } else {
                              return GestureDetector(
                                onTap: () {
                                  perflogin();
                                },
                                child: Container(
                                  height: 130,
                                  width: wdgtWidth,
                                  child: Image.asset('fingerF.png'),
                                ),
                              );
                            }
                          });
                    } else {
                      return Container();
                    }
                  })
            ])));
  }

  Container setUpContainer({double wdgtWidth}) {
    return Container(
      height: 130,
      width: wdgtWidth,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(18),
        color: Pallet.inner1,
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Text(
            pageDetails["text15"],
            style: TextStyle(
              fontSize: 16,
              color: Pallet.fontcolor,
            ),
          ),
          SizedBox(
            height: 10,
          ),
          Text(
            pageDetails["text16"],
            style: TextStyle(
              fontSize: 10,
              color: Pallet.fontcolor,
            ),
          ),
          SizedBox(
            height: 10,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Container(
                width: 20,
                child: Image.asset(
                  "fingerprint.png",
                  color: Pallet.fontcolor,
                ),
              ),
              SizedBox(
                width: 5,
              ),
              Text(
                pageDetails["text17"],
                style: TextStyle(
                  fontSize: 15,
                  color: Pallet.fontcolor,
                ),
              ),
            ],
          ),
          SizedBox(
            height: 15,
          ),
          Text(
            pageDetails["text18"],
            style: TextStyle(
              fontSize: 15,
              color: Pallet.fontcolor,
            ),
          ),
        ],
      ),
    );
  }

  // ignore: non_constant_identifier_names
  void resend_email({String username, email}) async {
    Map<String, dynamic> map = {
      "system_product_id": 1,
      "user_name": username,
      "email_id": email,
    };
    Map<String, dynamic> result =
        await HttpRequest.Post('resendEmail', Utils.constructPayload(map));
    if (Utils.isServerError(result)) {
      print('server error');
      print(result['response']['error']);
    } else {
      // await Utils.setSharedPrefereces(
      //     Utils.toJSONString(result['response']['data']), 'data');
    }
  }

  /// forget password Api

  // ignore: non_constant_identifier_names
  void forgetpassword_save({String new_password, email, otp_val}) async {
    Map<String, dynamic> map = {
      "email": email,
      "otp": otp_val,
      "new_password": new_password,
    };
    Map<String, dynamic> result =
        await HttpRequest.Post('forgotPassword', Utils.constructPayload(map));
    if (Utils.isServerError(result)) {
      //print(result['response']['error']);
      //print("Password Change Fail");
      snack.snack(title: pageDetails["text62"]);
      // snackBar = SnackBar(content: Text(pageDetails['text62']));
      // ScaffoldMessenger.of(context).showSnackBar(snackBar);
    } else {
      snack.snack(title: pageDetails["text63"]);

      // snackBar = SnackBar(content: Text(pageDetails['text63']));
      // ScaffoldMessenger.of(context).showSnackBar(snackBar);
      await Utils.setSharedPrefereces(
          Utils.toJSONString(result['response']['data']), 'data');
    }
  }

  void fsendOtp({String email}) async {
    Map<String, dynamic> map = {
      "email_id": email,
      "system_product_id": 1,
    };
    Map<String, dynamic> result =
        await HttpRequest.Post('sendOtp', Utils.constructPayload(map));
    if (Utils.isServerError(result)) {
      //print(result['response']['error']);
    } else {
      //print('fajsdfkjads;lkfja;sdklfjadlskfj');
    }
  }

  // void checkOtp({String username, String email, String otp, setData}) async {
  //   Map<String, dynamic> map = {
  //     "product_id": 1,
  //     "username": username,
  //     "otp": otp,
  //     "email": email,
  //     "s_flag": false
  //   };
  //   Map<String, dynamic> result = await HttpRequest.Post(
  //       'c_check_signup_otp', Utils.constructPayload(map));
  //   if (Utils.isServerError(result)) {
  //     //print('yay');
  //     otpError = pageDetails["text19"];
  //   } else {
  //     otpError = null;
  //     Navigator.of(context).pop();
  //     ScaffoldMessenger.of(context).showSnackBar(otpRegistered);
  //     perflogin();
  //   }
  //   _formKey2.currentState.validate();
  // }

  void check2Fa(String otp) async {
    Map<String, dynamic> _map = {
      "system_product_id": 1,
      "username": username.text,
      "password": password.text,
      "password_future": otp,
      "hardware_id": deviceId,
      "is_finger_print": false,
    };
    reqLogin(_map);
    _formKey2.currentState.validate();
  }

  // clear() async {
  //   //print(
  //       'rrrrrrrrreeeeeeeeeeeeemmmmoooooooovvvveeeeddddddddddddddddddddddddd');
  //   await Utils.removeSharedPrefrences('fingerprint');
  //   setState(() {
  //     fingerPrintAuth = false;
  //   });
  //   //print(await Utils.getSharedPrefereces('fingerprint'));
  // }

  setUpFingerprint() async {
    if (fingerPrintAuth == true) {
      var ram = Utils.fromJSONString(await loaclStorage);
      Map<String, dynamic> hashkey = {
        "username": ram["user_name"],
        "product_id": 1,
        "device_model": deviceModel,
        "hardware_id": deviceId,
        "mac_address": macAddress,
        "time_snap": DateTime.now().toString()
      };

      String key = Encryption().encode(Utils.toJSONString(hashkey));

      Map<String, dynamic> map = {
        "username": username.text,
        "password": password.text,
        "product_id": 1,
        "hash_key": key.toString()
      };
      Map<String, dynamic> result = await HttpRequest.Post(
          'c_fingerprint_setup', Utils.constructPayload(map));

      if (Utils.isServerError(result)) {
      } else {
        await Utils.setSharedPrefereces(
            result['response']['data']['auth'], 'fingerprint');
        //print(
        // '######################################fingerprint set ##################################################');
        //print(result['response']['data']);
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => Login()),
        );
      }
    } else {
      _authenticate();
    }
  }

  perflogin() async {
    Map<String, dynamic> _map;
    //print(fingerPrintSet);
    //print('here');

    String oldPassword = await Utils.getSharedPrefereces('fingerprint');

    if (oldPassword != null) {
      if (password.text.length == 0) {
        await _authenticate();
        if (fingerPrintAuth == true) {
          //print(
          // '**************************************fingerprint**********************************************');
          Map<String, dynamic> hashkey = {
            "username": username.text,
            "system_product_id": 1,
            "device_model": deviceModel,
            "hardware_id": deviceId,
            "mac_address": macAddress,
            "is_finger_print": true,
            "time_snap": DateTime.now().toString()
          };
          String key = Encryption().encode(Utils.toJSONString(hashkey));
          //print("************************************" +
          // oldPassword.toString() +
          // "******************************************************");
          _map = {
            "username": username.text,
            "password": oldPassword,
            "password_future": key,
            "hardware_id": deviceId,
            "is_finger_print": false,
            "system_product_id": 1
          };
          reqLogin(_map);
        }
      } else {
        // print(
        // '**************************************normal*************************************');
        _map = {
          "username": username.text,
          "password": password.text,
          "password_future": "null",
          "hardware_id": "null",
          "is_finger_print": false,
          "system_product_id": 1
        };
        reqLogin(_map);
      }
    } else {
      //print(
      // '**************************************normal*******************************************');
      _map = {
        "username": username.text,
        "password": password.text,
        "password_future": "null",
        "hardware_id": "null",
        "is_finger_print": false,
        "system_product_id": 1
      };
      reqLogin(_map);
    }
  }

  reqLogin(Map<String, dynamic> map) async {
    if (map['username'] == "") {
      var ram = Utils.fromJSONString(await loaclStorage);
      map['username'] = ram['user_name'];
    }

    print('--------HHHHHHHHHHHHHH-----');
    print(map);
    Map<String, dynamic> result =
        await HttpRequest.Post('login', Utils.constructPayload(map));

    if (Utils.isServerError(result)) {
      if (result['response']['error'] == "wrong_combination") {
        setState(() {
          passwordError = pageDetails["text21"];
        });
      } else if (result['response']['error'] == "auth_error3") {
        snack.snack(title: pageDetails["text64"]);

        // ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        //     content: Text(
        //   pageDetails['text64'],
        // )));
      } else if (result['response']['error'] == "duplicate_email") {
        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (BuildContext context) =>
                ResetAccount(email: username.text, password: password.text),
          ),
        );
      } else {
        snack.snack(
            title:
                await Utils.getMessage(result['response']['error'].toString()));

        // String loginfailed = SnackBar(
        //   content: Text(
        //       await Utils.getMessage(result['response']['error'].toString())),
        // );

        // ScaffoldMessenger.of(context).showSnackBar(loginfailed);
      }
    } else {
      if (language_change == true) {
        print('language_deafault_english');
        Pallet.changelang('English');
      }
      print('jjjjjjjjjjjjjjj');
      print(result['response']['data']);
      if (result['response']['data']['email_verified'] == false) {
        showDialog(
            barrierDismissible: false,
            context: context,
            builder: (_) => AlertDialog(
                  elevation: 24.0,
                  backgroundColor: Pallet.inner1,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.all(Radius.circular(15.0)),
                  ),
                  title: Row(
                    children: [
                      Image.asset("c-logo.png", width: 40),
                      SizedBox(width: 10),
                      Text(pageDetails["text22"],
                          style: TextStyle(
                              fontSize: Pallet.heading3,
                              color: Pallet.fontcolor,
                              fontWeight: Pallet.font500)),
                    ],
                  ),
                  content: Container(
                    height: 130,
                    child: SingleChildScrollView(
                      child: Column(
                        children: [
                          Text(pageDetails["text43"],
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                  color: Pallet.fontcolor,
                                  fontSize: Pallet.heading3)),
                          SizedBox(height: 15),
                          Text(
                              result['response']['data']['email_id'].toString(),
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                  color: Pallet.fontcolor,
                                  fontSize: Pallet.heading3)),
                          SizedBox(height: 15),
                          Text(pageDetails["text44"],
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                  color: Pallet.fontcolor,
                                  height: 1.5,
                                  fontSize: Pallet.heading4)),
                        ],
                      ),
                    ),
                  ),
                  actions: [
                    PopupButton(
                      text: pageDetails["text23"],
                      onpress: () {
                        print('mmmm');
                        print(result['response']['data']['user_name']);
                        print(result['response']['data']['email_id']);
                        resend_email(
                            username: result['response']['data']['user_name'],
                            email: result['response']['data']['email_id']);
                        Navigator.of(context, rootNavigator: true)
                            .pop('dialog');
                        showDialog(
                            barrierDismissible: false,
                            context: context,
                            builder: (_) => AlertDialog(
                                  backgroundColor: Pallet.inner1,
                                  title: Row(
                                    children: [
                                      Image.asset("c-logo.png", width: 40),
                                      SizedBox(width: 10),
                                      Text(pageDetails["text22"],
                                          style: TextStyle(
                                            fontSize: Pallet.heading3,
                                            color: Pallet.fontcolor,
                                            fontWeight: Pallet.font500,
                                          )),
                                    ],
                                  ),
                                  content: Container(
                                    height: 130,
                                    child: SingleChildScrollView(
                                      child: Column(
                                        children: [
                                          Text(pageDetails["text43"],
                                              textAlign: TextAlign.center,
                                              style: TextStyle(
                                                  color: Pallet.fontcolor,
                                                  fontSize: Pallet.heading3)),
                                          SizedBox(height: 20),
                                          Text(
                                              result['response']['data']
                                                      ['email_id']
                                                  .toString(),
                                              textAlign: TextAlign.center,
                                              style: TextStyle(
                                                  color: Pallet.fontcolor,
                                                  fontSize: Pallet.heading3)),
                                          SizedBox(height: 20),
                                          Container(
                                            decoration: BoxDecoration(
                                              border: Border.all(
                                                  color: Pallet.fontcolor,
                                                  width: 2),
                                              borderRadius:
                                                  BorderRadius.circular(10),
                                            ),
                                            width: 200,
                                            height: 50,
                                            child: Center(
                                              child: Text(pageDetails["text45"],
                                                  style: TextStyle(
                                                      color: Pallet.fontcolor,
                                                      fontSize:
                                                          Pallet.heading2)),
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                  actions: [
                                    PopupButton(
                                      text: pageDetails["text40"],
                                      onpress: () {
                                        Navigator.of(context).pop();
                                      },
                                    ),
                                  ],
                                ));
                      },
                    ),
                    PopupButton(
                      text: pageDetails["text40"],
                      onpress: () {
                        Navigator.of(context).pop();
                      },
                    ),
                  ],
                ));
      } else if (result['response']['data']['two_fa_enabled'] == true) {
        showDialog(
            context: context,
            builder: (_) => AlertDialog(
                  backgroundColor: Pallet.popupcontainerback,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(Pallet.radius)),
                  title: Row(
                    children: [
                      Image.asset(
                        'assets/c-logo.png',
                        width: 40,
                      ),
                      SizedBox(width: 10),
                      Align(
                        alignment: Alignment.topLeft,
                        child: Text(
                          pageDetails['text28'],
                          style: TextStyle(
                            fontSize: 15,
                            color: Pallet.fontcolor,
                          ),
                        ),
                      ),
                    ],
                  ),
                  content: Form(
                    key: _formKey2,
                    child: TextFormField(
                        style: TextStyle(color: Pallet.fontcolor),
                        controller: otp,
                        inputFormatters: <TextInputFormatter>[
                          FilteringTextInputFormatter.digitsOnly,
                          new LengthLimitingTextInputFormatter(8),
                        ],
                        cursorColor: Pallet.fontcolor,
                        decoration: InputDecoration(
                          border: OutlineInputBorder(
                              borderSide:
                                  BorderSide(color: Pallet.fontcolor, width: 2),
                              borderRadius:
                                  BorderRadius.circular(Pallet.radius)),
                          enabledBorder: OutlineInputBorder(
                              borderSide:
                                  BorderSide(color: Pallet.fontcolor, width: 2),
                              borderRadius:
                                  BorderRadius.circular(Pallet.radius)),
                          focusedBorder: OutlineInputBorder(
                              borderSide:
                                  BorderSide(color: Pallet.fontcolor, width: 2),
                              borderRadius:
                                  BorderRadius.circular(Pallet.radius)),
                          errorBorder: OutlineInputBorder(
                              borderSide:
                                  BorderSide(color: Colors.red, width: 2),
                              borderRadius:
                                  BorderRadius.circular(Pallet.radius)),
                          focusedErrorBorder: OutlineInputBorder(
                              borderSide:
                                  BorderSide(color: Colors.red, width: 2),
                              borderRadius:
                                  BorderRadius.circular(Pallet.radius)),
                          labelText: pageDetails["text29"],
                          labelStyle: TextStyle(
                            fontSize: 15,
                            color: Pallet.fontcolor,
                          ),
                          errorText: otpError,
                        ),
                        keyboardType: TextInputType.number,
                        validator: (value) {
                          if (value.isEmpty) {
                            return pageDetails["text30"];
                          } else {
                            return otpError;
                          }
                        }),
                  ),
                  actions: [
                    PopupButton(
                      text: pageDetails["text27"],
                      onpress: () {
                        twofastatus = true;
                        _formKey2.currentState.validate();
                        check2Fa(otp.text);
                      },
                    ),
                  ],
                ));
      } else {
        if (result['response']['data']['accounts'].length > 1) {
          await showGeneralDialog(
              context: context,
              barrierDismissible: false,
              pageBuilder: (context, Animation animation,
                      Animation secondaryAnimation) =>
                  StatefulBuilder(builder: (context, setState1) {
                    return Material(
                      color: Colors.transparent,
                      child: BackdropFilter(
                        filter: ImageFilter.blur(
                          sigmaX: 3,
                          sigmaY: 3,
                        ),
                        child: Container(
                          color: Colors.transparent,
                          height: MediaQuery.of(context).size.width >= 768
                              ? MediaQuery.of(context).size.height
                              : MediaQuery.of(context).size.height * 0.6,
                          width: MediaQuery.of(context).size.width,
                          child: MediaQuery.of(context).size.width >= 768
                              ? Row(
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    IconButton(
                                        icon: Icon(
                                          Icons.keyboard_arrow_left,
                                          color: Pallet.fontcolor,
                                        ),
                                        onPressed: () {
                                          _carouselController.previousPage();
                                        }),
                                    Container(
                                      //color: Colors.red,
                                      width: MediaQuery.of(context).size.width *
                                          0.7,
                                      height: 200,
                                      child: CarouselSlider.builder(
                                          carouselController:
                                              _carouselController,
                                          options: CarouselOptions(
                                              autoPlay: false,
                                              initialPage: 0,
                                              enableInfiniteScroll: false,
                                              enlargeStrategy:
                                                  CenterPageEnlargeStrategy
                                                      .scale,
                                              reverse: false,
                                              scrollPhysics:
                                                  BouncingScrollPhysics(),
                                              viewportFraction: 0.3,
                                              enlargeCenterPage: true,
                                              scrollDirection: Axis.horizontal,
                                              onPageChanged: (index, reason) {
                                                setState1(() {
                                                  _current = index;
                                                });
                                              }),
                                          itemCount: result['response']['data']
                                                  ['accounts']
                                              .length,
                                          itemBuilder: (BuildContext context,
                                                  int index, _) =>
                                              GestureDetector(
                                                onTap: () async {
                                                  Utils.setSharedPrefereces(
                                                      result['response']['data']
                                                              ['token']
                                                          .toString(),
                                                      'for_acount');
                                                  Map<String, dynamic> _map = {
                                                    "system_product_id": 1,
                                                    "account_id":
                                                        result['response']
                                                                        ['data']
                                                                    ['accounts']
                                                                [index]
                                                            ['account_id'],
                                                    "token": (result['response']
                                                            ['data']['token'])
                                                        .toString(),
                                                  };
                                                  result =
                                                      await HttpRequest.Post(
                                                          'use_account',
                                                          Utils
                                                              .constructPayload(
                                                                  _map));
                                                  if (Utils.isServerError(
                                                      result)) {
                                                    snack.snack(
                                                        title: pageDetails[
                                                            "text65"]);

                                                    // ScaffoldMessenger.of(
                                                    //         context)
                                                    //     .showSnackBar(SnackBar(
                                                    //         content: Text(
                                                    //             pageDetails[
                                                    //                 'text65'])));
                                                    Navigator.of(context).pop();
                                                    //print(Utils.isServerError(
                                                    // result));
                                                  } else {
                                                    result['response']['data']
                                                        ['ismobile'] = ismobile;
                                                    await Utils
                                                        .setSharedPrefereces(
                                                            Utils.toJSONString(
                                                                result['response']
                                                                    ['data']),
                                                            'data');

                                                    //print(result['response']
                                                    // ['data']);
                                                    Navigator.of(context).pop();
                                                    SharedPreferences prefs =
                                                        await SharedPreferences
                                                            .getInstance();
                                                    // var browsername =
                                                    //     browser.browser +
                                                    //         browser.version
                                                    //             .toString();
                                                    final now = DateTime.now();
                                                    // if (prefs.getString(
                                                    //         'browsername') ==
                                                    //     browsername) {
                                                    //   print(
                                                    //       "ccccccccccaaaaaaaaaaaaaaaaaaaaaa");
                                                    //   print(prefs.getString(
                                                    //       'sessiontime'));

                                                    prefs.setString(
                                                        "sessiontime",
                                                        DateTime(
                                                                now.year,
                                                                now.month,
                                                                now.day,
                                                                now.hour,
                                                                now.minute)
                                                            .toString());
                                                    // }
                                                    prefs.setBool(
                                                        'shopbannershow', true);
                                                    prefs.setBool(
                                                        "newshow", true);
                                                    if (widget.winnerpage ==
                                                        true) {
                                                      prefs.setString('reload',
                                                          'winner_Announcement');
                                                      Navigator.pushNamed(
                                                          context, '/');
                                                    } else {
                                                      Navigator.pushReplacement(
                                                        context,
                                                        MaterialPageRoute(
                                                          builder: (context) =>
                                                              Home(
                                                            sessionstart: true,
                                                            twofapopup:
                                                                twofastatus,
                                                            //newshow: false,
                                                            route: 'dashboard',
                                                          ),
                                                        ),
                                                      );
                                                    }
                                                  }
                                                },
                                                child: Column(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment.center,
                                                  crossAxisAlignment:
                                                      CrossAxisAlignment.center,
                                                  children: [
                                                    CircleAvatar(
                                                      radius: index == _current
                                                          ? 70
                                                          : 30,
                                                      backgroundImage: result['response']
                                                                          ['data']
                                                                      ['accounts'][index]
                                                                  [
                                                                  'profile_image'] !=
                                                              null
                                                          ? NetworkImage((appSettings[
                                                                      'SERVER_URL'] +
                                                                  '/' +
                                                                  result['response']['data']
                                                                              ['accounts']
                                                                          [index]
                                                                      ['profile_image'])
                                                              .toString())
                                                          : AssetImage("individual.png"),
                                                    ),
                                                    SizedBox(height: 20),
                                                    Container(
                                                      child: Text(
                                                          (result['response'][
                                                                          'data']
                                                                      [
                                                                      'accounts'][index]
                                                                  [
                                                                  'screen_name'])
                                                              .toString(),
                                                          overflow: TextOverflow
                                                              .ellipsis,
                                                          maxLines: 4,
                                                          style: TextStyle(
                                                            height: 1.6,
                                                            fontSize: index ==
                                                                    _current
                                                                ? Pallet
                                                                    .heading3
                                                                : Pallet
                                                                    .heading7,
                                                            color: Pallet
                                                                .fontcolor,
                                                          )),
                                                    ),
                                                  ],
                                                ),
                                              )),
                                    ),
                                    IconButton(
                                        icon: Icon(
                                          Icons.keyboard_arrow_right,
                                          color: Pallet.fontcolor,
                                        ),
                                        onPressed: () {
                                          setState1(() {
                                            _carouselController.nextPage();
                                          });
                                        }),
                                  ],
                                )
                              : Container(
                                  //color: Colors.red,
                                  height: MediaQuery.of(context).size.height,
                                  width: MediaQuery.of(context).size.width,
                                  color: Colors.transparent,
                                  child: CarouselSlider.builder(
                                    carouselController: _carouselController,
                                    options: CarouselOptions(
                                        autoPlay: false,
                                        initialPage: 0,
                                        enableInfiniteScroll: false,
                                        viewportFraction: 0.28,
                                        enlargeCenterPage: true,
                                        reverse: false,
                                        scrollPhysics: BouncingScrollPhysics(),
                                        scrollDirection: Axis.horizontal,
                                        onPageChanged: (index, reason) {
                                          setState(() {
                                            setState(() {
                                              _current = index;
                                            });
                                          });
                                        }),
                                    itemCount: result['response']['data']
                                            ['accounts']
                                        .length,
                                    itemBuilder:
                                        (BuildContext context, int index, _) =>
                                            GestureDetector(
                                      onTap: () async {
                                        Map<String, dynamic> _map = {
                                          "system_product_id": 1,
                                          "account_id": result['response']
                                                  ['data']['accounts'][index]
                                              ['account_id'],
                                          "token": (result['response']['data']
                                                  ['token'])
                                              .toString(),
                                        };
                                        result = await HttpRequest.Post(
                                            'use_account',
                                            Utils.constructPayload(_map));
                                        if (Utils.isServerError(result)) {
                                          snack.snack(
                                              title: pageDetails["text65"]);

                                          // ScaffoldMessenger.of(context)
                                          //     .showSnackBar(SnackBar(
                                          //         content: Text(
                                          //             pageDetails['text65'])));
                                          Navigator.of(context).pop();
                                          //print(Utils.isServerError(result));
                                        } else {
                                          result['response']['data']
                                              ['ismobile'] = ismobile;
                                          await Utils.setSharedPrefereces(
                                              Utils.toJSONString(
                                                  result['response']['data']),
                                              'data');

                                          //print(result['response']['data']);
                                          Navigator.of(context).pop();
                                          SharedPreferences prefs =
                                              await SharedPreferences
                                                  .getInstance();
                                          // var browsername = browser.browser +
                                          //     browser.version.toString();
                                          final now = DateTime.now();
                                          // if (prefs.getString('browsername') ==
                                          //     browsername) {
                                          //   print(
                                          //       "ccccccccccaaaaaaaaaaaaaaaaaaaaaa");
                                          //   print(
                                          //       prefs.getString('sessiontime'));

                                          prefs.setString(
                                              "sessiontime",
                                              DateTime(
                                                      now.year,
                                                      now.month,
                                                      now.day,
                                                      now.hour,
                                                      now.minute)
                                                  .toString());
                                          // }
                                          prefs.setBool('shopbannershow', true);
                                          prefs.setBool("newshow", true);
                                          if (widget.winnerpage == true) {
                                            prefs.setString('reload',
                                                'winner_Announcement');
                                            Navigator.pushNamed(context, '/');
                                          } else {
                                            Navigator.pushReplacement(
                                              context,
                                              MaterialPageRoute(
                                                builder: (context) => Home(
                                                  sessionstart: true,
                                                  twofapopup: twofastatus,
                                                  route: 'dashboard',
                                                ),
                                              ),
                                            );
                                          }
                                        }
                                      },
                                      child: Container(
                                        // height:
                                        //     MediaQuery.of(context).size.height *
                                        //         0.4,
                                        //padding: EdgeInsets.all(10),
                                        child: Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.center,
                                          children: [
                                            CircleAvatar(
                                              radius: 25,
                                              backgroundImage: result['response']
                                                                      ['data']
                                                                  ['accounts']
                                                              [index]
                                                          ['profile_image'] !=
                                                      null
                                                  ? NetworkImage((appSettings[
                                                              'SERVER_URL'] +
                                                          '/' +
                                                          result['response']
                                                                          ['data']
                                                                      ['accounts']
                                                                  [index]
                                                              ['profile_image'])
                                                      .toString())
                                                  : AssetImage("individual.png"),
                                            ),
                                            SizedBox(height: 15),
                                            Padding(
                                              padding: const EdgeInsets.only(
                                                  left: 8.0),
                                              child: Container(
                                                child: Text(
                                                    (result['response']['data']
                                                                    ['accounts']
                                                                [index]
                                                            ['screen_name'])
                                                        .toString(),
                                                    overflow: TextOverflow.fade,
                                                    //maxLines: 4,
                                                    style: TextStyle(
                                                      fontSize: Pallet.heading7,
                                                      color: Pallet.fontcolor,
                                                    )),
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ),
                                  ),
                                ),
                        ),
                      ),
                    );
                  }));
        } else {
          Map<String, dynamic> _map = {
            "system_product_id": 1,
            "account_id": result['response']['data']['accounts'][0]
                ['account_id'],
            "token": (result['response']['data']['token']).toString(),
          };
          result = await HttpRequest.Post(
              'use_account', Utils.constructPayload(_map));
          if (Utils.isServerError(result)) {
            snack.snack(title: pageDetails["text65"]);

            // ScaffoldMessenger.of(context)
            //     .showSnackBar(SnackBar(content: Text(pageDetails['text65'])));
            Navigator.of(context).pop();
            //print(Utils.isServerError(result));
          } else {
            await Utils.setSharedPrefereces(
                Utils.toJSONString(result['response']['data']), 'data');
            SharedPreferences prefs = await SharedPreferences.getInstance();
            // var browsername = browser.browser + browser.version.toString();
            final now = DateTime.now();
            // if (prefs.getString('browsername') == browsername) {
            //   print("ccccccccccaaaaaaaaaaaaaaaaaaaaaa");
            //   print(prefs.getString('sessiontime'));

            prefs.setString(
                "sessiontime",
                DateTime(now.year, now.month, now.day, now.hour, now.minute)
                    .toString());
            // }
            if (widget.winnerpage == true) {
              prefs.setString('reload', 'winner_Announcement');
              Navigator.pushNamed(context, '/');
            } else {
              Navigator.pushReplacement(
                context,
                MaterialPageRoute(
                  builder: (context) => Home(
                    twofapopup: twofastatus,
                    //  newshow: false,
                    route: 'dashboard',
                    sessionstart: true,
                  ),
                ),
              );
            }
          }
        }
      }
    }
  }

  forgot(BuildContext context) {
    // set up the button

    // set up the AlertDialog
    AlertDialog alert = AlertDialog(
      backgroundColor: Pallet.fontcolor,
      title: Text(pageDetails["text10"],
          style: TextStyle(
              color: Pallet.fontcolornew, fontWeight: Pallet.font500)),
      content: Form(
        child: Wrap(
          runSpacing: 15,
          children: [
            Textbox(
              label: pageDetails["text35"],
              header: pageDetails["text35"],
              // controller: fotp1,
              errorText: otperror,
              validation: (value) {
                validate(setState);
              },
            ),
            Textbox(
              label: pageDetails["text32"],
              header: pageDetails["text32"],
              controller: pwd,
              errorText: pwderror,
            ),
            Textbox(
              label: pageDetails["text33"],
              header: pageDetails["text33"],
              controller: cnfrmpwd,
              errorText: cnfrmpwderror,
            ),
          ],
        ),
      ),
      actions: [],
    );

    // show the dialog
    showDialog(
      barrierDismissible: true,
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }

  validate(setState) {
    if (fotp.text.isEmpty)
      setState(() {
        otperror = pageDetails["text34"];
      });
    if (pwd.text.isEmpty)
      setState(() {
        pwderror = pageDetails["text34"];
      });
    if (cnfrmpwd.text.isEmpty)
      setState(() {
        cnfrmpwderror = pageDetails["text34"];
      });
  }
}
