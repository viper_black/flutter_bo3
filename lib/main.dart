import 'dart:async';
import 'package:centurion/screens/spleash_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:uni_links/uni_links.dart';
import 'package:url_strategy/url_strategy.dart';
import 'home.dart';
import 'login.dart';
import 'config.dart';
import 'package:get/get.dart';

import 'lottery/common.dart';
import 'screens/maintenancepage.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  SystemChrome.setEnabledSystemUIOverlays([]);
  setPathUrlStrategy();
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  // ignore: unused_field
  String _latestLink = 'Unknown';
  Uri _latestUri;
  var key, params;

  // ignore: cancel_subscriptions
  StreamSubscription _sub;

  var code;

  Future<void> initPlatformStateForUriUniLinks() async {
    _sub = getUriLinksStream().listen((Uri uri) {
      if (!mounted) return;
      setState(() {
        _latestUri = uri;
        if (_latestUri != null) {
          final queryParams = _latestUri.queryParameters;
          if (queryParams.length > 0) {
            String userName = queryParams["invite"];
            // ignore: unused_local_variable
            String success = queryParams["success"];
            String activate = queryParams["activation"];
            // ignore: unused_local_variable
            String fail = queryParams["fail"];
            //code = userName;
            if (userName != null) {
              MaterialPageRoute(
                  builder: (BuildContext context) => Spleash_Screen(
                        referrral: userName,
                      ));
            } else if (activate != null) {
              MaterialPageRoute(
                  builder: (BuildContext context) => Spleash_Screen(
                        activatekey: activate,
                      ));
            }
            //print("username:$userName");
            //print("My users username is: $userName");
          }
        }
        _latestLink = uri?.toString() ?? 'Unknown';
      });
    }, onError: (Object err) {
      if (!mounted) return;
      setState(() {
        _latestUri = null;
        _latestLink = 'Failed to get latest link: $err.';
      });
    });
  }

  reload() async {
    print("snnnnnn");
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String stringValue = prefs.getString('reload');

    if (stringValue == "login" || stringValue == "signup") {
    } else {
      Navigator.pushReplacement(
          context,
          MaterialPageRoute(
            builder: (BuildContext context) => Home(
              route: stringValue,
            ),
          ));
    }
    print("lllllllllllllllll");
    print(stringValue);
  }

  @override
  void initState() {
    super.initState();
  }

  String success;
  String fail;
  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
        debugShowCheckedModeBanner: false,
        title: 'Centurion',
        theme: ThemeData(
          fontFamily: 'Montserrat',
          // fontFamily: 'Montserrat', sans-serif;
          primaryColor: Colors.blue,
          primarySwatch: Colors.blue,
        ),
        routes: {
          '/': (context) => Spleash_Screen(),
          '/maintenance': (context) => Maintenancepage(),
        },
        initialRoute: '/',
        // home: Scaffold(body: Spleash_Screen()),
        // ignore: missing_return
        onGenerateRoute: (settings) {
          // ignore: unused_local_variable
          List<String> pathComponents = settings.name.split('/');
          final settingsUri = Uri.parse(settings.name);
          // final postID = settingsUri.queryParameters['invite'];
          final sucess = settingsUri.queryParameters['validate'];
          final fail = settingsUri.queryParameters['fail'];
          final activate = settingsUri.queryParameters['activation'];
          print("path is here");
          print(pathComponents.length);
          if (pathComponents[1] == 'invite') {
            print("waiting");
          }
          print(pathComponents);

          if (pathComponents[1] == 'lottery') {
            if (pathComponents.length == 3) {
              return MaterialPageRoute(
                  builder: (BuildContext context) => Spleash_Screen(
                        winnerpage: true,
                      ));
            } else {
              return MaterialPageRoute(
                  builder: (BuildContext context) => Spleash_Screen());
            }
          }

          if (pathComponents[1] == 'invite') {
            if (pathComponents.length == 3) {
              return MaterialPageRoute(
                  builder: (BuildContext context) => Spleash_Screen(
                        referrral: pathComponents[2],
                      ));
            } else {
              return MaterialPageRoute(
                  builder: (BuildContext context) => Spleash_Screen());
            }
          }
          if (pathComponents[1] == 'change_password') {
            if (pathComponents.length == 3) {
              return MaterialPageRoute(
                builder: (BuildContext context) => Spleash_Screen(
                  passwordchange: pathComponents[2],
                ),
              );
            }
          }
          // advcash payment url = site.com/payments/advcash_success

          // if (pathComponents[1] == 'payments') {
          //   if (pathComponents.length == 3) {
          //     return MaterialPageRoute(
          //         builder: (BuildContext context) => Spleash_Screen(
          //               sucess: pathComponents[2].toString().split('_')[1],
          //             ));
          //   } else {
          //     return MaterialPageRoute(
          //         builder: (BuildContext context) => Spleash_Screen());
          //   }
          // }

          if (pathComponents[1] == 'payments') {
            if (pathComponents.length == 4) {
              return MaterialPageRoute(
                  builder: (BuildContext context) => Spleash_Screen(
                        shopingtoken: pathComponents[3],
                      ));
            } else if (pathComponents.length == 3) {
              return MaterialPageRoute(
                  builder: (BuildContext context) => Spleash_Screen(
                        sucess: pathComponents[2].toString().split('_')[1],
                      ));
            } else {
              snack.snack(title: 'Wrong URL');
              return MaterialPageRoute(
                  builder: (BuildContext context) => Spleash_Screen());
            }
          } else if (activate != null) {
            return MaterialPageRoute(
                builder: (BuildContext context) => Spleash_Screen(
                      activatekey: activate,
                    ));
          } else if (sucess != null) {
            return MaterialPageRoute(
                builder: (BuildContext context) => Spleash_Screen(
                      sucess: sucess,
                    ));
          } else if (fail != null) {
            return MaterialPageRoute(
                builder: (BuildContext context) => Spleash_Screen(
                      fail: fail,
                    ));
          }
        });
  }
}

class Main extends StatefulWidget {
  @override
  _MainState createState() => _MainState();
}

class _MainState extends State<Main> {
  @override
  Widget build(BuildContext context) {
    Brightness mode = MediaQuery.of(context).platformBrightness;
    if (mode == Brightness.light) {
      setState(() {
        Pallet.litTheme();
      });
    } else {
      Pallet.darkTheme();
    }
    return Login();
  }
}
