// import 'services/business/user.dart';
// import 'package:centurion/services/business/account.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/services.dart';
import 'package:flutter_icons/flutter_icons.dart';
//import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:intl/intl.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:url_launcher/url_launcher.dart';
import 'animations/Bouncing_button.dart';
import 'data.dart';
import 'config.dart';
import 'package:dropdownfield/dropdownfield.dart';
import 'services/business/cart.dart';
import 'utils/utils.dart' show Utils;
import 'services/communication/index.dart' show HttpRequest;
import 'common.dart';
//------------------------------------------------------------
//web supported import
//this import is won't support android if you build apk please comment this one.............................................
// ignore: avoid_web_libraries_in_flutter
import 'package:universal_html/html.dart' as html;
//---------------------------------------------------

import 'login.dart';
import './config/index.dart';
import 'package:intl_phone_number_input/intl_phone_number_input.dart';
// import 'dart:convert';
// import 'login.dart';

// ignore: must_be_immutable
class Signup extends StatefulWidget {
  Signup({Key key, this.referral}) : super(key: key) {
    sponsor.text = referral;
  }
  TextEditingController sponsor = TextEditingController(text: '');

  final String referral;
  // final String invitertype;

  @override
  _SignupState createState() => _SignupState();
}

class _SignupState extends State<Signup> {
  Map<String, dynamic> pageDetails = {};

  void setPageDetails() async {
    String data = await Utils.getPageDetails('signup');
    setState(() {
      pageDetails = Utils.fromJSONString(data);
    });
  }

  reload() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString("reload", "signup");
    String stringValue = prefs.getString('reload');
    print(stringValue);
    print("sssssssssssss");
  }

  String invitertype = 'none';
  identifyusertype() async {
    if (widget.referral != null) {
      Map<String, dynamic> _map = {
        "system_product_id": 1,
        "user_name": widget.referral,
      };
      var _result =
          await HttpRequest.Post('findusertype', Utils.constructPayload(_map));
      print("zzzzzzzzzzzzzzzzzzzzzzzzzzzz");
      print(_result);
      if (Utils.isServerError(_result)) {
        if (_result['response']['error'] == 'user_does_not_exists') {
          snack.snack(title: pageDetails["text58"]);
          // final snackBar = SnackBar(content: Text(pageDetails['text58']));
          // ScaffoldMessenger.of(context).showSnackBar(snackBar);
        } else {
          snack.snack(title: pageDetails["text78"]);
          // final snackBar = SnackBar(content: Text(pageDetails['text78']));
          // ScaffoldMessenger.of(context).showSnackBar(snackBar);
        }
      } else {
        var val = _result['response']['data']['account_type_id'].toString();
        print(val);

        if (val == '1') {
          setState(() {
            pageidx = 3;
            accountHoldingType = 1;
            accountType = 1;

            invitertype = 'Q3VzdG9tZXI=';
            print("jjjjjjjjjjjjjjjj");
            print(invitertype);
          });
        } else if (val == '2') {
          setState(() {
            invitertype = 'bmV0d29ya2Vy';
            pageidx = 1;
            accountHoldingType = 1;
            print("jjjjjjjjjjjjjjjj");
            print(invitertype);
          });
        } else {
          setState(() {
            pageidx = 1;
            invitertype = 'none';
            print("jjjjjjjjjjjjjjjj");
            print(invitertype);
          });
        }
        print(_result['response']);
      }
    } else {
      setState(() {
        pageidx = 1;
        invitertype = 'none';
      });
    }
  }

  bool enabled = false;

  int invitetype;
  void initState() {
    reload();
    print("jssssssssssssssssssjsjjjs");
    print(widget.referral);
    identifyusertype();
    //networker invitelink
    // if (widget.invitertype == "bmV0d29ya2Vy") {
    //   pageidx = 1;
    //   accountHoldingType = 1;
    // }
    // //customer invitelink
    // else if (widget.invitertype == "Q3VzdG9tZXI=") {
    //   pageidx = 3;
    //   accountHoldingType = 1;
    //   accountType = 1;
    // } else {
    //   pageidx = 1;
    // }
    super.initState();
  }

  _SignupState() {
    SampleClass()
        .getCountries({"product_id": 2, "option": 2, "find": "null"}, output);
    setPageDetails();
  }
  int pageidx = 1;
  // double headerLineLeft = 0;
  // List dots = [
  //   {"color": Pallet.fontcolor, "used": true},
  //   {"color": Pallet.fontcolor, "used": false},
  //   {"color": Pallet.fontcolor, "used": false},
  //   {"color": Pallet.fontcolor, "used": false},
  //   {"color": Pallet.fontcolor, "used": false},
  //   {"color": Pallet.fontcolor, "used": false},
  //   {"color": Pallet.fontcolor, "used": false},
  //   {"color": Pallet.fontcolor, "used": false}
  // ];
// Individual Id | Costomer Id = 1
//Corporate Id | Networker Id = 2
  int accountHoldingType = 1;
  int accountType = 1;

  int sponsorId = 1;

  TextEditingController username = TextEditingController();
  String userError;
  FocusNode usernode = FocusNode();

  TextEditingController companyName = TextEditingController();
  String companyError;
  FocusNode companynode = FocusNode();

  TextEditingController email = TextEditingController();
  String emailError;
  FocusNode emailnode = FocusNode();

  TextEditingController domain = TextEditingController();
  String domainError;
  FocusNode domainnode = FocusNode();

  TextEditingController password = TextEditingController();
  String passwordError;
  FocusNode passwordnode = FocusNode();

  TextEditingController passwordconfirm = TextEditingController();
  String confirmError;
  FocusNode confirmnode = FocusNode();

  // TextEditingController sponsor = TextEditingController(text: '');
  String sponsorError;
  FocusNode sponsornode = FocusNode();
  FocusNode next1 = FocusNode();
  FocusNode next2 = FocusNode();
  FocusNode next3 = FocusNode();
  FocusNode next4 = FocusNode();
  FocusNode next5 = FocusNode();
  FocusNode next6 = FocusNode();
  FocusNode next7 = FocusNode();

  FocusNode back1 = FocusNode();
  FocusNode back2 = FocusNode();
  FocusNode back3 = FocusNode();
  FocusNode back4 = FocusNode();
  FocusNode back5 = FocusNode();
  FocusNode back6 = FocusNode();
  FocusNode back7 = FocusNode();
//  FocusNode back1=FocusNode();

  TextEditingController firstname = TextEditingController();
  String firstnameError;
  FocusNode firstnamenode = FocusNode();

  TextEditingController lastname = TextEditingController();
  String lastnameError;
  FocusNode lastnamenode = FocusNode();

  TextEditingController altEmail = TextEditingController();
  String altEmailError;
  FocusNode altemailnode = FocusNode();

  TextEditingController phone = TextEditingController();
  String phoneError;
  String errorMessage;
  String initialCountry = 'IN';
  PhoneNumber number = PhoneNumber(isoCode: 'IN');
  // bool setSelectorButtonAsPrefixIcon = true;
  FocusNode phonenode = FocusNode();

  TextEditingController address = TextEditingController();
  String addressError;
  FocusNode addressnode = FocusNode();

  TextEditingController dob = TextEditingController();
  String dobError;
  FocusNode dobnode = FocusNode();
  FocusNode countrynode = FocusNode();

  String country = "";
  String countryError = "";

  List<String> countryList = [];

  List dbCountryList = [];

  output(value) {
    dbCountryList = value;
    for (var item in value) {
      setState(() {
        countryList.add(item['country_name']);
      });
    }
  }

  GlobalKey<FormState> _screen3FormKey = GlobalKey<FormState>();
  GlobalKey<FormState> _screen4FormKey = GlobalKey<FormState>();
  GlobalKey<FormState> _screen5FormKey = GlobalKey<FormState>();
  GlobalKey<FormState> _screen6FormKey = GlobalKey<FormState>();
  GlobalKey<FormState> _screen7FormKey = GlobalKey<FormState>();

  DateTime selectedDate = DateTime.now();
  // Future<Null> _selectDate(BuildContext context) async {
  //   final DateTime picked = await showDatePicker(
  //       context: context,
  //       initialDate: selectedDate,
  //       firstDate: DateTime(1900, 1),
  //       lastDate: DateTime(2101));
  //   if (picked != null && picked != selectedDate)
  //     setState(() {
  //       selectedDate = picked;
  //     });
  // }
  TextEditingController datecontroller = TextEditingController();

  DateTime _date = DateTime.now();
  // final adult = DateTime(DateTime.now().year - 18);
  final currentyear = DateTime(DateTime.now().year);
  final int age = 18;

  // bool isAdult(String birthDateString) {
  //   String datePattern = "dd-MM-yyyy";

  //   DateTime birthDate = DateFormat(datePattern).parse(birthDateString);
  //   DateTime today = DateTime.now();

  //   int yearDiff = today.year - birthDate.year;
  //   int monthDiff = today.month - birthDate.month;
  //   int dayDiff = today.day - birthDate.day;

  //   return yearDiff > 18 || yearDiff == 18 && monthDiff >= 0 && dayDiff >= 0;
  // }

  _handleDatePicker() async {
    final DateFormat _dateFormatter = DateFormat('MMM dd, yyyy');
    @override
    final DateTime date = await showDatePicker(
      context: context,
      initialDate: _date,
      firstDate: DateTime(1900),
      lastDate: DateTime(currentyear.year + 1),
    );
    if (date != null && date != _date) {
      setState(() {
        _date = date;
      });
      if (currentyear.year - date.year < 18) {
        setState(() {
          dobError = pageDetails['text79'];
        });
      } else {
        dobError = null;
      }
      // DateTime test = _dateFormatter.parse(date.toString());
      dob.text = _dateFormatter.format(date);
    }
  }

  bool ismobile;
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      body: LayoutBuilder(builder: (context, constraints) {
        if (constraints.maxWidth < ScreenSize.ipad) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              // Maintainance(),
              Expanded(
                child: Container(
                  padding: EdgeInsets.only(top: 15),
                  width: size.width,
                  height: size.height,
                  color: Pallet.inner2,
                  child: SingleChildScrollView(
                    child: Column(
                      children: [
                        SizedBox(
                          height: 60,
                        ),
                        Align(
                          alignment: Alignment.center,
                          child: Image.asset(
                            'mobile_logo.png',
                            height: 100,
                          ),
                        ),
                        SizedBox(
                          height: 30,
                        ),
                        Container(
                            padding: EdgeInsets.all(15),
                            width: size.width,
                            height: size.height - 205,
                            decoration: BoxDecoration(
                                color: Pallet.inner1,
                                borderRadius: BorderRadius.only(
                                    topRight: Radius.circular(30),
                                    topLeft: Radius.circular(30))),
                            child: screen(wdgtWidth: 400, ismobile: true)),
                      ],
                    ),
                  ),
                ),
              ),
            ],
          );
        } else {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              // Maintainance(),
              Expanded(
                child: Row(
                  children: [
                    Container(
                      padding: EdgeInsets.only(top: 15),
                      width: 420,
                      height: size.height,
                      color: Pallet.inner1,
                      child: Center(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            SizedBox(
                              height: 20,
                            ),
                            Image.asset(
                              'logo.png',
                              height: 100,
                            ),
                            SizedBox(
                              height: 30,
                            ),
                            Container(
                                padding: EdgeInsets.all(15),
                                height: size.height - 180,
                                child: screen(wdgtWidth: 350, ismobile: false)),
                          ],
                        ),
                      ),
                    ),
                    Center(
                      child: Container(
                        width: size.width - 420,
                        height: size.height,
                        color: Pallet.inner2,
                        child: Image.asset(
                          "centurion-logo.png",
                        ),
                      ),
                    )
                  ],
                ),
              ),
            ],
          );
        }
      }),
    );
  }

  Container headerline() {
    return Container(
        width: 200,
        height: 50,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
              padding: const EdgeInsets.only(left: 35),
              child: header(),
            ),
            Container(
              margin: EdgeInsets.only(left: 30),
              color: Pallet.fontcolor,
              width: 150,
              height: 1,
            ),
            RotationTransition(
              turns: AlwaysStoppedAnimation(-45 / 360),
              child: Container(
                  margin: EdgeInsets.only(top: 13),
                  width: 30,
                  height: 1,
                  color: Pallet.fontcolor),
            ),
          ],
        ));
  }

  // ignore: missing_return
  Text header() {
    if (pageidx == 0)
      return Text(pageDetails["text1"],
          style: TextStyle(color: Pallet.fontcolor));
    else if (pageidx == 1)
      return Text(pageDetails["text1"],
          style: TextStyle(color: Pallet.fontcolor));
    else if (pageidx == 2)
      return Text(pageDetails["text2"],
          style: TextStyle(color: Pallet.fontcolor));
    else if (pageidx == 3)
      return Text(pageDetails["text3"],
          style: TextStyle(color: Pallet.fontcolor));
    else if (pageidx == 4)
      return Text(pageDetails["text4"],
          style: TextStyle(color: Pallet.fontcolor));
    else if (pageidx == 5)
      return Text(pageDetails["text5"],
          style: TextStyle(color: Pallet.fontcolor));
    else if (pageidx == 6)
      return Text(pageDetails["text6"],
          style: TextStyle(color: Pallet.fontcolor));
    else if (pageidx == 7)
      return Text(pageDetails["text7"],
          style: TextStyle(color: Pallet.fontcolor));
  }

  // ignore: missing_return
  Widget screen({double wdgtWidth, bool ismobile}) {
    if (pageidx == 0)
      return screen1(wdgtWidth: wdgtWidth);
    else if (accountHoldingType == 1) {
      if (pageidx == 1)
        return screen2(wdgtWidth: wdgtWidth);
      else if (pageidx == 2)
        return screen3(wdgtWidth: wdgtWidth);
      else if (pageidx == 3)
        return screen4(wdgtWidth: wdgtWidth);
      else if (pageidx == 4) return screen5(wdgtWidth: wdgtWidth);
      if (accountType == 1) {
        if (pageidx == 5) return screen7(wdgtWidth: wdgtWidth);
        // }else {
        // if (pageidx == 4) return screen5(wdgtWidth: wdgtWidth);
      }
    } else {
      if (pageidx == 1)
        return screen3(wdgtWidth: wdgtWidth);
      else if (pageidx == 2)
        return screen4(wdgtWidth: wdgtWidth);
      else if (pageidx == 3) return screen6(wdgtWidth: wdgtWidth);
    }
  }

  Container screen1({double wdgtWidth}) {
    Size size = MediaQuery.of(context).size;

    return Container(
        width: wdgtWidth,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(height: 30),
            Text(
              pageDetails["text1"],
              style: TextStyle(
                  fontSize: Pallet.heading1,
                  fontWeight: Pallet.font500,
                  color: Pallet.fontcolor),
            ),
            SizedBox(
              height: 30,
            ),
            option(
                label: pageDetails["text8"],
                // myimage: 'individual11.ico',
                myicon: Icons.person,
                condition: accountHoldingType == 1,
                wdgtWidth: wdgtWidth,
                operation: () {
                  setState(() {
                    accountHoldingType = 1;
                  });
                }),
            SizedBox(
              height: 20,
            ),
            option(
                label: pageDetails["text9"],
                // myimage: 'corporate11.ico',
                myicon: Icons.people_outline_rounded,
                condition: accountHoldingType == 2,
                wdgtWidth: wdgtWidth,
                operation: () {
                  setState(() {
                    accountHoldingType = 2;
                    ismobile == true
                        ? confirmcorporate(wdgtWidth, size.width)
                        : confirmcorporate(
                            wdgtWidth,
                            400,
                          );
                  });
                }),
            SizedBox(height: 20),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                // widget.invitertype == "bmV0d29ya2Vy"
                //     ? Container()
                //     :
                button(
                  node: back1,
                  label: pageDetails["text10"],
                  opration: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => Login()),
                    );
                  },
                ),
                button(
                  node: next1,
                  label: pageDetails["text11"],
                  opration: () {
                    setState(() {
                      next(width: wdgtWidth);
                    });
                  },
                ),
              ],
            ),
          ],
        ));
  }

  confirmcorporate(
    double wdgtWidth,
    dialogwidth,
  ) {
    return showDialog(
        barrierDismissible: false,
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            backgroundColor: Pallet.popupcontainerback,
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(Pallet.radius)),
            title: Row(
              // mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                Image.asset(
                  'assets/c-logo.png',
                  width: 40,
                ),
                SizedBox(width: 10),
                Expanded(
                  child: Align(
                    alignment: Alignment.topLeft,
                    child: Text(
                      pageDetails['text69'],
                      style: TextStyle(
                        fontSize: Pallet.heading3,
                        // fontWeight: FontWeight.bold,
                        color: Pallet.fontcolor,
                      ),
                    ),
                  ),
                ),
              ],
            ),
            content: SingleChildScrollView(
              child: Container(
                  width: dialogwidth,
                  // width: ismobile == null ? size.width : 400,
                  height: 320,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      Text(
                        pageDetails['text70'],
                        style: TextStyle(
                          fontSize: Pallet.heading4,
                          color: Pallet.fontcolor,
                        ),
                      ),
                      SizedBox(height: 5),
                      Text(
                        pageDetails['text71'],
                        style: TextStyle(
                          fontSize: Pallet.heading4,
                          color: Pallet.fontcolor,
                        ),
                      ),
                      SizedBox(height: 5),
                      Text(pageDetails['text72'],
                          style: TextStyle(
                              fontSize: Pallet.heading4,
                              color: Pallet.fontcolor,
                              fontWeight: Pallet.font500)),
                      SizedBox(height: 5),
                      Text(
                        pageDetails['text73'],
                        style: TextStyle(
                          fontSize: Pallet.heading4,
                          // fontWeight: FontWeight.bold,
                          color: Pallet.fontcolor,
                        ),
                      ),
                      SizedBox(height: 5),
                      Text(
                        pageDetails['text74'],
                        style: TextStyle(
                            fontSize: Pallet.heading4,
                            color: Colors.yellow,
                            fontWeight: Pallet.font500
                            // color: Pallet.fontcolor,
                            ),
                      ),
                    ],
                  )),
            ),
            actions: [
              PopupButton(
                text: pageDetails['text10'],
                onpress: () {
                  Navigator.of(context).pop();
                },
              ),
              PopupButton(
                text: pageDetails['text11'],
                onpress: () {
                  setState(() {
                    pageidx = 1;
                    accountHoldingType = 2;
                    accountType = 2;
                  });
                  Navigator.of(context).pop();
                  // next(width: wdgtWidth);
                },
              ),
            ],
          );
        });
  }

  Widget button(
      {String label, Function opration, FocusNode node, Function onkey}) {
    return
        // RawKeyboardListener(
        //   focusNode: node,
        //   onKey: onkey,
        //   child:
        Bouncing(
      onPress: opration,
      child: MouseRegion(
        cursor: SystemMouseCursors.click,
        child: Container(
          width: 100,
          height: 35,
          decoration: BoxDecoration(
            color: Pallet.fontcolor,
//shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(8.0),
//side: BorderSide(color: Colors.red),
//),
          ),
// padding: const EdgeInsets.fromLTRB(0, 18, 0, 18),
          child: Center(
            child: Text(
              label,
              style: TextStyle(
                color: Pallet.fontcolornew,
                fontSize: Pallet.heading3,
                // fontWeight: FontWeight.w600
              ),
            ),
          ),
        ),
      ),
      // ),
    );
  }

  InkWell option({
    String label,
    bool condition,
    Function operation,
    double wdgtWidth,
    @required IconData myicon,
  }) {
    return InkWell(
      onTap: operation,
      child: Container(
        padding: EdgeInsets.symmetric(horizontal: 10, vertical: 15),
        width: wdgtWidth * 0.50,
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10),
            color: condition ? Pallet.dashcontainerback : Pallet.fontcolor,
            border: Border.all(
                width: 2,
                color: condition ? Pallet.fontcolor : Pallet.fontcolor)),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Icon(
              myicon,
              size: 25,
              color: condition ? Pallet.fontcolor : Pallet.fontcolornew,
            ),
            SizedBox(height: 5),
            // Image.asset(
            //   myimage,
            //   width: 25,
            //   color: condition ? Pallet.fontcolor : Pallet.fontcolornew,
            // ),
            Text(
              label,
              style: TextStyle(
                  color: condition ? Pallet.fontcolor : Pallet.fontcolornew,
                  fontSize: Pallet.heading3,
                  fontWeight: Pallet.font500),
            ),
          ],
        ),
      ),
    );
  }

  Widget screen2({double wdgtWidth}) {
    return Container(
      width: wdgtWidth,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SizedBox(height: 30),
          Text(
            pageDetails["text77"],
            style: TextStyle(
                fontSize: Pallet.heading1,
                fontWeight: Pallet.font500,
                color: Pallet.fontcolor),
          ),
          SizedBox(
            height: 30,
          ),
          option(
              label: pageDetails["text13"],
              // myimage: 'customer.png',
              myicon: Feather.user,
              condition: accountType == 1,
              wdgtWidth: wdgtWidth,
              operation: () {
                setState(() {
                  accountType = 1;
                });
              }),
          SizedBox(
            height: 20,
          ),
          option(
              label: pageDetails["text12"],
              myicon: Icons.people_alt,
              condition: accountType == 2,
              wdgtWidth: wdgtWidth,
              operation: () {
                setState(() {
                  accountType = 2;
                });
              }),
          SizedBox(height: 20),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              invitertype == "bmV0d29ya2Vy"
                  ? Container()
                  : button(
                      node: back2,
                      label: pageDetails["text10"],
                      opration: () {
                        // setState(() {
                        //   back(width: wdgtWidth);
                        // });
                        Navigator.push(
                          context,
                          MaterialPageRoute(builder: (context) => Login()),
                        );
                      },
                    ),
              button(
                node: next2,
                label: pageDetails["text11"],
                opration: () {
                  setState(() {
                    next(width: wdgtWidth);
                  });
                },
              ),
            ],
          ),
        ],
      ),
    );
  }

  Widget screen3({double wdgtWidth}) {
    screen3Validation() {
      _screen3FormKey.currentState.validate();
    }

    screen3Submit() {
      screen3Validation();
      if (sponsorError == null) {
        setState(() {
          next(width: wdgtWidth);
        });
      }
    }

    return Container(
      width: wdgtWidth,
      child: Form(
        key: _screen3FormKey,
        child: ListView(
          // crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(height: 30),
            Text(
              pageDetails["text2"],
              style: TextStyle(
                  fontSize: Pallet.heading1,
                  fontWeight: Pallet.font500,
                  color: Pallet.fontcolor),
            ),
            SizedBox(height: 20),
            SignupTextBox(
              label: pageDetails["text14"],
              icon: Icons.person,
              // myimage: 'sponser.png',
              onChanged: (value) => screen3Validation(),
              onFieldSubmitted: (value) => screen3Submit(),
              isPrefixIcon: true,
              isSuffixIcon: false,
              // autofocus: false,
              // focusnode: sponsornode,
              digitsOnlyPhone: false,
              autofocus: widget.referral == null ? true : false,
              readonly: widget.referral == null
                  ? false
                  : widget.sponsor.text == ''
                      ? false
                      : sponsorError == null
                          ? true
                          : false,
              // tabpress: (event) {
              //   if (event.isKeyPressed(LogicalKeyboardKey.tab)) {
              //     FocusScope.of(context).requestFocus(next3);
              //   }
              // },
              isPassword: false,
              controller: widget.sponsor,
              errorText: sponsorError,
              // onsubmit: () {
              //   FocusScope.of(context).requestFocus(next3);
              // },
              validation: (value) {
                setState(() {
                  checSponsor(value);
                });
              },
            ),
            SizedBox(height: 10),
            Text(pageDetails["text15"],
                textAlign: TextAlign.left,
                style: TextStyle(
                    fontSize: Pallet.heading6,
                    color: Pallet.fontcolor,
                    fontWeight: Pallet.font500)),
            SizedBox(height: 20),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                button(
                  node: back3,
                  label: pageDetails["text10"],
                  opration: () {
                    setState(() {
                      back(width: wdgtWidth);
                      widget.sponsor.text = null;
                    });
                  },
                ),
                button(
                  // onkey: (event) {
                  //   if (event.isKeyPressed(LogicalKeyboardKey.enter) ||
                  //       event.isKeyPressed(LogicalKeyboardKey.numpadEnter)) {
                  //     if (sponsorError == null) {
                  //       setState(() {
                  //         next(width: wdgtWidth);
                  //       });
                  //     }
                  //   }
                  // },
                  // node: next3,
                  label: pageDetails["text11"],
                  opration: () {
                    screen3Submit();
                    // screen3Validation();
                    // if (sponsorError == null) {
                    //   setState(() {
                    //     next(width: wdgtWidth);
                    //   });
                    // }
                    // if (sponsor.text.length < 1) {
                    //   setState(() {
                    //     sponsorError = "Sponsor Required";
                    //   });
                    // } else {
                    //   if (sponsorError == null) {
                    //     setState(() {
                    //       next(width: wdgtWidth);
                    //     });
                    //   }
                    // }
                  },
                ),
              ],
            ),
            // Center(
            //   child: Text(
            //     pageDetails["text2"],
            //     style: TextStyle(
            //         fontSize: Pallet.heading1,
            //         fontWeight: Pallet.font500,
            //         color: Pallet.fontcolor),
            //   ),
            // ),
            // SizedBox(height: 20),
            // SignupTextBox(
            //   label: pageDetails["text14"],
            //   icon: Icons.person,
            //   isPassword: false,
            //   controller: sponsor,
            //   errorText: sponsorError,
            //   validation: (value) {
            //     setState(() {
            //       checSponsor(value);
            //     });
            //   },
            // ),
            // SizedBox(height: 20),
            // SizedBox(height: 20),
            // Row(
            //   mainAxisAlignment: MainAxisAlignment.spaceBetween,
            //   children: [
            //     button(
            //       label: pageDetails["text10"],
            //       opration: () {
            //         setState(() {
            //           back(width: wdgtWidth);
            //         });
            //       },
            //     ),
            //     button(
            //       label: pageDetails["text11"],
            //       opration: () {
            //         if (sponsor.text.length < 1) {
            //           setState(() {
            //             sponsorError = "Sponsor Required";
            //           });
            //         } else {
            //           if (sponsorError == null) {
            //             setState(() {
            //               next(width: wdgtWidth);
            //             });
            //           }
            //         }
            //       },
            //     ),
            //   ],
            // ),
          ],
        ),
      ),
    );
  }

  triger() {
    showDialog(
        barrierDismissible: false,
        context: context,
        builder: (_) => confirmaccount());
  }

  AlertDialog confirmaccount() {
    return AlertDialog(
      backgroundColor: Pallet.inner1,
      title: Text(pageDetails["text16"],
          style: TextStyle(
            color: Pallet.fontcolor,
          )),
      content: SingleChildScrollView(
        child: ListBody(
          children: [
            DataTable(
              columns: <DataColumn>[
                DataColumn(
                  label: Text(
                    pageDetails["text17"],
                    style: TextStyle(color: Pallet.fontcolor),
                  ),
                ),
                DataColumn(
                  label: Text(
                    username.text,
                    style: TextStyle(color: Pallet.fontcolor),
                  ),
                ),
              ],
              rows: <DataRow>[
                DataRow(
                  cells: <DataCell>[
                    DataCell(Text(pageDetails["text7"],
                        style: TextStyle(color: Pallet.fontcolor))),
                    DataCell(Text(email.text,
                        style: TextStyle(color: Pallet.fontcolor))),
                  ],
                ),
                DataRow(
                  cells: <DataCell>[
                    DataCell(Text(pageDetails["text19"],
                        style: TextStyle(color: Pallet.fontcolor))),
                    DataCell(Text(
                        widget.sponsor.text == "" ? "" : widget.sponsor.text,
                        style: TextStyle(color: Pallet.fontcolor))),
                  ],
                ),
                DataRow(
                  cells: <DataCell>[
                    DataCell(Text(pageDetails["text20"],
                        style: TextStyle(color: Pallet.fontcolor))),
                    DataCell(Text(country,
                        style: TextStyle(color: Pallet.fontcolor))),
                  ],
                ),
              ],
            ),
          ],
        ),
      ),
      actions: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            PopupButton(
              text: pageDetails["text21"],
              onpress: () {
                Navigator.of(context).pop();
                signup();
              },
            ),
            SizedBox(width: 10),
            PopupButton(
              text: pageDetails["text22"],
              onpress: () {
                //------------------------------------------------------------
                //web supported import
                //this import is won't support android if you build apk please comment this one.............................................

                //----------------------------------------------------------------------

                //android support
                invitertype == null
                    ? Navigator.of(context).pop()
                    : html.window.location.href = appSettings["CLIENT_URL"];
              },
            ),
          ],
        ),
      ],
    );
  }

  AlertDialog accountsuccess() {
    return AlertDialog(
      backgroundColor: Pallet.inner1,
      title: CircleAvatar(
          backgroundColor: Pallet.fontcolor,
          radius: 25,
          child: Icon(
            Icons.done_outline_rounded,
            color: Pallet.inner1,
            size: 20,
          )),
      content: Container(
        height: 130,
        child: SingleChildScrollView(
          child: Column(
            children: [
              Text(pageDetails["text23"],
                  textAlign: TextAlign.center,
                  style: TextStyle(color: Pallet.fontcolor)),
              SizedBox(height: 15),
              Text(pageDetails["text75"],
                  textAlign: TextAlign.center,
                  style: TextStyle(color: Pallet.fontcolor)),
              SizedBox(height: 15),
              Text(pageDetails["text76"],
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      color: Pallet.fontcolor, fontSize: Pallet.heading4)),
              SizedBox(height: 10),
              Text(email.text,
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      color: Pallet.fontcolor, fontSize: Pallet.heading4)),
            ],
          ),
        ),
      ),
      actions: [
        PopupButton(
          text: pageDetails['text80'],
          onpress: () {
            Navigator.of(context).pop();
          },
        ),
        SizedBox(height: 10),
      ],
    );
  }

  AlertDialog accountcancelled() {
    return AlertDialog(
      backgroundColor: Pallet.inner1,
      title: CircleAvatar(
          backgroundColor: Pallet.fontcolor,
          radius: 25,
          child: Icon(
            Icons.close,
            color: Pallet.errortxt,
            size: 20,
          )),
      content: Text(pageDetails["text24"],
          textAlign: TextAlign.center,
          style: TextStyle(color: Pallet.errortxt)),
      actions: [
        PopupButton(
          text: pageDetails['text80'],
          onpress: () {
            Navigator.of(context).pop();
          },
        ),
        SizedBox(height: 10),
      ],
    );
  }

// screen4
  Widget screen4({double wdgtWidth}) {
    screen4Validation() {
      _screen4FormKey.currentState.validate();
    }

    screen4Submit() async {
      screen4Validation();
      if (username.text.isEmpty ||
          password.text.isEmpty ||
          passwordconfirm.text.isEmpty) {
        if (username.text.isEmpty) {
          setState(() {
            userError = pageDetails["text30"];
          });
        }
        if (password.text.isEmpty) {
          setState(() {
            passwordError = pageDetails["text31"];
          });
        }
        if (passwordconfirm.text.isEmpty) {
          setState(() {
            confirmError = pageDetails["text32"];
          });
        }
      } else {
        await checkUsername(username.text);
        print('USERERROR: ${userError}');

        if (userError == null) {
          // if (!usernamecheck) {
          // setState(() {
          //   userError = pageDetails["text33"];
          // });
          if (username.text == password.text) {
            setState(() {
              passwordError = pageDetails["text34"];
            });
          } else {
            if (userError == null &&
                passwordError == null &&
                confirmError == null) {
              setState(() {
                next(width: wdgtWidth);
              });
            }
          }
        } else {
          print('USERERROR: ${userError}');

          print("dddddddddddddsssssxxxxxxxxxx");
        }
      }
    }

    return Container(
      width: wdgtWidth,
      child: Form(
        key: _screen4FormKey,
        child: ListView(
          // crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(height: 30),
            Text(
              pageDetails["text3"],
              style: TextStyle(
                  fontSize: Pallet.heading1,
                  fontWeight: Pallet.font500,
                  color: Pallet.fontcolor),
            ),
            SizedBox(height: 20),
            SignupTextBox(
                onChanged: (value) => screen4Validation(),
                onFieldSubmitted: (value) => screen4Submit(),
                label: pageDetails["text25"],
                icon: Icons.person,
                isPrefixIcon: true,
                isSuffixIcon: false,
                // focusnode: usernode,
                digitsOnlyPhone: false,
                autofocus: true,
                // tabpress: (event) {
                //   if (event.isKeyPressed(LogicalKeyboardKey.tab)) {
                //     FocusScope.of(context).requestFocus(passwordnode);
                //   }
                // },
                controller: username,
                isPassword: false,
                errorText: userError,
                validation: (value) {
                  setState(() {
                    checkUsername(value);
                  });
                }),
            SizedBox(height: 10),
            Text(pageDetails["text26"],
                style: TextStyle(
                    fontSize: Pallet.heading6,
                    color: Pallet.fontcolor,
                    fontWeight: Pallet.font500)),
            SizedBox(height: 20),
            SignupTextBox(
              // focusnode: passwordnode,
              // tabpress: (event) {
              //   if (event.isKeyPressed(LogicalKeyboardKey.tab)) {
              //     FocusScope.of(context).requestFocus(confirmnode);
              //   }
              // },
              isSuffixIcon: true,
              label: pageDetails["text27"],
              isPrefixIcon: true, onChanged: (value) => screen4Validation(),
              onFieldSubmitted: (value) => screen4Submit(),

              icon: Icons.lock,
              controller: password,
              digitsOnlyPhone: false,
              isPassword: true,
              errorText: passwordError,
              validation: (value) {
                setState(() {
                  checkPassword(value);
                });
              },
            ),
            SizedBox(height: 10),
            Text(pageDetails["text28"],
                style: TextStyle(
                    fontSize: Pallet.heading6,
                    // fontWeight: Pallet.font500,
                    color: Pallet.fontcolor)),
            SizedBox(height: 20),
            SignupTextBox(
              // tabpress: (event) {
              //   if (event.isKeyPressed(LogicalKeyboardKey.tab)) {
              //     FocusScope.of(context).requestFocus(next4);
              //   }
              // },
              label: pageDetails["text29"],
              // focusnode: confirmnode,
              isPrefixIcon: true,
              isSuffixIcon: true,
              icon: Icons.lock, onChanged: (value) => screen4Validation(),
              onFieldSubmitted: (value) => screen4Submit(),

              isPassword: true,
              digitsOnlyPhone: false,
              controller: passwordconfirm,
              errorText: confirmError,
              // onsubmit: () {
              //   FocusScope.of(context).requestFocus(next4);
              // },
              validation: (value) {
                setState(() {
                  checkReTypePassword(value);
                });
              },
            ),
            SizedBox(height: 20),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                invitertype == "Q3VzdG9tZXI="
                    ? Container()
                    : button(
                        // node: back4,
                        label: pageDetails["text10"],
                        opration: () {
                          setState(() {
                            back(width: wdgtWidth);
                          });
                        },
                      ),
                button(
                    // node: next4,
                    // onkey: (event) {
                    //   if (event.isKeyPressed(LogicalKeyboardKey.enter) ||
                    //       event.isKeyPressed(LogicalKeyboardKey.numpadEnter)) {
                    //     if (username.text.isEmpty ||
                    //         password.text.isEmpty ||
                    //         passwordconfirm.text.isEmpty) {
                    //       if (username.text.isEmpty) {
                    //         setState(() {
                    //           userError = pageDetails["text30"];
                    //         });
                    //       }
                    //       if (password.text.isEmpty) {
                    //         setState(() {
                    //           passwordError = pageDetails["text31"];
                    //         });
                    //       }
                    //       if (passwordconfirm.text.isEmpty) {
                    //         setState(() {
                    //           confirmError = pageDetails["text32"];
                    //         });
                    //       }
                    //     } else {
                    //       if (username.text.length > 0) {
                    //         var usernamecheck = Utils.isUsername(username.text);
                    //         if (!usernamecheck) {
                    //           setState(() {
                    //             userError = pageDetails["text33"];
                    //           });
                    //         } else {
                    //           if (username.text == password.text) {
                    //             setState(() {
                    //               passwordError = pageDetails["text34"];
                    //             });
                    //           } else {
                    //             if (userError == null &&
                    //                 passwordError == null &&
                    //                 confirmError == null) {
                    //               setState(() {
                    //                 next(width: wdgtWidth);
                    //               });
                    //             }
                    //           }
                    //         }
                    //       }
                    //     }
                    //   }
                    // },
                    //node: next2,
                    label: pageDetails["text11"],
                    opration: () {
                      screen4Submit();
                      // if (username.text.isEmpty ||
                      //     password.text.isEmpty ||
                      //     passwordconfirm.text.isEmpty) {
                      //   if (username.text.isEmpty) {
                      //     setState(() {
                      //       userError = pageDetails["text30"];
                      //     });
                      //   }
                      //   if (password.text.isEmpty) {
                      //     setState(() {
                      //       passwordError = pageDetails["text31"];
                      //     });
                      //   }
                      //   if (passwordconfirm.text.isEmpty) {
                      //     setState(() {
                      //       confirmError = pageDetails["text32"];
                      //     });
                      //   }
                      // } else {
                      //   if (username.text.length > 0) {
                      //     var usernamecheck = Utils.isUsername(username.text);
                      //     if (!usernamecheck) {
                      //       setState(() {
                      //         userError = pageDetails["text33"];
                      //       });
                      //     } else {
                      //       if (username.text == password.text) {
                      //         setState(() {
                      //           passwordError = pageDetails["text34"];
                      //         });
                      //       } else {
                      //         if (userError == null &&
                      //             passwordError == null &&
                      //             confirmError == null) {
                      //           setState(() {
                      //             next(width: wdgtWidth);
                      //           });
                      //         }
                      //       }
                      //     }
                      //   }
                      // }
                      // if (username.text.length < 1) {
                      //   setState(() {
                      //     userError = pageDetails["text30"];
                      //   });
                      // } else if (password.text.length < 1) {
                      //   setState(() {
                      //     passwordError = pageDetails["text31"];
                      //   });
                      // } else if (passwordconfirm.text.length < 1) {
                      //   setState(() {
                      //     confirmError = "Re-Type Password Required";
                      //   });
                      // } else {
                      //   if (userError == null &&
                      //       passwordError == null &&
                      //       confirmError == null) {
                      //     setState(() {
                      //       next(width: wdgtWidth);
                      //     });
                      //   }
                      // }
                    }),
              ],
            ),
            SizedBox(height: 30),
          ],
        ),
      ),
    );
  }

  Widget screen5({double wdgtWidth}) {
    screen5Validation() {
      _screen5FormKey.currentState.validate();
    }

    screen5Submit() {
      screen5Validation();
      if (accountType == 2) {
        if (email.text.length < 1) {
          setState(() {
            emailError = pageDetails["text38"];
          });
        } else if (country.length < 1) {
          setState(() {
            countryError = pageDetails["text39"];
          });
        } else if (enabled != true) {
          snack.snack(title: pageDetails["text81"]);

          // ScaffoldMessenger.of(context).hideCurrentSnackBar();
          // ScaffoldMessenger.of(context)
          //     .showSnackBar(SnackBar(content: Text(pageDetails["text81"])));
        } else {
          countryError = "";
          if (emailError == null && countryError == "" && enabled == true) {
            // confirmaccount();
            triger();
            // signup();
          }
        }
      } else {
        if (email.text.length < 1) {
          setState(() {
            emailError = pageDetails["text38"];
          });
        } else if (country.length < 1) {
          setState(() {
            countryError = pageDetails["text39"];
          });
        } else if (enabled != true) {
          snack.snack(title: pageDetails["text81"]);

          // ScaffoldMessenger.of(context).hideCurrentSnackBar();
          // ScaffoldMessenger.of(context)
          //     .showSnackBar(SnackBar(content: Text(pageDetails["text81"])));
        } else {
          countryError = "";
          if (emailError == null && countryError == "" && enabled == true) {
            next(width: wdgtWidth);
          }
        }
      }
    }

    return Container(
      width: wdgtWidth,
      child: Form(
        key: _screen5FormKey,
        child: ListView(
          // crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(height: 30),
            Text(
              pageDetails["text4"],
              style: TextStyle(
                  fontSize: Pallet.heading1,
                  fontWeight: Pallet.font500,
                  color: Pallet.fontcolor),
            ),
            SizedBox(height: 20),
            SignupTextBox(
              autofocus: true,
              // focusnode: emailnode,
              // tabpress: (event) {
              //   if (event.isKeyPressed(LogicalKeyboardKey.tab)) {
              //     FocusScope.of(context).requestFocus(next5);
              //   }
              // },
              label: pageDetails["text35"],
              onChanged: (value) => screen5Validation(),
              onFieldSubmitted: (value) => screen5Submit(),
              isPrefixIcon: true,
              icon: Icons.mail,
              isSuffixIcon: false,
              errorText: emailError,
              digitsOnlyPhone: false,
              // onsubmit: () {
              //   FocusScope.of(context).requestFocus(next5);
              // },
              isPassword: false,
              controller: email,
              validation: (value) {
                setState(() {
                  print(value);
                  checkEmail(value);
                });
              },
            ),
            SizedBox(height: 20),
            Container(
              width: double.infinity,
              padding: const EdgeInsets.only(left: 10.0, right: 10.0),
              decoration: BoxDecoration(
                color: Pallet.dashcontainerback,
                borderRadius: BorderRadius.circular(10.0),
                // color: Pallet.component,
              ),
              child: DropdownButtonHideUnderline(
                // child: RawKeyboardListener(
                //   focusNode: countrynode,
                child: DropDownField(
                  labelText: pageDetails["text36"],
                  labelStyle: TextStyle(
                      color: Pallet.fontcolor,
                      fontSize: 12.0,
                      fontWeight: Pallet.font500),
                  enabled: true,
                  value: country,
                  items: countryList,
                  onValueChanged: (value) {
                    setState(() {
                      country = value;
                    });
                  },
                  itemsVisibleInDropdown: 5,
                  textStyle: TextStyle(
                      color: Pallet.fontcolor, fontSize: Pallet.heading5),
                ),
              ),
            ),
            // ),
            Container(
                margin: EdgeInsets.only(left: 10),
                child: Text(
                  countryError,
                  style: TextStyle(color: Colors.red),
                )),
            SizedBox(height: 20),
            Container(
              child: Row(
                children: [
                  Checkbox(
                    overlayColor: MaterialStateColor.resolveWith(
                        (states) => Pallet.fontcolornew),
                    fillColor: MaterialStateColor.resolveWith(
                        (states) => Pallet.fontcolor),
                    hoverColor: Pallet.fontcolor,
                    checkColor: Pallet.fontcolornew,
                    value: enabled,
                    onChanged: (value) {
                      setState(() {
                        print("kkksssxxxzzz");
                        print(value);
                        enabled = value;
                        print(enabled);
                      });
                    },
                  ),
                  SizedBox(
                    width: 5,
                  ),
                  MouseRegion(
                    cursor: SystemMouseCursors.click,
                    child: GestureDetector(
                      onTap: () {
                        launch(appSettings['SERVER_URL'] +
                            '/document_ducatus/Terms_and_Conditions.pdf');
                      },
                      child: Text(
                        pageDetails['text82'],
                        style: TextStyle(
                          fontSize: 12,
                          decoration: TextDecoration.underline,
                          color: Pallet.fontcolor,
                          fontWeight: Pallet.font500,
                          letterSpacing: 0.5,
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
            SizedBox(height: 20),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                button(
                  // node: back5,
                  label: pageDetails["text10"],
                  opration: () {
                    setState(() {
                      back(width: wdgtWidth);
                    });
                  },
                ),
                if (accountType == 2)
                  button(
                      // node: next5,
                      // onkey: (event) {
                      //   if (event.isKeyPressed(LogicalKeyboardKey.enter) ||
                      //       event
                      //           .isKeyPressed(LogicalKeyboardKey.numpadEnter)) {
                      //     if (email.text.length < 1) {
                      //       setState(() {
                      //         emailError = pageDetails["text38"];
                      //       });
                      //     } else if (country.length < 1) {
                      //       setState(() {
                      //         countryError = pageDetails["text39"];
                      //       });
                      //     } else if (enabled != true) {
                      //       ScaffoldMessenger.of(context).hideCurrentSnackBar();
                      //       ScaffoldMessenger.of(context).showSnackBar(
                      //           SnackBar(content: Text(pageDetails["text81"])));
                      //     } else {
                      //       countryError = "";
                      //       if (email.text.trim().isEmpty) {
                      //         emailError = pageDetails["text38"];
                      //       } else {
                      //         output(value) {
                      //           if (value == 'true') {
                      //             setState(() {
                      //               emailError = null;
                      //             });
                      //           } else {
                      //             setState(() {
                      //               emailError = pageDetails['text68'];
                      //             });
                      //           }
                      //           if (emailError == null &&
                      //               countryError == "" &&
                      //               enabled == true) {
                      //             // confirmaccount();
                      //             triger();
                      //           }
                      //         }

                      //         SampleClass().checkemail(email.text, (output));
                      //       }
                      //     }
                      //   }
                      // },
                      label: pageDetails["text37"],
                      opration: () {
                        screen5Submit();
                        // if (email.text.length < 1) {
                        //   setState(() {
                        //     emailError = pageDetails["text38"];
                        //   });
                        // } else if (country.length < 1) {
                        //   setState(() {
                        //     countryError = pageDetails["text39"];
                        //   });
                        // } else if (enabled != true) {
                        //   ScaffoldMessenger.of(context).hideCurrentSnackBar();
                        //   ScaffoldMessenger.of(context).showSnackBar(
                        //       SnackBar(content: Text(pageDetails["text81"])));
                        // } else {
                        //   countryError = "";
                        //   if (emailError == null &&
                        //       countryError == "" &&
                        //       enabled == true) {
                        //     // confirmaccount();
                        //     triger();
                        //     // signup();
                        //   }
                        // }
                      })
                else
                  button(
                    // node: next5,
                    label: pageDetails["text11"],
                    // onkey: (event) {
                    //   if (event.isKeyPressed(LogicalKeyboardKey.tab)) {
                    //     setState(() {
                    //       if (email.text.length < 1) {
                    //         setState(() {
                    //           emailError = pageDetails["text38"];
                    //         });
                    //       } else if (country.length < 1) {
                    //         setState(() {
                    //           countryError = pageDetails["text39"];
                    //         });
                    //       } else if (enabled != true) {
                    //         ScaffoldMessenger.of(context).hideCurrentSnackBar();
                    //         ScaffoldMessenger.of(context).showSnackBar(
                    //             SnackBar(content: Text(pageDetails["text81"])));
                    //       } else {
                    //         countryError = "";
                    //         if (emailError == null &&
                    //             countryError == "" &&
                    //             enabled == true) {
                    //           next(width: wdgtWidth);
                    //         }
                    //       }
                    //     });
                    //   }
                    // },
                    opration: () {
                      setState(() {
                        screen5Submit();

                        // if (email.text.length < 1) {
                        //   setState(() {
                        //     emailError = pageDetails["text38"];
                        //   });
                        // } else if (country.length < 1) {
                        //   setState(() {
                        //     countryError = pageDetails["text39"];
                        //   });
                        // } else if (enabled != true) {
                        //   ScaffoldMessenger.of(context).hideCurrentSnackBar();
                        //   ScaffoldMessenger.of(context).showSnackBar(
                        //       SnackBar(content: Text(pageDetails["text81"])));
                        // } else {
                        //   countryError = "";
                        //   if (emailError == null &&
                        //       countryError == "" &&
                        //       enabled == true) {
                        //     next(width: wdgtWidth);
                        //   }
                        // }
                      });
                    },
                  )
              ],
            ),
            SizedBox(height: 30),
          ],
        ),
      ),
    );
  }

  Widget screen6({double wdgtWidth}) {
    screen6Validation() {
      _screen6FormKey.currentState.validate();
    }

    screen6Submit() {
      screen6Validation();
      if (companyName.text.isEmpty ||
          email.text.isEmpty ||
          country.isEmpty ||
          domain.text.isEmpty) {
        if (companyName.text.length < 1) {
          setState(() {
            companyError = pageDetails["text43"];
          });
        }
        if (domain.text.length < 1) {
          setState(() {
            domainError = pageDetails["text43"];
          });
        }
        if (email.text.length < 1) {
          setState(() {
            emailError = pageDetails["text38"];
          });
        }
        if (country.length < 1) {
          setState(() {
            countryError = pageDetails["text39"];
          });
        }
      } else {
        countryError = "";
        if (companyError == null &&
            emailError == null &&
            domainError == null &&
            countryError == "") {
          // signup();
          triger();
        }
      }
    }

    return Container(
      width: wdgtWidth,
      child: Form(
        key: _screen6FormKey,
        child: ListView(
          // crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(height: 30),
            Text(
              pageDetails["text40"],
              style: TextStyle(
                  fontSize: Pallet.heading1,
                  fontWeight: Pallet.font500,
                  color: Pallet.fontcolor),
            ),
            SizedBox(height: 20),
            SignupTextBox(
                // focusnode: companynode,
                autofocus: true,
                // tabpress: (event) {
                //   if (event.isKeyPressed(LogicalKeyboardKey.tab)) {
                //     FocusScope.of(context).requestFocus(domainnode);
                //   }
                // },
                label: pageDetails["text41"],
                icon: Icons.person_add_alt_1_outlined,
                onChanged: (value) => screen6Validation(),
                onFieldSubmitted: (value) => screen6Submit(),
                isPrefixIcon: true,
                isSuffixIcon: true,
                errorText: companyError,
                controller: companyName,
                digitsOnlyPhone: false,
                isPassword: false,
                validation: (value) {
                  checkCompany(value);
                }),
            SizedBox(height: 20),
            SignupTextBox(
              onChanged: (value) => screen6Validation(),
              onFieldSubmitted: (value) => screen6Submit(),

              // focusnode: domainnode,
              label: pageDetails["text67"],
              digitsOnlyPhone: false,
              isPrefixIcon: true,
              isSuffixIcon: true,
              icon: Icons.adjust,
              errorText: domainError,
              isPassword: false,
              controller: domain,
              validation: (value) {
                setState(() {
                  checkCompanyDomain(value);
                });
              },
            ),
            SizedBox(height: 20),
            SignupTextBox(
              onChanged: (value) => screen6Validation(),
              onFieldSubmitted: (value) => screen6Submit(),

              // focusnode: emailnode,
              label: pageDetails["text42"],
              isSuffixIcon: true,
              isPrefixIcon: true,
              icon: Icons.mail,
              errorText: emailError,
              digitsOnlyPhone: false,
              isPassword: false,
              controller: email,
              validation: (value) {
                setState(() {
                  checkEmail(value);
                });
              },
            ),
            SizedBox(height: 20),
            Container(
              width: double.infinity,
              padding: const EdgeInsets.only(left: 10.0, right: 10.0),
              decoration: BoxDecoration(
                color: Pallet.dashcontainerback,
                borderRadius: BorderRadius.circular(10.0),
                // color: Pallet.component,
              ),
              child: DropdownButtonHideUnderline(
                child: DropDownField(
                  labelText: pageDetails["text36"],
                  labelStyle: TextStyle(
                      color: Pallet.fontcolor,
                      fontSize: 12.0,
                      fontWeight: Pallet.font500),
                  enabled: true,
                  value: country,
                  items: countryList,
                  onValueChanged: (value) {
                    setState(() {
                      country = value;
                    });
                  },
                  itemsVisibleInDropdown: 5,
                  textStyle: TextStyle(
                      color: Pallet.fontcolor, fontSize: Pallet.heading5),
                ),
              ),
            ),
            SizedBox(height: 20),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                button(
                  // node: back6,
                  label: pageDetails["text10"],
                  opration: () {
                    setState(() {
                      back(width: wdgtWidth);
                    });
                  },
                ),
                button(
                  // node: next6,
                  label: pageDetails["text37"],
                  opration: () {
                    screen6Submit();
                    // if (companyName.text.isEmpty ||
                    //     email.text.isEmpty ||
                    //     country.isEmpty ||
                    //     domain.text.isEmpty) {
                    //   if (companyName.text.length < 1) {
                    //     setState(() {
                    //       companyError = pageDetails["text43"];
                    //     });
                    //   }
                    //   if (domain.text.length < 1) {
                    //     setState(() {
                    //       domainError = pageDetails["text43"];
                    //     });
                    //   }
                    //   if (email.text.length < 1) {
                    //     setState(() {
                    //       emailError = pageDetails["text38"];
                    //     });
                    //   }
                    //   if (country.length < 1) {
                    //     setState(() {
                    //       countryError = pageDetails["text39"];
                    //     });
                    //   }
                    // } else {
                    //   countryError = "";
                    //   if (companyError == null &&
                    //       emailError == null &&
                    //       domainError == null &&
                    //       countryError == "") {
                    //     // signup();
                    //     triger();
                    //   }
                    // }
                  },
                ),
              ],
            ),
            SizedBox(height: 30),
          ],
        ),
      ),
    );
  }

  Widget screen7({double wdgtWidth}) {
    screen7Validation() {
      _screen7FormKey.currentState.validate();
    }

    screen7Submit() {
      screen7Validation();
      if (firstname.text.isEmpty ||
          lastname.text.isEmpty ||
          phone.text.isEmpty ||
          address.text.isEmpty ||
          dob.text.isEmpty) {
        if (firstname.text.length < 1) {
          setState(() {
            firstnameError = pageDetails["text46"];
          });
        }
        if (lastname.text.length < 1) {
          setState(() {
            lastnameError = pageDetails["text48"];
          });
        }
        if (phone.text.length < 1) {
          setState(() {
            phoneError = pageDetails["text51"];
          });
        }
        if (address.text.length < 1) {
          setState(() {
            addressError = pageDetails["text53"];
          });
        }
        if (dob.text.length < 1) {
          setState(() {
            dobError = pageDetails["text55"];
          });
        }
      } else {
        if (firstname.text == lastname.text) {
          setState(() {
            lastnameError = pageDetails["text56"];
          });
        } else if (dobError == null) {
          triger();
        } else {
          // signup();
        }
      }
    }

    return Container(
      width: wdgtWidth,
      child: Form(
        key: _screen7FormKey,
        child: ListView(
          // crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(height: 30),
            Text(
              pageDetails["text44"],
              style: TextStyle(
                  fontSize: Pallet.heading1,
                  fontWeight: Pallet.font500,
                  color: Pallet.fontcolor),
            ),
            SizedBox(height: 20),
            SignupTextBox(
                autofocus: true,
                // focusnode: firstnamenode,
                // tabpress: (event) {
                //   if (event.isKeyPressed(LogicalKeyboardKey.tab)) {
                //     FocusScope.of(context).requestFocus(lastnamenode);
                //   }
                // },
                label: pageDetails["text45"],
                icon: Icons.person_add_alt_1_outlined,
                onChanged: (value) => screen7Validation(),
                onFieldSubmitted: (value) => screen7Submit(),
                errorText: firstnameError,
                isPrefixIcon: true,
                controller: firstname,
                isSuffixIcon: false,
                digitsOnlyPhone: false,
                isPassword: false,
                validation: (value) {
                  setState(() {
                    if (value.isEmpty) {
                      firstnameError = pageDetails["text46"];
                    } else {
                      firstnameError = null;
                    }
                  });
                }),
            SizedBox(height: 20),
            SignupTextBox(
              onChanged: (value) => screen7Validation(),
              onFieldSubmitted: (value) => screen7Submit(),

              // focusnode: lastnamenode,
              // tabpress: (event) {
              //   if (event.isKeyPressed(LogicalKeyboardKey.tab)) {
              //     FocusScope.of(context).requestFocus(altemailnode);
              //   }
              // },
              label: pageDetails["text47"],
              icon: Icons.person_add_alt_1_outlined,
              errorText: lastnameError,
              isPassword: false,
              isPrefixIcon: true,
              controller: lastname,
              digitsOnlyPhone: false,
              isSuffixIcon: false,
              validation: (value) {
                setState(() {
                  if (value.isEmpty) {
                    lastnameError = pageDetails["text48"];
                  } else {
                    lastnameError = null;
                  }
                });
              },
            ),
            // SizedBox(height: 20),
            // SignupTextBox(
            //   tabpress: (event) {
            //     if (event.isKeyPressed(LogicalKeyboardKey.tab)) {
            //       FocusScope.of(context).requestFocus(phonenode);
            //     }
            //   },
            //   focusnode: altemailnode,
            //   label: pageDetails["text49"],
            //   icon: Icons.mail,
            //   isSuffixIcon: true,
            //   isPrefixIcon: true,
            //   isPassword: false,
            //   digitsOnlyPhone: false,
            //   controller: altEmail,
            // ),
            SizedBox(height: 20),
            // Container(
            //     decoration: BoxDecoration(
            //       borderRadius: BorderRadius.circular(10),
            //       color: Pallet.fontcolor,
            //     ),
            //     child: InternationalPhoneNumberInput(
            //       onInputChanged: (PhoneNumber number) {
            //       },
            //       onInputValidated: (bool value) {
            //       },
            //       selectorConfig: SelectorConfig(
            //         selectorType: PhoneInputSelectorType.BOTTOM_SHEET,
            //         // showFlags: true,
            //         // useEmoji = true,
            //         // setSelectorButtonAsPrefixIcon = true,
            //       ),
            //       ignoreBlank: false,
            //       cursorColor: Pallet.fontcolornew,
            //       hintText: "Phone",
            //       textStyle: TextStyle(color: Pallet.fontcolornew),
            //       errorMessage: phoneError,
            //       textFieldController: phone,
            //       autoValidateMode: AutovalidateMode.disabled,
            //       selectorTextStyle: TextStyle(color: Pallet.fontcolornew),
            //       initialValue: number,
            //       // textFieldController: controller,
            //       formatInput: false,
            //       keyboardType: TextInputType.numberWithOptions(
            //           signed: true, decimal: true),
            //       inputBorder: OutlineInputBorder(
            //         borderSide: BorderSide.none,
            //       ),

            //       onSaved: (PhoneNumber number) {
            //       },
            //     )),
            // SizedBox(height: 20),
            SignupTextBox(
              onChanged: (value) => screen7Validation(),
              onFieldSubmitted: (value) => screen7Submit(),

              // tabpress: (event) {
              //   if (event.isKeyPressed(LogicalKeyboardKey.tab)) {
              //     FocusScope.of(context).requestFocus(addressnode);
              //   }
              // },
              // focusnode: phonenode,
              label: pageDetails["text50"],
              icon: Icons.phone,
              isSuffixIcon: false,
              isPrefixIcon: true,
              errorText: phoneError,
              isPassword: false,
              digitsOnlyPhone: true,
              controller: phone,
              validation: (value) {
                setState(() {
                  if (value.isEmpty) {
                    phoneError = pageDetails["text51"];
                    // } else if (!RegExp(r"^[0-9]*$").hasMatch(value)) {
                    //   phone.text =
                    //       phone.text.substring(0, phone.text.toString().length - 1);
                  } else {
                    phoneError = null;
                  }
                });
              },
            ),
            SizedBox(height: 20),
            SignupTextBox(
              onChanged: (value) => screen7Validation(),
              onFieldSubmitted: (value) => screen7Submit(),

              // focusnode: addressnode,
              // tabpress: (event) {
              //   if (event.isKeyPressed(LogicalKeyboardKey.enter) ||
              //       event.isKeyPressed(LogicalKeyboardKey.numpadEnter)) {
              //     FocusScope.of(context).requestFocus(dobnode);
              //   }
              // },
              label: pageDetails["text52"],
              icon: Icons.location_on,
              errorText: addressError,
              isSuffixIcon: false,
              isPrefixIcon: true,
              digitsOnlyPhone: false,
              isPassword: false,
              controller: address,
              validation: (value) {
                setState(() {
                  if (value.isEmpty) {
                    addressError = pageDetails["text53"];
                  } else {
                    addressError = null;
                  }
                });
              },
            ),
            SizedBox(height: 20),
            TextFormField(
              onChanged: (value) => screen7Validation(),
              onFieldSubmitted: (value) => screen7Submit(),

              // focusNode: dobnode,
              readOnly: true,
              controller: dob,
              onTap: _handleDatePicker,
              textAlign: TextAlign.left,
              style: TextStyle(
                  color: Pallet.fontcolornew,
                  fontSize: Pallet.heading3,
                  fontWeight: Pallet.font500),
              cursorColor: Pallet.fontcolornew,
              keyboardType: TextInputType.emailAddress,
              decoration: InputDecoration(
                errorText: dobError,
                errorStyle: TextStyle(
                  fontSize: Pallet.heading4,
                  color: Pallet.errortxt,
                ),
                border: OutlineInputBorder(
                    borderSide:
                        const BorderSide(color: Color(0XFF1034a6), width: 1.0),
                    borderRadius: BorderRadius.circular(8.0)),
                hintText: pageDetails["text54"],
                hintStyle: TextStyle(
                    fontSize: Pallet.heading4,
                    fontWeight: Pallet.font500,
                    color: Pallet.fontcolornew),
                fillColor: Pallet.fontcolor,
                focusedBorder: OutlineInputBorder(
                    borderSide:
                        const BorderSide(color: Color(0XFF1034a6), width: 1.0),
                    borderRadius: BorderRadius.circular(8.0)),
                enabledBorder: OutlineInputBorder(
                    borderSide:
                        BorderSide(color: Color(0xFF4570ae), width: 0.0),
                    borderRadius: BorderRadius.circular(8.0)),
                filled: true,
                prefixIcon: Padding(
                    padding: EdgeInsets.only(left: 20, right: 10),
                    child: Icon(Icons.calendar_today,
                        size: 22, color: Pallet.fontcolornew)),
              ),
            ),
            SizedBox(height: 20),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                button(
                  // node: back7,
                  label: pageDetails["text10"],
                  opration: () {
                    setState(() {
                      back(width: wdgtWidth);
                    });
                  },
                ),
                button(
                  // node: next7,
                  label: pageDetails["text37"],
                  opration: () {
                    screen7Submit();
                    // if (firstname.text.isEmpty ||
                    //     lastname.text.isEmpty ||
                    //     phone.text.isEmpty ||
                    //     address.text.isEmpty ||
                    //     dob.text.isEmpty) {
                    //   if (firstname.text.length < 1) {
                    //     setState(() {
                    //       firstnameError = pageDetails["text46"];
                    //     });
                    //   }
                    //   if (lastname.text.length < 1) {
                    //     setState(() {
                    //       lastnameError = pageDetails["text48"];
                    //     });
                    //   }
                    //   if (phone.text.length < 1) {
                    //     setState(() {
                    //       phoneError = pageDetails["text51"];
                    //     });
                    //   }
                    //   if (address.text.length < 1) {
                    //     setState(() {
                    //       addressError = pageDetails["text53"];
                    //     });
                    //   }
                    //   if (dob.text.length < 1) {
                    //     setState(() {
                    //       dobError = pageDetails["text55"];
                    //     });
                    //   }
                    // } else {
                    //   if (firstname.text == lastname.text) {
                    //     setState(() {
                    //       lastnameError = pageDetails["text56"];
                    //     });
                    //   } else {
                    //     // signup();
                    //     triger();
                    //   }
                    // }
                  },
                ),
              ],
            ),
            // SizedBox(height: 30),
          ],
        ),
      ),
    );
  }

  next({double width}) {
    switch (pageidx) {
      case 0:
        pageidx += 1;

        break;
      case 1:
        if (widget.sponsor.text != null && widget.sponsor.text != '')
          checSponsor(widget.sponsor.text);
        pageidx += 1;
        break;
      case 2:
        checSponsor(widget.sponsor.text);
        if (sponsorError == null) {
          pageidx += 1;
        }
        break;
      case 3:
        // if (username.text != null && username.text != '')
        //   checkPassword(username.text);
        pageidx += 1;

        break;
      case 4:
        pageidx += 1;
        break;
      case 5:
        pageidx += 1;
        break;
      default:
    }
  }

  back({double width, int index}) {
    switch (pageidx) {
      case 0:
        pageidx -= 1;
        break;
      case 1:
        pageidx -= 1;
        break;
      case 2:
        sponsorError = null;
        pageidx -= 1;

        break;
      case 3:
        sponsorError = null;
        userError = null;
        passwordError = null;
        confirmError = null;
        pageidx -= 1;
        break;
      case 4:
        pageidx -= 1;
        break;
      case 5:
        firstnameError = null;
        lastnameError = null;
        phoneError = null;
        addressError = null;
        dobError = null;
        pageidx -= 1;
        break;
      default:
    }
  }

  checSponsor(value) {
    // sponsorError = null;
    if (value.length < 6 && value.length > 0) {
      sponsorError = pageDetails["text57"];
    } else {
      if (value.trim().isEmpty || value == '') {
        // sponsorError = "Sponsor Required";
        sponsorError = null;
        value = appSettings['ROOT'];
      }
      output(value) {
        Map<String, dynamic> _map;
        if (value == pageDetails["text58"]) {
          setState(() {
            if (value.trim().isEmpty || value == '') {
              // sponsorError = "Sponsor Required";
              sponsorError = null;
              value = appSettings['ROOT'];
            } else {
              sponsorError = pageDetails["text58"];
            }
          });
        } else {
          _map = Utils.fromJSONString(value);
          sponsorId = _map['sponsor_id'];
          setState(() {
            sponsorError = null;
          });
        }
      }

      SampleClass().checksponsor(value, (output));
    }
  }

  checkUsername(value) {
    if (value.trim().isEmpty) {
      userError = pageDetails["text30"];
    } else {
      output(value) {
        print(value);
        if (value.toString().toLowerCase() == "usernameexists") {
          setState(() {
            userError = pageDetails["text59"];
          });
        } else if (value == "The username must have minimum 6 characters") {
          setState(() {
            userError = pageDetails["text26"];
          });
        } else if (value ==
            "The username must not contain special characters") {
          setState(() {
            userError = 'Special Characters Not Allowed';
          });
        } else if (value == "The username must not start with digits") {
          setState(() {
            userError = 'Can\'t Start Digit';
          });
        } else {
          setState(() {
            userError = null;
          });
        }
      }

      SampleClass().checkusername(value, (output));
    }
  }

  checkPassword(value) {
    if (value.trim().isEmpty) {
      print('111111');
      passwordError = pageDetails["text31"];
    } else {
      print('333333');
      output(value) {
        print(value);
        if (value == 'The password must be 8 character') {
          setState(() {
            passwordError = pageDetails["text60"];
          });
        } else if (value == 'The password must have a special character') {
          setState(() {
            passwordError = pageDetails["text61"];
          });
        } else if (value == 'The password must have a upper case') {
          setState(() {
            passwordError = pageDetails["text63"];
          });
        } else if (value == 'The password must have a digit') {
          setState(() {
            passwordError = pageDetails["text62"];
          });
        } else if (value == 'The password must have a lower case') {
          setState(() {
            passwordError = pageDetails["text64"];
          });
        } else if (value == '') {
          setState(() {
            passwordError = null;
          });
        } else {
          setState(() {
            passwordError = value;
          });
        }
      }

      SampleClass().checkpassword(value, (output));
    }
    if (passwordconfirm.text.isEmpty) {
    } else {
      if (value == passwordconfirm.text) {
        confirmError = null;
      } else if (value != passwordconfirm.text) {
        confirmError = pageDetails["text65"];
      }
    }
  }

  checkReTypePassword(value) {
    if (value.trim().isEmpty) {
      confirmError = pageDetails["text32"];
    } else if (password.text != passwordconfirm.text) {
      confirmError = pageDetails["text65"];
    } else {
      confirmError = null;
    }
  }

  checkEmail(value) {
    if (value.trim().isEmpty) {
      emailError = pageDetails["text38"];
    } else if (!RegExp(
            r"^[a-zA-Z-0-9.a-zA-Z0-9.!#$%&'*+/=?^_`{|}~]+@[a-zA-Z-0-9]+\.[a-zA-Z]+")
        .hasMatch(value)) {
      emailError = pageDetails["text66"];
    } else {
      output(value) {
        if (value == 'true') {
          setState(() {
            emailError = null;
          });
        } else {
          setState(() {
            emailError = pageDetails['text68'];
          });
        }
        // if (value.toString() == "Email Already Exists".toString()) {
        //
        //   setState(() {
        //     emailError = pageDetails["text68"];
        //
        //   });
        // } else {
        //   setState(() {
        //     emailError = null;
        //   });
        // }
      }

      SampleClass().checkemail(value, (output));
    }
  }

  checkCompany(value) {
    if (value.trim().isEmpty) {
      setState(() {
        companyError = pageDetails["text43"];
      });
    } else {
      setState(() {
        companyError = null;
      });
    }
  }

  checkCompanyDomain(value) {
    String regex =
        "^((?!-)[A-Za-z0-9-]" + "{1,63}(?<!-)\\.)" + "+[A-Za-z]{2,6}";
    if (value.trim().isEmpty) {
      setState(() {
        domainError = pageDetails["text43"];
      });
    } else if (!RegExp(regex).hasMatch(value)) {
      domainError = pageDetails["text33"];
    } else {
      setState(() {
        domainError = null;
      });
    }
  }

  void signup() async {
    String countryCode = "";
    for (var _country in dbCountryList) {
      if (_country["country_name"] == country) {
        countryCode = _country["country_id"];
      }
    }

    if (companyName.text == "") {
      companyName.text = "null";
    }

    Map<String, dynamic> details = {
      "sponsor": sponsorId,
      "account_type": accountHoldingType,
      "username": username.text.toString(),
      "password": password.text.toString(),
      "email": email.text.toString(),
      "country": countryCode.toString(),
      "product_id": 1,
      "company_name": companyName.text.toString(),
      "user_type": accountType
    };
    Map<String, dynamic> result =
        await HttpRequest.Post('signup', Utils.constructPayload(details));
    if (Utils.isServerError(result)) {
      showDialog(context: context, builder: (_) => accountcancelled());
    } else {
      int accid = result['response']['data']['account_id'];
      if (accountHoldingType == 1) {
        var map = {
          "profile_id": 0,
          "account_id": accid,
          "screen_name": username.text,
          "profile_image": 'null',
          "first_name": firstname.text,
          "last_name": lastname.text,
          "date_of_birth": dob.text,
          "gender": 'null',
          "phone_no": phone.text,
          "address_line1": address.text,
          "address_line2": 'null',
          "nationality": country,
          "country": country,
          "region": 'null',
          "city": 'null',
          "postal_code": 'null',
          "account_number": "null",
          "beneficiary": "null",
          "bank_name": "null",
          "bank_identification_code": "null",
        };
        cart.profiledetails(map);
      }

      Navigator.push(
        context,
        MaterialPageRoute(builder: (context) => Login()),
      );
      showDialog(context: context, builder: (_) => accountsuccess());
      // Utils.toJSONString(
      //     await Utils.convertMessage(result['response']['data']));
    }
  }
}
