import 'package:centurion/common.dart';
import 'package:centurion/config.dart';
import 'package:centurion/config/app_settings.dart';
import 'package:centurion/login.dart';
import 'package:centurion/services/communication/http/index.dart';

import 'package:centurion/utils/utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:shared_preferences/shared_preferences.dart';
// import 'package:shared_preferences/shared_preferences.dart';
import 'data.dart';

class NewPassword extends StatefulWidget {
  final String userid;

  const NewPassword({Key key, this.userid}) : super(key: key);
  @override
  _NewPasswordState createState() => _NewPasswordState();
}

class _NewPasswordState extends State<NewPassword> {
  //variables

  Map<String, dynamic> pageDetails = {};
  bool isMobile = false;
  bool isipad = false;
  Future<String> _temp;
  var errorTxt = MyText(text: "fbwe");
  bool secureText1 = true;
  bool secureText2 = true;
  var visiblity = Icon(Icons.visibility_off);
  TextEditingController newpasscon = TextEditingController();
  TextEditingController cnfrmpasscon = TextEditingController();
  final _formKey = GlobalKey<FormState>();
  String newError;
  String confError;
  double width;
  String error;
  int userid;
  String expiretoken;

  // functions
  Future<String> validatetoken() async {
    Map<String, dynamic> _map = {
      "link_expired": expiretoken,
    };
    var _result = await HttpRequest.Post(
        'check_token_valid', Utils.constructPayload(_map));
    print('_MAP: $_map');
    print(_result);
    if (Utils.isServerError(_result)) {
      // snack.snack(title: 'This is Has Been Expired');
      error = _result['response']['error'].toString();
      throw (await Utils.getMessage(_result['response']['error']));
    } else {
      return 'start';
    }
  }

  void setPageDetails() async {
    String data = await Utils.getPageDetails('settings');
    setState(() {
      pageDetails = Utils.fromJSONString(data);
    });
  }

  @override
  void initState() {
    setPageDetails();
    super.initState();
    userid = int.parse(widget.userid.toString().split('_')[0]);
    print('USERID: $userid');
    expiretoken = widget.userid.toString().split('_')[1].toString();
    print('EXPIRETOKEN: $expiretoken');
    _temp = validatetoken();
  }

  @override
  Widget build(BuildContext context) {
    width = MediaQuery.of(context).size.width;

    return Scaffold(
      body: FutureBuilder(
        future: _temp,
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            return Scaffold(
              backgroundColor: Colors.blue[900],
              // isMobile == true ? Colors.white : Colors.blue[900],
              body: LayoutBuilder(
                builder: (context, constraints) {
                  if (constraints.maxWidth < 800) {
                    isMobile = true;
                    if (constraints.maxHeight < 595) {
                      return SingleChildScrollView(
                        child: passwordchange(),
                      );
                    } else {
                      return passwordchange();
                    }
                  } else {
                    isMobile = false;
                    return passwordchange();
                  }
                },
              ),
            );
          } else if (snapshot.hasError) {
            return Scaffold(
              backgroundColor: Colors.white,
              body: error == 'link_expired'
                  ? Container(
                      child: Center(
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          children: [
                            Container(
                              padding: EdgeInsets.all(7),
                              decoration: BoxDecoration(
                                shape: BoxShape.circle,
                                color: Colors.red,
                              ),
                              child: Icon(
                                Icons.close,
                                size: 40,
                                color: Colors.white,
                              ),
                            ),
                            SizedBox(
                              height: 10,
                            ),
                            Text(
                              'Link Expired Please Contact the Admin',
                              style: TextStyle(
                                fontSize: Pallet.heading2,
                                fontWeight: Pallet.font600,
                                color: Pallet.fontcolornew,
                              ),
                            ),
                            SizedBox(
                              height: 20,
                            ),
                            ElevatedButton(
                              onPressed: () async {
                                SharedPreferences prefs =
                                    await SharedPreferences.getInstance();
                                prefs.remove("shopbannershow");
                                prefs.remove("newshow");
                                prefs.remove("reload");
                                prefs.remove("sessiontime");
                                Navigator.pushReplacementNamed(context, '/');
                              },
                              child: Text(
                                'Go Back',
                              ),
                            ),
                          ],
                        ),
                      ),
                    )
                  : SomethingWentWrongMessage(),
            );
          } else {
            return Loader();
          }
        },
      ),
    );
  }

  passwordchange() {
    return Container(
      child: isMobile == false
          ? Stack(
              children: [
                isMobile == false
                    ? Container(
                        alignment: Alignment.centerLeft,
                        width: MediaQuery.of(context).size.height * 0.5,
                        child: Image.asset("centurion-logo.png"),
                      )
                    : Container(),
                centercard(),
              ],
            )
          : Stack(
              children: [
                Container(
                  alignment: Alignment.topCenter,
                  child: Image.asset(
                    "centurion-logo.png",
                    height: MediaQuery.of(context).size.height * 0.3,
                  ),
                ),
                // : Container(),
                centercard(),
              ],
            ),
    );
  }

  Widget centercard() {
    return Center(
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Container(
            padding: EdgeInsets.all(20),
            margin: EdgeInsets.all(20),
            width: isMobile == true ? MediaQuery.of(context).size.width : 500,
            // height:
            //     isMobile == true ? MediaQuery.of(context).size.width : 500,
            decoration: BoxDecoration(
              color: Colors.white,
              border: Border.all(
                color: Colors.black,
                width: 2,
              ),
              borderRadius: BorderRadius.circular(12),
            ),
            child: Form(
              key: _formKey,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  // if (isMobile == true)
                  //   Container(
                  //     alignment: Alignment.centerLeft,
                  //     width: width > 600 ? 120 : 70,
                  //     height: width > 600 ? 120 : 70,
                  //     child: Image.asset("centurion-logo.png"),
                  //   ),
                  isMobile == true
                      ? Container(
                          child: Image(
                          image: NetworkImage(appSettings['SERVER_URL'] +
                              '/' +
                              'assets/mobile_logo.png'),
                        ))
                      : Container(),
                  if (isMobile == true)
                    SizedBox(
                      height: 20,
                    ),
                  Row(
                    children: [
                      Expanded(
                        child: Text(
                          "Reset Account Password",
                          style: TextStyle(
                            fontSize:
                                width > 600 ? Pallet.heading1 : Pallet.heading2,
                            fontWeight: Pallet.font600,
                          ),
                        ),
                      ),
                    ],
                  ),
                  Padding(
                    padding: const EdgeInsets.fromLTRB(0, 20, 0, 20),
                    child: Container(
                        alignment: Alignment.centerLeft,
                        child: Text(
                          "Enter New Password",
                          style: TextStyle(
                            fontSize:
                                width > 600 ? Pallet.heading3 : Pallet.heading4,
                            fontWeight: Pallet.font600,
                          ),
                        )),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Expanded(
                        flex: 3,
                        child: Center(
                          child: Text(
                            "New Password",
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                        ),
                      ),
                      SizedBox(
                        width: 5,
                      ),
                      Expanded(
                        flex: 7,
                        child: Center(
                          child: Container(
                            // width: 300,
                            child: TextFormField(
                              onEditingComplete: () {
                                changepass();
                              },
                              // ignore: missing_return
                              validator: (value) {
                                setState(() {
                                  checkPassword(value);
                                });
                              },
                              onChanged: (value) {
                                setState(() {
                                  checkPassword(value);
                                });
                              },

                              style: TextStyle(color: Pallet.dashcontainerback),
                              controller: newpasscon,
                              cursorColor: Pallet.fontcolornew,
                              inputFormatters: [
                                FilteringTextInputFormatter.deny(RegExp(' '))
                              ],
                              decoration: InputDecoration(
                                errorText: newError,
                                contentPadding:
                                    EdgeInsets.fromLTRB(10.0, 10.0, 20.0, 10.0),
                                suffixIcon: IconButton(
                                  color: Pallet.dashcontainerback,
                                  icon: Icon(secureText1
                                      ? Icons.visibility_off
                                      : Icons.visibility),
                                  onPressed: () {
                                    setState(() {
                                      secureText1 = !secureText1;
                                    });
                                  },
                                ),
                                filled: true,
                                errorBorder: OutlineInputBorder(
                                    borderSide: const BorderSide(
                                        color: Colors.red, width: 2),
                                    borderRadius: BorderRadius.circular(8.0)),
                                border: OutlineInputBorder(
                                    borderSide: const BorderSide(
                                        color: Color(0XFF1034a6), width: 1.0),
                                    borderRadius: BorderRadius.circular(8.0)),
                                fillColor: Pallet.fontcolor,
                                focusedBorder: OutlineInputBorder(
                                    borderSide: const BorderSide(
                                        color: Color(0XFF1034a6), width: 1.0),
                                    borderRadius: BorderRadius.circular(8.0)),
                                enabledBorder: OutlineInputBorder(
                                  borderSide: BorderSide(
                                      color: Color(0xFF4570ae), width: 0.0),
                                  borderRadius: BorderRadius.circular(8.0),
                                ),
                              ),
                              obscureText: secureText1,
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Expanded(
                        flex: 3,
                        child: Center(
                          child: Text(
                            "Confirm Password",
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                        ),
                      ),
                      SizedBox(
                        width: 5,
                      ),
                      Expanded(
                        flex: 7,
                        child: Center(
                          child: Container(
                            // width: 300,
                            child: TextFormField(
                              onFieldSubmitted: (value) {
                                setState(() {
                                  checkReTypePassword(value);
                                });
                              },

                              // ignore: missing_return
                              validator: (value) {
                                setState(() {
                                  checkReTypePassword(value);
                                });
                              },
                              onChanged: (value) {
                                setState(() {
                                  checkReTypePassword(value);
                                });
                              },
                              style: TextStyle(color: Pallet.dashcontainerback),
                              controller: cnfrmpasscon,
                              cursorColor: Pallet.fontcolornew,
                              inputFormatters: [
                                FilteringTextInputFormatter.deny(RegExp(' '))
                              ],
                              decoration: InputDecoration(
                                errorText: confError,
                                contentPadding:
                                    EdgeInsets.fromLTRB(10.0, 10.0, 20.0, 10.0),
                                suffixIcon: IconButton(
                                  color: Pallet.dashcontainerback,
                                  icon: secureText2 == true
                                      ? Icon(Icons.visibility_off)
                                      : Icon(Icons.visibility),
                                  onPressed: () {
                                    setState(() {
                                      secureText2 = !secureText2;
                                    });
                                  },
                                ),
                                filled: true,
                                errorBorder: OutlineInputBorder(
                                    borderSide: const BorderSide(
                                        color: Colors.red, width: 2),
                                    borderRadius: BorderRadius.circular(8.0)),
                                border: OutlineInputBorder(
                                    borderSide: const BorderSide(
                                        color: Color(0XFF1034a6), width: 1.0),
                                    borderRadius: BorderRadius.circular(8.0)),
                                fillColor: Pallet.fontcolor,
                                focusedBorder: OutlineInputBorder(
                                    borderSide: const BorderSide(
                                        color: Color(0XFF1034a6), width: 1.0),
                                    borderRadius: BorderRadius.circular(8.0)),
                                enabledBorder: OutlineInputBorder(
                                  borderSide: BorderSide(
                                      color: Color(0xFF4570ae), width: 0.0),
                                  borderRadius: BorderRadius.circular(8.0),
                                ),
                              ),
                              obscureText: secureText2,
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),

                  SizedBox(
                    height: 20,
                  ),
                  Row(
                    mainAxisAlignment: isMobile == true
                        ? MainAxisAlignment.center
                        : MainAxisAlignment.end,
                    children: [
                      Container(
                        child: Row(
                          mainAxisSize: MainAxisSize.min,
                          children: [
                            ElevatedButton(
                              onPressed: () {
                                print("password updated");
                                Navigator.pushReplacementNamed(context, '/');
                              },
                              child: Text(
                                "Cancel",
                              ),
                            ),
                            SizedBox(
                              width: 20,
                            ),
                            ElevatedButton(
                              onPressed: () {
                                changepass();
                              },
                              child: Text(
                                pageDetails["text28"],
                              ),
                            ),
                          ],
                        ),
                      ),
                      SizedBox(
                        width: 20,
                      ),
                    ],
                  )
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  changepass() async {
    print(",mnbvgfgyhjskl,mnb");
    print(newpasscon.text);

    print(cnfrmpasscon.text);
    print(",msnhgssssssssssssssssssss");

    if (newpasscon.text.isEmpty ||
        newpasscon.text == null ||
        newpasscon.text == '') {
      setState(() {
        newError = pageDetails['text21'];
      });
    }
    if (cnfrmpasscon.text.isEmpty ||
        cnfrmpasscon.text == null ||
        cnfrmpasscon.text == '') {
      setState(() {
        confError = pageDetails['text25'];
      });
    } else if (newpasscon.text == cnfrmpasscon.text) {
      newError = null;
      confError = null;

      waiting.waitpopup(context);
      Map<String, dynamic> _map = {
        "account_id": userid,
        "link_code": expiretoken,
        'password': cnfrmpasscon.text,
      };
      var _result = await HttpRequest.Post(
          'link_resetpassword', Utils.constructPayload(_map));
      // print("zzzzzzzzzzzzzzzzzzzzzzzzzzzz");
      print(_result);
      if (Utils.isServerError(_result)) {
        snack.snack(title: 'Password Reset Failed');
      } else {
        custompopup.showdialog(
          context: context,
          popupwidth: 200,
          barrierDismissible: false,
          title: 'Success',
          content: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisSize: MainAxisSize.min,
            children: [
              CircleAvatar(
                  backgroundColor: Pallet.fontcolor,
                  radius: 40,
                  child: Icon(
                    Icons.done_outline_rounded,
                    color: Pallet.inner1,
                    size: 30,
                  )),
              SizedBox(
                height: 10,
              ),
              Text(
                'Password Changed Successfully',
                style: TextStyle(
                  color: Pallet.fontcolor,
                  fontSize: Pallet.heading5,
                ),
              ),
            ],
          ),
          actions: [
            PopupButton(
              text: 'Continue',
              onpress: () async {
                Navigator.pushReplacementNamed(context, '/');
              },
            ),
          ],
        );
      }
    }
  }

  checkPassword(value) {
    if (value.trim().isEmpty) {
      print('111111');
      passwordError = pageDetails["text21"];
    } else {
      print('333333');
      output(value) {
        print(value);
        if (value == 'The password must be 8 character') {
          setState(() {
            newError = pageDetails["text22"];
          });
        } else if (value == 'The password must have a special character') {
          setState(() {
            newError = pageDetails["text31"];
          });
        } else if (value == 'The password must have a upper case') {
          setState(() {
            newError = pageDetails["text33"];
          });
        } else if (value == 'The password must have a digit') {
          setState(() {
            newError = pageDetails["text32"];
          });
        } else if (value == 'The password must have a lower case') {
          setState(() {
            newError = pageDetails["text34"];
          });
        } else {
          setState(() {
            newError = null;
          });
        }
      }

      SampleClass().checkpassword(value, (output));
    }
    if (cnfrmpasscon.text.isEmpty) {
    } else {
      if (value == cnfrmpasscon.text) {
        confError = null;
      } else if (value != cnfrmpasscon.text) {
        confError = pageDetails["text26"];
      }
    }
  }

  checkReTypePassword(value) {
    if (value.trim().isEmpty) {
      confError = pageDetails['text25'];
    } else if (newpasscon.text != cnfrmpasscon.text) {
      confError = pageDetails['text37'];
    } else {
      confError = null;
    }
  }
}
