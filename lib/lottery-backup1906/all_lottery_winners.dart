import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

import '../common.dart';
import '../services/communication/http/index.dart';
import '../utils/utils.dart';
import 'config.dart';

class AllLotteryHistory extends StatefulWidget {
  final double wdgtWidth, wdgtHeight;

  const AllLotteryHistory({Key key, this.wdgtWidth, this.wdgtHeight})
      : super(key: key);
  @override
  _AllLotteryHistoryState createState() => _AllLotteryHistoryState();
}

class _AllLotteryHistoryState extends State<AllLotteryHistory> {
  bool isMobile = false;

  int selectedIndex;
  Map<String, dynamic> map = {
    'product_id': 1,
  };
  List data = [];
  List unProcessed = [];
  Future<String> temp;
  void initState() {
    temp = lotteryHistory(map);
    super.initState();
  }

  Future<String> lotteryHistory(Map map) async {
    Map<String, dynamic> result = await HttpRequest.Post(
        'lottery_all_history', Utils.constructPayload(map));
    if (Utils.isServerError(result)) {
      print('wowowowowwowowowwowow');
      print(result);
      print("LLLLLLLLAAAAAAAAAAAAAA");
      return throw (await Utils.getMessage(result['response']['error']));
    } else {
      print('fff');

      data = result['response']['data'];
      print('fffzzzzzzzzzzzzzzzzzz');

      return 'starrt';
    }
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<Object>(
        future: temp,
        builder: (context, snapshot) {
          if (snapshot.hasError) {
            return SomethingWentWrongMessage();
          } else if (snapshot.hasData) {
            return LayoutBuilder(builder: (context, constraints) {
              if (constraints.maxWidth <= 600) {
                isMobile = true;
                return Padding(
                  padding: EdgeInsets.all(Pallet1.defaultPadding),
                  child: historyList(widget.wdgtWidth),
                );
              } else if (constraints.maxWidth > 600 &&
                  constraints.maxWidth <= 950) {
                isMobile = true;

                return Padding(
                  padding: EdgeInsets.only(left: Pallet1.leftPadding),
                  child: historyList(widget.wdgtWidth * .8),
                );
              } else if (constraints.maxWidth > 950 &&
                  constraints.maxWidth <= 1200) {
                isMobile = false;

                return Padding(
                  padding: EdgeInsets.only(left: Pallet1.leftPadding),
                  child: historyList(widget.wdgtWidth * .7),
                );
              } else {
                isMobile = false;

                return Padding(
                  padding: EdgeInsets.all(Pallet1.leftPadding),
                  child: historyList(widget.wdgtWidth * .7),
                );
              }
            });
          } else {
            return Loader();
          }
        });
  }

  Widget historyList(wdgtWidth) {
    // var formatter = new DateFormat('dd-MM-yyyy');
    // String formattedDate;

    return Container(
      width: wdgtWidth,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            children: [
              Text('Lottery Winners History',
                  style: TextStyle(
                      color: Pallet1.fontcolornew,
                      fontSize: Pallet1.heading1,
                      fontWeight: Pallet1.font600)),
            ],
          ),
          SizedBox(
            height: 20,
          ),
          Container(
            padding: EdgeInsets.only(left: Pallet1.defaultPadding),
            child: Wrap(children: [
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 3),
                child: Row(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    SizedBox(
                      width: 10,
                      height: 10,
                      child: Container(
                        color: Colors.grey,
                      ),
                    ),
                    SizedBox(width: 5),
                    Text("Super",
                        style: TextStyle(color: Pallet1.fontcolornew)),
                  ],
                ),
              ),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 3),
                child: Row(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    SizedBox(
                      width: 10,
                      height: 10,
                      child: Container(
                        color: Colors.amber[900],
                      ),
                    ),
                    SizedBox(width: 5),
                    Text("Jackpot",
                        style: TextStyle(color: Pallet1.fontcolornew)),
                  ],
                ),
              ),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 3),
                child: Row(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    SizedBox(
                      width: 10,
                      height: 10,
                      child: Container(
                        color: Colors.red,
                      ),
                    ),
                    SizedBox(width: 5),
                    Text("Super Jackpot",
                        style: TextStyle(color: Pallet1.fontcolornew)),
                  ],
                ),
              ),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 3),
                child: Row(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    SizedBox(
                      width: 10,
                      height: 10,
                      child: Container(
                        color: Colors.pink[300],
                      ),
                    ),
                    SizedBox(width: 5),
                    Text("Grand Prize",
                        style: TextStyle(color: Pallet1.fontcolornew)),
                  ],
                ),
              ),
            ]),
          ),
          SizedBox(
            height: 20,
          ),
          data.length != 0
              ? Expanded(
                  child: ListView.builder(
                      itemCount: data.length,
                      itemBuilder: (context, index) {
                        var time =
                            data[index]['created_at'].toString().split('T');

                        var a = time[1].toString().split('.');
                        return Padding(
                          padding: EdgeInsets.all(Pallet1.defaultPadding),
                          child: Container(
                            padding: EdgeInsets.all(Pallet1.defaultPadding),
                            decoration: BoxDecoration(
                                boxShadow: [
                                  BoxShadow(
                                    color: Colors.black12,
                                    spreadRadius: 5,
                                    blurRadius: 5,
                                  )
                                ],
                                color: Colors.white,
                                borderRadius: BorderRadius.circular(10)),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Expanded(
                                  child: Center(
                                    child: Text(time[0] + ' ' + a[0].toString(),
                                        style: TextStyle(
                                          color: Pallet1.fontcolornew,
                                          fontSize: Pallet1.normalfont,
                                        )),
                                  ),
                                ),
                                Expanded(
                                  child: Center(
                                    child: Text(
                                        'LOT' +
                                            data[index]['lottery_id']
                                                .toString(),
                                        style: TextStyle(
                                          color: Pallet1.fontcolornew,
                                          fontSize: Pallet1.normalfont,
                                        )),
                                  ),
                                ),
                                Expanded(
                                  child: Center(
                                    child: Text(
                                        data[index]['screen_name'].toString(),
                                        // overflow: TextOverflow.ellipsis,
                                        style: TextStyle(
                                          color: Pallet1.fontcolornew,
                                          fontSize: Pallet1.normalfont,
                                        )),
                                  ),
                                ),
                                Expanded(
                                  child: Center(
                                    child: Text(
                                        data[index]['particulars'] == 'Dollar'
                                            ? ('\$ ' +
                                                    double.parse(data[index]
                                                                ['amount']
                                                            .toString())
                                                        .toStringAsFixed(2))
                                                .toString()
                                            : ((double.parse(data[index]
                                                                ['amount']
                                                            .toString()))
                                                        .toStringAsFixed(2) +
                                                    ' DUC')
                                                .toString(),
                                        style: TextStyle(
                                          color: data[index]['winner_type'] ==
                                                  'normal'
                                              ? Colors.grey
                                              : data[index]['winner_type'] ==
                                                      'special'
                                                  ? Colors.amber[900]
                                                  : data[index]
                                                              ['winner_type'] ==
                                                          'weekly'
                                                      ? Colors.pink[300]
                                                      : Colors.red,
                                          fontSize: Pallet1.normalfont,
                                          fontWeight: FontWeight.w700,
                                        )),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        );
                      }),
                )
              : Expanded(
                  child: Center(
                    child: Text("No Recent Winners Found",
                        style: TextStyle(
                          color: Pallet1.fontcolor,
                          fontSize: Pallet1.normalfont,
                        )),
                  ),
                ),
        ],
      ),
    );
  }

  Row rowweeklyProcess(item) {
    return Row(
      children: [
        Text('Weekly Draw : ',
            style: TextStyle(
                color: Pallet1.fontcolornew, fontSize: Pallet1.heading4)),
        SizedBox(width: 5),
        item['weekly_processed'] == true
            ? Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(Pallet1.radius),
                  color: Pallet1.fontcolornew,
                ),
                child: Icon(
                  Icons.check,
                  size: 10,
                  color: Pallet1.fontcolor,
                ))
            : Text('--')
      ],
    );
  }

  Row rowDailyProcess(item) {
    return Row(
      children: [
        Text('Daily Draw : ',
            style: TextStyle(
                color: Pallet1.fontcolornew, fontSize: Pallet1.heading4)),
        SizedBox(width: 5),
        item['daily_processed'] == true
            ? Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(Pallet1.radius),
                  color: Pallet1.fontcolornew,
                ),
                child: Icon(
                  Icons.check,
                  size: 15,
                  color: Pallet1.fontcolor,
                ))
            : Text('--')
      ],
    );
  }
}

class jackpotPercent extends StatelessWidget {
  const jackpotPercent({
    Key key,
    @required this.data,
    @required this.i,
  }) : super(key: key);

  final List data;
  final int i;

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.end,
      children: [
        Text('Jackpot Percentage'.toString(),
            style: TextStyle(
                color: Pallet1.fontcolornew, fontSize: Pallet1.heading2)),
        Text(data[i]['jackpot_percentage'].toString() + ' %',
            style: TextStyle(
                color: Pallet1.fontcolornew, fontSize: Pallet1.heading4)),
      ],
    );
  }
}

class luckyNumbers extends StatelessWidget {
  const luckyNumbers({
    Key key,
    @required this.items,
  }) : super(key: key);

  final items;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.all(Pallet1.defaultPadding / 2),
      child: Container(
        decoration: BoxDecoration(
            color: ([0, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89]
                        .contains(int.parse(items))) ==
                    true
                ? Colors.red
                : Colors.green,
            // color:
            //     ([0, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89].contains(items)) == true
            //         ? Colors.red
            //         :
            //         Colors.green,
            borderRadius: BorderRadius.circular(Pallet1.radius)),
        child: Padding(
          padding: EdgeInsets.all(Pallet1.defaultPadding),
          child: Text(items.toString(),
              style: TextStyle(
                  color: Pallet1.fontcolor, fontSize: Pallet1.heading4)),
        ),
      ),
    );
  }
}

class textCreatedat extends StatelessWidget {
  const textCreatedat({
    Key key,
    @required this.item,
  }) : super(key: key);

  final item;

  @override
  Widget build(BuildContext context) {
    return Text(item['created_at'].toString(),
        style:
            TextStyle(color: Pallet1.fontcolornew, fontSize: Pallet1.heading4));
  }
}

class unprocessedTickets extends StatelessWidget {
  const unprocessedTickets({
    Key key,
    @required this.item,
  }) : super(key: key);

  final item;

  @override
  Widget build(BuildContext context) {
    return Container(
      // color: Colors.black,
      child: Wrap(
        children: [
          for (var number in item['numbers'])
            Padding(
              padding: EdgeInsets.all(Pallet1.defaultPadding),
              child: Container(
                decoration: BoxDecoration(
                    color: ([0, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89]
                                .contains(number)) ==
                            true
                        ? Colors.red
                        : Pallet1.fontcolornew,
                    borderRadius: BorderRadius.circular(Pallet1.radius)),
                child: Padding(
                  padding: EdgeInsets.all(Pallet1.defaultPadding),
                  child: Text(
                    number.toString(),
                    style: TextStyle(
                        color: Pallet1.fontcolor, fontSize: Pallet1.heading4),
                  ),
                ),
              ),
            ),
        ],
      ),
    );
  }
}

class wrappp extends StatelessWidget {
  const wrappp({
    Key key,
    @required this.item,
  }) : super(key: key);

  final item;

  @override
  Widget build(BuildContext context) {
    return Container(
      // color: Colors.black,
      child: Wrap(
        children: [
          for (var number in item['numbers'])
            Padding(
              padding: EdgeInsets.all(Pallet1.defaultPadding),
              child: Container(
                decoration: BoxDecoration(
                    color: ([0, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89]
                                .contains(number)) ==
                            true
                        ? Colors.red
                        : Pallet1.fontcolornew,
                    borderRadius: BorderRadius.circular(Pallet1.radius)),
                child: Padding(
                  padding: EdgeInsets.all(Pallet1.defaultPadding),
                  child: Text(
                    number.toString(),
                    style: TextStyle(
                        color: Pallet1.fontcolor, fontSize: Pallet1.heading4),
                  ),
                ),
              ),
            ),
        ],
      ),
    );
  }
}
