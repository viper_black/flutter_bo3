import 'dart:async';

import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:centurion/home.dart';
import 'package:centurion/utils/utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'config.dart';

// import 'lottery.dart';
DateTime now = DateTime.now();

extension date on DateTime {
  String utcweekend({int weekday}) {
    var date = this.toUtc();
    var calsunday = weekday - date.weekday;
    var sunday = date.add(Duration(days: calsunday)).toString().split(' ');
    var value = sunday[0].split('-');
    String finalsundayval =
        value[2] + '-' + value[1] + '-' + value[0].toString();
    return finalsundayval;
  }
}

class Lottery {
  bool enabled = false;
  int selectedindex;
  var value;
  lotterypopup(context) async {
    var isEnable = await Utils.getSharedPrefereces('showlotterypopup');

    List serverval = [
      {'amt': '5', 'special': '1', 'normal': '8'},
      {'amt': '10', 'special': '2', 'normal': '13'}
    ];
    showDialog(
        context: context,
        builder: (BuildContext context) => StatefulBuilder(
              builder: (BuildContext context,
                      void Function(void Function()) setState1) =>
                  AlertDialog(
                backgroundColor: Pallet1.fontcolor,
                content: Container(
                  // decoration: BoxDecoration(
                  //   image: DecorationImage(
                  //     image: AssetImage("c-logo.png"),
                  //     fit: BoxFit.fill,
                  //   ),
                  // ),
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      Text(
                        "Select The Lottery Ticket Amount",
                        style: TextStyle(
                          color: Pallet1.fontcolornew,
                          fontSize: Pallet1.heading3,
                          fontWeight: Pallet1.font600,
                        ),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Wrap(
                        // spacing: 5,
                        runSpacing: 5,
                        children: [
                          for (var i = 0; i < serverval.length; i++)
                            selectedindex != i
                                ? InkWell(
                                    onTap: () async {
                                      setState1(() {
                                        value = serverval[i]['amt'];
                                        selectedindex = i;
                                        print(selectedindex);
                                      });

                                      // confirmm(context);

                                      if (await Utils.getSharedPrefereces(
                                              'showlotterypopup') !=
                                          'true') {
                                        confirmm(context, setState1);
                                      }
                                    },
                                    child: Padding(
                                      padding: const EdgeInsets.symmetric(
                                          horizontal: 5),
                                      child: Container(
                                          padding: EdgeInsets.all(10),
                                          decoration: BoxDecoration(
                                              borderRadius:
                                                  BorderRadius.circular(5),
                                              color: Colors.white,
                                              border: Border.all(
                                                color: Pallet1.fontcolornew,
                                              )),
                                          child: Column(
                                            mainAxisSize: MainAxisSize.min,
                                            children: [
                                              SizedBox(
                                                height: 10,
                                              ),
                                              Text(
                                                "\$ " + serverval[i]['amt'],
                                                style: TextStyle(
                                                  color: Pallet1.fontcolornew,
                                                  fontSize: Pallet1.heading1,
                                                  fontWeight: Pallet1.font600,
                                                ),
                                              ),
                                              SizedBox(
                                                height: 5,
                                              ),
                                              Text(
                                                serverval[i]['special']
                                                        .toString() +
                                                    " Special Number & " +
                                                    serverval[i]['normal']
                                                        .toString() +
                                                    " Normal Numbers",
                                                style: TextStyle(
                                                  fontSize: Pallet1.heading6,
                                                  color: Pallet1.fontcolornew,
                                                  fontWeight: Pallet1.font500,
                                                ),
                                              )
                                            ],
                                          )),
                                    ),
                                  )
                                : InkWell(
                                    onTap: () async {
                                      setState1(() {
                                        if (selectedindex == i) {
                                          value = null;
                                          selectedindex = null;
                                          print(selectedindex);
                                        }
                                      });
                                      if (await Utils.getSharedPrefereces(
                                              'showlotterypopup') !=
                                          'true') {
                                        confirmm(context, setState1);
                                      }
                                    },
                                    child: Padding(
                                      padding: const EdgeInsets.symmetric(
                                          horizontal: 5),
                                      child: Container(
                                          padding: EdgeInsets.all(10),
                                          decoration: BoxDecoration(
                                              borderRadius:
                                                  BorderRadius.circular(5),
                                              color: Pallet1.fontcolornew,
                                              border: Border.all(
                                                color: Pallet1.fontcolornew,
                                              )),
                                          child: Column(
                                            mainAxisSize: MainAxisSize.min,
                                            children: [
                                              SizedBox(
                                                height: 10,
                                              ),
                                              Text(
                                                "\$ " + serverval[i]['amt'],
                                                style: TextStyle(
                                                  color: Pallet1.fontcolor,
                                                  fontSize: Pallet1.heading1,
                                                  fontWeight: Pallet1.font600,
                                                ),
                                              ),
                                              SizedBox(
                                                height: 5,
                                              ),
                                              Text(
                                                serverval[i]['special']
                                                        .toString() +
                                                    " Special Number & " +
                                                    serverval[i]['normal']
                                                        .toString() +
                                                    " Normal Numbers",
                                                style: TextStyle(
                                                  fontSize: Pallet1.heading6,
                                                  color: Pallet1.fontcolor,
                                                  fontWeight: Pallet1.font500,
                                                ),
                                              )
                                            ],
                                          )),
                                    ),
                                  )
                        ],
                      ),
                      // SizedBox(
                      //   height: 10,
                      // ),
                      // Container(
                      //   width: 150,
                      //   child: PopupButton1(
                      //     text: 'View Prize Results',
                      //     buttoncolor: Pallet1.fontcolornew,
                      //     textcolor: Pallet1.fontcolor,
                      //     onpress: () {
                      //       Navigator.push(
                      //           context,
                      //           MaterialPageRoute(
                      //               builder: (BuildContext context) => Home(
                      //                     route: 'winner_Announcement',
                      //                   )));
                      //     },
                      //   ),
                      // ),
                    ],
                  ),
                ),
                actions: [
                  PopupButton1(
                    text: 'Close',
                    buttoncolor: Pallet1.fontcolornew,
                    textcolor: Pallet1.fontcolor,
                    onpress: () {
                      Navigator.pop(context);
                    },
                  ),
                  // SizedBox(width: 10,),
                  if (isEnable == 'true')
                    PopupButton1(
                      text: 'Continue',
                      buttoncolor: Pallet1.fontcolornew,
                      textcolor: Pallet1.fontcolor,
                      onpress: () {
                        print('RRRRRRRRRRRRRRRRRRRRRRRRRRR');
                        print(selectedindex);
                        if (selectedindex != null) {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (BuildContext context) => Home(
                                        amt: value,
                                        route: 'lottery',
                                      )));
                          selectedindex = null;
                        } else {
                          ScaffoldMessenger.of(context).hideCurrentSnackBar();
                          ScaffoldMessenger.of(context).showSnackBar(
                              SnackBar(content: Text("Please select Lottery")));
                        }

                        // Navigator.push(
                        //     context,
                        //     MaterialPageRoute(
                        //         builder: (BuildContext context) => Home(
                        //               amt: value,
                        //               route: 'lottery',
                        //             )));
                      },
                    ),
                ],
              ),
            ));
    return value;
  }

  confirmm(context, setState1) async {
    DateTime cdate = DateTime.now();
    var currentDate = await cdate.toUtc();
    var startdate =
        currentDate.toString().split(' ')[0] + " " + "12:00".toString();
    var localTime = currentDate.toLocal();
    int currentDay = currentDate.weekday;
    DateFormat formatter = DateFormat('yyyy-MM-dd');
    var formattedDate = formatter.parse(currentDate.toString());

    print('dddddddddddd');
    print(formattedDate);

    print(localTime);
    print(currentDay);
    int sunday = 7 - currentDay;
    print(sunday);
    var finaldate = formattedDate.day + sunday;

    print('ppppppp');
    print(finaldate);
    print(currentDate);
    return showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) => StatefulBuilder(
            builder: (BuildContext context,
                    void Function(void Function()) setState2) =>
                AlertDialog(
                  elevation: 24.0,
                  backgroundColor: Pallet1.popupcontainerback,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.all(Radius.circular(15.0))),
                  title: Row(
                    children: [
                      Image.asset(
                        "c-logo.png",
                      ),
                      SizedBox(width: 10),
                      Text('Confirmation',
                          style: TextStyle(
                              color: Pallet1.fontcolor,
                              fontWeight: Pallet1.font500)),
                    ],
                  ),
                  content: Container(
                    width: 455,
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        Text(
                            "Your Cash Account will be debited with \$ $value against the Lottery ticket Price. This ticket will stand eligible for the Draw dated $startdate (UTC) & also will be considered for the Weekly Draw on Sunday (${DateTime.now().utcweekend(weekday: 7)}).",
                            style: TextStyle(
                              height: 1.5,
                              color: Pallet1.fontcolor,
                              fontSize: 14,
                            )),
                        SizedBox(height: 10),
                        Row(
                          children: [
                            Checkbox(
                              overlayColor: MaterialStateColor.resolveWith(
                                  (states) => Pallet1.fontcolornew),
                              fillColor: MaterialStateColor.resolveWith(
                                  (states) => Pallet1.fontcolor),
                              hoverColor: Pallet1.fontcolor,
                              checkColor: Pallet1.fontcolornew,
                              value: enabled,
                              onChanged: (value) {
                                setState2(() {
                                  print("kkksssxxxzzz");
                                  print(value);
                                  enabled = value;
                                  print(enabled);
                                });
                              },
                            ),
                            Text('Don\'t show this again',
                                style: TextStyle(
                                  fontSize: 14,
                                  color: Pallet1.fontcolor,
                                ))
                          ],
                        ),
                      ],
                    ),
                  ),
                  actions: [
                    PopupButton1(
                      text: 'Close',
                      onpress: () {
                        setState1(() {
                          selectedindex = null;
                        });
                        print(selectedindex);
                        Navigator.of(context).pop();
                      },
                    ),
                    PopupButton1(
                      text: 'Confirm',
                      onpress: () async {
                        if (enabled == true) {
                          Utils.setSharedPrefereces('true', 'showlotterypopup');
                        }
                        if (selectedindex != null) {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (BuildContext context) => Home(
                                        amt: value,
                                        route: 'lottery',
                                      )));
                          selectedindex = null;
                        } else {
                          ScaffoldMessenger.of(context).hideCurrentSnackBar();
                          ScaffoldMessenger.of(context).showSnackBar(
                              SnackBar(content: Text("Please select Lottery")));
                        }
                      },
                    ),
                    SizedBox(height: 10),
                  ],
                )));

    // AwesomeDialog(
    //   width: 455,
    //   context: context,
    //   animType: AnimType.SCALE,
    //   dialogType: DialogType.QUESTION,
    //   body: Column(
    //     children: [
    //       Text(
    //         "Your Cash Account will be Debited with \$5 \$10 for the Ticket price.This Ticket Numbers will stand eligible for the Draw dated ________ &also be considered fro the Weekly draw on _______",
    //         style: TextStyle(fontStyle: FontStyle.italic),
    //       ),
    //       Row(
    //         children: [
    //           Checkbox(
    //             overlayColor:
    //                 MaterialStateColor.resolveWith((states) => Colors.grey),
    //             fillColor:
    //                 MaterialStateColor.resolveWith((states) => Colors.yellow),
    //             hoverColor: Colors.green,
    //             checkColor: Colors.red,
    //             value: enabled,
    //             onChanged: (value) {
    //               setState1(() {
    //                 print("kkksssxxxzzz");
    //                 print(value);
    //                 enabled = value;
    //                 print(enabled);
    //               });
    //             },
    //           ),
    //           Text('Don\'t show this again')
    //         ],
    //       ),
    //     ],
    //   ),
    //   title: 'Lottery Ticket',
    //   desc: 'Confirmed Successsfully',
    //   btnOkText: 'OK',
    //   btnCancelText: 'Cancel',
    //   btnCancelOnPress: () {
    //     selectedindex = null;
    //     print(selectedindex);
    //     Navigator.of(context).pop();
    //   },
    //   btnOkOnPress: () async {
    //     Utils.setSharedPrefereces('true', 'showlotterypopup');
    //     if (selectedindex != null) {
    //       Navigator.push(
    //           context,
    //           MaterialPageRoute(
    //               builder: (BuildContext context) => Home(
    //                     amt: value,
    //                     route: 'lottery',
    //                   )));
    //       selectedindex = null;
    //     } else {
    //       ScaffoldMessenger.of(context).hideCurrentSnackBar();
    //       ScaffoldMessenger.of(context)
    //           .showSnackBar(SnackBar(content: Text("Please select Lottery")));
    //     }
    //   },
    //   dismissOnTouchOutside: true,
    // )..show();
  }
}

class Bouncing extends StatefulWidget {
  final Widget child;
  final VoidCallback onPress;

  Bouncing({@required this.child, Key key, this.onPress})
      : assert(child != null),
        super(key: key);

  @override
  _BouncingState createState() => _BouncingState();
}

class _BouncingState extends State<Bouncing>
    with SingleTickerProviderStateMixin {
  double _scale;
  AnimationController _controller;

  @override
  void initState() {
    super.initState();
    _controller = AnimationController(
      vsync: this,
      duration: Duration(milliseconds: 100),
      lowerBound: 0.0,
      upperBound: 0.1,
    );
    _controller.addListener(() {
      setState(() {});
    });
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    _scale = 1 - _controller.value / 10;
    return Listener(
      onPointerDown: (PointerDownEvent event) {
        if (widget.onPress != null) {
          _controller.forward();
        }
      },
      onPointerUp: (PointerUpEvent event) {
        if (widget.onPress != null) {
          _controller.reverse();
          widget.onPress();
        }
      },
      child: Transform.scale(
        scale: _scale,
        child: widget.child,
      ),
    );
  }
}

class CustomButton1 extends StatelessWidget {
  final Color textcolor;
  final String text;
  final double textsize;
  final double width;
  final double height;
  final Color buttoncolor;
  final double vpadding;
  final double bradius;
  final bool bshadow;
  final double radius;
  final double sradius;
  final double hpadding;
  final Function onpress;
  final Widget child;
  final FontWeight fontweight;
  final TextDecoration textDecoration;
  const CustomButton1(
      {Key key,
      this.textcolor,
      this.text,
      this.textsize,
      this.width,
      this.height,
      this.buttoncolor,
      this.vpadding,
      this.hpadding,
      this.onpress,
      this.fontweight,
      this.child,
      this.textDecoration,
      this.bradius,
      this.bshadow,
      this.sradius,
      this.radius})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Bouncing(
      onPress: onpress,
      child: MouseRegion(
        cursor: SystemMouseCursors.click,
        child: Container(
          height: height,
          width: width,
          padding: EdgeInsets.symmetric(
            vertical: vpadding == null ? 5 : vpadding,
            horizontal: hpadding == null ? 10 : hpadding,
          ),
          decoration: BoxDecoration(
            boxShadow: [
              bshadow == true
                  ? BoxShadow(
                      color: Pallet1.fontcolornew.withOpacity(1),
                      offset: Offset(4, 6),
                      spreadRadius: sradius,
                      blurRadius: bradius)
                  : bshadow == null
                      ? BoxShadow()
                      : BoxShadow(),
            ],
            color: buttoncolor == null ? Pallet1.buttonback : buttoncolor,
            borderRadius: BorderRadius.circular(radius == null ? 7.0 : radius),
          ),
          child: child == null
              ? Center(
                  child: Text(
                    text,
                    style: TextStyle(
                      decoration: textDecoration,
                      fontWeight:
                          fontweight == null ? Pallet1.font500 : fontweight,
                      color:
                          textcolor == null ? Pallet1.fontcolornew : textcolor,
                      fontSize: textsize == null ? Pallet1.heading4 : textsize,
                    ),
                  ),
                )
              : child,
        ),
      ),
    );
  }
}

class PopupButton1 extends StatelessWidget {
  final Color textcolor;
  final String text;
  final double textsize;
  final double width;
  final double height;
  final Color buttoncolor;
  final double vpadding;
  final double hpadding;
  final Function onpress;
  final Widget child;
  final FontWeight fontweight;
  const PopupButton1(
      {Key key,
      this.textcolor,
      this.text,
      this.textsize,
      this.width,
      this.height,
      this.buttoncolor,
      this.vpadding,
      this.hpadding,
      this.onpress,
      this.fontweight,
      this.child})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onpress,
      child: MouseRegion(
        cursor: SystemMouseCursors.click,
        child: Container(
          height: height,
          width: width,
          padding: EdgeInsets.symmetric(
            vertical: vpadding == null ? 5 : vpadding,
            horizontal: hpadding == null ? 10 : hpadding,
          ),
          decoration: BoxDecoration(
            color: buttoncolor == null ? Pallet1.buttonback : buttoncolor,
            borderRadius: BorderRadius.circular(5.0),
          ),
          child: child == null
              ? Center(
                  child: Text(
                    text,
                    style: TextStyle(
                      fontWeight: fontweight,
                      color:
                          textcolor == null ? Pallet1.fontcolornew : textcolor,
                      fontSize: textsize == null ? Pallet1.heading6 : textsize,
                    ),
                  ),
                )
              : child,
        ),
      ),
    );
  }
}

class SomethingWentWrongMessage extends StatelessWidget {
  @override
  Center build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Center(
      child: Container(
        width: size.width,
        height: size.height * 0.9,
        child: Center(
          child: Image.asset(
            'oops1.gif',
            // width: size.width,
            // height: size.width * 0.50,
          ),
        ),
        // child: Text('oh oh'),
      ),
    );
  }
}

class Loader extends StatelessWidget {
  final Color indicatorcolor;
  final String text;
  final FontWeight textweight;
  final Color textcolor;
  final double textsize;
  final double sizedbox;

  const Loader(
      {Key key,
      this.indicatorcolor,
      this.sizedbox,
      this.text,
      this.textweight,
      this.textcolor,
      this.textsize})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          CircularProgressIndicator(
            valueColor: AlwaysStoppedAnimation(
                indicatorcolor != null ? indicatorcolor : Pallet1.fontcolornew),
          ),
          SizedBox(height: sizedbox == null ? 10 : sizedbox),
          Text(
            text == null ? 'Loading...' : text,
            style: TextStyle(
              fontSize: textsize == null ? Pallet1.heading2 : textsize,
              color: textcolor == null ? Pallet1.fontcolornew : textcolor,
              fontWeight: textweight == null ? Pallet1.font500 : textweight,
            ),
          ),
        ],
      ),
    );
  }
}

class Mypath extends CustomClipper<Path> {
  @override
  getClip(Size size) {
    Path path = Path()
      ..lineTo(0, size.height)
      ..lineTo(size.width / 2, size.height)
      ..lineTo(size.width, 0);

    return path;
  }

  @override
  bool shouldReclip(covariant CustomClipper oldClipper) {
    return true;
  }
}

class Snack {
  snack({@required String title}) {
    Get.snackbar('', '',
        maxWidth: 600,
        margin: EdgeInsets.only(top: 10),
        boxShadows: [Pallet1.shadowEffect],
        progressIndicatorBackgroundColor: Pallet1.fontcolornew,
        titleText: Text(
          title,
          textAlign: TextAlign.center,
          style: TextStyle(
            color: Pallet1.fontcolor,
          ),
        ),
        backgroundColor: Pallet1.snackback,
        snackPosition: SnackPosition.TOP);
  }
}

Snack snack = Snack();

Lottery lottery = Lottery();
