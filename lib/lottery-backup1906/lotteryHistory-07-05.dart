import '../lottery/common.dart';
import 'package:flutter/material.dart';

// import '../common.dart';
import '../services/communication/http/index.dart';
import '../utils/utils.dart';
import 'config.dart';

class LotteryHistory extends StatefulWidget {
  final double wdgtWidth, wdgtHeight;

  const LotteryHistory({Key key, this.wdgtWidth, this.wdgtHeight})
      : super(key: key);
  @override
  _LotteryHistoryState createState() => _LotteryHistoryState();
}

class _LotteryHistoryState extends State<LotteryHistory> {
  bool isMobile = false;

  int selectedIndex;
  Map<String, dynamic> map = {
    'product_id': 1,
  };
  List data = [];
  Future<String> temp;

  Future<String> lotteryHistory(Map map) async {
    Map<String, dynamic> result =
        await HttpRequest.Post('lottery_History', Utils.constructPayload(map));
    if (Utils.isServerError(result)) {
      throw (await Utils.getMessage(result['response']['error']));
    } else {
      print('RESULT: ${result['response']['data']['history']}');

      // data.add(result['response']['data']['history']) ;
      print('DATA: ${data.length}');
      return 'starrt';
    }
  }

  @override
  void initState() {
    super.initState();
    temp = lotteryHistory(map);
    print(";;;;;;;;;;;;;;");
    print(data.length);
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
        future: temp,
        builder: (context, snapshot) {
          print('SNAPSHOT: ${snapshot.data}');
          print("gkkkkkkkkkkkk");
          if (snapshot.hasData) {
            return LayoutBuilder(builder: (context, constraints) {
              if (constraints.maxWidth <= 600) {
                isMobile = true;
                return Padding(
                  padding: EdgeInsets.all(Pallet1.defaultPadding),
                  child: historyList(widget.wdgtWidth),
                );
              } else if (constraints.maxWidth > 600 &&
                  constraints.maxWidth <= 900) {
                isMobile = true;

                return Padding(
                  padding: EdgeInsets.only(left: Pallet1.leftPadding),
                  child: historyList(widget.wdgtWidth * .8),
                );
              } else if (constraints.maxWidth > 900 &&
                  constraints.maxWidth <= 1200) {
                return Padding(
                  padding: EdgeInsets.only(left: Pallet1.leftPadding),
                  child: historyList(widget.wdgtWidth * .7),
                );
              } else {
                return Padding(
                  padding: EdgeInsets.all(Pallet1.leftPadding),
                  child: historyList(widget.wdgtWidth * .7),
                );
              }
            });
          } else if (snapshot.hasError) {
            return SomethingWentWrongMessage();
          } else {
            return Loader();
          }
        });
  }

  Widget historyList(wdgtWidth) {
    return Container(
      width: wdgtWidth,
      child: Column(
        children: [
          Row(
            children: [
              Text('Lottery History',
                  style: TextStyle(
                      color: Pallet1.fontcolornew,
                      fontSize: Pallet1.heading1,
                      fontWeight: Pallet1.font600)),
            ],
          ),
          SizedBox(
            height: 20,
          ),
          data.length == 0
              ? Expanded(
                  child: Center(
                    child: Text("No History Found",
                        style: TextStyle(
                          color: Pallet1.fontcolornew,
                          fontSize: Pallet1.normalfont,
                        )),
                  ),
                )
              : Container(),
          for (var i = 0; i < data.length; i++)
            InkWell(
              onTap: () {
                setState(() {
                  if (selectedIndex == i) {
                    selectedIndex = null;
                  } else {
                    selectedIndex = i;
                  }
                });
              },
              child: Padding(
                padding: EdgeInsets.all(Pallet1.defaultPadding),
                child: Container(
                  decoration: BoxDecoration(
                      color: selectedIndex == i
                          ? Colors.blue[100]
                          : Colors.grey[200],
                      border: Border.all(color: Colors.black),
                      borderRadius: BorderRadius.circular(Pallet1.radius)),
                  child: Column(
                    children: [
                      Padding(
                        padding: EdgeInsets.all(Pallet1.defaultPadding),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text(data[i]['created_at'].toString(),
                                style: TextStyle(
                                    color: Pallet1.fontcolornew,
                                    fontSize: Pallet1.heading4)),
                            Text(data[i]['day'].toString(),
                                style: TextStyle(
                                    color: Pallet1.fontcolornew,
                                    fontSize: Pallet1.heading4)),
                          ],
                        ),
                      ),
                      selectedIndex == i
                          ? SingleChildScrollView(
                              child: Column(
                                children: [
                                  Container(
                                    // color: Colors.green,
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        if (data[i]['purchase'] != null)
                                          for (var item in data[i]['purchase'])
                                            Padding(
                                              padding: EdgeInsets.all(
                                                  Pallet1.defaultPadding),
                                              child: Container(
                                                  decoration: BoxDecoration(
                                                      borderRadius:
                                                          BorderRadius.circular(
                                                              Pallet1.radius),
                                                      border: Border.all(
                                                          color: Colors.black),
                                                      color: (item['daily_processed'] ==
                                                                      true &&
                                                                  item['weekly_processed'] ==
                                                                      true) ==
                                                              true
                                                          ? Colors.white
                                                          : (item['daily_processed'] ==
                                                                          false &&
                                                                      item['weekly_processed'] ==
                                                                          false) ==
                                                                  true
                                                              ? Colors.pink
                                                              : (item['daily_processed'] ==
                                                                              false &&
                                                                          item['weekly_processed'] ==
                                                                              true) ==
                                                                      true
                                                                  ? Colors.yellow
                                                                  : (item['daily_processed'] == true && item['weekly_processed'] == false) == true
                                                                      ? Colors.green
                                                                      : Colors.white),
                                                  child: Padding(
                                                    padding: EdgeInsets.all(
                                                        Pallet1.defaultPadding),
                                                    child: isMobile == true
                                                        ? Column(
                                                            crossAxisAlignment:
                                                                CrossAxisAlignment
                                                                    .start,
                                                            children: [
                                                              textCreatedat(
                                                                  item: item),
                                                              wrappp(
                                                                  item: item),
                                                            ],
                                                          )
                                                        : Row(
                                                            // mainAxisAlignment:
                                                            //     MainAxisAlignment.spaceBetween,
                                                            children: [
                                                              wrappp(
                                                                  item: item),
                                                              Spacer(
                                                                flex: 2,
                                                              ),
                                                              textCreatedat(
                                                                  item: item),
                                                            ],
                                                          ),
                                                  )),
                                            ),
                                        if (data[i]['purchase'] == null)
                                          // SizedBox(
                                          //   height: 10,
                                          // ),
                                          Text('No purchases found',
                                              style: TextStyle(
                                                  color: Pallet1.fontcolornew,
                                                  fontWeight: Pallet1.bold,
                                                  fontSize: Pallet1.heading4)),
                                        SizedBox(
                                          height: 10,
                                        ),
                                      ],
                                    ),
                                  ),
                                  // for (var item in )
                                  Padding(
                                    padding:
                                        EdgeInsets.all(Pallet1.defaultPadding),
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Text('Lucky Numbers',
                                            style: TextStyle(
                                                color: Pallet1.fontcolornew,
                                                fontSize: Pallet1.heading4)),
                                        isMobile == true
                                            ? Column(
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.start,
                                                children: [
                                                  Wrap(
                                                    children: [
                                                      for (var items in data[i]
                                                          ['lucky_numbers'])
                                                        luckyNumbers(
                                                            items: items),
                                                    ],
                                                  ),
                                                  SizedBox(
                                                    height: 10,
                                                  ),
                                                  // Spacer(flex: 1),
                                                  jackpotPercent(
                                                      data: data, i: i)
                                                ],
                                              )
                                            : Row(
                                                // mainAxisAlignment:
                                                //     MainAxisAlignment
                                                //         .spaceBetween,
                                                children: [
                                                  for (var items in data[i]
                                                      ['lucky_numbers'])
                                                    luckyNumbers(items: items),
                                                  SizedBox(
                                                    height: 10,
                                                  ),
                                                  Spacer(flex: 2),
                                                  jackpotPercent(
                                                      data: data, i: i)
                                                ],
                                              ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            )
                          : Container(
                              color: Colors.yellow,
                            )
                    ],
                  ),
                ),
              ),
            )
        ],
      ),
    );
  }
}

class jackpotPercent extends StatelessWidget {
  const jackpotPercent({
    Key key,
    @required this.data,
    @required this.i,
  }) : super(key: key);

  final List data;
  final int i;

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.end,
      children: [
        Text('Jackpot Percentage'.toString(),
            style: TextStyle(
                color: Pallet1.fontcolornew, fontSize: Pallet1.heading2)),
        Text(data[i]['jackpot_percentage'].toString() + ' %',
            style: TextStyle(
                color: Pallet1.fontcolornew, fontSize: Pallet1.heading4)),
      ],
    );
  }
}

class luckyNumbers extends StatelessWidget {
  const luckyNumbers({
    Key key,
    @required this.items,
  }) : super(key: key);

  final items;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.all(Pallet1.defaultPadding / 2),
      child: Container(
        decoration: BoxDecoration(
            color: Colors.green,
            borderRadius: BorderRadius.circular(Pallet1.radius)),
        child: Padding(
          padding: EdgeInsets.all(Pallet1.defaultPadding),
          child: Text(items.toString(),
              style: TextStyle(
                  color: Pallet1.fontcolor, fontSize: Pallet1.heading4)),
        ),
      ),
    );
  }
}

class textCreatedat extends StatelessWidget {
  const textCreatedat({
    Key key,
    @required this.item,
  }) : super(key: key);

  final item;

  @override
  Widget build(BuildContext context) {
    return Text(item['created_at'].toString(),
        style:
            TextStyle(color: Pallet1.fontcolornew, fontSize: Pallet1.heading4));
  }
}

class wrappp extends StatelessWidget {
  const wrappp({
    Key key,
    @required this.item,
  }) : super(key: key);

  final item;

  @override
  Widget build(BuildContext context) {
    return Container(
      // color: Colors.black,
      child: Wrap(
        children: [
          for (var number in item['numbers'])
            Padding(
              padding: EdgeInsets.all(Pallet1.defaultPadding),
              child: Container(
                decoration: BoxDecoration(
                    color: ([0, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89]
                                .contains(number)) ==
                            true
                        ? Colors.red
                        : Pallet1.fontcolornew,
                    borderRadius: BorderRadius.circular(Pallet1.radius)),
                child: Padding(
                  padding: EdgeInsets.all(Pallet1.defaultPadding),
                  child: Text(
                    number.toString(),
                    style: TextStyle(
                        color: Pallet1.fontcolor, fontSize: Pallet1.heading4),
                  ),
                ),
              ),
            ),
        ],
      ),
    );
  }
}
