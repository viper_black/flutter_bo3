// import 'dart:math';
import 'dart:async';

import 'package:centurion/lottery/common.dart';
import 'package:confetti/confetti.dart';

import 'package:centurion/config.dart';
import 'package:centurion/services/business/account.dart';
import 'package:flutter_countdown_timer/countdown_timer_controller.dart';

import '../home.dart';
import '../services/communication/index.dart' show HttpRequest;
import '../services/business/account.dart';
import 'package:centurion/utils/utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter_countdown_timer/current_remaining_time.dart';
import 'package:flutter_countdown_timer/flutter_countdown_timer.dart';
import 'config.dart';

// class CurrentRemaininggTime {
//   final int days;
//   final int hours;
//   final int min;
//   final int sec;
//   final Animation<double> milliseconds;

//   CurrentRemaininggTime.utc(
//       {this.days, this.hours, this.min, this.sec, this.milliseconds});

//   @override
//   String toString() {
//     return 'CurrentRemainingTime{days: $days, hours: $hours, min: $min, sec: $sec, milliseconds: ${milliseconds?.value}';
//   }
// }

class WinnerAnnouncement extends StatefulWidget {
  final double wdgtWidth, wdgtHeight;

  const WinnerAnnouncement({Key key, this.wdgtWidth, this.wdgtHeight})
      : super(key: key);
  @override
  _WinnerAnnouncementState createState() => _WinnerAnnouncementState();
}

class _WinnerAnnouncementState extends State<WinnerAnnouncement> {
  Future<bool> _temp;
  List<dynamic> data = [];
  List<dynamic> winners = [];

  List<dynamic> splNumber = [];

  Future<bool> lotteryWinner(Map map) async {
    // final DateFormat formatter = DateFormat('yyyy-MM-dd');

    Map<String, dynamic> result =
        await HttpRequest.Post('lotteryWinner', Utils.constructPayload(map));
    if (Utils.isServerError(result)) {
      print(result);
      return throw (await Utils.getMessage(result['response']['error']));
    } else {
      print('000000|_|++++++++++++++++');
      print(result['response']['data']);
      // setState(() {
      data = result['response']['data']['config'];
      winners = result['response']['data']['winners'];

      // });
      if (result['response']['data']['isBlast'] == true) {
        // controllerCenter.play();
      }
      lottryDataSink.add(result['response']['data']['config']);
      // splNumber = result['response']['data']['spl_numbers'];
      print('MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM');
      // print(splNumber);
      print(result['response']['data']['last_lucky_numbers']);
      print('UUUUUUUUUUUUUUUUUUUUUUUUU');

      print(data);
      // setState(() {});

      return true;
    }
  }

  Future<bool> testLottry(Map map) async {
    Map<String, dynamic> result =
        await HttpRequest.Post('daily_draw', Utils.constructPayload(map));
    if (Utils.isServerError(result)) {
      print(result);
      return throw (await Utils.getMessage(result['response']['error']));
    } else {
      lotteryWinner(map);
      return true;
    }
  }

  Future<bool> testLottry2(Map map) async {
    Map<String, dynamic> result =
        await HttpRequest.Post('weekly_draw', Utils.constructPayload(map));
    if (Utils.isServerError(result)) {
      print(result);
      return throw (await Utils.getMessage(result['response']['error']));
    } else {
      lotteryWinner(map);
      return true;
    }
  }

  StreamController<List> controller;
  Stream lottryDataStream;
  Sink lottryDataSink;

  ConfettiController controllerCenter;

  caltimer() {
    var now = new DateTime.now().toString();

    var dateParse = DateTime.parse(now);

    // var currentUTC = DateTime.now().toUtc().toString();
    // var parseCurrentUTC = DateTime.parse(currentUTC);

    endTime3 =
        DateTime.utc(dateParse.year, dateParse.month, dateParse.day, 12, 00)
            .millisecondsSinceEpoch;

    // -1620648-18000000-60000

    // endTime3=20000;

    print("xxxxxxxxxxx");
    print(endTime3);
    print(
        (DateTime.utc(dateParse.year, dateParse.month, dateParse.day, 12, 00)));
  }

  Map<String, dynamic> map = {'product_id': 1};
  // CountdownTimerController controller1;
  @override
  void initState() {
    caltimer();
    // controller1 = CountdownTimerController(endTime: endTime3, onEnd: caltimer);

    controllerCenter =
        ConfettiController(duration: const Duration(seconds: 10));
    print('sssssssssssssssssssssssssssssssssssssssssss');
    print(dashboard.username);
    _temp = lotteryWinner(map);
    controller = StreamController<List>();
    lottryDataStream = controller.stream;
    lottryDataSink = controller.sink;

    super.initState();
  }

  bool isMobile = false;
  // CountdownTimerController _countdownTimerController;

  // ignore: override_on_non_overriding_member
  // List<Map> winnerList = [
  //   {'name': 'winner', 'price': '100'},
  //   {'name': 'winfner', 'price': 'd100'},
  //   {'name': 'winnere', 'price': '100'},
  //   {'name': 'winnere', 'price': '100'},
  //   {'name': 'winnere', 'price': '100'},
  //   {'name': 'winnere', 'price': '100'},
  //   {'name': 'winnere', 'price': '100'},
  //   {'name': 'winnere', 'price': '100'},
  // ];

//
  int endTime3;
  // int endTime = DateTime.now().millisecondsSinceEpoch + 1000 * (300 * 30000);
  // int endTime2 = DateTime.now().second;

  @override
  Widget build(BuildContext context) {
    print('OOOOOOOOOOOOOOOOOOOOOOOOOO');
    // lotteryWinner(map);

    return StreamBuilder(
        stream: lottryDataStream,
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            controllerCenter.play();
            return LayoutBuilder(builder: (context, constraints) {
              if (constraints.maxWidth <= 425) {
                isMobile = true;
                return Padding(
                  padding: EdgeInsets.all(Pallet1.defaultPadding),
                  child: Center(
                    child: SingleChildScrollView(
                      child: Column(
                        children: [
                          winnerAnnouncement(width: widget.wdgtWidth),
                          SizedBox(
                            height: 20,
                          ),
                          recentWinners(width: widget.wdgtWidth),
                          SizedBox(
                            height: 20,
                          ),
                          luckyNumbers(width: widget.wdgtWidth)
                        ],
                      ),
                    ),
                  ),
                );
              } else if (constraints.maxWidth >= 425 &&
                  constraints.maxWidth <= 1024) {
                isMobile = false;
                return Center(
                  child: SingleChildScrollView(
                    child: Column(
                      children: [
                        winnerAnnouncement(width: widget.wdgtWidth),
                        SizedBox(
                          height: 20,
                        ),
                        recentWinners(width: widget.wdgtWidth),
                        SizedBox(
                          height: 20,
                        ),
                        luckyNumbers(width: widget.wdgtWidth)
                      ],
                    ),
                  ),
                );
              } else if (constraints.maxWidth >= 1024 &&
                  constraints.maxWidth <= 1450) {
                isMobile = false;
                return SingleChildScrollView(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Container(
                        width: widget.wdgtWidth,
                        child: Column(
                          //        mainAxisAlignment: MainAxisAlignment.center,
                          // crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            InkWell(
                                onTap: () {
                                  testLottry(map);
                                },
                                child: Text('Daily Draw')),
                            InkWell(
                                onTap: () {
                                  testLottry2(map);
                                },
                                child: Text('Weekly Draw')),
                            winnerAnnouncement(
                                width: widget.wdgtWidth * .7,
                                imgsize: widget.wdgtHeight * 0.3),
                            SizedBox(
                              height: 20,
                            ),
                            Container(
                              width: widget.wdgtWidth * 0.7,
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  recentWinners(width: widget.wdgtWidth * .34),
                                  luckyNumbers(width: widget.wdgtWidth * .34)
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                );
              } else {
                isMobile = false;
                return SingleChildScrollView(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Container(
                        width: widget.wdgtWidth,
                        child: Column(
                          //        mainAxisAlignment: MainAxisAlignment.center,
                          // crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            TextButton(
                                onPressed: () {
                                  testLottry(map);

                                  setState(() {});
                                },
                                child: Text('Daily Draw')),
                            TextButton(
                                onPressed: () {
                                  testLottry2(map);

                                  setState(() {});
                                },
                                child: Text('Weekly Draw')),
                            winnerAnnouncement(width: widget.wdgtWidth * .7),
                            SizedBox(
                              height: 20,
                            ),
                            Container(
                              width: widget.wdgtWidth * 0.7,
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  recentWinners(width: widget.wdgtWidth * .34),
                                  luckyNumbers(width: widget.wdgtWidth * .34)
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                );
              }
            });
          } else if (snapshot.hasError) {
            return SomethingWentWrongMessage();
          } else {
            return Loader();
          }
        });
  }

  bool timer = true;

  stop() {
    // controllerCenter.stop();
    // timer = false;
    var now = new DateTime.now().toString();

    var dateParse = DateTime.parse(now);
    if (endTime3 == endTime3) {
      endTime3 = DateTime.utc(
              dateParse.year, dateParse.month, dateParse.day + 1, 12, 00)
          .millisecondsSinceEpoch;
      // -1620648-18000000-60000
    }
  }

  Widget winnerAnnouncement({double width, double imgsize}) {
    double swidth = MediaQuery.of(context).size.width;
    return Container(
        padding: EdgeInsets.all(Pallet1.defaultPadding),
        decoration: BoxDecoration(
            color: Pallet1.dashcontainerback,
            borderRadius: BorderRadius.circular(10)),
        width: width,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                // for (var item in data)

                ConfettiWidget(
                  particleDrag: 0.05, // apply drag to the confetti
                  emissionFrequency: 0.05, // how often it should emit
                  // blastDirection: 0,
                  numberOfParticles: 20, // number of particles to emit
                  gravity: 0.01, // gravity - or fall speed

                  confettiController: controllerCenter,
                  blastDirectionality: BlastDirectionality
                      .explosive, // don't specify a direction, blast randomly
                  shouldLoop:
                      false, // start again as soon as the animation is finished
                  colors: const [
                    Colors.green,
                    Colors.blue,
                    Colors.pink,
                    Colors.orange,
                    Colors.purple
                  ], // manually specify the colors to be used
                  // createParticlePath: drawStar, // define a custom shape/path.
                ),

                ConfettiWidget(
                  particleDrag: 0.05, // apply drag to the confetti
                  emissionFrequency: 0.05, // how often it should emit
                  // blastDirection: pi,
                  numberOfParticles: 20, // number of particles to emit
                  gravity: 0.01, // gravity - or fall speed

                  confettiController: controllerCenter,
                  blastDirectionality: BlastDirectionality
                      .explosive, // don't specify a direction, blast randomly
                  shouldLoop:
                      false, // start again as soon as the animation is finished
                  colors: const [
                    Colors.green,
                    Colors.blue,
                    Colors.pink,
                    Colors.orange,
                    Colors.purple
                  ], // manually specify the colors to be used
                  // createParticlePath: drawStar, // define a custom shape/path.
                ),
              ],
            ),
            swidth >= 768
                ? Container(
                    // width: width,
                    padding: EdgeInsets.symmetric(
                      horizontal: 10,
                    ),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      // mainAxisSize: MainAxisSize.min,
                      children: [
                        Container(
                          padding: EdgeInsets.only(top: 5),
                          child: Row(
                            mainAxisSize: MainAxisSize.min,
                            children: [
                              Text('Jackpot Fund :',
                                  style: TextStyle(
                                    color: Pallet1.fontcolor,
                                    fontSize: Pallet1.normalfont,
                                  )),
                              SizedBox(width: 10),
                              for (var item in data)
                                Text("\$ " + item['jackpot_fund'].toString(),
                                    style: TextStyle(
                                      color: Pallet1.fontcolor,
                                      fontSize: Pallet1.normalfont,
                                    )),
                            ],
                          ),
                        ),
                        imgsize == null
                            ? Image.asset('winnerAnnouncement.png')
                            : Image.asset(
                                'winnerAnnouncement.png',
                                height: imgsize,
                              ),
                        Container(
                          padding: EdgeInsets.only(top: 5),
                          child: Row(
                            mainAxisSize: MainAxisSize.min,
                            children: [
                              Text('Grand Prize Fund :',
                                  style: TextStyle(
                                    color: Pallet1.fontcolor,
                                    fontSize: Pallet1.normalfont,
                                  )),
                              SizedBox(width: 10),
                              for (var item in data)
                                Text("\$ " + item['weekly_fund'].toString(),
                                    style: TextStyle(
                                      color: Pallet1.fontcolor,
                                      fontSize: Pallet1.normalfont,
                                    )),
                            ],
                          ),
                        ),
                      ],
                    ),
                  )
                : Container(
                    padding: EdgeInsets.symmetric(
                      horizontal: 10,
                    ),
                    child: Column(
                      // mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        imgsize == null
                            ? Image.asset('winnerAnnouncement.png')
                            : Image.asset(
                                'winnerAnnouncement.png',
                                height: imgsize,
                              ),
                        Container(
                          padding: EdgeInsets.only(top: 5),
                          child: Row(
                            mainAxisSize: MainAxisSize.min,
                            children: [
                              Text('Jackpot Fund :',
                                  style: TextStyle(
                                    color: Pallet1.fontcolor,
                                    fontSize: Pallet1.normalfont,
                                    fontWeight: FontWeight.w600,
                                  )),
                              SizedBox(width: 10),
                              for (var item in data)
                                Text(item['jackpot_fund'].toString(),
                                    style: TextStyle(
                                      color: Pallet1.fontcolor,
                                      fontSize: Pallet1.normalfont,
                                      fontWeight: FontWeight.w600,
                                    )),
                            ],
                          ),
                        ),
                        Container(
                          padding: EdgeInsets.only(top: 5),
                          child: Row(
                            mainAxisSize: MainAxisSize.min,
                            children: [
                              Text('Grand Prize Fund :',
                                  style: TextStyle(
                                    color: Pallet1.fontcolor,
                                    fontSize: Pallet1.normalfont,
                                    fontWeight: FontWeight.w600,
                                  )),
                              SizedBox(width: 10),
                              for (var item in data)
                                Text(item['weekly_fund'].toString(),
                                    style: TextStyle(
                                      color: Pallet1.fontcolor,
                                      fontSize: Pallet1.normalfont,
                                      fontWeight: FontWeight.w600,
                                    )),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
            SizedBox(
              height: 20,
            ),
            Text('Time Left for Next Lucky Draw',
                style: TextStyle(
                    color: Pallet1.fontcolor,
                    fontSize: Pallet1.normalfont + 5,
                    fontWeight: Pallet1.bold)),
            SizedBox(
              height: 20,
            ),
            Center(
              child: Container(
                // width: 250,
                child: CountdownTimer(
                  // controller: controller1,
                  endTime: endTime3,
                  onEnd: () {
                    lotteryWinner(map);
                    Timer(Duration(minutes: 1), stop());
                  },
                  widgetBuilder: (_, CurrentRemainingTime time) {
                    if (time == null) {
                      return Text('Time Out',
                          style: TextStyle(
                              color: Pallet1.fontcolor,
                              fontSize: Pallet1.normalfont,
                              fontWeight: Pallet1.bold));
                    }

                    return Column(
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            time.days == null
                                ? Container()
                                : Row(
                                    children: [
                                      Container(
                                        // decoration: BoxDecoration(
                                        // color: Pallet1.fontcolor,
                                        // borderRadius: BorderRadius.circular(2)),
                                        child: Padding(
                                          padding: EdgeInsets.all(
                                              Pallet1.defaultPadding / 2),
                                          child: Text(
                                              time.days.toString().length == 1
                                                  ? '0' + '${time.days}'
                                                  : '${time.days}',
                                              style: TextStyle(
                                                  color: Pallet1.fontcolor,
                                                  fontSize: Pallet1.normalfont,
                                                  fontWeight: Pallet1.bold)),
                                        ),
                                      ),
                                      Text('days',
                                          style: TextStyle(
                                              color: Pallet1.fontcolor,
                                              fontSize: Pallet1.normalfont,
                                              fontWeight: Pallet1.bold))
                                    ],
                                  ),
                            // SizedBox(
                            //   width: 20,
                            // ),
                            Row(
                              children: [
                                Container(
                                  // decoration: BoxDecoration(
                                  //     color: Pallet1.fontcolor,
                                  //     borderRadius: BorderRadius.circular(2)),
                                  child: Padding(
                                    padding: EdgeInsets.all(
                                        Pallet1.defaultPadding / 2),
                                    child: Text(
                                        time.hours == null
                                            ? '00'
                                            : time.hours.toString().length == 1
                                                ? '0' + '${time.hours}'
                                                : '${time.hours}',
                                        style: TextStyle(
                                            color: Pallet1.fontcolor,
                                            fontSize: Pallet1.normalfont,
                                            fontWeight: Pallet1.bold)),
                                  ),
                                ),
                                Text('Hrs',
                                    style: TextStyle(
                                      color: Pallet1.fontcolor,
                                      fontSize: Pallet1.normalfont,
                                    ))
                              ],
                            ),
                            // SizedBox(
                            //   width: 20,
                            // ),
                            Row(
                              children: [
                                Container(
                                  // decoration: BoxDecoration(
                                  //     color: Pallet1.fontcolor,
                                  //     borderRadius: BorderRadius.circular(2)),
                                  child: Padding(
                                    padding: EdgeInsets.all(
                                        Pallet1.defaultPadding / 2),
                                    child: Text(
                                        time.min == null
                                            ? "00"
                                            : time.min.toString().length == 1
                                                ? '0' + '${time.min}'
                                                : '${time.min}',
                                        style: TextStyle(
                                          color: Pallet1.fontcolor,
                                          fontSize: Pallet1.normalfont,
                                        )),
                                  ),
                                ),
                                Text('Mins',
                                    style: TextStyle(
                                      color: Pallet1.fontcolor,
                                      fontSize: Pallet1.normalfont,
                                    ))
                              ],
                            ),
                            // SizedBox(
                            //   width: 20,
                            // ),
                            Row(
                              children: [
                                Container(
                                  // decoration: BoxDecoration(
                                  //     color: Pallet1.fontcolor,
                                  //     borderRadius: BorderRadius.circular(2)),
                                  child: Padding(
                                    padding: EdgeInsets.all(
                                        Pallet1.defaultPadding / 2),
                                    child: Text(
                                        time.sec == null
                                            ? "00"
                                            : time.sec.toString().length == 1
                                                ? '0' + '${time.sec}'
                                                : '${time.sec}',
                                        style: TextStyle(
                                            color: Pallet1.fontcolor,
                                            fontSize: Pallet1.normalfont,
                                            fontWeight: Pallet1.bold)),
                                  ),
                                ),
                                Text('Secs ',
                                    style: TextStyle(
                                      color: Pallet1.fontcolor,
                                      fontSize: Pallet1.normalfont,
                                    )),
                              ],
                            ),
                            isMobile == true ? Container() : text()
                          ],
                        ),
                        isMobile == true ? text() : Container()
                      ],
                    );
                  },
                ),
              ),
            ),
          ],
        ));
  }

  Widget text() {
    return Text('left until Today\'s Draw',
        style: TextStyle(
          color: Pallet1.fontcolor,
          fontSize: Pallet1.normalfont,
        ));
  }

  bool iscalled = false;
  Widget recentWinners({double width}) {
    return Container(
      height: 273,
      padding: EdgeInsets.all(Pallet1.defaultPadding),
      width: width,
      decoration: BoxDecoration(
          color: Pallet1.dashcontainerback,
          borderRadius: BorderRadius.circular(10)),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text('Today Winners',
              style: TextStyle(
                  color: Pallet1.fontcolor,
                  fontSize: Pallet1.normalfont + 5,
                  fontWeight: Pallet1.bold)),
          SizedBox(height: 7),
          Wrap(children: [
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 3),
              child: Row(
                mainAxisSize: MainAxisSize.min,
                children: [
                  SizedBox(
                    width: 10,
                    height: 10,
                    child: Container(
                      color: Colors.grey,
                    ),
                  ),
                  SizedBox(width: 5),
                  Text("Super", style: TextStyle(color: Pallet1.fontcolor)),
                ],
              ),
            ),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 3),
              child: Row(
                mainAxisSize: MainAxisSize.min,
                children: [
                  SizedBox(
                    width: 10,
                    height: 10,
                    child: Container(
                      color: Colors.amber[900],
                    ),
                  ),
                  SizedBox(width: 5),
                  Text("Jackpot", style: TextStyle(color: Pallet1.fontcolor)),
                ],
              ),
            ),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 3),
              child: Row(
                mainAxisSize: MainAxisSize.min,
                children: [
                  SizedBox(
                    width: 10,
                    height: 10,
                    child: Container(
                      color: Colors.red,
                    ),
                  ),
                  SizedBox(width: 5),
                  Text("Super Jackpot",
                      style: TextStyle(color: Pallet1.fontcolor)),
                ],
              ),
            ),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 3),
              child: Row(
                mainAxisSize: MainAxisSize.min,
                children: [
                  SizedBox(
                    width: 10,
                    height: 10,
                    child: Container(
                      color: Colors.pink[300],
                    ),
                  ),
                  SizedBox(width: 5),
                  Text("Grand Prize",
                      style: TextStyle(color: Pallet1.fontcolor)),
                ],
              ),
            ),
          ]),
          SizedBox(height: 3),
          winners.length != 0
              ? Expanded(
                  child: ListView.builder(
                      itemCount: winners.length,
                      itemBuilder: (context, index) {
                        var time =
                            winners[index]['created_at'].toString().split('T');

                        var a = time[1].toString().split('.');
                        return Padding(
                          padding: EdgeInsets.all(Pallet1.defaultPadding),
                          child: Container(
                            padding: EdgeInsets.all(Pallet1.defaultPadding),
                            decoration: BoxDecoration(
                                color: winners[index]['winner_type'] == 'normal'
                                    ? Colors.grey
                                    : winners[index]['winner_type'] == 'special'
                                        ? Colors.amber[900]
                                        : winners[index]['winner_type'] ==
                                                'weekly'
                                            ? Colors.pink[300]
                                            : Colors.red,
                                borderRadius: BorderRadius.circular(10)),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Expanded(
                                  child: Center(
                                    child: Text(time[0] + ' ' + a[0].toString(),
                                        style: TextStyle(
                                          color: Pallet1.fontcolor,
                                          fontSize: Pallet1.normalfont,
                                        )),
                                  ),
                                ),
                                Expanded(
                                  child: Center(
                                    child: Text(
                                        'LOT' +
                                            winners[index]['lottery_id']
                                                .toString(),
                                        style: TextStyle(
                                          color: Pallet1.fontcolor,
                                          fontSize: Pallet1.normalfont,
                                        )),
                                  ),
                                ),
                                Expanded(
                                  child: Center(
                                    child: Text(
                                        winners[index]['screen_name']
                                            .toString(),
                                        // overflow: TextOverflow.ellipsis,
                                        style: TextStyle(
                                          color: Pallet1.fontcolor,
                                          fontSize: Pallet1.normalfont,
                                        )),
                                  ),
                                ),
                                Expanded(
                                  child: Center(
                                    child: Text(
                                        winners[index]['particulars'] ==
                                                'Dollar'
                                            ? '\$ ' +
                                                (double.parse(winners[index]
                                                            ['amount']
                                                        .toString()))
                                                    .toStringAsFixed(2)
                                            : ((double.parse(winners[index]
                                                                ['amount']
                                                            .toString()))
                                                        .toStringAsFixed(2) +
                                                    ' DUC')
                                                .toString(),
                                        style: TextStyle(
                                          color: Pallet1.fontcolor,
                                          fontSize: Pallet1.normalfont,
                                        )),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        );
                      }))
              : Expanded(
                  child: Center(
                    child: Text("No Recent Winners Found",
                        style: TextStyle(
                          color: Pallet1.fontcolor,
                          fontSize: Pallet1.normalfont,
                        )),
                  ),
                ),
        ],
      ),
    );
  }

  Widget luckyNumbers({double width}) {
    return Container(
        width: width,
        height: 273,
        padding: EdgeInsets.all(Pallet1.defaultPadding),
        decoration: BoxDecoration(
            color: Pallet1.dashcontainerback,
            borderRadius: BorderRadius.circular(10)),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text('Last Draw of Winning Numbers',
                style: TextStyle(
                    color: Pallet1.fontcolor,
                    fontSize: Pallet1.normalfont + 5,
                    fontWeight: Pallet1.bold)),
            Container(
              height: 200,
              child: SingleChildScrollView(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    // Text('Last Lucky Draw Numbers',
                    //     style: TextStyle(
                    //       color: Pallet1.fontcolor,
                    //       fontSize: Pallet1.normalfont,
                    //     )),
                    SingleChildScrollView(
                      scrollDirection: Axis.horizontal,
                      child: Row(
                        children: [
                          for (var item in data)
                            if (item['last_lucky_numbers'] != null)
                              for (var newItem in item['last_lucky_numbers'])
                                Padding(
                                  padding:
                                      EdgeInsets.all(Pallet.defaultPadding),
                                  child: Container(
                                      width: 40,
                                      decoration: BoxDecoration(
                                          borderRadius: BorderRadius.circular(
                                              Pallet.radius),
                                          color: ([
                                                    0,
                                                    1,
                                                    2,
                                                    3,
                                                    5,
                                                    8,
                                                    13,
                                                    21,
                                                    34,
                                                    55,
                                                    89
                                                  ].contains(newItem)) ==
                                                  true
                                              ? Pallet.dashsmallcontainerback
                                              : Pallet.fontcolor,
                                          border: Border.all(
                                              color: Pallet.fontcolornew)),
                                      child: Center(
                                        child: Padding(
                                          padding: EdgeInsets.all(
                                              Pallet.defaultPadding),
                                          child: Text(
                                            newItem.toString(),
                                            style: TextStyle(
                                                color: ([
                                                          0,
                                                          1,
                                                          2,
                                                          3,
                                                          5,
                                                          8,
                                                          13,
                                                          21,
                                                          34,
                                                          55,
                                                          89
                                                        ].contains(newItem)) ==
                                                        true
                                                    ? Pallet.fontcolor
                                                    : Pallet.fontcolornew),
                                          ),
                                        ),
                                      )),
                                )
                        ],
                      ),
                    ),
                    Text('Next Week Jackpot %(Cash : DUC)',
                        style: TextStyle(
                          color: Pallet1.fontcolor,
                          fontSize: Pallet1.normalfont,
                        )),
                    Padding(
                      padding: EdgeInsets.all(10),
                      child: Text(
                          data[0]["next_jackpot_percentage"].toString() +
                              '% ' +
                              ': ' +
                              (100 - data[0]["next_jackpot_percentage"])
                                  .toString() +
                              '%',
                          style: TextStyle(
                              color: Colors.grey,
                              fontSize: 21,
                              fontWeight: FontWeight.w800)),
                    ),

                    Row(
                      children: [
                        CustomButton1(
                          vpadding: 10,
                          hpadding: 8,
                          text: 'View History',
                          buttoncolor: Pallet1.fontcolor,
                          textcolor: Pallet1.fontcolornew,
                          onpress: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) =>
                                        Home(route: 'all_lottery_history')));
                          },
                        ),
                      ],
                    ),
                    SizedBox(width: 10),

                    // dashboard.username != null
                    //     ?

                    SizedBox(height: 10),
                    Column(
                      children: [
                        // Row(
                        //   children: [
                        //     Text('Jackpot Fund :',
                        //         style: TextStyle(
                        //           color: Pallet1.fontcolor,
                        //           fontSize: Pallet1.normalfont,
                        //         )),
                        //     SizedBox(width: 10),
                        //     for (var item in data)
                        //       Text(item['jackpot_fund'].toString(),
                        //           style: TextStyle(
                        //             color: Pallet1.fontcolor,
                        //             fontSize: Pallet1.normalfont,
                        //           )),
                        //   ],
                        // ),
                        // Row(
                        //   children: [
                        //     Text('Weekly Fund :',
                        //         style: TextStyle(
                        //           color: Pallet1.fontcolor,
                        //           fontSize: Pallet1.normalfont,
                        //         )),
                        //     SizedBox(width: 10),
                        //     for (var item in data)
                        //       Text(item['weekly_fund'].toString(),
                        //           style: TextStyle(
                        //             color: Pallet1.fontcolor,
                        //             fontSize: Pallet1.normalfont,
                        //           )),
                        //   ],
                        // ),
                        // Row(
                        //   children: [
                        //     Text('Company Fund :',
                        //         style: TextStyle(
                        //           color: Pallet1.fontcolor,
                        //           fontSize: Pallet1.normalfont,
                        //         )),
                        //     SizedBox(width: 10),
                        //     for (var item in data)
                        //       Text(item['company_fund'].toString(),
                        //           style: TextStyle(
                        //             color: Pallet1.fontcolor,
                        //             fontSize: Pallet1.normalfont,
                        //           )),
                        //   ],
                        // ),
                      ],
                    )
                    // : Container()

                    // Center(child: Image.asset('Ticketimg.png')),
                    // Center(
                    //   child: Container(
                    //     decoration: BoxDecoration(
                    //         color: Pallet1.fontcolor,
                    //         borderRadius: BorderRadius.circular(5)),
                    //     child: TextButton(
                    //       onPressed: () {},
                    //       child: Text('Invite',
                    //           style: TextStyle(
                    //             color: Pallet1.dashcontainerback,
                    //             fontSize: Pallet1.normalfont,
                    //           )),
                    //     ),
                    //   ),
                    // )
                  ],
                ),
              ),
            ),
          ],
        ));
  }
}
