import 'package:flutter/material.dart';
import 'package:graphview/GraphView.dart';
import '../common.dart';
import '../services/communication/http/index.dart';
import '../utils/utils.dart';
import 'config.dart';

class LotteryHistory extends StatefulWidget {
  final double wdgtWidth, wdgtHeight;

  const LotteryHistory({Key key, this.wdgtWidth, this.wdgtHeight})
      : super(key: key);
  @override
  _LotteryHistoryState createState() => _LotteryHistoryState();
}

class _LotteryHistoryState extends State<LotteryHistory> {
  bool isMobile = false;

  int selectedIndex;
  Map<String, dynamic> map = {
    'product_id': 1,
  };
  List data = [];
  List unProcessed = [];
  Future<String> temp;
  void initState() {
    temp = lotteryHistory(map);
    super.initState();
  }

  Future<String> lotteryHistory(Map map) async {
    Map<String, dynamic> result =
        await HttpRequest.Post('lottery_History', Utils.constructPayload(map));
    if (Utils.isServerError(result)) {
      print('wowowowowwowowowwowow');
      return throw (await Utils.getMessage(result['response']['error']));
    } else {
      print('fff');

      data = result['response']['data']['history'];
      unProcessed = result['response']['data']['unprocessed'];
      print('.............................');
      print(unProcessed);
      return 'starrt';
    }
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<Object>(
        future: temp,
        builder: (context, snapshot) {
          if (snapshot.hasError) {
            return SomethingWentWrongMessage();
          } else if (snapshot.hasData) {
            return LayoutBuilder(builder: (context, constraints) {
              if (constraints.maxWidth <= 600) {
                isMobile = true;
                return Padding(
                  padding: EdgeInsets.all(Pallet1.defaultPadding),
                  child: historyList(widget.wdgtWidth),
                );
              } else if (constraints.maxWidth > 600 &&
                  constraints.maxWidth <= 950) {
                isMobile = false;

                return Padding(
                  padding: EdgeInsets.all(Pallet1.defaultPadding),
                  child: historyList(widget.wdgtWidth),
                );
              } else if (constraints.maxWidth > 950 &&
                  constraints.maxWidth <= 1200) {
                isMobile = false;

                return Padding(
                  padding: EdgeInsets.only(left: Pallet1.leftPadding),
                  child: historyList(widget.wdgtWidth * .8),
                );
              } else {
                isMobile = false;

                return Padding(
                  padding: EdgeInsets.all(Pallet1.leftPadding),
                  child: historyList(widget.wdgtWidth * .7),
                );
              }
            });
          } else {
            return Loader();
          }
        });
  }

  Widget historyList(wdgtWidth) {
    return Container(
      width: wdgtWidth,
      child: ListView(
        children: [
          Text('My Lottery Tickets',
              style: TextStyle(
                  color: Pallet1.fontcolornew,
                  fontSize: Pallet1.heading1,
                  fontWeight: Pallet1.font600)),
          SizedBox(
            height: 20,
          ),
          unProcessed.length == 0
              ? Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text("No Lottery Tickets Found",
                        style: TextStyle(
                            color: Pallet1.fontcolornew,
                            fontSize: Pallet1.heading4,
                            fontWeight: Pallet1.font500)),
                  ],
                )
              : Container(),
          Column(
            children: [
              for (var i = unProcessed.length - 1; i >= 0; i--)
                SingleChildScrollView(
                  scrollDirection: Axis.horizontal,
                  child: Padding(
                    padding: EdgeInsets.all(Pallet1.defaultPadding),
                    child: Container(
                      width: wdgtWidth,
                      decoration: BoxDecoration(
                          image: DecorationImage(
                              image: AssetImage(
                                'lotteryTicket.png',
                              ),
                              fit: BoxFit.fill)),
                      child: Padding(
                        padding: EdgeInsets.all(Pallet1.defaultPadding * 2),
                        child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Padding(
                                padding: EdgeInsets.all(Pallet1.defaultPadding),
                                child: Container(
                                  padding:
                                      EdgeInsets.all(Pallet1.defaultPadding),
                                  width: wdgtWidth * .65,
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Text(
                                          'Ticket Id : ' +
                                              'LOT' +
                                              unProcessed[i]['id'].toString(),
                                          style: TextStyle(
                                              color: Pallet1.fontcolornew,
                                              fontSize: Pallet1.heading4)),
                                      UnprocessedTickets(
                                        item: unProcessed[i],
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                              Padding(
                                padding: EdgeInsets.only(
                                    right: isMobile == true ? 0 : 10),
                                child: Container(
                                  // decoration: BoxDecoration(
                                  //     border: Border.all(color: Colors.orange)),
                                  width: wdgtWidth * .16,
                                  child: Column(
                                    children: [
                                      rowDailyProcess(unProcessed[i], true),
                                      SizedBox(height: 10),
                                      rowDailyProcess(unProcessed[i], false),

                                      // rowweeklyProcess(unProcessed[i]),
                                    ],
                                  ),
                                ),
                              )
                            ]),
                      ),
                    ),
                  ),
                )
            ],
          ),
          SizedBox(
            height: 10,
          ),
          data.length == 0
              ? Container()
              : Text('Purchase History',
                  style: TextStyle(
                      color: Pallet1.fontcolornew,
                      fontSize: Pallet1.heading1,
                      fontWeight: Pallet1.font600)),
          if (data.length == 0)
            SizedBox(
              height: 20,
            ),
          for (var i = 0; i < data.length; i++)
            if (data[i]['purchase'] != null)
              InkWell(
                onTap: () {
                  setState(() {
                    if (selectedIndex == i) {
                      selectedIndex = null;
                    } else {
                      selectedIndex = i;
                    }
                  });
                },
                child: Padding(
                  padding: EdgeInsets.all(Pallet1.defaultPadding),
                  child: Container(
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(Pallet1.radius)),
                    child: Column(
                      children: [
                        Padding(
                          padding: EdgeInsets.all(Pallet1.defaultPadding),
                          child: Container(
                            padding: EdgeInsets.all(Pallet1.defaultPadding),
                            decoration: BoxDecoration(
                                color: selectedIndex == i
                                    ? Colors.blue[100]
                                    : Colors.grey[200],
                                border: Border.all(color: Colors.black),
                                borderRadius:
                                    BorderRadius.circular(Pallet1.radius)),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Text(
                                    data[i]['created_at']
                                            .toString()
                                            .split('T')[0] +
                                        " " +
                                        data[i]['created_at']
                                            .toString()
                                            .split('T')[1]
                                            .toString()
                                            .split('.')[0]
                                            .toString(),
                                    style: TextStyle(
                                        color: Pallet1.fontcolornew,
                                        fontSize: Pallet1.heading4)),
                                Image.asset(
                                  'shadowTicket.png',
                                  color: Pallet1.fontcolornew,
                                  height: 30,
                                  width: 30,
                                ),
                                Text(data[i]['day'].toString(),
                                    style: TextStyle(
                                        color: Pallet1.fontcolornew,
                                        fontSize: Pallet1.heading4)),
                              ],
                            ),
                          ),
                        ),
                        selectedIndex == i
                            ? SingleChildScrollView(
                                child: Column(
                                  children: [
                                    Container(
                                      child: Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          if (data[i]['purchase'] != null)
                                            for (var item in data[i]
                                                ['purchase'])
                                              Padding(
                                                padding: EdgeInsets.all(
                                                    Pallet1.defaultPadding),
                                                child: Container(
                                                  decoration: BoxDecoration(
                                                      image: DecorationImage(
                                                          image: AssetImage(
                                                            'lotteryTicket.png',
                                                          ),
                                                          fit: BoxFit.fill)),
                                                  child: Padding(
                                                    padding: EdgeInsets.all(
                                                        Pallet1.defaultPadding *
                                                            2),
                                                    child: Row(
                                                        mainAxisAlignment:
                                                            MainAxisAlignment
                                                                .spaceBetween,
                                                        children: [
                                                          Padding(
                                                            padding: EdgeInsets
                                                                .all(Pallet1
                                                                    .defaultPadding),
                                                            child: Container(
                                                              padding: EdgeInsets
                                                                  .all(Pallet1
                                                                      .defaultPadding),
                                                              width: wdgtWidth *
                                                                  .65,
                                                              child: Column(
                                                                crossAxisAlignment:
                                                                    CrossAxisAlignment
                                                                        .start,
                                                                children: [
                                                                  Ticket(
                                                                    item: item,
                                                                  ),
                                                                  UnprocessedTickets(
                                                                    item: item,
                                                                  ),
                                                                  SizedBox(
                                                                    height: 10,
                                                                  ),
                                                                  // JackpotPercent(
                                                                  //     data:
                                                                  //         data,
                                                                  //     i: i)
                                                                ],
                                                              ),
                                                            ),
                                                          ),
                                                          Container(
                                                            width:
                                                                wdgtWidth * .17,
                                                            child: Column(
                                                              children: [
                                                                rowDailyProcess(
                                                                    item, true),
                                                                SizedBox(
                                                                    height: 10),
                                                                rowDailyProcess(
                                                                    item,
                                                                    false),
                                                                // rowweeklyProcess(
                                                                //     item),
                                                              ],
                                                            ),
                                                          )
                                                        ]),
                                                  ),
                                                ),
                                              ),
                                          if (data[i]['purchase'] == null)
                                            Text('No purchases found',
                                                style: TextStyle(
                                                    color: Pallet1.fontcolornew,
                                                    fontWeight: Pallet1.bold,
                                                    fontSize:
                                                        Pallet1.heading4)),
                                          SizedBox(
                                            height: 10,
                                          ),
                                        ],
                                      ),
                                    ),
                                    Padding(
                                      padding: EdgeInsets.all(
                                          Pallet1.defaultPadding),
                                      child: Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          Container(
                                            padding: EdgeInsets.symmetric(
                                                horizontal: 5),
                                            child: Text('Lucky Numbers',
                                                style: TextStyle(
                                                    color: Pallet1.fontcolornew,
                                                    fontSize:
                                                        Pallet1.heading4)),
                                          ),
                                          Row(
                                            // mainAxisAlignment:
                                            //     MainAxisAlignment
                                            //         .spaceBetween,
                                            children: [
                                              for (var items in data[i]
                                                  ['lucky_numbers'])
                                                LuckyNumbers(items: items),
                                              SizedBox(
                                                height: 10,
                                              ),
                                            ],
                                          ),
                                        ],
                                      ),
                                    ),
                                  ],
                                ),
                              )
                            : Container(
                                color: Colors.yellow,
                              )
                      ],
                    ),
                  ),
                ),
              )
        ],
      ),
    );
  }

  Column rowDailyProcess(item, bool isDaily) {
    return Column(
      children: [
        Text(isDaily == true ? 'Daily Draw' : 'Weekly Draw',
            style: TextStyle(
                color: Pallet1.fontcolornew, fontSize: Pallet1.heading4)),
        SizedBox(height: 10),
        Container(
          padding: EdgeInsets.all(Pallet1.defaultPadding),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(Pallet1.radius),
            color: Colors.grey[200],
          ),
          child: isDaily == true
              ? item['daily_processed'] == true && item['daily_winner'] == true
                  ? Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(Pallet1.radius),
                        color: Pallet1.fontcolornew,
                      ),
                      child: Icon(
                        Icons.check,
                        size: 15,
                        color: Pallet1.fontcolor,
                      ))
                  : Text('--')
              : item['weekly_processed'] == true &&
                      item['weekly_winner'] == true
                  ? Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(Pallet1.radius),
                        color: Pallet1.fontcolornew,
                      ),
                      child: Icon(
                        Icons.check,
                        size: 15,
                        color: Pallet1.fontcolor,
                      ))
                  : Text('--'),
        )
      ],
    );
  }
}

class JackpotPercent extends StatelessWidget {
  const JackpotPercent({
    Key key,
    @required this.data,
    @required this.i,
  }) : super(key: key);

  final List data;
  final int i;

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Text('Next Week Jackpot %(Cash : DUC)',
            style: TextStyle(
                color: Pallet1.fontcolornew, fontSize: Pallet1.normalfont)),
        SizedBox(width: 10),
        Text(
            data[i]["jackpot_percentage"].toString() +
                '% ' +
                ': ' +
                (100 - num.parse(data[i]["jackpot_percentage"])).toString() +
                '%',
            style: TextStyle(
                color: Pallet1.fontcolornew, fontSize: Pallet1.normalfont)),
      ],
    );
  }
}

class LuckyNumbers extends StatelessWidget {
  const LuckyNumbers({
    Key key,
    @required this.items,
  }) : super(key: key);

  final items;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.all(Pallet1.defaultPadding / 2),
      child: Container(
        width: 40,
        decoration: BoxDecoration(
          color: ([0, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89]
                      .contains(int.parse(items))) ==
                  true
              ? Pallet1.dashsmallcontainerback
              : Colors.white,
          shape: BoxShape.circle,
          border: Border.all(
              color: ([0, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89]
                          .contains(int.parse(items))) ==
                      true
                  ? Colors.transparent
                  : Colors.grey),
        ),
        child: Padding(
          padding: EdgeInsets.symmetric(vertical: Pallet1.defaultPadding),
          child: Center(
            child: Text(items.toString(),
                style: TextStyle(
                    color: ([0, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89]
                                .contains(int.parse(items))) ==
                            true
                        ? Pallet1.fontcolor
                        : Pallet1.dashsmallcontainerback,
                    fontSize: Pallet1.heading4)),
          ),
        ),
      ),
    );
  }
}

class Ticket extends StatelessWidget {
  const Ticket({
    Key key,
    @required this.item,
  }) : super(key: key);

  final item;

  @override
  Widget build(BuildContext context) {
    return Text('LOT' + item['id'].toString(),
        style: TextStyle(
            color: Pallet1.fontcolornew,
            fontSize: Pallet1.heading4,
            fontWeight: Pallet1.bold));
  }
}

class TextCreatedAt extends StatelessWidget {
  const TextCreatedAt({
    Key key,
    @required this.item,
  }) : super(key: key);

  final item;

  @override
  Widget build(BuildContext context) {
    return Text(
        item['created_at'].toString().split('T')[0] +
            " " +
            item['created_at']
                .toString()
                .split('T')[1]
                .toString()
                .split('.')[0]
                .toString(),
        style:
            TextStyle(color: Pallet1.fontcolornew, fontSize: Pallet1.heading4));
  }
}

class UnprocessedTickets extends StatelessWidget {
  const UnprocessedTickets({
    Key key,
    @required this.item,
  }) : super(key: key);

  final item;

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Wrap(
        children: [
          for (var number in item['numbers'])
            Padding(
              padding: EdgeInsets.all(Pallet1.defaultPadding),
              child: Container(
                padding: EdgeInsets.symmetric(vertical: Pallet1.defaultPadding),
                width: 75,
                decoration: BoxDecoration(
                    color: ([0, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89]
                                .contains(number)) ==
                            true
                        ? Pallet1.dashsmallcontainerback
                        : Pallet1.fontcolor,
                    border: Border.all(
                        color: ([0, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89]
                                    .contains(number)) ==
                                true
                            ? Colors.transparent
                            : Colors.grey),
                    borderRadius: BorderRadius.circular(Pallet1.radius)),
                child: Row(
                  children: [
                    Padding(
                      padding: EdgeInsets.symmetric(
                          horizontal: Pallet1.defaultPadding / 2),
                      child: Image.asset(
                        'lotteryicon.png',
                        width: 25,
                        height: 25,
                        color: ([0, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89]
                                    .contains(number)) ==
                                true
                            ? Pallet1.fontcolor
                            : Pallet1.fontcolornew,
                      ),
                    ),
                    Center(
                      child: Padding(
                        padding: EdgeInsets.all(Pallet1.defaultPadding / 2),
                        child: Text(
                          number.toString(),
                          style: TextStyle(
                              color: ([0, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89]
                                          .contains(number)) ==
                                      true
                                  ? Pallet1.fontcolor
                                  : Pallet1.dashsmallcontainerback,
                              fontSize: Pallet1.heading4),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
        ],
      ),
    );
  }
}
