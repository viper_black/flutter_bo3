import 'dart:math';

import 'package:centurion/lottery/common.dart';
import 'package:centurion/utils/utils.dart';
import 'package:flutter/material.dart';
import '../home.dart';
import 'config.dart';
import '../services/communication/index.dart' show HttpRequest;

class RedeemLottery extends StatefulWidget {
  final String amt;
  final double wdgtWidth, wdgtHeight;

  const RedeemLottery({Key key, this.amt, this.wdgtWidth, this.wdgtHeight})
      : super(key: key);
  @override
  _RedeemLotteryState createState() => _RedeemLotteryState();
}

class _RedeemLotteryState extends State<RedeemLottery> {
  int amtval;
  bool isMobile;
  bool isTab = false;

  List selecttickets = [];
  List<int> fab;
  bool specialselected;
  List specialticketselected = [];
  bool special = false;
  List total = [];
  int data;
  // ignore: unused_field
  Future<dynamic> _temp;

  List serverval = [
    {'amt': '5', 'special': '1', 'normal': '8'},
    {'amt': '10', 'special': '2', 'normal': '13'}
  ];
// ------------------------------------apply this to server values----------------------

  int maxnarmalselect;
  int maxspecialselect;

// ------------------------------------------------------------------------------------
  void initState() {
    super.initState();
    findtkenlength();
    fab = fibonacci1(12);
    print(fab);
    print("mmmmmmmmmmmm");
    print(widget.amt);
    amtval = int.parse(widget.amt);
    print(amtval.runtimeType);
    // test();
  }

  Future<bool> savelotterytoken(Map map) async {
    print(map);
    Map<String, dynamic> result =
        await HttpRequest.Post('lottery_token', Utils.constructPayload(map));
    print(map);
    if (Utils.isServerError(result)) {
      snack.snack(title: 'Something Went Wrong!');
      // ScaffoldMessenger.of(context)
      //     .showSnackBar(SnackBar(content: Text("Something Went Wrong!")));
    } else {
      data = result['response']['data']['id'];
      print(result['response']['data']);
      return true;
    }
  }

  findtkenlength() {
    print("lllllllllddddddddddddd");
    for (var i = 0; i < serverval.length; i++) {
      if (widget.amt == serverval[i]['amt']) {
        maxnarmalselect = int.parse(serverval[i]['normal']);
        maxspecialselect = int.parse(serverval[i]['special']);
      }
    }
    print("wwwwwwwwwwwwwwwwww");
  }

  List<int> fibonacci1(int n) {
    int last = 1;
    int last2 = 0;
    return List<int>.generate(n, (int i) {
      if (i < 2) return i;
      int curr = last + last2;
      last2 = last;
      last = curr;
      return curr;
    });
  }

  List testlist = [];
  List temptestlist = [];

  test() async {
//   var limit=0;
// while (
//   limit<=100

// ) {
//   temptestlist=[];
//     final _random = new Random();

// testlist=[];
//     var element = fab[_random.nextInt(fab.length)];

//     for (var i = 0; i < 100; i++) {
//       if (fab.contains(i)) {
//       } else {
//         testlist.add(i);
//       }
//     }

// for (var i = 0; i < 6; i++) {
//   temptestlist.add(testlist[_random.nextInt(testlist.length)]);
// }

// temptestlist.add(element);
// List finallist=temptestlist;
//    print(finallist);
// //  var map = {
// //             'product_id': 1,
// //             'amount': int.parse(widget.amt),
// //             'numbers': finallist,
// //           };
// //           bool result = await savelotterytoken(map);
// }
  }
  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
      builder: (context, constraints) {
        if (constraints.maxWidth < 320) {
          isMobile = true;
          isTab = false;
          return Padding(
            padding: EdgeInsets.fromLTRB(Pallet1.defaultPadding,
                Pallet1.defaultPadding, Pallet1.defaultPadding, 0),
            child: SingleChildScrollView(
              child: Column(
                children: [
                  profile(
                    width: widget.wdgtWidth,
                  ),
                  SizedBox(
                    height: 15,
                  ),
                  slots(
                    width: widget.wdgtWidth,
                  ),
                  SizedBox(
                    height: total.length == 0 ? 0 : 15,
                  ),
                  // tickets(
                  //   width: widget.wdgtWidth,
                  // ),
                  SizedBox(
                    height: 15,
                  ),
                  count(width: widget.wdgtWidth),
                  SizedBox(
                    height: 15,
                  ),
                  Row(
                    children: [
                      cancel(),
                      SizedBox(
                        width: 20,
                      ),
                      submit(
                        width: widget.wdgtWidth,
                      )
                    ],
                  ),
                  SizedBox(
                    height: 15,
                  ),
                ],
              ),
            ),
          );
        } else if (constraints.maxWidth >= 320 && constraints.maxWidth < 443) {
          isMobile = true;
          isTab = false;
          return Padding(
            padding: EdgeInsets.fromLTRB(Pallet1.defaultPadding,
                Pallet1.defaultPadding, Pallet1.defaultPadding, 0),
            child: SingleChildScrollView(
              child: Column(
                children: [
                  profile(
                    width: widget.wdgtWidth,
                  ),
                  SizedBox(
                    height: 15,
                  ),
                  slots(
                    width: widget.wdgtWidth,
                  ),
                  SizedBox(
                    height: total.length == 0 ? 0 : 15,
                  ),
                  // tickets(
                  //   width: widget.wdgtWidth,
                  // ),
                  SizedBox(
                    height: 15,
                  ),
                  count(width: widget.wdgtWidth),
                  SizedBox(
                    height: 15,
                  ),
                  Row(
                    children: [
                      cancel(),
                      SizedBox(
                        width: 20,
                      ),
                      submit(
                        width: widget.wdgtWidth,
                      )
                    ],
                  ),
                  SizedBox(
                    height: 15,
                  ),
                ],
              ),
            ),
          );
        } else if (constraints.maxWidth >= 443 && constraints.maxWidth <= 703) {
          isMobile = true;
          isTab = false;
          return Padding(
            padding: EdgeInsets.fromLTRB(Pallet1.defaultPadding,
                Pallet1.defaultPadding, Pallet1.defaultPadding, 0),
            child: SingleChildScrollView(
              child: Column(
                children: [
                  profile(
                    width: widget.wdgtWidth,
                  ),
                  SizedBox(
                    height: 15,
                  ),
                  slots(
                    width: widget.wdgtWidth,
                  ),
                  SizedBox(
                    height: total.length == 0 ? 0 : 15,
                  ),
                  // tickets(
                  //   width: widget.wdgtWidth,
                  // ),
                  SizedBox(
                    height: 15,
                  ),
                  count(width: widget.wdgtWidth),
                  SizedBox(
                    height: 15,
                  ),
                  Row(
                    children: [
                      cancel(),
                      SizedBox(
                        width: 20,
                      ),
                      submit(
                        width: widget.wdgtWidth,
                      )
                    ],
                  ),
                  SizedBox(
                    height: 15,
                  ),
                ],
              ),
            ),
          );
        } else if (constraints.maxWidth > 703 && constraints.maxWidth <= 1024) {
          isMobile = false;
          isTab = true;

          return Padding(
            padding: EdgeInsets.fromLTRB(Pallet1.defaultPadding,
                Pallet1.defaultPadding, Pallet1.defaultPadding, 0),
            child: SingleChildScrollView(
              child: Column(
                children: [
                  profile(
                    width: widget.wdgtWidth,
                  ),
                  SizedBox(
                    height: 15,
                  ),
                  slots(
                    width: widget.wdgtWidth,
                  ),
                  SizedBox(
                    height: total.length == 0 ? 0 : 15,
                  ),
                  // tickets(
                  //   width: widget.wdgtWidth,
                  // ),
                  SizedBox(
                    height: 15,
                  ),
                  count(
                    width: widget.wdgtWidth,
                  ),
                  SizedBox(
                    height: 15,
                  ),
                  Row(
                    children: [
                      cancel(),
                      SizedBox(
                        width: 20,
                      ),
                      submit(
                        width: widget.wdgtWidth,
                      )
                    ],
                  ),
                  SizedBox(
                    height: 15,
                  ),
                ],
              ),
            ),
          );
        } else {
          isMobile = false;
          isTab = false;
          return Padding(
            padding: EdgeInsets.symmetric(
              horizontal: Pallet1.leftPadding,
            ),
            child: SingleChildScrollView(
              child: Container(
                // color: Colors.green,
                child: Column(
                  children: [
                    profile(
                      width: widget.wdgtWidth,
                    ),
                    SizedBox(
                      height: 15,
                    ),
                    slots(
                      width: widget.wdgtWidth,
                    ),
                    SizedBox(
                      height: total.length == 0 ? 0 : 15,
                    ),
                    // tickets(
                    //   width: widget.wdgtWidth,
                    // ),
                    SizedBox(
                      height: 15,
                    ),
                    Wrap(
                      children: [
                        count(
                          width: (constraints.maxWidth > 1024 &&
                                  constraints.maxWidth <= 1500)
                              ? widget.wdgtWidth * .85
                              : widget.wdgtWidth * .6,
                        )
                      ],
                    ),
                    SizedBox(
                      height: 15,
                    ),
                    Row(children: [
                      cancel(),
                      SizedBox(
                        width: 20,
                      ),
                      submit(
                        width: widget.wdgtWidth,
                      ),
                    ]),
                    SizedBox(
                      height: 15,
                    ),
                  ],
                ),
              ),
            ),
          );
        }
      },
    );
  }

  Widget profile({double width}) {
    return Container(
      width: width,
      child: Text(
        "Lottery",
        style: TextStyle(
            color: Pallet1.fontcolornew,
            fontSize: Pallet1.heading1,
            fontWeight: Pallet1.bold),
      ),
    );
  }

  Widget tickets({double width}) {
    return Container(
      width: width,
      child: Text(
        "Lottery Tickets:",
        style: TextStyle(
            color: Pallet1.fontcolornew,
            fontSize: Pallet1.heading2,
            fontWeight: Pallet1.bold),
      ),
    );
  }

  Widget legends() {
    return Column(
      children: [
        rowLegend(Pallet1.fontcolornew, "Special Numbers"),
        SizedBox(height: 5),
        rowLegend(Pallet1.fontcolor, "Normal Numbers")
      ],
    );
  }

  Row rowLegend(Color c1, String text) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        Container(
          padding: EdgeInsets.all(Pallet1.defaultPadding),
          decoration: BoxDecoration(
              color: c1,
              shape: BoxShape.circle,
              border: Border.all(color: Pallet1.fontcolornew)),
          child: Text(''),
        ),
        SizedBox(width: 10),
        Text(
          text,
          style: TextStyle(
            color: Pallet1.fontcolornew,
            fontWeight: Pallet1.font600,
            fontSize: Pallet1.normalfont,
          ),
        ),
      ],
    );
  }

  Widget slots({double width}) {
    return Container(
      width: width,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          isMobile == true ? legends() : Container(),
          columnSlot(),
          // isMobile == true
          //     ? (selecttickets.length == 0 && specialticketselected.length == 0)
          //         ? Container()
          //         : columnSlot()
          //     : columnSlot(),
        ],
      ),
    );
  }

  Column columnSlot() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            rowSlot(),
            isMobile == true ? Container() : legends(),
          ],
        ),
        SizedBox(
          height: 10,
        ),
        isMobile == true ? columnNormalNumbers() : Container(),
      ],
    );
  }

  Row rowSlot() {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Container(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                "Special Numbers",
                style: TextStyle(
                  color: Pallet1.fontcolornew,
                  fontWeight: Pallet1.font600,
                  fontSize: Pallet1.heading3,
                ),
              ),
              SizedBox(
                height: 10,
              ),
              Wrap(
                spacing: 5,
                runSpacing: 5,
                children: [
                  for (var i = 0; i < maxspecialselect; i++)
                    Opacity(
                      opacity: specialticketselected.length - 1 >= i ? 1 : 0.6,
                      child: myContainer(specialticketselected.length - 1 >= i
                          ? specialticketselected[i]
                          : ''),
                    ),

                  // for (var i = 0; i < specialticketselected.length; i++)
                  //   myContainer(i, specialticketselected[i]),
                ],
              ),
            ],
          ),
        ),
        isMobile == true
            ? Container()
            : Padding(
                padding:
                    EdgeInsets.symmetric(horizontal: Pallet1.defaultPadding),
                child: Container(
                    width: 2,
                    height:
                        //  (selecttickets.length == 0 &&
                        //         specialticketselected.length == 0)
                        //     ? 25
                        //     :
                        75,
                    color: Pallet1.fontcolornew),
              ),
        isMobile == true ? Container() : columnNormalNumbers(),
      ],
    );
  }

  Widget columnNormalNumbers() {
    return Container(
      width: isMobile == true
          ? widget.wdgtWidth
          : isTab == true
              ? widget.wdgtWidth * .55
              : widget.wdgtWidth * .6,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            "Normal Numbers",
            style: TextStyle(
              color: Pallet1.fontcolornew,
              fontWeight: Pallet1.font600,
              fontSize: Pallet1.heading3,
            ),
          ),
          SizedBox(
            height: 10,
          ),
          Wrap(
            spacing: 5,
            runSpacing: 5,
            children: [
              for (var i = 0; i < maxnarmalselect; i++)
                Opacity(
                  opacity: selecttickets.length - 1 >= i ? 1 : 0.6,
                  child: myContainer(
                      selecttickets.length - 1 >= i ? selecttickets[i] : ''),
                ),
              // for (var i = 0; i < selecttickets.length; i++)
              //   myContainer(i, selecttickets[i]),
            ],
          ),
        ],
      ),
    );
  }

  myContainer(text) {
    return Container(
      width: 55,
      height: 55,
      padding: EdgeInsets.all(5),
      decoration: BoxDecoration(
          color: Pallet1.inner1,
          shape: BoxShape.circle,
          boxShadow: [
            BoxShadow(
              color: Colors.black12,
            )
          ]),
      child: Center(
        child: Padding(
          padding: EdgeInsets.all(Pallet1.defaultPadding),
          child: Text(text.toString(),
              style: TextStyle(
                  fontSize: Pallet1.heading5, color: Pallet1.fontcolor)),
        ),
      ),
    );
  }

  // myContainer(i, text) {
  //   return Container(
  //     width: 50,
  //     height: 50,
  //     padding: EdgeInsets.all(5),
  //     decoration: BoxDecoration(
  //         color: Pallet1.inner1,
  //         shape: BoxShape.circle,
  //         boxShadow: [
  //           BoxShadow(
  //             color: Colors.black12,
  //           )
  //         ]),
  //     child: Center(
  //       child: Padding(
  //         padding: EdgeInsets.all(Pallet1.defaultPadding),
  //         child: Text(text.toString(),
  //             style: TextStyle(
  //                 fontSize: Pallet1.normalfont, color: Pallet1.fontcolor)),
  //       ),
  //     ),
  //   );
  // }

  // mydialge() {
  //   return showDialog(
  //       context: context,
  //       barrierDismissible: false,
  //       builder: (BuildContext context) {
  //         return AlertDialog(
  //           scrollable: true,
  //           elevation: 24.0,
  //           backgroundColor: Pallet1.fontcolornew,
  //           shape: RoundedRectangleBorder(
  //               borderRadius: BorderRadius.all(Radius.circular(15.0))),
  //           title: Container(
  //             width: 450,
  //             child: Column(
  //               mainAxisSize: MainAxisSize.min,
  //               crossAxisAlignment: CrossAxisAlignment.start,
  //               children: [
  //                 Text('New Lottery',
  //                     style: TextStyle(
  //                         fontWeight: Pallet1.font500,
  //                         fontSize: Pallet1.heading3,
  //                         color: Pallet1.fontcolor)),
  //                 SizedBox(height: 10),
  //                 Text('Ticket No : LOT' + data.toString(),
  //                     style: TextStyle(
  //                         fontSize: Pallet1.heading5,
  //                         color: Pallet1.fontcolor)),
  //                 SizedBox(height: 10),
  //                 Text('Special Number',
  //                     style: TextStyle(
  //                         fontWeight: Pallet1.font500,
  //                         fontSize: Pallet1.heading4,
  //                         color: Pallet1.fontcolor)),
  //                 SizedBox(height: 15),
  //                 Container(
  //                   width: double.infinity,
  //                   decoration: BoxDecoration(
  //                       color: Colors.transparent,
  //                       borderRadius: BorderRadius.circular(6)),
  //                   child: Wrap(
  //                     spacing: 5,
  //                     runSpacing: 5,
  //                     children: [
  //                       for (var i = 0; i < specialticketselected.length; i++)
  //                         Container(
  //                           width: 50,
  //                           height: 50,
  //                           decoration: BoxDecoration(
  //                               color: Pallet1.inner1,
  //                               borderRadius: BorderRadius.circular(50)),
  //                           child: Center(
  //                             child: Text(specialticketselected[i].toString(),
  //                                 style: TextStyle(
  //                                     fontWeight: Pallet1.font500,
  //                                     fontSize: Pallet1.heading4,
  //                                     color: Pallet1.fontcolor)),
  //                           ),
  //                         ),
  //                     ],
  //                   ),
  //                 ),
  //                 SizedBox(height: 10),
  //                 Text('Normal Number',
  //                     style: TextStyle(
  //                         fontWeight: Pallet1.font500,
  //                         fontSize: Pallet1.heading4,
  //                         color: Pallet1.fontcolor)),
  //                 SizedBox(height: 15),
  //                 Container(
  //                   width: double.infinity,
  //                   decoration: BoxDecoration(
  //                       color: Colors.transparent,
  //                       borderRadius: BorderRadius.circular(6)),
  //                   child: Wrap(
  //                     spacing: 5,
  //                     runSpacing: 5,
  //                     children: [
  //                       for (var i = 0; i < selecttickets.length; i++)
  //                         Container(
  //                           width: 50,
  //                           height: 50,
  //                           decoration: BoxDecoration(
  //                               // boxShadow: [Pallet1.shadowEffect],
  //                               color: Pallet1.fontcolor,
  //                               borderRadius: BorderRadius.circular(50)),
  //                           child: Center(
  //                             child: Text(selecttickets[i].toString(),
  //                                 style: TextStyle(
  //                                     fontWeight: Pallet1.font500,
  //                                     fontSize: Pallet1.heading4,
  //                                     color: Pallet1.fontcolornew)),
  //                           ),
  //                         ),
  //                     ],
  //                   ),
  //                 ),
  //               ],
  //             ),
  //           ),
  //           actions: [
  //             PopupButton1(
  //               text: 'Continue',
  //               onpress: () async {
  //                 Navigator.of(context).pop();
  //                 lotterySuccess();
  //               },
  //             ),
  //             SizedBox(height: 10),
  //           ],
  //         );
  //       });
  // }

  // lotterySuccess() {
  //   return showDialog(
  //       context: context,
  //       barrierDismissible: false,
  //       builder: (BuildContext context) {
  //         return AlertDialog(
  //           scrollable: true,
  //           elevation: 24.0,
  //           backgroundColor: Pallet1.fontcolor,
  //           shape: RoundedRectangleBorder(
  //               borderRadius: BorderRadius.all(Radius.circular(15.0))),
  //           title: Text('Lottery Ticket Confirmation',
  //               maxLines: 2,
  //               style: TextStyle(
  //                   fontSize: Pallet1.heading4,
  //                   color: Pallet1.fontcolornew,
  //                   fontWeight: Pallet1.font500)),
  //           content: Container(
  //             width: 350,
  //             child: Column(
  //               mainAxisSize: MainAxisSize.min,
  //               children: [
  //                 ClipOval(
  //                   child: Container(
  //                     padding: EdgeInsets.all(15),
  //                     color: Colors.green,
  //                     child: Icon(
  //                       Icons.done,
  //                       color: Colors.white,
  //                     ),
  //                   ),
  //                 ),
  //                 SizedBox(height: 10),
  //                 Text("Lottery Ticket is Bought Successfully with",
  //                     textAlign: TextAlign.center,
  //                     style: TextStyle(
  //                         height: 1.4,
  //                         fontWeight: Pallet1.font500,
  //                         fontSize: Pallet1.heading4,
  //                         color: Pallet1.fontcolornew)),
  //                 SizedBox(height: 15),
  //                 Text('Ticket No : LOT' + data.toString(),
  //                     textAlign: TextAlign.center,
  //                     style: TextStyle(
  //                         fontSize: Pallet1.heading5,
  //                         fontWeight: Pallet1.subheading1wgt,
  //                         color: Pallet1.fontcolornew)),
  //                 SizedBox(height: 15),
  //               ],
  //             ),
  //           ),
  //           actions: [
  //             PopupButton1(
  //               text: 'Continue',
  //               buttoncolor: Pallet1.fontcolornew,
  //               textcolor: Pallet1.fontcolor,
  //               onpress: () {
  //                 Navigator.push(
  //                     context,
  //                     MaterialPageRoute(
  //                         builder: (BuildContext context) => Home(
  //                               route: 'winner_Announcement',
  //                             )));
  //               },
  //             ),
  //             SizedBox(height: 10),
  //           ],
  //         );
  //       });
  // }

  Widget submit({double width}) {
    return PopupButton1(
      text: "Submit",
      onpress: () async {
        if (specialticketselected.length < maxspecialselect) {
          snack.snack(
              title: 'You haven\'t selected all the special lottery numbers');
        } else if (selecttickets.length < maxnarmalselect) {
          snack.snack(
              title: 'You haven\'t selected all the normal lottery numbers');
        } else {
          var map = {
            'product_id': 1,
            'amount': int.parse(widget.amt),
            'numbers': total,
          };
          bool result = await savelotterytoken(map);
          print("mmmmmmmmmmmmmmmmmmmmmmmmmmmnnnnnnnnnnnn");
          print(result);
          if (result == true) {
            successdialog();
          } else {}
        }
      },
      vpadding: 10,
      hpadding: 10,
      buttoncolor: Pallet1.fontcolornew,
      textcolor: Pallet1.fontcolor,
      fontweight: Pallet1.font600,
    );
  }

  successdialog() {
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;
    return showDialog(
        context: context,
        barrierDismissible: false,
        barrierColor: Color(0xFF0027a6).withOpacity(0.4),
        builder: (BuildContext context) {
          return AlertDialog(
            elevation: 0,
            scrollable: true,
            backgroundColor: Colors.transparent,
            title: Stack(
              children: [
                Container(
                  color: Colors.transparent,
                  height: width > 310 && width < 373
                      ? 830
                      : height > 970
                          ? height * 0.7
                          : height > 765 && height <= 970
                              ? height * 0.8
                              : width < 450 && width >= 373
                                  ? 680
                                  : 600,
                  width: width > 650
                      ? 350
                      : width > 473 && width <= 650
                          ? 350
                          : width > 423 && width <= 473
                              ? 300
                              : width > 373 && width <= 423
                                  ? 250
                                  : 200,
                  child: Image.asset(
                    "s1.png",
                    fit: BoxFit.fill,
                  ),
                ),
                Positioned(
                  right: 0,
                  top: 10,
                  child: Container(
                    decoration: BoxDecoration(
                      color: Colors.red,
                      shape: BoxShape.circle,
                    ),
                    child: IconButton(
                      icon: Icon(Icons.close),
                      onPressed: () {
                        // Navigator.pop(context);
                        Navigator.pushReplacement(
                            context,
                            MaterialPageRoute(
                                builder: (BuildContext context) => Home(
                                      route: 'winner_Announcement',
                                    )));
                      },
                    ),
                  ),
                ),
                Positioned(
                  top: height > 970
                      ? height * 0.17
                      : height > 765 && height <= 970
                          ? height * 0.20
                          : width < 450 && width >= 373
                              ? 165
                              : width > 310 && width < 373
                                  ? 200
                                  : 150,
                  child: Container(
                    padding: EdgeInsets.all(20),
                    width: width > 650
                        ? 350
                        : width > 473 && width <= 650
                            ? 350
                            : width > 423 && width <= 473
                                ? 300
                                : width > 373 && width <= 423
                                    ? 250
                                    : 200,
                    // color: Colors.amber,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Container(
                          // padding: EdgeInsets.all(20),
                          color: Colors.transparent,
                          height: width > 460 ? height * 0.16 : 100,
                          width: width > 650
                              ? 360
                              : width > 473 && width <= 650
                                  ? 310
                                  : width > 423 && width <= 473
                                      ? 260
                                      : width > 373 && width <= 423
                                          ? 210
                                          : 170,
                          child: Image.asset(
                            "s2.png",
                            fit: BoxFit.fill,
                          ),
                        ),
                        SizedBox(height: 15),
                        Wrap(
                          children: [
                            Text("Lottery Ticket is Bought Successfully with",
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                    height: 1.4,
                                    fontWeight: Pallet1.font500,
                                    fontSize: Pallet1.heading4,
                                    color: Pallet1.fontcolornew)),
                          ],
                        ),
                        SizedBox(height: 15),
                        Wrap(
                          children: [
                            Text('Ticket No : LOT' + data.toString(),
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                    fontSize: Pallet1.heading5,
                                    fontWeight: Pallet1.subheading1wgt,
                                    color: Pallet1.fontcolornew)),
                          ],
                        ),
                        SizedBox(height: 15),
                        Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Container(
                              child: Text('Special Number',
                                  style: TextStyle(
                                      fontWeight: Pallet1.font500,
                                      fontSize: Pallet1.heading4,
                                      color: Pallet1.fontcolornew)),
                            ),
                          ],
                        ),
                        SizedBox(height: 10),
                        Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Container(
                              // width: double.infinity,
                              // decoration: BoxDecoration(
                              //     color: Colors.transparent,
                              //     borderRadius: BorderRadius.circular(6)),
                              child: Wrap(
                                spacing: 5,
                                runSpacing: 5,
                                children: [
                                  for (var i = 0;
                                      i < specialticketselected.length;
                                      i++)
                                    Container(
                                      width: 38,
                                      height: 38,
                                      decoration: BoxDecoration(
                                        color: Pallet1.inner1,
                                        shape: BoxShape.circle,
                                      ),
                                      child: Center(
                                        child: Text(
                                            specialticketselected[i].toString(),
                                            style: TextStyle(
                                                fontWeight: Pallet1.font500,
                                                fontSize: Pallet1.heading5,
                                                color: Pallet1.fontcolor)),
                                      ),
                                    ),
                                ],
                              ),
                            ),
                          ],
                        ),
                        SizedBox(height: 15),
                        Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text('Normal Number',
                                style: TextStyle(
                                    fontWeight: Pallet1.font500,
                                    fontSize: Pallet1.heading4,
                                    color: Pallet1.fontcolornew)),
                          ],
                        ),
                        SizedBox(height: 10),
                        Container(
                          // width: double.infinity,
                          // decoration: BoxDecoration(
                          //     color: Colors.transparent,
                          //     borderRadius: BorderRadius.circular(6)),
                          child: Wrap(
                            spacing: 5,
                            runSpacing: 5,
                            children: [
                              for (var i = 0; i < selecttickets.length; i++)
                                Container(
                                  width: 38,
                                  height: 38,
                                  decoration: BoxDecoration(
                                    // boxShadow: [Pallet1.shadowEffect],
                                    color: Pallet1.fontcolor,
                                    border: Border.all(
                                      color: Colors.black,
                                    ),
                                    shape: BoxShape.circle,
                                  ),
                                  child: Center(
                                    child: Text(selecttickets[i].toString(),
                                        style: TextStyle(
                                            fontWeight: Pallet1.font500,
                                            fontSize: Pallet1.heading5,
                                            color: Pallet1.fontcolornew)),
                                  ),
                                ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          );
        });
  }

  Widget cancel() {
    return Container(
        child: Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        PopupButton1(
          text: "Clear",
          onpress: () {
            setState(() {
              total = [];
              selecttickets = [];
              specialticketselected = [];
            });
          },
          vpadding: 10,
          hpadding: 10,
          buttoncolor: Pallet1.fontcolornew,
          textcolor: Pallet1.fontcolor,
          fontweight: Pallet1.font600,
        ),
        SizedBox(width: 20),
        PopupButton1(
          text: "Cancel",
          onpress: () {
            Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (BuildContext context) => Home(
                          route: 'dashboard',
                        )));
          },
          vpadding: 10,
          hpadding: 10,
          buttoncolor: Pallet1.fontcolornew,
          textcolor: Pallet1.fontcolor,
          fontweight: Pallet1.font600,
        ),
      ],
    ));
  }

  Widget count({width}) {
    return Container(
      width: width,
      child: Wrap(
        runSpacing: 5,
        spacing: 5,
        verticalDirection: VerticalDirection.down,
        children: [
          for (var index = 0; index < 100; index++)
            InkWell(
              onTap: () {
                setState(() {
                  if (fab.contains(index) == true) {
                    if (specialticketselected.contains(index) == true) {
                      special = false;
                      specialticketselected.remove(index);
                    } else if (specialticketselected.length + 1 <=
                        maxspecialselect) {
                      specialticketselected.add(index);
                    } else if (specialticketselected.length + 1 >
                        maxspecialselect) {
                      snack.snack(
                          title:
                              'You already Selected $maxspecialselect Special Lottery');
                    }
                  } else {
                    if (selecttickets.contains(index) == true) {
                      print("bbbbbvvvvvvvvvvvv");

                      print("eeeeeeeeeeeeeeeeeeeeeee");
                      for (var i = 0; i < selecttickets.length; i++) {
                        if (index == selecttickets[i]) {
                          print("eeeeeeeeeeeeeeeeeeeeeee");
                          print(i);
                          selecttickets.removeAt(i);
                        }
                      }
                      print("eeeeeeeeeeeeeeeeeeeeeee");
                    } else {
                      print("ggggggggggggggggg");
                      print(specialticketselected.length);
                      print(special);
                      if (selecttickets.length + 1 > maxnarmalselect) {
                        // ScaffoldMessenger.of(context)
                        //     .removeCurrentSnackBar();

                        // ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                        //   content: Text(
                        //       "You already Selected $maxnarmalselect Lottery"),
                        // ));

                        snack.snack(
                            title:
                                'You already Selected $maxnarmalselect Lottery');
                      } else if (selecttickets.length + 1 <= maxnarmalselect) {
                        if (fab.contains(index) == true) {
                        } else {
                          selecttickets.add(index);
                        }
                        print("vvvvvvvvvvvvvvvvvvvvvvvv");
                      }
                    }
                  }
                });

                // -----------------------find the total selected tickets-----------------------------------
                total = selecttickets + specialticketselected;
                // ----------------------------------------------------------------------------------
                print(specialticketselected);
                print(specialticketselected.length);
                print("ddddddddddd");
                print(selecttickets);
                print(selecttickets.length);
                print("eeeeeeeeeeeeee");
                print(selecttickets + specialticketselected);
              },
              child: Container(
                width: 70,
                padding: EdgeInsets.all(3),
                decoration: BoxDecoration(
                  // color: Colors.white,
                  color: total.contains(index) == true
                      ? specialticketselected.contains(index) == true
                          ? Colors.green
                          : Colors.red
                      : Pallet1.fontcolor,
                  shape: BoxShape.circle,
                  border: Border.all(
                    color: total.contains(index) == true
                        ? specialticketselected.contains(index) == true
                            ? Colors.green
                            : Colors.red
                        : Pallet1.fontcolor,
                  ),
                ),
                child: Container(
                  // width: 200,
                  padding: EdgeInsets.all(5),
                  decoration: BoxDecoration(
                      color: total.contains(index) == true
                          ? Pallet1.popupcontainerback
                          : fab.contains(index) == true
                              ? Pallet1.dashsmallcontainerback
                              : Colors.white,
                      shape: BoxShape.circle,
                      border: Border.all(
                        color: Pallet1.fontcolornew,
                      ),
                      boxShadow: [
                        BoxShadow(
                          color: Colors.black12,
                        )
                      ]),
                  child: Center(
                    child: Padding(
                      padding: EdgeInsets.all(Pallet1.defaultPadding),
                      child: Text(
                        index.toString(),
                        style: TextStyle(
                          fontSize: Pallet1.normalfont,
                          color: total.contains(index) == true
                              ? Colors.grey
                              : fab.contains(index) == true
                                  ? Colors.white
                                  : Pallet1.fontcolornew,
                        ),
                      ),
                    ),
                  ),
                ),
              ),
            ),
        ],
      ),
    );
  }
}
