import 'dart:math';

import 'package:centurion/lottery/common.dart';
import 'package:centurion/utils/utils.dart';
import 'package:flutter/material.dart';
import '../home.dart';
import 'config.dart';
import '../services/communication/index.dart' show HttpRequest;

class RedeemLottery extends StatefulWidget {
  final String amt;
  final double wdgtWidth, wdgtHeight;

  const RedeemLottery({Key key, this.amt, this.wdgtWidth, this.wdgtHeight})
      : super(key: key);
  @override
  _RedeemLotteryState createState() => _RedeemLotteryState();
}

class _RedeemLotteryState extends State<RedeemLottery> {
  int amtval;
  bool isMobile;
  List selecttickets = [];
  List<int> fab;
  bool specialselected;
  bool isTab;
  bool isDesktop;
  List specialticketselected = [];
  bool special = false;
  List total = [];
  int data;
  // ignore: unused_field
  Future<dynamic> _temp;

  List serverval = [
    {'amt': '5', 'special': '1', 'normal': '8'},
    {'amt': '10', 'special': '2', 'normal': '13'}
  ];
// ------------------------------------apply this to server values----------------------

  int maxnarmalselect;
  int maxspecialselect;

// ------------------------------------------------------------------------------------
  void initState() {
    super.initState();
    findtkenlength();
    fab = fibonacci1(12);
    print(fab);
    print("mmmmmmmmmmmm");
    print(widget.amt);
    amtval = int.parse(widget.amt);
    print(amtval.runtimeType);
    // test();
  }

  Future<bool> savelotterytoken(Map map) async {
    print(map);
    Map<String, dynamic> result =
        await HttpRequest.Post('lottery_token', Utils.constructPayload(map));
    print(map);
    if (Utils.isServerError(result)) {
      snack.snack(title: 'Something Went Wrong!');
      // ScaffoldMessenger.of(context)
      //     .showSnackBar(SnackBar(content: Text("Something Went Wrong!")));
    } else {
      data = result['response']['data']['id'];
      print(result['response']['data']);
      return true;
    }
  }

  findtkenlength() {
    print("lllllllllddddddddddddd");
    for (var i = 0; i < serverval.length; i++) {
      if (widget.amt == serverval[i]['amt']) {
        maxnarmalselect = int.parse(serverval[i]['normal']);
        maxspecialselect = int.parse(serverval[i]['special']);
      }
    }
    print("wwwwwwwwwwwwwwwwww");
  }

  List<int> fibonacci1(int n) {
    int last = 1;
    int last2 = 0;
    return List<int>.generate(n, (int i) {
      if (i < 2) return i;
      int curr = last + last2;
      last2 = last;
      last = curr;
      return curr;
    });
  }

  List testlist = [];
  List temptestlist = [];

  test() async {
//   var limit=0;
// while (
//   limit<=100

// ) {
//   temptestlist=[];
//     final _random = new Random();

// testlist=[];
//     var element = fab[_random.nextInt(fab.length)];

//     for (var i = 0; i < 100; i++) {
//       if (fab.contains(i)) {
//       } else {
//         testlist.add(i);
//       }
//     }

// for (var i = 0; i < 6; i++) {
//   temptestlist.add(testlist[_random.nextInt(testlist.length)]);
// }

// temptestlist.add(element);
// List finallist=temptestlist;
//    print(finallist);
// //  var map = {
// //             'product_id': 1,
// //             'amount': int.parse(widget.amt),
// //             'numbers': finallist,
// //           };
// //           bool result = await savelotterytoken(map);
// }
  }

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
      builder: (context, constraints) {
        if (constraints.maxWidth < 320) {
          isMobile = true;
          return Padding(
            padding: EdgeInsets.fromLTRB(Pallet1.defaultPadding,
                Pallet1.defaultPadding, Pallet1.defaultPadding, 0),
            child: SingleChildScrollView(
              child: Column(
                children: [
                  profile(
                    width: widget.wdgtWidth,
                  ),
                  SizedBox(
                    height: 15,
                  ),
                  slots(
                    width: widget.wdgtWidth,
                  ),
                  SizedBox(
                    height: total.length == 0 ? 0 : 15,
                  ),
                  // tickets(
                  //   width: widget.wdgtWidth,
                  // ),
                  SizedBox(
                    height: 15,
                  ),
                  count(
                    gridcount: 5,
                    fontSize: 10,
                    aspectratio: 0.98,
                    height: widget.wdgtHeight * 0.8,
                  ),
                  SizedBox(
                    height: 15,
                  ),
                  submit(
                    width: widget.wdgtWidth,
                  ),
                  SizedBox(
                    height: 15,
                  ),
                ],
              ),
            ),
          );
        } else if (constraints.maxWidth >= 320 && constraints.maxWidth < 443) {
          isMobile = true;
          return Padding(
            padding: EdgeInsets.fromLTRB(Pallet1.defaultPadding,
                Pallet1.defaultPadding, Pallet1.defaultPadding, 0),
            child: SingleChildScrollView(
              child: Column(
                children: [
                  profile(
                    width: widget.wdgtWidth,
                  ),
                  SizedBox(
                    height: 15,
                  ),
                  slots(
                    width: widget.wdgtWidth,
                  ),
                  SizedBox(
                    height: total.length == 0 ? 0 : 15,
                  ),
                  // tickets(
                  //   width: widget.wdgtWidth,
                  // ),
                  SizedBox(
                    height: 15,
                  ),
                  count(
                    gridcount: 5,
                    fontSize: 10,
                    height: widget.wdgtHeight * 0.75,
                  ),
                  SizedBox(
                    height: 15,
                  ),
                  submit(
                    width: widget.wdgtWidth,
                  ),
                  SizedBox(
                    height: 15,
                  ),
                ],
              ),
            ),
          );
        } else if (constraints.maxWidth >= 443 && constraints.maxWidth <= 703) {
          isMobile = true;
          return Padding(
            padding: EdgeInsets.fromLTRB(Pallet1.defaultPadding,
                Pallet1.defaultPadding, Pallet1.defaultPadding, 0),
            child: SingleChildScrollView(
              child: Column(
                children: [
                  profile(
                    width: widget.wdgtWidth,
                  ),
                  SizedBox(
                    height: 15,
                  ),
                  slots(
                    width: widget.wdgtWidth,
                  ),
                  SizedBox(
                    height: total.length == 0 ? 0 : 15,
                  ),
                  // tickets(
                  //   width: widget.wdgtWidth,
                  // ),
                  SizedBox(
                    height: 15,
                  ),
                  count(
                    gridcount: 10,
                    fontSize: 10,
                    height: widget.wdgtHeight * 0.9,
                  ),
                  SizedBox(
                    height: 15,
                  ),
                  submit(
                    width: widget.wdgtWidth,
                  ),
                  SizedBox(
                    height: 15,
                  ),
                ],
              ),
            ),
          );
        } else if (constraints.maxWidth > 703 && constraints.maxWidth <= 1024) {
          isTab = true;
          return Padding(
            padding: EdgeInsets.fromLTRB(Pallet1.defaultPadding,
                Pallet1.defaultPadding, Pallet1.defaultPadding, 0),
            child: SingleChildScrollView(
              child: Column(
                children: [
                  profile(
                    width: widget.wdgtWidth,
                  ),
                  SizedBox(
                    height: 15,
                  ),
                  slots(
                    width: widget.wdgtWidth,
                  ),
                  SizedBox(
                    height: total.length == 0 ? 0 : 15,
                  ),
                  // tickets(
                  //   width: widget.wdgtWidth,
                  // ),
                  SizedBox(
                    height: 15,
                  ),
                  count(
                    gridcount: 10,
                    fontSize: 10,
                    height: widget.wdgtHeight * 0.6,
                  ),
                  SizedBox(
                    height: 15,
                  ),
                  Row(
                    children: [
                      cancel(),
                      SizedBox(
                        width: 20,
                      ),
                      submit(
                        width: widget.wdgtWidth,
                      )
                    ],
                  ),
                  SizedBox(
                    height: 15,
                  ),
                ],
              ),
            ),
          );
        } else {
          isDesktop = true;
          return Padding(
            padding: EdgeInsets.symmetric(
              horizontal: Pallet1.leftPadding,
            ),
            child: SingleChildScrollView(
              child: Container(
                // color: Colors.green,
                child: Column(
                  children: [
                    profile(
                      width: widget.wdgtWidth,
                    ),
                    SizedBox(
                      height: 15,
                    ),
                    slots(
                      width: widget.wdgtWidth,
                    ),
                    SizedBox(
                      height: total.length == 0 ? 0 : 15,
                    ),
                    // tickets(
                    //   width: widget.wdgtWidth,
                    // ),
                    SizedBox(
                      height: 15,
                    ),
                    Wrap(
                      children: [
                        count(
                          gridcount: 10,
                          fontSize: 15,
                          height: widget.wdgtHeight * 0.5,
                        )
                      ],
                    ),
                    SizedBox(
                      height: 15,
                    ),
                    Row(children: [
                      cancel(),
                      SizedBox(
                        width: 20,
                      ),
                      submit(
                        width: widget.wdgtWidth,
                      ),
                    ]),
                    SizedBox(
                      height: 15,
                    ),
                  ],
                ),
              ),
            ),
          );
        }
      },
    );
  }

  Widget profile({double width}) {
    return Container(
      width: width,
      child: Text(
        "Lottery",
        style: TextStyle(
            color: Pallet1.fontcolornew,
            fontSize: Pallet1.heading1,
            fontWeight: Pallet1.bold),
      ),
    );
  }

  Widget tickets({double width}) {
    return Container(
      width: width,
      child: Text(
        "Lottery Tickets:",
        style: TextStyle(
            color: Pallet1.fontcolornew,
            fontSize: Pallet1.heading2,
            fontWeight: Pallet1.bold),
      ),
    );
  }

  Widget slots({double width}) {
    return Container(
      width: width,
      child: Wrap(
        children: [
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                selecttickets.length == 0 ? '' : "Normal Numbers",
                style: TextStyle(
                  color: Pallet1.fontcolornew,
                  fontWeight: Pallet1.font600,
                  fontSize: Pallet1.heading3,
                ),
              ),
              SizedBox(
                height: 10,
              ),
              Container(
                child: SingleChildScrollView(
                  scrollDirection: Axis.horizontal,
                  child: Row(
                    children: [
                      // for (var i = 0; i < 10; i++)
                      //   Container(
                      //     width: 10,
                      //     height: 15,
                      //     padding: EdgeInsets.all(5),
                      //     decoration: BoxDecoration(
                      //       color: Colors.white,
                      //       shape: BoxShape.rectangle,
                      //       borderRadius: BorderRadius.circular(3),
                      //       border: Border.all(
                      //         color: Pallet1.dashcontainerback,
                      //       ),
                      //     ),
                      //     child: Center(
                      //       child: Text(selecttickets.length == i + 1
                      //           ? (selecttickets.elementAt(i)).toString()
                      //           : ''),
                      //     ),
                      //   ),
                      for (var i = 0; i < selecttickets.length; i++)
                        Padding(
                          padding: const EdgeInsets.symmetric(
                            horizontal: 3,
                          ),
                          child: Container(
                            padding: EdgeInsets.all(10),
                            decoration: BoxDecoration(
                              color: Pallet1.dashcontainerback,
                              shape: BoxShape.rectangle,
                              borderRadius: BorderRadius.circular(3),
                              border: Border.all(
                                color: Pallet1.dashcontainerback,
                              ),
                            ),
                            child: Center(
                              child: Text(
                                (selecttickets[i].toString()),
                                style: TextStyle(
                                  color: Pallet1.fontcolor,
                                  fontWeight: Pallet1.font500,
                                ),
                              ),
                            ),
                          ),
                        ),
                    ],
                  ),
                ),
              ),
              SizedBox(
                height: specialticketselected.length == 0 ? 0 : 10,
              ),
              Text(
                specialticketselected.length == 0 ? '' : "Special Numbers",
                style: TextStyle(
                  color: Pallet1.fontcolornew,
                  fontWeight: Pallet1.font600,
                  fontSize: Pallet1.heading3,
                ),
              ),
              SizedBox(
                height: specialticketselected.length == 0 ? 0 : 10,
              ),
              Row(
                children: [
                  for (var i = 0; i < specialticketselected.length; i++)
                    Padding(
                      padding: const EdgeInsets.symmetric(
                        horizontal: 3,
                      ),
                      child: Container(
                        padding: EdgeInsets.all(10),
                        decoration: BoxDecoration(
                          color: Pallet1.fontcolornew,
                          shape: BoxShape.rectangle,
                          borderRadius: BorderRadius.circular(3),
                          border: Border.all(
                            color: Pallet1.fontcolornew,
                          ),
                        ),
                        child: Center(
                          child: Text(
                            (specialticketselected[i].toString()),
                            style: TextStyle(
                              color: Pallet1.fontcolor,
                              fontWeight: Pallet1.font500,
                            ),
                          ),
                        ),
                      ),
                    ),
                ],
              )
              // Text(
              //   "Lottery",
              //   style: TextStyle(
              //       color: Pallet1.fontcolornew,
              //       fontSize: Pallet1.heading1,
              //       fontWeight: Pallet1.bold),
              // ),
            ],
          ),
        ],
      ),
    );
  }

  mydialge() {
    return showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return AlertDialog(
            scrollable: true,
            elevation: 24.0,
            backgroundColor: Pallet1.fontcolornew,
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(15.0))),
            title: Container(
              width: 450,
              child: Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text('New Lottery',
                      style: TextStyle(
                          fontWeight: Pallet1.font500,
                          fontSize: Pallet1.heading3,
                          color: Pallet1.fontcolor)),
                  SizedBox(height: 10),
                  Text('Ticket No : LOT' + data.toString(),
                      style: TextStyle(
                          fontSize: Pallet1.heading5,
                          color: Pallet1.fontcolor)),
                  SizedBox(height: 10),
                  Text('Special Number',
                      style: TextStyle(
                          fontWeight: Pallet1.font500,
                          fontSize: Pallet1.heading4,
                          color: Pallet1.fontcolor)),
                  SizedBox(height: 15),
                  Container(
                    width: double.infinity,
                    // height: 60,
                    decoration: BoxDecoration(
                        // boxShadow: [Pallet1.shadowEffect],
                        color: Colors.transparent,
                        // border: Border.all(width: 1, color: Pallet1.fontcolor),
                        borderRadius: BorderRadius.circular(6)),
                    child: Wrap(
                      spacing: 5,
                      runSpacing: 5,
                      children: [
                        for (var i = 0; i < specialticketselected.length; i++)
                          Container(
                            width: 50,
                            height: 50,
                            decoration: BoxDecoration(
                                // border: Border.all(color: Colors.red),
                                // boxShadow: [Pallet1.shadowEffect],
                                color: Pallet1.inner1,
                                borderRadius: BorderRadius.circular(50)),
                            child: Center(
                              child: Text(specialticketselected[i].toString(),
                                  style: TextStyle(
                                      fontWeight: Pallet1.font500,
                                      fontSize: Pallet1.heading4,
                                      color: Pallet1.fontcolor)),
                            ),
                          ),
                      ],
                    ),
                  ),
                  SizedBox(height: 10),
                  Text('Normal Number',
                      style: TextStyle(
                          fontWeight: Pallet1.font500,
                          fontSize: Pallet1.heading4,
                          color: Pallet1.fontcolor)),
                  SizedBox(height: 15),
                  Container(
                    width: double.infinity,
                    // height: 80,
                    decoration: BoxDecoration(
                        // boxShadow: [Pallet1.shadowEffect],
                        color: Colors.transparent,
                        // border: Border.all(width: 1, color: Pallet1.fontcolor),
                        borderRadius: BorderRadius.circular(6)),
                    child: Wrap(
                      spacing: 5,
                      runSpacing: 5,
                      children: [
                        for (var i = 0; i < selecttickets.length; i++)
                          Container(
                            width: 50,
                            height: 50,
                            decoration: BoxDecoration(
                                // boxShadow: [Pallet1.shadowEffect],
                                color: Pallet1.fontcolor,
                                borderRadius: BorderRadius.circular(50)),
                            child: Center(
                              child: Text(selecttickets[i].toString(),
                                  style: TextStyle(
                                      fontWeight: Pallet1.font500,
                                      fontSize: Pallet1.heading4,
                                      color: Pallet1.fontcolornew)),
                            ),
                          ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
            actions: [
              // PopupButton1(
              //   text: 'Close',
              //   onpress: () {
              //     Navigator.of(context).pop();
              //   },
              // ),
              PopupButton1(
                text: 'Continue',
                onpress: () async {
                  Navigator.of(context).pop();
                  lotterySuccess();
                  // var map = {
                  //   'product_id': 1,
                  //   'amount': int.parse(widget.amt),
                  //   'numbers': total,
                  // };
                  // print(map);
                  // print("lotterymap");
                  // var val = await savelotterytoken(map);
                  // if (val == true) {
                  //   Navigator.push(
                  //       context,
                  //       MaterialPageRoute(
                  //           builder: (BuildContext context) => Home(
                  //                 route: 'winner_Announcement',
                  //               )));
                  // } else {
                  //   print("errorrrrrrrrrrrrrrrr");
                  // }
                },
              ),
              SizedBox(height: 10),
            ],
          );
        });
  }

  lotterySuccess() {
    return showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return AlertDialog(
            scrollable: true,
            elevation: 24.0,
            backgroundColor: Pallet1.fontcolor,
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(15.0))),
            title: Text('Lottery Ticket Confirmation',
                maxLines: 2,
                style: TextStyle(
                    fontSize: Pallet1.heading4,
                    color: Pallet1.fontcolornew,
                    fontWeight: Pallet1.font500)),
            content: Container(
              width: 350,
              child: Column(
                mainAxisSize: MainAxisSize.min,
                // crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  ClipOval(
                    child: Container(
                      padding: EdgeInsets.all(15),
                      color: Colors.green,
                      child: Icon(
                        Icons.done,
                        color: Colors.white,
                      ),
                    ),
                  ),
                  SizedBox(height: 10),
                  Text("Lottery Ticket is Bought Successfully with",
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          height: 1.4,
                          fontWeight: Pallet1.font500,
                          fontSize: Pallet1.heading4,
                          color: Pallet1.fontcolornew)),
                  SizedBox(height: 15),
                  Text('Ticket No : LOT' + data.toString(),
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          fontSize: Pallet1.heading5,
                          fontWeight: Pallet1.subheading1wgt,
                          color: Pallet1.fontcolornew)),
                  SizedBox(height: 15),
                ],
              ),
            ),
            actions: [
              // PopupButton1(
              //   text: 'Close',
              //   buttoncolor: Pallet1.fontcolornew,
              //   textcolor: Pallet1.fontcolor,
              //   onpress: () {
              //     Navigator.of(context).pop();
              //   },
              // ),
              PopupButton1(
                text: 'Continue',
                buttoncolor: Pallet1.fontcolornew,
                textcolor: Pallet1.fontcolor,
                onpress: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (BuildContext context) => Home(
                                route: 'winner_Announcement',
                              )));
                },
              ),
              SizedBox(height: 10),
            ],
          );
        });
  }

  Widget submit({double width}) {
    return PopupButton1(
      text: "Submit",
      onpress: () async {
        if (specialticketselected.length < maxspecialselect) {
          snack.snack(
              title: 'You haven\'t selected all the special lottery numbers');
          // ScaffoldMessenger.of(context).removeCurrentSnackBar();
          // ScaffoldMessenger.of(context).showSnackBar(SnackBar(
          //   content:
          //       Text("You haven't selected all the special lottery numbers"),
          // ));
        } else if (selecttickets.length < maxnarmalselect) {
          snack.snack(
              title: 'You haven\'t selected all the normal lottery numbers');

          // ScaffoldMessenger.of(context).removeCurrentSnackBar();
          // ScaffoldMessenger.of(context).showSnackBar(SnackBar(
          //   content:
          //       Text("You haven't selected all the normal lottery numbers"),
          // ));
        } else {
          var map = {
            'product_id': 1,
            'amount': int.parse(widget.amt),
            'numbers': total,
          };
          bool result = await savelotterytoken(map);
          print("mmmmmmmmmmmmmmmmmmmmmmmmmmmnnnnnnnnnnnn");
          print(result);
          if (result == true) {
            mydialge();
          } else {}

          // AwesomeDialog(
          //   width: 455,
          //   context: context,
          //   animType: AnimType.SCALE,
          //   dialogType: DialogType.SUCCES,
          //   body: Center(
          //     child: Text(
          //       'Success',
          //       style: TextStyle(fontStyle: FontStyle.italic),
          //     ),
          //   ),
          //   title: 'Lottery Ticket',
          //   desc: 'Submitted Successsfully',
          //   btnOkText: 'OK',
          //   btnOkOnPress: () async {
          //     var map = {
          //       'product_id': 1,
          //       'amount': int.parse(widget.amt),
          //       'numbers': total,
          //     };
          //     print(map);
          //     print("lotterymap");
          //     var val = await savelotterytoken(map);
          //     if (val == true) {
          //       Navigator.push(
          //           context,
          //           MaterialPageRoute(
          //               builder: (BuildContext context) => Home(
          //                     route: 'winner_Announcement',
          //                   )));
          //     } else {
          //       print("errorrrrrrrrrrrrrrrr");
          //     }
          //   },
          //   dismissOnTouchOutside: true,
          // )..show();
        }
      },
      vpadding: 10,
      hpadding: 10,
      buttoncolor: Pallet1.fontcolornew,
      textcolor: Pallet1.fontcolor,
      fontweight: Pallet1.font600,
    );
  }

  Widget cancel() {
    return Container(
        child: Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        PopupButton1(
          text: "Cancel",
          onpress: () {
            Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (BuildContext context) => Home(
                          route: 'dashboard',
                        )));
          },
          vpadding: 10,
          hpadding: 10,
          buttoncolor: Pallet1.fontcolornew,
          textcolor: Pallet1.fontcolor,
          fontweight: Pallet1.font600,
        ),
      ],
    ));
  }

  ScrollController _scrollController = ScrollController();
  Widget count(
      {int gridcount, double fontSize, double aspectratio, double height}) {
// ------------------Scrollbar------------------

    double width = MediaQuery.of(context).size.width;
    return RawScrollbar(
      controller: _scrollController,
      isAlwaysShown: true,
      radius: Radius.circular(10),
      thumbColor: Pallet1.fontcolornew,
      child: Container(
          width: isMobile == true
              ? widget.wdgtWidth - 50
              : isTab == true
                  ? widget.wdgtWidth - 200
                  : isDesktop == true
                      ? width > 1024 && width <= 1440
                          ? widget.wdgtWidth - 500
                          : width > 1440 && width <= 1500
                              ? widget.wdgtWidth - 600
                              : widget.wdgtWidth - 650
                      : widget.wdgtWidth,
          height: height,
          padding: EdgeInsets.all(10),
          decoration: BoxDecoration(
            border: Border.all(color: Pallet1.fontcolornew),
            borderRadius: BorderRadius.circular(10),
          ),
          child: GridView.builder(
              controller: _scrollController,
              itemCount: 100,
              gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                  crossAxisCount: gridcount,
                  childAspectRatio: aspectratio == null ? 0.90 : aspectratio,
                  mainAxisSpacing: isMobile == true ? 2 : 5,
                  crossAxisSpacing: isMobile == true ? 5 : 10),
              itemBuilder: (context, index) => InkWell(
                    onTap: () {
                      setState(() {
                        if (fab.contains(index) == true) {
                          if (specialticketselected.contains(index) == true) {
                            special = false;
                            specialticketselected.remove(index);
                          } else if (specialticketselected.length + 1 <=
                              maxspecialselect) {
                            specialticketselected.add(index);
                            // specialticketselected.length == 1
                            //     ? special = true
                            //     : special = false;
                          } else if (specialticketselected.length + 1 >
                              maxspecialselect) {
                            // ScaffoldMessenger.of(context).removeCurrentSnackBar();

                            // ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                            //   content: Text(
                            //       "You already Selected $maxspecialselect Special Lottery"),
                            // ));

                            snack.snack(
                                title:
                                    'You already Selected $maxspecialselect Special Lottery');
                          }
                        } else {
                          if (selecttickets.contains(index) == true) {
                            print("bbbbbvvvvvvvvvvvv");

                            print("eeeeeeeeeeeeeeeeeeeeeee");
                            for (var i = 0; i < selecttickets.length; i++) {
                              if (index == selecttickets[i]) {
                                print("eeeeeeeeeeeeeeeeeeeeeee");
                                print(i);
                                selecttickets.removeAt(i);
                              }
                            }
                            print("eeeeeeeeeeeeeeeeeeeeeee");
                          } else {
                            print("ggggggggggggggggg");
                            print(specialticketselected.length);
                            print(special);
                            if (selecttickets.length + 1 > maxnarmalselect) {
                              // ScaffoldMessenger.of(context)
                              //     .removeCurrentSnackBar();

                              // ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                              //   content: Text(
                              //       "You already Selected $maxnarmalselect Lottery"),
                              // ));

                              snack.snack(
                                  title:
                                      'You already Selected $maxnarmalselect Lottery');
                            } else if (selecttickets.length + 1 <=
                                maxnarmalselect) {
                              if (fab.contains(index) == true) {
                              } else {
                                selecttickets.add(index);
                              }
                              print("vvvvvvvvvvvvvvvvvvvvvvvv");
                            }
                          }
                        }
                      });

                      // -----------------------find the total selected tickets-----------------------------------
                      total = selecttickets + specialticketselected;
                      // ----------------------------------------------------------------------------------
                      print(specialticketselected);
                      print(specialticketselected.length);
                      print("ddddddddddd");
                      print(selecttickets);
                      print(selecttickets.length);
                      print("eeeeeeeeeeeeee");
                      print(selecttickets + specialticketselected);
                    },
                    child: Container(
                      padding: EdgeInsets.all(3),
                      decoration: BoxDecoration(
                        // color: Colors.white,
                        color: total.contains(index) == true
                            ? specialticketselected.contains(index) == true
                                ? Colors.green
                                : Colors.red
                            : Pallet1.fontcolor,
                        shape: BoxShape.circle,
                        border: Border.all(
                          color: total.contains(index) == true
                              ? specialticketselected.contains(index) == true
                                  ? Colors.green
                                  : Colors.red
                              : Pallet1.fontcolor,
                        ),
                      ),
                      child: Container(
                        padding: EdgeInsets.all(5),
                        decoration: BoxDecoration(
                            color: total.contains(index) == true
                                ? Pallet1.popupcontainerback
                                : fab.contains(index) == true
                                    ? Pallet1.dashsmallcontainerback
                                    : Colors.white,
                            shape: BoxShape.circle,
                            border: Border.all(
                              color: Pallet1.fontcolornew,
                            ),
                            boxShadow: [
                              BoxShadow(
                                color: Colors.black12,
                              )
                            ]),
                        child: Center(
                          child: Text(
                            index.toString(),
                            style: TextStyle(
                              fontSize: fontSize,
                              color: total.contains(index) == true
                                  ? Colors.grey
                                  : fab.contains(index) == true
                                      ? Colors.white
                                      : Pallet1.fontcolornew,
                            ),
                          ),
                        ),
                      ),
                    ),
                  ))),
    );
  }
}
