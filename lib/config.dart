import 'package:centurion/utils/utils.dart';
import 'package:flutter/material.dart';
import 'home.dart';

class ScreenSize {
  static const double verysmall = 320;
  static const double iphone = 500;
  static const double ipad = 900;
  static const double tab = 1366;
}

class Pallet {
  static List<String> options = ["red", "blue", "pink"];
  static bool isSwitched = false;
  static String option = 'red';
  static String previous = 'red';

  static changetheme(_option, context) async {
    previous = option;
    option = _option;
    if (option == 'red') {
      darkTheme();
    } else if (option == 'pink') {
      pinkTheme();
    } else {
      litTheme();
    }
    await Navigator.push(
      context,
      MaterialPageRoute(
          builder: (context) => Home(route: 'Dashboard', bottomsheet: true)),
    );
  }

  static List<String> languages = [
    "English",
// "Chinese",
    "Vietnamese",
    "Italian",
    "German",
    "Turkish",
    'Swedish'
  ];

  static String language = 'English';
//static String language1 = 'Chinese';
  static String language2 = 'Vietnamese';
  static String language3 = 'Italian';
  static String language4 = 'German';
  static String language5 = 'Turkish';
  static String language6 = 'Swedish';

  static changelang(String _language) {
    language = _language;
    // language1 = _language;
    language2 = _language;
    language3 = _language;
    language4 = _language;
    language5 = _language;
    language6 = _language;

    //print("printing");
    //print(_language);

    Utils.setSharedPrefereces(
            Utils.toJSONString({'language': _language}), 'language')
        .then((value) => Utils.getSharedPrefereces('language'));
  }

  static String helpmailnetwork = 'support@centuriongm.com';
  static String helpmailcustomer = 'support@centuriongm.com';

  static Color inner1 = Color(0xFF272a67);
  static Color inner2 = Colors.white;
  static Color fontcolor = Colors.white;
  static Color fontcolornew = Color(0XFF1034a6);
  static Color fontblccolor = Colors.black;
  static Color component = Color(0xFFb6c3db);
  static Color activelinkcolor = Color(0XFF626593);
  static Color activebordercolor = Color(0XFF1034A6);
  static Color buttonclr = Color(0XFF1034A6);
  static Color buttonback = Colors.white;
  static Color buttontxtclr = Color(0xFF2c539a);
  static Color lastpackback = Color(0XFF3553b4);
  static Color lastpackheadingback = Color(0XFF7186ca);
  static Color lastpackfooterback = Color(0XFFd0d7ed);
  static Color lastmembercircleavatarback = Color(0XFFd2d2d2);
  static Color dashcontainerback = Color(0XFF1034a6);
  static Color specialdashcontainerback = Color(0XFF1034a6);
  static Color dashsmallcontainerback = Color(0XFF3251b3);
  static Color popupcontainerback = Color(0xFF272a67);
  static Color sucesspopupcontainerback = Colors.green;
  static Color bonustrack = Color(0XFF2e5aea);
  static Color bonuscolor = Color(0XFF3251b3);
  static Color performanceline1 = Colors.amber[900];
  static Color performanceline2 = Colors.white;
  static Color performanceline3 = Colors.pink[900];
  static Color buttonsplashcolor = Colors.blueAccent;
  static Color applybuttontxtclr = Colors.white;
  static Color applybuttonclr = Colors.green;
  static Color cancelbtntxtclr = Colors.white;
  static Color cancelbuttonclr = Colors.red;
  static Color errortxt = Colors.red;
  static Color errorborder = Colors.red;
  static Color success = Color(0XFF23BF08);
  static Color error = Color(0XFFdc3545);
  static Color pending = Color(0XFFf5da45);
  static Color docbg = Color(0XFFefefef);
  static Color failed = Color(0XFFdb4845);
  static Color grey = Color(0XFF8C9091);
  static Color platinum = Color(0XFFaab4c7);
  static Color iron = Color(0XFF46474a);
  static Color aluminium = Color(0XFFacb0b8);
  static Color zinc = Color(0XFF607573);
  static Color nickel = Color(0XFFa2b8cd);
  static Color copper = Color(0XFF9e6756);
  static Color bronze = Color(0XFF7f4826);
  static Color silver = Color(0XFFc9cfe0);
  static Color gold = Color(0XFFc39d47);
  static Color titanium = Color(0XFF454556);
  static Color senator = Color(0XFFcc5a3b);
  static Color cenurion = Color(0XFF41d3ff);
  static Color empror = Color(0XFF895b43);
  static Color snackback = Color(0XFF333333);

  static double heading1 = 22;
  static double mobileheading1 = 15;
  static double heading2 = 18;
  static double heading3 = 16;
  static double heading4 = 15;
  static double heading5 = 14;
  static double heading6 = 13;
  static double heading7 = 12;
  static double heading8 = 11;
  static double heading9 = 10;
  static double subheading1 = 20;
  static FontWeight bold = FontWeight.bold;

  static FontWeight heading1wgt = FontWeight.w800;
  static FontWeight heading2wgt = FontWeight.w700;
  static FontWeight subheading1wgt = FontWeight.w700;
  static FontWeight font600 = FontWeight.w600;
  static FontWeight font500 = FontWeight.w500;
  static double normalfont = 15.0;
  static double notefont = 13.0;
  static Color notebg = Color(0XFFf1f1f1);
  static BoxShadow shadowEffect = BoxShadow(color: Colors.grey, blurRadius: 7);

  static TextStyle profiletext = TextStyle(
    color: Pallet.fontcolornew,
    fontSize: Pallet.heading3,
  );
  static TextStyle profiletextsubheading = TextStyle(
    color: Pallet.fontcolornew,
    fontSize: Pallet.heading3,
  );

  static LinearGradient linearGradient = LinearGradient(
    colors: <Color>[
      Color(0XFF00409E),
      Color(0XFF3E8BFE),
    ],
    stops: <double>[0.1, 0.9],
    // Setting alignment for the series gradient
    begin: Alignment.bottomLeft,
    end: Alignment.topRight,
  );
  static LinearGradient performanceGradient1 = LinearGradient(
    colors: <Color>[
      Color.fromARGB(100, 15, 107, 2),
      Color.fromARGB(1, 15, 107, 22)
    ],
    // stops: <double>[0.1, 0.9],
    // Setting alignment for the series gradient
    begin: Alignment.topCenter,
    end: Alignment.bottomCenter,
  );
  static LinearGradient goldColor = LinearGradient(
    colors: <Color>[
      Color(0XFFe0c775),
      Color(0XFFebdcbe),
      Color(0XFFc6a24b),
    ],
    // stops: <double>[0.1, 0.9],
    // Setting alignment for the series gradient
    begin: Alignment.topCenter,
    end: Alignment.bottomCenter,
  );
  static LinearGradient performanceGradient2 = LinearGradient(
    colors: <Color>[
      Color.fromARGB(1000, 255, 255, 255),
      Color.fromARGB(1, 255, 255, 255)
    ],
    // stops: <double>[0.1, 0.3, 0.6, 0.9],
    // Setting alignment for the series gradient
    begin: Alignment.topCenter,
    end: Alignment.bottomCenter,
  );
  static LinearGradient performanceGradient3 = LinearGradient(
    colors: <Color>[
      Color.fromARGB(205, 205, 205, 255),
      Color.fromARGB(1, 16, 136, 255),
    ],
    // stops: <double>[0.1, 0.3, 0.6, 0.9],
    // Setting alignment for the series gradient
    begin: Alignment.topCenter,
    end: Alignment.bottomCenter,
  );
  static double topPadding = 50;
  static double topPadding1 = 30;
  static double leftPadding = 50;
  static double defaultPadding = 10;
  static double radius = 10.0;

  static double vpadding = 5.0;
  static double hpadding = 10.0;

  static darkTheme() {
    inner1 = Color(0xFF262A69);
    inner2 = Colors.white;
    fontcolor = Colors.white;
    component = Color(0xFFb6c3db);
    buttonclr = Colors.white;
    buttontxtclr = Color(0xFF2c539a);
    errortxt = Colors.red;
    errorborder = Colors.red;
  }

  static litTheme() {
    inner1 = Colors.black;
    inner2 = Colors.grey;
    fontcolor = Colors.white;
    component = Color(0xFFb6c3db);
    buttonclr = Colors.white;
    buttontxtclr = Color(0xFF2c539a);
    errortxt = Colors.red;
    errorborder = Colors.red;
  }

  static pinkTheme() {
    inner1 = Colors.red;
    inner2 = Colors.pink;
    fontcolor = Colors.white;
    component = Color(0xFFb6c3db);
    buttonclr = Colors.white;
    buttontxtclr = Color(0xFF2c539a);
    errortxt = Colors.red;
    errorborder = Colors.red;
  }

  // List<Map<String,String>> documentsData = [
  //   {"name":"Rules of conduct","path":"document_ducatus/Rules_of_Conduct.pdf"},
  //   {"name":"Network Compensation Plan","path":"document_ducatus/2_June_2020_Network_Business_Compensation_Plan.pdf"},
  //   {"name":"Change Password","path":"document_ducatus/Change_password_1Aug2019.pdf"},
  //   {"name":"Ducatus Bonuses","path":"document_ducatus/Ducatus_Bonuses_Graphics_1Aug2019.pdf"},
  //   {"name":"Glossary","path":"document_ducatus/6_June_2020_DUCATUS_GLOSSARY.pdf"},
  //   {"name":"Africa and Middle East (Weekly Zoom Call invitation)","path":"document_ducatus/Africa_and_Middle_East_Weekly_Zoom_Call.pdf"},
  //   {"name":"Ducatus Corporate Profile","path":"document_ducatus/3_June_2020_Ducatus_Profile_v3.pdf"},
  //   {"name":"Forgot password","path":"document_ducatus/Forgot+password.pdf"},
  //   {"name":"2FA Guide","path":"document_ducatus/Guide+to+2FA.pdf"},
  //   {"name":"Network Registration Guide","path":"document_ducatus/Network_Registration_Guide_1Aug2019.pdf"},
  //   {"name":"Privacy Policy","path":"document_ducatus/Privacy_Policy.pdf"},
  //   {"name":"Terms and Conditions","path":"document_ducatus/Terms_and_Conditions.pdf"}];

}
