// import 'package:centurion/screens/myshop.dart';
// ignore: unused_import
import 'package:centurion/screens/individualprofile.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import '../config.dart';
import '../home.dart';
import '../services/business/cart.dart';
// import 'package:auto_size_text/auto_size_text.dart';
import '../common.dart';
import '../config/app_settings.dart';
import '../utils/utils.dart';
import '../services/communication/index.dart' show HttpRequest;

class Payout extends StatefulWidget {
  const Payout({Key key, this.wdgtWidth, this.wdgtHeight}) : super(key: key);
  final double wdgtWidth, wdgtHeight;
  @override
  _PayoutState createState() => _PayoutState();
}

class _PayoutState extends State<Payout> {
  bool isKyc;
  bool userActiveStatus = false;

  bool isEligible;
  String bankName = '';
  String beneficiary = '';
  String accountNumber = '';
  String bankIdentificationcode = '';

  Map<String, dynamic> pageDetails = {};
  _PayoutState() {
    setPageDetails();
  }

  void setPageDetails() async {
    String data = await Utils.getPageDetails('payout');
    setState(() {
      pageDetails = Utils.fromJSONString(data);
    });
  }

  bool selectedType = true;

  int selectedoption = 1;
  List<Map> payoutAccountDeatails = [
    {
      'id': 1,
      'title': 'Ducatus Coins',
      'payoutsprice': '0.00',
      'symbol': 'DUC ',
      'asset_type': 'duc',
      'icon': appSettings['SERVER_URL'] + '/' + 'assets/duc.png',
    },
    {
      'id': 2,
      'title': 'Cash / E-Wallet',
      'symbol': '\$ ',
      'payoutsprice': '0.00',
      'asset_type': 'cash',
      'icon': appSettings['SERVER_URL'] + '/' + 'assets/cash.png',
    },
  ];
  bool cashval = false;
  bool tradeval = false;
  IconData icon = Icons.arrow_drop_down;
  Fund cart = Fund();
  bool duc_kyc_status;
  double today_duc_price;
  double last_week_duc_payout;
  Future<String> temp;
  FocusNode duc = FocusNode();
  FocusNode adr1 = FocusNode();
  FocusNode acc = FocusNode();
  FocusNode adr2 = FocusNode();
  FocusNode btn = FocusNode();
  FocusNode btn1 = FocusNode();
  Map<String, dynamic> payoutSystemDetails = {};
  Map<String, dynamic> payoutUserDetails = {};
  double totalDucAmount = 0, totalCashAmount = 0;
  TextEditingController ducAmount = TextEditingController();
  String ducError;
  String addressError;
  String udscError;
  TextEditingController udscaddress = TextEditingController();
  TextEditingController cashAmount = TextEditingController();
  String cashError;
  String bankTransferDetails = 'Bank Transfer Details';
  TextEditingController address = TextEditingController();
  double maxBankAmount, minBankAmount;
  void initState() {
    temp = getDetails();

    super.initState();
  }

  // output(value) {
  //   //print(value);
  //   setState(() {
  //     payoutAccountDeatails[0]['payoutsprice'] =
  //         payoutss.payoutUserDetails['total_duc'].toString();
  //     payoutAccountDeatails[1]['payoutsprice'] =
  //         payoutss.payoutUserDetails['cash_account_bal'].toString();
  //     //print(payoutAccountDeatails);
  //   });
  // }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: temp,
      builder: (context, snapshot) {
        if (snapshot.hasError) {
          if (snapshot.error == 'Something Went Wrong') {
            return SomethingWentWrongMessage();
          } else {
            return Error(
              message: snapshot.error != null ? snapshot.error : '',
            );
          }
        } else if (snapshot.hasData) {
          return LayoutBuilder(
            // ignore: missing_return
            builder: (context, constraints) {
              if (constraints.maxWidth <= 730) {
                return SingleChildScrollView(
                    // child: payout(wdgtwidth: widget.wdgtWidth),
                    child: Padding(
                  padding: EdgeInsets.only(top: Pallet.topPadding1),
                  child: Column(
                    children: [
                      payoutHeader(wdgtwidth: widget.wdgtWidth),
                      payoutOptions(wdgtwidth: widget.wdgtWidth),
                      SizedBox(
                        height: 10,
                      ),
                      payoutForm(wdgtwidth: widget.wdgtWidth),
                    ],
                  ),
                ));
              }
              // else if (constraints.maxWidth > 730 &&
              //     constraints.maxWidth < ScreenSize.ipad) {
              //   // return SingleChildScrollView(
              //   //     child: payout(wdgtwidth: widget.wdgtWidth));
              //   return Padding(
              //     padding: EdgeInsets.all(Pallet.leftPadding),
              //     child: Column(
              //       // mainAxisAlignment: MainAxisAlignment.spaceAround,
              //       children: [
              //         Row(
              //           children: [
              //             payoutHeader(wdgtwidth: widget.wdgtWidth * .85),
              //           ],
              //         ),
              //         SizedBox(
              //           height: 40,
              //         ),
              //         Row(
              //           crossAxisAlignment: CrossAxisAlignment.start,
              //           // mainAxisAlignment: MainAxisAlignment.spaceBetween,
              //           children: [
              //             Column(
              //               children: [
              //                 payoutForm(wdgtwidth: widget.wdgtWidth * .45),
              //               ],
              //             ),
              //             // SizedBox(
              //             //   width: widget.wdgtWidth * .05,
              //             // ),
              //             Column(
              //               children: [
              //                 payoutOptions(wdgtwidth: widget.wdgtWidth * .45),
              //               ],
              //             )
              //           ],
              //         )
              //       ],
              //     ),
              //   );
              // }
              else {
                return Padding(
                  padding: EdgeInsets.only(
                      left: Pallet.leftPadding, top: Pallet.topPadding1),
                  child: Container(
                    width: widget.wdgtWidth * .85,
                    child: SingleChildScrollView(
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        // mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: [
                          payoutHeader(wdgtwidth: widget.wdgtWidth * .85),
                          SizedBox(
                            height: 40,
                          ),
                          Container(
                            // width: widget.wdgtWidth * .85,
                            child: Row(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Column(
                                  children: [
                                    payoutForm(
                                        wdgtwidth: widget.wdgtWidth * .45),
                                  ],
                                ),
                                // SizedBox(
                                //   width: widget.wdgtWidth * .05,
                                // ),
                                Column(
                                  children: [
                                    payoutOptions(
                                        wdgtwidth: widget.wdgtWidth * .4),
                                  ],
                                )
                              ],
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                );
              }
            },
          );
        }
        return Loader();
      },
    );
  }

  Widget payoutHeader({double wdgtwidth}) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisSize: MainAxisSize.min,
      children: [
        MyText(
            text: pageDetails["text1"],
            style: TextStyle(
              color: Pallet.fontcolornew,
              fontSize: Pallet.heading1,
            )),
        Padding(
          padding: EdgeInsets.symmetric(
              // horizontal: Pallet.defaultPadding,
              vertical: Pallet.defaultPadding * 2),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Container(
                  padding: EdgeInsets.all(Pallet.defaultPadding / 2),
                  width: wdgtwidth,
                  decoration: BoxDecoration(
                      color: Pallet.fontcolor,
                      boxShadow: [Pallet.shadowEffect],
                      borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(Pallet.radius),
                          bottomRight: Radius.circular(Pallet.radius))),
                  child: Center(
                    child: Row(
                      // mainAxisSize: MainAxisSize.min,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      // mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: [
                        MyText(
                          text: pageDetails['text15'],
                          style: TextStyle(
                              color: Pallet.dashcontainerback,
                              fontSize: Pallet.normalfont,
                              fontWeight: FontWeight.bold),
                        ),
                        Container(
                          padding: EdgeInsets.symmetric(
                              horizontal: Pallet.defaultPadding / 5),
                          width: wdgtwidth - 90,
                          child: MyText(
                            text: pageDetails['text16'],
                            style: TextStyle(
                                color: Pallet.dashcontainerback,
                                fontSize: Pallet.notefont,
                                height: 1.7,
                                fontWeight: FontWeight.w400),
                            textAlign: TextAlign.justify,
                          ),
                        )
                      ],
                    ),
                  )),
            ],
          ),
        ),
      ],
    );
  }

  Widget payoutOptions({double wdgtwidth}) {
    return Container(
      // color:Colors.green,
      width: wdgtwidth,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,

        // crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          MyText(
              text: pageDetails["text2"],
              style: TextStyle(
                  color: Pallet.dashcontainerback,
                  fontSize: Pallet.subheading1 - 3,
                  fontWeight: Pallet.subheading1wgt)),
          SizedBox(height: 20),
          Column(
            crossAxisAlignment: CrossAxisAlignment.center,

            // mainAxisAlignment: MainAxisAlignment.spaceBetween,

            // spacing: 30,
            // runSpacing: 30,
            children: [
              for (var i = 0; i < payoutAccountDeatails.length; i++)
                Padding(
                  padding:
                      EdgeInsets.symmetric(vertical: Pallet.leftPadding / 2),
                  child: Option(
                    // isBank: payoutAccountDeatails[i]['id'] == 2 ? true : false,
                    wdgtWidth: wdgtwidth,
                    icon: payoutAccountDeatails[i]['icon'],
                    title: payoutAccountDeatails[i]['title'],
                    subtitle: payoutAccountDeatails[i]["id"] == 2
                        ? payoutAccountDeatails[i]['symbol'].toString() +
                            payoutAccountDeatails[i]['payoutsprice'].toString()
                        : payoutAccountDeatails[i]['payoutsprice'].toString() +
                            " " +
                            payoutAccountDeatails[i]['symbol'].toString(),
                    selected: selectedoption == payoutAccountDeatails[i]["id"],
                    // onTap: () {
                    //   setState(() {
                    //     selectedoption = payoutAccountDeatails[i]["id"];
                    //     selectedType = !selectedType;
                    //     print(selectedType);
                    //   });
                    // },
                    // isSelected: selectedType == true ? true : false,
                    onselect: () {
                      setState(() {
                        address.text = '';
                        selectedoption = payoutAccountDeatails[i]["id"];
                        if (selectedoption == 1 && selectedType == false) {
                          print('print');
                          selectedType = true;
                        }
                        // payoutss.ducAmount.text = '';
                      });
                    },
                  ),
                ),
              SizedBox(height: 15),
              // if(selectedoption == 2)
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  InkWell(
                    onTap: () {
                      selectedoption = 2;
                      setState(() {
                        cashError = null;
                        udscaddress.text = '';
                        udscError = null;
                        cashAmount.text = '';
                        selectedType = !selectedType;
                        print(selectedType);
                      });
                    },
                    child: Container(
                      padding: EdgeInsets.all(Pallet.defaultPadding),
                      decoration: BoxDecoration(
                          color: selectedType == true
                              ? Pallet.dashcontainerback
                              : Pallet.fontcolor,
                          borderRadius: BorderRadius.circular(Pallet.radius)),
                      child: Text(
                        'Crypto Wallet',
                        style: TextStyle(
                            fontSize: 15.0,
                            color: selectedType == true
                                ? Pallet.fontcolor
                                : Pallet.dashcontainerback,
                            fontWeight: selectedType == true
                                ? FontWeight.bold
                                : FontWeight.w600),
                      ),
                    ),
                  ),
                  SizedBox(width: 10),
                  // Text(
                  //   ' / ',
                  //   style: TextStyle(
                  //       fontSize: 15.0,
                  //       color: Pallet.dashcontainerback,
                  //       fontWeight: FontWeight.w600),
                  // ),
                  InkWell(
                    onTap: () {
                      setState(() {
                        selectedoption = 2;
                        cashError = null;
                        udscaddress.text = '';
                        udscError = null;
                        cashAmount.text = '';
                        selectedType = !selectedType;
                        print(selectedType);
                      });
                    },
                    child: Container(
                      padding: EdgeInsets.all(Pallet.defaultPadding),
                      decoration: BoxDecoration(
                          color: selectedType == true
                              ? Pallet.fontcolor
                              : Pallet.dashcontainerback,
                          borderRadius: BorderRadius.circular(Pallet.radius)),
                      child: Text(
                        'Bank Account',
                        style: TextStyle(
                            fontSize: 15.0,
                            color: selectedType == true
                                ? Pallet.dashcontainerback
                                : Pallet.fontcolor,
                            fontWeight: selectedType == true
                                ? FontWeight.w600
                                : FontWeight.bold),
                      ),
                    ),
                  ),
                ],
              ),

              SizedBox(
                height: 10,
              ),
              if (selectedType == false)
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Container(
                      width: 250 + wdgtwidth * 0.05,
                      child: Wrap(
                        // runAlignment: WrapAlignment.center,
                        children: [
                          Text(
                            "(Pay-out will be done automatically to the given bank account every 15th of the month if this option is chosen.Transfer fees to be covered by the receipient)",
                            style: TextStyle(
                              color: Pallet.fontcolornew,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                )
            ],
          ),
        ],
      ),
    );
  }

  Widget payoutForm({double wdgtwidth}) {
    if (selectedoption == 1) {
      udscError = null;
      cashAmount.text = "";
      cashError = null;
      totalCashAmount = 0;
      udscaddress.text = "";
    } else if (selectedoption == 2) {
      ducAmount.text = "";
      ducError = null;
      addressError = null;
      totalDucAmount = 0;
      address.text = "";
    }
    return Container(
      width: wdgtwidth,
      child: Column(children: [
        selectedoption == 1
            ? payoutFormStructure(
                wdgtwidth,
                pageDetails["text3"] +
                    ' ' +
                    payoutSystemDetails['duc_used'].toString() +
                    " / " +
                    payoutSystemDetails['duc_max'].toString(),
                pageDetails["text17"],
                pageDetails["text5"],
                pageDetails["text14"],
                pageDetails["text19"],
                pageDetails["text6"],
                payoutAccountDeatails[0]['payoutsprice'].toString() +
                    ' ' +
                    payoutAccountDeatails[0]['symbol'].toString(),
                totalDucAmount.toString() + pageDetails["text8"],
                address,
                addressError,
                ducAmount,
                ducError,
                duc,
                adr1,
                Image.asset(
                  'cashbalance.png',
                  width: 20,
                ),
                validateDuc(),
                (value) {
                  validdateDuc(setState);
                },
              )
            : selectedType == true
                ? payoutFormStructure(
                    wdgtwidth,
                    pageDetails["text10"] +
                        ' ' +
                        payoutSystemDetails['cash_min'].toString() +
                        " / " +
                        payoutSystemDetails['cash_max'].toString(),
                    pageDetails["text20"],
                    pageDetails["text12"],
                    pageDetails["text4"],
                    pageDetails["text19"],
                    pageDetails["text13"],
                    "\$ " + payoutUserDetails['cash_account_bal'].toString(),
                    "\$ " + totalCashAmount.toString(),
                    udscaddress,
                    udscError,
                    cashAmount,
                    cashError,
                    adr1,
                    adr2,
                    Image.asset('payout.png', width: 20),
                    validateCash(),
                    (value) {
                      validdateCash(setState);
                    },
                  )
                : Container(
                    child: Column(
                      children: [
                        payoutFormStructure(
                          wdgtwidth,
                          // isEligible == true
                          //     ?
                          'Minimum Withdrawal Limit :' +
                              ' ' +
                              '\$' +
                              payoutSystemDetails['bank_min'].toString(),
                          // " / " +
                          // payoutSystemDetails['bank_max'].toString()
                          // : 'USD Withdrawal Limit :' +
                          //     ' ' +
                          //     payoutSystemDetails['bank_min'].toString() +
                          //     " / " +
                          //     payoutSystemDetails['bank_max'].toString() +
                          //     " ( InSufficient Minimal bal )",
                          // pageDetails["text20"],
                          '',
                          bankTransferDetails,
                          'Amount in USD',
                          "Please ensure that the Bank details provided above are Correct to avoid any Loss of Funds",
                          pageDetails["text13"],
                          "\$ " +
                              payoutUserDetails['cash_account_bal'].toString(),
                          "\$ " + totalCashAmount.toString(),
                          udscaddress,
                          udscError,
                          cashAmount,
                          cashError,
                          adr1,
                          adr2,
                          Image.asset(
                            'cashbalance.png',
                            width: 20,
                          ),
                          validateCash(),
                          (value) {
                            validdateBank(setState);
                          },
                        ),
                      ],
                    ),
                  )
      ]),
    );
  }

  validateCash() {
    return (value) {
      if (value.isEmpty) {
        setState(() {
          // udscError
          udscError = null;
        });
      } else if (value.toString().length < 3) {
        setState(() {
          udscError = pageDetails['text21'];
        });
      } else {
        setState(() {
          udscError = null;
        });
      }
    };
  }

  validateDuc() {
    return (value) {
      if (value.isEmpty) {
        setState(() {
          addressError = null;
        });
      } else if (value.toString().length < 3) {
        setState(() {
          addressError = pageDetails['text18'];
        });
      } else {
        setState(() {
          addressError = null;
        });
      }
    };
  }

  Column payoutFormStructure(
      double wdgtwidth,
      String text,
      text1,
      text2,
      text3,
      infoText,
      text4,
      text5,
      text6,
      contoller,
      errorText,
      controller1,
      errorText2,
      FocusNode node,
      nextnode,
      Image image,
      Function onChanged,
      validation) {
    return Column(
      children: [
        Container(
          // color: Colors.pink,
          // padding: EdgeInsets.only(top: Pallet.defaultPadding),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              MyText(
                  text: text,
                  style: TextStyle(
                    fontSize: Pallet.subheading1 - 3,
                    fontWeight: Pallet.subheading1wgt,
                    color: Pallet.fontcolornew,
                  )),
              SizedBox(height: 10.0),
              Textbox(
                focusnode: node,
                tabpress: (event) {
                  if (event.isKeyPressed(LogicalKeyboardKey.tab)) {
                    FocusScope.of(context).requestFocus(nextnode);
                  }
                },
                controller: controller1,
                errorText: errorText2,
                validation: validation,
                header: text3,
                label: text3,
              ),
              SizedBox(height: 5),
              MyText(
                  text: text1,
                  //  pageDetails["text20"],
                  style: TextStyle(
                    color: Pallet.fontcolornew,
                    fontSize: Pallet.heading5,
                    // fontWeight: FontWeight.w300
                  )),
              SizedBox(height: 10),
              MyText(
                  text: text2 == null ? "" : text2,
                  style: TextStyle(
                      color: Pallet.fontcolornew,
                      fontSize: Pallet.heading4,
                      fontWeight: FontWeight.w300)),
              SizedBox(height: 5),
              selectedoption == 1 ||
                      (selectedoption == 2 && selectedType == true)
                  ? RawKeyboardListener(
                      focusNode: adr2,
                      onKey: (event) {
                        if (event.isKeyPressed(LogicalKeyboardKey.tab)) {
                          FocusScope.of(context).requestFocus(adr2);
                        }
                      },
                      child: TextFormField(
                        textAlign: TextAlign.left,
                        inputFormatters: [
                          FilteringTextInputFormatter.allow(
                              RegExp("[a-zA-Z0-9]")),
                        ],
                        style: TextStyle(color: Pallet.dashcontainerback),
                        controller: contoller,
                        //  udscaddress,
                        // maxLength: 34,

                        onChanged: onChanged,
                        cursorColor: Pallet.dashcontainerback,
                        keyboardType:
                            TextInputType.numberWithOptions(decimal: true),
                        onEditingComplete: () {
                          FocusScope.of(context).requestFocus(btn1);
                        },
                        decoration: InputDecoration(
                          errorText: errorText,
                          border: OutlineInputBorder(
                            borderSide: BorderSide(
                                color: Pallet.dashcontainerback, width: 2),
                          ),
                          // labelText: label,
                          focusedBorder: OutlineInputBorder(
                              borderSide: BorderSide(
                                  color: Pallet.dashcontainerback, width: 2),
                              borderRadius:
                                  BorderRadius.circular(Pallet.radius)),
                          // fillColor: Pallet.dashcontainerback,
                          enabledBorder: OutlineInputBorder(
                              borderSide: BorderSide(
                                  color: Pallet.dashcontainerback, width: 2),
                              borderRadius:
                                  BorderRadius.circular(Pallet.radius)),
                          errorBorder: OutlineInputBorder(
                              borderSide:
                                  BorderSide(color: Colors.red, width: 2),
                              borderRadius:
                                  BorderRadius.circular(Pallet.radius)),
                          focusedErrorBorder: OutlineInputBorder(
                              borderSide:
                                  BorderSide(color: Colors.red, width: 2),
                              borderRadius:
                                  BorderRadius.circular(Pallet.radius)),
                          // labelStyle: TextStyle(
                          //   color: Pallet.fontcolor,
                          //   fontSize: Pallet.heading3,
                          // ),
                          filled: true,
                        ),
                      ),
                    )
                  : bankName == null ||
                          bankName == 'null' && beneficiary == null ||
                          bankName == 'null' && accountNumber == null ||
                          accountNumber == 'null' &&
                              bankIdentificationcode == null ||
                          bankIdentificationcode == 'null'
                      ? Container(
                          height: 150,
                          padding: EdgeInsets.all(Pallet.defaultPadding),
                          decoration: BoxDecoration(
                              borderRadius:
                                  BorderRadius.circular(Pallet.radius),
                              border: Border.all(color: Pallet.fontcolornew)),
                          child: Center(
                              child: InkWell(
                            onTap: () {
                              if (isKyc == true) {
                                showDialog(
                                    barrierDismissible: false,
                                    context: context,
                                    builder: (BuildContext context) {
                                      // return NoKycPopUp();
                                      return KycPopUp();
                                    });
                              } else {
                                Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                    builder: (BuildContext context) => Home(
                                      route: 'individual_profile',
                                      manageaddressonly: false,
                                      bankBool: true,
                                    ),
                                  ),
                                );
                              }
                            },
                            child: Row(
                              mainAxisSize: MainAxisSize.min,
                              children: [
                                Icon(
                                  Icons.add,
                                  color: Pallet.fontcolornew,
                                ),
                                SizedBox(
                                  width: 6,
                                ),
                                Text("Add Bank Details",
                                    style: TextStyle(
                                      fontSize: Pallet.heading3,
                                      // fontWeight: Pallet.subheading1wgt,
                                      color: Pallet.fontcolornew,
                                    )),
                              ],
                            ),
                          )))
                      : Container(
                          padding: EdgeInsets.all(Pallet.defaultPadding),
                          decoration: BoxDecoration(
                              borderRadius:
                                  BorderRadius.circular(Pallet.radius),
                              border: Border.all(color: Pallet.fontcolornew)),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  MyText(
                                      text: 'Name of Beneficiary',
                                      style: TextStyle(
                                        fontSize: Pallet.heading3,
                                        // fontWeight: Pallet.subheading1wgt,
                                        color: Pallet.fontcolornew,
                                      )),
                                  SizedBox(height: 5),
                                  MyText(
                                      text: beneficiary,
                                      style: TextStyle(
                                        fontSize: Pallet.normalfont,
                                        // fontWeight: Pallet.subheading1wgt,
                                        color: Pallet.fontcolornew,
                                      )),
                                  SizedBox(height: 10),
                                  MyText(
                                      text: 'Account Number',
                                      style: TextStyle(
                                        fontSize: Pallet.heading3,
                                        // fontWeight: Pallet.subheading1wgt,
                                        color: Pallet.fontcolornew,
                                      )),
                                  SizedBox(height: 5),
                                  MyText(
                                      text: accountNumber,
                                      style: TextStyle(
                                        fontSize: Pallet.normalfont,
                                        // fontWeight: Pallet.subheading1wgt,
                                        color: Pallet.fontcolornew,
                                      )),
                                ],
                              ),
                              // SizedBox(width:)
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  MyText(
                                      text: 'Name Of Bank',
                                      style: TextStyle(
                                        fontSize: Pallet.heading3,
                                        // fontWeight: Pallet.subheading1wgt,
                                        color: Pallet.fontcolornew,
                                      )),
                                  SizedBox(height: 5),
                                  MyText(
                                      text: bankName,
                                      style: TextStyle(
                                        fontSize: Pallet.normalfont,
                                        // fontWeight: Pallet.subheading1wgt,
                                        color: Pallet.fontcolornew,
                                      )),
                                  SizedBox(height: 10),
                                  MyText(
                                      text: 'SWIFT / BIC',
                                      style: TextStyle(
                                        fontSize: Pallet.heading3,
                                        // fontWeight: Pallet.subheading1wgt,
                                        color: Pallet.fontcolornew,
                                      )),
                                  SizedBox(height: 5),
                                  MyText(
                                      text: bankIdentificationcode,
                                      style: TextStyle(
                                        fontSize: Pallet.normalfont,
                                        // fontWeight: Pallet.subheading1wgt,
                                        color: Pallet.fontcolornew,
                                      )),
                                ],
                              ),
                            ],
                          ),
                        ),
              SizedBox(height: 5),
              MyText(
                  text: infoText,
                  style: TextStyle(
                    color: Pallet.fontcolornew,
                    fontSize: Pallet.heading4,
// fontWeight: FontWeight.w300
                  )),
            ],
          ),
        ),
        SizedBox(height: 20),
        Container(
          // padding: EdgeInsets.all(15.0),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(Pallet.radius),
            color: Pallet.fontcolor,
            boxShadow: [Pallet.shadowEffect],
          ),
          padding: EdgeInsets.symmetric(
              horizontal: Pallet.defaultPadding,
              vertical: Pallet.defaultPadding + wdgtwidth * 0.04),
          // width: wdgtwidth,
          // height: wdgtwidth * 0.35,
          width: wdgtwidth,
          // padding: EdgeInsets.all(15.0),
          // width: wdgtwidth,
          // height: wdgtwidth,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                    child: Row(
                      children: [
                        CircleAvatar(
                            // backgroundColor: Color(0XFFd2d2d2),
                            backgroundColor: Pallet.dashcontainerback,
                            radius: 20,
                            child: image),
                        SizedBox(width: 10.0),
                        MyText(
                          text: text4,
                          style: TextStyle(
                              fontSize: 18.0, color: Pallet.dashcontainerback),
                        ),
                      ],
                    ),
                  ),
                  MyText(
                    text: text5,
                    style: TextStyle(
                        fontSize: 16.0,
                        color: Pallet.dashcontainerback,
                        fontWeight: FontWeight.w600),
                  ),
                ],
              ),
              SizedBox(height: 10),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  MyText(
                    text: pageDetails["text7"],
                    style: TextStyle(
                        fontSize: 16.0,
                        color: Pallet.dashcontainerback,
                        fontWeight: FontWeight.w400),
                  ),
                  MyText(
                    text: text6,
                    style: TextStyle(
                        fontSize: 20.0,
                        color: Pallet.dashcontainerback,
                        fontWeight: FontWeight.w600),
                  ),
                  SizedBox(height: 20),
                  RawKeyboardListener(
                    focusNode: btn,
                    onKey: (event) {
                      if (event.isKeyPressed(LogicalKeyboardKey.enter)) {
                        setState(() {
                          pyout(setState);
                        });
                      }
                    },
                    child: Container(
                      width: double.infinity,
                      child: AbsorbPointer(
                        // absorbing: isEligible == false ? true : false,
                        absorbing: false,
                        child: CustomButton(
                          onpress: () {
                            userActiveStatus == false
                                ? snack.snack(
                                    title:
                                        'Admin has blocked your transfer permissions')
                                : setState(() {
                                    pyout(setState);
                                  });
                          },
                          vpadding: 12,
                          text: pageDetails['text9'],
                          buttoncolor: Pallet.fontcolornew,
                          textcolor: Pallet.fontcolor,
                          textsize: Pallet.heading4,
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ],
    );
  }

  Future<String> getDetails() async {
    Map<String, dynamic> result =
        await HttpRequest.Post('getPayoutDetails', Utils.getSiteID());
    if (Utils.isServerError(result)) {
      //  result['response']['error'] == 'negative_amount'
      //     ? snack.snack(
      //         title:
      //             "Blocked because of Negative balance in any of your account")
      //     : snack.snack(title: "Something went wrong");
      return throw (await Utils.getMessage(result['response']['error']));
    } else {
      print('ooooooooooooooooooooooooo');
      print(result['response']['data']);
      userActiveStatus = result['response']['data']['user_active_status'];
      isKyc = result['response']['data']['system_data']['ask_kyc'];
      payoutSystemDetails = result['response']['data']['system_data'];
      duc_kyc_status =
          result['response']['data']['system_data']['duc_kyc_status'];

      print('DUC_KYC_STATUS: $duc_kyc_status}');
      today_duc_price =
          result['response']['data']['system_data']['today_duc_price'];
      print('TODAY_DUC_PRICE: $today_duc_price}');
      last_week_duc_payout =
          result['response']['data']['system_data']['last_week_duc_payout'];
      print('LAST_WEEK_DUC_PAYOUT: $last_week_duc_payout}');
      payoutUserDetails = result['response']['data']['user_data'];
      payoutAccountDeatails[0]['payoutsprice'] = payoutUserDetails['total_duc'];
      payoutAccountDeatails[1]['payoutsprice'] =
          payoutUserDetails['cash_account_bal'];

      bankName = result['response']['data']['bank_data']['bank_name'];
      beneficiary = result['response']['data']['bank_data']['beneficiary'];
      accountNumber = result['response']['data']['bank_data']['account_number'];
      bankIdentificationcode =
          result['response']['data']['bank_data']['bank_identification_code'];
      minBankAmount = payoutSystemDetails['bank_min'];
      maxBankAmount = payoutSystemDetails['bank_max'];
      minBankAmount > maxBankAmount ? isEligible = false : isEligible = true;
      print('PPPPPPPPPPPPPP');
      print(isEligible);
      return Utils.toJSONString(
          await Utils.convertMessage(result['response']['data']));
    }
  }

  validdateDuc(setState) {
    if (ducAmount.text.length <= 0) {
      setState(() {
        print('IIIIIIIIIII');
        print(ducAmount.text);
        ducError = null;
      });
    } else if (payoutUserDetails['total_duc'] == 0) {
      setState(() {
        print('IIIIIIIIIII');
        print(ducAmount.text);
        ducError = pageDetails['text22'];
      });
    } else if (ducAmount.text.length > 0) {
      setState(() {
        totalDucAmount = double.parse(ducAmount.text);

        /// payoutSystemDetails['duc_price'];
      });

      print('DUC_KYC_STATUS: $duc_kyc_status}');

      if (totalDucAmount >= payoutSystemDetails['duc_min']) {
        if (totalDucAmount <= payoutUserDetails['total_duc']) {
          if (totalDucAmount <=
              payoutSystemDetails['duc_max'] -
                  payoutSystemDetails['duc_used']) {
            if (duc_kyc_status == true) {
              var ducval = totalDucAmount * today_duc_price;
              print('DUCVAL: $ducval}');

              var res = ducval + last_week_duc_payout;
              print('RES: $res}');
              if (res > 1000) {
                double ducmaxval = 1000 / today_duc_price;
                setState(() {
                  ducError =
                      ' Max Withdrawable amount is ${ducmaxval.toStringAsFixed(2)} DUC per Week';
                });
              } else {
                setState(() {
                  ducError = null;
                });
              }
            } else {
              setState(() {
                ducError = null;
              });
            }
          } else {
            setState(() {
              ducError = ' Max Withdrawable amount is 50000 DUC per Month';
              //  pageDetails['text23'];
            });
          }
        } else if (totalDucAmount == 0 || totalDucAmount == null) {
          setState(() {
            ducError = pageDetails['text22'];
          });
        } else {
          setState(() {
            ducError = pageDetails['text22'];
          });
        }
      } else {
        setState(() {
          ducError = pageDetails['text24'];
        });
      }
    } else {
      setState(() {
        ducError = null;
        totalDucAmount = 0;
      });
    }
  }

  validdateCash(setState) {
    if (cashAmount.text.length > 0) {
      setState(() {
        totalCashAmount = double.parse(cashAmount.text);
      });

      if (totalCashAmount >= payoutSystemDetails['cash_min']) {
        if (totalCashAmount <= payoutUserDetails['cash_account_bal']) {
          if (totalCashAmount <= payoutSystemDetails['cash_max']) {
            setState(() {
              cashError = null;
            });
          } else {
            setState(() {
              cashError = pageDetails['text23'];
            });
          }
        } else {
          setState(() {
            cashError = pageDetails['text22'];
          });
        }
      } else if (payoutUserDetails['cash_account_bal'] == 0) {
        setState(() {
          cashError = pageDetails['text22'];
        });
      } else {
        setState(() {
          cashError = pageDetails['text24'];
        });
      }
    } else {
      setState(() {
        cashError = null;
        totalCashAmount = 0;
      });
    }
  }

  validdateBank(setState) {
    if (cashAmount.text.length > 0) {
      setState(() {
        totalCashAmount = double.parse(cashAmount.text);
      });

      if (totalCashAmount >= payoutSystemDetails['bank_min']) {
        if (totalCashAmount <= payoutUserDetails['cash_account_bal']) {
          if (totalCashAmount <= payoutSystemDetails['bank_max']) {
            setState(() {
              cashError = null;
            });
          } else {
            setState(() {
              // cashError = pageDetails['text23'];
              cashError =
                  "Max Withdrawable amt is \"\$$maxBankAmount\" as per your last month Closing bal";
              // "Max Withdrawable Limit for the Month is $maxBankAmount as per you last month's Closing balance";
            });
          }
        } else {
          setState(() {
            cashError = pageDetails['text22'];
          });
        }
      } else if (payoutUserDetails['cash_account_bal'] == 0) {
        setState(() {
          cashError = pageDetails['text22'];
        });
      } else {
        setState(() {
          cashError = pageDetails['text24'];
        });
      }
    } else {
      setState(() {
        cashError = null;
        totalCashAmount = 0;
      });
    }
  }

  pyout(setstate1) async {
    // ignore: unused_local_variable

    // ignore: unused_local_variable
    bool isValid = true;

    bool status = false;
    // ignore: unused_local_variable
    Map<String, dynamic> _map = {};

    if (selectedoption == 1) {
      print('PPPPPPPPPPPPPPPPPPPPPPPPPPPPP');
      udscaddress.text = "";
      cashAmount.text = "";

      cashError = null;
      udscError = null;
      if (ducAmount.text.length < 1) {
        isValid = false;
        ducError = pageDetails['text25'];
      }
      if (address.text.length < 3) {
        isValid = false;
        addressError = pageDetails['text18'];
      } else if (addressError != null || ducError != null) {
        //print(ducError);
        //print(addressError);
        isValid = false;
        // final snackBar = SnackBar(content: MyText(text:'conditions not met'));
        // ScaffoldMessenger.of(context).showSnackBar(snackBar);
      } else if (addressError == null && ducError == null) {
        status = true;
      }
    }
    if (selectedoption == 2) {
      if (selectedType == true) {
        print('SSSSSSSSSSSSSSSSSSSSSSssssssssssssssssss');
        print(selectedType);
        ducAmount.text = "";
        address.text = "";
        addressError = null;
        ducError = null;
        if (cashAmount.text.length < 1) {
          isValid = false;
          cashError = pageDetails['text25'];
        }
        if (udscaddress.text.length < 3) {
          isValid = false;
          udscError = pageDetails['text21'];
        } else if (udscError != null || cashError != null) {
          //print(cashError);
          //print(udscError);
          isValid = false;
          // final snackBar = SnackBar(content: MyText(text:'conditions not met'));
          // ScaffoldMessenger.of(context).showSnackBar(snackBar);
        } else if (udscError == null && cashError == null) {
          status = true;
        }
      } else {
        print('SSSSSSSSSSSSSSSSSSSSSSssssssssssssssssss');
        print(selectedType);
        ducAmount.text = "";
        address.text = "";
        addressError = null;
        ducError = null;
        if (cashAmount.text.length < 1) {
          isValid = false;
          cashError = pageDetails['text25'];
        }
        // if (udscaddress.text.length < 3) {
        //   isValid = false;
        //   udscError = pageDetails['text21'];
        // }
        else if (cashError != null) {
          //print(cashError);
          //print(udscError);
          isValid = false;
          // final snackBar = SnackBar(content: MyText(text:'conditions not met'));
          // ScaffoldMessenger.of(context).showSnackBar(snackBar);
        } else if (cashError == null) {
          status = true;
        }
      }
    }

    if (status == true) {
      if (selectedoption == 1) {
        udscaddress.text = "";
        cashAmount.text = "";
        cashError = null;
        udscError = null;
        status = false;
        callAPI(
            _map = {
              "product_id": 1,
              "amount": double.parse(ducAmount.text),
              "asset_type": "duc",
              "address_line": address.text,
              'isBankTransfer': false
            },
            payoutSystemDetails['allow_payout_duc'] == false,
            'You already have a Pending Payout request for DUC');
        // if (isKyc == true) {
        //   showDialog(
        //       barrierDismissible: false,
        //       context: context,
        //       builder: (BuildContext context) {
        //         return KycPopUp();
        //       });
        // } else {
        // if (payoutSystemDetails['allow_payout_duc'] == false) {
        //   print('YES CASH');
        //   snack.snack(
        //       title: 'You already have a Pending Payout request for DUC');

        //   // final snackBar = SnackBar(
        //   //     content: MyText(text:await Utils.getMessage(
        //   //         'You already have one pending payout')));
        //   // ScaffoldMessenger.of(context).showSnackBar(snackBar);
        // } else {
        // _map = {
        //   "product_id": 1,
        //   "amount": double.parse(ducAmount.text),
        //   "asset_type": "duc",
        //   "address_line": address.text,
        //   'isBankTransfer': false
        // };
        //   waiting.waitpopup(context);

        //   Map<String, dynamic> _result = await HttpRequest.Post(
        //       'c_rms_payout', Utils.constructPayload(_map));
        //   if (Utils.isServerError(_result)) {
        //     Navigator.pop(context);
        //     isKyc == true
        //         ? print("kyc")
        //         : showDialog(
        //             context: context,
        //             builder: (_) => SucessPopup(
        //                 message: pageDetails['text26'],
        //                 route: 'request_payout'));
        //     // final snackBar = SnackBar(
        //     //     content:
        //     //         MyText(text:await Utils.getMessage(_result['response']['error'])));
        //     // ScaffoldMessenger.of(context).showSnackBar(snackBar);
        //   } else {
        //     udscaddress.text = "";
        //     cashAmount.text = "";
        //     ducAmount.text = "";
        //     address.text = "";
        //     totalCashAmount = 0;
        //     totalDucAmount = 0;
        //     addressError = null;
        //     ducError = null;
        //     cashError = null;
        //     udscError = null;
        //     Navigator.pop(context);
        //     print(_result['response']['data']);
        //     showDialog(
        //         context: context,
        //         builder: (_) => SucessPopup(
        //             message: pageDetails['text27'], route: 'request_payout'));
        //   }
        // }
        // }
      } else if (selectedoption == 2) {
        print('OOOOOOOOOOOOOOOOOOOOOOOOOOOO');
        if (selectedType == true) {
          print('OOOOOiiiiiiiiiiiiiiiiiiiiiiiOOOOOOO');
          ducAmount.text = "";
          address.text = "";
          addressError = null;
          ducError = null;
          callAPI(
              _map = {
                "product_id": 1,
                "amount": double.parse(cashAmount.text),
                "asset_type": "cash",
                "address_line": udscaddress.text,
                'isBankTransfer': false
              },
              payoutSystemDetails['allow_payout_cash'] == false,
              'You already have a Pending Payout request for Cash');
        } else {
          if (bankName == null ||
              bankName == 'null' && beneficiary == null ||
              bankName == 'null' && accountNumber == null ||
              accountNumber == 'null' && bankIdentificationcode == null ||
              bankIdentificationcode == 'null') {
            snack.snack(title: 'Please Add Bank Details');
          } else if (payoutSystemDetails['bank_already_requested'] == true) {
            snack.snack(
                title:
                    'Only one successful bank transfer is allowed per month');
          } else {
            print('uuuuuuuuuuuuuuuuuuuuuuuuuuuuuuOOOOO');
            ducAmount.text = "";
            address.text = "";
            addressError = null;
            ducError = null;
            callAPI(
                _map = {
                  "product_id": 1,
                  "amount": double.parse(cashAmount.text),
                  "asset_type": "cash",
                  "address_line": 'null',
                  'isBankTransfer': true
                },
                payoutSystemDetails['allow_payout_bank'] == false,
                'You already have a Pending Payout request for Cash');
          }
          // ducAmount.text = "";
          // address.text = "";
          // addressError = null;
          // ducError = null;
          // if (isKyc == true) {
          //   showDialog(
          //       barrierDismissible: false,
          //       context: context,
          //       builder: (BuildContext context) {
          //         // return NoKycPopUp();
          //         return KycPopUp();
          //       });
          // } else {
          //   if (payoutSystemDetails['allow_payout_cash'] == false) {
          //     print('YES DUC');
          // snack.snack(
          //     title: 'You already have a Pending Payout request for Cash');
          //   } else {
          // _map = {
          //   "product_id": 1,
          //   "amount": double.parse(cashAmount.text),
          //   "asset_type": "cash",
          //   "address_line": 'null',
          //   'isBankTransfer': true
          // };
          //     waiting.waitpopup(context);

          //     Map<String, dynamic> _result = await HttpRequest.Post(
          //         'c_rms_payout', Utils.constructPayload(_map));
          //     if (Utils.isServerError(_result)) {
          //       Navigator.pop(context);
          //       isKyc == true
          //           ? print("kyc")
          //           : showDialog(
          //               context: context,
          //               builder: (_) => SucessPopup(
          //                   message: pageDetails['text26'],
          //                   route: 'request_payout'));
          //       // final snackBar = SnackBar(
          //       //     content:
          //       //         MyText(text:await Utils.getMessage(_result['response']['error'])));
          //       // ScaffoldMessenger.of(context).showSnackBar(snackBar);
          //     } else {
          //       udscaddress.text = "";
          //       cashAmount.text = "";
          //       ducAmount.text = "";
          //       address.text = "";
          //       totalCashAmount = 0;
          //       totalDucAmount = 0;
          //       addressError = null;
          //       ducError = null;
          //       cashError = null;
          //       udscError = null;
          //       Navigator.pop(context);
          //       print(_result['response']['data']);
          //       showDialog(
          //           context: context,
          //           builder: (_) => SucessPopup(
          //               message: pageDetails['text27'],
          //               route: 'request_payout'));
          //     }
          //   }
          // }
        }
      }
    } else {
      // final snackBar = SnackBar(content: MyText(text:'conditions not met'));
      // ScaffoldMessenger.of(context).showSnackBar(snackBar);
      // print("error");
    }
  }

  callAPI(_map, condition, title) async {
    // ducAmount.text = "";
    // address.text = "";
    // addressError = null;
    // ducError = null;
    if (selectedoption == 2 && isKyc == true) {
      showDialog(
          barrierDismissible: false,
          context: context,
          builder: (BuildContext context) {
            // return NoKycPopUp();
            return KycPopUp();
          });
    } else {
      if (condition) {
        print('YES DUC');
        snack.snack(title: title);
      } else {
        waiting.waitpopup(context);

        Map<String, dynamic> _result = await HttpRequest.Post(
            'c_rms_payout', Utils.constructPayload(_map));
        if (Utils.isServerError(_result)) {
          Navigator.pop(context);
          // snack.snack(title: pageDetails['text38']);
          print('I=====================I');
          print(_result['response']['error']);
          if (_result['response']['error'] == 'kyc_not_verified') {
            snack.snack(title: 'KYC Not Verified');
          } else {
            _result['response']['error'] == 'negative_amount'
                ? snack.snack(
                    title:
                        "Blocked because of Negative balance in any of your account")
                : isKyc == true
                    ? print("kyc")
                    : snack.snack(title: pageDetails['text26']);
          }
          // isKyc == true
          //     ? print("kyc")
          //     : snack.snack(title: pageDetails['text26']);
          return await Utils.getMessage(_result['response']['error']);
          // showDialog(
          //     context: context,
          //     builder: (_) => SucessPopup(
          //         message: pageDetails['text26'], route: 'request_payout'));
          // final snackBar = SnackBar(
          //     content:
          //         MyText(text:await Utils.getMessage(_result['response']['error'])));
          // ScaffoldMessenger.of(context).showSnackBar(snackBar);
        } else {
          udscaddress.text = "";
          cashAmount.text = "";
          ducAmount.text = "";
          address.text = "";
          totalCashAmount = 0;
          totalDucAmount = 0;
          addressError = null;
          ducError = null;
          cashError = null;
          udscError = null;
          Navigator.pop(context);
          print(_result['response']['data']);
          showDialog(
              context: context,
              barrierDismissible: false,
              builder: (_) => SucessPopup(
                  message: pageDetails['text27'], route: 'request_payout'));
        }
      }
    }
  }
  // waitpopup(context) {
  //   showDialog(
  //       barrierDismissible: false,
  //       context: context,
  //       builder: (BuildContext context) {
  //         return AlertDialog(
  //           elevation: 24.0,
  //           backgroundColor: Pallet.popupcontainerback,
  //           shape: RoundedRectangleBorder(
  //               borderRadius: BorderRadius.all(Radius.circular(15.0))),
  //           title: Container(
  //             width: 50,
  //             child: Column(
  //               crossAxisAlignment: CrossAxisAlignment.center,
  //               children: [
  //                 CircularProgressIndicator(
  //                     valueColor: AlwaysStoppedAnimation<Color>(Colors.white)),
  //                 SizedBox(
  //                   height: 10,
  //                 ),
  //                 MyText(text:"Please Wait...",
  //                     style: TextStyle(
  //                         fontSize: Pallet.heading3,
  //                         color: Pallet.fontcolor,
  //                         fontWeight: Pallet.font500)),
  //               ],
  //             ),
  //           ),
  //         );
  //       }
  // );
  // }
}
