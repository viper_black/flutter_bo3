import 'dart:async';

import 'package:centurion/login.dart';
import 'package:centurion/screens/payment_success.dart';
import 'package:centurion/signup.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:web_browser_detect/web_browser_detect.dart';
import '../home.dart';

// ignore: camel_case_types
class Spleash_Screen extends StatefulWidget {
  final String referrral;
  final String activatekey;
  final String paymentstatus;

  const Spleash_Screen({
    Key key,
    this.referrral,
    this.activatekey,
    this.paymentstatus,
  }) : super(key: key);

  @override
  SpleashScreenState createState() => SpleashScreenState();
}

class SpleashScreenState extends State<Spleash_Screen> {
  // bool spleash = true;
  // reload() async {
  //   SharedPreferences prefs = await SharedPreferences.getInstance();
  //   prefs.remove("reload");
  //   String stringValue = prefs.getString('reload');
  //   print("lllllllllllllllll");
  //   print(stringValue);
  // }
  final browser = Browser();
  reload() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var browsername = browser.browser + browser.version.toString();
    print(browsername);
    final now = DateTime.now();
    DateTime endTime =
        DateTime(now.year, now.month, now.day, now.hour, now.minute);
    print(endTime);
    if (prefs.getString('browsername') == null) {
      print("aaaaaaaaaaaaaaaaaaaaaa");
      print(prefs.getString('browsername'));
      prefs.setString("browsername", browsername);
      prefs.setString(
          "sessiontime",
          DateTime(now.year, now.month, now.day, now.hour, now.minute)
              .toString());
      loginredirect();
    } else if (prefs.getString('browsername') == browsername) {
      print("ccccccccccaaaaaaaaaaaaaaaaaaaaaa");
      print(prefs.getString('sessiontime'));
      if (prefs.getString('sessiontime') != null) {
        print("dddddddddddddaaaaaaaaaaaaaaaaaaaaaa");
        String storedtime = prefs.getString('sessiontime');
        var startTime = DateTime.parse(storedtime);
        // ignore: non_constant_identifier_names
        final diff_time = endTime.difference(startTime).inMinutes;
        print("sssssssssssssssssss");

        print(endTime.difference(startTime).inDays);
        print(endTime.difference(startTime).inHours);
        print(diff_time);
        print("sssssssssssssssssss");
        print(startTime);
        if (endTime.year - startTime.year == 0) {
          if (endTime.month - startTime.month == 0) {
            if (endTime.difference(startTime).inDays == 0) {
              print(endTime.day - startTime.day);
              if (endTime.difference(startTime).inHours == 0) {
                print(endTime.hour - startTime.hour);
                print(endTime.minute - startTime.minute);
                print("bbbbbbbbbbbbbbbb");
                print(startTime.minute);
                print("cccccccccccccccccccccc");
                print(endTime.minute);
                if (diff_time >= 0 && diff_time <= 25) {
                  print("rrrrrrrrrrrrrrrrrraaaaaaaaaaaaaaaaaaaaaa");

                  if (prefs.getString('reload') != null) {
                    if (prefs.getString('reload') == "login" ||
                        prefs.getString('reload') == "signup") {
                      Navigator.pushReplacement(
                          context,
                          MaterialPageRoute(
                              builder: (BuildContext context) => Login()));
                    } else {
                      print("fffffffffffffffffffffffff");
                      print(prefs.getString('reload'));
                      String stringValue = prefs.getString('reload');
                      Navigator.pushReplacement(
                          context,
                          MaterialPageRoute(
                            builder: (BuildContext context) => Home(
                              route: stringValue,
                            ),
                          ));
                    }
                  } else {
                    prefs.remove("reload");
                    prefs.remove("sessiontime");
                    prefs.remove("browsername");
                    Navigator.pushReplacement(
                      context,
                      MaterialPageRoute(
                        builder: (BuildContext context) => Login(),
                      ),
                    );
                    prefs.remove("reload");
                    prefs.remove("sessiontime");
                    prefs.remove("browsername");
                    Navigator.pushReplacement(
                      context,
                      MaterialPageRoute(
                        builder: (BuildContext context) => Login(),
                      ),
                    );
                  }
                } else {
                  prefs.remove("sessiontime");
                  prefs.remove("reload");
                  prefs.remove("browsername");
                  Navigator.pushReplacement(
                    context,
                    MaterialPageRoute(
                      builder: (BuildContext context) => Login(),
                    ),
                  );
                }
              } else {
                prefs.remove("sessiontime");
                prefs.remove("browsername");
                prefs.remove("reload");

                Navigator.pushReplacement(
                  context,
                  MaterialPageRoute(
                    builder: (BuildContext context) => Login(),
                  ),
                );
              }
            } else {
              prefs.remove("sessiontime");
              prefs.remove("browsername");
              prefs.remove("reload");

              Navigator.pushReplacement(
                context,
                MaterialPageRoute(
                  builder: (BuildContext context) => Login(),
                ),
              );
            }
          } else {
            prefs.remove("sessiontime");
            prefs.remove("browsername");
            prefs.remove("reload");

            Navigator.pushReplacement(
              context,
              MaterialPageRoute(
                builder: (BuildContext context) => Login(),
              ),
            );
          }
        } else {
          prefs.remove("sessiontime");
          Navigator.pushReplacement(
            context,
            MaterialPageRoute(
              builder: (BuildContext context) => Login(),
            ),
          );
        }
      } else {
        Navigator.pushReplacement(
          context,
          MaterialPageRoute(
            builder: (BuildContext context) => Login(),
          ),
        );
      }
    } else {
      prefs.remove("browsername");

      Navigator.pushReplacement(
        context,
        MaterialPageRoute(
          builder: (BuildContext context) => Login(),
        ),
      );
    }
    // if (stringValue == "login" || stringValue == "signup") {
    // } else {
    //   Navigator.pushReplacement(
    //       context,
    //       MaterialPageRoute(
    //         builder: (BuildContext context) => Home(
    //           route: stringValue,
    //         ),
    //       ));
    // }
  }

  void initState() {
    redirect();
    super.initState();
    //print(widget.referrral);
  }

  redirect() {
    if (widget.activatekey != null) {
      Timer(Duration(seconds: 2), actiavteredirect);
    } else if (widget.referrral != null) {
      Timer(Duration(seconds: 2), signupredirect);
    } else if (widget.paymentstatus != null) {
      Timer(Duration(seconds: 2), paymentsuccess);
    } else {
      Timer(Duration(seconds: 2), reload);
    }
  }

  paymentsuccess() {
    var name = widget.paymentstatus;
    print("vvvvvvvxxxzdddddd");
    print(name);
    var val = name.split('_');
    print("llkkksss");
    print(val);

    Navigator.pushReplacement(
      context,
      MaterialPageRoute(
          builder: (BuildContext context) => Success(
                value: val[1].trim(),
              )),
    );
  }

  signupredirect() {
    //print("sssssssssssssssssssssssssssssss");

    // var str = widget.referrral;
    // var split = str.split('/');
    // var _invitertype = split[0].trim();
    var _invitername = widget.referrral.trim();

    Navigator.pushReplacement(
      context,
      MaterialPageRoute(
        builder: (BuildContext context) => Signup(
          // invitertype: _invitertype,
          referral: _invitername,
        ),
      ),
    );
  }

  actiavteredirect() {
    Navigator.pushReplacement(
      context,
      MaterialPageRoute(
        builder: (BuildContext context) => Login(
          activatekey: widget.activatekey,
        ),
      ),
    );
  }

  loginredirect() {
    //print("kllllllllllllllllllllllllllllllllllllllllllll");
    Navigator.pushReplacement(
      context,
      MaterialPageRoute(
        builder: (BuildContext context) => Login(),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Center(
      child: Container(
          height: MediaQuery.of(context).size.height * 0.6,
          child: Center(
            child: Image.asset("spleash.gif"),
          )),
    ));
  }
}
