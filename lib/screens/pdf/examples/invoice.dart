import 'dart:typed_data';
import 'package:flutter/services.dart';
import 'package:pdf/pdf.dart';
import 'package:pdf/widgets.dart' as pw;

Future<Uint8List> generateInvoice(
    PdfPageFormat pageFormat, dynamic profile, dynamic report) async {
  print("ffffffffffffffffffffffdddddddddddddddddddddd");
  final profileImage = pw.MemoryImage(
    (await rootBundle.load('assets/c-logo.png')).buffer.asUint8List(),
  );
  final invoice = Invoice(
    profile: profile,
    report: report,
    profileImage: profileImage,
  );
  print("sssssssssssssssssszzzzzzzzzzzzzzzzzzzzzzz");

  return await invoice.buildPdf(pageFormat);
}

class Invoice {
  Invoice({
    this.profileImage,
    this.profile,
    this.report,
  });

  final profile;
  final profileImage;
  final report;

  Future<Uint8List> buildPdf(PdfPageFormat pageFormat) async {
    final doc = pw.Document();
    print("eeeeeeeeeeeeeeeeeeeeeesssssssssssssssssssss");

    doc.addPage(
      pw.MultiPage(
        theme: pw.ThemeData.withFont(
          base: pw.Font.ttf(
              await rootBundle.load('assets/Montserrat/Montserrat-Medium.ttf')),
          bold: pw.Font.ttf(
              await rootBundle.load('assets/Montserrat/Montserrat-Medium.ttf')),
        ),
        header: _buildHeader,
        footer: _buildFooter,
        build: (context) => [
          _contentHeader(context),
        ],
      ),
    );

    return doc.save();
  }

  pw.Widget _buildHeader(pw.Context context) {
    print("rrrrrrrrrrrrrrrtttttttttttttttttttttttt");

    PdfColor fontcolor = PdfColors.white;
    var fontcolornew = PdfColor.fromInt(0XFF1034a6);
    var dashboardcontainerback = PdfColor.fromInt(0xFF272a67);

    var diff = DateTime.parse(report['end_of_month'].toString());

    DateTime dateval = diff;
    String enddate;

    if (dateval.year == DateTime.now().year) {
      if (dateval.month == DateTime.now().month) {
        enddate = dateval.year.toString() +
            '-' +
            dateval.month.toString() +
            '-' +
            DateTime.now().subtract(Duration(days: 1)).day.toString();
      } else {
        // DateTime dateval = diff.add(Duration(hours: dtime, minutes: dmin));

        enddate = dateval.toString().split(' ')[0].toString();
      }
    } else {
      // DateTime dateval = diff.add(Duration(hours: dtime, minutes: dmin));
      // print('DATEVAL: ${dateval}');

      enddate = dateval.toString().split(' ')[0].toString();
    }

    // .split('T')[0].toString();
    return pw.Container(
      decoration: pw.BoxDecoration(
        border: pw.Border.all(color: dashboardcontainerback),
      ),
      padding: pw.EdgeInsets.all(10),
      // width: 250,
      child: pw.Column(
        children: [
          pw.Container(
            decoration: pw.BoxDecoration(
              color: dashboardcontainerback,
              borderRadius: pw.BorderRadius.circular(10),
            ),
            height: 150,
            width: double.infinity,
            child: pw.Center(
              child: pw.Row(
                mainAxisAlignment: pw.MainAxisAlignment.center,
                children: [
                  pw.Text("The Network By ",
                      style:
                          pw.TextStyle(color: PdfColors.white, fontSize: 15)),
                  pw.Container(
                    width: 40,
                    child: pw.Image(profileImage),
                  ),
                  pw.Text(" CENTURION",
                      style:
                          pw.TextStyle(color: PdfColors.white, fontSize: 15)),
                ],
              ),
            ),
          ),
          pw.SizedBox(height: 15),
          report["end_of_month"] == null || profile == null
              ? pw.Container(
                  height: 300,
                  child: pw.Center(
                    child: pw.Text(
                      "Sorry No Commission Earned for This Month ",
                      style: pw.TextStyle(
                        color: fontcolornew,
                        fontSize: 15,
                      ),
                    ),
                  ),
                )
              : pw.Container(
                  height: 300,
                  child: pw.Column(
                    mainAxisAlignment: pw.MainAxisAlignment.spaceEvenly,
                    children: [
                      pw.Padding(
                        padding: pw.EdgeInsets.symmetric(
                          horizontal: 15,
                        ),
                        child: pw.Row(
                          mainAxisAlignment: pw.MainAxisAlignment.spaceBetween,
                          crossAxisAlignment: pw.CrossAxisAlignment.start,
                          children: [
                            pw.Container(
                              child: pw.Column(
                                crossAxisAlignment: pw.CrossAxisAlignment.start,
                                children: [
                                  pw.Text(
                                    profile['first_name'].toString() +
                                        ' ' +
                                        profile['last_name'].toString(),
                                    style: pw.TextStyle(
                                      color: fontcolornew,
                                      fontSize: 15,
                                      fontWeight: pw.FontWeight.bold,
                                    ),
                                  ),
                                  if (profile['address_line1'] != null)
                                    pw.Text(profile['address_line1'].toString(),
                                        style: pw.TextStyle(
                                          color: fontcolornew,
                                          fontSize: 15,
                                          fontWeight: pw.FontWeight.bold,
                                        )),
                                  if (profile['address_line2'] != null &&
                                      profile['address_line2'] != '')
                                    pw.Text(profile['address_line2'].toString(),
                                        style: pw.TextStyle(
                                          color: fontcolornew,
                                          fontSize: 15,
                                          fontWeight: pw.FontWeight.bold,
                                        )),
                                  if (profile['city'] != null)
                                    pw.Text(profile['city'].toString(),
                                        style: pw.TextStyle(
                                          color: fontcolornew,
                                          fontSize: 15,
                                          fontWeight: pw.FontWeight.bold,
                                        )),
                                  if (report['address'][0]['country'] != null)
                                    pw.Text(report['address'][0]['country'],
                                        style: pw.TextStyle(
                                          color: fontcolornew,
                                          fontSize: 15,
                                          fontWeight: pw.FontWeight.bold,
                                        )),
                                ],
                              ),
                            ),
                            pw.Container(
                              child: pw.Column(
                                crossAxisAlignment: pw.CrossAxisAlignment.start,
                                children: [
                                  pw.Text("Date",
                                      style: pw.TextStyle(
                                        color: fontcolornew,
                                        fontSize: 15,
                                        fontWeight: pw.FontWeight.bold,
                                      )),
                                  pw.Text(
                                      DateTime.now()
                                          .toString()
                                          .split(' ')[0]
                                          .toString(),
                                      style: pw.TextStyle(
                                        color: fontcolornew,
                                        fontSize: 15,
                                        fontWeight: pw.FontWeight.bold,
                                      )),
                                ],
                              ),
                            )
                          ],
                        ),
                      ),
                      pw.SizedBox(
                        height: 20,
                      ),
                      pw.Row(
                        mainAxisAlignment: pw.MainAxisAlignment.center,
                        children: [
                          pw.Container(
                            child: pw.Text(
                                "Statement of Commission Earned During The Period From",
                                style: pw.TextStyle(
                                  color: fontcolornew,
                                  fontSize: 15,
                                  fontWeight: pw.FontWeight.bold,
                                )),
                          ),
                        ],
                      ),
                      pw.SizedBox(
                        height: 5,
                      ),
                      pw.Row(
                        mainAxisAlignment: pw.MainAxisAlignment.center,
                        children: [
                          pw.Container(
                            decoration: pw.BoxDecoration(
                              border: pw.Border.all(color: PdfColors.blue),
                            ),
                            padding: pw.EdgeInsets.symmetric(
                                vertical: 5, horizontal: 10),
                            child: pw.Center(
                              child: pw.Text(
                                  report['start_of_month']
                                      .toString()
                                      .split('T')[0]
                                      .toString(),
                                  style: pw.TextStyle(
                                    color: fontcolornew,
                                    fontSize: 15,
                                    fontWeight: pw.FontWeight.bold,
                                  )),
                            ),
                          ),
                          pw.SizedBox(
                            width: 10,
                          ),
                          pw.Text("to",
                              style: pw.TextStyle(
                                color: fontcolornew,
                                fontSize: 15,
                                fontWeight: pw.FontWeight.bold,
                              )),
                          pw.SizedBox(
                            width: 10,
                          ),
                          pw.Container(
                            decoration: pw.BoxDecoration(
                              border: pw.Border.all(color: PdfColors.blue),
                            ),
                            padding: pw.EdgeInsets.symmetric(
                                vertical: 5, horizontal: 10),
                            child: pw.Center(
                              child: pw.Text(enddate,
                                  style: pw.TextStyle(
                                    color: fontcolornew,
                                    fontSize: 15,
                                    fontWeight: pw.FontWeight.bold,
                                  )),
                            ),
                          ),
                        ],
                      ),
                      pw.SizedBox(
                        height: 5,
                      ),
                      pw.Row(
                        mainAxisAlignment: pw.MainAxisAlignment.center,
                        children: [
                          pw.Container(
                            decoration: pw.BoxDecoration(
                              border: pw.Border.all(color: PdfColors.blue),
                            ),
                            padding: pw.EdgeInsets.symmetric(
                                vertical: 10, horizontal: 20),
                            child: pw.Center(
                              child: pw.Text(
                                  '\$ ' +
                                      double.parse((report["total_commission"])
                                              .toString())
                                          .toStringAsFixed(2)
                                          .toString(),
                                  style: pw.TextStyle(
                                    color: fontcolornew,
                                    fontSize: 15,
                                    fontWeight: pw.FontWeight.bold,
                                  )),
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
          pw.Container(
            padding: pw.EdgeInsets.symmetric(horizontal: 15, vertical: 10),
            decoration: pw.BoxDecoration(
              color: dashboardcontainerback,
              borderRadius: pw.BorderRadius.circular(10),
            ),
            child: pw.Row(
              mainAxisAlignment: pw.MainAxisAlignment.spaceBetween,
              crossAxisAlignment: pw.CrossAxisAlignment.end,
              children: [
                pw.Container(
                  child: pw.Column(
                    crossAxisAlignment: pw.CrossAxisAlignment.start,
                    children: [
                      pw.Text("Centurion Global Limited ",
                          style: pw.TextStyle(color: fontcolor, fontSize: 12)),
                      pw.SizedBox(height: 3),
                      pw.Text("Quality Corporate Services Ltd.,",
                          style: pw.TextStyle(color: fontcolor, fontSize: 12)),
                      pw.SizedBox(height: 3),
                      pw.Text("Suite 102, Cannon Place, P.O. Box 712,",
                          style: pw.TextStyle(color: fontcolor, fontSize: 12)),
                      pw.SizedBox(height: 3),
                      pw.Text("North Sound Road, George Town,",
                          style: pw.TextStyle(color: fontcolor, fontSize: 12)),
                      pw.SizedBox(height: 3),
                      pw.Text("Grand Cayman, KY1-9006, Cayman Islands",
                          style: pw.TextStyle(color: fontcolor, fontSize: 12)),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  pw.Widget _buildFooter(pw.Context context) {
    print("nnnnnnnnnnnmmmmmmmmmmmmmmmmmmmmmmm");

    return pw.Container();
  }

  pw.Widget _contentHeader(pw.Context context) {
    print("iiiiiiiiiiiiiiiooooooooooooooooooooo");

    return pw.Container();
  }
}
