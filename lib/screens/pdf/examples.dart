import 'dart:async';
import 'dart:typed_data';

import 'package:pdf/pdf.dart';

import 'examples/invoice.dart';

const examples = <Example>[
  Example(
    'INVOICE',
    'invoice.dart',
    generateInvoice,
  ),
];

typedef LayoutCallbackWithData = Future<Uint8List> Function(
    PdfPageFormat pageFormat, dynamic title, dynamic report);

class Example {
  const Example(this.name, this.file, this.builder);

  final String name;
  final String file;
  final LayoutCallbackWithData builder;
}
