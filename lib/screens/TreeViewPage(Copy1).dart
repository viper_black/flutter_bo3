import 'dart:async';
import 'package:centurion/common.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:graphview/GraphView.dart';
import '../config.dart';
import '../home.dart';
import '../services/business/account.dart';
import '../utils/utils.dart' show Utils;
import '../services/communication/index.dart' show HttpRequest;
// ignore: unused_shown_name
import 'package:vector_math/vector_math_64.dart' show Quad, Vector3, Matrix4;
import '../config/app_settings.dart';

class TreeViewPage extends StatefulWidget {
  final double wdgtWidth;
  final double wdgtHeight;

  const TreeViewPage({Key key, this.wdgtWidth, this.wdgtHeight})
      : super(key: key);

  @override
  _TreeViewPageState createState() => _TreeViewPageState();
}

class _TreeViewPageState extends State<TreeViewPage> {
  bool editable = false;
  Graph graph = Graph();
  BuchheimWalkerConfiguration builder = BuchheimWalkerConfiguration();
  List<Node> _nodes;
  // int _MainHead = 1;
  var state = [];
  var state1 = [];
  List<Map<String, dynamic>> stack = [];
  Future<String> _temp;
  List templist = [];

  int sp = 1;
  int ul = 0;
  int ll = 0;
  int diff = 9;
  int max;
  int headId;

  var productId = 1;
  var id = 0;
  var searchheight = 200.0;
  bool selected = false;

  @override
  // ignore: must_call_super
  void initState() {
    _temp = initChart();
    setPageDetails();
    sp = 1;
    diff = 9;
    ul = diff;
    ll = 0;
    // _transformationController.addListener(() {});
  }

  Map<String, dynamic> pageDetails = {};
  TransformationController _transformationController =
      TransformationController();
  void setPageDetails() async {
    String data = await Utils.getPageDetails('my_network');
    setState(() {
      pageDetails = Utils.fromJSONString(data);
    });
  }
  // StreamController _streamController = StreamController.broadcast();
  // StreamSink get stateSink => _streamController.sink;
  // Stream get stateStream => _streamController.stream;
  // @override
  // void dispose() {
  //   _streamController.close();
  //   super.dispose();
  // }

  confirmpopup(BuildContext context, int data, int idx) {
    return showDialog(
      context: context,
      builder: (BuildContext context) => AlertDialog(
        scrollable: true,
        elevation: 24.0,
        backgroundColor: Pallet.popupcontainerback,
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.all(Radius.circular(15.0))),
        title: Row(
          children: [
            Image.asset("c-logo.png", width: 35),
            SizedBox(width: 10),
            MyText(
                text: pageDetails['text1'],
                style: TextStyle(
                    // fontSize: popheading,
                    color: Pallet.fontcolor,
                    fontWeight: Pallet.font500)),
          ],
        ),
        content: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisSize: MainAxisSize.min,
          children: [
            MyText(
              text: pageDetails['text2'],
              style: TextStyle(
                // fontSize: popheading,
                color: Pallet.fontcolor,
                fontWeight: Pallet.font500,
              ),
            ),
          ],
        ),
        actions: [
          PopupButton(
            text: pageDetails['text3'],
            onpress: () {
              Navigator.pop(context);
            },
          ),
          PopupButton(
            text: pageDetails['text4'],
            onpress: () async {
              var map = {
                "system_product_id": 1,
                "account_id": data,
                "placement_id": idx,
              };
              Map<String, dynamic> result = await HttpRequest.Post(
                  'change_placement', Utils.constructPayload(map));

              // print('$result, change_placement');
              if (Utils.isServerError(result)) {
                throw (await Utils.getMessage(result['response']['error']));
              } else {
                setState(() {
                  myindex = null;
                });
                var _newSub = [];
                int _stackIndex;
                int _subIndex;
                int _mainIndex;
                for (var i = 0; i < state.length; i++) {
                  if (state[i]['id'] == data) {
                    _subIndex = i;
                  }
                }
                for (var i = 0; i < state.length; i++) {
                  if (state[i]['id'] == idx) {
                    _mainIndex = i;
                  }
                }

                Map<String, dynamic> _new;
                var _main;
                for (var i = 0; i < stack.length; i++) {
                  for (var _temp in stack[i]['sub']) {
                    if (_temp == _subIndex) {
                      _main = stack[i]['main'];
                      for (var _temp in stack[i]['sub']) {
                        if (_temp != _subIndex) {
                          _newSub.remove(_subIndex);
                          _newSub.add(_temp);
                          _new = {'main': _main, 'sub': _newSub};
                          stack.removeAt(i);
                          stack.insert(i, _new);
                        }
                      }
                    }
                  }
                }

                _newSub = [];
                for (var i = 0; i < stack.length; i++) {
                  if (stack[i]['main'] == _mainIndex) {
                    for (var _temp in stack[i]['sub']) {
                      _newSub.add(int.parse(_temp.toString()));
                      _stackIndex = i;
                    }
                  }
                }

                _newSub.add(_subIndex);
                _new = {'main': _mainIndex, 'sub': _newSub};
                if (_stackIndex != null) {
                  stack.removeAt(_stackIndex);
                  stack.insert(_stackIndex, _new);
                } else {
                  stack.add(_new);
                }

                setState(() {
                  graph = Graph();
                });
                for (var _temp in stack) {
                  for (var sub in _temp['sub']) {
                    graph.addEdge(_nodes[int.parse(_temp['main'].toString())],
                        _nodes[int.parse(sub.toString())]);
                  }
                }
              }
              // setState(() {
              //   state[]
              // });

              Navigator.pop(context);
              snack.snack(title: pageDetails['text5']);
              // ScaffoldMessenger.of(context)
              //     .showSnackBar(SnackBar(content: MyText(text:pageDetails['text5'])));

              Navigator.pushReplacement(
                  context,
                  MaterialPageRoute(
                      builder: (BuildContext context) => Home(
                            route: 'my_network',
                          )));
            },
          ),
        ],
      ),
    );
  }

  Future<String> initChart() async {
    // if (selected) {
    stack.clear();
    state.clear();
    // }
    Map<String, dynamic> data = selected == true
        ? {"system_product_id": productId, "account_id": id}
        : {"system_product_id": productId};

    Map<String, dynamic> result = await HttpRequest.Post(
        'rms_get_network_data', Utils.constructPayload(data));
    if (Utils.isServerError(result)) {
      throw (await Utils.getMessage(result['response']['error']));
    } else {
      state = result['response']['data'];
      if (state1.length == 0) {
        for (var item in state) {
          state1.add(item);
        }
      }

      if (state.length <= 1) {
        _temp = Future<String>.value('done');
        return 'no partners found';
      } else if (state.length > 10) {
        for (var i = ll; i <= ul; i++) {
          templist.add(state.elementAt(i));
        }
      } else {
        for (var i = 0; i < state.length; i++) {
          templist.add(state.elementAt(i));
        }
      }
      setState(() {
        // print("sssssssscccccc");
        headId = state[0]['id'];
        // print(state);

        max = (state.length / 10).ceil();
      });
    }
    setState(() {
      graph = Graph();
    });
    _nodes = List.generate(
      state.length,
      (i) => Node(
        getNodeText(
          state[i]['id'],
          state[i]['name'],
          state[i]['img'],
          state[i]['title'],
          state[i]['color'],
        ),
      ),
    );

    graph.addEdge(_nodes[1], _nodes[0]);
    // graph.addEdge(_nodes[1], _nodes[3]);
    // graph.addEdge(_nodes[1], _nodes[4]);
    // graph.addEdge(_nodes[1], _nodes[5]);

    builder
      ..siblingSeparation = (100)
      ..levelSeparation = (-110)
      ..subtreeSeparation = (150)
      ..orientation = (BuchheimWalkerConfiguration.ORIENTATION_TOP_BOTTOM);

    _temp = Future<String>.value('done');
    return 'start';
  }

  double animatewidth = 50;
  List sortedlist = [];
  TextEditingController searchcontroller = TextEditingController();
  int myindex;
  Map<String, dynamic> enddrawerval = {'purchaselist': '[]'};
  List gridhistorylist = [];
  double width1;
  String treeenabled = 'tree';
  double helpcardwidth = 20;
  bool helpcardenabled = false;
  final GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    width1 = width;
    return FutureBuilder(
        future: _temp,
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            return Scaffold(
              key: scaffoldKey,
              backgroundColor: Colors.white,
              drawerScrimColor: Colors.transparent,
              endDrawer: enddrawer(),
              body: Container(
                width: widget.wdgtWidth,
                child: Padding(
                  padding: const EdgeInsets.fromLTRB(10, 15, 0, 0),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisSize: MainAxisSize.max,
                    children: [
                      Container(
                        child: Padding(
                          padding: const EdgeInsets.only(
                            bottom: 10,
                          ),
                          child: Padding(
                            padding: const EdgeInsets.only(right: 10),
                            child: Row(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Row(
                                      children: [
                                        treetype(
                                          ontap: () {
                                            waiting.waitpopup(context);

                                            setState(() {
                                              treeenabled = 'tree';
                                            });
                                            Navigator.pop(context);
                                          },
                                          iconname: Icons.account_tree,
                                          listname: "tree",
                                        ),
                                        SizedBox(width: 10),
                                        treetype(
                                          ontap: () {
                                            waiting.waitpopup(context);
                                            setState(() {
                                              headId = state[0]['id'];
                                              sortedlist.clear();
                                              gridhistorylist.clear();
                                              gridhistorylist.add(headId);
                                            });

                                            // print(gridhistorylist);
                                            // print(gridhistorylist.length);

                                            // print("vvvvvvvvvvvvvvvvvvvvv");
                                            for (var item in state) {
                                              if (item['pid'] == headId) {
                                                sortedlist.add(item);
                                              }
                                            }
                                            setState(() {
                                              // headId = sortedlist;
                                              treeenabled = 'grid';
                                            });
                                            Navigator.pop(context);
                                          },
                                          iconname: Icons.grid_view,
                                          listname: "grid",
                                        ),
                                        SizedBox(width: 10),
                                        treetype(
                                            iconname: Icons.list,
                                            listname: "list",
                                            ontap: () {
                                              waiting.waitpopup(context);

                                              sortedlist.clear();

                                              setState(() {
                                                animatewidth = 50;
                                                searchcontroller.text = '';
                                                treeenabled = 'list';
                                              });
                                              Navigator.pop(context);
                                            }),
                                      ],
                                    ),
                                    if (treeenabled == 'tree')
                                      Padding(
                                        padding: const EdgeInsets.all(10.0),
                                        child: Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.start,
                                          children: [
                                            selected == true
                                                ? Padding(
                                                    padding:
                                                        const EdgeInsets.all(
                                                            10.0),
                                                    child: Container(
                                                      width: 50,
                                                      height: 50,
                                                      decoration: BoxDecoration(
                                                        border: Border.all(
                                                          color: Pallet
                                                              .fontcolornew,
                                                        ),
                                                        borderRadius:
                                                            BorderRadius
                                                                .circular(30),
                                                      ),
                                                      child: GestureDetector(
                                                        onTap: () {
                                                          setState(() {
                                                            animatewidth = 50;
                                                            searchcontroller
                                                                .text = '';
                                                            selected = false;
                                                            searchheight = 0.0;
                                                            productId = 1;
                                                            id = 0;
                                                            initChart();
                                                          });
                                                        },
                                                        child: Icon(
                                                          Icons.arrow_back,
                                                          color: Pallet
                                                              .fontcolornew,
                                                        ),
                                                      ),
                                                    ),
                                                  )
                                                : Container(),
                                            Container(
                                              child: MouseRegion(
                                                onEnter: (event) {
                                                  setState(() {
                                                    animatewidth = 300;
                                                  });
                                                },
                                                onExit: (event) {
                                                  setState(() {
                                                    searchcontroller.text ==
                                                                null ||
                                                            searchcontroller
                                                                    .text ==
                                                                ''
                                                        ? animatewidth = 50
                                                        : animatewidth = 300;
                                                  });
                                                },
                                                child: AnimatedContainer(
                                                  curve: Curves
                                                      .fastLinearToSlowEaseIn,
                                                  width: animatewidth,
                                                  decoration: BoxDecoration(
                                                    border: Border.all(
                                                      color:
                                                          Pallet.fontcolornew,
                                                    ),
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            30),
                                                  ),
                                                  duration: Duration(
                                                    milliseconds: 500,
                                                  ),
                                                  child: animatewidth == 50
                                                      ? Center(
                                                          child:
                                                              GestureDetector(
                                                            onTap: () {
                                                              setState(() {
                                                                animatewidth =
                                                                    300;
                                                              });
                                                            },
                                                            child: Container(
                                                              padding: EdgeInsets
                                                                  .symmetric(
                                                                      vertical:
                                                                          12),
                                                              child: Icon(
                                                                Icons.search,
                                                                color: Pallet
                                                                    .fontcolornew,
                                                              ),
                                                            ),
                                                          ),
                                                        )
                                                      : Column(
                                                          children: [
                                                            TextFormField(
                                                              controller:
                                                                  searchcontroller,
                                                              onChanged:
                                                                  (value) {
                                                                sortedlist = [];
                                                                if (value !=
                                                                    '') {
                                                                  for (var item
                                                                      in state1) {
                                                                    if (item[
                                                                            'name']
                                                                        .toString()
                                                                        .startsWith(
                                                                            value)) {
                                                                      sortedlist
                                                                          .add(
                                                                              item);
                                                                    }
                                                                  }
                                                                  searchheight =
                                                                      200.0;
                                                                } else {}

                                                                setState(() {
                                                                  sortedlist =
                                                                      sortedlist;
                                                                });
                                                                // print(sortedlist);
                                                              },
                                                              decoration:
                                                                  InputDecoration(
                                                                contentPadding:
                                                                    EdgeInsets
                                                                        .all(
                                                                            15),
                                                                border:
                                                                    InputBorder
                                                                        .none,
                                                                hintText:
                                                                    pageDetails[
                                                                        'text11'],
                                                                hintStyle:
                                                                    TextStyle(
                                                                  color: Pallet
                                                                      .fontcolornew,
                                                                ),
                                                                prefixIcon:
                                                                    Icon(Icons
                                                                        .search),
                                                                suffixIcon:
                                                                    MouseRegion(
                                                                  cursor:
                                                                      SystemMouseCursors
                                                                          .click,
                                                                  child:
                                                                      GestureDetector(
                                                                    onTap: () {
                                                                      setState(
                                                                          () {
                                                                        searchcontroller.text =
                                                                            '';
                                                                        selected =
                                                                            false;
                                                                        searchheight =
                                                                            0.0;
                                                                        productId =
                                                                            1;
                                                                        id = 0;
                                                                        initChart();
                                                                      });
                                                                    },
                                                                    child: Icon(
                                                                      Icons
                                                                          .clear,
                                                                      color: Pallet
                                                                          .fontcolornew,
                                                                    ),
                                                                  ),
                                                                ),
                                                              ),
                                                            ),
                                                            if (searchcontroller
                                                                    .text !=
                                                                '')
                                                              Column(
                                                                children: [
                                                                  Container(
                                                                    height:
                                                                        searchheight,
                                                                    child: ListView
                                                                        .builder(
                                                                      shrinkWrap:
                                                                          true,
                                                                      itemCount:
                                                                          sortedlist
                                                                              .length,
                                                                      itemBuilder:
                                                                          (BuildContext context,
                                                                              int index) {
                                                                        String
                                                                            listData =
                                                                            sortedlist[index]['name'];
                                                                        return ListTile(
                                                                          title:
                                                                              Text(listData.toString()),
                                                                          onTap:
                                                                              () {
                                                                            searchcontroller.text =
                                                                                sortedlist[index]['name'];

                                                                            setState(() {
                                                                              selected = true;
                                                                              productId = 5;
                                                                              id = sortedlist[index]['id'];
                                                                              searchheight = 0;
                                                                              initChart();
                                                                              id = 0;
                                                                            });
                                                                          },
                                                                        );
                                                                      },
                                                                    ),
                                                                  ),
                                                                ],
                                                              ),
                                                          ],
                                                        ),
                                                ),
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                  ],
                                ),
                                if (treeenabled == 'tree')
                                  Column(
                                    children: [
                                      GestureDetector(
                                        onTap: () {
                                          setState(() {
                                            helpcardwidth = width / 1.6;
                                            helpcardenabled = true;
                                          });

                                          collapse() {
                                            setState(() {
                                              helpcardwidth = 20;
                                              helpcardenabled = false;
                                            });
                                          }

                                          Timer(Duration(seconds: 4), collapse);
                                        },
                                        child: width > 600
                                            ? Column(
                                                mainAxisAlignment:
                                                    MainAxisAlignment.start,
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.start,
                                                children: [
                                                  usertypeindicator(
                                                      color: Colors.blue,
                                                      type:
                                                          pageDetails['text6']),
                                                  SizedBox(
                                                    height: 5,
                                                  ),
                                                  usertypeindicator(
                                                      color: Colors.red,
                                                      type:
                                                          pageDetails['text7']),
                                                  SizedBox(
                                                    height: 5,
                                                  ),
                                                  usertypeindicator(
                                                      color: Colors.grey,
                                                      type:
                                                          pageDetails['text8']),
                                                  SizedBox(
                                                    height: 5,
                                                  ),
                                                  usertypeindicator(
                                                      color: Colors.yellow,
                                                      type:
                                                          pageDetails['text9']),
                                                ],
                                              )
                                            : AnimatedContainer(
                                                duration:
                                                    Duration(milliseconds: 300),
                                                alignment:
                                                    Alignment.centerRight,
                                                width: helpcardwidth,
                                                child: Column(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment.start,
                                                  crossAxisAlignment:
                                                      CrossAxisAlignment.start,
                                                  children: [
                                                    usertypeindicator(
                                                        color: Colors.blue,
                                                        type: helpcardenabled ==
                                                                false
                                                            ? ''
                                                            : pageDetails[
                                                                'text6']),
                                                    SizedBox(
                                                      height: 5,
                                                    ),
                                                    usertypeindicator(
                                                        color: Colors.red,
                                                        type: helpcardenabled ==
                                                                false
                                                            ? ''
                                                            : pageDetails[
                                                                'text7']),
                                                    SizedBox(
                                                      height: 5,
                                                    ),
                                                    usertypeindicator(
                                                        color: Colors.grey,
                                                        type: helpcardenabled ==
                                                                false
                                                            ? ''
                                                            : pageDetails[
                                                                'text8']),
                                                    SizedBox(
                                                      height: 5,
                                                    ),
                                                    usertypeindicator(
                                                        color: Colors.yellow,
                                                        type: helpcardenabled ==
                                                                false
                                                            ? ''
                                                            : pageDetails[
                                                                'text9']),
                                                  ],
                                                ),
                                              ),
                                      ),
                                    ],
                                  ),
                              ],
                            ),
                          ),
                        ),
                      ),
                      if (treeenabled == 'list')
                        Expanded(
                          child: SingleChildScrollView(
                            child: width > 600 ? listview() : mobilelistview(),
                          ),
                        ),
                      if (treeenabled == 'grid')
                        Expanded(
                          child: width < 440
                              ? gridfunc(gridcount: 2)
                              : width >= 440 && width < 500
                                  ? gridfunc(gridcount: 3)
                                  : width >= 500 && width < 730
                                      ? gridfunc(gridcount: 4)
                                      : width >= 730 && width < 896
                                          ? gridfunc(gridcount: 5)
                                          : width >= 896 && width < 986
                                              ? gridfunc(gridcount: 6)
                                              : width >= 986 && width < 1440
                                                  ? gridfunc(gridcount: 7)
                                                  : gridfunc(gridcount: 8),
                        ),
                      if (treeenabled == 'tree')
                        Expanded(
                          child: InteractiveViewer(
                            transformationController: _transformationController,
                            minScale: 0.05,
                            maxScale: 4.0,
                            boundaryMargin:
                                const EdgeInsets.all(double.infinity),
                            constrained: false,

                            child: Transform.translate(
                              offset: width > 600
                                  ? Offset(width / 3,
                                      MediaQuery.of(context).size.height / 3)
                                  : Offset(width / 5,
                                      MediaQuery.of(context).size.height / 5),
                              child: (state.length == 1)
                                  ? Container(
                                      child: getNodeText(
                                        state[0]['id'],
                                        state[0]['name'],
                                        state[0]['img'],
                                        state[0]['title'],
                                        state[0]['color'],
                                      ),
                                    )
                                  : GraphView(
                                      graph: graph,
                                      algorithm: BuchheimWalkerAlgorithm(
                                          builder, TreeEdgeRenderer(builder)),
                                      paint: Paint()
                                        ..color = Colors.black
                                        ..strokeWidth = 1.1
                                        ..style = PaintingStyle.stroke
                                        ..strokeJoin = StrokeJoin.miter
                                        ..strokeMiterLimit = 2,
                                    ),
                            ),
                            // ),
                          ),
                        ),
                      // ),
                      // )
                    ],
                  ),
                ),
              ),
            );
          } else if (snapshot.hasError) {
            return SomethingWentWrongMessage();
          }
          return Loader();
        });
  }

// -------------------------------gridview builder----------------------------------------------

  Widget gridfunc({int gridcount}) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        gridhistorylist.length == 1
            ? Container()
            : IconButton(
                icon: Icon(Icons.arrow_back),
                onPressed: () {
                  waiting.waitpopup(context);

                  setState(() {
                    headId =
                        gridhistorylist.elementAt(gridhistorylist.length - 2);
                  });
                  sortedlist.clear();
                  for (var item in state) {
                    if (item['pid'] == headId) {
                      sortedlist.add(item);
                    }
                  }
                  setState(() {
                    gridhistorylist.removeAt(gridhistorylist.length - 1);
                  });
                  Navigator.pop(context);
                }),
        SizedBox(
          height: 10,
        ),
        Expanded(
          child: sortedlist.length != 0
              ? GridView.builder(
                  itemCount: sortedlist.length,
                  gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                    crossAxisCount: gridcount,
                    childAspectRatio: 1.15,
                    // mainAxisSpacing: 20,
                    // crossAxisSpacing: 20,
                    // maxCrossAxisExtent: null
                  ),
                  itemBuilder: (context, index) {
                    Color bgcolor = HexColor(sortedlist[index]['color']);
                    Color bgcolor1 = dashboard
                        .rankColor(sortedlist[index]['rank_id'].toString());

                    return InkWell(
                      onTap: () {
                        waiting.waitpopup(context);

                        setState(() {
                          headId = sortedlist[index]['id'];
                          sortedlist.clear();
                        });
                        gridhistorylist.add(headId);
                        // print("kkkkkkkkkkkkkk");
                        // print(gridhistorylist);
                        for (var item in state) {
                          if (item['pid'] == headId) {
                            sortedlist.add(item);
                          }
                        }
                        setState(() {});
                        Navigator.pop(context);
                      },
                      child: Container(
                        margin: EdgeInsets.all(10),
                        decoration: BoxDecoration(
                          color: bgcolor,
                          boxShadow: [
                            // BoxShadow(
                            //   color: Colors.black12,
                            //   blurRadius: 7,
                            //   spreadRadius: 8,
                            // )
                          ],
                          borderRadius: BorderRadius.circular(7),
                        ),
                        child: Column(
                          // mainAxisAlignment: MainAxisAlignment.end,
                          children: [
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Container(
                                  transform:
                                      Matrix4.translationValues(-10, -10, 0),
                                  child: CircleAvatar(
                                    backgroundColor: Colors.blue,
                                    backgroundImage: NetworkImage(
                                        appSettings['SERVER_URL'] +
                                            '/' +
                                            sortedlist[index]['img']
                                                .toString()),
                                    radius: 25,
                                  ),
                                ),
                                Container(
                                  margin: EdgeInsets.symmetric(
                                    vertical: 5,
                                    horizontal: 5,
                                  ),
                                  padding: EdgeInsets.all(5),
                                  // transform: Matrix4.translationValues(-10, -10, 0),
                                  decoration: BoxDecoration(
                                    color: Colors.white,
                                    borderRadius: BorderRadius.circular(5),
                                  ),
                                  child: MyText(
                                    text: sortedlist[index]['child_count']
                                        .toString(),
                                    style: TextStyle(color: Colors.black),
                                  ),
                                ),
                              ],
                            ),
                            Expanded(
                              child: Container(
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    Container(
                                      alignment: Alignment.center,
                                      padding: EdgeInsets.only(
                                        right: 5,
                                      ),
                                      width: double.infinity,
                                      child: MyText(
                                        text: sortedlist[index]['name']
                                            .toString(),
                                        overflow: TextOverflow.ellipsis,
                                        style: TextStyle(
                                          color: Colors.black,
                                          fontSize: Pallet.heading4,
                                          fontWeight: Pallet.font600,
                                        ),
                                      ),
                                    ),
                                    SizedBox(
                                      height: 5,
                                    ),
                                    Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: [
                                        Expanded(
                                          flex: 3,
                                          child: Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.end,
                                            children: [
                                              Container(
                                                alignment:
                                                    Alignment.centerRight,
                                                padding: EdgeInsets.all(3),
                                                decoration: BoxDecoration(
                                                  color: bgcolor1,
                                                  borderRadius:
                                                      BorderRadius.circular(5),
                                                ),
                                                child: MyText(
                                                  text: sortedlist[index]
                                                          ['title']
                                                      .toString(),
                                                  style: TextStyle(
                                                    color: sortedlist[index]
                                                                ['rank_id'] !=
                                                            0
                                                        ? Colors.white
                                                        : Colors.black,
                                                  ),
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                        Expanded(
                                          flex: 1,
                                          child: Container(
                                            padding: EdgeInsets.only(
                                              right: 10,
                                              left: 10,
                                            ),
                                            // alignment: Alignment.centerRight,
                                            child: GestureDetector(
                                              child: Icon(
                                                Icons.remove_red_eye_rounded,
                                                size: 15,
                                              ),
                                              onTap: () async {
                                                waiting.waitpopup(context);

                                                var map = {
                                                  "system_product_id": 1,
                                                  "account_id":
                                                      sortedlist[index]['id'],
                                                };
                                                // print(map);
                                                // print("kkkkkkkkkkk");

                                                Map<String, dynamic> result =
                                                    await HttpRequest.Post(
                                                        'gettreedetail',
                                                        Utils.constructPayload(
                                                            map));

                                                // print('$result, gettreedetail');
                                                if (Utils.isServerError(
                                                    result)) {
                                                  Navigator.pop(context);

                                                  throw (await Utils.getMessage(
                                                      result['response']
                                                          ['error']));
                                                } else {
                                                  // print("vvvvvvvvvvvvvvvvvv");
                                                  // print(result);
                                                  setState(() {
                                                    enddrawerval =
                                                        result['response']
                                                            ['data'];
                                                  });
                                                  Navigator.pop(context);

                                                  scaffoldKey.currentState
                                                      .openEndDrawer();
                                                }
                                              },
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ],
                                ),
                              ),
                            )
                          ],
                        ),
                      ),
                    );
                  },
                )
              : Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  // crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Expanded(
                      flex: 0,
                      child: Center(
                        child: Image.asset(
                          'no_data_found.png',
                          fit: BoxFit.fill,
                          width: 300,
                        ),
                      ),
                    ),
                    // Expanded(
                    //   child: SizedBox(
                    //     height: 10,
                    //   ),
                    // ),
                    Expanded(
                      flex: 0,
                      child: Center(
                        child: MyText(
                          text: pageDetails['text10'],
                          style: TextStyle(
                            fontSize: Pallet.heading3,
                            color: Pallet.fontcolornew,
                            fontWeight: Pallet.font600,
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
        ),
      ],
    );
  }

// ------------------------tree view builder-----------------------------------------

  Widget getNodeText(
      int idx, String name, String img, String title, String color) {
    // print("dddddddddddaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa");
    // print(color);
    Color bgcolor = HexColor(color);
    return InkWell(
      onTap: () async {
        waiting.waitpopup(context);
        var map = {
          "system_product_id": 1,
          "account_id": idx,
        };
        // print(map);
        // print("kkkkkkkkkkk");

        Map<String, dynamic> result = await HttpRequest.Post(
            'gettreedetail', Utils.constructPayload(map));

        // print('$result, gettreedetail');
        if (Utils.isServerError(result)) {
          Navigator.pop(context);

          throw (await Utils.getMessage(result['response']['error']));
        } else {
          // print("vvvvvvvvvvvvvvvvvv");
          // print(result);
          setState(() {
            enddrawerval = result['response']['data'];
            // print(enddrawerval['screen_name']);
          });
          Navigator.pop(context);

          scaffoldKey.currentState.openEndDrawer();
        }

        // Scaffold.of(context).openEndDrawer();
      },
      child: DragTarget<int>(builder: (
        BuildContext context,
        List<dynamic> accepted,
        List<dynamic> rejected,
      ) {
        return Draggable<int>(
          onDragStarted: () {
            if (color == '9E9E9E') {
              editable = true;
            } else {
              editable = false;
            }
          },
          onDragEnd: (details) {
            setState(() {
              myindex = null;
            });
          },
          onDragCompleted: () {
            setState(() {
              myindex = null;
            });
          },
          onDraggableCanceled: (velocity, offset) {
            setState(() {
              myindex = null;
            });
          },
          onDragUpdate: (details) {},
          data: idx,
          child: Container(
            height: 110.0,
            color: Colors.transparent,
            child: Container(
              height: 110.0,
              width: 220.0,
              decoration: BoxDecoration(
                boxShadow: [
                  // BoxShadow(
                  //   color: myindex != null ? Colors.black26 : Colors.black12,
                  //   spreadRadius: 8,
                  //   blurRadius: 8,
                  // ),
                ],
                color: bgcolor,
                // .withOpacity(
                //   myindex != null ? 0.4 : 1.0,
                // ),
                borderRadius: BorderRadius.circular(10),
              ),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Container(
                        transform: Matrix4.translationValues(10, -10, 0),
                        width: 60,
                        height: 60,
                        decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            image: DecorationImage(
                                image: NetworkImage(
                                    appSettings['SERVER_URL'] + '/' + img))),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 8.0, right: 8.0),
                        child: MyText(
                          text: title,
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 13,
                            fontWeight: FontWeight.w100,
                          ),
                        ),
                      ),
                    ],
                  ),
                  Container(
                    child: MyText(
                      text: name,
                      overflow: TextOverflow.ellipsis,
                      style: TextStyle(
                        letterSpacing: 1.1,
                        color: Colors.white,
                        fontSize: 17,
                        fontWeight: FontWeight.w500,
                      ),
                    ),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      for (var i = 0; i < state.length; i++)
                        if (state[i]['id'] == idx)
                          state[i]['child_count'] == 0
                              ? Container()
                              : GestureDetector(
                                  onTap: () {
                                    waiting.waitpopup(context);
                                    int _mainId;
                                    List<int> _subIds = [];
                                    int stackId;
                                    int remStackCount = 0;
                                    Map<String, dynamic> _new;
                                    for (var i = 0; i < state.length; i++) {
                                      if (state[i]['id'] == idx) {
                                        _mainId = i;
                                      }
                                    }
                                    for (var i = 0; i < state.length; i++) {
                                      if (state[i]['pid'] == idx) {
                                        _subIds.add(i);
                                      }
                                    }
                                    if (_subIds.length > 0) {
                                      _new = {'main': _mainId, 'sub': _subIds};

                                      for (var i = 0; i < stack.length; i++) {
                                        if (stack[i].toString() ==
                                            _new.toString()) {
                                          stackId = i;
                                        }
                                        if (stackId != null) {
                                          remStackCount++;
                                        }
                                      }
                                      if (stackId != null) {
                                        for (var i = 0;
                                            i < remStackCount;
                                            i++) {
                                          stack.removeAt(stackId);
                                        }
                                      } else {
                                        stack.add(_new);
                                      }

                                      if (idx == headId && stack.length == 0) {
                                        // print('here');
                                        builder.levelSeparation = -110;
                                        setState(() {
                                          graph = Graph();
                                        });
                                        graph.addEdge(_nodes[1], _nodes[0]);
                                      } else {
                                        builder.levelSeparation = 150;
                                        setState(() {
                                          graph = Graph();
                                        });
                                        for (var _temp in stack) {
                                          for (var sub in _temp['sub']) {
                                            graph.addEdge(
                                                _nodes[int.parse(
                                                    _temp['main'].toString())],
                                                _nodes[
                                                    int.parse(sub.toString())]);
                                          }
                                        }
                                      }
                                    }
                                    Navigator.pop(context);
                                  },
                                  child: Opacity(
                                    opacity: myindex != null ? 0.0 : 0.7,
                                    child: Container(
                                      // transform: Matrix4.translationValues(0, 3, 0),
                                      child: Container(
                                        decoration: BoxDecoration(
                                            border: Border.all(
                                              color:
                                                  Colors.black.withOpacity(0.5),
                                            ),
                                            shape: BoxShape.circle,
                                            color: Colors.white),
                                        child: Icon(
                                          Icons.add,
                                          color: Colors.black.withOpacity(0.5),
                                        ),
                                      ),
                                    ),
                                  ),
                                ),
                    ],
                  ),
                ],
              ),
            ),
          ),
          feedback: Material(
            color: Colors.transparent,
            child: AnimatedContainer(
              duration: Duration(
                milliseconds: 100,
              ),
              height: 110.0,
              width: 220.0,
              decoration: BoxDecoration(
                boxShadow: [
                  BoxShadow(
                    color: Colors.black12,
                    spreadRadius: 10,
                    blurRadius: 12,
                  ),
                ],
                color: Colors.blue,
                borderRadius: BorderRadius.circular(10),
              ),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  Padding(
                    padding: const EdgeInsets.only(
                      right: 15,
                      left: 10,
                    ),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        CircleAvatar(
                            radius: 30,
                            // backgroundColor: Colors.green,
                            backgroundImage: NetworkImage(
                                appSettings['SERVER_URL'] + '/' + img)),
                        SizedBox(
                          height: 5,
                        ),
                        Container(
                          child: MyText(
                            text: title,
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: 13,
                              fontWeight: FontWeight.w100,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  SizedBox(
                    height: 5,
                  ),
                  Container(
                    child: MyText(
                      text: name,
                      overflow: TextOverflow.ellipsis,
                      style: TextStyle(
                        letterSpacing: 1.1,
                        color: Colors.white,
                        fontSize: 17,
                        fontWeight: FontWeight.w500,
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
          childWhenDragging: Opacity(
            opacity: 0.6,
            child: Container(
              height: 110.0,
              width: 220.0,
              decoration: BoxDecoration(
                color: Colors.blue,
                boxShadow: [
                  BoxShadow(
                    color: Colors.black12,
                    spreadRadius: 10,
                    blurRadius: 12,
                  ),
                ],
                borderRadius: BorderRadius.circular(10),
              ),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  Padding(
                    padding: const EdgeInsets.only(
                      right: 15,
                      left: 10,
                    ),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        CircleAvatar(
                          radius: 30,
                          backgroundColor: Colors.transparent,
                          backgroundImage: NetworkImage(
                              appSettings['SERVER_URL'] + '/' + img),
                        ),
                        SizedBox(
                          height: 5,
                        ),
                        Container(
                          child: MyText(
                            text: title,
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: 13,
                              fontWeight: FontWeight.w100,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  SizedBox(
                    height: 5,
                  ),
                  Container(
                    child: MyText(
                      text: name,
                      overflow: TextOverflow.ellipsis,
                      style: TextStyle(
                        letterSpacing: 1.1,
                        color: Colors.white,
                        fontSize: 17,
                        fontWeight: FontWeight.w500,
                      ),
                    ),
                  ),
                  // Column(
                  //   mainAxisAlignment: MainAxisAlignment.end,
                  //   mainAxisSize: MainAxisSize.min,
                  //   children: [
                  //     Opacity(
                  //       opacity: 0.7,
                  //       child: Padding(
                  //         padding: EdgeInsets.fromLTRB(0, 0, 0, 0),
                  //         child: InkWell(
                  //           child: Container(
                  //             decoration: BoxDecoration(
                  //                 border: Border.all(
                  //                   color: Colors.black.withOpacity(0.5),
                  //                 ),
                  //                 shape: BoxShape.circle,
                  //                 color: Colors.white),
                  //             child: Icon(
                  //               Icons.add,
                  //               color: Colors.black.withOpacity(0.5),
                  //             ),
                  //           ),
                  //         ),
                  //       ),
                  //     ),
                  //   ],
                  // )
                ],
              ),
            ),
          ),
        );
      }, onWillAccept: (value) {
        setState(() {
          myindex = value;
        });
        if (idx != value && color != 'F44336') {
          return editable;
        } else {
          return false;
        }
      }, onLeave: (data) {
        setState(() {
          myindex = null;
        });
      }, onAccept: (int data) {
        confirmpopup(context, data, idx);
        // --aa
      }),
    );
  }

// usertype indicator like preferred customer

  Widget usertypeindicator({
    @required String type,
    @required Color color,
  }) {
    return Container(
      child: Row(
        children: [
          Container(
            width: 15,
            height: 15,
            color: color,
          ),
          SizedBox(
            width: 5,
          ),
          MyText(
            text: type,
            style: TextStyle(
              fontSize: width1 > 600 ? Pallet.heading4 : Pallet.heading6,
            ),
          ),
        ],
      ),
    );
  }

// data view type(list,grid,tree)

  Widget treetype({
    @required String listname,
    @required IconData iconname,
    void Function() ontap,
  }) {
    return InkWell(
      onTap: ontap,
      child: Container(
        height: 33,
        padding: EdgeInsets.all(5),
        decoration: BoxDecoration(
            color: treeenabled == listname ? Colors.blue : Colors.white,
            border: Border.all(
                color: treeenabled == listname ? Colors.blue : Colors.blue),
            borderRadius: BorderRadius.circular(
              5,
            )),
        child: Row(
          children: [
            Icon(
              iconname,
              size: 20,
              color: treeenabled == listname ? Colors.white : Colors.blue,
            ),
          ],
        ),
      ),
    );
  }

//  --------------------------------------listview-----------------------------

// listview for web

  Widget listview() {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Container(
            margin: EdgeInsets.all(5),
            padding: EdgeInsets.all(10),
            child: Column(
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Container(
                      child: MouseRegion(
                        onEnter: (event) {
                          setState(() {
                            animatewidth = 200;
                          });
                        },
                        onExit: (event) {
                          setState(() {
                            searchcontroller.text == null ||
                                    searchcontroller.text == ''
                                ? animatewidth = 50
                                : animatewidth = 200;
                          });
                        },
                        child: AnimatedContainer(
                          curve: Curves.fastLinearToSlowEaseIn,
                          width: animatewidth,
                          decoration: BoxDecoration(
                            border: Border.all(
                              color: Pallet.fontcolornew,
                            ),
                            borderRadius: BorderRadius.circular(30),
                          ),
                          duration: Duration(
                            milliseconds: 500,
                          ),
                          child: animatewidth == 50
                              ? Center(
                                  child: GestureDetector(
                                    onTap: () {
                                      setState(() {
                                        animatewidth = 200;
                                      });
                                    },
                                    child: Container(
                                      padding:
                                          EdgeInsets.symmetric(vertical: 12),
                                      child: Icon(
                                        Icons.search,
                                        color: Pallet.fontcolornew,
                                      ),
                                    ),
                                  ),
                                )
                              : TextFormField(
                                  controller: searchcontroller,
                                  onChanged: (value) {
                                    // print(value);
                                    sortedlist = [];
                                    if (value != '') {
                                      for (var item in state) {
                                        if (item['name']
                                            .toString()
                                            .startsWith(value)) {
                                          sortedlist.add(item);
                                        }
                                      }
                                    } else {}
                                    //
                                    setState(() {});
                                    // print(sortedlist);
                                  },
                                  decoration: InputDecoration(
                                    contentPadding: EdgeInsets.all(15),
                                    border: InputBorder.none,
                                    hintText: pageDetails['text11'],
                                    hintStyle: TextStyle(
                                      color: Pallet.fontcolornew,
                                    ),
                                    prefixIcon: GestureDetector(
                                      onTap: () {
                                        setState(() {
                                          animatewidth = 50;
                                        });
                                      },
                                      child: Icon(
                                        Icons.search,
                                        color: Pallet.fontcolornew,
                                      ),
                                    ),
                                  ),
                                ),
                        ),
                      ),
                    ),
                  ],
                ),
                SizedBox(
                  height: 20,
                ),
                Container(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      Expanded(
                        flex: 1,
                        child: Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 5),
                          child: MyText(
                            text: pageDetails['text12'],
                          ),
                        ),
                      ),
                      Expanded(
                        flex: 1,
                        child: Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 5),
                          child: MyText(
                            text: pageDetails['text13'],
                          ),
                        ),
                      ),
                      Expanded(
                        flex: 1,
                        child: Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 5),
                          child: MyText(
                            text: pageDetails['text14'],
                          ),
                        ),
                      ),
                      Expanded(
                        flex: 1,
                        child: Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 5),
                          child: MyText(
                            text: pageDetails['text15'],
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                Divider(
                  color: Colors.black,
                  thickness: 1,
                ),
                Column(children: [
                  for (var item in ((searchcontroller.text != '' ||
                          searchcontroller.text.trim().isNotEmpty)
                      ? sortedlist
                      : templist))
                    InkWell(
                      onTap: () async {
                        // print("dffffffffffff");

                        var map = {
                          "system_product_id": 1,
                          "account_id": item['id'],
                        };
                        waiting.waitpopup(context);
                        Map<String, dynamic> result = await HttpRequest.Post(
                            'gettreedetail', Utils.constructPayload(map));

                        // print('$result, addressProcess');
                        if (Utils.isServerError(result)) {
                          Navigator.pop(context);

                          throw (await Utils.getMessage(
                              result['response']['error']));
                        } else {
                          setState(() {
                            enddrawerval = result['response']['data'];
                          });
                          Navigator.pop(context);
                          scaffoldKey.currentState.openEndDrawer();
                        }
                      },
                      child: listtile(
                        name: item['name'].toString(),
                        img: item['img'].toString(),
                        rank: item['title'].toString(),
                        sponsor: nullremover.nullremove(
                          text: item['sponsor_name'].toString(),
                          ifnullreturnval: '',
                          enableemptyvalcheck: true,
                        ),
                        placement: nullremover.nullremove(
                          text: item['placement_name'].toString(),
                          ifnullreturnval: '',
                          enableemptyvalcheck: true,
                        ),

                        //  item['placement_name'].toString(),
                        rankvalue: item['rank_id'],
                        color: item['color'],
                      ),
                    ),
                  SizedBox(
                    height: 10,
                  ),
                  if (searchcontroller.text == '')
                    Container(
                      padding: EdgeInsets.symmetric(horizontal: 10),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: [
                          sp > 1
                              ? InkWell(
                                  onTap: () {
                                    templist.clear();
                                    sp -= 1;
                                    setState(() {
                                      ul = sp * 10;
                                      ll = ul - diff;
                                    });
                                    for (var i = ll; i <= ul; i++) {
                                      if (state.length > i) {
                                        templist.add(state.elementAt(i));
                                      }
                                    }
                                  },
                                  child: Container(
                                    padding: EdgeInsets.all(5),
                                    decoration: BoxDecoration(
                                      border: Border.all(
                                        color: Pallet.fontcolornew,
                                      ),
                                      borderRadius: BorderRadius.circular(5),
                                    ),
                                    child: Center(
                                      child: MyText(text: (sp - 1).toString()),
                                    ),
                                  ),
                                )
                              : Container(),
                          SizedBox(
                            width: sp > 1 ? 3 : 0,
                          ),
                          Container(
                            padding: EdgeInsets.all(5),
                            decoration: BoxDecoration(
                              color: Pallet.fontcolornew,
                              borderRadius: BorderRadius.circular(5),
                            ),
                            child: Center(
                              child: MyText(
                                  text: sp.toString(),
                                  style: TextStyle(
                                    color: Pallet.fontcolor,
                                  )),
                            ),
                          ),
                          SizedBox(
                            width: 3,
                          ),
                          sp != max
                              ? InkWell(
                                  onTap: () {
                                    templist.clear();
                                    sp += 1;
                                    setState(() {
                                      ul = sp * 10;
                                      ll = ul - diff;
                                    });

                                    for (var i = ll; i <= ul; i++) {
                                      if (state.length > i) {
                                        templist.add(state.elementAt(i));
                                      }
                                    }
                                  },
                                  child: Container(
                                    padding: EdgeInsets.all(5),
                                    decoration: BoxDecoration(
                                      border: Border.all(
                                        color: Pallet.fontcolornew,
                                      ),
                                      borderRadius: BorderRadius.circular(5),
                                    ),
                                    child: Center(
                                      child: MyText(text: (sp + 1).toString()),
                                    ),
                                  ),
                                )
                              : Container(),
                          SizedBox(
                            width: sp != max ? 3 : 0,
                          ),
                          sp != max
                              ? sp + 2 < max
                                  ? InkWell(
                                      onTap: () {
                                        templist.clear();
                                        sp += 2;
                                        setState(() {
                                          ul = sp * 10;
                                          ll = ul - diff;
                                        });
                                        for (var i = ll; i <= ul; i++) {
                                          if (state.length > i) {
                                            templist.add(state.elementAt(i));
                                          }
                                        }
                                      },
                                      child: Container(
                                        padding: EdgeInsets.all(5),
                                        decoration: BoxDecoration(
                                          border: Border.all(
                                            color: Pallet.fontcolornew,
                                          ),
                                          borderRadius:
                                              BorderRadius.circular(5),
                                        ),
                                        child: Center(
                                          child:
                                              MyText(text: (sp + 2).toString()),
                                        ),
                                      ),
                                    )
                                  : Container()
                              : Container(),
                          SizedBox(
                            width: sp != max
                                ? sp + 1 < max
                                    ? 3
                                    : 0
                                : 0,
                          ),
                          sp != max
                              ? sp + 1 < max
                                  ? MyText(
                                      text: "...",
                                    )
                                  : Container()
                              : Container(),
                          SizedBox(
                            width: sp != max
                                ? sp + 1 < max
                                    ? 3
                                    : 0
                                : 0,
                          ),
                          sp != max
                              ? sp + 1 < max
                                  ? InkWell(
                                      onTap: () {
                                        templist.clear();
                                        sp = max;
                                        print("sssssssssssss");
                                        print(state.length);
                                        setState(() {
                                          ul = sp * 10;
                                          ll = ul - diff;
                                        });
                                        for (var i = ll; i <= ul; i++) {
                                          if (state.length > i) {
                                            templist.add(state.elementAt(i));
                                          }
                                        }
                                      },
                                      child: Container(
                                        padding: EdgeInsets.all(5),
                                        decoration: BoxDecoration(
                                          border: Border.all(
                                            color: Pallet.fontcolornew,
                                          ),
                                          borderRadius:
                                              BorderRadius.circular(5),
                                        ),
                                        child: Center(
                                          child: MyText(text: (max).toString()),
                                        ),
                                      ),
                                    )
                                  : Container()
                              : Container(),
                        ],
                      ),
                    )
                ]),
              ],
            ),
          )
        ],
      ),
    );
  }

// mobile listview with scroll the list veritically and horizonally

  Widget mobilelistview() {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Container(
            margin: EdgeInsets.all(5),
            padding: EdgeInsets.all(10),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Container(
                      child: MouseRegion(
                        onEnter: (event) {
                          setState(() {
                            animatewidth = 200;
                          });
                        },
                        onExit: (event) {
                          setState(() {
                            searchcontroller.text == null ||
                                    searchcontroller.text == ''
                                ? animatewidth = 50
                                : animatewidth = 200;
                          });
                        },
                        child: AnimatedContainer(
                          curve: Curves.fastLinearToSlowEaseIn,
                          width: animatewidth,
                          decoration: BoxDecoration(
                            border: Border.all(
                              color: Pallet.fontcolornew,
                            ),
                            borderRadius: BorderRadius.circular(30),
                          ),
                          duration: Duration(
                            milliseconds: 500,
                          ),
                          child: animatewidth == 50
                              ? Center(
                                  child: GestureDetector(
                                    onTap: () {
                                      setState(() {
                                        animatewidth = 200;
                                      });
                                    },
                                    child: Container(
                                      padding:
                                          EdgeInsets.symmetric(vertical: 12),
                                      child: Icon(
                                        Icons.search,
                                        color: Pallet.fontcolornew,
                                      ),
                                    ),
                                  ),
                                )
                              : TextFormField(
                                  controller: searchcontroller,
                                  onChanged: (value) {
                                    // print(value);
                                    sortedlist = [];
                                    if (value != '') {
                                      for (var item in state) {
                                        if (item['name']
                                            .toString()
                                            .startsWith(value)) {
                                          sortedlist.add(item);
                                        }
                                      }
                                    } else {}
                                    //
                                    setState(() {});
                                    // print(sortedlist);
                                  },
                                  decoration: InputDecoration(
                                    contentPadding: EdgeInsets.all(15),
                                    border: InputBorder.none,
                                    hintText: pageDetails['text11'],
                                    hintStyle: TextStyle(
                                      color: Pallet.fontcolornew,
                                    ),
                                    prefixIcon: GestureDetector(
                                      onTap: () {
                                        setState(() {
                                          animatewidth = 50;
                                        });
                                      },
                                      child: Icon(
                                        Icons.search,
                                        color: Pallet.fontcolornew,
                                      ),
                                    ),
                                  ),
                                ),
                        ),
                      ),
                    ),
                  ],
                ),
                SizedBox(
                  height: 20,
                ),
                SingleChildScrollView(
                  controller: listscrollcontroller,
                  scrollDirection: Axis.horizontal,
                  child: Column(
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: [
                          Padding(
                            padding: const EdgeInsets.symmetric(horizontal: 5),
                            child: Container(
                              width: width1 * 0.4,
                              child: Center(
                                child: MyText(
                                  text: pageDetails['text12'],
                                ),
                              ),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.symmetric(horizontal: 5),
                            child: Container(
                              width: width1 * 0.4,
                              child: Center(
                                child: MyText(
                                  text: pageDetails['text13'],
                                ),
                              ),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.symmetric(horizontal: 5),
                            child: Container(
                              width: width1 * 0.4,
                              child: Center(
                                child: MyText(
                                  text: pageDetails['text14'],
                                ),
                              ),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.symmetric(horizontal: 5),
                            child: Container(
                              width: width1 * 0.4,
                              child: Center(
                                child: MyText(
                                  text: pageDetails['text15'],
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                      Divider(
                        color: Colors.black,
                        thickness: 1,
                      ),
                      for (var item in ((searchcontroller.text != '' ||
                              searchcontroller.text.trim().isNotEmpty)
                          ? sortedlist
                          : templist))
                        InkWell(
                          onTap: () async {
                            // print("dffffffffffff");/

                            var map = {
                              "system_product_id": 1,
                              "account_id": item['id'],
                            };
                            waiting.waitpopup(context);
                            Map<String, dynamic> result =
                                await HttpRequest.Post('gettreedetail',
                                    Utils.constructPayload(map));

                            // print('$result, addressProcess');
                            if (Utils.isServerError(result)) {
                              Navigator.pop(context);

                              throw (await Utils.getMessage(
                                  result['response']['error']));
                            } else {
                              setState(() {
                                enddrawerval = result['response']['data'];
                              });
                              Navigator.pop(context);
                              scaffoldKey.currentState.openEndDrawer();
                            }
                          },
                          child: listtile(
                            name: item['name'].toString(),
                            img: item['img'].toString(),
                            rank: item['title'].toString(),
                            // sponsor: item['sponsor_name'].toString(),
                            // placement: item['placement_name'].toString(),
                            sponsor: nullremover.nullremove(
                              text: item['sponsor_name'].toString(),
                              ifnullreturnval: '',
                              enableemptyvalcheck: true,
                            ),
                            placement: nullremover.nullremove(
                              text: item['placement_name'].toString(),
                              ifnullreturnval: '',
                              enableemptyvalcheck: true,
                            ),
                            rankvalue: item['rank_id'],
                            color: item['color'],
                          ),
                        ),
                      SizedBox(
                        height: 10,
                      ),
                    ],
                  ),
                ),
                if (searchcontroller.text == '')
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 10),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        sp > 1
                            ? InkWell(
                                onTap: () {
                                  templist.clear();
                                  sp -= 1;
                                  setState(() {
                                    ul = sp * 10;
                                    ll = ul - diff;
                                  });
                                  for (var i = ll; i <= ul; i++) {
                                    templist.add(state.elementAt(i));
                                  }
                                },
                                child: Container(
                                  padding: EdgeInsets.all(5),
                                  decoration: BoxDecoration(
                                    border: Border.all(
                                      color: Pallet.fontcolornew,
                                    ),
                                    borderRadius: BorderRadius.circular(5),
                                  ),
                                  child: Center(
                                    child: MyText(text: (sp - 1).toString()),
                                  ),
                                ),
                              )
                            : Container(),
                        SizedBox(
                          width: sp > 1 ? 3 : 0,
                        ),
                        Container(
                          padding: EdgeInsets.all(5),
                          decoration: BoxDecoration(
                            color: Pallet.fontcolornew,
                            borderRadius: BorderRadius.circular(5),
                          ),
                          child: Center(
                            child: MyText(
                                text: sp.toString(),
                                style: TextStyle(
                                  color: Pallet.fontcolor,
                                )),
                          ),
                        ),
                        SizedBox(
                          width: 3,
                        ),
                        sp != max
                            ? InkWell(
                                onTap: () {
                                  templist.clear();
                                  sp += 1;
                                  setState(() {
                                    ul = sp * 10;
                                    ll = ul - diff;
                                  });
                                  for (var i = ll; i <= ul; i++) {
                                    templist.add(state.elementAt(i));
                                  }
                                },
                                child: Container(
                                  padding: EdgeInsets.all(5),
                                  decoration: BoxDecoration(
                                    border: Border.all(
                                      color: Pallet.fontcolornew,
                                    ),
                                    borderRadius: BorderRadius.circular(5),
                                  ),
                                  child: Center(
                                    child: MyText(text: (sp + 1).toString()),
                                  ),
                                ),
                              )
                            : Container(),
                        SizedBox(
                          width: sp != max ? 3 : 0,
                        ),
                        sp != max
                            ? sp + 2 < max
                                ? InkWell(
                                    onTap: () {
                                      templist.clear();
                                      sp += 2;
                                      setState(() {
                                        ul = sp * 10;
                                        ll = ul - diff;
                                      });
                                      for (var i = ll; i <= ul; i++) {
                                        templist.add(state.elementAt(i));
                                      }
                                    },
                                    child: Container(
                                      padding: EdgeInsets.all(5),
                                      decoration: BoxDecoration(
                                        border: Border.all(
                                          color: Pallet.fontcolornew,
                                        ),
                                        borderRadius: BorderRadius.circular(5),
                                      ),
                                      child: Center(
                                        child:
                                            MyText(text: (sp + 2).toString()),
                                      ),
                                    ),
                                  )
                                : Container()
                            : Container(),
                        SizedBox(
                          width: sp != max
                              ? sp + 1 < max
                                  ? 3
                                  : 0
                              : 0,
                        ),
                        sp != max
                            ? sp + 1 < max
                                ? MyText(
                                    text: "...",
                                  )
                                : Container()
                            : Container(),
                        SizedBox(
                          width: sp != max
                              ? sp + 1 < max
                                  ? 3
                                  : 0
                              : 0,
                        ),
                        sp != max
                            ? sp + 1 < max
                                ? InkWell(
                                    onTap: () {
                                      templist.clear();
                                      sp = max;
                                      setState(() {
                                        ul = sp * 10;
                                        ll = ul - diff;
                                      });
                                      for (var i = ll; i <= ul; i++) {
                                        templist.add(state.elementAt(i));
                                      }
                                    },
                                    child: Container(
                                      padding: EdgeInsets.all(5),
                                      decoration: BoxDecoration(
                                        border: Border.all(
                                          color: Pallet.fontcolornew,
                                        ),
                                        borderRadius: BorderRadius.circular(5),
                                      ),
                                      child: Center(
                                        child: MyText(text: (max).toString()),
                                      ),
                                    ),
                                  )
                                : Container()
                            : Container(),
                      ],
                    ),
                  ),
              ],
            ),
          )
        ],
      ),
    );
  }

  ScrollController listscrollcontroller = ScrollController();

// Listview's listtile design code

  Widget listtile({
    @required String name,
    @required String rank,
    @required String img,
    @required String sponsor,
    @required String placement,
    @required int rankvalue,
    @required String color,
  }) {
    Color bgcolor = dashboard.rankColor(rankvalue.toString());
    Color tilebgcolor = HexColor(color);

    return width1 > 600
        ? Container(
            margin: EdgeInsets.all(5),
            padding: EdgeInsets.all(10),
            decoration: BoxDecoration(
              boxShadow: [
                BoxShadow(
                  color: Colors.black12,
                  spreadRadius: 5,
                  blurRadius: 8,
                )
              ],
              color: tilebgcolor,
              borderRadius: BorderRadius.circular(5),
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                Expanded(
                  flex: 1,
                  child: Row(
                    children: [
                      CircleAvatar(
                        radius: 20,
                        backgroundImage: NetworkImage(
                            appSettings['SERVER_URL'] + '/' + img.toString()),
                      ),
                      SizedBox(
                        width: 5,
                      ),
                      Expanded(
                        child: Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 5),
                          child: MyText(
                            text: name,
                            overflow: TextOverflow.ellipsis,
                            style: TextStyle(
                                // color:
                                //     rankvalue != 0 ? Colors.white : Colors.black,
                                ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                Expanded(
                  flex: 1,
                  child: Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 5),
                    child: Row(
                      children: [
                        Container(
                          padding: EdgeInsets.all(3),
                          decoration: BoxDecoration(
                            color: bgcolor,
                            borderRadius: BorderRadius.circular(5),
                          ),
                          child: MyText(
                            text: rank,
                            style: TextStyle(
                              color:
                                  rankvalue != 0 ? Colors.white : Colors.black,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                Expanded(
                  flex: 1,
                  child: Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 5),
                    child: MyText(
                      text: sponsor,
                      style: TextStyle(
                          // color: rankvalue != 0 ? Colors.white : Colors.black,
                          ),
                    ),
                  ),
                ),
                Expanded(
                  flex: 1,
                  child: Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 5),
                    child: MyText(
                      text: placement,
                      style: TextStyle(
                          // color: rankvalue != 0 ? Colors.white : Colors.black,
                          ),
                    ),
                  ),
                ),
              ],
            ))
        : Container(
            margin: EdgeInsets.all(5),
            padding: EdgeInsets.all(10),
            decoration: BoxDecoration(
              boxShadow: [
                BoxShadow(
                  color: Colors.black12,
                  spreadRadius: 5,
                  blurRadius: 8,
                )
              ],
              color: tilebgcolor,
              borderRadius: BorderRadius.circular(5),
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                Container(
                  width: width1 * 0.4,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      CircleAvatar(
                        radius: 20,
                        backgroundImage: NetworkImage(
                            appSettings['SERVER_URL'] + '/' + img.toString()),
                      ),
                      SizedBox(
                        width: 5,
                      ),
                      Expanded(
                        child: Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 5),
                          child: MyText(
                            text: name,
                            overflow: TextOverflow.ellipsis,
                            // style: TextStyle(
                            //   color:
                            //       rankvalue != 0 ? Colors.white : Colors.black,
                            // ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 5),
                  child: Container(
                    width: width1 * 0.4,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Center(
                          child: Container(
                            padding: EdgeInsets.all(3),
                            decoration: BoxDecoration(
                              color: bgcolor,
                              borderRadius: BorderRadius.circular(5),
                            ),
                            child: MyText(
                              text: rank,
                              overflow: TextOverflow.ellipsis,
                              style: TextStyle(
                                color: rankvalue != 0
                                    ? Colors.white
                                    : Colors.black,
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 5),
                  child: Container(
                    width: width1 * 0.4,
                    child: Center(
                      child: MyText(
                        text: sponsor,
                        // style: TextStyle(
                        //   color: rankvalue != 0 ? Colors.white : Colors.black,
                        // ),
                        overflow: TextOverflow.ellipsis,
                      ),
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 5),
                  child: Container(
                    width: width1 * 0.4,
                    child: Center(
                      child: MyText(
                        text: placement,
                        // style: TextStyle(
                        //   color: rankvalue != 0 ? Colors.white : Colors.black,
                        // ),
                        overflow: TextOverflow.ellipsis,
                      ),
                    ),
                  ),
                ),
              ],
            ),
          );
  }

// -----------------------------------------enddrawer-------------------------------

// end drawer design and functionalites code

  Widget enddrawer() {
    return Container(
      padding: width1 > 500
          ? EdgeInsets.fromLTRB(0, 15, 15, 15)
          : EdgeInsets.fromLTRB(0, 15, 15, 15),
      color: Colors.transparent,
      child: Container(
        width: 400,
        decoration: BoxDecoration(
          boxShadow: [
            BoxShadow(
              color: Colors.black12,
              spreadRadius: 15,
              blurRadius: 12,
            ),
          ],
          color: Colors.white,
          borderRadius: BorderRadius.circular(20),
        ),
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                height: 100,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.only(
                    topRight: Radius.circular(20),
                    topLeft: Radius.circular(20),
                  ),
                  color: Colors.blue,
                ),
                child: Padding(
                  padding: const EdgeInsets.symmetric(
                    horizontal: 20,
                  ),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Row(
                        children: [
                          CircleAvatar(
                            backgroundColor: Colors.transparent,
                            backgroundImage: NetworkImage(
                                appSettings['SERVER_URL'] +
                                    '/' +
                                    enddrawerval['img'].toString()),
                            radius: 35,
                          ),
                          SizedBox(
                            width: 20,
                          ),
                          Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              MyText(
                                text: enddrawerval['screen_name'].toString(),
                                style: TextStyle(
                                  letterSpacing: 1.3,
                                  color: Colors.white,
                                  fontSize: 19,
                                  fontWeight: FontWeight.w500,
                                ),
                              ),
                              SizedBox(
                                height: 5,
                              ),
                              InkWell(
                                onTap: () {},
                                child: Row(
                                  children: [
                                    Container(
                                      decoration: BoxDecoration(
                                          shape: BoxShape.circle,
                                          border: Border.all(
                                            color: Colors.white,
                                          )),
                                      child: Icon(
                                        Icons.add,
                                        size: 15,
                                        color: Colors.white,
                                      ),
                                    ),
                                    SizedBox(
                                      width: 5,
                                    ),
                                    InkWell(
                                      onTap: () {
                                        Navigator.push(
                                            context,
                                            MaterialPageRoute(
                                                builder:
                                                    (BuildContext context) =>
                                                        Home(
                                                          route:
                                                              'invite_member',
                                                        )));
                                      },
                                      child: MyText(
                                        text: pageDetails['text16'],
                                        style: TextStyle(
                                          color: Colors.white,
                                          fontSize: 13,
                                          fontWeight: FontWeight.w100,
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                      Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          InkWell(
                            onTap: () {
                              scaffoldKey.currentState.openDrawer();
                            },
                            child: Padding(
                              padding: const EdgeInsets.only(top: 10.0),
                              child: Container(
                                child: Icon(
                                  Icons.close,
                                  color: Colors.white,
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.symmetric(
                  vertical: 5,
                  horizontal: 10,
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        MyText(
                          text: pageDetails['text17'],
                          style: TextStyle(
                            color: Colors.black,
                            fontSize: 16,
                          ),
                        ),
                        SizedBox(
                          height: 5,
                        ),
                        MyText(
                          text: '\$ ' + enddrawerval['total_bonus'].toString(),
                          style: TextStyle(
                            color: Colors.black,
                            fontSize: 19,
                          ),
                        ),
                      ],
                    ),
                    Padding(
                      padding: const EdgeInsets.only(right: 5.0),
                      child: Row(
                        children: [
                          Container(
                            padding: EdgeInsets.all(5),
                            decoration: BoxDecoration(
                              shape: BoxShape.circle,
                              color: Colors.blue,
                            ),
                            child: Icon(
                              Icons.double_arrow_rounded,
                              color: Colors.white,
                            ),
                          ),
                          SizedBox(
                            width: 5,
                          ),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              MyText(
                                text: pageDetails['text18'],
                                style: TextStyle(
                                  fontSize: 12,
                                  fontWeight: FontWeight.w300,
                                  color: Colors.black,
                                ),
                              ),
                              SizedBox(
                                height: 5,
                              ),
                              MyText(
                                text: enddrawerval['business_value'].toString(),
                                style: TextStyle(
                                  fontSize: 13,
                                  fontWeight: FontWeight.w300,
                                  color: Colors.blue,
                                ),
                              )
                            ],
                          )
                        ],
                      ),
                    ),
                  ],
                ),
              ),
              SizedBox(
                height: 20,
              ),
              Padding(
                padding: const EdgeInsets.symmetric(
                  horizontal: 10,
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    bonuscontainer(
                      amt: enddrawerval['mb'] == 't'
                          ? pageDetails['text21']
                          : pageDetails['text22'],
                      fontsize: 15,
                      enabled: enddrawerval['mb'] == 't' ? true : false,
                      title: pageDetails['text19'],
                    ),
                    bonuscontainer(
                      amt: enddrawerval['tb'] == 't'
                          ? pageDetails['text21']
                          : pageDetails['text22'],
                      fontsize: 15,
                      enabled: enddrawerval['tb'] == 't' ? true : false,
                      title: pageDetails['text20'],
                    ),
                  ],
                ),
              ),
              SizedBox(
                height: 20,
              ),
              Padding(
                padding: const EdgeInsets.symmetric(
                  horizontal: 10,
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    bonuscontainer(
                      amt: enddrawerval['customer_count'].toString(),
                      enabled: null,
                      title: pageDetails['text23'],
                    ),
                    bonuscontainer(
                      amt: enddrawerval['network_count'].toString(),
                      enabled: null,
                      title: pageDetails['text24'],
                    ),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.symmetric(
                  horizontal: 10,
                ),
                child: latestpurchase(
                  purchaselist: enddrawerval['last_purchase'] == null ||
                          enddrawerval['last_purchase'].length == 0
                      ? []
                      : enddrawerval['last_purchase'],
                  wdgtHeight: 500,
                  wdgtWidth: width1 < 350
                      ? 300
                      : width1 >= 350 && width1 < 390
                          ? 320
                          : 390,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

// enddrawer bonus containers design

  Widget bonuscontainer(
      {@required bool enabled,
      @required String title,
      @required String amt,
      double fontsize}) {
    return Container(
      padding: EdgeInsets.all(5),
      width: 150,
      height: 80,
      decoration: BoxDecoration(
        color: Colors.blue,
        borderRadius: BorderRadius.circular(
          10,
        ),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: const EdgeInsets.fromLTRB(7, 5, 5, 3),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                MyText(
                  text: title,
                  style: TextStyle(
                    fontSize: 11,
                    fontWeight: FontWeight.w200,
                    color: Colors.white,
                  ),
                ),
                enabled != null
                    ? Container(
                        height: 10,
                        width: 10,
                        decoration: BoxDecoration(
                          color: enabled == true
                              ? Colors.greenAccent[700]
                              : Colors.red,
                          shape: BoxShape.circle,
                        ),
                      )
                    : Container()
              ],
            ),
          ),
          SizedBox(
            height: 10,
          ),
          Padding(
            padding: const EdgeInsets.fromLTRB(7, 5, 5, 3),
            child: MyText(
              text: amt,
              style: TextStyle(
                color: Colors.white,
                fontSize: fontsize == null ? 22 : fontsize,
                fontWeight: FontWeight.w500,
              ),
            ),
          )
        ],
      ),
    );
  }

// latest purchases list generator inside the end drawer

  Widget latestpurchase(
      {double wdgtWidth, double wdgtHeight, List purchaselist}) {
    // print("ddddddddddddddddddsssssssssssssssssssss");
    // print(purchaselist);
    return Container(
        width: wdgtWidth,
        height: wdgtHeight,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          // mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Padding(
              padding: const EdgeInsets.only(left: 20.0, bottom: 20.0, top: 20),
              child: Row(
                children: [
                  MyText(
                      text: pageDetails['text25'],
                      style: TextStyle(
                        fontSize: Pallet.heading3,
                        fontWeight: Pallet.font500,
                        color: Pallet.fontcolornew,
                      ))
                ],
              ),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Container(
                  decoration: BoxDecoration(
                      color: Pallet.lastpackheadingback,
                      borderRadius: BorderRadius.circular(5)),
                  width: wdgtWidth * 0.90,
                  height: wdgtHeight * 0.09,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      Expanded(
                        flex: 1,
                        child: Center(
                          child: Container(
                            child: MyText(
                                text: pageDetails['text26'],
                                style: TextStyle(
                                    fontSize: Pallet.heading5,
                                    fontWeight: Pallet.font500,
                                    color: Pallet.fontcolor)),
                          ),
                        ),
                      ),
                      Expanded(
                        flex: 1,
                        child: Center(
                          child: Container(
                            child: MyText(
                                text: pageDetails['text27'],
                                style: TextStyle(
                                    fontSize: Pallet.heading5,
                                    fontWeight: Pallet.font500,
                                    color: Pallet.fontcolor)),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
            SizedBox(height: 13),
            purchaselist.length != 0
                ? Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Container(
                        decoration: BoxDecoration(
                            // color: Pallet.lastpackheadingback,
                            borderRadius: BorderRadius.circular(5)),
                        width: wdgtWidth * 0.90,
                        height: wdgtHeight * 0.55,
                        child: Column(
                          children: [
                            for (var i = 0; i < purchaselist.length; i++)
                              Padding(
                                padding:
                                    EdgeInsets.only(bottom: wdgtHeight * 0.03),
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Container(
                                      decoration: BoxDecoration(
                                          color: Pallet.lastpackback,
                                          borderRadius:
                                              BorderRadius.circular(5)),
                                      width: wdgtWidth * 0.90,
                                      height: wdgtHeight * 0.08,
                                      child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceAround,
                                        children: [
                                          Expanded(
                                            flex: 1,
                                            child: Center(
                                              child: Container(
                                                padding: EdgeInsets.only(
                                                  left: 5,
                                                ),
                                                child: MyText(
                                                    text: purchaselist[i]
                                                                ['is_dsv_p'] ==
                                                            true
                                                        ? purchaselist[i]
                                                                    ['price']
                                                                .toString() +
                                                            ' DSV-P'
                                                        : purchaselist[i]
                                                                    ['price']
                                                                .toString() +
                                                            ' DSV',
                                                    overflow:
                                                        TextOverflow.ellipsis,
                                                    style: TextStyle(
                                                        fontSize:
                                                            Pallet.heading5,
                                                        fontWeight:
                                                            Pallet.font500,
                                                        color:
                                                            Pallet.fontcolor)),
                                              ),
                                            ),
                                          ),
                                          Expanded(
                                            flex: 1,
                                            child: Container(
                                              padding:
                                                  EdgeInsets.only(right: 5),
                                              child: MyText(
                                                  text: purchaselist[i]
                                                          ['purchase_date']
                                                      .toString(),
                                                  overflow:
                                                      TextOverflow.ellipsis,
                                                  style: TextStyle(
                                                      fontSize: Pallet.heading5,
                                                      fontWeight:
                                                          Pallet.font500,
                                                      color: Pallet.fontcolor)),
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                          ],
                        ),
                      ),
                    ],
                  )
                : Container(
                    decoration: BoxDecoration(
                        // color: Pallet.lastpackheadingback,
                        borderRadius: BorderRadius.circular(5)),
                    width: wdgtWidth * 0.90,
                    height: wdgtHeight * 0.55,
                    child: Center(
                      child: Padding(
                        padding: const EdgeInsets.only(left: 10.0),
                        child: MyText(
                          text: pageDetails['text28'],
                          style: TextStyle(
                            color: Pallet.fontcolornew,
                            fontSize: Pallet.heading3,
                            fontWeight: Pallet.font500,
                          ),
                        ),
                      ),
                    ),
                  ),
            SizedBox(height: 5),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Container(
                  decoration: BoxDecoration(
                      color: Pallet.lastpackfooterback,
                      borderRadius: BorderRadius.circular(5)),
                  width: wdgtWidth * 0.90,
                  height: wdgtHeight * 0.09,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      Expanded(
                        flex: 1,
                        child: Center(
                          child: Container(
                            child: MyText(
                                text: pageDetails['text29'],
                                style: TextStyle(
                                    fontSize: Pallet.heading4,
                                    fontWeight: Pallet.font500,
                                    color: Pallet.fontcolornew)),
                          ),
                        ),
                      ),
                      Expanded(
                        flex: 1,
                        child: Center(
                          child: Container(
                            child: MyText(
                                text: enddrawerval['total_packages'].toString(),
                                style: TextStyle(
                                    fontSize: Pallet.heading4,
                                    fontWeight: Pallet.font500,
                                    color: Pallet.fontcolornew)),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ],
        ));
  }
}

// hex code to flutter colordata generator

class HexColor extends Color {
  static int _getColorFromHex(String hexColor) {
    hexColor = hexColor.toUpperCase().replaceAll("#", "");
    if (hexColor.length == 6) {
      hexColor = "FF" + hexColor;
      // print('HEXCOLOR: ${hexColor}');
    }
    return int.parse(hexColor, radix: 16);
    //
  }

  HexColor(final String hexColor) : super(_getColorFromHex(hexColor));
}
