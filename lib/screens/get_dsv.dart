// import 'package:blinking_text/blinking_text.dart';
import '../utils/utils.dart';
import 'package:clippy_flutter/label.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import '../config.dart';
// import './../data.dart';
import '../config/app_settings.dart';
import '../services/business/account.dart';
import '../home.dart';

import '../common.dart';
import '../utils/utils.dart' show Utils;
import '../services/communication/index.dart' show HttpRequest;
// import '../services/business/account.dart';

class Dsv extends StatefulWidget {
  const Dsv({
    Key key,
    this.wdgtWidth,
    this.wdgtHeight,
  }) : super(key: key);
  final double wdgtWidth, wdgtHeight;

  @override
  _DsvState createState() => _DsvState();
}

class _DsvState extends State<Dsv> with SingleTickerProviderStateMixin {
  Map<String, dynamic> pageDetails = {};
  _DsvState() {
    setPageDetails();
  }
  List cart = [];
  int total = 0;
  int packageCount = dashboard.accountType == 2 ? 8 : 4;
  // int packageCount = dashboard.accountType == 2 ? 8 : 5;
  bool isDropDown = false;
  // ignore: non_constant_identifier_names
  double temp_total = 0;
  // ignore: non_constant_identifier_names
  double temp_selected_trade = 0;
  List packages;
  List tempList;
  String isKyc;
  String showAdminFee;
  double cashBal;
  double tradeBal;
  double cashUsed = 0;
  double tradeUsed = 0;
  // ignore: non_constant_identifier_names
  double cashBal_view;
  bool iscashonly = false;
  // ignore: non_constant_identifier_names
  double tradeBal_view;
  Future<String> temp1;

  TextEditingController cashAccount = TextEditingController();
  String cashError;
  TextEditingController tradAccount = TextEditingController();
  AnimationController _animationController;
  Animation animation;
  String tradError;
  bool iconstatus = true;
  bool isMobile = false;
  bool userActiveStatus = false;

  bool cashSelected = dashboard.accountType == 1 ? true : false;
  bool tradeSelected = dashboard.accountType == 1 ? false : true;

  void setPageDetails() async {
    String data = await Utils.getPageDetails('getDSV');
    setState(() {
      pageDetails = Utils.fromJSONString(data);
    });
  }

  int selectedoption = 1;
  IconData icon = Icons.arrow_drop_down;

  Future<String> temp;
  double totalAmount;
  void initState() {
    _animationController =
        AnimationController(vsync: this, duration: Duration(seconds: 1));
    _animationController.repeat(reverse: true);
    animation = Tween(begin: 5.0, end: 35.0).animate(_animationController)
      ..addListener(() {
        setState(() {});
      });
    temp = getPackages();
    dashboard.dashboard();
    // temp1 = dashboard.dashboard();
    super.initState();
    // //print("test $temp");
    // _animationController = new AnimationController(
    //     vsync: this, duration: Duration(milliseconds: 500));
    // _animationController.repeat(reverse: true);
  }

  @override
  void dispose() {
    _animationController.dispose();
    super.dispose();
  }

  callAlert() {
    //print('Amoun(t is less than 50');
    // AlertDialogg(
    //   label: MyText(text:'gdyuag'),
    //   content: MyText(text:'fjhesfvds'),
    // );
  }

  FocusNode cashaccnode = FocusNode();
  FocusNode tradeaccNode = FocusNode();
  FocusNode continuenode = FocusNode();
  @override
  FutureBuilder build(BuildContext context) {
    Size size = MediaQuery.of(context).size;

    return FutureBuilder(
      future: temp,
      builder: (context, snapshot) {
        if (snapshot.hasError) {
          return SomethingWentWrongMessage();
        } else if (snapshot.hasData) {
          return LayoutBuilder(
            // ignore: missing_return
            builder: (context, constraints) {
              if (constraints.maxWidth <= ScreenSize.iphone) {
                isDropDown = true;

                // isMobile == false
                // dashboard.accountType == 2
                //     ? packageCount =
                //         iconstatus == false ? packages.length : packageCount = 4
                //     : packageCount = iconstatus == false
                //         ? packages.length
                //         : packageCount = 5;
                // packageCount = dashboard.accountType == 2 ? 4 : 5;
                return ListView(
                  children: [
                    Padding(
                      padding: EdgeInsets.only(
                          left: Pallet.defaultPadding,
                          top: Pallet.topPadding1,
                          right: Pallet.defaultPadding),
                      child: Container(
                        padding: EdgeInsets.only(
                            // left: Pallet.defaultPadding,
                            // right: Pallet.defaultPadding,
                            top: Pallet.defaultPadding),
                        width: widget.wdgtWidth,
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(Pallet.radius),
                            color: Pallet.fontcolor),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Align(
                                  alignment: Alignment.topLeft,
                                  child: MyText(
                                    text: pageDetails["text1"],
                                    style: TextStyle(
                                      color: Pallet.fontcolornew,
                                      fontSize: Pallet.heading1,
                                    ),
                                  ),
                                ),
                                // CustomButton(
                                //   child: Center(
                                //     child: BlinkText(
                                //       pageDetails["text43"],
                                //       beginColor: Colors.orange,
                                //       endColor: Pallet.fontcolor,
                                //     ),
                                //   ),
                                //   hpadding: 15,
                                //   vpadding: 8,
                                //   textcolor: Pallet.fontcolor,
                                //   buttoncolor: Pallet.dashcontainerback,
                                //   onpress: () {
                                //     _showMaterialDialog(context);
                                //   },
                                //   fontweight: Pallet.font600,
                                // ),
                              ],
                            ),
                            SizedBox(height: 10.0),
                            MyText(
                              text: pageDetails["text17"],
                              style: TextStyle(
                                color: Pallet.fontcolornew,
                                fontSize: Pallet.heading3,
                                fontWeight: Pallet.subheading1wgt,
                              ),
                            ),
                            Row(
                              children: [
                                SizedBox(height: 10.0),
                                Container(
                                  margin: EdgeInsets.only(
                                      top: Pallet.defaultPadding),
                                  decoration: BoxDecoration(
                                      boxShadow: [Pallet.shadowEffect],
                                      borderRadius:
                                          BorderRadius.circular(Pallet.radius),
                                      color: Pallet.dashcontainerback),
                                  child: Center(
                                      child: MyText(
                                          text: pageDetails["text3"],
                                          style: TextStyle(
                                              color: Pallet.fontcolor,
                                              fontWeight: FontWeight.w600))),
                                  height: 30,
                                  width: size.width * 0.24,
                                ),
                                SizedBox(width: 15),
                                MyText(
                                  text: total.toString() + pageDetails["text2"],
                                  style: TextStyle(
                                      color: Pallet.dashcontainerback,
                                      fontSize: size.width * .06,
                                      fontWeight: FontWeight.w600),
                                ),
                              ],
                            ),
                            SizedBox(height: 30),
                            dropdown(
                                wdgtwidth: widget.wdgtWidth,
                                boxperow: 2,
                                dropfontsize: 8),
                          ],
                        ),
                      ),
                    ),
                    SizedBox(height: 20),
                    utilizeAssets(wdgtwidth: widget.wdgtWidth),
                  ],
                );
              } else if (constraints.maxWidth > ScreenSize.iphone &&
                  constraints.maxWidth <= ScreenSize.ipad) {
                isDropDown = true;

                //  isMobile == false
                // dashboard.accountType == 2
                //     ? packageCount =
                //         iconstatus == false ? packages.length : packageCount = 9
                //     : packageCount = iconstatus == false
                //         ? packages.length
                //         : packageCount = 5;
                return Container(
                  padding: EdgeInsets.only(
                      left: Pallet.leftPadding, top: Pallet.topPadding1),
                  child: SingleChildScrollView(
                    child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Container(
                              child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              ClipRRect(
                                borderRadius:
                                    BorderRadius.circular(Pallet.radius),
                                child: Container(
                                  padding: EdgeInsets.symmetric(
                                      // horizontal: Pallet.defaultPadding,
                                      vertical: Pallet.defaultPadding),
                                  width: widget.wdgtWidth * .8,
                                  color: Pallet.fontcolor,
                                  child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                          children: [
                                            MyText(
                                              text: pageDetails["text1"],
                                              style: TextStyle(
                                                color: Pallet.fontcolornew,
                                                fontSize: Pallet.heading1,
                                              ),
                                            ),
                                            // FadeTransition(
                                            //   opacity: _animationController,
                                            //   child: CustomButton(
                                            //     text: 'Offers',
                                            //     hpadding: 15,
                                            //     vpadding: 8,
                                            //     textcolor: Pallet.fontcolor,
                                            //     buttoncolor:
                                            //         Pallet.dashcontainerback,
                                            //     onpress: () {
                                            //       _showMaterialDialog(
                                            //           context);
                                            //     },
                                            //     fontweight: Pallet.font600,
                                            //   ),
                                            // ),
                                            // CustomButton(
                                            //   child: Center(
                                            //     child: BlinkText(
                                            //       pageDetails["text43"],
                                            //       beginColor: Colors.orange,
                                            //       endColor: Pallet.fontcolor,
                                            //     ),
                                            //   ),
                                            //   hpadding: 15,
                                            //   vpadding: 8,
                                            //   textcolor: Pallet.fontcolor,
                                            //   buttoncolor:
                                            //       Pallet.dashcontainerback,
                                            //   onpress: () {
                                            //     _showMaterialDialog(context);
                                            //   },
                                            //   fontweight: Pallet.font600,
                                            // ),
                                          ],
                                        ),
                                        SizedBox(height: 10.0),
                                        MyText(
                                          text: pageDetails["text17"],
                                          style: TextStyle(
                                              color: Pallet.dashcontainerback,
                                              fontSize: Pallet.subheading1,
                                              fontWeight:
                                                  Pallet.subheading1wgt),
                                        ),
                                        SizedBox(height: 15.0),
                                        Row(
                                          children: [
                                            Container(
                                              margin: EdgeInsets.only(
                                                  bottom: 15, top: 5),
                                              decoration: BoxDecoration(
                                                  borderRadius:
                                                      BorderRadius.circular(
                                                          Pallet.radius),
                                                  color:
                                                      Pallet.dashcontainerback),
                                              child: Center(
                                                  child: MyText(
                                                      text:
                                                          pageDetails["text3"],
                                                      style: TextStyle(
                                                          color:
                                                              Pallet.fontcolor,
                                                          fontWeight: FontWeight
                                                              .w600))),
                                              height: 30,
                                              width: 100,
                                            ),
                                            SizedBox(width: 15.0),
                                            MyText(
                                              text: pageDetails["text5"] +
                                                  ' ' +
                                                  total.toString(),
                                              style: TextStyle(
                                                  color:
                                                      Pallet.dashcontainerback,
                                                  fontSize: Pallet.subheading1,
                                                  fontWeight:
                                                      Pallet.subheading1wgt),
                                            ),
                                          ],
                                        ),
                                      ]),
                                ),
                              ),
                              SizedBox(height: 20),
                              dropdown(
                                  wdgtwidth: widget.wdgtWidth * .83888,
                                  boxperow: 3,
                                  dropfontsize: 8),
                            ],
                          )),
                          SizedBox(height: 15),
                          Column(
                            children: [
                              SizedBox(height: widget.wdgtWidth * .065),
                              Center(
                                  child: utilizeAssets(
                                      wdgtwidth: widget.wdgtWidth * .8)),
                            ],
                          ),
                        ]),
                  ),
                );
              } else if (constraints.maxWidth >= ScreenSize.ipad
                  // &&
                  // constraints.maxWidth <= ScreenSize.tab
                  ) {
                // isMobile == false
                // dashboard.accountType == 2
                //     ? packageCount =
                //         iconstatus == false ? packages.length : packageCount = 9
                //     : packageCount = iconstatus == false
                //         ? packages.length
                //         : packageCount = 5;

                return Container(
                  padding: EdgeInsets.only(
                      left: Pallet.leftPadding, top: Pallet.topPadding1),
                  child: SingleChildScrollView(
                    child: Row(crossAxisAlignment: CrossAxisAlignment.start,
                        // mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: [
                          Center(
                            child: Container(
                                child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                ClipRRect(
                                  borderRadius:
                                      BorderRadius.circular(Pallet.radius),
                                  child: Container(
                                    padding: EdgeInsets.symmetric(
                                        horizontal: Pallet.defaultPadding,
                                        vertical: Pallet.defaultPadding),
                                    width: widget.wdgtWidth * .65,
                                    color: Pallet.fontcolor,
                                    child: Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          MyText(
                                            text: pageDetails["text1"],
                                            style: TextStyle(
                                              color: Pallet.fontcolornew,
                                              fontSize: Pallet.heading1,
                                            ),
                                          ),
                                          // SizedBox(height: 10.0),
                                          // MyText(text:
                                          //   "Digital Shopping Voucher",
                                          //   style: TextStyle(
                                          //       color: Pallet.dashcontainerback,
                                          //       fontSize: Pallet.subheading1,
                                          //       fontWeight:
                                          //           Pallet.subheading1wgt),
                                          // ),
                                          SizedBox(height: 15.0),
                                          Row(
                                            // mainAxisAlignment:
                                            //     MainAxisAlignment
                                            //         .,
                                            children: [
                                              Container(
                                                margin: EdgeInsets.only(
                                                    bottom:
                                                        Pallet.defaultPadding,
                                                    top: Pallet.defaultPadding),
                                                decoration: BoxDecoration(
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            Pallet.radius),
                                                    color: Pallet
                                                        .dashcontainerback),
                                                child: Center(
                                                    child: MyText(
                                                        text: pageDetails[
                                                            "text3"],
                                                        style: TextStyle(
                                                            color: Pallet
                                                                .fontcolor,
                                                            fontWeight:
                                                                FontWeight
                                                                    .w600))),
                                                height: 30,
                                                width: 100,
                                              ),
                                              SizedBox(width: 15.0),
                                              MyText(
                                                text: pageDetails["text5"] +
                                                    ' ' +
                                                    total.toString(),
                                                style: TextStyle(
                                                    color: Pallet.fontcolornew,
                                                    fontSize: Pallet.heading3,
                                                    fontWeight:
                                                        Pallet.subheading1wgt),
                                              ),
                                              SizedBox(width: 10.0),
                                              MyText(
                                                text: pageDetails["text18"],
                                                style: TextStyle(
                                                    color: Pallet.fontcolornew,
                                                    fontSize: Pallet.heading4,
                                                    fontWeight:
                                                        Pallet.subheading1wgt),
                                              ),
                                            ],
                                          ),
                                        ]),
                                  ),
                                ),
                                SizedBox(height: 20),
                                dropdown(
                                    wdgtwidth: widget.wdgtWidth * .68,
                                    boxperow: 6,
                                    dropfontsize: 8),
                              ],
                            )),
                          ),
                          // SizedBox(width: Pallet.leftPadding),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.end,
                            children: [
                              // CustomButton(
                              //   child: Center(
                              //     child: BlinkText(
                              //       pageDetails["text43"],
                              //       beginColor: Colors.orange,
                              //       endColor: Pallet.fontcolor,
                              //     ),
                              //   ),
                              //   hpadding: 15,
                              //   vpadding: 8,
                              //   onpress: () {
                              //     _showMaterialDialog(context);
                              //   },
                              //   textcolor: Pallet.fontcolor,
                              //   buttoncolor: Pallet.dashcontainerback,
                              //   fontweight: Pallet.font600,
                              // ),
                              SizedBox(height: 15),
                              Center(
                                  child: utilizeAssets(
                                      wdgtwidth: widget.wdgtWidth * .28)),
                            ],
                          ),
                        ]),
                  ),
                );
              }
              // else {
              //   //  isMobile == false,

              //   // dashboard.accountType == 2
              //   //     ? packageCount =
              //   //         iconstatus == false ? packages.length : packageCount = 8
              //   //     : packageCount = iconstatus == false
              //   //         ? packages.length
              //   //         : packageCount = 5;
              //   return SingleChildScrollView(
              //     child: Padding(
              //       padding: EdgeInsets.symmetric(
              //           horizontal: Pallet.leftPadding,
              //           vertical: Pallet.leftPadding),
              //       child: Container(
              //         child: Column(
              //             crossAxisAlignment: CrossAxisAlignment.start,
              //             children: [
              //               Row(
              //                 mainAxisAlignment: MainAxisAlignment.spaceBetween,
              //                 children: [
              //                   MyText(text:
              //                     pageDetails["text1"],
              //                     style: TextStyle(
              //                         color: Pallet.dashcontainerback,
              //                         fontSize: Pallet.heading1,
              //                         fontWeight: Pallet.heading2wgt),
              //                   ),
              //                   // FadeTransition(
              //                   //   opacity: _animationController,
              //                   //   child: CustomButton(
              //                   //     text: 'Offers',
              //                   //     hpadding: 15,
              //                   //     vpadding: 8,
              //                   //     textcolor: Pallet.fontcolor,
              //                   //     buttoncolor: Pallet.dashcontainerback,
              //                   //     onpress: () {
              //                   //       _showMaterialDialog(context);
              //                   //     },
              //                   //     fontweight: Pallet.font600,
              //                   //   ),
              //                   // ),
              //                   Container(
              //                     decoration: BoxDecoration(boxShadow: [
              //                       BoxShadow(
              //                         color:
              //                             Pallet.fontcolornew.withOpacity(0.5),
              //                         spreadRadius:
              //                             _animationController.value * 4,
              //                         blurRadius:
              //                             _animationController.value * 4,
              //                       )
              //                     ]),
              //                     child: CustomButton(
              //                       text: "Offers",
              //                       bshadow: false,
              //                       hpadding: 15,
              //                       vpadding: 8,
              //                       onpress: () {
              //                         _showMaterialDialog(context);
              //                       },
              //                       textcolor: Pallet.fontcolor,
              //                       buttoncolor: Pallet.dashcontainerback,
              //                       fontweight: Pallet.font600,
              //                     ),
              //                   ),
              //                 ],
              //               ),
              //               SizedBox(height: 10),
              //               Row(
              //                 crossAxisAlignment: CrossAxisAlignment.start,
              //                 children: [
              //                   Column(
              //                       mainAxisAlignment:
              //                           MainAxisAlignment.spaceEvenly,
              //                       crossAxisAlignment:
              //                           CrossAxisAlignment.start,
              //                       children: [
              //                         Row(
              //                           mainAxisAlignment:
              //                               MainAxisAlignment.spaceEvenly,
              //                           children: [
              //                             Container(
              //                               margin: EdgeInsets.only(
              //                                   bottom: Pallet.defaultPadding,
              //                                   top: Pallet.defaultPadding),
              //                               decoration: BoxDecoration(
              //                                   borderRadius:
              //                                       BorderRadius.circular(
              //                                           Pallet.radius),
              //                                   color:
              //                                       Pallet.dashcontainerback),
              //                               child: Center(
              //                                   child: MyText(text:
              //                                       pageDetails["text3"],
              //                                       style: TextStyle(
              //                                           color: Pallet.fontcolor,
              //                                           fontWeight:
              //                                               FontWeight.w600))),
              //                               height: 30,
              //                               width: 100,
              //                             ),
              //                             SizedBox(width: 10.0),
              //                             MyText(text:
              //                               pageDetails["text5"] +
              //                                   total.toString(),
              //                               style: TextStyle(
              //                                   color: Pallet.dashcontainerback,
              //                                   fontSize: Pallet.subheading1,
              //                                   fontWeight:
              //                                       Pallet.subheading1wgt),
              //                             ),
              //                             SizedBox(width: 10.0),
              //                             MyText(text:
              //                               "(Digital Shopping Voucher)",
              //                               style: TextStyle(
              //                                   color: Pallet.dashcontainerback,
              //                                   fontSize: Pallet.heading4,
              //                                   fontWeight:
              //                                       Pallet.subheading1wgt),
              //                             ),
              //                           ],
              //                         ),
              //                         SizedBox(height: 30),
              //                         dropdown(
              //                             wdgtwidth: widget.wdgtWidth * .65,
              //                             boxperow: 6,
              //                             dropfontsize: 9),
              //                       ]),
              //                   SizedBox(width: widget.wdgtWidth * .025),
              //                   Column(children: [
              //                     Padding(
              //                       padding:
              //                           EdgeInsets.all(Pallet.defaultPadding),
              //                       child: utilizeAssets(
              //                           wdgtwidth: widget.wdgtWidth * .2),
              //                     )
              //                   ])
              //                 ],
              //               )
              //             ]),
              //       ),
              //     ),
              //   );
              //   // Container(
              //   //   padding: EdgeInsets.only(
              //   //       top: Pallet.topPadding, left: Pallet.leftPadding),
              //   //   child: SingleChildScrollView(
              //   //     child: Row(crossAxisAlignment: CrossAxisAlignment.start,
              //   //         // mainAxisAlignment: MainAxisAlignment.spaceAround,
              //   //         children: [
              //   //           Center(
              //   //             child: Container(
              //   //                 width: widget.wdgtWidth * .45,
              //   //                 child: Column(
              //   //                   crossAxisAlignment: CrossAxisAlignment.start,
              //   //                   children: [
              //   //                     ClipRRect(
              //   //                       borderRadius:
              //   //                           BorderRadius.circular(Pallet.radius),
              //   //                       child: Container(
              //   //                         padding: EdgeInsets.symmetric(
              //   //                             horizontal: Pallet.defaultPadding,
              //   //                             vertical: Pallet.defaultPadding),
              //   //                         width: widget.wdgtWidth * .45,
              //   //                         color: Pallet.fontcolor,
              //   //                         child: Column(
              //   //                             crossAxisAlignment:
              //   //                                 CrossAxisAlignment.start,
              //   //                             children: [
              //   // MyText(text:
              //   //   pageDetails["text1"],
              //   //   style: TextStyle(
              //   //       color: Pallet
              //   //           .dashcontainerback,
              //   //       fontSize: Pallet.heading1,
              //   //       fontWeight:
              //   //           Pallet.heading1wgt),
              //   // ),
              //   //                               SizedBox(height: 15.0),
              //   // Row(
              //   //   children: [
              //   //     Container(
              //   //       margin: EdgeInsets.only(
              //   //           bottom: Pallet
              //   //               .defaultPadding,
              //   //           top: Pallet
              //   //               .defaultPadding),
              //   //       decoration: BoxDecoration(
              //   //           borderRadius:
              //   //               BorderRadius
              //   //                   .circular(Pallet
              //   //                       .radius),
              //   //           color: Pallet
              //   //               .dashcontainerback),
              //   //       child: Center(
              //   //           child: MyText(text:
              //   //               pageDetails[
              //   //                   "text3"],
              //   //               style: TextStyle(
              //   //                   color: Pallet
              //   //                       .fontcolor,
              //   //                   fontWeight:
              //   //                       FontWeight
              //   //                           .w600))),
              //   //       height: 30,
              //   //       width: 100,
              //   //     ),
              //   //     SizedBox(width: 10.0),
              //   //     MyText(text:
              //   //       pageDetails["text5"] +
              //   //           total.toString(),
              //   //       style: TextStyle(
              //   //           color: Pallet
              //   //               .dashcontainerback,
              //   //           fontSize:
              //   //               Pallet.subheading1,
              //   //           fontWeight: Pallet
              //   //               .subheading1wgt),
              //   //     ),
              //   //   ],
              //   // ),
              //   //                             ]),
              //   //                       ),
              //   //                     ),
              //   //                     SizedBox(height: 20),
              //   //                     Center(
              //   // child: dropdown(
              //   //     wdgtwidth: widget.wdgtWidth * .45,
              //   //     boxperow: 5,
              //   //     fontsize: 12),
              //   //                     ),
              //   //                   ],
              //   //                 )),
              //   //           ),
              //   //           SizedBox(width: Pallet.leftPadding),
              //   //           Column(
              //   //             children: [
              //   //               SizedBox(height: widget.wdgtWidth * .045),
              //   //               Center(
              //   // child: utilizeAssets(
              //   //     wdgtwidth: widget.wdgtWidth * .4)),
              //   //             ],
              //   //           ),
              //   //         ]),
              //   //   ),
              //   // );
              // }
            },
          );
        } else {
          return Loader();
        }
      },
    );
  }

  // Widget gridView() {
  //   if (packages.length > 0) {
  //   } else {
  //     Container();
  //   }
  // }

  Container dropdown({double wdgtwidth, int boxperow, double dropfontsize}) {
    return packageDesignContainer(wdgtwidth, boxperow, dropfontsize);
  }

  Container packageDesignContainer(
      double wdgtwidth, int boxperow, double dropfontsize) {
    return Container(
      // color: Colors.pink,
      padding: EdgeInsets.symmetric(horizontal: Pallet.defaultPadding),
      // decoration: BoxDecoration(
      //   color: Pallet.fontcolor,
      //   boxShadow: [Pallet.shadowEffect],
      // ),
      width: wdgtwidth,
      // height: 500,
      child: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Wrap(
                crossAxisAlignment: WrapCrossAlignment.center,
                spacing: (boxperow * 2).toDouble(),
                runSpacing: (boxperow * 2).toDouble(),
                children: [
                  for (var i = 0;
                      isDropDown == true
                          ? i < tempList.length
                          : i < packages.length;
                      i++)
                    Container(
                      padding: EdgeInsets.only(
                          bottom: 5,
                          // left: 5,
                          right: wdgtwidth * 0.01,
                          top: wdgtwidth * 0.01),
                      width: isDropDown == true
                          ? wdgtwidth / (boxperow + .4)
                          : packages[i]["package_qty"] != 0
                              ? wdgtwidth / (boxperow - 1.585)
                              : wdgtwidth / (boxperow - 1.585),
                      //  wdgtwidth / (boxperow - 1.2),

                      // packages[i]["package_qty"] != 0
                      // ? wdgtwidth / (boxperow - .4)
                      //     : wdgtwidth / (boxperow + .35),
                      decoration: BoxDecoration(
                          border: packages[i]["package_qty"] != 0
                              ? Border.all(color: Pallet.fontcolornew, width: 1)
                              : packages[i]["qty"] > 0
                                  ? Border.all(color: Pallet.grey, width: 2)
                                  : null,
                          boxShadow: [Pallet.shadowEffect],
                          borderRadius: BorderRadius.circular(Pallet.radius),
                          // border: Border.all(color: Pallet.dashcontainerback),
                          color: Pallet.fontcolor),
                      child: Column(
                        children: [
                          Container(
                            padding: EdgeInsets.symmetric(
                              vertical: 10,
                            ),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                packages[i]["package_qty"] == 1
                                    ? Label(
                                        triangleHeight: 7.0,
                                        edge: Edge.RIGHT,
                                        child: Container(
                                          padding: const EdgeInsets.only(
                                              left: 8.0,
                                              right: 10.0,
                                              top: 5.0,
                                              bottom: 5.0),
                                          color: Pallet.fontcolornew,
                                          child: Center(
                                            child: MyText(
                                              text: pageDetails['text19'],
                                              style: TextStyle(
                                                color: Pallet.fontcolor,
                                                fontSize: dropfontsize,
                                              ),
                                            ),
                                          ),
                                        ),
                                      )
                                    : Container(),
                                // packages[i]["package_qty"] != 0
                                //     ? Container(
                                //         padding: EdgeInsets.all(5),
                                //         decoration: BoxDecoration(
                                //           borderRadius:
                                //               BorderRadius.circular(15),
                                //           color: Colors.red,
                                //         ),
                                //         child: MyText(text:
                                //           packages[i]["package_qty"] != 0
                                //               ? pageDetails['text20'] +
                                //                   ' ' +
                                //                   packages[i]["package_qty"]
                                //                       .toString() +
                                //                   pageDetails['text21']
                                //               : "",
                                //           style: TextStyle(
                                //             fontSize: dropfontsize,
                                //             color: Pallet.fontcolor,
                                //           ),
                                //         ),
                                //       )
                                //     : Container(
                                //         child: SizedBox(
                                //         height: 15,
                                //       )),
                              ],
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(
                              bottom: 10,
                            ),
                            child: MyText(
                              text: packages[i]["package_name"].toString(),
                              style: TextStyle(
                                  color: Pallet.dashcontainerback,
                                  fontSize: Pallet.heading4,
                                  fontWeight: FontWeight.w600),
                            ),
                          ),
                          if (dashboard.accountType == 2)
                            Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Container(
                                  decoration: BoxDecoration(
                                    color: packages[i]["package_qty"] != 0
                                        ? Pallet.fontcolornew
                                        : Pallet.grey,
                                    borderRadius: BorderRadius.circular(4),
                                  ),
                                  padding: EdgeInsets.all(3),
                                  child: MyText(
                                    text: "+ " +
                                        (packages[i]["offer_val"] * 100)
                                            .toString() +
                                        '%',
                                    style: TextStyle(
                                        fontSize: dropfontsize,
                                        color: Pallet.fontcolor),
                                  ),
                                ),
                                SizedBox(
                                  width: 3,
                                ),
                                Container(
                                  decoration: BoxDecoration(
                                    color: packages[i]["package_qty"] != 0
                                        ? Pallet.fontcolornew
                                        : Pallet.grey,
                                    borderRadius: BorderRadius.circular(4),
                                  ),
                                  padding: EdgeInsets.all(3),
                                  child: MyText(
                                    text: pageDetails['text22'],
                                    style: TextStyle(
                                      fontSize: dropfontsize,
                                      color: Pallet.fontcolor,
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          SizedBox(
                            height: 10,
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Container(
                                  // margin: EdgeInsets.symmetric(horizontal: 2),
                                  height: 30,
                                  decoration: BoxDecoration(
                                      // boxShadow: [Pallet.shadowEffect],
                                      border: Border.all(
                                          color: packages[i]["qty"] != 0
                                              ? packages[i]["package_qty"] != 0
                                                  ? Pallet.fontcolornew
                                                  : Pallet.grey
                                              : packages[i]["package_qty"] == 0
                                                  ? Pallet.grey
                                                  : Pallet.fontcolornew),
                                      borderRadius:
                                          BorderRadius.circular(Pallet.radius),
                                      color: packages[i]["qty"] != 0
                                          ? packages[i]["package_qty"] != 0
                                              ? Pallet.fontcolornew
                                              : packages[i]["package_qty"] == 0
                                                  ? Pallet.grey
                                                  : Pallet.fontcolor
                                          : Pallet.fontcolor),
                                  child: Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceAround,
                                    children: [
                                      IconButton(
                                        icon: Icon(Icons.remove),
                                        color: packages[i]["qty"] != 0
                                            ? Pallet.fontcolor
                                            : Pallet.fontcolornew,
                                        iconSize: 15,
                                        onPressed: () {
                                          //print(tempList);
                                          //print('.......');

                                          setState(() {
                                            if (tradeSelected) {
                                              //print(
                                              // "hello_trade_bal $tradeBal");
                                              temp_selected_trade =
                                                  packages[i]['price'];
                                              //print(temp_selected_trade);

                                              if (tradeSelected &&
                                                  tradeBal > 0) {
                                                if (temp_selected_trade >
                                                        tradeBal &&
                                                    !cashSelected) {
                                                  snack.snack(
                                                      title: pageDetails[
                                                          'text23']);
                                                  // final snackBar = SnackBar(
                                                  //     content: MyText(text:pageDetails[
                                                  //         'text23']));
                                                  // ScaffoldMessenger.of(context)
                                                  //     .showSnackBar(snackBar);
                                                } else {
                                                  temp_total = temp_total -
                                                      packages[i]['price'];
                                                  //print("temp_total");
                                                  //print(temp_total);
                                                  if (temp_total > tradeBal &&
                                                      !cashSelected) {
                                                    snack.snack(
                                                        title: pageDetails[
                                                            'text23']);
                                                    // final snackBar = SnackBar(
                                                    //     content: MyText(text:
                                                    //         pageDetails[
                                                    //             'text23']));
                                                    // ScaffoldMessenger.of(
                                                    //         context)
                                                    //     .showSnackBar(snackBar);
                                                  } else {
                                                    sub(packages[i]);
                                                  }
                                                }
                                              } else {
                                                snack.snack(
                                                    title:
                                                        pageDetails['text24']);
                                                // final snackBar = SnackBar(
                                                //     content: MyText(text:
                                                //         pageDetails['text24']));
                                                // ScaffoldMessenger.of(context)
                                                //     .showSnackBar(snackBar);
                                              }
                                            } else if (tradeSelected) {
                                              if (cashSelected && cashBal > 0) {
                                                sub(packages[i]);
                                              } else {
                                                snack.snack(
                                                    title:
                                                        pageDetails['text25']);
                                                // final snackBar = SnackBar(
                                                //     content: MyText(text:
                                                //         pageDetails['text25']));
                                                // ScaffoldMessenger.of(context)
                                                //     .showSnackBar(snackBar);
                                              }
                                            } else if (!tradeSelected &&
                                                !cashSelected) {
                                              snack.snack(
                                                  title: pageDetails['text26']);
                                              // final snackBar = SnackBar(
                                              //     content: MyText(text:
                                              //         pageDetails['text26']));
                                              // ScaffoldMessenger.of(context)
                                              //     .showSnackBar(snackBar);
                                            } else {
                                              setState(() {
                                                if (cashSelected) {
                                                  sub(packages[i]);
                                                } else {
                                                  snack.snack(
                                                      title: pageDetails[
                                                          'text26']);
                                                  // final snackBar = SnackBar(
                                                  //     content: MyText(text:pageDetails[
                                                  //         'text26']));
                                                  // ScaffoldMessenger.of(context)
                                                  //     .showSnackBar(snackBar);
                                                }
                                              });
                                            }
                                          });
                                        },
                                      ),
                                      MyText(
                                          text: packages[i]["qty"].toString(),
                                          style: TextStyle(
                                              fontSize: Pallet.heading4,
                                              color: packages[i]["qty"] != 0
                                                  ? Pallet.fontcolor
                                                  : Pallet.fontcolornew,
                                              fontWeight: FontWeight.w600)),
                                      IconButton(
                                        icon: Icon(Icons.add),
                                        color: packages[i]["qty"] != 0
                                            ? Pallet.fontcolor
                                            : Pallet.fontcolornew,
                                        iconSize: 15,
                                        onPressed: () {
                                          print('Package id1');
                                          print(packages[i]["package_id"]);
                                          //print(packages.length);
                                          //print('-----------------');

                                          //print(packages[i]);
                                          if (dashboard.accountType == 2) {
                                            print('Package id2');
                                            print(packages[i]["package_id"]);
                                            if (packages[i]["package_id"] ==
                                                    5 &&
                                                packages[i]["qty"] == 0) {
                                              print('Package id3');
                                              print(packages[i]["package_id"]);
                                              showDialog(
                                                  barrierDismissible: false,
                                                  context: context,
                                                  builder:
                                                      (BuildContext context) {
                                                    return AlertDialog(
                                                      backgroundColor: Pallet
                                                          .popupcontainerback,
                                                      shape: RoundedRectangleBorder(
                                                          borderRadius:
                                                              BorderRadius
                                                                  .circular(Pallet
                                                                      .radius)),
                                                      title: Row(
                                                        mainAxisAlignment:
                                                            MainAxisAlignment
                                                                .start,
                                                        children: [
                                                          Image.asset(
                                                              "c-logo.png",
                                                              width: 30),
                                                          SizedBox(width: 10),
                                                          Expanded(
                                                            child: MyText(
                                                                text:
                                                                    'Special Package',
                                                                style: TextStyle(
                                                                    fontSize: Pallet
                                                                        .heading3,
                                                                    color: Pallet
                                                                        .fontcolor,
                                                                    fontWeight:
                                                                        Pallet
                                                                            .font500)),
                                                          ),
                                                        ],
                                                      ),
                                                      content: Container(
                                                        width: 150,
                                                        child: Column(
                                                          mainAxisSize:
                                                              MainAxisSize.min,
                                                          children: [
                                                            MyText(
                                                                text:
                                                                    'Following your 1000 DSV purchase, 500 USDC will be added to the AA+ trading pool. You will receive a 500DSV credit into your account immediately, another 500+100 DSV will be added after 8 months from the date of purchase.',
                                                                textAlign:
                                                                    TextAlign
                                                                        .justify,
                                                                style:
                                                                    TextStyle(
                                                                  fontSize: Pallet
                                                                      .heading4,
                                                                  height: 1.3,
                                                                  color: Pallet
                                                                      .fontcolor,
                                                                )),
                                                          ],
                                                        ),
                                                      ),
                                                      actions: [
                                                        PopupButton(
                                                          text: 'Cancel',
                                                          onpress: () {
                                                            Navigator.pop(
                                                                context);
                                                          },
                                                        ),
                                                        PopupButton(
                                                          text: 'Proceed',
                                                          onpress: () {
                                                            Navigator.pop(
                                                                context);
                                                            setState(() {
                                                              tradeSelected =
                                                                  false;
                                                              cashSelected =
                                                                  true;
                                                              iscashonly = true;
                                                            });

                                                            updatepackage(i);
                                                          },
                                                        ),
                                                      ],
                                                    );
                                                  });
                                            }
                                            // else if (packages[i]
                                            //             ["package_id"] ==
                                            //         2 &&
                                            //     packages[i]["qty"] == 0) {
                                            //   print('Package id3');
                                            //   print(packages[i]["package_id"]);
                                            //   showDialog(
                                            //       barrierDismissible: false,
                                            //       context: context,
                                            //       builder:
                                            //           (BuildContext context) {
                                            //         return AlertDialog(
                                            //           backgroundColor: Pallet
                                            //               .popupcontainerback,
                                            //           shape: RoundedRectangleBorder(
                                            //               borderRadius:
                                            //                   BorderRadius
                                            //                       .circular(Pallet
                                            //                           .radius)),
                                            //           title: Row(
                                            //             mainAxisAlignment:
                                            //                 MainAxisAlignment
                                            //                     .start,
                                            //             children: [
                                            //               Image.asset(
                                            //                   "c-logo.png",
                                            //                   width: 30),
                                            //               SizedBox(width: 10),
                                            //               Expanded(
                                            //                 child: MyText(
                                            //                     text:
                                            //                         'Special Package',
                                            //                     style: TextStyle(
                                            //                         fontSize: Pallet
                                            //                             .heading3,
                                            //                         color: Pallet
                                            //                             .fontcolor,
                                            //                         fontWeight:
                                            //                             Pallet
                                            //                                 .font500)),
                                            //               ),
                                            //             ],
                                            //           ),
                                            //           content: Container(
                                            //             width: 150,
                                            //             child: Column(
                                            //               mainAxisSize:
                                            //                   MainAxisSize.min,
                                            //               children: [
                                            //                 MyText(
                                            //                     text:
                                            //                         'As a dedicated \$ 100 package for AA+ token, the purchase of such will trigger an instant credit of 200 AA+ token into your account. No DSV will be credited.',
                                            //                     textAlign:
                                            //                         TextAlign
                                            //                             .justify,
                                            //                     style:
                                            //                         TextStyle(
                                            //                       fontSize: Pallet
                                            //                           .heading4,
                                            //                       height: 1.3,
                                            //                       color: Pallet
                                            //                           .fontcolor,
                                            //                     )),
                                            //               ],
                                            //             ),
                                            //           ),
                                            //           actions: [
                                            //             PopupButton(
                                            //               text: 'Cancel',
                                            //               onpress: () {
                                            //                 Navigator.pop(
                                            //                     context);
                                            //               },
                                            //             ),
                                            //             PopupButton(
                                            //               text: 'Proceed',
                                            //               onpress: () {
                                            //                 Navigator.pop(
                                            //                     context);
                                            //                 cashSelected = true;
                                            //                 tradeSelected =
                                            //                     false;
                                            //                 updatepackage(i);
                                            //               },
                                            //             ),
                                            //           ],
                                            //         );
                                            //       });
                                            // }
                                            // if (isKyc != 'verified') {
                                            //   showDialog(
                                            //       barrierDismissible: false,
                                            //       context: context,
                                            //       builder:
                                            //           (BuildContext context) {
                                            //         return KycPopUp();
                                            //       });
                                            // }
                                            else {
                                              updatepackage(i);
                                            }
                                          } else {
                                            setState(() {
                                              if (cashSelected) {
                                                add(
                                                    packages[i]['package_id'],
                                                    packages[i]['price'],
                                                    context);
                                              } else {
                                                snack.snack(
                                                    title:
                                                        pageDetails['text26']);
                                                // final snackBar = SnackBar(
                                                //     content: MyText(text:
                                                //         pageDetails['text26']));
                                                // ScaffoldMessenger.of(context)
                                                //     .showSnackBar(snackBar);
                                              }
                                            });
                                          }
                                        },
                                      )
                                    ],
                                  )),
                            ],
                          ),
                        ],
                      ),
                      //Divider(height:6,color:Colors.white)
                    ),
                ]),
            SizedBox(
              height: 10,
            ),
            // IconData(U+0E04F),
            // packages.length >= 4
            // packageCount > 4
            // ?
            isDropDown == true
                ? Center(
                    child: CircleAvatar(
                      backgroundColor: Pallet.fontcolornew,
                      child: InkWell(
                        onTap: () {
                          setState(() {
                            iconstatus = !iconstatus;
                            //print(packageCount);
                            //print(iconstatus);
                            //print('-------------');
                            // isMobile == false
                            //     ? packageCount = iconstatus == false
                            //         ? packages.length
                            //         : packageCount = 8
                            //     : packageCount = iconstatus == false
                            //         ? packages.length
                            //         : packageCount = 5;
                            iconstatus == false
                                ? tempList = packages
                                : tempList =
                                    // dashboard.accountType == 1?
                                    packages.sublist(0, 4);
                            // : packages.sublist(0, 8);
                            //print('-------0------');
                            //print(packageCount);
                            //print(iconstatus);
                          });
                        },
                        child: Icon(
                          // icon,
                          icon = iconstatus == true
                              ? Icons.arrow_drop_down_outlined
                              : Icons.arrow_drop_up_outlined,

                          color: Pallet.fontcolor, size: 40,
                        ),
                      ),
                    ),
                  )
                : MyText(text: '__', style: TextStyle(color: Pallet.fontcolor))
          ],
          // )
        ),
      ),
    );
  }

  activespecialpackage() {
    bool activepackage = false;
    for (var items in packages) {
      if (items["package_id"] == 5 && items["qty"] > 0) {
        activepackage = true;
        // tradeSelected = false;
      }
      // else if (items["package_id"] == 2 && items["qty"] > 0) {
      //   activepackage = true;
      // }
    }
    return activepackage;
  }

  updatepackage(i) {
    setState(() {
      if (tradeSelected) {
        //print(
        // "hello_trade_bal $tradeBal");
        temp_selected_trade = packages[i]['price'];
        //print(temp_selected_trade);

        if (tradeSelected && tradeBal > 0) {
          if (temp_selected_trade > tradeBal && !cashSelected) {
            snack.snack(title: pageDetails['text23']);
            // final snackBar = SnackBar(
            //     content: MyText(text:
            //         pageDetails[
            //             'text23']));
            // ScaffoldMessenger.of(
            //         context)
            //     .showSnackBar(snackBar);
          } else {
            temp_total = temp_total + packages[i]['price'];
            //print("temp_total");
            //print(temp_total);
            if (temp_total > tradeBal && !cashSelected) {
              temp_total = temp_total - packages[i]['price'];
              snack.snack(title: pageDetails['text23']);
              // final snackBar = SnackBar(
              //     content: MyText(text:
              //         pageDetails[
              //             'text23']));
              // ScaffoldMessenger.of(
              //         context)
              //     .showSnackBar(
              //         snackBar);
            } else {
              add(packages[i]['package_id'], packages[i]['price'], context);
            }
          }
        } else {
          snack.snack(title: pageDetails['text24']);
          // final snackBar = SnackBar(
          //     content: MyText(text:pageDetails[
          //         'text24']));
          // ScaffoldMessenger.of(context)
          //     .showSnackBar(snackBar);
        }
      } else if (cashSelected) {
        if (cashSelected && cashBal > 0) {
          add(packages[i]['package_id'], packages[i]['price'], context);
        } else {
          snack.snack(title: pageDetails['text25']);
          // final snackBar = SnackBar(
          //     content: MyText(text:pageDetails[
          //         'text25']));
          // ScaffoldMessenger.of(context)
          //     .showSnackBar(snackBar);
        }
      } else if (!tradeSelected && !cashSelected) {
        snack.snack(title: pageDetails['text26']);
        // final snackBar = SnackBar(
        //     content: MyText(text:
        //         pageDetails['text26']));
        // ScaffoldMessenger.of(context)
        //     .showSnackBar(snackBar);
      } else {
        setState(() {
          add(packages[i]['package_id'], packages[i]['price'], context);
        });
      }

      // if (tradeSelected &&
      //     tradeBal > 0) {
      //   add(
      //       packages[i]['package_id'],
      //       packages[i]['price'],
      //       context);
      // } else if (!tradeSelected &&
      //     !cashSelected) {
      //   final snackBar = SnackBar(
      //       content: MyText(text:
      //           "Please select any Account"));
      //   ScaffoldMessenger.of(context)
      //       .showSnackBar(snackBar);
      // } else if (cashSelected &&
      //     cashBal > 0) {
      //   add(
      //       packages[i]['package_id'],
      //       packages[i]['price'],
      //       context);
      // } else {
      //   final snackBar = SnackBar(
      //       content: MyText(text:
      //           "Trade balance is 0. Please Select cash account"));
      //   ScaffoldMessenger.of(context)
      //       .showSnackBar(snackBar);
      // }
    });
  }

  voucherAlert(BuildContext context) {
    // set up the buttons
    // ignore: deprecated_member_use
    Widget cancelButton = PopupButton(
      onpress: () {
        Navigator.of(context).pop();
      },
      text: pageDetails['text15'],
      buttoncolor: Pallet.fontcolor,
      textcolor: Pallet.fontcolornew,
    );
    // ignore: deprecated_member_use
    Widget continueButton = PopupButton(
      onpress: () {},
      text: pageDetails['text12'],
      buttoncolor: Pallet.fontcolor,
      textcolor: Pallet.fontcolornew,
    );

    // set up the AlertDialog
    AlertDialog alert = AlertDialog(
      title: MyText(text: pageDetails['text27']),
      content: MyText(text: pageDetails['text28']),
      actions: [
        cancelButton,
        continueButton,
      ],
    );

    // show the dialog
    showDialog(
      barrierDismissible: false,
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }

  Container utilizeAssets({double wdgtwidth}) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: Pallet.defaultPadding),
      width: wdgtwidth,
      // color: Colors.green,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          MyText(
              text: pageDetails["text7"],
              style: TextStyle(
                  fontSize: Pallet.heading3,
                  color: Pallet.fontcolornew,
                  fontWeight: Pallet.subheading1wgt)),
          SizedBox(height: 45),
          Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              crossAxisAlignment: CrossAxisAlignment.center,
              // spacing: 10,
              // runSpacing: 10,
              children: [
                // for (var i = 0; i < cart.assets.length; i++)
                if (dashboard.accountType != 1)
                  Option(
                      // ? tradeSelected = false
                      // : tradeSelected = true,
                      wdgtWidth: wdgtwidth,
                      title: pageDetails["text9"],
                      subtitle: pageDetails["text29"] +
                          ' ' +
                          tradeBal_view.toString(),
                      icon: appSettings['SERVER_URL'] +
                          '/' +
                          'assets/trading.png',
                      selected: tradeSelected,
                      onselect: () {
                        if (!activespecialpackage()) {
                          setState(() {
                            //print("_hello2");
                            // if (cashSelected == false && tradeSelected == true) {
                            //   //print("_hello3");
                            //   cart = [];
                            // }
                            if (tradeSelected == true && cashSelected == true) {
                              tradeSelected = false;
                              tradAccount.text = '';
                            } else {
                              tradeSelected = true;
                            }
                            // tradeSelected == true
                            //     ? tradeSelected = false
                            //     : tradeSelected = true;
                            cashError = null;
                            tradError = null;

                            autofill();
                          });
                        } else {
                          snack.snack(
                              title:
                                  'only cash account is allowed for buying special DSV pack');
                        }
                      }),
                SizedBox(height: 25),
                Option(
                    // ? cashSelected = false
                    // : cashSelected = true,
                    wdgtWidth: wdgtwidth,
                    title: pageDetails["text8"],
                    enable: cashBal_view.toString().length > 10 ? true : false,
                    amt: '\$ ' + cashBal_view.toString(),
                    subtitle: cashBal_view.toString().length > 10
                        ? pageDetails["text29"]
                        : pageDetails["text29"] +
                            ' \$ ' +
                            cashBal_view.toString(),
                    // ' ' +
                    // '\$ ' +
                    // cashBal_view.toString(),
                    icon: appSettings['SERVER_URL'] + '/' + 'assets/cash.png',
                    selected: cashSelected,
                    onselect: () {
                      setState(() {
                        //print("_hello1");
                        //print(cashSelected);
                        //print(tradeSelected);

                        // if (cashSelected == true && tradeSelected == false) {
                        //   //print("_hello4");
                        //   cart = [];
                        // }
                        if (cashSelected == true && tradeSelected == true) {
                          cashSelected = false;
                          cashAccount.text = '';
                        } else {
                          cashSelected = true;
                        }
                        // cashSelected == true
                        //     ? cashSelected = false
                        //     : cashSelected = true;
                        cashError = null;
                        tradError = null;

                        //print(cashSelected);
                        //print(tradeSelected);
                        //print(dashboard.accountType);
                        //print(dashboard.accountType != 1);
                        if (dashboard.accountType == 1) {
                          //print('autofill1');
                          autofill1();
                        } else {
                          //print('autofill');
                          autofill();
                        }
                      });
                    }),
              ],
            ),
          ),
          SizedBox(height: 30),

          if (tradeSelected == true)
            Textbox(
              tabpress: (event) {
                if (event.isKeyPressed(LogicalKeyboardKey.tab)) {
                  FocusScope.of(context).requestFocus(continuenode);
                }
              },
              focusnode: tradeaccNode,
              onsubmit: () {
                FocusScope.of(context).requestFocus(continuenode);
              },
              errorText: tradError,
              header: pageDetails["text11"],
              label: pageDetails["text11"],
              controller: tradAccount,
              validation: (value) {
                autofill(value: value, isCash: false);
              },
            ),
          // dashboard.accountType == 2
          //     ? (showAdminFee == 'f')
          //         ? Padding(
          //             padding: EdgeInsets.all(Pallet.defaultPadding),
          //             child: MyText(text:
          //               'Admin Fee 30, charged once at first DSV purchase only!',
          //               style: TextStyle(
          //                   fontSize: Pallet.heading2,
          //                   color: Pallet.dashcontainerback),
          //             ),
          //           )
          //         : SizedBox(height: 25)
          //     : SizedBox(height: 25),
          SizedBox(height: 10),

          if (cashSelected == true)
            Textbox(
              focusnode: cashaccnode,
              errorText: cashError,
              tabpress: (event) {
                if (tradeSelected == true) {
                  if (event.isKeyPressed(LogicalKeyboardKey.tab)) {
                    FocusScope.of(context).requestFocus(tradeaccNode);
                  }
                } else {
                  if (event.isKeyPressed(LogicalKeyboardKey.tab)) {
                    FocusScope.of(context).requestFocus(continuenode);
                  }
                }
              },
              header: pageDetails["text10"],
              onsubmit: () {
                if (tradeSelected == true) {
                  FocusScope.of(context).requestFocus(continuenode);
                }
              },
              label: pageDetails["text10"],
              controller: cashAccount,
              validation: (value) {
                if (dashboard.accountType != 1) {
                  autofill(value: value, isCash: true);
                } else {
                  autofill1();
                }
              },
            ),
          SizedBox(height: 10),
          dashboard.accountType == 2
              ? (showAdminFee == 'f')
                  ? Padding(
                      padding: EdgeInsets.all(Pallet.defaultPadding),
                      child: MyText(
                        text: pageDetails['text30'],
                        style: TextStyle(
                            fontSize: Pallet.heading4,
                            fontWeight: Pallet.font500,
                            color: Pallet.fontcolornew),
                      ),
                    )
                  : SizedBox(height: 5)
              : SizedBox(height: 5),
          SizedBox(height: 15),
          if (cashSelected == true || tradeSelected == true)
            Center(
              child: Container(
                width: double.infinity,
                child: RawKeyboardListener(
                  focusNode: continuenode,
                  onKey: (event) {
                    if (event.isKeyPressed(LogicalKeyboardKey.enter)) {
                      setState(() {
                        buyPackages(context);
                      });
                    }
                  },
                  // ignore: deprecated_member_use

                  // ignore: deprecated_member_use
                  // child: FlatButton(
                  //     height: 50,
                  //     shape: RoundedRectangleBorder(
                  //         borderRadius: BorderRadius.circular(Pallet.radius),
                  //         side: BorderSide(
                  //             color: Pallet.dashcontainerback, width: 2)),
                  //     padding: EdgeInsets.all(Pallet.defaultPadding),
                  //     child: MyText(text:
                  //       pageDetails["text16"],
                  //       style: TextStyle(
                  //         fontSize: Pallet.heading3,
                  //       ),
                  //     ),
                  //     color: Pallet.dashcontainerback,
                  //     textColor: Pallet.fontcolor,
                  //     onPressed: () {
                  //       //print(isKyc);
                  //       //print(
                  //       // '00000000000000000000000000000000000000000000000');
                  //       // if (isKyc == 'not_verified') {
                  //       //   showDialog(
                  //       //       barrierDismissible: false,
                  //       //       context: context,
                  //       //       builder: (BuildContext context) {
                  //       //         return KycPopUp();
                  //       //       });
                  //       // } else {
                  //       setState(() {
                  //         buyPackages(context);
                  //       });
                  //     }
                  //     // },
                  //     ),
                  child: CustomButton(
                    onpress: () {
                      userActiveStatus == false
                          ? snack.snack(
                              title:
                                  'Admin has blocked your transfer permissions')
                          :
                          //print(isKyc);
                          //print(
                          // '00000000000000000000000000000000000000000000000');
                          // if (isKyc == 'not_verified') {
                          //   showDialog(
                          //       barrierDismissible: false,
                          //       context: context,
                          //       builder: (BuildContext context) {
                          //         return KycPopUp();
                          //       });
                          // } else {
                          setState(() {
                              buyPackages(context);
                            });
                    },
                    vpadding: 12,
                    text: pageDetails['text16'],
                    buttoncolor: Pallet.fontcolornew,
                    textcolor: Pallet.fontcolor,
                    textsize: Pallet.heading3,
                  ),
                ),
              ),
            ),
          SizedBox(height: 25),

          // Center(
          //   child: FlatButton(
          //     color: Colors.transparent,
          //     onPressed: () {
          //       setState(() {
          //         buyPackages(context);
          //       });
          //     },
          //     child: Container(
          //         decoration: BoxDecoration(
          //             borderRadius: BorderRadius.circular(7),
          //             color: Pallet.inner1),
          //         width: wdgtwidth * .75,
          //         height: 50,
          //         child: Center(
          //           child: MyText(text:
          //             pageDetails["text12"],
          //             style: TextStyle(color: Pallet.fontcolor),
          //           ),
          //         )),
          //   ),
          // ),
        ],
      ),
    );
  }

  autofill({String value, bool isCash}) async {
    try {
      //print('called');
      if (cashBal + tradeBal >= total) {
        //print("-indetified");
        double cashUsed = 0;
        double tradeUsed = 0;
        double adder = 0;
        if (cashAccount.text.length > 0) {
          cashUsed = double.parse(cashAccount.text);
        }
        if (tradAccount.text.length > 0) {
          //print("-i12312");
          tradeUsed = double.parse(tradAccount.text);
        }

        //print("-indetified123-0");
        //print(value);
        print(total);
        //print(temp_selected_trade);

        if (value == null) {
          // cashAccount.text = "";
          // tradAccount.text = "";
          //print("-indetified4");
          if (cashSelected && tradeSelected) {
            if (tradeBal >= total) {
              print("-indetified123");
              print(cashUsed);
              print(tradeUsed);
              print(temp_selected_trade);
              print(tradeBal);
              print(cashBal);
              print(total);

              if (tradeSelected && cashSelected) {
                // if (cashUsed == 0) {
                //   double temp_val1 = total - tradeUsed;
                //   tradAccount.text = temp_val1.toString();
                // } else {
                if (tradeBal <= tradeUsed) {
                  // if(cashUsed>temp_total)
                  // {

                  // }
                  // else
                  // {
                  adder = total - tradeBal;
                  print("-indetified54593");
                  //print(adder);
                  //print(total);
                  //print(tradeBal);
                  if (adder < 0) {
                    print("object56gfd");
                    // ignore: non_constant_identifier_names
                    double tem_val = temp_selected_trade - cashUsed + adder;
                    //print(tem_val);
                    //print(tem_val <= 0 ? '0' : tem_val.toStringAsFixed(2));
                    cashAccount.text =
                        tem_val <= 0 ? '0' : tem_val.toStringAsFixed(2);

                    // ignore: non_constant_identifier_names
                    double tem_val1 = tradeUsed + adder;
                    //print(tem_val1);
                    //print(tem_val1 <= 0 ? '0' : tem_val1.toStringAsFixed(2));
                    tradAccount.text =
                        tem_val1 <= 0 ? '0' : tem_val1.toStringAsFixed(2);
                  } else {
                    tradAccount.text =
                        tradeBal <= 0 ? '0' : tradeBal.toStringAsFixed(2);
                    cashAccount.text =
                        adder <= 0 ? '0' : adder.toStringAsFixed(2);
                  }

                  // }

                } else if (total > tradeUsed &&
                    tradeUsed != 0 &&
                    tradeBal > tradeUsed) {
                  print("-in`12`12``heloo");
                  //print(tradeUsed);
                  //print(total);

                  if (tradeUsed < total && total > tradeBal) {
                    print("hellodasdas");
                    double tempr = total - tradeBal;
                    tradAccount.text = tradeBal.toStringAsFixed(2);
                    cashAccount.text = tempr.toStringAsFixed(2);
                  } else {
                    tradAccount.text = total.toStringAsFixed(2);
                  }
                } else if (total > tradeUsed && tradeUsed != 0) {
                  print("-hgfghdf");
                  double tempr = total - tradeBal;
                  tradAccount.text = tradeBal.toStringAsFixed(2);
                  cashAccount.text = tempr.toStringAsFixed(2);
                } else if (total > tradeUsed && tradeUsed != 0) {
                  print("-indeqweqw93");
                  //print(tradeUsed);
                  //print(total);
                  //print(tradeBal);
                  adder = total - tradeBal;
                  tradAccount.text = tradeBal.toStringAsFixed(2);
                  cashAccount.text = adder.toStringAsFixed(2);
                } else if (cashUsed != 0) {
                  print("-in`12`12``");
                  if (tradeUsed > total) {
                    print('ksadbfjhb');
                    print(tradeUsed);
                    print(total);
                    print(cashUsed);
                    adder = total - cashUsed + tradeUsed;
                    tradAccount.text = adder.toStringAsFixed(2);
                  } else {
                    print(tradeUsed);
                    print(total);
                    print(cashUsed);
                    adder = total - cashUsed;
                    tradAccount.text = adder.toStringAsFixed(2);
                  }

                  // if (cashUsed + tradeUsed == total) {
                  //   cashUsed = 0;
                  //   tradeUsed = 0;
                  // }
                  // else
                  // {}
                } else {
                  print("-ind123w93");
                  print(total);

                  if (tradeBal < total) {
                    //print("hello");

                    double temp2 = total - tradeBal;
                    cashAccount.text = temp2.toStringAsFixed(2);
                    double temp = total - temp2;
                    // temp = temp.toStringAsFixed(2);
                    //print(temp);
                    tradAccount.text = temp.toStringAsFixed(2);
                  } else {
                    tradAccount.text = total.toStringAsFixed(2);
                  }
                }

                // }
              } else {
                cashAccount.text = total.toStringAsFixed(2);
              }
            } else {
              //print("-indetified1238765");
              // cashAccount.text = cashBal.toStringAsFixed(2);
              // adder = total - cashBal;
              // tradAccount.text = adder.toStringAsFixed(2);
              tradAccount.text = tradeBal.toStringAsFixed(2);
              adder = total - tradeBal;
              cashAccount.text = adder.toStringAsFixed(2);
            }
          } else if (cashSelected) {
            if (cashBal >= total) {
              //print("-indetified234");
              cashAccount.text = total.toString();
            } else {
              // tradeSelected = true;
              // cashAccount.text = cashBal.toString();
              if (tradeSelected == false) {
                cashAccount.text = total.toStringAsFixed(2);
                autofill(value: total.toString(), isCash: true);
              } else {
                cashAccount.text = cashBal.toStringAsFixed(2);
              }
              // adder = total - cashBal;
              // tradAccount.text = adder.toString();
            }
          } else if (tradeSelected) {
            if (tradeBal >= total) {
              tradAccount.text = total.toStringAsFixed(2);
            } else {
              //print("-indetified4432");
              // cashSelected = true;
              if (cashSelected == false) {
                tradAccount.text = total.toStringAsFixed(2);
                autofill(value: total.toString(), isCash: false);
              } else {
                tradAccount.text = tradeBal.toStringAsFixed(2);
              }
              // tradAccount.text = tradeBal.toString();
              // adder = total - tradeBal;
              // cashAccount.text = adder.toString();
            }
          }
        } else {
          setState(() {
            cashError = null;
          });
          setState(() {
            tradError = null;
          });
          double amount = 0;
          if (value.length > 0) {
            amount = double.parse(value);
          }
          //print('amount : $amount');
          //print('cash used : $cashUsed');
          //print('trade used: $tradeUsed');
          //print('total : $total');
          // if (cashUsed + tradeUsed <= total) {
          //print('check');
          if (isCash == true) {
            adder = total - amount;
            if (adder < 0) {
            } else {
              //print("21");
              if (cashSelected && tradeSelected) {
                tradAccount.text = adder.toStringAsFixed(2);
              }
            }
            if (cashBal < amount) {
              print('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!');
              print(cashBal);
              print(amount);

              String error = await Utils.getMessage(pageDetails['text13']);
              setState(() {
                cashError = error;
              });
            }
            if (tradeBal < total - amount) {
              print('kjhkjlkj');
              print(total);
              print(amount);

              String error = await Utils.getMessage(pageDetails['text13']);
              setState(() {
                tradError = error;
              });
            }
          } else {
            adder = total - amount;
            if (adder < 0) {
            } else {
              //print("1");
              if (cashSelected && tradeSelected) {
                cashAccount.text = adder.toStringAsFixed(2);
              }
            }
            if (tradeBal < amount) {
              String error = await Utils.getMessage(pageDetails['text13']);
              setState(() {
                tradError = error;
              });
            }
            if (cashBal < total - amount) {
              print('::::::::::::::::');

              String error = await Utils.getMessage(pageDetails['text13']);
              setState(() {
                cashError = error;
              });
            }
          }
          // }
        }
      }
      // ignore: unused_catch_stack
    } catch (e, trace) {
      //print('here');

      //print(e);
      //print(trace);
    }
  }

  autofill1({String value, bool isCash}) async {
    try {
      double cashUsed = 0;

      if (cashAccount.text.length > 0) {
        cashUsed = double.parse(cashAccount.text);
      }

      if (cashBal >= cashUsed) {
      } else {
        String error = await Utils.getMessage(pageDetails['text13']);
        setState(() {
          cashError = error;
        });

        // }

      }
      // ignore: unused_catch_stack
    } catch (e, trace) {
      //print('here');

      //print(e);
      //print(trace);
    }
  }

  Future<String> getPackages() async {
    Map<String, dynamic> result =
        await HttpRequest.Post('addFundingInfo', Utils.getSiteID());
    if (Utils.isServerError(result)) {
      return throw (await Utils.getMessage(result['response']['error']));
    } else {
      Map response = result['response']['data'];
      print('UUUUUUUUUUUUUUUUUUUUUUUUUUU');
      print(response);
      packages = response["system_data"]["packages"];
      tempList =
          // dashboard.accountType == 1?
          packages.sublist(0, 4);
      // : packages.sublist(0, 8);
      for (var i = 0; i < packages.length; i++) {
        packages[i]['qty'] = 0;
      }

      cashBal_view = double.parse(response["user_data"]["cash_balance"]);
      cashBal = double.parse(response["user_data"]["cash_balance"]);
      tradeBal_view = double.parse(response["user_data"]["trading_balance"]);
      tradeBal = double.parse(response["user_data"]["trading_balance"]);

      // tradeBal_view = 502.25;
      // tradeBal = 502.25;
      // cashBal_view = 123.37;
      // cashBal = 123.37;

      //     double.parse(response["user_data"]["cash_balance"]);
      // //  cashBal = double.parse(cashBal_view.toStringAsFixed(0));
      // cashBal = double.parse(response["user_data"]["cash_balance"]);
      // //print("Main_print");
      // //print(cashBal);
      // tradeBal_view = double.parse(response["user_data"]["trading_balance"]);
      // tradeBal_view = 508.546587.truncateToDouble();
      // tradeBal = double.parse(response["user_data"]["trading_balance"]);
      // //  tradeBal = double.parse(tradeBal_view.toStringAsFixed(2));
      // // tradeBal = double.parse(response["user_data"]["trading_balance"]);
      //print("Main_print2");
      //print(tradeBal_view);

      isKyc = response["user_data"]["kyc_status"].toString();
      showAdminFee = response["user_data"]["made_purchase"].toString();
      userActiveStatus = response["user_data"]["user_active_status"];
      print('AAAAAAA');
      print(showAdminFee);

      return Utils.toJSONString(
          await Utils.convertMessage(result['response']['data']));
    }
  }

  sub(Map package) {
    for (var i = 0; i < cart.length; i++) {
      if (cart[i]["package_id"] == package["package_id"]) {
        if (cart[i]["qty"] <= 0) {
          cart.remove(package);
          for (var i = 0; i < packages.length; i++) {
            if (packages[i]["package_id"] == package["package_id"]) {
              if (packages[i]["qty"] >= 1) {
                packages[i]["qty"] -= 1;
                total -= package["price"];
              }
            }
          }
        } else {
          cart[i]["qty"] -= 1;
          for (var i = 0; i < packages.length; i++) {
            if (packages[i]["package_id"] == package["package_id"]) {
              if (packages[i]["qty"] >= 1) {
                packages[i]["qty"] -= 1;
                total -= package["price"];
              }
            }
          }
        }
      }
    }
    autofill(value: null);
  }

  add(int id, int price, context) {
    bool exists = false;
    int rt = total + price;
    //print("-12343211");
    //print(packages);
    if (cashBal + tradeBal >= rt) {
      for (var i = 0; i < packages.length; i++) {
        if (packages[i]["package_id"].toString() == id.toString()) {
          packages[i]["qty"] += 1;
        }
      }

      for (var i = 0; i < cart.length; i++) {
        if (cart[i]["package_id"].toString() == id.toString()) {
          cart[i]["qty"] += 1;
          total += price;
          exists = true;
        }
      }

      if (exists == false) {
        Map _temp = {};
        _temp['qty'] = 1;
        _temp['package_id'] = id;
        cart.add(_temp);
        total += price;
      }
      //print(
      // '*******************************************************************ASDFASDFASDFASDFAS*******************************************************************************');
      //print(cart[0]);
      autofill(value: null);
    } else {
      // showAlertDialog(context);
      insufficientfund();
    }
  }

  insufficientfund() {
    return showDialog(
        barrierDismissible: false,
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            backgroundColor: Pallet.popupcontainerback,
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(Pallet.radius)),
            title: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Image.asset("c-logo.png", width: 30),
                SizedBox(width: 10),
                MyText(
                    text: pageDetails["text13"],
                    style: TextStyle(
                        fontSize: Pallet.normalfont,
                        color: Pallet.fontcolor,
                        fontWeight: Pallet.font500)),
              ],
            ),
            content: MyText(
                text: pageDetails["text14"],
                style: TextStyle(
                  fontSize: Pallet.normalfont,
                  color: Pallet.fontcolor,
                  // fontWeight: Pallet.font500
                )),
            actions: [
              PopupButton(
                onpress: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => Home(route: 'add_funding')));
                },
                vpadding: 10,
                text: pageDetails['text12'],
                buttoncolor: Pallet.fontcolor,
                textcolor: Pallet.fontcolornew,
              ),
              PopupButton(
                onpress: () {
                  Navigator.of(context).pop();
                },
                vpadding: 10,
                text: pageDetails['text15'],
                buttoncolor: Pallet.fontcolor,
                textcolor: Pallet.fontcolornew,
              ),
            ],
          );
        });
  }
//   showAlertDialog(BuildContext context) {
// // set up the button
//     Widget okButton = FlatButton(
//       child: MyText(text:"Add Fund"),
//       onPressed: () {
//         Navigator.push(
//             context,
//             MaterialPageRoute(
//                 builder: (context) => Home(route: 'add_funding')));
//       },
//     );
//     Widget cancl = FlatButton(
//       child: MyText(text:"Cancel"),
//       onPressed: () {
//         Navigator.of(context).pop();
//       },
//     );
// // set up the AlertDialog
//     AlertDialog alert = AlertDialog(
//       title: MyText(text:"Insufficient Funds"),
//       content: MyText(text:"Please Add Funds..."),
//       actions: [okButton, cancl],
//     );

// // show the dialog
//     showDialog(
//       context: context,
//       builder: (BuildContext context) {
//         return alert;
//       },
//     );
//   }

  buyPackages(context) async {
    //print('cashSelected :' + cashSelected.toString());
    //print('tradeSelected :' + tradeSelected.toString());
    //print("cart : " + cart.toString());
    List tempCart = [];

    // ignore: unused_local_variable
    String splitType;

    // Find the Scaffold in the widget tree and use it to show a SnackBar.

    //print("-indetified1");
    bool isValid = true;
    //print("cashAccount.text : " + cashAccount.text);
    //print("tradAccount.text : " + tradAccount.text);

    if (cashAccount.text.length < 1) {
      cashUsed = 0;
      //print("-indetified2");
    } else {
      cashUsed = double.parse(cashAccount.text);
    }

    if (tradAccount.text.length < 1) {
      tradeUsed = 0;
    } else {
      tradeUsed = double.parse(tradAccount.text);
    }

    if (cart.isEmpty) {
      isValid = false;
      snack.snack(title: pageDetails['text31']);
      // final snackBar = SnackBar(content: MyText(text:pageDetails['text31']));
      // ScaffoldMessenger.of(context).showSnackBar(snackBar);
    } else {
      //print(total);
      //print(cashUsed);
      //print(tradeUsed);
      if (cashSelected == true && tradeSelected == true) {
        splitType = 'both';
      } else if (cashSelected == true && tradeSelected == false) {
        splitType = 'cash';
      } else if (cashSelected == false && tradeSelected == true) {
        splitType = 'trading';
      }

      if (cashSelected == false && tradeSelected == false) {
        snack.snack(title: pageDetails['text32']);
        // final snackBar = SnackBar(content: MyText(text:pageDetails['text32']));
        // ScaffoldMessenger.of(context).showSnackBar(snackBar);
      } else if (total != cashUsed + tradeUsed) {
        if (iscashonly == true) {
          if (cashSelected == true && tradeSelected == false) {
            // cashUsed = tradeUsed;
            tradeUsed = 0;
          }
        } else {
          print("ddddddddddddddddddddddddd");
          snack.snack(
              title: pageDetails['text33'] +
                  ' ' +
                  total.toString() +
                  ' ' +
                  pageDetails['text34']);
        }

        isValid = false;
      } else {
        if (cashSelected && tradeSelected) {
        } else {
          if (cashSelected) {
            if (cashUsed == 0) {
              isValid = false;
              cashError = pageDetails['text35'];
            } else {
              cashError = null;
            }
          }
          if (tradeSelected) {
            if (tradeUsed == 0) {
              isValid = false;
              tradError = pageDetails['text35'];
            } else {
              tradError = null;
            }
          }
        }
      }

      if (cashSelected && cashBal < cashUsed) {
        //print("object-lo1");
        //print(cashBal);
        isValid = false;
        snack.snack(title: pageDetails['text13']);

        // final snackBar = SnackBar(content: MyText(text:pageDetails['text13']));
        // ScaffoldMessenger.of(context).showSnackBar(snackBar);
        if (cashBal == 0) {
          snack.snack(title: pageDetails['text36']);

          // final snackBar = SnackBar(content: MyText(text:pageDetails['text36']));
          // ScaffoldMessenger.of(context).showSnackBar(snackBar);
        }
      } else if (tradeSelected && tradeBal < tradeUsed) {
        //print("object-lo");
        //print(tradeBal);
        isValid = false;
        snack.snack(title: pageDetails['text13']);

        // final snackBar = SnackBar(content: MyText(text:pageDetails['text13']));
        // ScaffoldMessenger.of(context).showSnackBar(snackBar);
        // String error = await Utils.getMessage('Insufficient Funds');
        if (tradeBal == 0) {
          snack.snack(title: pageDetails['text37']);

          // final snackBar = SnackBar(content: MyText(text:pageDetails['text37']));
          // ScaffoldMessenger.of(context).showSnackBar(snackBar);
        }
      }
      // else if()
      // {

      // }
      else {
        isValid = true;
      }

      if (isValid) {
        cashError = null;
        tradError = null;
        if (iscashonly == true) {
          if (cashSelected == true && tradeSelected == false) {
            // cashUsed = tradeUsed;
            tradeUsed = 0;
          }
        }
        if (cashUsed + tradeUsed >= total) {
          for (var temp in cart) {
            if (temp['qty'] == 0) {
              var ram = {"package_id": temp["package_id"], "qty": temp["qty"]};
              tempCart.remove(ram);
            } else {
              var ram = {"package_id": temp["package_id"], "qty": temp["qty"]};
              tempCart.add(ram);
            }
          }
          print('AAAAAAAAAAAAAA');
          print(cashUsed);
          print(tradeUsed);
          print('TRADESELECTED: $tradeSelected}');
          print('TRADESELECTED: $cashSelected}');

          print(dashboard.accountType);
          if (dashboard.accountType == 2 &&
              isKyc != 'verified' &&
              ((cashUsed + tradeUsed) > 1000 ||
                  tradeUsed > 1000 ||
                  cashUsed > 1000)) {
            print('BBBBBBBBBBB');
            print(cashUsed);
            print(tradeUsed);

            showDialog(
                barrierDismissible: false,
                context: context,
                builder: (BuildContext context) {
                  return KycPopUp();
                });
          } else if (dashboard.accountType == 1 && cashUsed > 1000) {
            snack.snack(title: pageDetails['text64']);
          } else if (total != cashUsed + tradeUsed) {
            print("eeeeeeeeeeeeeeeeeeeeeeeeeeee");

            print('TOTAL: $total}');
            print('CASHUSED + TRADEUSED: ${cashUsed + tradeUsed}');
            snack.snack(
                title: pageDetails['text33'] +
                    ' ' +
                    total.toString() +
                    ' ' +
                    pageDetails['text34']);

            isValid = false;
          } else {
            var map = {
              "payload": {
                "product_id": 1,
                "split_type": 'none',
                "is_dsv_p": false,
                "total": 0,
                "cash_account_used": cashUsed,
                "trading_account_used": tradeUsed,
                "packages": tempCart,
              }
            };
            waiting.waitpopup(context);
            print('MAP: $map}');

            Map<String, dynamic> result =
                await HttpRequest.Post('buyPackages', map);
            if (Utils.isServerError(result)) {
              Navigator.pop(context);
              // snack.snack(title: pageDetails['text38']);
              print('I=====================I');
              print(result['response']['error']);
              if (result['response']['error'] == 'kyc_not_verified') {
                snack.snack(title: 'KYC Not Verified');
              } else if (result['response']['error'] == 'only_cash_allowed') {
                snack.snack(title: 'Only allow Cash');
              } else if (result['response']['error'] == 'negative_amount') {
                snack.snack(
                    title:
                        "Blocked because of Negative balance in any of your account");
              } else {
                snack.snack(title: pageDetails['text38']);
              }

              // return throw (await Utils.getMessage(result['response']['error']));

              // final snackBar = SnackBar(content: MyText(text:pageDetails['text38']));
              // ScaffoldMessenger.of(context).showSnackBar(snackBar);
              return await Utils.getMessage(result['response']['error']);
            } else {
              print('NKNNNJNJNJNJKL');

              Navigator.pop(context);
              setState(() {
                cart = [];
              });
              double temp = cashUsed + tradeUsed;
              print('===================');

              print(temp);
              print(dashboard.mypriceofcoin['dsv_balance']);

              double temp1 = dashboard.mypriceofcoin['dsv_balance'] == null
                  ? dashboard.mypriceofcoin['dsv_balance'] + temp
                  : dashboard.mypriceofcoin['dsv_balance'] + temp;
              print('nnnnnnnnnnnnnnnnnnnnn');

              print(temp1);
              total = 0;
              cashAccount.text = '0';
              tradAccount.text = '0';
              print(temp);
              print(temp1);

              // showDialog(
              //     barrierDismissible: false,
              //     context: context,
              //     builder: (_) => SucessPopup(
              //         message: 'Get Dsv Successful', route: 'buy_dsv'));
              showDialog(
                  barrierDismissible: false,
                  context: context,
                  builder: (BuildContext context) {
                    return AlertDialog(
                      backgroundColor: Pallet.popupcontainerback,
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(Pallet.radius)),
                      title: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Image.asset("c-logo.png", width: 30),
                          SizedBox(width: 10),
                          Expanded(
                            child: MyText(
                                text: pageDetails['text39'],
                                style: TextStyle(
                                    fontSize: Pallet.normalfont + 10,
                                    color: Pallet.fontcolor,
                                    fontWeight: Pallet.font500)),
                          ),
                        ],
                      ),
                      content: Container(
                        height: 150,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: [
                            CircleAvatar(
                              backgroundColor: Colors.white,
                              radius: 30,
                              child: Icon(Icons.done,
                                  color: Pallet.dashcontainerback, size: 40),
                            ),
                            MyText(
                                text: pageDetails['text40'] +
                                    ' ' +
                                    temp.toString(),
                                style: TextStyle(
                                    fontSize: Pallet.normalfont,
                                    color: Pallet.fontcolor,
                                    fontWeight: Pallet.font500)),
                            MyText(
                                text: pageDetails['text41'] +
                                    ' ' +
                                    temp1.toString(),
                                style: TextStyle(
                                    fontSize: Pallet.normalfont,
                                    color: Pallet.fontcolor,
                                    fontWeight: Pallet.font500))
                          ],
                        ),
                      ),
                      actions: [
                        PopupButton(
                          text: pageDetails['text42'],
                          onpress: () {
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => Home(route: 'buy_dsv')),
                            );
                          },
                        ),
                        PopupButton(
                          text: pageDetails['text12'],
                          onpress: () {
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) =>
                                      Home(route: 'dashboard')),
                            );
                          },
                        ),
                      ],
                    );
                  });
            }
          }
        }
      }
    }
  }

  // ignore: unused_element
  _showMaterialDialog(context) {
    showDialog(
        barrierDismissible: false,
        context: context,
        builder: (_) => AlertDialog(
              backgroundColor: Pallet.inner1,
              title: Row(
                children: [
                  Image.asset(
                    "c-logo.png",
                    width: 40,
                  ),
                  SizedBox(
                    width: 10,
                  ),
                  MyText(
                    text: pageDetails['text43'],
                    style: TextStyle(
                      color: Pallet.fontcolor,
                      fontSize: Pallet.heading4,
                      fontWeight: Pallet.font500,
                    ),
                  ),
                ],
              ),
              content: Container(
                // padding: EdgeInsets.all(2),
                width: 455,
                //color: Pallet.dashcontainerback,
                child: SingleChildScrollView(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      SizedBox(
                        height: 20,
                      ),
                      MyText(
                        text: pageDetails['text44'],
                        style: TextStyle(
                          color: Pallet.fontcolor,
                          fontSize: Pallet.heading4,
                          fontWeight: Pallet.font500,
                        ),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      MyText(
                        text: pageDetails['text45'] + ' ',
                        style: TextStyle(
                          fontStyle: FontStyle.italic,
                          color: Pallet.fontcolor,
                          fontSize: Pallet.heading4,
                          fontWeight: Pallet.font500,
                        ),
                      ),
                      SizedBox(
                        height: 5,
                      ),
                      MyText(
                        text: pageDetails['text46'],
                        style: TextStyle(
                          color: Pallet.fontcolor,
                          fontSize: Pallet.heading6,
                          // fontWeight: Pallet.font600,
                        ),
                      ),
                      SizedBox(
                        height: 5,
                      ),
                      MyText(
                        text: pageDetails['text47'],
                        style: TextStyle(
                          color: Pallet.fontcolor,
                          fontSize: Pallet.heading6,
                          // fontWeight: Pallet.font600,
                        ),
                      ),
                      SizedBox(
                        height: 5,
                      ),
                      MyText(
                        text: pageDetails['text48'],
                        style: TextStyle(
                          color: Pallet.fontcolor,
                          fontSize: Pallet.heading6,
                          // fontWeight: Pallet.font600,
                        ),
                      ),
                      SizedBox(
                        height: 5,
                      ),
                      MyText(
                        text: pageDetails['text49'],
                        style: TextStyle(
                          color: Pallet.fontcolor,
                          fontSize: Pallet.heading6,
                          // fontWeight: Pallet.font600,
                        ),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Divider(
                        height: 1,
                        color: Colors.white,
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      MyText(
                        text: pageDetails['text50'],
                        style: TextStyle(
                          color: Pallet.fontcolor,
                          fontSize: Pallet.heading4,
                          fontWeight: Pallet.font500,
                        ),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      MyText(
                        text: pageDetails['text45'] + ' ',
                        style: TextStyle(
                          fontStyle: FontStyle.italic,
                          color: Pallet.fontcolor,
                          fontSize: Pallet.heading4,
                          fontWeight: Pallet.font500,
                        ),
                      ),
                      SizedBox(
                        height: 5,
                      ),
                      MyText(
                        text: pageDetails['text51'],
                        style: TextStyle(
                          color: Pallet.fontcolor,
                          fontSize: Pallet.heading6,
                          // fontWeight: Pallet.font600,
                        ),
                      ),
                      SizedBox(
                        height: 5,
                      ),
                      MyText(
                        text: pageDetails['text52'],
                        style: TextStyle(
                          color: Pallet.fontcolor,
                          fontSize: Pallet.heading6,
                          // fontWeight: Pallet.font600,
                        ),
                      ),
                      SizedBox(
                        height: 5,
                      ),
                      MyText(
                        text: pageDetails['text53'],
                        style: TextStyle(
                          color: Pallet.fontcolor,
                          fontSize: Pallet.heading6,
                          // fontWeight: Pallet.font600,
                        ),
                      ),
                      SizedBox(
                        height: 5,
                      ),
                      MyText(
                        text: pageDetails['text54'],
                        style: TextStyle(
                          color: Pallet.fontcolor,
                          fontSize: Pallet.heading6,
                          // fontWeight: Pallet.font600,
                        ),
                      ),
                      SizedBox(
                        height: 5,
                      ),
                      MyText(
                        text: pageDetails['text55'],
                        style: TextStyle(
                          color: Pallet.fontcolor,
                          fontSize: Pallet.heading6,
                          // fontWeight: Pallet.font600,
                        ),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Divider(
                        height: 1,
                        color: Colors.white,
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      MyText(
                        text: pageDetails['text56'],
                        style: TextStyle(
                          color: Pallet.fontcolor,
                          fontSize: Pallet.heading4,
                          fontWeight: Pallet.font500,
                        ),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      MyText(
                        text: pageDetails['text45'] + ' ',
                        style: TextStyle(
                          fontStyle: FontStyle.italic,
                          color: Pallet.fontcolor,
                          fontSize: Pallet.heading4,
                          fontWeight: Pallet.font500,
                        ),
                      ),
                      SizedBox(
                        height: 5,
                      ),
                      MyText(
                        text: pageDetails['text57'],
                        style: TextStyle(
                          color: Pallet.fontcolor,
                          fontSize: Pallet.heading6,
                          // fontWeight: Pallet.font600,
                        ),
                      ),
                      SizedBox(
                        height: 5,
                      ),
                      MyText(
                        text: pageDetails['text58'],
                        style: TextStyle(
                          color: Pallet.fontcolor,
                          fontSize: Pallet.heading6,
                          // fontWeight: Pallet.font600,
                        ),
                      ),
                      SizedBox(
                        height: 5,
                      ),
                      MyText(
                        text: pageDetails['text59'],
                        style: TextStyle(
                          color: Pallet.fontcolor,
                          fontSize: Pallet.heading6,
                          // fontWeight: Pallet.font600,
                        ),
                      ),
                      SizedBox(
                        height: 5,
                      ),
                      MyText(
                        text: pageDetails['text60'],
                        style: TextStyle(
                          color: Pallet.fontcolor,
                          fontSize: Pallet.heading6,
                          // fontWeight: Pallet.font600,
                        ),
                      ),
                      SizedBox(
                        height: 5,
                      ),
                      MyText(
                        text: pageDetails['text61'],
                        style: TextStyle(
                          color: Pallet.fontcolor,
                          fontSize: Pallet.heading6,
                          // fontWeight: Pallet.font600,
                        ),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Divider(
                        height: 1,
                        color: Colors.white,
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      MyText(
                        text: pageDetails['text62'],
                        style: TextStyle(
                          color: Pallet.fontcolor,
                          fontSize: Pallet.heading6,
                          // fontWeight: Pallet.font600,
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              actions: <Widget>[
                PopupButton(
                  text: pageDetails['text63'],
                  onpress: () {
                    Navigator.of(context).pop();
                  },
                ),
              ],
            ));
  }
}

// John_Wick
// Abcd@123

//john_wick
//Abcd@123
