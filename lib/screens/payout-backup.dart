// import 'package:centurion/screens/myshop.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import '../config.dart';
import '../services/business/cart.dart';
// import 'package:auto_size_text/auto_size_text.dart';
import '../common.dart';
import '../config/app_settings.dart';
import '../utils/utils.dart';
import '../services/communication/index.dart' show HttpRequest;

class Payout extends StatefulWidget {
  const Payout({Key key, this.wdgtWidth, this.wdgtHeight}) : super(key: key);
  final double wdgtWidth, wdgtHeight;
  @override
  _PayoutState createState() => _PayoutState();
}

class _PayoutState extends State<Payout> {
  bool isKyc;
  Map<String, dynamic> pageDetails = {};
  _PayoutState() {
    setPageDetails();
  }

  void setPageDetails() async {
    String data = await Utils.getPageDetails('payout');
    setState(() {
      pageDetails = Utils.fromJSONString(data);
    });
  }

  int selectedoption = 1;
  List<Map> payoutAccountDeatails = [
    {
      'id': 1,
      'title': 'Ducatus Coins',
      'payoutsprice': '0.00',
      'symbol': 'DUC ',
      'asset_type': 'duc',
      'icon': appSettings['SERVER_URL'] + '/' + 'assets/duc.png',
    },
    {
      'id': 2,
      'title': 'Cash / E-Wallet',
      'symbol': '\$ ',
      'payoutsprice': '0.00',
      'asset_type': 'cash',
      'icon': appSettings['SERVER_URL'] + '/' + 'assets/cash.png',
    },
  ];
  bool cashval = false;
  bool tradeval = false;
  IconData icon = Icons.arrow_drop_down;
  Fund cart = Fund();

  Future<String> temp;
  FocusNode duc = FocusNode();
  FocusNode adr1 = FocusNode();
  FocusNode acc = FocusNode();
  FocusNode adr2 = FocusNode();
  FocusNode btn = FocusNode();
  FocusNode btn1 = FocusNode();
  Map<String, dynamic> payoutSystemDetails = {};
  Map<String, dynamic> payoutUserDetails = {};
  double totalDucAmount = 0, totalCashAmount = 0;
  TextEditingController ducAmount = TextEditingController();
  String ducError;
  String addressError;
  String udscError;
  TextEditingController udscaddress = TextEditingController();
  TextEditingController cashAmount = TextEditingController();
  String cashError;

  TextEditingController address = TextEditingController();

  void initState() {
    temp = getDetails();

    super.initState();
  }

  // output(value) {
  //   //print(value);
  //   setState(() {
  //     payoutAccountDeatails[0]['payoutsprice'] =
  //         payoutss.payoutUserDetails['total_duc'].toString();
  //     payoutAccountDeatails[1]['payoutsprice'] =
  //         payoutss.payoutUserDetails['cash_account_bal'].toString();
  //     //print(payoutAccountDeatails);
  //   });
  // }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: temp,
      builder: (context, snapshot) {
        if (snapshot.hasError) {
          if (snapshot.error == 'Something Went Wrong') {
            return SomethingWentWrongMessage();
          } else {
            return Error(
              message: snapshot.error != null ? snapshot.error : '',
            );
          }
        } else if (snapshot.hasData) {
          return LayoutBuilder(
            // ignore: missing_return
            builder: (context, constraints) {
              if (constraints.maxWidth <= 730) {
                return SingleChildScrollView(
                    // child: payout(wdgtwidth: widget.wdgtWidth),
                    child: Padding(
                  padding: EdgeInsets.only(top: Pallet.topPadding1),
                  child: Column(
                    children: [
                      payoutHeader(wdgtwidth: widget.wdgtWidth),
                      payoutOptions(wdgtwidth: widget.wdgtWidth),
                      payoutForm(wdgtwidth: widget.wdgtWidth),
                    ],
                  ),
                ));
              }
              // else if (constraints.maxWidth > 730 &&
              //     constraints.maxWidth < ScreenSize.ipad) {
              //   // return SingleChildScrollView(
              //   //     child: payout(wdgtwidth: widget.wdgtWidth));
              //   return Padding(
              //     padding: EdgeInsets.all(Pallet.leftPadding),
              //     child: Column(
              //       // mainAxisAlignment: MainAxisAlignment.spaceAround,
              //       children: [
              //         Row(
              //           children: [
              //             payoutHeader(wdgtwidth: widget.wdgtWidth * .85),
              //           ],
              //         ),
              //         SizedBox(
              //           height: 40,
              //         ),
              //         Row(
              //           crossAxisAlignment: CrossAxisAlignment.start,
              //           // mainAxisAlignment: MainAxisAlignment.spaceBetween,
              //           children: [
              //             Column(
              //               children: [
              //                 payoutForm(wdgtwidth: widget.wdgtWidth * .45),
              //               ],
              //             ),
              //             // SizedBox(
              //             //   width: widget.wdgtWidth * .05,
              //             // ),
              //             Column(
              //               children: [
              //                 payoutOptions(wdgtwidth: widget.wdgtWidth * .45),
              //               ],
              //             )
              //           ],
              //         )
              //       ],
              //     ),
              //   );
              // }
              else {
                return Padding(
                  padding: EdgeInsets.only(
                      left: Pallet.leftPadding, top: Pallet.topPadding1),
                  child: Container(
                    width: widget.wdgtWidth * .85,
                    child: SingleChildScrollView(
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        // mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: [
                          payoutHeader(wdgtwidth: widget.wdgtWidth * .85),
                          SizedBox(
                            height: 40,
                          ),
                          Container(
                            // width: widget.wdgtWidth * .85,
                            child: Row(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Column(
                                  children: [
                                    payoutForm(
                                        wdgtwidth: widget.wdgtWidth * .45),
                                  ],
                                ),
                                // SizedBox(
                                //   width: widget.wdgtWidth * .05,
                                // ),
                                Column(
                                  children: [
                                    payoutOptions(
                                        wdgtwidth: widget.wdgtWidth * .4),
                                  ],
                                )
                              ],
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                );
              }
            },
          );
        }
        return Loader();
      },
    );
  }

  Widget payoutHeader({double wdgtwidth}) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisSize: MainAxisSize.min,
      children: [
        MyText(text:pageDetails["text1"],
            style: TextStyle(
              color: Pallet.fontcolornew,
              fontSize: Pallet.heading1,
            )),
        Padding(
          padding: EdgeInsets.symmetric(
              // horizontal: Pallet.defaultPadding,
              vertical: Pallet.defaultPadding * 2),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Container(
                  padding: EdgeInsets.all(Pallet.defaultPadding / 2),
                  width: wdgtwidth,
                  decoration: BoxDecoration(
                      color: Pallet.fontcolor,
                      boxShadow: [Pallet.shadowEffect],
                      borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(Pallet.radius),
                          bottomRight: Radius.circular(Pallet.radius))),
                  child: Center(
                    child: Row(
                      // mainAxisSize: MainAxisSize.min,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      // mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: [
                        MyText(text:
                          pageDetails['text15'],
                          style: TextStyle(
                              color: Pallet.dashcontainerback,
                              fontSize: Pallet.normalfont,
                              fontWeight: FontWeight.bold),
                        ),
                        Container(
                          padding: EdgeInsets.symmetric(
                              horizontal: Pallet.defaultPadding / 5),
                          width: wdgtwidth - 90,
                          child: MyText(text:
                            pageDetails['text16'],
                            style: TextStyle(
                                color: Pallet.dashcontainerback,
                                fontSize: Pallet.notefont,
                                height: 1.7,
                                fontWeight: FontWeight.w400),
                            textAlign: TextAlign.justify,
                          ),
                        )
                      ],
                    ),
                  )),
            ],
          ),
        ),
      ],
    );
  }

  Widget payoutOptions({double wdgtwidth}) {
    return Container(
      width: wdgtwidth,
      child: Column(
        // crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          MyText(text:pageDetails["text2"],
              style: TextStyle(
                  color: Pallet.dashcontainerback,
                  fontSize: Pallet.subheading1 - 3,
                  fontWeight: Pallet.subheading1wgt)),
          SizedBox(height: 20),
          Column(
            crossAxisAlignment: CrossAxisAlignment.center,

            // mainAxisAlignment: MainAxisAlignment.spaceBetween,

            // spacing: 30,
            // runSpacing: 30,
            children: [
              for (var i = 0; i < payoutAccountDeatails.length; i++)
                Padding(
                  padding:
                      EdgeInsets.symmetric(vertical: Pallet.leftPadding / 2),
                  child: Option(
                    wdgtWidth: wdgtwidth,
                    icon: payoutAccountDeatails[i]['icon'],
                    title: payoutAccountDeatails[i]['title'],
                    subtitle: payoutAccountDeatails[i]["id"] == 2
                        ? payoutAccountDeatails[i]['symbol'].toString() +
                            payoutAccountDeatails[i]['payoutsprice'].toString()
                        : payoutAccountDeatails[i]['payoutsprice'].toString() +
                            " " +
                            payoutAccountDeatails[i]['symbol'].toString(),
                    selected: selectedoption == payoutAccountDeatails[i]["id"],
                    onselect: () {
                      setState(() {
                        address.text = '';
                        selectedoption = payoutAccountDeatails[i]["id"];
                        // payoutss.ducAmount.text = '';
                      });
                    },
                  ),
                ),
              SizedBox(height: 20),
            ],
          ),
        ],
      ),
    );
  }

  Widget payoutForm({double wdgtwidth}) {
    if (selectedoption == 1) {
      udscError = null;
      cashAmount.text = "";
      cashError = null;
      totalCashAmount = 0;
      udscaddress.text = "";
    } else if (selectedoption == 2) {
      ducAmount.text = "";
      ducError = null;
      addressError = null;
      totalDucAmount = 0;
      address.text = "";
    }
    return Container(
      width: wdgtwidth,
      child: Column(children: [
        selectedoption == 1
            ? Column(
                children: [
                  Container(
                    // padding: EdgeInsets.only(top: Pallet.defaultPadding),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        MyText(text:
                            pageDetails["text3"] +
                                ' ' +
                                payoutSystemDetails['duc_used'].toString() +
                                " / " +
                                payoutSystemDetails['duc_max'].toString(),
                            style: TextStyle(
                              fontSize: Pallet.subheading1 - 3,
                              fontWeight: Pallet.subheading1wgt,
                              color: Pallet.fontcolornew,
                            )),
                        SizedBox(height: 10.0),
                        Textbox(
                          focusnode: duc,
                          autofocus: true,
                          tabpress: (event) {
                            if (event.isKeyPressed(LogicalKeyboardKey.tab)) {
                              FocusScope.of(context).requestFocus(adr1);
                            }
                          },
                          controller: ducAmount,
                          errorText: ducError,
                          validation: (value) {
                            validdateDuc(setState);
                          },
                          header: pageDetails["text14"],
                          label: pageDetails["text14"],
                        ),
                        SizedBox(height: 5),
                        MyText(text:pageDetails["text17"],
                            style: TextStyle(
                              color: Pallet.fontcolornew,
                              fontSize: Pallet.heading5,
                              // fontWeight: FontWeight.w300
                            )),
                        SizedBox(height: 10),
                        MyText(text:
                            pageDetails["text5"] == null
                                ? ""
                                : pageDetails["text5"],
                            style: TextStyle(
                                color: Pallet.fontcolornew,
                                fontSize: Pallet.heading4,
                                fontWeight: FontWeight.w300)),
                        SizedBox(height: 5),
                        RawKeyboardListener(
                          focusNode: adr1,
                          onKey: (event) {
                            if (event.isKeyPressed(LogicalKeyboardKey.tab)) {
                              FocusScope.of(context).requestFocus(btn1);
                            }
                          },
                          child: TextFormField(
                            textAlign: TextAlign.left,
                            inputFormatters: [
                              FilteringTextInputFormatter.allow(
                                  RegExp("[a-zA-Z0-9]")),
                            ],
                            style: TextStyle(color: Pallet.dashcontainerback),
                            controller: address,
                            // maxLength: 34,

                            onChanged: (value) {
                              if (value.isEmpty) {
                                setState(() {
                                  addressError = null;
                                });
                              } else if (value.toString().length < 3) {
                                setState(() {
                                  addressError = pageDetails['text18'];
                                });
                              }
                              // else if (!RegExp(".*[0-9].*").hasMatch(value)) {
                              //   setState(() {
                              //     //print("123456");
                              //     addressError =
                              //         "A valid DUC address is Mandatory";
                              //   });
                              // } else if (!RegExp(".*[A-Z].*").hasMatch(value)) {
                              //   setState(() {
                              //     //print("1234567");
                              //     addressError =
                              //         "A valid DUC address is Mandatory";
                              //   });
                              // } else if (!RegExp(".*[a-z].*").hasMatch(value)) {
                              //   setState(() {
                              //     //print("12345678");
                              //     addressError =
                              //         "A valid DUC address is Mandatory";
                              //   });
                              // }
                              else {
                                setState(() {
                                  addressError = null;
                                });
                              }
                            },
                            cursorColor: Pallet.dashcontainerback,
                            keyboardType:
                                TextInputType.numberWithOptions(decimal: true),
                            onEditingComplete: () {
                              FocusScope.of(context).requestFocus(btn1);
                            },
                            decoration: InputDecoration(
                              errorText: addressError,
                              border: OutlineInputBorder(
                                borderSide: BorderSide(
                                    color: Pallet.dashcontainerback, width: 2),
                              ),
                              // labelText: label,
                              focusedBorder: OutlineInputBorder(
                                  borderSide: BorderSide(
                                      color: Pallet.dashcontainerback,
                                      width: 2),
                                  borderRadius:
                                      BorderRadius.circular(Pallet.radius)),
                              // fillColor: Pallet.dashcontainerback,
                              enabledBorder: OutlineInputBorder(
                                  borderSide: BorderSide(
                                      color: Pallet.dashcontainerback,
                                      width: 2),
                                  borderRadius:
                                      BorderRadius.circular(Pallet.radius)),
                              errorBorder: OutlineInputBorder(
                                  borderSide:
                                      BorderSide(color: Colors.red, width: 2),
                                  borderRadius:
                                      BorderRadius.circular(Pallet.radius)),
                              focusedErrorBorder: OutlineInputBorder(
                                  borderSide:
                                      BorderSide(color: Colors.red, width: 2),
                                  borderRadius:
                                      BorderRadius.circular(Pallet.radius)),
                              // labelStyle: TextStyle(
                              //   color: Pallet.fontcolor,
                              //   fontSize: Pallet.heading2,
                              // ),
                              filled: true,
                            ),
                          ),
                        ),
                        SizedBox(height: 5),
                        MyText(text:pageDetails['text19'],
                            style: TextStyle(
                              color: Pallet.fontcolornew,
                              fontSize: Pallet.heading4,
// fontWeight: FontWeight.w300
                            )),
                      ],
                    ),
                  ),
                  SizedBox(height: 20),
                  Container(
                    decoration: BoxDecoration(
                      color: Pallet.fontcolor,
                      borderRadius: BorderRadius.circular(Pallet.radius),
                      boxShadow: [Pallet.shadowEffect],
                    ), // padding: EdgeInsets.all(15.0),
                    padding: EdgeInsets.symmetric(
                        horizontal: Pallet.defaultPadding,
                        vertical: Pallet.defaultPadding + wdgtwidth * 0.04),
                    // width: wdgtwidth,
                    // height: wdgtwidth * 0.35,
                    width: wdgtwidth,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Container(
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: [
                                  CircleAvatar(
                                      // backgroundColor: Color(0XFFd2d2d2),
                                      backgroundColor: Pallet.dashcontainerback,
                                      radius: 20,
                                      child:
                                          Image.asset('payout.png', width: 20)),
                                  SizedBox(width: 10.0),
                                  MyText(text:
                                    pageDetails["text6"],
                                    style: TextStyle(
                                        fontSize: 18.0,
                                        color: Pallet.dashcontainerback),
                                  ),
                                ],
                              ),
                            ),
                            MyText(text:
                              payoutAccountDeatails[0]['payoutsprice']
                                      .toString() +
                                  ' ' +
                                  payoutAccountDeatails[0]['symbol'].toString(),
                              style: TextStyle(
                                  fontSize: 16.0,
                                  color: Pallet.dashcontainerback,
                                  fontWeight: FontWeight.w600),
                            ),
                          ],
                        ),
                        SizedBox(height: 10),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: [
                            MyText(text:
                              pageDetails["text7"],
                              style: TextStyle(
                                  fontSize: 16.0,
                                  color: Pallet.dashcontainerback,
                                  fontWeight: FontWeight.w400),
                            ),
                            MyText(text:
                              totalDucAmount.toString() + pageDetails["text8"],
                              style: TextStyle(
                                  fontSize: 18.0,
                                  color: Pallet.dashcontainerback,
                                  fontWeight: FontWeight.w600),
                            ),
                            SizedBox(height: 20),
                            Container(
                              width: double.infinity,
                              child: RawKeyboardListener(
                                focusNode: btn1,
                                onKey: (event) {
                                  if (event
                                      .isKeyPressed(LogicalKeyboardKey.enter)) {
                                    setState(() {
                                      pyout(setState);
                                    });
                                  }
                                },
                                // ignore: deprecated_member_use
                                child: Container(
                                  // width: double.infinity,
                                  child: CustomButton(
                                    onpress: () {
                                      setState(() {
                                        pyout(setState);
                                      });
                                    },
                                    vpadding: 12,
                                    text: pageDetails['text9'],
                                    buttoncolor: Pallet.fontcolornew,
                                    textcolor: Pallet.fontcolor,
                                    textsize: Pallet.heading4,
                                  ),
                                ),
                              ),
                            ),
                          ],
                        )
                      ],
                    ),
                  ),
                ],
              )
            :
            // else if (selectedoption == 2)
            Column(
                children: [
                  Container(
                    // padding: EdgeInsets.only(top: Pallet.defaultPadding),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        MyText(text:
                            pageDetails["text10"] +
                                ' ' +
                                payoutSystemDetails['cash_min'].toString() +
                                " / " +
                                payoutSystemDetails['cash_max'].toString(),
                            style: TextStyle(
                              fontSize: Pallet.subheading1 - 3,
                              fontWeight: Pallet.subheading1wgt,
                              color: Pallet.fontcolornew,
                            )),
                        SizedBox(height: 10.0),
                        Textbox(
                          focusnode: acc,
                          tabpress: (event) {
                            if (event.isKeyPressed(LogicalKeyboardKey.tab)) {
                              FocusScope.of(context).requestFocus(adr2);
                            }
                          },
                          controller: cashAmount,
                          errorText: cashError,
                          validation: (value) {
                            validdateCash(setState);
                          },
                          header: pageDetails["text4"],
                          label: pageDetails["text4"],
                        ),
                        SizedBox(height: 5),
                        MyText(text:pageDetails["text20"],
                            style: TextStyle(
                              color: Pallet.fontcolornew,
                              fontSize: Pallet.heading5,
                              // fontWeight: FontWeight.w300
                            )),
                        SizedBox(height: 10),
                        MyText(text:
                            pageDetails["text12"] == null
                                ? ""
                                : pageDetails["text12"],
                            style: TextStyle(
                                color: Pallet.fontcolornew,
                                fontSize: Pallet.heading4,
                                fontWeight: FontWeight.w300)),
                        SizedBox(height: 5),
                        RawKeyboardListener(
                          focusNode: adr2,
                          onKey: (event) {
                            if (event.isKeyPressed(LogicalKeyboardKey.tab)) {
                              FocusScope.of(context).requestFocus(adr2);
                            }
                          },
                          child: TextFormField(
                            textAlign: TextAlign.left,
                            inputFormatters: [
                              FilteringTextInputFormatter.allow(
                                  RegExp("[a-zA-Z0-9]")),
                            ],
                            style: TextStyle(color: Pallet.dashcontainerback),
                            controller: udscaddress,
                            // maxLength: 34,

                            onChanged: (value) {
                              if (value.isEmpty) {
                                setState(() {
                                  udscError = null;
                                });
                              } else if (value.toString().length < 3) {
                                setState(() {
                                  udscError = pageDetails['text21'];
                                });
                              }
                              // else if (!RegExp(".*[0-9].*").hasMatch(value)) {
                              //   setState(() {
                              //     //print("123456");
                              //     udscError =
                              //         "A valid USDC address is Mandatory";
                              //   });
                              // } else if (!RegExp(".*[A-Z].*").hasMatch(value)) {
                              //   setState(() {
                              //     //print("1234567");
                              //     udscError =
                              //         "A valid USDC address is Mandatory";
                              //   });
                              // } else if (!RegExp(".*[a-z].*").hasMatch(value)) {
                              //   setState(() {
                              //     //print("12345678");
                              //     udscError =
                              //         "A valid USDC address is Mandatory";
                              //   });
                              // }
                              else {
                                setState(() {
                                  udscError = null;
                                });
                              }
                            },
                            cursorColor: Pallet.dashcontainerback,
                            keyboardType:
                                TextInputType.numberWithOptions(decimal: true),
                            onEditingComplete: () {
                              FocusScope.of(context).requestFocus(btn1);
                            },
                            decoration: InputDecoration(
                              errorText: udscError,
                              border: OutlineInputBorder(
                                borderSide: BorderSide(
                                    color: Pallet.dashcontainerback, width: 2),
                              ),
                              // labelText: label,
                              focusedBorder: OutlineInputBorder(
                                  borderSide: BorderSide(
                                      color: Pallet.dashcontainerback,
                                      width: 2),
                                  borderRadius:
                                      BorderRadius.circular(Pallet.radius)),
                              // fillColor: Pallet.dashcontainerback,
                              enabledBorder: OutlineInputBorder(
                                  borderSide: BorderSide(
                                      color: Pallet.dashcontainerback,
                                      width: 2),
                                  borderRadius:
                                      BorderRadius.circular(Pallet.radius)),
                              errorBorder: OutlineInputBorder(
                                  borderSide:
                                      BorderSide(color: Colors.red, width: 2),
                                  borderRadius:
                                      BorderRadius.circular(Pallet.radius)),
                              focusedErrorBorder: OutlineInputBorder(
                                  borderSide:
                                      BorderSide(color: Colors.red, width: 2),
                                  borderRadius:
                                      BorderRadius.circular(Pallet.radius)),
                              // labelStyle: TextStyle(
                              //   color: Pallet.fontcolor,
                              //   fontSize: Pallet.heading2,
                              // ),
                              filled: true,
                            ),
                          ),
                        ),
                        SizedBox(height: 5),
                        MyText(text:pageDetails['text19'],
                            style: TextStyle(
                              color: Pallet.fontcolornew,
                              fontSize: Pallet.heading4,
// fontWeight: FontWeight.w300
                            )),
                      ],
                    ),
                  ),
                  SizedBox(height: 20),
                  Container(
                    // padding: EdgeInsets.all(15.0),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(Pallet.radius),
                      color: Pallet.fontcolor,
                      boxShadow: [Pallet.shadowEffect],
                    ),
                    padding: EdgeInsets.symmetric(
                        horizontal: Pallet.defaultPadding,
                        vertical: Pallet.defaultPadding + wdgtwidth * 0.04),
                    // width: wdgtwidth,
                    // height: wdgtwidth * 0.35,
                    width: wdgtwidth,
                    // padding: EdgeInsets.all(15.0),
                    // width: wdgtwidth,
                    // height: wdgtwidth,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Container(
                              child: Row(
                                children: [
                                  CircleAvatar(
                                      // backgroundColor: Color(0XFFd2d2d2),
                                      backgroundColor: Pallet.dashcontainerback,
                                      radius: 20,
                                      child: Image.asset(
                                        'cashbalance.png',
                                        width: 20,
                                      )),
                                  SizedBox(width: 10.0),
                                  MyText(text:
                                    pageDetails["text13"],
                                    style: TextStyle(
                                        fontSize: 18.0,
                                        color: Pallet.dashcontainerback),
                                  ),
                                ],
                              ),
                            ),
                            MyText(text:
                              "\$ " +
                                  payoutUserDetails['cash_account_bal']
                                      .toString(),
                              style: TextStyle(
                                  fontSize: 16.0,
                                  color: Pallet.dashcontainerback,
                                  fontWeight: FontWeight.w600),
                            ),
                          ],
                        ),
                        SizedBox(height: 10),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: [
                            MyText(text:
                              pageDetails["text7"],
                              style: TextStyle(
                                  fontSize: 16.0,
                                  color: Pallet.dashcontainerback,
                                  fontWeight: FontWeight.w400),
                            ),
                            MyText(text:
                              "\$ " + totalCashAmount.toString(),
                              style: TextStyle(
                                  fontSize: 20.0,
                                  color: Pallet.dashcontainerback,
                                  fontWeight: FontWeight.w600),
                            ),
                            SizedBox(height: 20),
                            RawKeyboardListener(
                              focusNode: btn,
                              onKey: (event) {
                                if (event
                                    .isKeyPressed(LogicalKeyboardKey.enter)) {
                                  setState(() {
                                    pyout(setState);
                                  });
                                }
                              },
                              child: Container(
                                width: double.infinity,
                                child: CustomButton(
                                  onpress: () {
                                    setState(() {
                                      pyout(setState);
                                    });
                                  },
                                  vpadding: 12,
                                  text: pageDetails['text9'],
                                  buttoncolor: Pallet.fontcolornew,
                                  textcolor: Pallet.fontcolor,
                                  textsize: Pallet.heading4,
                                ),
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                ],
              )
      ]),
    );
  }

  Future<String> getDetails() async {
    Map<String, dynamic> result =
        await HttpRequest.Post('getPayoutDetails', Utils.getSiteID());
    if (Utils.isServerError(result)) {
      return throw (await Utils.getMessage(result['response']['error']));
    } else {
      isKyc = result['response']['data']['system_data']['ask_kyc'];
      payoutSystemDetails = result['response']['data']['system_data'];
      payoutUserDetails = result['response']['data']['user_data'];
      payoutAccountDeatails[0]['payoutsprice'] = payoutUserDetails['total_duc'];
      payoutAccountDeatails[1]['payoutsprice'] =
          payoutUserDetails['cash_account_bal'];
      return Utils.toJSONString(
          await Utils.convertMessage(result['response']['data']));
    }
  }

  validdateDuc(setState) {
    if (ducAmount.text.length <= 0) {
      setState(() {
        print('IIIIIIIIIII');
        print(ducAmount.text);
        ducError = null;
      });
    } else if (payoutUserDetails['total_duc'] == 0) {
      setState(() {
        print('IIIIIIIIIII');
        print(ducAmount.text);
        ducError = pageDetails['text22'];
      });
    } else if (ducAmount.text.length > 0) {
      setState(() {
        totalDucAmount = double.parse(ducAmount.text);

        /// payoutSystemDetails['duc_price'];
      });
      if (totalDucAmount >= payoutSystemDetails['duc_min']) {
        if (totalDucAmount <= payoutUserDetails['total_duc']) {
          if (totalDucAmount <=
              payoutSystemDetails['duc_max'] -
                  payoutSystemDetails['duc_used']) {
            setState(() {
              ducError = null;
            });
          } else {
            setState(() {
              ducError = pageDetails['text23'];
            });
          }
        } else if (totalDucAmount == 0 || totalDucAmount == null) {
          setState(() {
            ducError = pageDetails['text22'];
          });
        } else {
          setState(() {
            ducError = pageDetails['text22'];
          });
        }
      } else {
        setState(() {
          ducError = pageDetails['text24'];
        });
      }
    } else {
      setState(() {
        ducError = null;
        totalDucAmount = 0;
      });
    }
  }

  validdateCash(setState) {
    if (cashAmount.text.length > 0) {
      setState(() {
        totalCashAmount = double.parse(cashAmount.text);
      });

      if (totalCashAmount >= payoutSystemDetails['cash_min']) {
        if (totalCashAmount <= payoutUserDetails['cash_account_bal']) {
          if (totalCashAmount <= payoutSystemDetails['cash_max']) {
            setState(() {
              cashError = null;
            });
          } else {
            setState(() {
              cashError = pageDetails['text23'];
            });
          }
        } else {
          setState(() {
            cashError = pageDetails['text22'];
          });
        }
      } else if (payoutUserDetails['cash_account_bal'] == 0) {
        setState(() {
          cashError = pageDetails['text22'];
        });
      } else {
        setState(() {
          cashError = pageDetails['text24'];
        });
      }
    } else {
      setState(() {
        cashError = null;
        totalCashAmount = 0;
      });
    }
  }

  pyout(setstate1) async {
    // ignore: unused_local_variable

    // ignore: unused_local_variable
    bool isValid = true;

    bool status = false;
    Map<String, dynamic> _map = {};

    if (selectedoption == 1) {
      udscaddress.text = "";
      cashAmount.text = "";

      cashError = null;
      udscError = null;
      if (ducAmount.text.length < 1) {
        isValid = false;
        ducError = pageDetails['text25'];
      }
      if (address.text.length < 3) {
        isValid = false;
        addressError = pageDetails['text18'];
      } else if (addressError != null || ducError != null) {
        //print(ducError);
        //print(addressError);
        isValid = false;
        // final snackBar = SnackBar(content: MyText(text:'conditions not met'));
        // ScaffoldMessenger.of(context).showSnackBar(snackBar);
      } else if (addressError == null && ducError == null) {
        status = true;
      }
    }
    if (selectedoption == 2) {
      ducAmount.text = "";
      address.text = "";
      addressError = null;
      ducError = null;
      if (cashAmount.text.length < 1) {
        isValid = false;
        cashError = pageDetails['text25'];
      }
      if (udscaddress.text.length < 3) {
        isValid = false;
        udscError = pageDetails['text21'];
      } else if (udscError != null || cashError != null) {
        //print(cashError);
        //print(udscError);
        isValid = false;
        // final snackBar = SnackBar(content: MyText(text:'conditions not met'));
        // ScaffoldMessenger.of(context).showSnackBar(snackBar);
      } else if (udscError == null && cashError == null) {
        status = true;
      }
    }

    if (status == true) {
      if (selectedoption == 1) {
        udscaddress.text = "";
        cashAmount.text = "";
        cashError = null;
        udscError = null;
        status = false;
        // if (isKyc == true) {
        //   showDialog(
        //       barrierDismissible: false,
        //       context: context,
        //       builder: (BuildContext context) {
        //         return KycPopUp();
        //       });
        // } else {
        if (payoutSystemDetails['allow_payout_duc'] == false) {
          print('YES CASH');
          snack.snack(
              title: 'You already have a Pending Payout request for DUC');

          // final snackBar = SnackBar(
          //     content: MyText(text:await Utils.getMessage(
          //         'You already have one pending payout')));
          // ScaffoldMessenger.of(context).showSnackBar(snackBar);
        } else {
          _map = {
            "product_id": 1,
            "amount": double.parse(ducAmount.text),
            "asset_type": "duc",
            "address_line": address.text
          };
          waiting.waitpopup(context);

          Map<String, dynamic> _result = await HttpRequest.Post(
              'c_rms_payout', Utils.constructPayload(_map));
          if (Utils.isServerError(_result)) {
            Navigator.pop(context);
            isKyc == true
                ? print("kyc")
                : showDialog(
                    context: context,
                    builder: (_) => SucessPopup(
                        message: pageDetails['text26'],
                        route: 'request_payout'));
            // final snackBar = SnackBar(
            //     content:
            //         MyText(text:await Utils.getMessage(_result['response']['error'])));
            // ScaffoldMessenger.of(context).showSnackBar(snackBar);
          } else {
            udscaddress.text = "";
            cashAmount.text = "";
            ducAmount.text = "";
            address.text = "";
            totalCashAmount = 0;
            totalDucAmount = 0;
            addressError = null;
            ducError = null;
            cashError = null;
            udscError = null;
            Navigator.pop(context);
            print(_result['response']['data']);
            showDialog(
                context: context,
                builder: (_) => SucessPopup(
                    message: pageDetails['text27'], route: 'request_payout'));
          }
        }
        // }
      } else if (selectedoption == 2) {
        ducAmount.text = "";
        address.text = "";
        addressError = null;
        ducError = null;
        if (isKyc == true) {
          showDialog(
              barrierDismissible: false,
              context: context,
              builder: (BuildContext context) {
                // return NoKycPopUp();
                return KycPopUp();
              });
        } else {
          if (payoutSystemDetails['allow_payout_cash'] == false) {
            print('YES DUC');
            snack.snack(
                title: 'You already have a Pending Payout request for Cash');
          } else {
            _map = {
              "product_id": 1,
              "amount": double.parse(cashAmount.text),
              "asset_type": "cash",
              "address_line": udscaddress.text
            };
            waiting.waitpopup(context);

            Map<String, dynamic> _result = await HttpRequest.Post(
                'c_rms_payout', Utils.constructPayload(_map));
            if (Utils.isServerError(_result)) {
              Navigator.pop(context);
              isKyc == true
                  ? print("kyc")
                  : showDialog(
                      context: context,
                      builder: (_) => SucessPopup(
                          message: pageDetails['text26'],
                          route: 'request_payout'));
              // final snackBar = SnackBar(
              //     content:
              //         MyText(text:await Utils.getMessage(_result['response']['error'])));
              // ScaffoldMessenger.of(context).showSnackBar(snackBar);
            } else {
              udscaddress.text = "";
              cashAmount.text = "";
              ducAmount.text = "";
              address.text = "";
              totalCashAmount = 0;
              totalDucAmount = 0;
              addressError = null;
              ducError = null;
              cashError = null;
              udscError = null;
              Navigator.pop(context);
              print(_result['response']['data']);
              showDialog(
                  context: context,
                  builder: (_) => SucessPopup(
                      message: pageDetails['text27'], route: 'request_payout'));
            }
          }
        }
      }
    } else {
      // final snackBar = SnackBar(content: MyText(text:'conditions not met'));
      // ScaffoldMessenger.of(context).showSnackBar(snackBar);
      // print("error");
    }
  }

  // waitpopup(context) {
  //   showDialog(
  //       barrierDismissible: false,
  //       context: context,
  //       builder: (BuildContext context) {
  //         return AlertDialog(
  //           elevation: 24.0,
  //           backgroundColor: Pallet.popupcontainerback,
  //           shape: RoundedRectangleBorder(
  //               borderRadius: BorderRadius.all(Radius.circular(15.0))),
  //           title: Container(
  //             width: 50,
  //             child: Column(
  //               crossAxisAlignment: CrossAxisAlignment.center,
  //               children: [
  //                 CircularProgressIndicator(
  //                     valueColor: AlwaysStoppedAnimation<Color>(Colors.white)),
  //                 SizedBox(
  //                   height: 10,
  //                 ),
  //                 MyText(text:"Please Wait...",
  //                     style: TextStyle(
  //                         fontSize: Pallet.heading3,
  //                         color: Pallet.fontcolor,
  //                         fontWeight: Pallet.font500)),
  //               ],
  //             ),
  //           ),
  //         );
  //       });
  // }
}
