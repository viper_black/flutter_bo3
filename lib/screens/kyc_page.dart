import '../animations/drop.dart';
import '../animations/dropdown.dart';
import '../common.dart';
import '../services/business/cart.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/services.dart';
import 'package:intl/intl.dart';
import '../animations/Bouncing_button.dart';
import '../config.dart';
// ignore: avoid_web_libraries_in_flutter
import 'package:universal_html/html.dart' hide Text, Navigator;
import 'dart:convert';
import 'dart:typed_data';

import '../home.dart';

// ignore: must_be_immutable
class KycPage extends StatefulWidget {
  final double wdgtWidth, wdgtHeight;

  const KycPage({
    Key key,
    this.wdgtWidth,
    this.wdgtHeight,
  }) : super(key: key);
  @override
  _KycPageState createState() => _KycPageState();
}

class _KycPageState extends State<KycPage> {
  int pageidx = 0;

  // TextEditingController sponsor = TextEditingController(text: '');
  String sponsorError;
  FocusNode sponsornode = FocusNode();
  FocusNode next1 = FocusNode();
  FocusNode next2 = FocusNode();
  FocusNode next3 = FocusNode();
  FocusNode next4 = FocusNode();

  FocusNode back1 = FocusNode();
  FocusNode back2 = FocusNode();
  FocusNode back3 = FocusNode();

  TextEditingController firstname = TextEditingController();
  String firstnameError;
  FocusNode firstnamenode = FocusNode();

  TextEditingController lastname = TextEditingController();
  String lastnameError;
  FocusNode lastnamenode = FocusNode();

  TextEditingController address = TextEditingController();
  String addressError;
  FocusNode addressnode = FocusNode();
  TextEditingController city = TextEditingController();
  String cityError;
  FocusNode citynode = FocusNode();
  TextEditingController zip = TextEditingController();
  String zipError;
  FocusNode zipnode = FocusNode();
  TextEditingController country = TextEditingController();
  String countryError;
  FocusNode countrynode = FocusNode();
  String countryname, nationality, country2;
  TextEditingController dob = TextEditingController();
  String dobError;
  FocusNode dobnode = FocusNode();

  List<String> countryList = [];

  List dbCountryList = [];

  output(value) {
    dbCountryList = value;
    for (var item in value) {
      setState(() {
        countryList.add(item['country_name']);
      });
    }
  }

  String radioItem = '';
  String kycstatus = "Your KYC is under Processing";
  DateTime selectedDate = DateTime.now();
  TextEditingController datecontroller = TextEditingController();
  DateTime _date = DateTime.now();
  final currentyear = DateTime(DateTime.now().year);
  final int age = 18;

  handleDatePicker() async {
    final DateFormat _dateFormatter = DateFormat('MMM dd, yyyy');
    @override
    final DateTime date = await showDatePicker(
      context: context,
      initialDate: _date,
      firstDate: DateTime(1900),
      lastDate: DateTime(currentyear.year + 1),
    );
    if (date != null && date != _date) {
      setState(() {
        _date = date;
      });
      if (currentyear.year - date.year < 18) {
        setState(() {
          dobError = 'Age Must Be Greater than 18';
        });
      } else {
        dobError = null;
      }
      // DateTime test = _dateFormatter.parse(date.toString());
      dob.text = _dateFormatter.format(date);
    }
  }

  Image file;
  Uint8List uploadedImage1;
  FileReader reader = FileReader();
  uploadImage() {
    InputElement uploadInput = FileUploadInputElement()..accept = 'image/*';
    uploadInput.click();
    uploadInput.onChange.listen((event) {
      final file = uploadInput.files.first;
      reader.readAsArrayBuffer(file);
      reader.onLoadEnd.listen((event) {
        setState(() {
          uploadedImage1 = reader.result;
        });
        var test = base64.encode(uploadedImage1);
        print(test);
      });
    });
  }

  Image file2;
  Uint8List uploadedImage2;
  FileReader reader2 = FileReader();
  uploadImage2() {
    InputElement uploadInput = FileUploadInputElement()..accept = 'image/*';
    uploadInput.click();
    uploadInput.onChange.listen((event) {
      final file = uploadInput.files.first;
      reader2.readAsArrayBuffer(file);
      reader2.onLoadEnd.listen((event) {
        setState(() {
          uploadedImage2 = reader2.result;
        });
        var test = base64.encode(uploadedImage2);
        print(test);
      });
    });
  }

  bool isRequired = false;
  bool isRequiredcountry2 = false;
  bool isRequiredcountry3 = false;

  bool enablecity = false;
  bool enablezipcode = false;
  String dropDownCountry = '';
  String dropDownCity = '';
  String dropDownZip = '';
  Map<String, dynamic> getRegionsMap = {
    'system_product_id': 1,
    'region_level': 2,
    'parent_region': 'null'
  };

  List<DropdownMenuItem<String>> countrylist = [];

  void initState() {
    cart.getAddressInfo(setState);
    cart.getRegions(getRegionsMap);
    super.initState();
  }

  bool ismobile;
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Container(
      child: LayoutBuilder(builder: (context, constraints) {
        if (constraints.maxWidth < ScreenSize.ipad) {
          return Container(
            width: size.width,
            child: SingleChildScrollView(
              child: Container(
                  child: screen(wdgtWidth: widget.wdgtWidth, ismobile: true)),
            ),
          );
        } else {
          return Center(
            child: Container(
                width: widget.wdgtWidth,
                height: size.height,
                child: SingleChildScrollView(
                  child: screen(
                      wdgtWidth: pageidx == 2 || pageidx == 3 ? 550 : 400,
                      ismobile: false),
                )),
          );
        }
      }),
    );
  }

  Container headerline() {
    return Container(
      padding: EdgeInsets.all(15),
      child: header(),
    );
  }

  // ignore: missing_return
  Text header() {
    if (pageidx == 0)
      return Text("KYC Verfication",
          style: TextStyle(
              fontWeight: FontWeight.w700,
              fontSize: Pallet.heading2,
              color: Pallet.fontcolornew));
    else if (pageidx == 1)
      return Text("",
          style: TextStyle(
              fontWeight: Pallet.font600, color: Pallet.fontcolornew));
    else if (pageidx == 2)
      return Text("", style: TextStyle(color: Pallet.fontcolornew));
    else if (pageidx == 3)
      return Text("", style: TextStyle(color: Pallet.fontcolornew));
    else if (pageidx == 4)
      return Text("", style: TextStyle(color: Pallet.fontcolornew));
  }

  // ignore: missing_return
  Widget screen({double wdgtWidth, bool ismobile}) {
    if (pageidx == 0)
      return screen1(wdgtWidth: wdgtWidth);
    else if (pageidx == 1)
      return screen2(wdgtWidth: wdgtWidth);
    else if (pageidx == 2)
      return screen3(wdgtWidth: wdgtWidth);
    else if (pageidx == 3)
      return screen4(wdgtWidth: wdgtWidth);
    else if (pageidx == 4) return screen5(wdgtWidth: wdgtWidth);
  }

  Widget button(
      {String label,
      Function opration,
      @required FocusNode node,
      double width,
      Function onkey}) {
    return RawKeyboardListener(
      focusNode: node,
      onKey: onkey,
      child: Bouncing(
        onPress: opration,
        child: MouseRegion(
          cursor: SystemMouseCursors.click,
          child: Container(
            width: width == null ? 100 : width,
            height: 35,
            padding: EdgeInsets.all(5),
            decoration: BoxDecoration(
              color: Pallet.fontcolornew,
              borderRadius: BorderRadius.circular(8.0),
            ),
            child: Center(
              child: Text(
                label,
                style: TextStyle(
                  color: Pallet.fontcolor,
                  fontSize: Pallet.heading5,
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

  Padding screen1({double wdgtWidth}) {
    return Padding(
      padding: ismobile == true
          ? EdgeInsets.symmetric(horizontal: 10)
          : EdgeInsets.symmetric(horizontal: 50),
      child: Container(
          width: wdgtWidth,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              headerline(),
              Container(
                child: SizedBox(
                  height: wdgtWidth * 0.3,
                  child: Text("User Must Upload their identity"),
                ),
              ),
              button(
                width: 150,
                node: next1,
                label: "Start Verification",
                opration: () {
                  setState(() {
                    next(width: wdgtWidth);
                  });
                },
              ),
              SizedBox(
                height: 30,
              ),
            ],
          )),
    );
  }

  Widget screen2({double wdgtWidth}) {
    return Padding(
      padding: ismobile == true
          ? EdgeInsets.symmetric(horizontal: 5)
          : EdgeInsets.symmetric(horizontal: 50),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Container(
            width: wdgtWidth,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                headerline(),
                Text(
                  "Country",
                  style: Pallet.profiletext,
                ),
                SizedBox(
                  height: ismobile == false ? 15 : 10,
                ),
                Container(
                  padding: EdgeInsets.all(10),
                  child: Container(
                    width: ismobile == true ? 200 : 350,
                    child: DropDownField(
                      // hintText: 'Please choose country',
                      required: isRequiredcountry3,
                      labelText: "Country",
                      labelStyle: TextStyle(
                        color: Pallet.fontcolornew,
                      ),
                      textStyle: TextStyle(color: Pallet.fontcolornew),
                      value: country2,
                      onValueChanged: (val) {
                        setState(() {
                          country2 = val;
                          getRegionsMap['parent_region'] =
                              country2.toUpperCase();
                        });
                      },
                      items: cart.countries,
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 40.0),
                  child: Text(
                    "Choose your Identity Type:",
                    style: Pallet.profiletextsubheading,
                  ),
                ),
                SizedBox(
                  height: ismobile == false ? 10 : 15,
                ),
                Container(
                  width: ismobile == true ? 20 : 350,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      Container(
                        child: Row(
                          children: [
                            Radio(
                              groupValue: radioItem,
                              value: 'passport',
                              onChanged: (val) {
                                setState(() {
                                  radioItem = val;
                                });
                              },
                            ),
                            SizedBox(
                              width: 5,
                            ),
                            Text(
                              'Passport',
                              style: Pallet.profiletextsubheading,
                            ),
                          ],
                        ),
                      ),
                      Container(
                        width: ismobile == true ? 50 : 150,
                        child: Row(
                          children: [
                            Radio(
                              groupValue: radioItem,
                              value: 'identity',
                              onChanged: (val) {
                                setState(() {
                                  radioItem = val;
                                });
                              },
                            ),
                            SizedBox(
                              width: 5,
                            ),
                            Text(
                              'Identity',
                              style: Pallet.profiletextsubheading,
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
                SizedBox(
                  height: 50,
                ),
                Container(
                  width: ismobile == true ? 200 : 350,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      button(
                        node: next1,
                        label: "Back",
                        opration: () {
                          setState(() {
                            radioItem = '';
                            country2 = null;
                            back(width: wdgtWidth);
                          });
                        },
                      ),
                      button(
                        node: next1,
                        label: "next",
                        opration: () {
                          setState(() {
                            isRequiredcountry3 = true;
                            if ((radioItem == '' || radioItem == null)) {
                              ScaffoldMessenger.of(context)
                                  .showSnackBar(SnackBar(
                                content: Text("Must Select Document Type"),
                              ));
                            } else if (country2 == null || country2 == '') {
                              isRequiredcountry3 = true;
                            } else {
                              print("xccccccccccccccccc");
                              print(radioItem);
                              next(width: wdgtWidth);
                            }
                            // if (country2 != null &&
                            //     (radioItem != '' || radioItem != null)) {

                            // }
                          });
                        },
                      ),
                    ],
                  ),
                ),
                SizedBox(
                  height: 30,
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget screen3({double wdgtWidth}) {
    return Padding(
      padding: ismobile == true
          ? EdgeInsets.symmetric(horizontal: 10)
          : EdgeInsets.symmetric(horizontal: 50),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Container(
            width: wdgtWidth,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                headerline(),
                Text(
                  "Document",
                  style: Pallet.profiletext,
                ),
                SizedBox(
                  height: ismobile == false ? 15 : 10,
                ),
                Wrap(
                  children: [
                    Container(
                        child: Column(
                      children: [
                        if (uploadedImage1 == null)
                          Container(
                            height: 200,
                            width: 170,
                            child: Image.asset("passports.png"),
                          ),
                        if (uploadedImage1 != null)
                          Container(
                            height: 200,
                            width: 170,
                            child: Image.memory(uploadedImage1),
                          ),
                        SizedBox(
                          width: 15,
                        ),
                        GestureDetector(
                          onTap: uploadedImage1 == null
                              ? () {
                                  setState(() {
                                    uploadImage();
                                  });
                                }
                              : () {
                                  setState(() {
                                    uploadedImage1 = null;
                                    uploadedImage2 = null;
                                  });
                                },
                          child: MouseRegion(
                            cursor: SystemMouseCursors.click,
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                uploadedImage1 == null
                                    ? Icon(Icons.add)
                                    : Icon(Icons.delete),
                                SizedBox(
                                  width: 5,
                                ),
                                Text(
                                  uploadedImage1 == null
                                      ? "upload Document"
                                      : "Delete",
                                  style: Pallet.profiletextsubheading,
                                ),
                              ],
                            ),
                          ),
                        ),
                      ],
                    )),
                    SizedBox(
                      height: 10,
                    ),
                    uploadedImage1 != null
                        ? radioItem == "passport"
                            ? Container(
                                child: Column(
                                  children: [
                                    if (uploadedImage2 == null)
                                      Container(
                                        height: 200,
                                        width: 170,
                                        child: Image.asset("passports.png"),
                                      ),
                                    if (uploadedImage2 != null)
                                      Container(
                                        height: 200,
                                        width: 170,
                                        child: Image.memory(uploadedImage2),
                                      ),
                                    SizedBox(
                                      width: 15,
                                    ),
                                    GestureDetector(
                                      onTap: uploadedImage2 == null
                                          ? () {
                                              setState(() {
                                                uploadImage2();
                                              });
                                            }
                                          : () {
                                              setState(() {
                                                uploadedImage2 = null;
                                              });
                                            },
                                      child: MouseRegion(
                                        cursor: SystemMouseCursors.click,
                                        child: Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.center,
                                          children: [
                                            uploadedImage2 == null
                                                ? Icon(Icons.add)
                                                : Icon(Icons.delete),
                                            SizedBox(
                                              width: 5,
                                            ),
                                            Text(
                                              uploadedImage2 == null
                                                  ? "upload Document"
                                                  : "Delete",
                                              style:
                                                  Pallet.profiletextsubheading,
                                            ),
                                          ],
                                        ),
                                      ),
                                    ),
                                    SizedBox(
                                      height: 10,
                                    )
                                  ],
                                ),
                              )
                            : Container()
                        : Container(),
                  ],
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 40.0),
                  child: Text(
                    "Personal:",
                    style: Pallet.profiletextsubheading,
                  ),
                ),
                SizedBox(
                  height: ismobile == false ? 10 : 15,
                ),
                Container(
                    child: Wrap(
                  children: [
                    Container(
                      width: 250,
                      padding: EdgeInsets.all(10),
                      child: SignupTextBox(
                        autofocus: true,
                        focusnode: firstnamenode,
                        tabpress: (event) {
                          if (event.isKeyPressed(LogicalKeyboardKey.tab)) {
                            FocusScope.of(context).requestFocus(lastnamenode);
                          }
                        },
                        label: "First Name",
                        icon: Icons.person_add_alt_1_outlined,
                        errorText: firstnameError,
                        isPrefixIcon: true,
                        controller: firstname,
                        isSuffixIcon: false,
                        digitsOnlyPhone: false,
                        isPassword: false,
                        validation: (value) {
                          setState(() {
                            if (value.trim().isEmpty) {
                              firstnameError = "Required";
                            } else {
                              firstnameError = null;
                            }
                          });
                        },
                      ),
                    ),
                    Container(
                      width: 250,
                      padding: EdgeInsets.all(10),
                      child: TextFormField(
                        focusNode: dobnode,
                        readOnly: true,
                        controller: dob,
                        onTap: handleDatePicker,
                        textAlign: TextAlign.left,
                        style: TextStyle(
                            color: Pallet.fontcolornew,
                            fontSize: Pallet.heading3,
                            fontWeight: Pallet.font500),
                        cursorColor: Pallet.fontcolornew,
                        keyboardType: TextInputType.emailAddress,
                        decoration: InputDecoration(
                          errorText: dobError,
                          errorStyle: TextStyle(
                            fontSize: Pallet.heading4,
                            color: Pallet.errortxt,
                          ),
                          border: OutlineInputBorder(
                              borderSide: const BorderSide(
                                  color: Color(0XFF1034a6), width: 1.0),
                              borderRadius: BorderRadius.circular(8.0)),
                          hintText: "Date of Birth",
                          hintStyle: TextStyle(
                              fontSize: Pallet.heading4,
                              fontWeight: Pallet.font500,
                              color: Pallet.fontcolornew),
                          fillColor: Pallet.fontcolor,
                          focusedBorder: OutlineInputBorder(
                              borderSide: const BorderSide(
                                  color: Color(0XFF1034a6), width: 1.0),
                              borderRadius: BorderRadius.circular(8.0)),
                          enabledBorder: OutlineInputBorder(
                              borderSide: BorderSide(
                                  color: Color(0xFF4570ae), width: 0.0),
                              borderRadius: BorderRadius.circular(8.0)),
                          filled: true,
                          prefixIcon: Padding(
                              padding: EdgeInsets.only(left: 20, right: 10),
                              child: Icon(Icons.calendar_today,
                                  size: 22, color: Pallet.fontcolornew)),
                        ),
                      ),
                    ),
                    Container(
                      width: 250,
                      padding: EdgeInsets.all(10),
                      child: SignupTextBox(
                        focusnode: lastnamenode,
                        tabpress: (event) {
                          if (event.isKeyPressed(LogicalKeyboardKey.tab)) {
                            FocusScope.of(context).requestFocus(dobnode);
                          }
                        },
                        label: "Last name",
                        icon: Icons.person_add_alt_1_outlined,
                        errorText: lastnameError,
                        isPassword: false,
                        isPrefixIcon: true,
                        controller: lastname,
                        digitsOnlyPhone: false,
                        isSuffixIcon: false,
                        validation: (value) {
                          setState(() {
                            if (value.trim().isEmpty) {
                              lastnameError = "Required";
                            } else {
                              lastnameError = null;
                            }
                          });
                        },
                      ),
                    ),
                    Container(
                        padding: EdgeInsets.all(10),
                        child: Container(
                          width: 240,
                          child: DropDownField(
                            // hintText: 'Please choose country',
                            labelText: "Nationality",
                            labelStyle: TextStyle(
                              color: Pallet.fontcolornew,
                            ),
                            required: isRequired,
                            textStyle: TextStyle(color: Pallet.fontcolornew),
                            value: nationality,
                            onValueChanged: (newValue) {
                              setState(() {
                                nationality = newValue;
                              });
                            },
                            items: cart.countries,
                          ),
                        )),
                  ],
                )),
                SizedBox(
                  height: 50,
                ),
                Container(
                  width: ismobile == true ? 200 : 480,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      button(
                        node: next1,
                        label: "Back",
                        opration: () {
                          setState(() {
                            isRequired = false;
                            nationality = null;
                            firstname.text = '';
                            lastname.text = '';
                            dob.text = '';
                            firstnameError = null;
                            lastnameError = null;
                            dobError = null;
                            back(width: wdgtWidth);
                          });
                        },
                      ),
                      button(
                        node: next1,
                        label: "next",
                        opration: () {
                          setState(() {
                            isRequired = true;
                            if (firstname.text.trim() == '' ||
                                firstname.text.trim() == null) {
                              firstnameError = "Required";
                            }
                            if (lastname.text.trim() == '' ||
                                lastname.text.trim() == null) {
                              lastnameError = "Required";
                            }
                            if (dob.text.trim() == '' ||
                                dob.text.trim() == null) {
                              dobError = "Required";
                            }
                            if (nationality == null) {}
                            if (uploadedImage1 == null) {
                              ScaffoldMessenger.of(context)
                                  .showSnackBar(SnackBar(
                                content: Text("Must upload document"),
                              ));
                            }
                            if (firstnameError == null &&
                                lastnameError == null &&
                                dobError == null &&
                                nationality != null &&
                                uploadedImage1 != null) {
                              next(width: wdgtWidth);
                            }
                          });
                        },
                      ),
                    ],
                  ),
                ),
                SizedBox(
                  height: 30,
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  // screen4
  Widget screen4({double wdgtWidth}) {
    return Padding(
      padding: ismobile == true
          ? EdgeInsets.symmetric(horizontal: 10)
          : EdgeInsets.symmetric(horizontal: 50),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Container(
            width: wdgtWidth,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                headerline(),
                Text(
                  "Address:",
                  style: Pallet.profiletext,
                ),
                SizedBox(
                  height: ismobile == false ? 10 : 15,
                ),
                Container(
                    // width: ismobile == true ? 200 : 350,
                    child: Wrap(
                  // spacing: 10,
                  // runSpacing: 10,
                  // mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    Container(
                      width: 250,
                      padding: EdgeInsets.all(10),
                      child: SignupTextBox(
                        autofocus: true,
                        focusnode: addressnode,
                        tabpress: (event) {
                          if (event.isKeyPressed(LogicalKeyboardKey.tab)) {
                            FocusScope.of(context).requestFocus(citynode);
                          }
                        },
                        label: "Address",
                        errorText: addressError,
                        controller: address,
                        digitsOnlyPhone: false,
                        isPassword: false,
                        validation: (value) {
                          setState(() {
                            if (value.trim().isEmpty) {
                              addressError = "Required";
                            } else {
                              addressError = null;
                            }
                          });
                        },
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.all(10),
                      child: Container(
                        width: 230,
                        child: DropDownField(
                          labelText: "Country",
                          // hintText: 'Please choose country',
                          required: isRequiredcountry2,
                          textStyle: TextStyle(color: Pallet.fontcolornew),
                          value: countryname,
                          onValueChanged: (val) {
                            setState(() {
                              cart.cities = [];
                              cart.zipCode = [];
                              city.text = '';
                              zip.text = '';
                              enablecity = true;
                              countryname = val;
                              getRegionsMap['region_level'] = 4;
                              getRegionsMap['parent_region'] =
                                  countryname.toUpperCase();

                              cart.getCities(getRegionsMap);
                            });
                          },
                          items: cart.countries,
                        ),
                      ),
                    ),
                    Container(
                      width: 250,
                      padding: EdgeInsets.all(10),
                      child: AbsorbPointer(
                        absorbing: !enablecity ? true : false,
                        child: TypeAheadFormField(
                          hinttext: "City",
                          errortext: cityError,
                          textFieldConfiguration: TextFieldConfiguration(
                            cursorColor: Pallet.fontcolornew,
                            style: TextStyle(
                                color: Pallet.fontcolornew,
                                fontSize: Pallet.heading3,
                                fontWeight: Pallet.font500),
                            controller: city,
                          ),
                          suggestionsCallback: (pattern) {
                            return cart.cities;
                          },
                          itemBuilder: (context, suggestion) {
                            return ListTile(
                              title: Text(
                                suggestion,
                                style: TextStyle(color: Pallet.fontcolornew),
                              ),
                            );
                          },
                          transitionBuilder:
                              (context, suggestionsBox, controller) {
                            return suggestionsBox;
                          },
                          onSuggestionSelected: (suggestion) {
                            setState(() {
                              print('lllllllllllllllllllllllllllll');
                              cart.zipCode.clear();

                              this.city.text = suggestion;
                              cityError = null;
                              enablezipcode = true;
                              getRegionsMap['region_level'] = 5;
                              getRegionsMap['parent_region'] = city.text;
                              print(
                                  'VVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVV');

                              cart.getZipCode(getRegionsMap);
                            });
                          },
                          // ignore: missing_return
                          onchanged: (value) {
                            if (value.trim().isEmpty) {
                              setState(() {
                                cityError = 'Required';
                              });
                            } else {
                              setState(() {
                                enablezipcode = true;
                                cityError = null;
                              });
                            }
                          },
                          onSaved: (value) {
                            setState(() {
                              if (value.trim().isEmpty) {
                                cityError = 'Required';
                              } else {
                                cityError = null;
                              }
                            });
                          },
                          // ignore: missing_return
                          validator: (value) {
                            if (value.trim().isEmpty) {
                              setState(() {
                                cityError = 'Required';
                              });
                            } else {
                              setState(() {
                                cityError = null;
                              });
                            }
                          },
                        ),
                      ),
                    ),
                    Container(
                        width: 250,
                        padding: EdgeInsets.all(10),
                        child: AbsorbPointer(
                            absorbing: !enablezipcode ? true : false,
                            child: TypeAheadFormField(
                              // ignore: missing_return
                              validator: (val) {
                                if (val.isEmpty) {}
                              },
                              errortext: zipError,
                              hinttext: "Zip Code",
                              textFieldConfiguration: TextFieldConfiguration(
                                  cursorColor: Pallet.fontcolornew,
                                  style: TextStyle(
                                      color: Pallet.fontcolornew,
                                      fontSize: Pallet.heading3,
                                      fontWeight: Pallet.font500),
                                  controller: zip,
                                  decoration: InputDecoration(
                                    border: OutlineInputBorder(
                                        borderSide: const BorderSide(
                                            color: Color(0XFF1034a6),
                                            width: 1.0),
                                        borderRadius:
                                            BorderRadius.circular(8.0)),
                                    errorBorder: OutlineInputBorder(
                                        borderSide: BorderSide(
                                            color: Colors.red, width: 2),
                                        borderRadius: BorderRadius.circular(
                                            Pallet.radius)),
                                    focusedErrorBorder: OutlineInputBorder(
                                        borderSide: BorderSide(
                                            color: Colors.red, width: 2),
                                        borderRadius: BorderRadius.circular(
                                            Pallet.radius)),
                                    focusedBorder: OutlineInputBorder(
                                        borderSide: const BorderSide(
                                            color: Color(0XFF1034a6),
                                            width: 1.0),
                                        borderRadius:
                                            BorderRadius.circular(8.0)),
                                    enabledBorder: OutlineInputBorder(
                                        borderSide: BorderSide(
                                            color: Color(0xFF4570ae),
                                            width: 0.0),
                                        borderRadius:
                                            BorderRadius.circular(8.0)),
                                  )),
                              suggestionsCallback: (pattern) {
                                return cart.zipCode;
                              },

                              itemBuilder: (context, suggestion) {
                                return ListTile(
                                  title: Text(
                                    suggestion,
                                    style:
                                        TextStyle(color: Pallet.fontcolornew),
                                  ),
                                );
                              },
                              transitionBuilder:
                                  (context, suggestionsBox, controller) {
                                return suggestionsBox;
                              },

                              onSuggestionSelected: (suggestion) {
                                setState(() {
                                  // isRequired = false;
                                  zipError = null;

                                  this.zip.text = suggestion;
                                });
                              },
                            ))),
                  ],
                )),
                SizedBox(
                  height: 50,
                ),
                Container(
                  width: ismobile == true ? 200 : 480,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      button(
                        node: next1,
                        label: "Back",
                        opration: () {
                          setState(() {
                            addressError = null;
                            cityError = null;
                            zipError = null;
                            countryError = null;
                            address.text = '';
                            city.text = '';
                            zip.text = '';
                            countryname = '';
                            back(width: wdgtWidth);
                          });
                        },
                      ),
                      button(
                        node: next1,
                        label: "Done",
                        opration: () {
                          isRequiredcountry2 = true;
                          setState(() {
                            if (address.text == null || address.text == '') {
                              print("aaaaaaaaaaaaaaaaaaaaaa");
                              addressError = "Required";
                            }
                            if (city.text == null || city.text == '') {
                              print("ccccccccccccccccccc");

                              cityError = "Required";
                            }
                            if (zip.text == null || zip.text == '') {
                              print("xxxxxxxxxxxxxxxxxx");
                              zipError = "Required";
                            } else {
                              print("hhhhhhhhhhhhhhhhhhhhhh");
                              print(addressError);
                              print(cityError);

                              print(zipError);

                              print(addressError);

                              if (addressError == null &&
                                  cityError == null &&
                                  zipError == null &&
                                  countryname != null) {
                                next(width: wdgtWidth);
                              }
                            }
                          });
                        },
                      ),
                    ],
                  ),
                ),
                SizedBox(
                  height: 30,
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget screen5({double wdgtWidth}) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Image.asset(
          "success.gif",
          height: 100,
          width: 100,
        ),
        Text(
          "Thank you!",
          style: Pallet.profiletext,
        ),
        SizedBox(
          height: 10,
        ),
        Text(
          "$kycstatus",
          style: Pallet.profiletextsubheading,
        ),
        SizedBox(
          height: 15,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            // ignore: deprecated_member_use
            RaisedButton(
                color: Pallet.fontcolornew,
                child: Center(
                    child: Text('Continue',
                        style: TextStyle(
                          color: Pallet.fontcolor,
                          fontWeight: Pallet.font600,
                        ))),
                onPressed: () {
                  Navigator.pushReplacement(
                      context,
                      MaterialPageRoute(
                          builder: (BuildContext context) =>
                              Home(route: 'corporate_kyc')));
                }),
          ],
        ),
      ],
    );
  }

  next({double width}) {
    switch (pageidx) {
      case 0:
        pageidx += 1;
        break;
      case 1:
        pageidx += 1;
        break;
      case 2:
        pageidx += 1;

        break;
      case 3:
        pageidx += 1;
        break;
      case 4:
        pageidx += 1;
        break;
      case 5:
        pageidx += 1;
        break;
      default:
    }
  }

  back({double width, int index}) {
    switch (pageidx) {
      case 0:
        pageidx -= 1;
        break;
      case 1:
        pageidx -= 1;
        break;
      case 2:
        pageidx -= 1;

        break;
      case 3:
        pageidx -= 1;
        break;
      case 4:
        pageidx -= 1;
        break;
      case 5:
        pageidx -= 1;
        break;
      default:
    }
  }
}
