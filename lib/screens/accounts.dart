import 'package:centurion/config/app_settings.dart';
import 'package:centurion/config/index.dart';
import 'package:centurion/utils/utils.dart';
import 'package:flutter/material.dart';
// import '../data.dart';
import 'package:carousel_slider/carousel_slider.dart';
import '../common.dart';
import '../config.dart';
import '../services/business/account.dart';
// import '../screens/add_funding.dart';
import '../home.dart';
// import 'package:url_launcher/url_launcher.dart';

class Accounts extends StatefulWidget {
  Accounts(
      {Key key,
      this.wdgtWidth,
      this.wdgtHeight,
      this.assetType,
      this.intialPage})
      : super(key: key);
  final double wdgtWidth, wdgtHeight;
  final String assetType;
  final int intialPage;

  @override
  _AccountsState createState() => _AccountsState();
}

class _AccountsState extends State<Accounts> {
  CarouselControllerImpl carousalController = CarouselControllerImpl();
  Future<String> temp;
  int _currentindex;
  void initState() {
    accounts.statement(null, setState);
    if (widget.assetType == null) {
      temp = accounts.statement('cash', setState);
    } else {
      temp = accounts.statement(widget.assetType, setState);
    }

    //print(temp);
    setPageDetails();
    if (widget.intialPage == null) {
      _currentindex = 0;
    } else {
      _currentindex = widget.intialPage;
    }

    super.initState();
  }

  Map<String, dynamic> pageDetails = {};

  void setPageDetails() async {
    String data = await Utils.getPageDetails('account');
    setState(() {
      pageDetails = Utils.fromJSONString(data);
    });
  }

  String _tableheading;
  // ignore: unused_field
  String _tableheading1;
  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
        future: temp,
        builder: (context, snapshot) {
          if (snapshot.hasError) {
            return SomethingWentWrongMessage();
          } else if (snapshot.hasData) {
            //print(accounts.accounts[_currentindex]["asset_name"]);
// //print("iiiiiiiiiiiiiii");
            _currentindex != null
                ? _tableheading = accounts.accounts[_currentindex]["asset_name"]
                    .replaceAll('Balance', '')
                : //print('null');
                _currentindex != null
                    ? accounts.statement(
                        accounts.accounts[_currentindex]["asset_type"]
                            .replaceAll('Balance', ''),
                        setState)
                    : //print('null');
                    _tableheading1 = accounts.accounts[_currentindex]
                            ["asset_name"]
                        .replaceAll('Balance', '');
            //print('Myaccounts');
            //print(myresult);
            return LayoutBuilder(builder: (context, constraints) {
              if (constraints.maxWidth < ScreenSize.iphone) {
                return SingleChildScrollView(
                  child: Container(
                    width: widget.wdgtWidth,
                    // height: widget.wdgtHeight,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        slider(
                            wdgtWidth: widget.wdgtWidth,
                            // wdgtHieght: widget.wdgtWidth * 0.99,
                            fraction: 0.7,
                            scrollable: true,
                            wdgetAxis: Axis.horizontal),
                        SizedBox(
                          height: 10,
                        ),
                        Trans(
                          tableheading: _tableheading,
                          wdgtWidth: widget.wdgtWidth,
                          // wdgtHeight: widget.wdgtHeight,
                        ),
                      ],
                    ),
                  ),
                );
              } else if (constraints.maxWidth > ScreenSize.iphone &&
                  constraints.maxWidth < ScreenSize.ipad) {
                return SingleChildScrollView(
                  child: Container(
                    padding: EdgeInsets.only(
                        left: Pallet.leftPadding, top: Pallet.topPadding1),
                    width: widget.wdgtWidth,
                    // height: widget.wdgtHeight,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Padding(
                          padding:
                              EdgeInsets.only(bottom: Pallet.defaultPadding),
                          child: MyText(
                            text: pageDetails["text9"] == null
                                ? ''
                                : pageDetails["text9"],
                            style: TextStyle(
                              color: Pallet.fontcolornew,
                              fontSize: Pallet.heading1,
                            ),
                          ),
                        ),
                        slider(
                            scrollable: true,
                            wdgtWidth: widget.wdgtWidth,
                            // wdgtHieght: widgetr.wdgtHeight,
                            // sldrheight: widget.wdgtWidth * .5,
                            fraction: 0.5,
                            wdgetAxis: Axis.horizontal),
                        SizedBox(
                          height: 10,
                        ),
                        Trans(
                          tableheading: _tableheading,
                          wdgtWidth: widget.wdgtWidth,
                          // wdgtHeight: widget.wdgtHeight,
                        ),
                      ],
                    ),
                  ),
                );
              } else {
                return SingleChildScrollView(
                  child: Padding(
                    padding: EdgeInsets.only(
                        left: Pallet.leftPadding, top: Pallet.topPadding1),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,

                      // mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Padding(
                          padding:
                              EdgeInsets.only(bottom: Pallet.defaultPadding),
                          child: MyText(
                            text: pageDetails["text9"] == null
                                ? ''
                                : pageDetails["text9"],
                            style: TextStyle(
                              color: Pallet.fontcolornew,
                              fontSize: Pallet.heading1,
                            ),
                          ),
                        ),
                        // SizedBox(
                        //   height: widget.wdgtWidth * 0.08,
                        //   child: Row(
                        //     mainAxisAlignment: MainAxisAlignment.start,
                        //     children: [
                        //       MyText('AccountsStatement'),
                        //     ],
                        //   ),
                        // ),
                        Row(
                          // mainAxisAlignment: MainAxisAlignment.spaceAround,
                          // crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Trans(
                              tableheading: _tableheading,
                              wdgtWidth: widget.wdgtWidth * 0.55,
                              wdgtHeight: widget.wdgtHeight * 43,
                            ),
                            Column(
                              children: [
                                Container(
                                  decoration: BoxDecoration(
                                    color: Pallet.fontcolornew,
                                    shape: BoxShape.circle,
                                  ),
                                  child: IconButton(
                                    onPressed: () {
                                      carousalController.nextPage();
                                    },
                                    icon: Icon(
                                      Icons.arrow_drop_up,
                                      color: Pallet.fontcolor,
                                    ),
                                  ),
                                ),
                                Padding(
                                  padding: EdgeInsets.symmetric(
                                      horizontal: Pallet.leftPadding,
                                      vertical: 20),
                                  child: slider(
                                      wdgtWidth: widget.wdgtWidth * 0.29,
                                      wdgtHieght: widget.wdgtWidth * .35,
                                      fraction: 0.4,
                                      scrollable: false,
                                      // sldrheight: widget.wdgtWidth * 2,
                                      wdgetAxis: Axis.vertical),
                                ),
                                Container(
                                  decoration: BoxDecoration(
                                    color: Pallet.fontcolornew,
                                    shape: BoxShape.circle,
                                  ),
                                  child: IconButton(
                                    onPressed: () {
                                      carousalController.previousPage();
                                    },
                                    icon: Icon(
                                      Icons.arrow_drop_down,
                                      color: Pallet.fontcolor,
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                );
              }
            });
          }
          return Loader();
        });
  }

  Widget slider(
      {double wdgtWidth,
      sldrheight,
      wdgtHieght,
      fraction,
      Axis wdgetAxis,
      bool scrollable}) {
    // var init =widget.intialPage==null?0:widget.intialPage;
    // var _currentindex=init;
    return Stack(children: [
      Container(
          color: Pallet.fontcolor,
          width: wdgtWidth,
          height: wdgtHieght,
          child: accounts.accounts.length == 0 ||
                  accounts.accounts.length == null
              ? Container()
              : CarouselSlider.builder(
                  carouselController: carousalController,
                  options: CarouselOptions(
                      scrollPhysics: scrollable == false
                          ? NeverScrollableScrollPhysics()
                          : ScrollPhysics(),
                      autoPlay: false,
                      // pageSnapping: false,
                      reverse: true,
                      initialPage: _currentindex,
                      enableInfiniteScroll: true,
                      viewportFraction: fraction,
                      enlargeCenterPage: true,
                      // height: sldrheight,
                      // autoPlayInterval: Duration(seconds: 2),
                      // autoPlayCurve: Curves.fastOutSlowIn,
                      scrollDirection: wdgetAxis,
                      // autoPlayAnimationDuration: Duration(milliseconds: 500),
                      onPageChanged: (index, reason) {
                        setState(() {
                          //print("current index=$_currentindex");
                          _currentindex = index;
                          _tableheading =
                              accounts.accounts[index]["asset_name"];
                          accounts.statement(
                              accounts.accounts[index]["asset_type"], setState);
                        });
                      }),
                  itemCount: accounts.accounts.length,
                  itemBuilder: (BuildContext context, int index, _) {
                    //print('KKKKKKKKKK');

                    //print("ffffffffffff$index");
                    return GestureDetector(
                      onTap: () {},
                      child: Opacity(
                        opacity: scrollable == false
                            ? _currentindex == index
                                ? 1.0
                                : 0.8
                            : 1.0,
                        child: Container(
                            width: wdgtWidth * 0.95,
                            padding: EdgeInsets.symmetric(
                                horizontal: Pallet.leftPadding,
                                vertical: Pallet.defaultPadding),
                            decoration: BoxDecoration(
                              color: Pallet.dashcontainerback,
                              borderRadius:
                                  BorderRadius.circular(Pallet.radius),
                            ),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.spaceAround,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                ImageIcon(
                                  NetworkImage(appSettings['SERVER_URL'] +
                                      '/' +
                                      accounts.accounts[index]['asset_icon']),
                                  color: Pallet.fontcolor,
                                  size: 50,
                                ),
                                if (accounts.accounts[index]['asset_type'] ==
                                    'cash')
                                  MyText(
                                      text: pageDetails["text12"],
                                      style: TextStyle(
                                          fontSize: Pallet.heading3,
                                          color: Pallet.fontcolor)),
                                if (accounts.accounts[index]['asset_type'] ==
                                    'trading')
                                  MyText(
                                      text: pageDetails["text13"],
                                      style: TextStyle(
                                          fontSize: Pallet.heading3,
                                          color: Pallet.fontcolor)),
                                if (accounts.accounts[index]['asset_type'] ==
                                    'shopping')
                                  MyText(
                                      text: pageDetails["text14"],
                                      style: TextStyle(
                                          fontSize: Pallet.heading3,
                                          color: Pallet.fontcolor)),
                                if (accounts.accounts[index]['asset_type'] ==
                                    'duc')
                                  MyText(
                                      text: pageDetails["text16"],
                                      style: TextStyle(
                                          fontSize: Pallet.heading3,
                                          color: Pallet.fontcolor)),
                                if (accounts.accounts[index]['asset_type'] ==
                                    'charity')
                                  MyText(
                                      text: pageDetails["text15"],
                                      style: TextStyle(
                                          fontSize: Pallet.heading3,
                                          color: Pallet.fontcolor)),
                                if (accounts.accounts[index]['asset_type'] ==
                                    'dsv')
                                  MyText(
                                      text: pageDetails["text17"],
                                      style: TextStyle(
                                          fontSize: Pallet.heading3,
                                          color: Pallet.fontcolor)),
                                if (accounts.accounts[index]['asset_type'] ==
                                    'dsv_p')
                                  MyText(
                                      text: pageDetails["text18"],
                                      style: TextStyle(
                                          fontSize: Pallet.heading3,
                                          color: Pallet.fontcolor)),
                                if (accounts.accounts[index]['asset_type'] ==
                                    'aap')
                                  MyText(
                                      text: 'AA+',
                                      style: TextStyle(
                                          fontSize: Pallet.heading3,
                                          color: Pallet.fontcolor)),
                                MyText(
                                  text: accounts.accounts[index]
                                          ["asset_symbol"] +
                                      ' ' +
                                      accounts.accounts[index]["asset_val"],
                                  style: TextStyle(
                                      fontSize: Pallet.heading2,
                                      // fontWeight: Pallet.font500,
                                      color: Pallet.fontcolor),
                                ),
                                SizedBox(height: 10),
                              ],
                            )),
                      ),
                    );
                  })),
      // Container(height: 310, width: 600, color: Pallet.fontcolor)
    ]);
  }
}

class Trans extends StatefulWidget {
  // ignore: empty_constructor_bodies
  Trans({Key key, this.wdgtWidth, this.wdgtHeight, @required this.tableheading})
      // ignore: empty_constructor_bodies
      : super(key: key) {}
  final double wdgtWidth, wdgtHeight;
  final String tableheading;
  @override
  _TransState createState() => _TransState();
}

class _TransState extends State<Trans> {
  initState() {
    // //print("aaaaaaaaaaaaaaaa");
    // //print(widget.tableheading);
    setPageDetails();
    super.initState();
  }

  Map<String, dynamic> pageDetails = {};

  void setPageDetails() async {
    String data = await Utils.getPageDetails('account');
    setState(() {
      pageDetails = Utils.fromJSONString(data);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      width: widget.wdgtWidth,
      // height: widget.wdgtHeight,
      child: Wrap(
        // padding: const EdgeInsets.symmetric(horizontal: 5, vertical: 20),
        children: [
          Container(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(Pallet.radius),
              boxShadow: [Pallet.shadowEffect],
            ),
            // decoration: BoxDecoration(
            //     border: Border.all(color: Pallet.dashcontainerback)),
            width: widget.wdgtWidth / 1.05,
            // height: widget.wdgtHeight,
            child: PaginatedDataTable(
              columnSpacing: widget.wdgtWidth * 0.07,
              actions: <Widget>[
                Accountpopup(),
              ],
              header: MyText(
                  text: pageDetails["text1"] == null
                      ? ''
                      : widget.tableheading + " " + pageDetails["text1"],
                  style: TextStyle(
                      fontSize: Pallet.heading3,
                      color: Pallet.fontcolornew,
                      fontWeight: Pallet.subheading1wgt)),
              rowsPerPage: 10,
              columns: [
                DataColumn(
                    label: MyText(
                        text: pageDetails["text2"] == null
                            ? ''
                            : pageDetails["text2"],
                        style: TextStyle(
                            fontSize: Pallet.heading3,
                            fontWeight: Pallet.heading2wgt,
                            color: Pallet.fontcolornew))),
                DataColumn(
                    label: MyText(
                        text: pageDetails["text3"] == null
                            ? ''
                            : pageDetails["text3"],
                        style: TextStyle(
                            fontSize: Pallet.heading3,
                            fontWeight: Pallet.heading2wgt,
                            color: Pallet.fontcolornew))),
                DataColumn(
                    label: MyText(
                        text: pageDetails["text4"] == null
                            ? ''
                            : pageDetails["text4"],
                        style: TextStyle(
                            fontSize: Pallet.heading3,
                            fontWeight: Pallet.heading2wgt,
                            color: Pallet.fontcolornew))),
                DataColumn(
                    label: MyText(
                        text: pageDetails["text5"] == null
                            ? ''
                            : pageDetails["text5"],
                        style: TextStyle(
                            fontSize: Pallet.heading3,
                            fontWeight: Pallet.heading2wgt,
                            color: Pallet.fontcolornew))),
                // DataColumn(
                //     label: MyText(
                //         pageDetails["text10"] == null
                //             ? ''
                //             : pageDetails["text10"],
                //         style: TextStyle(
                //             fontSize: Pallet.heading3,
                //             fontWeight: Pallet.heading2wgt,
                //             color: Pallet.fontcolornew))),
                DataColumn(
                    label: MyText(
                        text: pageDetails["text11"] == null
                            ? ''
                            : pageDetails["text11"],
                        style: TextStyle(
                            fontSize: Pallet.heading3,
                            fontWeight: Pallet.heading2wgt,
                            color: Pallet.fontcolornew))),
              ],
              source: _DataSource(context),
            ),
          ),
        ],
      ),
    );
  }
}

class _Row {
  _Row(
    this.transType,
    this.transDate,
    this.transPart,
    this.transAmount,
    this.transStatus,
    // this.transRemarks,
    this.transNaration,
  );

  final String transType;
  final String transDate;
  final String transPart;
  final String transAmount;
  final String transStatus;
  // final String transRemarks;
  final String transNaration;

  bool selected = false;
}

class _DataSource extends DataTableSource {
  var localCasting = {
    "debit": "Debit",
    "credit": "Credit",
    "points": "Points",
    "success": "Success",
    "pending": "Pending",
    "failed": "Failed",
    "vouchers": "Vouchers",
    "tokens": "Tokens",
    "cash": "Cash",
    "dsv": "DSV",
  };
  _DataSource(this.context) {
    _rows = <_Row>[
      for (var trans in accounts.accountTrans)
        _Row(
          localCasting[trans["trans_mode"].toString()] == null
              ? null
              : localCasting[trans["trans_mode"].toString()],
          trans["updated_at"].toString(),
          trans["trans_name"].toString(),
          trans["amount"].toString(),
          trans["trans_status"].toString() == null
              ? null
              : localCasting[trans["trans_status"].toString()],
          // trans["remarks"].toString() == 'null'
          //     ? ''
          //     : trans["remarks"].toString(),
          trans["naration"].toString(),
        ),
    ];
  }

  final BuildContext context;
  List<_Row> _rows;

  int _selectedCount = 0;

  @override
  DataRow getRow(int index) {
    assert(index >= 0);
    if (index >= _rows.length) return null;
    final row = _rows[index];
    return DataRow.byIndex(
      index: index,
      cells: [
        DataCell(Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            row.transType.toString() == 'Debit'
                ? MyText(
                    text: row.transType.toString(),
                    style: TextStyle(color: Colors.red))
                : MyText(
                    text: row.transType.toString(),
                    style: TextStyle(color: Colors.green)),
            MyText(
                text: row.transDate,
                style: TextStyle(
                    color: Pallet.fontcolornew,
                    fontSize: Pallet.heading3,
                    fontWeight: Pallet.font500))
          ],
        )),
        DataCell(MyText(
            text: row.transPart.toString() == null
                ? ""
                : row.transPart.toString(),
            style: TextStyle(
                color: Pallet.fontcolornew,
                fontSize: Pallet.heading3,
                fontWeight: Pallet.font500))),
        DataCell(
          row.transType.toString() == 'Debit'
              ? MyText(
                  text: row.transAmount.toString(),
                  style: TextStyle(
                      color: Colors.red,
                      fontSize: Pallet.heading3,
                      fontWeight: Pallet.font500),
                )
              : MyText(
                  text: row.transAmount.toString(),
                  style: TextStyle(
                      color: Colors.green,
                      fontSize: Pallet.heading3,
                      fontWeight: Pallet.font500),
                ),
        ),
        if (row.transStatus.toString() == "Success")
          DataCell(MyText(
            text: row.transStatus.toString(),
            style: TextStyle(
                color: Colors.green,
                fontSize: Pallet.heading3,
                fontWeight: Pallet.font500),
          ))
        else if (row.transStatus.toString() == "Pending")
          DataCell(MyText(
            text: row.transStatus.toString(),
            style: TextStyle(
                color: Colors.yellow[700],
                fontSize: Pallet.heading3,
                fontWeight: Pallet.font500),
          ))
        else if (row.transStatus.toString() == "Failed")
          DataCell(MyText(
            text: row.transStatus.toString(),
            style: TextStyle(
                color: Colors.red,
                fontSize: Pallet.heading3,
                fontWeight: Pallet.font500),
          )),
        // DataCell(MyText(
        //   row.transRemarks.toString(),
        //   style: TextStyle(
        //       color: Pallet.fontcolornew,
        //       fontSize: Pallet.heading3,
        //       fontWeight: Pallet.font500),
        // )),
        DataCell(SelectableText(
          row.transNaration.toString(),
          style: TextStyle(
              color: Pallet.fontcolornew,
              fontSize: Pallet.heading3,
              fontWeight: Pallet.font500),
        )),
      ],
    );
  }

  @override
  int get rowCount => _rows.length;

  @override
  bool get isRowCountApproximate => false;

  @override
  int get selectedRowCount => _selectedCount;
}

// ignore: must_be_immutable
class Accountpopup extends StatelessWidget {
  Map<String, dynamic> pageDetails = {};

  void setPageDetails() async {
    String data = await Utils.getPageDetails('account');
    pageDetails = Utils.fromJSONString(data);
  }

  Accountpopup({Key key, this.wdgtWidth, this.wdgtHeight, this.accountCsvurl})
      : super(key: key) {
    setPageDetails();
  }
  final double wdgtWidth, wdgtHeight;
  final String accountCsvurl;

  @override
  Widget build(BuildContext context) {
    return dashboard.accountType == 2
        ? PopupMenuButton(
            color: Pallet.dashcontainerback,
            onSelected: (result) {
              if (result == 0) {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => Home(route: 'add_funding')),
                );
              }
              if (result == 1) {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => Home(route: 'request_payout')),
                );
              }
              // if (result == 2) {
              //   launch("https://www.google.com/");
              // }
            },
            itemBuilder: (BuildContext context) {
              return <PopupMenuEntry>[
                PopupMenuItem(
                  value: 0,
                  child: Row(
                    children: [
                      Image.asset(
                        'digitalasset.png',
                        width: 25.0,
                        color: Pallet.fontcolor,
                      ),
                      SizedBox(width: 10.0),
                      MyText(
                        text: pageDetails["text6"],
                        style:
                            TextStyle(color: Pallet.fontcolor, fontSize: 18.0),
                      ),
                    ],
                  ),
                ),

                PopupMenuItem(
                  value: 1,
                  child: Row(
                    children: [
                      Image.asset(
                        'payout.png',
                        width: 25.0,
                        color: Pallet.fontcolor,
                      ),
                      SizedBox(width: 10.0),
                      MyText(
                        text: pageDetails["text7"],
                        style:
                            TextStyle(color: Pallet.fontcolor, fontSize: 18.0),
                      ),
                    ],
                  ),
                )

                // PopupMenuItem(
                //   value: 2,
                //   child: Row(
                //     children: [
                //       Image.asset(
                //         'csv.png',
                //         width: 25.0,
                //         color: Pallet.fontcolor,
                //       ),
                //       SizedBox(width: 10.0),
                //       MyText(
                //         pageDetails["text8"],
                //         style: TextStyle(color: Pallet.fontcolor, fontSize: 18.0),
                //       ),
                //     ],
                //   ),
                // ),
              ];
            },
          )
        : Container();
  }
}
