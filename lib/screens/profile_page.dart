import 'dart:convert';

import 'package:centurion/services/business/user.dart';
import 'package:centurion/services/communication/http/index.dart';
import 'package:centurion/utils/utils.dart';

import '../animations/drop.dart';
import '../animations/dropdown.dart';
import '../common.dart';
import '../services/business/cart.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import '../config.dart';
// import '../common.dart';
import 'package:flutter/services.dart';

import '../home.dart';

class Profile extends StatefulWidget {
  Profile({
    Key key,
    this.wdgtWidth,
    this.wdgtHeight,
  }) : super(key: key);
  final double wdgtWidth, wdgtHeight;
  @override
  _ProfileState createState() => _ProfileState();
}

class _ProfileState extends State<Profile> {
  String company;
  String dateOfIncorp;
  String taxNumber;
  String fieldOfBuisness;
  String userName;
  String referral;
  String emailID;
  String addressLine1;
  String addressLine2;
  var phoneNumber;
  String stateString;
  String country;
  var zipCode;
  String cityString;
  String emailString;

  int index;
  bool isContactEdit = false;

  bool isAccountEdit = false;
  bool isShareHoldEdit = false;
  List<dynamic> shareholders = [];
  GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  bool isMobile = false;
  FocusNode submit = FocusNode();

  FocusNode companyName = FocusNode();
  String companyError;
  TextEditingController companyController = TextEditingController();

  FocusNode taxnumber = FocusNode();
  String taxnumberError;
  TextEditingController taxnumController = TextEditingController();

  String dateofIncorpError;
  FocusNode dateofIncorp = FocusNode();
  TextEditingController dateofIncorpcontroller = TextEditingController();

  String fieldofbuisnessError;
  FocusNode fieldofbuisness = FocusNode();
  TextEditingController fieldofbuisnesscontroller = TextEditingController();

  String firstNameError;
  FocusNode firstName = FocusNode();
  TextEditingController firstNamecontroller = TextEditingController();

  String lastNameError;
  FocusNode lastName = FocusNode();
  TextEditingController lastNamecontroller = TextEditingController();

  String nationalityError;
  FocusNode nationality = FocusNode();
  TextEditingController nationalitycontroller = TextEditingController();
  String dropdownnationality;
  String shareError;
  FocusNode share = FocusNode();
  TextEditingController sharecontroller = TextEditingController();
  double val, val1;
  DateTime _date = DateTime.now();
  // final adult = DateTime(DateTime.now().year - 18);
  final currentyear = DateTime(DateTime.now().year);
  final int age = 18;
  _handleDatePicker() async {
    final DateFormat _dateFormatter = DateFormat('MMM dd, yyyy');
    @override
    final DateTime date = await showDatePicker(
      context: context,
      initialDate: _date,
      firstDate: DateTime(1900),
      lastDate: DateTime(currentyear.year + 1),
    );
    if (date != null && date != _date) {
      setState(() {
        _date = date;
      });
      if (currentyear.year - date.year < 18) {
        setState(() {
          dateofIncorpError = 'Age Must Be Greater than 18';
        });
      } else {
        dateofIncorpError = null;
      }
      // DateTime test = _dateFormatter.parse(date.toString());
      dateofIncorpcontroller.text = _dateFormatter.format(date);
    }
  }

  String addressline1Error;
  FocusNode addressline1 = FocusNode();
  TextEditingController addressline1controller = TextEditingController();

  String addressline2Error;
  FocusNode addressline2 = FocusNode();
  TextEditingController addressline2controller = TextEditingController();

  String mobileError;
  FocusNode mobile = FocusNode();
  TextEditingController mobilecontroller = TextEditingController();

  String cityError;
  FocusNode city = FocusNode();
  TextEditingController citycontroller = TextEditingController();

  String zipError;
  FocusNode zip = FocusNode();
  TextEditingController zipcontroller = TextEditingController();

  String emailError;
  FocusNode email = FocusNode();
  TextEditingController emailcontroller = TextEditingController();

  String stateError;
  FocusNode state = FocusNode();
  TextEditingController statecontroller = TextEditingController();

  String countryError;
  bool update = false;
  bool delete = false;

  bool isRequired = false;
  bool isRequired2 = false;
  String accid;
  bool enablecity = false;
  bool enablezipcode = false;
  String dropDownCountry = '';
  String dropDownCity = '';
  String dropDownZip = '';
  Map<String, dynamic> getRegionsMap = {
    'system_product_id': 1,
    'region_level': 2,
    'parent_region': 'null'
  };
  Future<String> temp;
  shareHolder(
    Map<String, dynamic> data,
  ) async {
    print('SSSSSSSSSSSSSSSSSSSSS');
    print(data);
    Map<String, dynamic> result =
        await HttpRequest.Post('shareHolders', Utils.constructPayload(data));
    if (Utils.isServerError(result)) {
      print('eeeeeeeeeeeeeee');
      print(result['response']['error']);
      return snack.snack(title: result['response']['error']);
    } else {
      setState(() {
        delete = true;
        update = true;
      });
      print('wwwwwwwwwwwwwwww');
      print(result['response']['data']);
      return Utils.toJSONString(
          await Utils.convertMessage(result['response']['data']));
    }
  }

  onSaveCorporate(
    Map<String, dynamic> data,
  ) async {
    Map<String, dynamic> result =
        await HttpRequest.Post('onSaveCorporate', Utils.constructPayload(data));
    if (Utils.isServerError(result)) {
      print('IIIIIIIIIIIIIIII');
      print(result['response']['error']);
      print('IIIIIIIIIIIIIIII');

      // throw (await Utils.getMessage(result['response']['error']));
      return snack.snack(title: result['response']['error']);
    } else {
      print('eeeeeeeeeeeeeee');
      print(result['response']['data']);
      setState(() {
        company = companyController.text.trim();
        taxNumber = taxnumController.text.trim();
        dateOfIncorp = dateofIncorpcontroller.text.trim();
        fieldOfBuisness = fieldofbuisnesscontroller.text.trim();
        isAccountEdit = false;
        addressLine1 = addressline1controller.text.trim();
        addressLine2 = addressline2controller.text.trim();
        phoneNumber = mobilecontroller.text.trim();
        stateString = statecontroller.text.trim();
        country = dropDownCountry;
        zipCode = zipcontroller.text.trim();
        cityString = citycontroller.text.trim();
        // emailString = emailcontroller.text.trim();

        isContactEdit = false;
      });

      return Utils.toJSONString(
          await Utils.convertMessage(result['response']['data']));
    }
  }

  Future<String> corporate(Map<String, dynamic> data) async {
    Map<String, dynamic> result =
        await HttpRequest.Post('corporate', Utils.constructPayload(data));
    if (Utils.isServerError(result)) {
      print('************');
      print('error r r r r ' + result['response']['error']);
      throw (await Utils.getMessage(result['response']['error']));
    } else {
      print('*****------******');
      print(result['response']['data']);
      shareholders = result['response']['data']['share_holdes'] == null
          ? '-'
          : result['response']['data']['share_holdes'];
      emailID =
          result['response']['data']['company_details'][0]['email_id'] == null
              ? '-'
              : result['response']['data']['company_details'][0]['email_id'];
      addressLine1 = result['response']['data']['company_details'][0]
                  ['address_line1'] ==
              null
          ? '-'
          : result['response']['data']['company_details'][0]['address_line1'];
      addressLine2 = result['response']['data']['company_details'][0]
                  ['address_line2'] ==
              null
          ? '-'
          : result['response']['data']['company_details'][0]['address_line2'];
      phoneNumber =
          result['response']['data']['company_details'][0]['phone_no'] == null
              ? '-'
              : result['response']['data']['company_details'][0]['phone_no'];
      stateString =
          result['response']['data']['company_details'][0]['region'] == null
              ? '-'
              : result['response']['data']['company_details'][0]['region'];
      country =
          result['response']['data']['company_details'][0]['country'] == null
              ? '-'
              : result['response']['data']['company_details'][0]['country'];
      zipCode = result['response']['data']['company_details'][0]
                  ['postal_code'] ==
              null
          ? '-'
          : result['response']['data']['company_details'][0]['postal_code'];
      cityString =
          result['response']['data']['company_details'][0]['city'] == null
              ? '-'
              : result['response']['data']['company_details'][0]['city'];
      emailString = result['response']['data']['company_details'][0]
                  ['alt_email_id'] ==
              null
          ? '-'
          : result['response']['data']['company_details'][0]['alt_email_id'];
      referral = result['response']['data']['company_details'][0]
                  ['referal_link'] ==
              null
          ? '-'
          : result['response']['data']['company_details'][0]['referal_link'];
      dateOfIncorp = result['response']['data']['company_details'][0]
                  ['date_of_incorporation'] ==
              null
          ? '-'
          : result['response']['data']['company_details'][0]
              ['date_of_incorporation'];
      taxNumber =
          result['response']['data']['company_details'][0]['tax_number'] == null
              ? '-'
              : result['response']['data']['company_details'][0]['tax_number'];

      company = result['response']['data']['company_details'][0]['company_name']
          [0]['company_name'];
      fieldOfBuisness = result['response']['data']['company_details'][0]
                  ['field_of_business'] ==
              null
          ? '-'
          : result['response']['data']['company_details'][0]
              ['field_of_business'];
      userName =
          result['response']['data']['company_details'][0]['user_name'] == null
              ? '-'
              : result['response']['data']['company_details'][0]['user_name'];

      print('********************');
      print(shareholders);
      print(userName);
      return 'start';
    }
  }

  void initState() {
    cart.getAddressInfo(setState);
    cart.getRegions(getRegionsMap);
    temp = corporate({
      "account_id": 'null',
      "share_holer": false,
    });

    super.initState();
  }

  Future<String> checkusername(value) async {
    var res = await user.isSponser(value);
    print('RES: $res}');

    if (res.toString() == "Sponsor Not Found") {
      return 'Sponsor Not Found';
    } else {
      var _map = Utils.fromJSONString(res);
      return _map['sponsor_id'].toString();
    }
  }

  List<DropdownMenuItem<String>> countryList = [];

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<Object>(
        future: temp,
        builder: (context, snapshot) {
          if (snapshot.hasError) {
            print(')))))))))))))))))))');
            print(snapshot.hasError);
            return SomethingWentWrongMessage();
          } else if (snapshot.hasData) {
            print('________________________');
            return Container(
              child: LayoutBuilder(
                builder: (context, constraints) {
                  if (constraints.maxWidth <= 600) {
                    isMobile = true;
                    return Padding(
                      padding: EdgeInsets.fromLTRB(Pallet.defaultPadding,
                          Pallet.defaultPadding, Pallet.defaultPadding, 0),
                      child: SingleChildScrollView(
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            notice(
                              width: widget.wdgtWidth,
                            ),
                            Padding(
                              padding: EdgeInsets.only(top: Pallet.leftPadding),
                              child: profile(
                                width: widget.wdgtWidth,
                              ),
                            ),
                            Padding(
                              padding: EdgeInsets.only(top: Pallet.leftPadding),
                              child: accountDetails(
                                width: widget.wdgtWidth,
                              ),
                            ),
                            Padding(
                              padding: EdgeInsets.only(top: Pallet.leftPadding),
                              child: shareHolders(
                                width: widget.wdgtWidth,
                              ),
                            ),
                            Padding(
                              padding: EdgeInsets.only(top: Pallet.leftPadding),
                              child: contact(
                                width: widget.wdgtWidth,
                              ),
                            ),
                            // Padding(
                            //   padding: EdgeInsets.only(top: Pallet.leftPadding),
                            //   child: finalsubmit(
                            //     width: widget.wdgtWidth,
                            //   ),
                            // ),
                            SizedBox(
                              height: 30,
                            ),
                          ],
                        ),
                      ),
                    );
                  } else {
                    return Padding(
                      padding: EdgeInsets.symmetric(
                        horizontal: Pallet.leftPadding,
                      ),
                      child: SingleChildScrollView(
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            notice(
                              width: widget.wdgtWidth * .74,
                            ),
                            Padding(
                              padding: EdgeInsets.only(top: Pallet.leftPadding),
                              child: profile(
                                width: widget.wdgtWidth * .74,
                              ),
                            ),
                            Padding(
                              padding: EdgeInsets.only(top: Pallet.leftPadding),
                              child: accountDetails(
                                width: widget.wdgtWidth * .74,
                              ),
                            ),
                            Padding(
                              padding: EdgeInsets.only(top: Pallet.leftPadding),
                              child: shareHolders(
                                width: widget.wdgtWidth * .74,
                              ),
                            ),
                            Padding(
                              padding: EdgeInsets.only(top: Pallet.leftPadding),
                              child: contact(
                                width: widget.wdgtWidth * .74,
                              ),
                            ),
                            // Padding(
                            //   padding: EdgeInsets.only(top: Pallet.leftPadding),
                            //   child: finalsubmit(
                            //     width: widget.wdgtWidth * .74,
                            //   ),
                            // ),
                            SizedBox(
                              height: 30,
                            ),
                          ],
                        ),
                      ),
                    );
                  }
                },
              ),
            );
          }
          return Loader();
        });
  }

  Widget finalsubmit({double width}) {
    return Container(
      width: width,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          // ignore: deprecated_member_use
          RaisedButton(
              focusNode: submit,
              color: Pallet.fontcolornew,
              child: Center(
                  child: Text('Save',
                      style: TextStyle(
                        color: Pallet.fontcolor,
                        fontWeight: Pallet.font600,
                      ))),
              onPressed: () {
                print('!!!!!!!!!!!!');
                print(shareholders);
                setState(() {
                  print('*****@@@@@@@@@');
                  val = 0;

                  for (var i = 0; i < shareholders.length; i++) {
                    print('222222222222');
                    print(shareholders.length);
                    print(shareholders[i]['shares']);
                    val1 = double.parse(shareholders[i]['shares']);
                    print('UUUUUUUUUUUU');
                    print(val1);
                    if (val == 0) {
                      print('NNNNNNNNNNNNNNNNN');
                      val = 0 + val1;
                      print(val);
                    } else {
                      print('XXXXXXXXXXXXXXX');
                      val = val + val1;
                      print(val);
                    }

                    // print(val);
                  }
                  if (shareholders.length <= 0) {
                    print('_______________________');
                    snack.snack(title: "Please enter shareholders");

                    // ScaffoldMessenger.of(context).showSnackBar(
                    //     SnackBar(content: Text("Please enter shareholders")));
                  } else if (val < 100) {
                    print('---------------');
                    snack.snack(title: "Total Share must be 100");
                    // ScaffoldMessenger.of(context).showSnackBar(
                    //     SnackBar(content: Text("Total Share must be 100")));
                  } else {
                    print('+++++++++++++++');
                    shareHolder(
                        {"account_id": "null", "share_holders": shareholders});
                    // ScaffoldMessenger.of(context).showSnackBar(
                    //     SnackBar(content: Text("Successfully Saved!")));
                    // Navigator.push(
                    //     context,
                    //     MaterialPageRoute(
                    //         builder: (BuildContext context) => Home(
                    //               route: 'corporate_kyc',
                    //             )));
                  }
                });
              }),
        ],
      ),
    );
  }

  Widget notice({double width}) {
    return Padding(
      padding: const EdgeInsets.only(
        top: 5.0,
      ),
      child: Container(
          decoration: BoxDecoration(
            color: Colors.white,
            boxShadow: [
              BoxShadow(
                color: Colors.black12,
                blurRadius: 3,
                spreadRadius: 5,
              )
            ],
            borderRadius: BorderRadius.circular(10),
          ),
          // color: Colors.pink,
          padding: EdgeInsets.all(10),
          width: width,
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Container(
                padding: EdgeInsets.all(Pallet.defaultPadding / 2),
                decoration: BoxDecoration(
                  color: Pallet.fontcolornew,
                  borderRadius: BorderRadius.circular(Pallet.radius),
                ),
                child: Center(
                  child: Icon(
                    Icons.info_outline,
                    color: Pallet.fontcolor,
                    size: 30,
                  ),
                ),
              ),
              Container(
                width: width - 80,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    Text(
                      'Extremely important notice for all Network Members',
                      style: Pallet.profiletext,
                      // textAlign: TextAlign.justify,
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Text(
                      'The profile information you provide on this page must be exactly the same as the corresponding information in the documents you upload for your kyc. If there are any differences at all then ducatus reserves the right to reject your kyc application.',
                      style: TextStyle(
                        color: Pallet.fontcolornew,
                      ),
                      // textAlign: TextAlign.justify,
                    ),
                  ],
                ),
              )
            ],
          )),
    );
  }

  Widget profile({double width}) {
    return Container(
      width: width,
      child: Text(
        'Profile',
        style: TextStyle(
            color: Pallet.fontcolornew,
            fontSize: Pallet.heading1,
            fontWeight: Pallet.bold),
      ),
    );
  }

  Widget contact({double width}) {
    return Padding(
      padding: const EdgeInsets.only(
        right: 8,
        left: 8,
        bottom: 10,
      ),
      child: Container(
        // color: Colors.pink,xx
        padding: EdgeInsets.all(10),
        decoration: BoxDecoration(
          color: Colors.white,
          boxShadow: [
            BoxShadow(
              color: Colors.black12,
              blurRadius: 3,
              spreadRadius: 8,
            )
          ],
          borderRadius: BorderRadius.circular(10),
        ),
        width: width,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  'Contact',
                  style: Pallet.profiletext,
                ),
                InkWell(
                  onTap: () {
                    setState(() {
                      isContactEdit = !isContactEdit;

                      addressline1controller.text = addressLine1;
                      addressline2controller.text = addressLine2;
                      mobilecontroller.text = phoneNumber;
                      zipcontroller.text = zipCode;
                      dropDownCountry = country;
                      emailcontroller.text = emailString;
                      statecontroller.text = stateString;
                      citycontroller.text = cityString;
                    });
                  },
                  child: Text(
                    isContactEdit == false ? "Edit" : "Cancel",
                    style: TextStyle(
                        color: Pallet.fontcolornew,
                        decoration: TextDecoration.underline),
                  ),
                )
              ],
            ),
            SizedBox(
              height: 30,
            ),
            isContactEdit == false ? showContactDetails() : contactForm(width)
          ],
        ),
      ),
    );
  }

  Widget accountDetails({double width}) {
    return Container(
      // color: Colors.pink,
      width: width,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          isAccountEdit == false ? showAccountDetails() : editAccountDetails()
        ],
      ),
    );
  }

  Widget showContactDetails() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: [
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              'Address Line1 *',
              style: TextStyle(
                color: Pallet.fontcolornew,
              ),
            ),
            SizedBox(
              height: 5,
            ),
            Text(
              addressLine1,
              style: TextStyle(
                color: Pallet.fontcolornew,
                fontSize: Pallet.heading3,
              ),
            ),
            SizedBox(
              height: 20,
            ),
            Text(
              'Address Line2 *',
              style: TextStyle(
                color: Pallet.fontcolornew,
              ),
            ),
            SizedBox(
              height: 5,
            ),
            Text(
              addressLine2,
              style: TextStyle(
                color: Pallet.fontcolornew,
                fontSize: Pallet.heading3,
              ),
            ),
            SizedBox(
              height: 20,
            ),
            Text(
              'Mobile Number',
              style: TextStyle(
                color: Pallet.fontcolornew,
              ),
            ),
            SizedBox(
              height: 5,
            ),
            Text(
              phoneNumber,
              style: TextStyle(
                color: Pallet.fontcolornew,
                fontSize: Pallet.heading3,
              ),
            ),
            SizedBox(
              height: 20,
            ),
            Text(
              'Country',
              style: TextStyle(
                color: Pallet.fontcolornew,
              ),
            ),
            SizedBox(
              height: 5,
            ),
            Text(
              country,
              style: TextStyle(
                color: Pallet.fontcolornew,
                fontSize: Pallet.heading3,
              ),
            ),
            isMobile == true
                ? contactDetailsOfShowContactDetails()
                : Container()
          ],
        ),
        isMobile == false ? contactDetailsOfShowContactDetails() : Container()
      ],
    );
  }

  Widget editAccountDetails({double width}) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 8),
      child: Container(
        // width: width - 50,

        padding: EdgeInsets.all(10),
        decoration: BoxDecoration(
          color: Colors.white,
          boxShadow: [
            BoxShadow(
              color: Colors.black12,
              blurRadius: 3,
              spreadRadius: 8,
            )
          ],
          borderRadius: BorderRadius.circular(10),
        ),
        child: Column(
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text('Account', style: Pallet.profiletext),
                InkWell(
                  onTap: () {
                    setState(() {
                      isAccountEdit = !isAccountEdit;
                      companyController.text = company;
                      taxnumController.text = taxNumber;
                      dateofIncorpcontroller.text = dateOfIncorp;
                      fieldofbuisnesscontroller.text = fieldOfBuisness;
                    });
                  },
                  child: Text(
                    isAccountEdit == false ? "Edit" : "Cancel",
                    style: TextStyle(
                        color: Pallet.fontcolornew,
                        decoration: TextDecoration.underline),
                  ),
                )
              ],
            ),
            SizedBox(
              height: 30,
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                Text(
                  'Username',
                  style: Pallet.profiletext,
                ),
                Text(
                  userName,
                  style: Pallet.profiletext,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        SizedBox(
                          height: 20,
                        ),
                        Container(
                          width: 300,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text('Company Name *',
                                  style: TextStyle(
                                      fontSize: Pallet.normalfont,
                                      color: Pallet.fontcolornew,
                                      fontWeight: Pallet.font500)),
                              SizedBox(height: 10),
                              SignupTextBox(
                                  tabpress: (event) {
                                    if (event
                                        .isKeyPressed(LogicalKeyboardKey.tab)) {
                                      FocusScope.of(context)
                                          .requestFocus(taxnumber);
                                    }
                                  },
                                  errorText: companyError,
                                  focusnode: companyName,
                                  onsubmit: () {
                                    FocusScope.of(context)
                                        .requestFocus(taxnumber);
                                  },
                                  isPassword: false,
                                  digitsOnlyPhone: false,
                                  controller: companyController,
                                  validation: (value) {
                                    setState(() {
                                      if (value.trim().isEmpty) {
                                        companyError = 'Required';
                                      } else {
                                        companyError = null;
                                      }
                                    });
                                  }),
                            ],
                          ),
                        ),
                        SizedBox(
                          height: 20,
                        ),
                        Container(
                          width: 300,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text('Tax Number *',
                                  style: TextStyle(
                                      fontSize: Pallet.normalfont,
                                      color: Pallet.fontcolornew,
                                      fontWeight: Pallet.font500)),
                              SizedBox(height: 10),
                              SignupTextBox(
                                  tabpress: (event) {
                                    if (event
                                        .isKeyPressed(LogicalKeyboardKey.tab)) {
                                      FocusScope.of(context)
                                          .requestFocus(dateofIncorp);
                                    }
                                  },
                                  focusnode: taxnumber,
                                  onsubmit: () {
                                    FocusScope.of(context)
                                        .requestFocus(dateofIncorp);
                                  },
                                  errorText: taxnumberError,
                                  isPassword: false,
                                  digitsOnlyPhone: false,
                                  controller: taxnumController,
                                  validation: (value) {
                                    setState(() {
                                      if (value.trim().isEmpty) {
                                        taxnumberError = 'Required';
                                      } else {
                                        taxnumberError = null;
                                      }
                                    });
                                  }),
                            ],
                          ),
                        ),
                        isMobile == true
                            ? detailsOfEditAccountDetails()
                            : Container()
                      ],
                    ),
                    isMobile == false
                        ? detailsOfEditAccountDetails()
                        : Container(),
                  ],
                ),
              ],
            ),
            SizedBox(height: 30),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                // ignore: deprecated_member_use
                RaisedButton(
                  color: Colors.grey[300],
                  child: Text('Cancel',
                      style: TextStyle(
                        color: Pallet.fontcolornew,
                      )),
                  onPressed: () {
                    setState(() {
                      isAccountEdit = false;
                      companyError = null;
                      dateofIncorpError = null;
                      taxnumberError = null;
                      fieldofbuisnessError = null;
                    });
                  },
                ),
                // ignore: deprecated_member_use
                RaisedButton(
                  focusNode: submit,
                  child: Center(
                      child: Text(
                    'Submit',
                    style: TextStyle(color: Pallet.fontcolor),
                  )),
                  onPressed: () {
                    setState(() {
                      if (companyController.text.trim().isEmpty ||
                          taxnumController.text.trim().isEmpty ||
                          dateofIncorpcontroller.text.trim().isEmpty ||
                          fieldofbuisnesscontroller.text.trim().isEmpty) {
                        if (companyController.text.trim().length < 1) {
                          setState(() {
                            companyError = 'Required';
                          });
                        }
                        if (taxnumController.text.trim().length < 1) {
                          setState(() {
                            taxnumberError = 'Required';
                          });
                        }
                        if (dateofIncorpcontroller.text.trim().length < 1) {
                          setState(() {
                            dateofIncorpError = 'Required';
                          });
                        }
                        if (fieldofbuisnesscontroller.text.trim().length < 1) {
                          setState(() {
                            fieldofbuisnessError = 'Required';
                          });
                        }
                      } else {
                        if (companyError == null &&
                            dateofIncorpError == null &&
                            taxnumberError == null &&
                            fieldofbuisnessError == null) {
                          onSaveCorporate({
                            "profile_id": 'null',
                            "account_id": 'null',
                            "company_name": companyController.text.trim(),
                            "tax_number": taxnumController.text.trim(),
                            "user_name": userName,
                            // "last_name": 'null',
                            "date_of_incorporation":
                                dateofIncorpcontroller.text.trim(),
                            "field_of_business":
                                fieldofbuisnesscontroller.text.trim(),
                            "referal_link": referral,
                            "address_1": 'null',
                            "address_2": 'null',
                            "mobile_number_": 'null',
                            "country_": 'null',
                            "state_": 'null',
                            "city_": 'null',
                            "postal_code": 'null',
                            "email_id": 'null',
                            "identity_type": 'null',
                            "date_of_birth": 'null',
                            "nationality_": 'null',
                            "company_certificate": 'null',
                            "tax_certificate": 'null',
                            "is_company": true,
                          });
                          // company = companyController.text.trim();
                          // taxNumber = taxnumController.text.trim();
                          // dateOfIncorp = dateofIncorpcontroller.text.trim();
                          // fieldOfBuisness =
                          //     fieldofbuisnesscontroller.text.trim();
                          // isAccountEdit = false;
                        }
                      }
                    });
                  },
                  color: Pallet.fontcolornew,
                )
              ],
            ),
            SizedBox(height: 30),
          ],
        ),
      ),
    );
  }

  Column detailsOfEditAccountDetails() {
    return Column(
      mainAxisAlignment: MainAxisAlignment.end,
      children: [
        SizedBox(
          height: 20,
        ),
        Container(
          width: 300,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text("Date of incorporation *",
                  style: TextStyle(
                    fontSize: Pallet.normalfont,
                    color: Pallet.fontcolornew,
                    fontWeight: Pallet.font500,
                  )),
              SizedBox(height: 5),
              TextFormField(
                focusNode: dateofIncorp,
                readOnly: true,
                controller: dateofIncorpcontroller,
                onTap: _handleDatePicker,
                textAlign: TextAlign.left,
                onFieldSubmitted: (value) {
                  FocusScope.of(context).requestFocus(fieldofbuisness);
                },
                style: TextStyle(
                    color: Pallet.fontcolornew,
                    fontSize: Pallet.heading3,
                    fontWeight: Pallet.font500),
                cursorColor: Pallet.fontcolornew,
                keyboardType: TextInputType.emailAddress,
                decoration: InputDecoration(
                  errorText: dateofIncorpError,
                  errorStyle: TextStyle(
                    fontSize: Pallet.heading4,
                    color: Pallet.errortxt,
                  ),
                  border: OutlineInputBorder(
                      borderSide: const BorderSide(
                          color: Color(0XFF1034a6), width: 1.0),
                      borderRadius: BorderRadius.circular(8.0)),
                  hintText: "",
                  hintStyle: TextStyle(
                      fontSize: Pallet.heading4,
                      fontWeight: Pallet.font500,
                      color: Pallet.fontcolornew),
                  fillColor: Pallet.fontcolor,
                  focusedBorder: OutlineInputBorder(
                      borderSide: const BorderSide(
                          color: Color(0XFF1034a6), width: 1.0),
                      borderRadius: BorderRadius.circular(8.0)),
                  enabledBorder: OutlineInputBorder(
                      borderSide:
                          BorderSide(color: Color(0xFF4570ae), width: 0.0),
                      borderRadius: BorderRadius.circular(8.0)),
                  filled: true,
                  prefixIcon: Padding(
                      padding: EdgeInsets.only(left: 20, right: 10),
                      child: Icon(Icons.calendar_today,
                          size: 22, color: Pallet.fontcolornew)),
                ),
              ),
            ],
          ),
        ),
        SizedBox(
          height: 20,
        ),
        Container(
          width: 300,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text("Field of business *",
                  style: TextStyle(
                    fontSize: Pallet.normalfont,
                    color: Pallet.fontcolornew,
                    fontWeight: Pallet.font500,
                  )),
              SizedBox(height: 5),
              SignupTextBox(
                  tabpress: (event) {
                    if (event.isKeyPressed(LogicalKeyboardKey.tab)) {
                      FocusScope.of(context).requestFocus(submit);
                    }
                  },
                  focusnode: fieldofbuisness,
                  onsubmit: () {
                    FocusScope.of(context).requestFocus(submit);
                  },
                  errorText: fieldofbuisnessError,
                  isPassword: false,
                  digitsOnlyPhone: false,
                  controller: fieldofbuisnesscontroller,
                  validation: (value) {
                    setState(() {
                      if (value.trim().isEmpty) {
                        fieldofbuisnessError = 'Required';
                      } else {
                        fieldofbuisnessError = null;
                      }
                    });
                  }),
            ],
          ),
        ),
      ],
    );
  }

  Widget showAccountDetails({double width}) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 8),
      child: Container(
        padding: EdgeInsets.symmetric(
          horizontal: 15,
          vertical: 10,
        ),
        decoration: BoxDecoration(
          color: Colors.white,
          boxShadow: [
            BoxShadow(
              color: Colors.black12,
              blurRadius: 3,
              spreadRadius: 8,
            )
          ],
          borderRadius: BorderRadius.circular(10),
        ),
        child: Column(
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text('Account', style: Pallet.profiletext),
                InkWell(
                  onTap: () {
                    setState(() {
                      isAccountEdit = !isAccountEdit;
                      companyController.text = company;
                      taxnumController.text = taxNumber;
                      dateofIncorpcontroller.text = dateOfIncorp;
                      fieldofbuisnesscontroller.text = fieldOfBuisness;
                    });
                  },
                  child: Text(
                    isAccountEdit == false ? "Edit" : "Cancel",
                    style: TextStyle(
                        color: Pallet.fontcolornew,
                        decoration: TextDecoration.underline),
                  ),
                )
              ],
            ),
            SizedBox(
              height: 30,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      'Company Name *',
                      style: TextStyle(
                        color: Pallet.fontcolornew,
                      ),
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    Text(
                      company,
                      style: TextStyle(
                        color: Pallet.fontcolornew,
                        fontWeight: Pallet.font600,
                        fontSize: Pallet.heading3,
                      ),
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    Text(
                      'Tax number *',
                      style: TextStyle(
                        color: Pallet.fontcolornew,
                      ),
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    Text(
                      taxNumber,
                      style: TextStyle(
                        fontWeight: Pallet.font600,
                        color: Pallet.fontcolornew,
                        fontSize: Pallet.heading3,
                      ),
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    Text(
                      'Username',
                      style: TextStyle(
                        color: Pallet.fontcolornew,
                      ),
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    Text(
                      userName,
                      style: TextStyle(
                        fontWeight: Pallet.font600,
                        color: Pallet.fontcolornew,
                        fontSize: Pallet.heading3,
                      ),
                    ),
                    isMobile == true
                        ? accountDetailsOfShowAccountDetails()
                        : Container()
                  ],
                ),
                isMobile == false
                    ? accountDetailsOfShowAccountDetails()
                    : Container()
              ],
            ),
          ],
        ),
      ),
    );
  }

  Widget contactDetailsOfShowContactDetails() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          'State *',
          style: TextStyle(
            color: Pallet.fontcolornew,
          ),
        ),
        SizedBox(
          height: 5,
        ),
        Text(
          stateString,
          style: TextStyle(
            color: Pallet.fontcolornew,
            fontSize: Pallet.heading3,
          ),
        ),
        SizedBox(
          height: 20,
        ),
        Text(
          'City *',
          style: TextStyle(
            color: Pallet.fontcolornew,
          ),
        ),
        SizedBox(
          height: 5,
        ),
        Text(
          cityString,
          style: TextStyle(
            color: Pallet.fontcolornew,
          ),
        ),
        SizedBox(
          height: 20,
        ),
        Text(
          'Zip Code *',
          style: TextStyle(
            color: Pallet.fontcolornew,
          ),
        ),
        SizedBox(
          height: 5,
        ),
        Text(
          zipCode,
          style: TextStyle(
            color: Pallet.fontcolornew,
            fontSize: Pallet.heading3,
          ),
        ),
        SizedBox(
          height: 20,
        ),
        Text(
          'Email *',
          style: TextStyle(
            color: Pallet.fontcolornew,
          ),
        ),
        SizedBox(
          height: 5,
        ),
        Text(
          emailID,
          style: TextStyle(
            color: Pallet.fontcolornew,
            fontSize: Pallet.heading3,
          ),
        ),
      ],
    );
  }

  Widget accountDetailsOfShowAccountDetails() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          'Date of incorporation *',
          style: TextStyle(
            color: Pallet.fontcolornew,
          ),
        ),
        SizedBox(
          height: 5,
        ),
        Text(
          dateOfIncorp,
          style: TextStyle(
            fontWeight: Pallet.font600,
            color: Pallet.fontcolornew,
            fontSize: Pallet.heading3,
          ),
        ),
        SizedBox(
          height: 20,
        ),
        Text(
          'Field of business *',
          style: TextStyle(
            color: Pallet.fontcolornew,
          ),
        ),
        SizedBox(
          height: 5,
        ),
        Text(
          fieldOfBuisness,
          style: TextStyle(
            fontWeight: Pallet.font600,
            color: Pallet.fontcolornew,
            fontSize: Pallet.heading3,
          ),
        ),
        SizedBox(
          height: 20,
        ),
        Text(
          'Referal Link',
          style: TextStyle(
            color: Pallet.fontcolornew,
          ),
        ),
        SizedBox(
          height: 5,
        ),
        Text(
          referral,
          style: TextStyle(
            fontWeight: Pallet.font600,
            color: Pallet.fontcolornew,
            fontSize: Pallet.heading3,
          ),
        ),
      ],
    );
  }

  Widget shareHolders({double width}) {
    return Padding(
      padding: const EdgeInsets.symmetric(
        horizontal: 8,
        vertical: 5,
      ),
      child: Container(
          padding: EdgeInsets.symmetric(
            horizontal: 15,
            vertical: 10,
          ),
          decoration: BoxDecoration(
            color: Colors.white,
            boxShadow: [
              BoxShadow(
                color: Colors.black12,
                blurRadius: 3,
                spreadRadius: 8,
              )
            ],
            borderRadius: BorderRadius.circular(10),
          ),
          width: width,
          child: Column(
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text('Shareholders', style: Pallet.profiletext),
                  isShareHoldEdit == false
                      ? Row(
                          children: [
                            InkWell(
                              onTap: () {
                                setState(() {
                                  val = null;

                                  if (shareholders.length < 10) {
                                    firstNamecontroller.text = '';
                                    lastNamecontroller.text = '';
                                    sharecontroller.text = '';
                                    dropdownnationality = '';
                                    accid = null;

                                    for (var i = 0;
                                        i < shareholders.length;
                                        i++) {
                                      val1 = double.parse(
                                          shareholders[i]['shares']);
                                      if (val == null) {
                                        val = 0 + val1;
                                      } else {
                                        val = val + val1;
                                      }

                                      // print(val);
                                    }

                                    if (val == null || val < 100) {
                                      isShareHoldEdit = true;
                                    } else {
                                      print('ppppp');
                                      print(val);
                                    }
                                  } else {
                                    ScaffoldMessenger.of(context)
                                        .showSnackBar(SnackBar(
                                      content:
                                          Text('Only 10 share holders allow'),
                                    ));
                                  }
                                });
                              },
                              child: Text(
                                "Add",
                                style: TextStyle(
                                    color: Pallet.fontcolornew,
                                    decoration: TextDecoration.underline),
                              ),
                            ),
                          ],
                        )
                      : Row(
                          children: [
                            // InkWell(
                            //   onTap: () {
                            //     setState(() {
                            //       isShareHoldEdit = false;
                            //     });
                            //   },
                            //   child: Text(
                            //     "Add",
                            //     style: TextStyle(
                            //         color: Pallet.fontcolornew,
                            //         decoration: TextDecoration.underline),
                            //   ),
                            // ),
                            // SizedBox(width: 10),
                            InkWell(
                              onTap: () {
                                setState(() {
                                  isShareHoldEdit = false;
                                });
                              },
                              child: Text(
                                "Cancel",
                                style: TextStyle(
                                    color: Pallet.fontcolornew,
                                    decoration: TextDecoration.underline),
                              ),
                            ),
                          ],
                        )
                ],
              ),
              SizedBox(height: 30),
              isShareHoldEdit == false
                  ? Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10),
                      ),
                      width: width,
                      child: Column(
                        children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text(
                                'Name',
                                style: Pallet.profiletext,
                              ),
                              Text(
                                'Shares',
                                style: Pallet.profiletext,
                              ),
                              Text(
                                'Nationality',
                                style: Pallet.profiletext,
                              ),
                              Text(
                                'Actions',
                                style: Pallet.profiletext,
                              )
                            ],
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          Divider(),
                          shareholders.length == 0
                              ? SizedBox(
                                  height: 5,
                                )
                              : SizedBox(
                                  height: 10,
                                ),
                          for (var i = 0; i < shareholders.length; i++)
                            Column(
                              children: [
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Container(
                                      // width: 50,
                                      child: Text(
                                        shareholders[i]['first_name'] +
                                            ' ' +
                                            shareholders[i]['last_name'],
                                        // overflow: TextOverflow.ellipsis,
                                        style: Pallet.profiletextsubheading,
                                      ),
                                    ),
                                    Text(
                                      shareholders[i]['shares'] + "%",
                                      // overflow: TextOverflow.ellipsis,
                                      style: Pallet.profiletextsubheading,
                                    ),
                                    Text(
                                      shareholders[i]['nationality'] == null
                                          ? '-'
                                          : shareholders[i]['nationality'],
                                      overflow: TextOverflow.ellipsis,
                                      style: Pallet.profiletextsubheading,
                                    ),
                                    Row(
                                      children: [
                                        IconButton(
                                          icon: Icon(Icons.delete),
                                          onPressed: () {
                                            setState(() {
                                              shareHolder({
                                                "account_id": "null",
                                                "share_holders": [
                                                  {
                                                    'share_holder_id':
                                                        shareholders[i][
                                                                'share_holder_id']
                                                            .toString(),
                                                    'first_name':
                                                        shareholders[i]
                                                            ['first_name'],
                                                    'last_name': shareholders[i]
                                                        ['last_name'],
                                                    'shares': shareholders[i]
                                                        ['shares'],
                                                    'nationality':
                                                        shareholders[i]
                                                            ['nationality'],
                                                    'delete_flag': true
                                                  }
                                                ]
                                              });
                                              delete == true
                                                  ? shareholders.removeAt(i)
                                                  : snack.snack(
                                                      title:
                                                          'Data not deleted');
                                              print('^^^^^^^^^^^^^^^^^^');
                                              print(shareholders[i]
                                                  ['share_holder_id']);
                                            });
                                          },
                                        ),
                                        SizedBox(
                                          width: 10,
                                        ),
                                        IconButton(
                                          icon: Icon(Icons.edit),
                                          onPressed: () {
                                            setState(() {
                                              shareHolder({
                                                "account_id": "null",
                                                "share_holders": [
                                                  {
                                                    'share_holder_id':
                                                        shareholders[i][
                                                                'share_holder_id']
                                                            .toString(),
                                                    'first_name':
                                                        shareholders[i]
                                                            ['first_name'],
                                                    'last_name': shareholders[i]
                                                        ['last_name'],
                                                    'shares': shareholders[i]
                                                        ['shares'],
                                                    'nationality':
                                                        shareholders[i]
                                                            ['nationality'],
                                                    'delete_flag': false
                                                  }
                                                ]
                                              });
                                              index = i;
                                              isShareHoldEdit = true;
                                              accid = shareholders[i]
                                                  ['share_holder_id'];
                                              firstNamecontroller.text =
                                                  shareholders[i]['first_name'];
                                              lastNamecontroller.text =
                                                  shareholders[i]['last_name'];
                                              sharecontroller.text =
                                                  shareholders[i]['shares'];
                                              dropdownnationality =
                                                  shareholders[i]
                                                      ['nationality'];
                                            });
                                            print('UUUUU');
                                            print(accid);
                                          },
                                        )
                                      ],
                                    ),
                                  ],
                                ),
                                Divider(),
                              ],
                            ),
                          shareholders.length != 0
                              ? SizedBox(
                                  height: 30,
                                )
                              : Container(),
                          shareholders.length != 0
                              ? Padding(
                                  padding:
                                      EdgeInsets.only(top: Pallet.leftPadding),
                                  child: finalsubmit(
                                    width: isMobile == true
                                        ? widget.wdgtWidth
                                        : widget.wdgtWidth * .74,
                                  ),
                                )
                              : Container(),
                        ],
                      ))
                  : Container(
                      width: width,
                      child: Form(
                        key: _formKey,
                        child: Column(
                          children: [
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              children: [
                                Column(
                                  children: [
                                    Container(
                                      width: 300,
                                      child: Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          Text("User Name *",
                                              style: TextStyle(
                                                  fontSize: Pallet.normalfont,
                                                  color: Pallet.fontcolornew,
                                                  fontWeight: Pallet.font500)),
                                          SizedBox(height: 5),
                                          SignupTextBox(
                                              // onFieldSubmitted:
                                              //     validatesharevalues(),
                                              errorText: firstNameError,
                                              isPassword: false,
                                              digitsOnlyPhone: false,
                                              controller: firstNamecontroller,
                                              validation: (value) {
                                                setState(() {
                                                  if (value.trim().isEmpty) {
                                                    firstNameError = 'Required';
                                                  } else if (value
                                                          .toString()
                                                          .trim()
                                                          .length <
                                                      6) {
                                                    setState(() {
                                                      firstNameError =
                                                          'User not found';
                                                    });
                                                  } else {
                                                    checkusername(value)
                                                        .then((value) {
                                                      if (value ==
                                                          'Sponsor Not Found') {
                                                        setState(() {
                                                          firstNameError =
                                                              'User not found';
                                                        });
                                                      } else {
                                                        setState(() {
                                                          accid = value;
                                                          firstNameError = null;
                                                        });
                                                      }
                                                    });
                                                  }
                                                  if (lastNamecontroller.text
                                                          .trim() !=
                                                      value) {
                                                    lastNameError = null;
                                                  }
                                                });
                                              }),
                                        ],
                                      ),
                                    ),
                                    SizedBox(
                                      height: 20,
                                    ),
                                    Container(
                                      width: 300,
                                      child: Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          Text("Last Name *",
                                              style: TextStyle(
                                                  fontSize: Pallet.normalfont,
                                                  color: Pallet.fontcolornew,
                                                  fontWeight: Pallet.font500)),
                                          SizedBox(height: 5),
                                          SignupTextBox(
                                              // onFieldSubmitted:
                                              //     validatesharevalues(),
                                              errorText: lastNameError,
                                              isPassword: false,
                                              digitsOnlyPhone: false,
                                              controller: lastNamecontroller,
                                              validation: (value) {
                                                setState(() {
                                                  if (value.trim().isEmpty) {
                                                    lastNameError = 'Required';
                                                  } else if (firstNamecontroller
                                                          .text ==
                                                      value) {
                                                    lastNameError =
                                                        "First And Last Names Should Not Be Equal";
                                                  } else {
                                                    lastNameError = null;
                                                  }
                                                });
                                              }),
                                        ],
                                      ),
                                    ),
                                    isMobile == true
                                        ? SizedBox(
                                            height: 20,
                                          )
                                        : SizedBox(),
                                    isMobile == true
                                        ? shareHolderForm()
                                        : Container()
                                  ],
                                ),
                                isMobile == false
                                    ? shareHolderForm()
                                    : Container(),
                              ],
                            ),
                            SizedBox(
                              height: 20,
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              children: [
                                // ignore: deprecated_member_use
                                RaisedButton(
                                  color: Colors.grey[300],
                                  child: Text('Cancel',
                                      style: TextStyle(
                                          color: Pallet.fontcolornew)),
                                  onPressed: () {
                                    setState(() {
                                      isShareHoldEdit = false;
                                      firstNamecontroller.text = null;
                                      lastNamecontroller.text = null;
                                      sharecontroller.text = null;
                                      dropdownnationality = null;
                                      firstNameError = null;
                                      lastNameError = null;
                                      nationalityError = null;
                                      shareError = null;
                                      accid = null;
                                    });
                                  },
                                ),
                                // ignore: deprecated_member_use
                                RaisedButton(
                                  color: Pallet.fontcolornew,
                                  child: Text('Submit',
                                      style:
                                          TextStyle(color: Pallet.fontcolor)),
                                  onPressed: () async {
                                    print(
                                        'submitsubmitsubmitsubmitsubmitsubmitsubmit');
                                    await validatesharevalues();
                                    setState(() {
                                      if (firstNameError == null &&
                                          lastNameError == null &&
                                          nationalityError == null &&
                                          shareError == null) {
                                        print(
                                            '_________________________________________________________');
                                        isShareHoldEdit = false;
                                        Map<String, dynamic> shareHoldrMap = {
                                          'share_holder_id': 'null',
                                          'first_name':
                                              firstNamecontroller.text.trim(),
                                          'last_name':
                                              lastNamecontroller.text.trim(),
                                          'shares': sharecontroller.text.trim(),
                                          'nationality': dropdownnationality,
                                          'delete_flag': false
                                        };

                                        if (index != null) {
                                          shareholders[index]
                                              ['share_holder_id'] = 'null';
                                          shareholders[index]['first_name'] =
                                              firstNamecontroller.text;
                                          shareholders[index]['last_name'] =
                                              lastNamecontroller.text;
                                          shareholders[index]['shares'] =
                                              sharecontroller.text;
                                          shareholders[index]['nationality'] =
                                              dropdownnationality;

                                          index = null;
                                        } else {
                                          shareholders.add(shareHoldrMap);
                                          firstNamecontroller.text = null;
                                          lastNamecontroller.text = null;
                                          sharecontroller.text = null;
                                          dropdownnationality = null;
                                          firstNameError = null;
                                          lastNameError = null;
                                          accid = null;
                                          nationalityError = null;
                                          shareError = null;
                                        }
                                      } else {
                                        print(firstNameError);
                                        print('e');
                                        print(nationalityError);

                                        print(shareError);

                                        print(
                                            'ddddddddddddddddddddddddddddddddddddddddddddddd');
                                      }
                                    });
                                  },
                                ),
                              ],
                            ),
                            SizedBox(
                              height: 30,
                            ),
                          ],
                        ),
                      ),
                    ),
            ],
          )),
    );
  }

  validatesharevalues() {
    print(
        'validatesharevaluesvalidatesharevaluesvalidatesharevaluesvalidatesharevalues');
    _formKey.currentState.validate();
  }

  Widget shareHolderForm() {
    return Column(
      children: [
        Container(
          width: 300,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text("Nationality *",
                  style: TextStyle(
                      fontSize: Pallet.normalfont,
                      color: Pallet.fontcolornew,
                      fontWeight: Pallet.font500)),
              SizedBox(height: 5),
              DropDownField(
                // hintText: 'Please choose country',
                required: isRequired2,
                textStyle: TextStyle(color: Pallet.fontcolornew),
                value: dropdownnationality,
                onValueChanged: (newValue) {
                  setState(() {
                    dropdownnationality = newValue;
                    cart.getCities(getRegionsMap);
                  });
                },
                items: cart.countries,
              ),
            ],
          ),
        ),
        SizedBox(
          height: 20,
        ),
        Container(
          width: 300,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text("Shares(%) *",
                  style: TextStyle(
                      fontSize: Pallet.normalfont,
                      color: Pallet.fontcolornew,
                      fontWeight: Pallet.font500)),
              SizedBox(height: 5),
              SignupTextBox(
                  doubleonly: true,
                  errorText: shareError,
                  isPassword: false,
                  digitsOnlyPhone: false,
                  controller: sharecontroller,
                  validation: (value) {
                    setState(() {
                      val = null;
                      print('ooooooooooooooo');
                      for (var i = 0; i < shareholders.length; i++) {
                        val1 = double.parse(shareholders[i]['shares']);
                        if (val == null) {
                          print('eeeeeeeeeeeeee');
                          val = 0 + val1;
                          print(val);
                          print("<<<<<<<<<<<<<<<<<<<<<<<<");
                        } else {
                          val = val + val1;
                          print(val);
                          print('>>>>>>>>>>>>>>>>>>>>');
                        }
                      }
                      print('yyyyyyyyyyy');

                      if (value.trim().isEmpty) {
                        shareError = 'Required';
                        print('_______________');
                      } else if (value == '0') {
                        shareError = 'The Shares field should not zero';
                      } else if (shareholders.length >= 1) {
                        if (index != null) {
                          if (val == null) {
                            val = 0;
                            if ((double.parse(value) +
                                    val -
                                    double.parse(
                                        shareholders[index]['shares'])) <
                                100) {
                              print('hhhhhhhhh');

                              shareError = null;
                            } else {
                              print('sssssssssvvvvvvvvvvvvvvvv');

                              shareError = 'Total shares must be 100';
                            }
                          } else if (val != null) {
                            print('hhhhhdddddddddddddhhhh');

                            if (double.parse(value) +
                                    val -
                                    double.parse(
                                        shareholders[index]['shares']) <=
                                100) {
                              print('hhhhhhhhh');

                              shareError = null;
                            } else if ((double.parse(
                                    shareholders[index]['shares']) ==
                                (double.parse(value)))) {
                              print('cccccccccccccccccc');

                              shareError = null;
                            } else {
                              print(value);
                              print(val);
                              print(
                                  double.parse(shareholders[index]['shares']));
                              print('xxxxxxxxxxxvvvvvvvvvvvvvvvv');

                              shareError = 'Total shares must be 100';
                            }
                          }
                        } else if (index == null) {
                          if (val == null) {
                            val = 0;
                            if ((double.parse(value) + val) <= 100) {
                              print('hhhhhhhhh');

                              shareError = null;
                            } else {
                              print('mmmmmmmmmmvvvvvvvvvvvvvvvv');

                              shareError = 'Total shares must be 100';
                            }
                          } else if (val != null) {
                            if (double.parse(value) + val <= 100) {
                              print('hhhhhhhhh');

                              shareError = null;
                            } else {
                              print('nnnnnnnnnnnnnnvvvvvvvvvvvvvvvv');

                              shareError = 'Total shares must be 100';
                            }
                          }
                        }
                      } else if (shareholders.length == 0) {
                        if (index != null) {
                          if (val == null) {
                            val = 0;
                            if ((double.parse(value) +
                                    val -
                                    double.parse(
                                        shareholders[index]['shares'])) <=
                                100) {
                              print('hhhhhhhhh');

                              shareError = null;
                            } else {
                              print('bbbbbbbbbbbvvvvvvvvvvvvvvvv');

                              shareError = 'Total shares must be 100';
                            }
                          } else if (val != null) {
                            if (double.parse(value) +
                                    val -
                                    double.parse(
                                        shareholders[index]['shares']) <=
                                100) {
                              print('hhhhhhhhh');

                              shareError = null;
                            } else {
                              print(
                                  'ccccccccvvvvvvvvvccccccccvvvvvvvvvvvvvvvv');

                              shareError = 'Total shares must be 100';
                            }
                          }
                        } else if (index == null) {
                          if (val == null) {
                            val = 0;
                            if ((double.parse(value) + val) < 100) {
                              print('hhhhhhhhh');

                              shareError = null;
                            } else {
                              print('hhhhhhhvvvvvvvvvvvvvvvv');

                              shareError =
                                  'The Shares field must be 99 or less';
                            }
                          } else if (val != null) {
                            if (double.parse(value) + val < 100) {
                              print('hhhhhhhhh');

                              shareError = null;
                            } else {
                              print('lllllllllllllllllvvvvvvvvvvvvvvvv');

                              shareError = 'Total shares must be 100';
                            }
                          }
                        }
                      } else {
                        print('saddddddaaasassvvvvvvvvvvvvvvvv');

                        shareError = 'Total shares must be 100';
                      }
                    });
                  }),
            ],
          ),
        ),
      ],
    );
  }

  Widget contactForm(width) {
    return Container(

        // width:width,
        child: Column(
      children: [
        Container(
          width: isMobile == true ? width : width * .7,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text('Address Line1',
                  style: TextStyle(
                      fontSize: Pallet.normalfont,
                      color: Pallet.fontcolornew,
                      fontWeight: Pallet.font500)),
              SizedBox(height: 5),
              SignupTextBox(
                  tabpress: (event) {
                    if (event.isKeyPressed(LogicalKeyboardKey.tab)) {
                      FocusScope.of(context).requestFocus(addressline2);
                    }
                  },
                  focusnode: addressline1,
                  onsubmit: () {
                    FocusScope.of(context).requestFocus(addressline2);
                  },
                  errorText: addressline1Error,
                  isPassword: false,
                  digitsOnlyPhone: false,

                  // header: 'llllllllll',
                  // label: ' Addressline1',
                  controller: addressline1controller,
                  validation: (value) {
                    setState(() {
                      if (value.trim().isEmpty) {
                        addressline1Error = 'Required';
                      } else {
                        addressline1Error = null;
                      }
                    });
                  }),
            ],
          ),
        ),
        SizedBox(
          height: 10,
        ),
        Container(
          width: isMobile == true ? width : width * .7,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text('Address Line2',
                  style: TextStyle(
                      fontSize: Pallet.normalfont,
                      color: Pallet.fontcolornew,
                      fontWeight: Pallet.font500)),
              SizedBox(height: 5),
              SignupTextBox(
                  tabpress: (event) {
                    if (event.isKeyPressed(LogicalKeyboardKey.tab)) {
                      FocusScope.of(context).requestFocus(mobile);
                    }
                  },
                  focusnode: addressline2,
                  onsubmit: () {
                    FocusScope.of(context).requestFocus(mobile);
                  },
                  errorText: addressline2Error,
                  isPassword: false,
                  digitsOnlyPhone: false,

                  // header: 'llllllllll',
                  // label: ' Addressline1',
                  controller: addressline2controller,
                  validation: (value) {
                    setState(() {
                      if (value.trim().isEmpty) {
                        addressline2Error = 'Required';
                      } else {
                        addressline2Error = null;
                      }
                    });
                  }),
            ],
          ),
        ),
        SizedBox(
          height: 20,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Column(
              children: [
                Container(
                  width: 300,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text('Phone Number',
                          style: TextStyle(
                              fontSize: Pallet.normalfont,
                              color: Pallet.fontcolornew,
                              fontWeight: Pallet.font500)),
                      SizedBox(height: 5),
                      SignupTextBox(
                          // tabpress: (event) {
                          //   if (event.isKeyPressed(LogicalKeyboardKey.tab)) {
                          //     FocusScope.of(context).requestFocus(countryfocus);
                          //   }
                          // },
                          focusnode: mobile,
                          // onsubmit: () {
                          //   FocusScope.of(context).requestFocus(countryfocus);
                          // },
                          errorText: mobileError,
                          isPassword: false,
                          digitsOnlyPhone: true,
                          // header: 'llllllllll',
                          // label: 'addressLine12',
                          controller: mobilecontroller,
                          validation: (value) {
                            setState(() {
                              if (value.trim().isEmpty) {
                                mobileError = 'Required';
                              } else {
                                mobileError = null;
                              }
                            });
                          }),
                    ],
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
                Container(
                  width: 300,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text('Country',
                          style: TextStyle(
                              fontSize: Pallet.normalfont,
                              color: Pallet.fontcolornew,
                              fontWeight: Pallet.font500)),
                      SizedBox(height: 5),
                      DropDownField(
                          // hintText: 'Please choose country',
                          required: isRequired,
                          textStyle: TextStyle(color: Pallet.fontcolornew),
                          value: dropDownCountry,
                          onValueChanged: (newValue) {
                            setState(() {
                              cart.cities = [];
                              cart.zipCode = [];
                              citycontroller.text = '';
                              zipcontroller.text = '';
                              enablecity = true;
                              dropDownCountry = newValue;
                              getRegionsMap['region_level'] = 4;
                              getRegionsMap['parent_region'] =
                                  dropDownCountry.toUpperCase();

                              cart.getCities(getRegionsMap);
                            });
                          },
                          items: cart.countries),
                    ],
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
                Container(
                  width: 300,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text('Zip Code',
                          style: TextStyle(
                              fontSize: Pallet.normalfont,
                              color: Pallet.fontcolornew,
                              fontWeight: Pallet.font500)),
                      SizedBox(height: 5),
                      Container(
                          // decoration: BoxDecoration(
                          //     border:
                          //         Border.all(color: Pallet.fontcolornew)),
                          child: AbsorbPointer(
                              absorbing: !enablezipcode ? true : false,
                              child: TypeAheadFormField(
                                // ignore: missing_return
                                validator: (val) {
                                  if (val.trim().isEmpty) {}
                                },
                                errortext: zipError,
                                textFieldConfiguration: TextFieldConfiguration(
                                    cursorColor: Pallet.fontcolornew,
                                    style: TextStyle(
                                        color: Pallet.fontcolornew,
                                        fontSize: Pallet.heading3,
                                        fontWeight: Pallet.font500),
                                    controller: zipcontroller,
                                    decoration: InputDecoration(
                                      border: OutlineInputBorder(
                                          borderSide: const BorderSide(
                                              color: Color(0XFF1034a6),
                                              width: 1.0),
                                          borderRadius:
                                              BorderRadius.circular(8.0)),
                                      errorBorder: OutlineInputBorder(
                                          borderSide: BorderSide(
                                              color: Colors.red, width: 2),
                                          borderRadius: BorderRadius.circular(
                                              Pallet.radius)),
                                      focusedErrorBorder: OutlineInputBorder(
                                          borderSide: BorderSide(
                                              color: Colors.red, width: 2),
                                          borderRadius: BorderRadius.circular(
                                              Pallet.radius)),
                                      focusedBorder: OutlineInputBorder(
                                          borderSide: const BorderSide(
                                              color: Color(0XFF1034a6),
                                              width: 1.0),
                                          borderRadius:
                                              BorderRadius.circular(8.0)),
                                      enabledBorder: OutlineInputBorder(
                                          borderSide: BorderSide(
                                              color: Color(0xFF4570ae),
                                              width: 0.0),
                                          borderRadius:
                                              BorderRadius.circular(8.0)),
                                    )),
                                suggestionsCallback: (pattern) {
                                  return cart.zipCode;
                                },

                                itemBuilder: (context, suggestion) {
                                  return ListTile(
                                    title: Text(
                                      suggestion,
                                      style:
                                          TextStyle(color: Pallet.fontcolornew),
                                    ),
                                  );
                                },
                                transitionBuilder:
                                    (context, suggestionsBox, controller) {
                                  return suggestionsBox;
                                },

                                onSuggestionSelected: (suggestion) {
                                  setState(() {
                                    // isRequired = false;
                                    zipError = null;

                                    this.zipcontroller.text = suggestion;
                                  });
                                },
                              )))
                    ],
                  ),
                ),
                isMobile == true ? contactForm2() : Container()
              ],
            ),
            isMobile == false
                ? Column(
                    children: [contactForm2()],
                  )
                : Column(),
          ],
        ),
        SizedBox(
          height: 20,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            // ignore: deprecated_member_use
            RaisedButton(
              onPressed: () {
                setState(() {
                  addressline1Error = null;
                  addressline2Error = null;
                  stateError = null;
                  countryError = null;
                  cityError = null;
                  zipError = null;
                  mobileError = null;
                  isContactEdit = false;
                });
              },
              color: Colors.grey[300],
              child: Text('Cancel',
                  style: TextStyle(
                    color: Pallet.fontcolornew,
                  )),
            ),
            // ignore: deprecated_member_use
            RaisedButton(
                color: Pallet.fontcolornew,
                onPressed: () {
                  isRequired = true;
                  setState(() {
                    if (addressline1controller.text.trim().isEmpty ||
                        addressline2controller.text.trim().isEmpty ||
                        mobilecontroller.text.trim().isEmpty ||
                        zipcontroller.text.trim().isEmpty ||
                        dropDownCountry.isEmpty ||
                        emailcontroller.text.trim().isEmpty ||
                        statecontroller.text.trim().isEmpty ||
                        citycontroller.text.trim().isEmpty) {
                      if (addressline1controller.text.trim().length < 1) {
                        setState(() {
                          addressline1Error = 'Required';
                        });
                      }
                      if (addressline2controller.text.trim().length < 1) {
                        setState(() {
                          addressline2Error = 'Required';
                        });
                      }
                      if (dropDownCountry.length < 1) {
                        setState(() {
                          countryError = 'Required';
                        });
                      }
                      if (mobilecontroller.text.trim().length < 1) {
                        setState(() {
                          mobileError = 'Required';
                        });
                      }
                      if (citycontroller.text.trim().length < 1) {
                        setState(() {
                          cityError = 'Required';
                        });
                      }
                      if (zipcontroller.text.trim().length < 1) {
                        setState(() {
                          zipError = 'Required';
                        });
                      }
                      if (emailcontroller.text.trim().length < 1) {
                        setState(() {
                          emailError = 'Required';
                        });
                      }
                      if (statecontroller.text.trim().length < 1) {
                        setState(() {
                          stateError = 'Required';
                        });
                      }
                    } else {
                      onSaveCorporate({
                        "profile_id": 'null',
                        "account_id": 'null',
                        "company_name": 'null',
                        "tax_number": 'null',
                        "user_name": 'null',
                        "date_of_incorporation": 'null',
                        "field_of_business": 'null',
                        "referal_link": 'null',
                        "address_1": addressline1controller.text.trim(),
                        "address_2": addressline2controller.text.trim(),
                        "mobile_number_": mobilecontroller.text.trim(),
                        "country_": dropDownCountry,
                        "state_": statecontroller.text.trim(),
                        "city_": citycontroller.text.trim(),
                        "postal_code": zipcontroller.text.trim(),
                        "email_id": emailID,
                        "identity_type": 'null',
                        "date_of_birth": 'null',
                        "nationality_": 'null',
                        "company_certificate": 'null',
                        "tax_certificate": 'null',
                        "is_company": true,
                      });
                      // addressLine1 = addressline1controller.text.trim();
                      // addressLine2 = addressline2controller.text.trim();
                      // phoneNumber = mobilecontroller.text.trim();
                      // stateString = statecontroller.text.trim();
                      // country = dropDownCountry;
                      // zipCode = zipcontroller.text.trim();
                      // cityString = citycontroller.text.trim();
                      // emailString = emailcontroller.text.trim();

                      // isContactEdit = false;

                      // isAccountEdit = false;
                    }
                  });
                },
                child: Text(
                  'Submit',
                  style: TextStyle(color: Pallet.fontcolor),
                )),
          ],
        ),
        SizedBox(
          height: 30,
        ),
      ],
    ));
  }

  Widget contactForm2() {
    return Column(
      children: [
        Container(
          width: 300,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text('State',
                  style: TextStyle(
                      fontSize: Pallet.normalfont,
                      color: Pallet.fontcolornew,
                      fontWeight: Pallet.font500)),
              SizedBox(height: 5),
              SignupTextBox(
                  // tabpress: (event) {
                  //   if (event.isKeyPressed(LogicalKeyboardKey.tab)) {
                  //     FocusScope.of(context).requestFocus(countryfocus);
                  //   }
                  // },
                  focusnode: state,
                  // onsubmit: () {
                  //   FocusScope.of(context).requestFocus(countryfocus);
                  // },
                  errorText: stateError,
                  isPassword: false,
                  digitsOnlyPhone: false,
                  // header: 'llllllllll',
                  // label: 'addressLine12',
                  controller: statecontroller,
                  validation: (value) {
                    setState(() {
                      if (value.trim().isEmpty) {
                        stateError = 'Required';
                      } else {
                        stateError = null;
                      }
                    });
                  }),
            ],
          ),
        ),
        SizedBox(height: 10),
        Container(
          width: 300,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text('City',
                  style: TextStyle(
                      fontSize: Pallet.normalfont,
                      color: Pallet.fontcolornew,
                      fontWeight: Pallet.font500)),
              SizedBox(height: 5),
              AbsorbPointer(
                absorbing: !enablecity ? true : false,
                child: TypeAheadFormField(
                  errortext: cityError,
                  textFieldConfiguration: TextFieldConfiguration(
                    cursorColor: Pallet.fontcolornew,
                    style: TextStyle(
                        color: Pallet.fontcolornew,
                        fontSize: Pallet.heading3,
                        fontWeight: Pallet.font500),
                    controller: citycontroller,
                  ),
                  suggestionsCallback: (pattern) {
                    return cart.cities;
                  },
                  itemBuilder: (context, suggestion) {
                    return ListTile(
                      title: Text(
                        suggestion,
                        style: TextStyle(color: Pallet.fontcolornew),
                      ),
                    );
                  },
                  transitionBuilder: (context, suggestionsBox, controller) {
                    return suggestionsBox;
                  },
                  onSuggestionSelected: (suggestion) {
                    setState(() {
                      print('lllllllllllllllllllllllllllll');
                      cart.zipCode.clear();

                      this.citycontroller.text = suggestion;
                      cityError = null;
                      zipcontroller.text = '';
                      enablezipcode = true;
                      // postalcodeError = "Required";
                      getRegionsMap['region_level'] = 5;
                      getRegionsMap['parent_region'] = citycontroller.text;
                      // isRequired = false;

                      // print(isRequired);
                      print(
                          'VVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVV');

                      cart.getZipCode(getRegionsMap);
                    });
                  },
                  // ignore: missing_return
                  onchanged: (value) {
                    if (value.isEmpty) {
                      setState(() {
                        cityError = 'Required';
                      });
                    } else {
                      setState(() {
                        enablezipcode = true;
                        cityError = null;
                      });
                    }
                  },
                  onSaved: (value) {
                    setState(() {
                      if (value.trim().isEmpty) {
                        cityError = 'Required';
                      } else {
                        cityError = null;
                      }
                    });
                  },
                  // ignore: missing_return
                  validator: (value) {
                    if (value.trim().isEmpty) {
                      setState(() {
                        cityError = 'Required';
                      });
                    } else {
                      setState(() {
                        cityError = null;
                      });
                    }
                  },
                ),
              ),
            ],
          ),
        ),
        SizedBox(
          height: 10,
        ),
        Container(
          width: 300,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text('E-Mail',
                  style: TextStyle(
                      fontSize: Pallet.normalfont,
                      color: Pallet.fontcolornew,
                      fontWeight: Pallet.font500)),
              SizedBox(height: 5),
              SignupTextBox(
                  // tabpress: (event) {
                  //   if (event.isKeyPressed(LogicalKeyboardKey.tab)) {
                  //     FocusScope.of(context).requestFocus(countryfocus);
                  //   }
                  // },
                  focusnode: email,
                  // onsubmit: () {
                  //   FocusScope.of(context).requestFocus(countryfocus);
                  // },
                  readonly: true,
                  errorText: emailError,
                  isPassword: false,
                  digitsOnlyPhone: false,
                  // header: 'llllllllll',
                  label: emailID,
                  controller: emailcontroller,
                  validation: (value) {
                    setState(() {
                      if (value.trim().isEmpty) {
                        emailError = 'Required';
                      } else {
                        emailError = null;
                      }
                    });
                  }),
            ],
          ),
        ),
      ],
    );
  }
}
