import 'package:centurion/utils/utils.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:url_launcher/url_launcher.dart';
import '../config.dart';
import '../config/app_settings.dart';
import '../services/business/cart.dart';
import '../common.dart';

class AddFund extends StatefulWidget {
  const AddFund({Key key, this.wdgtWidth, this.wdgtHeight}) : super(key: key);
  final double wdgtWidth, wdgtHeight;
  @override
  _AddFundState createState() => _AddFundState();
}

class _AddFundState extends State<AddFund> {
  Map<String, dynamic> pageDetails = {};
  _AddFundState() {
    setPageDetails();
  }

  String errortext = "";
  void setPageDetails() async {
    String data = await Utils.getPageDetails('addFunding');
    setState(() {
      pageDetails = Utils.fromJSONString(data);
    });
  }

  List<int> dsvList = <int>[50, 100, 250, 500, 1000, 2500];
  int selectedValue = 50;
  bool isMobile;

  bool ismobil;
  bool istab;
  bool cashval = false;
  bool tradeval = false;
  int selectoption = 0;
  List<Map> selectedoptionDeatails = [
    {
      'id': 1,
      'title': 'Global Account Singapore',
    },
    {
      'id': 2,
      'title': 'Network Account GCC',
    },
  ];

  IconData icon = Icons.arrow_drop_down;
  Fund cart = Fund();

  FocusNode amtnode = FocusNode();
  FocusNode notesnode = FocusNode();
  FocusNode continuenode = FocusNode();
  FocusNode converstion = FocusNode();
  Future<String> temp;

  void initState() {
    print('hello-1234');

    // SampleClass().login(
    //   {"username": "testing", "password": "Eg@email.com1", "product_id": 1},
    //   output,
    // );

    temp = cart.getPaymetMethods();

    super.initState();
  }

  output(value) {
    temp = Fund().getPaymetMethods();
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: temp,
      builder: (context, snapshot) {
        if (snapshot.hasError) {
          return SomethingWentWrongMessage();
        } else if (snapshot.hasData) {
          return LayoutBuilder(
            // ignore: missing_return
            builder: (context, constraints) {
              if (constraints.maxWidth <= ScreenSize.iphone) {
                isMobile = true;
                return SingleChildScrollView(
                    child: Column(children: [
                  Padding(
                    padding: EdgeInsets.only(
                        left: Pallet.defaultPadding,
                        top: Pallet.topPadding1,
                        right: Pallet.defaultPadding),
                    child: addfund(ismobil = true, istab = false,
                        wdgtwidth: widget.wdgtWidth,
                        // fontsize: Pallet.normalfont * .75,

                        // cheight: widget.wdgtWidth * .03 + 3,
                        isize: widget.wdgtWidth * .025,
                        boxPerRow: 2),
                  ),
                  Padding(
                    padding: EdgeInsets.all(Pallet.defaultPadding),
                    child: note(
                      wdgtwidth: widget.wdgtWidth,
                    ),
                  ),
                  SizedBox(height: 15),
                  Padding(
                    padding: EdgeInsets.all(Pallet.defaultPadding),
                    child: form(
                      wdgtwidth: widget.wdgtWidth,
                    ),
                  )
                ]));
              } else if (constraints.maxWidth > ScreenSize.iphone &&
                  constraints.maxWidth <= ScreenSize.ipad) {
                isMobile = false;

                return SingleChildScrollView(
                    child: Column(
                  children: [
                    Padding(
                      padding: EdgeInsets.all(Pallet.defaultPadding),
                      child: addfund(ismobil = false, istab = true,
                          wdgtwidth: widget.wdgtWidth,
                          // fontsize: Pallet.mobileheading1 * .85,
                          // cheight: widget.wdgtWidth * .023 + 7,
                          isize: widget.wdgtWidth * .023,
                          boxPerRow: 2),
                    ),
                    Padding(
                      padding: EdgeInsets.all(Pallet.defaultPadding),
                      child: note(
                        wdgtwidth: widget.wdgtWidth,
                      ),
                    ),
                    SizedBox(height: 15),
                    Padding(
                      padding: EdgeInsets.all(Pallet.defaultPadding),
                      child: form(
                        wdgtwidth: widget.wdgtWidth,
                      ),
                    )
                  ],
                ));
              } else {
                isMobile = false;

                return SingleChildScrollView(
                  child: Padding(
                    padding: EdgeInsets.only(
                        left: Pallet.leftPadding, top: Pallet.topPadding1),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Padding(
                          padding:
                              EdgeInsets.only(right: Pallet.defaultPadding),
                          child: addfund(ismobil = false, istab = false,
                              wdgtwidth: widget.wdgtWidth * .7,
                              // fontsize: Pallet.heading3 * .9,
                              // cheight: widget.wdgtWidth * .009 + 6,
                              isize: widget.wdgtWidth * .011,
                              boxPerRow: 3),
                        ),
                        Padding(
                          padding:
                              EdgeInsets.only(right: Pallet.defaultPadding),
                          child: Container(
                            width: widget.wdgtWidth * .8,
                            child: Row(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                form(
                                  wdgtwidth: widget.wdgtWidth * .4,
                                ),
                                note(
                                  wdgtwidth: widget.wdgtWidth * .32,
                                ),
                              ],
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                );
              }
            },
          );
        }
        return Loader();
      },
    );
  }

  Widget addfund(
    bool ismobil,
    bool istab, {
    double wdgtwidth,
    boxPerRow,
    fontsize,
    isize,
  }) {
    return SingleChildScrollView(
      child: Container(
          width: wdgtwidth,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              MyText(
                  text: pageDetails["text1"],
                  style: TextStyle(
                    color: Pallet.fontcolornew,
                    fontSize: Pallet.heading1,
                  )),
              SizedBox(height: 10),
              Container(
                  padding: EdgeInsets.all(Pallet.defaultPadding),
                  width: wdgtwidth,
                  decoration: BoxDecoration(
                      color: Pallet.fontcolor,
                      boxShadow: [Pallet.shadowEffect],
                      // border: Border.all(color: Pallet.dashcontainerback),
                      borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(Pallet.radius),
                          bottomRight: Radius.circular(Pallet.radius))),
                  // height: 200,
                  child: Center(
                    child: Row(
                      // mainAxisAlignment: MainAxisAlignment.spaceAround,
                      // mainAxisAlignment: MainAxisAlignment.start  ,

                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        MyText(
                          text: pageDetails["text22"] + ' ',
                          style: TextStyle(
                              color: Pallet.fontcolornew,
                              fontSize: Pallet.normalfont,
                              fontWeight: FontWeight.bold),
                        ),
                        Container(
                          padding: EdgeInsets.symmetric(
                              horizontal: Pallet.defaultPadding / 6),
                          width: wdgtwidth - 115,
                          child: MyText(
                            text: pageDetails["text21"],
                            style: TextStyle(
                                color: Pallet.dashcontainerback,
                                height: 1.7,
                                fontSize: Pallet.notefont,
                                fontWeight: FontWeight.w400),
                            textAlign: TextAlign.justify,
                          ),
                        )
                      ],
                    ),
                  )),
              SizedBox(height: 20),
              MyText(
                  text: pageDetails["text2"],
                  style: TextStyle(
                    color: Pallet.fontcolornew,
                    fontSize: Pallet.heading3,
                  )),
              SizedBox(height: 20),
              Center(
                child: Wrap(spacing: 15, runSpacing: 10, children: [
                  for (var i = 0; i < cart.paymentGateway.length; i++)

// --------------------------------------start Temporary part-------------------------------------------------------------

                    // InkWell(

                    // -----------------------Temprary----------------------
                    // onTap:
                    //     cart.paymentGateway[i]["name"] == "quantumClearance"
                    //         ? () {}
                    //         :
                    //         () {
                    //             setState(() {
                    //               cart.selectGateway(
                    //                   cart.paymentGateway[i]["name"]);
                    //             });
                    //           },
                    // -----------------------Temprary----------------------
                    // child: cart.paymentGateway[i]["name"] ==
                    //         "quantumClearance"
                    //     ? InkWell(
                    //         onTap: () {
                    //           setState(() {
                    //             cart.selectGateway(
                    //                 cart.paymentGateway[i]["name"]);
                    //           });
                    //           // showDialog(
                    //           //     context: context,
                    //           //     builder: (BuildContext context) {
                    //           //       return StatefulBuilder(
                    //           //           builder: (context, setState) {
                    //           //         return AlertDialog(
                    //           //           title: Row(
                    //           //             children: [
                    //           //               Image.asset(
                    //           //                 "c-logo.png",
                    //           //                 width: 40,
                    //           //                 color:
                    //           //                     Pallet.dashcontainerback,
                    //           //               ),
                    //           //               SizedBox(width: 10),
                    //           //               Expanded(
                    //           //                 child: Text(
                    //           //                     'Scan The QR Code To Initiate Your Payment ',
                    //           //                     style: TextStyle(
                    //           //                       color: Pallet
                    //           //                           .dashcontainerback,
                    //           //                       fontSize:
                    //           //                           Pallet.heading2,
                    //           //                       fontWeight:
                    //           //                           FontWeight.w400,
                    //           //                     )),
                    //           //               ),
                    //           //             ],
                    //           //           ),
                    //           //           content:
                    //           //               singleChildScrollView(setState),
                    //           //           actions: [
                    //           //             CustomButton(
                    //           //               onpress: () {
                    //           //                 print(
                    //           //                     'UUUUUUUUUUUUUUUUUUUUUUUUU');
                    //           //                 setState(() {
                    //           //                   cart.selectGateway(cart
                    //           //                           .paymentGateway[i]
                    //           //                       ["name"] = 'bankwire');
                    //           //                 });
                    //           //                 print(cart.paymentGateway[i]);
                    //           //                 // setState(() {

                    //           //                 // cart.paymentGateway[i]
                    //           //                 //     ["name"] = "bankwire";
                    //           //                 // cart.selectGateway(
                    //           //                 //     cart.paymentGateway[i]["name"]=
                    //           //                 //     'bankwire');
                    //           //                 // print(cart.selectGateway);
                    //           //                 // });

                    //           //                 Navigator.of(context).pop();
                    //           //               },
                    //           //               vpadding: 5,
                    //           //               text: 'Close',
                    //           //               buttoncolor:
                    //           //                   Pallet.fontcolornew,
                    //           //               textcolor: Pallet.fontcolor,
                    //           //               textsize: Pallet.heading3,
                    //           //             ),
                    //           //           ],
                    //           //         );
                    //           //       });
                    //           //     });
                    //         },
                    //         child: Container(
                    //           decoration: BoxDecoration(
                    //               boxShadow: [Pallet.shadowEffect],
                    //               borderRadius:
                    //                   BorderRadius.circular(Pallet.radius),
                    //               color: Pallet.fontcolor),
                    //           width: (ismobil == true)
                    //               ? (wdgtwidth / boxPerRow - 10) * 0.52
                    //               : (istab == true)
                    //                   ? (wdgtwidth / boxPerRow - 10) * 0.42
                    //                   : (wdgtwidth / boxPerRow - 10) * 0.46,
                    //           height: (ismobil == true)
                    //               ? (wdgtwidth / boxPerRow) * 0.35
                    //               : (istab == true)
                    //                   ? (wdgtwidth / boxPerRow) * 0.28
                    //                   : (wdgtwidth / boxPerRow) * 0.33,
                    //           child: Stack(children: [
                    //             if (cart.paymentGateway[i]["logo"] != null)
                    //               Center(
                    //                 child: Column(
                    //                   mainAxisAlignment:
                    //                       MainAxisAlignment.center,
                    //                   crossAxisAlignment:
                    //                       CrossAxisAlignment.center,
                    //                   children: [
                    //                     Container(
                    //                       child: Column(
                    //                         children: [
                    //                           Image.network(
                    //                             appSettings['SERVER_URL'] +
                    //                                 '/' +
                    //                                 cart.paymentGateway[i]
                    //                                     ["logo"],
                    //                             height: ismobil == true
                    //                                 ? wdgtwidth * 0.08
                    //                                 : wdgtwidth * 0.05,
                    //                           ),
                    //                           MyText(
                    //                             text: cart.paymentGateway[i]
                    //                                 ["dname"],
                    //                             style: TextStyle(
                    //                                 color: Pallet
                    //                                     .dashcontainerback,
                    //                                 fontSize: ismobil ==
                    //                                         true
                    //                                     ? Pallet.normalfont /
                    //                                         1.2
                    //                                     : Pallet.normalfont,
                    //                                 fontWeight:
                    //                                     Pallet.bold),
                    //                           ),
                    //                         ],
                    //                       ),
                    //                     ),
                    //                   ],
                    //                 ),
                    //               ),
                    //             if (cart.selectedGeteway ==
                    //                 cart.paymentGateway[i]["name"])
                    //               Positioned(
                    //                 top: 0,
                    //                 right: 1,
                    //                 child: Container(
                    //                     margin: EdgeInsets.only(
                    //                         top:
                    //                             Pallet.defaultPadding / 2.5,
                    //                         right:
                    //                             Pallet.defaultPadding / 2),
                    //                     height: (ismobil == true)
                    //                         ? wdgtwidth * .035
                    //                         : (istab == true)
                    //                             ? wdgtwidth * .025
                    //                             : wdgtwidth * .018,
                    //                     width: (ismobil == true)
                    //                         ? wdgtwidth * .035
                    //                         : (istab == true)
                    //                             ? wdgtwidth * .025
                    //                             : wdgtwidth * .018,
                    //                     //  height: wdgtwidth ,
                    //                     // width: wdgtwidth ,
                    //                     decoration: BoxDecoration(
                    //                       shape: BoxShape.circle,
                    //                       color: Pallet.dashcontainerback,
                    //                     ),
                    //                     child: Center(
                    //                       child: Icon(
                    //                         Icons.done,
                    //                         color: Pallet.fontcolor,
                    //                         size: ismobil == true
                    //                             ? wdgtwidth * .035
                    //                             : wdgtwidth * .015,
                    //                       ),
                    //                     )),
                    //               ),
                    //           ]),
                    //         ),
                    //       )
                    // :
                    InkWell(
                        onTap: () {
                          setState(() {
                            cart.selectGateway(cart.paymentGateway[i]["name"]);
                          });
                        },
                        child: Container(
                          decoration: BoxDecoration(
                              boxShadow: [Pallet.shadowEffect],
                              borderRadius:
                                  BorderRadius.circular(Pallet.radius),
                              color: Pallet.fontcolor),
                          width: (ismobil == true)
                              ? (wdgtwidth / boxPerRow - 10) * 0.52
                              : (istab == true)
                                  ? (wdgtwidth / boxPerRow - 10) * 0.42
                                  : (wdgtwidth / boxPerRow - 10) * 0.46,
                          height: (ismobil == true)
                              ? (wdgtwidth / boxPerRow) * 0.35
                              : (istab == true)
                                  ? (wdgtwidth / boxPerRow) * 0.28
                                  : (wdgtwidth / boxPerRow) * 0.33,
                          child: Stack(children: [
                            if (cart.paymentGateway[i]["logo"] != null)
                              Center(
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    Container(
                                      child: Column(
                                        children: [
                                          Image.network(
                                            appSettings['SERVER_URL'] +
                                                '/' +
                                                cart.paymentGateway[i]["logo"],
                                            height: ismobil == true
                                                ? wdgtwidth * 0.08
                                                : wdgtwidth * 0.05,
                                          ),
                                          MyText(
                                            text: cart.paymentGateway[i]
                                                ["dname"],
                                            style: TextStyle(
                                                color: Pallet.dashcontainerback,
                                                fontSize: ismobil == true
                                                    ? Pallet.normalfont / 1.2
                                                    : Pallet.normalfont,
                                                fontWeight: Pallet.bold),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            if (cart.selectedGeteway ==
                                cart.paymentGateway[i]["name"])
                              Positioned(
                                top: 0,
                                right: 1,
                                child: Container(
                                    margin: EdgeInsets.only(
                                        top: Pallet.defaultPadding / 2.5,
                                        right: Pallet.defaultPadding / 2),
                                    height: (ismobil == true)
                                        ? wdgtwidth * .035
                                        : (istab == true)
                                            ? wdgtwidth * .025
                                            : wdgtwidth * .018,
                                    width: (ismobil == true)
                                        ? wdgtwidth * .035
                                        : (istab == true)
                                            ? wdgtwidth * .025
                                            : wdgtwidth * .018,
                                    //  height: wdgtwidth ,
                                    // width: wdgtwidth ,
                                    decoration: BoxDecoration(
                                      shape: BoxShape.circle,
                                      color: Pallet.dashcontainerback,
                                    ),
                                    child: Center(
                                      child: Icon(
                                        Icons.done,
                                        color: Pallet.fontcolor,
                                        size: ismobil == true
                                            ? wdgtwidth * .035
                                            : wdgtwidth * .015,
                                      ),
                                    )),
                              ),
                          ]),
                        ))
                  // ),

// --------------------------------------End Temporary part-------------------------------------------------------------

                  // child: Stack(
                  //   children: [

                  // Container(
                  //     padding: EdgeInsets.only(
                  //         bottom: Pallet.defaultPadding),
                  //     // width: wdgtwidth * 0.285,
                  //     // height: wdgtwidth * 0.25,
                  // width: (wdgtwidth / boxPerRow - 10) * 0.45,
                  // height: (wdgtwidth / boxPerRow) * 0.35,
                  //     decoration: BoxDecoration(
                  //         boxShadow: [Pallet.shadowEffect],
                  //         // border: cart.selectedGeteway ==
                  //         //         cart.paymentGateway[i]["name"]
                  //         //     ? Border.all(color: Pallet.dashcontainerback)
                  //         //     : Border.all(color: Colors.transparent),
                  //         borderRadius:
                  //             BorderRadius.circular(Pallet.radius),
                  //         color: Pallet.fontcolor),
                  //     child: Column(
                  //         mainAxisAlignment: MainAxisAlignment.start,
                  //         children: [
                  //           Container(
                  //             // color: Colors.pink,
                  //             // padding: EdgeInsets.only(top: 2, right: 2),
                  //             width: wdgtwidth * 0.285,
                  //             height: cheight,
                  //             child: Row(
                  //               mainAxisAlignment:
                  //                   MainAxisAlignment.end,
                  //               children: [
                  //                 if (cart.selectedGeteway ==
                  //                     cart.paymentGateway[i]["name"])
                  // Container(
                  //     margin: EdgeInsets.only(
                  //         top: 5, right: 5),
                  //     height: 25,
                  //     width: 25,
                  //     decoration: BoxDecoration(
                  //       shape: BoxShape.circle,
                  //       color:
                  //           Pallet.dashcontainerback,
                  //     ),
                  //     // backgroundColor: Pallet.fontcolor,
                  //     child: Center(
                  //       child: Icon(
                  //         Icons.done,
                  //         color: Pallet.fontcolor,
                  //         size: isize,
                  //       ),
                  //     )),

                  //                 // SizedBox(width: 10)
                  //               ],
                  //             ),
                  //           ),
                  // if (cart.paymentGateway[i]["logo"] != null)
                  //             Expanded(
                  //               child: Container(
                  //                 child: Center(
                  //                   child: Column(
                  //                       mainAxisAlignment:
                  //                           MainAxisAlignment.center,
                  //                       children: [
                  // Image.network(
                  //   appSettings['SERVER_URL'] +
                  //       '/' +
                  //       cart.paymentGateway[i]
                  //           ["logo"],
                  //   height: wdgtwidth * 0.04,
                  // ),
                  //                         SizedBox(height: 5),
                  // MyText(
                  //   cart.paymentGateway[i]
                  //       ["dname"],
                  //   style: TextStyle(
                  //       color: Pallet
                  //           .dashcontainerback,
                  //       fontSize: fontsize,
                  //       fontWeight:
                  //           Pallet.bold),
                  // ),
                  //                       ]),
                  //                 ),
                  //               ),
                  //             )
                  //         ])),
                  //   ],
                  // ),
                  // ),
                ]),
              ),
              SizedBox(height: 20),
              MyText(
                  text: cart.selectedDisplayName,
                  style: TextStyle(
                    color: Pallet.fontcolornew,
                    fontSize: Pallet.heading3,
                    fontWeight: Pallet.bold,
                  )),
              SizedBox(height: 10),
            ],
          )),
    );
  }

  SingleChildScrollView singleChildScrollView(
      StateSetter setState, double width) {
    return SingleChildScrollView(
      child: Container(
        width: width,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text('Scan The QR Code To Initiate Your Payment ',
                style: TextStyle(
                  color: Pallet.dashcontainerback,
                  fontSize: Pallet.heading3,
                  fontWeight: FontWeight.w400,
                )),
            // SizedBox(height: 10),

            // Text(
            //     'Note : To purchase the predefined amounts of 50, 250, 1000 or 2500 DSV with your credit card, please scan the respective QR codes and proceed following the required steps. Your funds will be credited to your Cash Account once payment is received during office hours 9am-5pm Singapore Time on Mondays-Fridays.',
            //     style: TextStyle(
            //       color: Pallet.dashcontainerback,
            //       fontSize: Pallet.heading4,
            //       fontWeight: FontWeight.w400,
            //     )),
            // SizedBox(height: 10),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                isMobile == false ? Container() : TextDSV(),
                SizedBox(height: 10),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  // spacing: 10,
                  // runSpacing: 10,
                  children: [
                    isMobile == false ? TextDSV() : Text(''),
                    Container(
                      width: 100,
                      decoration: BoxDecoration(
                          border: Border.all(
                              color: Pallet.dashcontainerback, width: 2.0),
                          color: Pallet.fontcolor,
                          borderRadius: BorderRadius.circular(Pallet.radius)),
                      child: DropdownButtonHideUnderline(
                        child: DropdownButton<int>(
                          items:
                              dsvList.map<DropdownMenuItem<int>>((int value) {
                            return DropdownMenuItem<int>(
                              value: value,
                              child: Padding(
                                padding: const EdgeInsets.only(left: 5.0),
                                child: MyText(
                                  text: value,
                                  style: TextStyle(
                                      color: Pallet.fontcolornew,
                                      fontSize: Pallet.heading4,
                                      fontWeight: Pallet.font500),
                                ),
                              ),
                            );
                          }).toList(),
                          value: selectedValue,
                          isExpanded: true,
                          onChanged: (int newValue) {
                            print('ppppppppppppppppppp');
                            setState(() {
                              selectedValue = newValue;
                            });
                            print(newValue);
                            print(selectedValue);
                          },
                        ),
                      ),
                    ),
                  ],
                ),
              ],
            ),
            SizedBox(height: 20),
            Center(
              child: Container(
                decoration: BoxDecoration(
                    color: Colors.grey[350],
                    borderRadius: BorderRadius.circular(Pallet.radius)

                    //  boxShadow: [

                    //   BoxShadow(color: Colors.grey[700], blurRadius: 20)
                    // ]
                    ),
                child: Padding(
                  padding: EdgeInsets.all(Pallet.defaultPadding + 5),
                  child:
                      // Image.network(appSettings['SERVER_URL'] + '/' + '90')
                      Image.network(
                    selectedValue == 50
                        ? appSettings['SERVER_URL'] + '/' + 'paypal/50.png'
                        : selectedValue == 250
                            ? appSettings['SERVER_URL'] + '/' + 'paypal/250.png'
                            : selectedValue == 100
                                ? appSettings['SERVER_URL'] +
                                    '/' +
                                    'paypal/100.png'
                                : selectedValue == 500
                                    ? appSettings['SERVER_URL'] +
                                        '/' +
                                        'paypal/500.png'
                                    : selectedValue == 1000
                                        ? appSettings['SERVER_URL'] +
                                            '/' +
                                            'paypal/1000.png'
                                        : selectedValue == 2500
                                            ? appSettings['SERVER_URL'] +
                                                '/' +
                                                'paypal/2500.png'
                                            : appSettings['SERVER_URL'] +
                                                '/' +
                                                'paypal/50.png',
                    height: 150,
                    width: 150,
                  ),
                ),
              ),
            ),
            SizedBox(height: 20),
            Wrap(
              // runSpacing: 10,
              // spacing: 10,
              children: [
                RichText(
                  // textHeightBehavior:TextHeightBehavior.fromEncoded(encoded) ,
                  text: TextSpan(
                    text:
                        'Once payment is done, please send proof of payment screenshot with your "USERNAME" to ',
                    style: TextStyle(
                      height: 1.5,
                      color: Colors.red,
                      fontSize: Pallet.heading5,
                      fontWeight: FontWeight.w600,
                    ),
                    children: <TextSpan>[
                      TextSpan(
                        text: ' finance@ducatus.net.',
                        style: TextStyle(
                          decoration: TextDecoration.underline,
                          color: Pallet.dashcontainerback,
                          fontSize: Pallet.heading5,
                          fontWeight: FontWeight.bold,
                        ),
                        recognizer: TapGestureRecognizer()
                          ..onTap = () {
                            launch('mailto:finace@ducatus.net');
                          },
                      ),
                    ],
                  ),
                ),
                // Text(
                //     'Once payment is done, please sent proof of payment screenshot with your "USERNAME" to',
                //     style: TextStyle(
                //       color: Colors.red,
                //       fontSize: Pallet.heading4,
                //       fontWeight: FontWeight.bold,
                //     )),
                // InkWell(
                //   onTap: () {
                //     launch('mailto:finace@ducatus.net');
                //   },
                //   child: Text('finance@ducatus.net.',
                // style: TextStyle(
                //   decoration: TextDecoration.underline,
                //   color: Pallet.dashcontainerback,
                //   fontSize: Pallet.heading3,
                //   fontWeight: FontWeight.bold,
                // )),
                // )
              ],
            ),

            // InkWell(
            //   onTap: () {
            //     launch('mailto:finace@ducatus.net');
            //   },
            //   child: Text('finance@ducatus.net.',
            //       style: TextStyle(
            //         decoration: TextDecoration.underline,
            //         color: Pallet.dashcontainerback,
            //         fontSize: Pallet.heading3,
            //         fontWeight: FontWeight.bold,
            //       )),
            // )
          ],
        ),
      ),
    );
  }

  Widget form({double wdgtwidth}) {
    return cart.selectedGeteway == "quantumClearance"
        ? singleChildScrollView(setState, wdgtwidth)
        //  Text('OOOOOOO')
        : Container(
            width: wdgtwidth,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                if (cart.selectedGeteway != "" &&
                    cart.selectedGeteway != "bankwire")
                  ClipRRect(
                    borderRadius: BorderRadius.circular(Pallet.radius),
                    child: Textbox(
                      focusnode: amtnode,
                      autofocus: true,
                      tabpress: (event) {
                        if (event.isKeyPressed(LogicalKeyboardKey.tab)) {
                          FocusScope.of(context).requestFocus(notesnode);
                        }
                      },
                      label: pageDetails["text3"],
                      header: pageDetails["text3"],
                      validation: (value) {
                        print("object-1234 " + value);

                        print(cart.currency_after_selection);
                        print(cart.currencies);
                        if (cart.currency_after_selection.isNotEmpty) {
                          cart.selectCurrency_convertion(
                              cart.currency_after_selection);
                        }
                        if (value.toString().length > 0) {
                          setState(() {
                            cart.amountError = null;
                          });
                        } else {
                          setState(() {
                            cart.amountError = null;
                          });
                        }
                      },
                      controller: cart.amount,
                      errorText: cart.amountError,
                    ),
                  ),

                if (cart.selectedGeteway == "Deutsche Bank Freiburg")
                  Container(
                    width: wdgtwidth,
                    padding: EdgeInsets.all(Pallet.defaultPadding * 2),
                    decoration: BoxDecoration(
                        color: Pallet.fontcolor,
                        boxShadow: [Pallet.shadowEffect],
                        borderRadius: BorderRadius.circular(Pallet.radius)),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        MyText(
                          text: pageDetails["text9"],
                          style: TextStyle(
                              fontSize: Pallet.normalfont,
                              color: Pallet.fontcolornew),
                        ),
                        MyText(
                          text: pageDetails["text25"],
                          style: TextStyle(
                              fontSize: Pallet.heading2,
                              color: Pallet.fontcolornew,
                              fontWeight: Pallet.font500),
                        ),
                        SizedBox(height: 15),
                        MyText(
                            text: pageDetails["text11"],
                            style: TextStyle(
                                fontSize: Pallet.normalfont,
                                color: Pallet.fontcolornew)),
                        MyText(
                          text: pageDetails["text11"],
                          style: TextStyle(
                              fontSize: Pallet.heading2,
                              color: Pallet.fontcolornew,
                              fontWeight: Pallet.font500),
                        ),
                        SizedBox(height: 15),
                        MyText(
                            text: pageDetails["text26"],
                            style: TextStyle(
                                fontSize: Pallet.normalfont,
                                color: Pallet.fontcolornew)),
                        MyText(
                          text: pageDetails["text32"],
                          style: TextStyle(
                              fontSize: Pallet.heading2,
                              color: Pallet.fontcolornew,
                              fontWeight: Pallet.font500),
                        ),
                        SizedBox(height: 15),
                        MyText(
                            text: pageDetails["text27"],
                            style: TextStyle(
                              fontSize: Pallet.normalfont,
                              color: Pallet.fontcolornew,
                            )),
                        MyText(
                          text: pageDetails["text17"],
                          style: TextStyle(
                              fontSize: Pallet.heading2,
                              color: Pallet.fontcolornew,
                              fontWeight: Pallet.font500),
                        ),
                      ],
                    ),
                  ),

                if (cart.selectedGeteway == "bankwire")
                  Container(
                    width: wdgtwidth,
                    padding: EdgeInsets.all(Pallet.defaultPadding * 2),
                    decoration: BoxDecoration(
                        color: Pallet.fontcolor,
                        boxShadow: [Pallet.shadowEffect],
                        // border: Border.all(color: Pallet.dashcontainerback),
                        borderRadius: BorderRadius.circular(Pallet.radius)),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Container(
                          width: wdgtwidth,
                          child: Wrap(
                            spacing: 10,
                            runSpacing: 10,
                            children: [
                              for (var i = 0;
                                  i < selectedoptionDeatails.length;
                                  i++)
                                InkWell(
                                  onTap: () {
                                    setState(() {
                                      selectoption = i;
                                      // print('ONE');
                                      print(selectoption);
                                    });
                                  },
                                  child: Container(
                                    margin: EdgeInsets.only(bottom: 10),
                                    width: wdgtwidth,
                                    height: 50,
                                    padding:
                                        EdgeInsets.all(Pallet.defaultPadding),
                                    decoration: BoxDecoration(
                                        color: selectoption == i
                                            ? Pallet.fontcolornew
                                            : Pallet.fontcolor,
                                        boxShadow: [Pallet.shadowEffect],
                                        // border: Border.all(color: Pallet.dashcontainerback),
                                        borderRadius: BorderRadius.circular(
                                            Pallet.radius)),
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceEvenly,
                                      // crossAxisAlignment: CrossAxisAlignment.start,
                                      children: [
                                        MyText(
                                          text: selectedoptionDeatails[i]
                                              ["title"],
                                          style: TextStyle(
                                              fontSize: Pallet.normalfont,
                                              color: selectoption == i
                                                  ? Pallet.fontcolor
                                                  : Pallet.fontcolornew),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                            ],
                          ),
                        ),
                        SizedBox(height: 10),
                        selectoption == 0
                            ? Container(
                                width: wdgtwidth,
                                padding:
                                    EdgeInsets.all(Pallet.defaultPadding * 2),
                                decoration: BoxDecoration(
                                    color: Pallet.fontcolor,
                                    boxShadow: [Pallet.shadowEffect],
                                    // border: Border.all(color: Pallet.dashcontainerback),
                                    borderRadius:
                                        BorderRadius.circular(Pallet.radius)),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    MyText(
                                      text: pageDetails["text9"],
                                      style: TextStyle(
                                          fontSize: Pallet.normalfont,
                                          color: Pallet.fontcolornew),
                                    ),
                                    MyText(
                                      text: pageDetails["text10"],
                                      style: TextStyle(
                                          fontSize: Pallet.heading2,
                                          color: Pallet.fontcolornew,
                                          fontWeight: Pallet.font500),
                                    ),
                                    SizedBox(height: 15),
                                    MyText(
                                      text: pageDetails["text11"],
                                      style: TextStyle(
                                          fontSize: Pallet.normalfont,
                                          color: Pallet.fontcolornew),
                                    ),
                                    MyText(
                                      text: pageDetails["text12"],
                                      style: TextStyle(
                                          fontSize: Pallet.heading2,
                                          color: Pallet.fontcolornew,
                                          fontWeight: Pallet.font500),
                                    ),
                                    SizedBox(height: 15),
                                    MyText(
                                        text: pageDetails["text32"],
                                        style: TextStyle(
                                            fontSize: Pallet.normalfont,
                                            color: Pallet.fontcolornew)),
                                    MyText(
                                      text: pageDetails["text14"],
                                      style: TextStyle(
                                          fontSize: Pallet.heading2,
                                          color: Pallet.fontcolornew,
                                          fontWeight: Pallet.font500),
                                    ),
                                    SizedBox(height: 15),
                                    MyText(
                                        text: pageDetails["text15"],
                                        style: TextStyle(
                                            fontSize: Pallet.normalfont,
                                            color: Pallet.fontcolornew)),
                                    MyText(
                                      text: pageDetails["text16"],
                                      style: TextStyle(
                                          fontSize: Pallet.heading2,
                                          color: Pallet.fontcolornew,
                                          fontWeight: Pallet.font500),
                                    ),
                                    SizedBox(height: 15),
                                    MyText(
                                        text: pageDetails["text17"],
                                        style: TextStyle(
                                            fontSize: Pallet.normalfont,
                                            color: Pallet.fontcolornew)),
                                    MyText(
                                      text: pageDetails["text18"],
                                      style: TextStyle(
                                          fontSize: Pallet.heading2,
                                          color: Pallet.fontcolornew,
                                          fontWeight: Pallet.font500),
                                    ),
                                    SizedBox(height: 15),
                                    MyText(
                                        text: pageDetails["text19"],
                                        style: TextStyle(
                                          fontSize: Pallet.normalfont,
                                          color: Pallet.fontcolornew,
                                        )),
                                    MyText(
                                      text: pageDetails["text20"],
                                      style: TextStyle(
                                          fontSize: Pallet.heading2,
                                          color: Pallet.fontcolornew,
                                          fontWeight: Pallet.font500),
                                    ),
                                  ],
                                ),
                              )
                            : Container(
                                width: wdgtwidth,
                                padding:
                                    EdgeInsets.all(Pallet.defaultPadding * 2),
                                decoration: BoxDecoration(
                                    color: Pallet.fontcolor,
                                    boxShadow: [Pallet.shadowEffect],
                                    // border: Border.all(color: Pallet.dashcontainerback),
                                    borderRadius:
                                        BorderRadius.circular(Pallet.radius)),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    MyText(
                                      text: pageDetails["text9"],
                                      style: TextStyle(
                                          fontSize: Pallet.normalfont,
                                          color: Pallet.fontcolornew),
                                    ),
                                    MyText(
                                      text: pageDetails["text25"],
                                      style: TextStyle(
                                          fontSize: Pallet.heading2,
                                          color: Pallet.fontcolornew,
                                          fontWeight: Pallet.font500),
                                    ),
                                    SizedBox(height: 15),
                                    MyText(
                                      text: pageDetails["text11"],
                                      style: TextStyle(
                                          fontSize: Pallet.normalfont,
                                          color: Pallet.fontcolornew),
                                    ),
                                    MyText(
                                      text: pageDetails["text26"],
                                      style: TextStyle(
                                          fontSize: Pallet.heading2,
                                          color: Pallet.fontcolornew,
                                          fontWeight: Pallet.font500),
                                    ),
                                    SizedBox(height: 15),
                                    MyText(
                                        text: pageDetails["text13"],
                                        style: TextStyle(
                                            fontSize: Pallet.normalfont,
                                            color: Pallet.fontcolornew)),
                                    MyText(
                                      text: pageDetails["text27"],
                                      style: TextStyle(
                                          fontSize: Pallet.heading2,
                                          color: Pallet.fontcolornew,
                                          fontWeight: Pallet.font500),
                                    ),
                                    SizedBox(height: 15),
                                    MyText(
                                        text: pageDetails["text29"],
                                        style: TextStyle(
                                            fontSize: Pallet.normalfont,
                                            color: Pallet.fontcolornew)),
                                    MyText(
                                      text: pageDetails["text30"],
                                      style: TextStyle(
                                          fontSize: Pallet.heading2,
                                          color: Pallet.fontcolornew,
                                          fontWeight: Pallet.font500),
                                    ),
                                    SizedBox(height: 15),
                                    MyText(
                                        text: pageDetails["text17"],
                                        style: TextStyle(
                                            fontSize: Pallet.normalfont,
                                            color: Pallet.fontcolornew)),
                                    MyText(
                                      text: pageDetails["text28"],
                                      style: TextStyle(
                                          fontSize: Pallet.heading2,
                                          color: Pallet.fontcolornew,
                                          fontWeight: Pallet.font500),
                                    ),
                                    SizedBox(height: 15),
                                    MyText(
                                        text: pageDetails["text15"],
                                        style: TextStyle(
                                            fontSize: Pallet.normalfont,
                                            color: Pallet.fontcolornew)),
                                    MyText(
                                      text: pageDetails["text31"],
                                      style: TextStyle(
                                          fontSize: Pallet.heading2,
                                          color: Pallet.fontcolornew,
                                          fontWeight: Pallet.font500),
                                    ),
                                    SizedBox(height: 15),
                                    MyText(
                                        text: pageDetails["text19"],
                                        style: TextStyle(
                                          fontSize: Pallet.normalfont,
                                          color: Pallet.fontcolornew,
                                        )),
                                    MyText(
                                      text: pageDetails["text24"],
                                      style: TextStyle(
                                          fontSize: Pallet.heading2,
                                          color: Pallet.fontcolornew,
                                          fontWeight: Pallet.font500),
                                    ),
                                  ],
                                ),
                              ),
                      ],
                    ),
                  ),
                SizedBox(height: 5),
                if (cart.selectedGeteway != "advcash" &&
                    //  cart.selectedGeteway != "quantumClearance" &&
                    cart.selectedGeteway != "" &&
                    cart.selectedGeteway != "bankwire")
                  // DropDown(
                  //     labeltext: pageDetails["text4"],
                  //     label: pageDetails["text4"],
                  //     items: cart.currencies,
                  //     value: cart.selectedCurrency,
                  //     controller: cart.currency,
                  //     onChange: (value) {
                  //       setState(() {
                  //         cart.currency_after_selection = value;
                  //         cart.selectCurrency(value);
                  //         cart.selectCurrency_convertion(value);
                  //         print("object123");
                  //         print(value);
                  //       });
                  //     },
                  //     errortext: cart.errortext),
                  Container(
                    child: MyText(
                        text: pageDetails["text4"],
                        textAlign: TextAlign.start,
                        style: TextStyle(
                          color: Pallet.fontcolornew,
                          fontSize: Pallet.heading4,
                          fontWeight: FontWeight.w300,
                        )),
                    padding: EdgeInsets.symmetric(
                        vertical: Pallet.defaultPadding / 3),
                  ),
                SizedBox(height: 5),
                if (cart.selectedGeteway != "advcash" &&
                    cart.selectedGeteway != "quantumClearance" &&
                    cart.selectedGeteway != "" &&
                    cart.selectedGeteway != "bankwire")
                  Container(
                    width: double.infinity,
                    // decoration: BoxDecoration(
                    //     border: Border.all(color: Colors.red, width: 5)),
                    decoration: BoxDecoration(
                        border: Border.all(
                            color: errortext == ''
                                ? Pallet.dashcontainerback
                                : Colors.red,
                            width: 2.0),
                        color: Pallet.fontcolor,
                        borderRadius: BorderRadius.circular(Pallet.radius)),
                    child: DropdownButtonHideUnderline(
                      child: DropdownButton<String>(
                        items: cart.currencies
                            .map<DropdownMenuItem<String>>((String value) {
                          return DropdownMenuItem<String>(
                            value: value,
                            child: Padding(
                              padding: const EdgeInsets.only(left: 5.0),
                              child: MyText(
                                text: value,
                                style: TextStyle(
                                    color: Pallet.fontcolornew,
                                    fontSize: Pallet.heading4,
                                    fontWeight: Pallet.font500),
                              ),
                            ),
                          );
                        }).toList(),
                        value: cart.selectedCurrency,
                        isExpanded: true,
                        onChanged: (String newValue) {
                          setState(() {
                            this.cart.selectedCurrency = newValue;
                            cart.currency_after_selection = newValue;
                            cart.selectCurrency(newValue);
                            cart.selectCurrency_convertion(newValue);
                            print("object123");
                            print(newValue);
                          });
                        },
                      ),
                    ),
                  ),
                SizedBox(height: 5),
                if (cart.selectedGeteway != "advcash" &&
                    //  cart.selectedGeteway != "quantumClearance" &&
                    cart.selectedGeteway != "aaplus" &&
                    cart.selectedGeteway != "bankwire")
                  Textbox(
                    focusnode: converstion,
                    label: pageDetails["text9"],
                    header: pageDetails["text9"] +
                        ' ' +
                        cart.selectedCurrency.toString(),
                    enabled: false,
                    controller: cart.convertion_amount,
                  ),
                SizedBox(height: 5),
                if (cart.selectedGeteway == "asiapay")
                  MyText(
                    text: pageDetails["text5"],
                    style: TextStyle(
                      color: Pallet.fontcolornew,
                      fontSize: Pallet.heading4,
                      fontWeight: FontWeight.w300,
                    ),
                  ),
                SizedBox(height: 5),
                if (cart.selectedGeteway == "asiapay")
                  Container(
                    width: double.infinity,
                    decoration: BoxDecoration(
                        border: Border.all(
                          color: errortext == ''
                              ? Pallet.dashcontainerback
                              : Colors.red,
                          width: 2.0,
                        ),
                        color: Pallet.fontcolor,
                        borderRadius: BorderRadius.circular(Pallet.radius)),
                    child: DropdownButtonHideUnderline(
                      child: DropdownButton<String>(
                        items: cart.banks
                            .map<DropdownMenuItem<String>>((String value) {
                          return DropdownMenuItem<String>(
                            value: value,
                            child: Padding(
                              padding: const EdgeInsets.only(left: 5.0),
                              child: MyText(
                                  text: value,
                                  style: TextStyle(
                                      color: Pallet.fontcolornew,
                                      fontSize: Pallet.heading4,
                                      fontWeight: Pallet.font500)),
                            ),
                          );
                        }).toList(),
                        value: cart.selectedBank,
                        isExpanded: true,
                        onChanged: (String newValue) {
                          setState(() {
                            this.cart.selectedBank = newValue;
                            cart.selectBank(newValue);
                            print("object1234");
                            print(newValue);
                          });
                        },
                      ),
                    ),
                  ),
                // DropDown(
                //     label: pageDetails["text5"],
                //     labeltext: pageDetails["text5"],
                //     items: cart.banks,
                //     value: cart.selectedBank,
                //     controller: cart.bank,
                //     onChange: (value) {
                //       setState(() {
                //         cart.selectBank(value);
                //       });
                //     },
                //     errortext: cart.errortext),
                if (cart.selectedGeteway != "" &&
                    cart.selectedGeteway != "bankwire")
                  RawKeyboardListener(
                    focusNode: notesnode,
                    onKey: (event) {
                      if (event.isKeyPressed(LogicalKeyboardKey.tab)) {
                        FocusScope.of(context).requestFocus(continuenode);
                      }
                    },
                    child: Padding(
                      padding: EdgeInsets.only(top: 5),
                      child: MultilineTextBox(
                        onsubmit: () {
                          FocusScope.of(context).requestFocus(continuenode);
                        },
                        label: pageDetails["text6"],
                        controller: cart.note,
                      ),
                    ),
                  ),
                SizedBox(
                  height: 20,
                ),
                if (cart.selectedGeteway != "bankwire")
                  Center(
                    child: Theme(
                      data: ThemeData(
                        buttonTheme: ButtonThemeData(
                            shape: RoundedRectangleBorder(
                                borderRadius:
                                    BorderRadius.circular(Pallet.radius),
                                side: BorderSide(
                                    color: Pallet.dashcontainerback,
                                    width: 2))),
                      ),
                      child: RawKeyboardListener(
                        focusNode: continuenode,
                        onKey: (event) {
                          if (event.isKeyPressed(LogicalKeyboardKey.enter) ||
                              event.isKeyPressed(
                                  LogicalKeyboardKey.numpadEnter)) {
                            setState(() {
                              cart.createPayment(context);
                            });
                          }
                        },
                        // ignore: deprecated_member_use
                        child: AbsorbPointer(
                          absorbing: cart.selectedGeteway != "quantumClearance"
                              ? false
                              : true,
                          child: Opacity(
                            opacity: cart.selectedGeteway != "quantumClearance"
                                ? 1
                                : .5,
                            child: CustomButton(
                              onpress: () {
                                setState(() {
                                  cart.createPayment(context);
                                });
                              },
                              vpadding: 10,
                              text: pageDetails['text7'],
                              buttoncolor: Pallet.fontcolornew,
                              textcolor: Pallet.fontcolor,
                              textsize: Pallet.heading3,
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                SizedBox(
                  height: 20,
                ),
              ],
            ),
          );
  }

  Widget note({double wdgtwidth}) {
    return Container(
        padding: EdgeInsets.all(Pallet.defaultPadding),
        width: wdgtwidth,
        decoration: BoxDecoration(
          boxShadow: [Pallet.shadowEffect],
          color: Pallet.fontcolor,
          // border: Border.all(color: Pallet.dashcontainerback),
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(
                Pallet.radius,
              ),
              bottomRight: Radius.circular(
                Pallet.radius,
              )),
        ),
        // height: 200,
        child: Center(
          child: Row(
            // mainAxisAlignment: MainAxisAlignment.spaceAround,
            // mainAxisAlignment: MainAxisAlignment.start  ,

            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              MyText(
                text: pageDetails["text22"] + ' ',
                style: TextStyle(
                    color: Pallet.dashcontainerback,
                    fontSize: Pallet.notefont,
                    fontWeight: FontWeight.bold),
              ),
              Container(
                padding:
                    EdgeInsets.symmetric(horizontal: Pallet.defaultPadding / 5),
                width: wdgtwidth - 115,
                child: MyText(
                  text: pageDetails["text23"][cart.selectedGeteway.toString()],
                  style: TextStyle(
                      color: Pallet.dashcontainerback,
                      height: 1.7,
                      fontSize: Pallet.notefont,
                      fontWeight: FontWeight.w400),
                  // textAlign: TextAlign.justify,
                ),
              )
            ],
          ),
        ));
  }
}

class TextDSV extends StatelessWidget {
  const TextDSV({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Text('Select DSV Package',
        style: TextStyle(
          color: Pallet.dashcontainerback,
          fontSize: Pallet.heading4,
          fontWeight: FontWeight.w400,
        ));
  }
}
