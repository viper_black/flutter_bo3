import 'package:centurion/home.dart';
import 'package:centurion/login.dart';
// import 'package:centurion/screens/myshop.dart';
import 'package:centurion/services/business/user.dart';
import 'package:centurion/services/communication/http/index.dart';
import 'package:centurion/utils/utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../config.dart';
import '../common.dart';
import '../services/business/account.dart';

class Settings extends StatefulWidget {
  final double wdgtWidth, wdgtHeight;

  const Settings({
    Key key,
    this.wdgtWidth,
    this.wdgtHeight,
  }) : super(key: key);
  @override
  _SettingsState createState() => _SettingsState();
}

class _SettingsState extends State<Settings> {
  Map<String, dynamic> pageDetails = {};
  bool is2faSwitched = false;
  Future<String> temp;
  // Future<String> temp1;

  // var _formkey;

  initState() {
    temp = settingdata.getSettings();
    print('**********');
    print(temp);
    // temp1 = settingskycdata.getKycStatus();
    print('%%%%%%%%');
    // print(temp1);

    setPageDetails();
    super.initState();
    dashboard.dashboard();
  }

  void setPageDetails() async {
    String data = await Utils.getPageDetails('settings');
    setState(() {
      pageDetails = Utils.fromJSONString(data);
    });
  }

  // var map = {
  //   "system_product_id": {"type": 1, "minimum": 0, "maximum": 999999}
  // };

  // void disable2Fa(String disableTwoFA) async {
  //   Map<String, dynamic> _map = {
  //     "system_product_id": 1,
  //     "username": username.text,
  //     "password": password.text,
  //     "password_future": otp,
  //     "hardware_id": deviceId,
  //     "is_finger_print": false,
  //   };
  //   reqLogin(_map);
  //   _formKey2.currentState.validate();
  // }

  // ignore: unused_field
  final GlobalKey<FormState> _formkey = GlobalKey<FormState>();

  TextEditingController oldpwdCon = TextEditingController();
  TextEditingController newpwdCon = TextEditingController();
  TextEditingController cnfmpwdCon = TextEditingController();
  TextEditingController disableTwoFA = TextEditingController();
  // ignore: unused_field
  GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  String disbale2faError;
  String oldError;
  String newError;
  String confError;
  final oldpass = FocusNode();
  final newpass = FocusNode();
  final cnfrmpass = FocusNode();

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: temp,
      builder: (context, snapshot) {
        if (snapshot.hasError) {
          if (snapshot.error == 'Something Went Wrong') {
            return SomethingWentWrongMessage();
          } else {
            return Error(
              message: snapshot.error != null ? snapshot.error : '',
            );
          }
        } else if (snapshot.hasData) {
          return LayoutBuilder(
            // ignore: missing_return
            builder: (context, constraints) {
              if (constraints.maxWidth <= ScreenSize.iphone) {
                return SingleChildScrollView(
                    child: Padding(
                  padding: EdgeInsets.only(
                      left: Pallet.defaultPadding,
                      top: Pallet.topPadding1,
                      right: Pallet.defaultPadding),
                  child: settingscreemn(
                      wdgtwidth: widget.wdgtWidth, isMobile: true),
                ));
              } else if (constraints.maxWidth > ScreenSize.iphone &&
                  constraints.maxWidth < ScreenSize.ipad) {
                // return settingscreemn(wdgtwidth: widget.wdgtWidth);
                return Padding(
                  padding: EdgeInsets.only(
                      left: Pallet.leftPadding, top: Pallet.topPadding1),
                  child: settingscreemn(
                      wdgtwidth: widget.wdgtWidth, isMobile: false),
                );
              } else if (constraints.maxWidth > ScreenSize.ipad &&
                  constraints.maxWidth < ScreenSize.tab) {
                // return settingscreemn(wdgtwidth: widget.wdgtWidth);
                return Padding(
                  padding: EdgeInsets.only(
                      left: Pallet.leftPadding, top: Pallet.topPadding1),
                  child: settingscreemn(
                      wdgtwidth: widget.wdgtWidth * .45, isMobile: false),
                );
              } else {
                return SingleChildScrollView(
                    child: Padding(
                  padding: EdgeInsets.only(
                      left: Pallet.leftPadding, top: Pallet.topPadding1),
                  child: settingscreemn(
                      wdgtwidth: widget.wdgtWidth * .45, isMobile: false),
                ));
              }
            },
          );
        }
        return Loader();
      },
    );
    // return settingscreemn(wdgtwidth: widget.wdgtWidth);
  }

  Widget settingscreemn({double wdgtwidth, bool isMobile, double popheading}) {
    return FutureBuilder(
        future: temp,
        builder: (context, snapshot) {
          if (snapshot.hasError) {
            return SomethingWentWrongMessage();
          } else if (snapshot.hasData) {
            return Container(
              width: wdgtwidth,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  MyText(
                      text: pageDetails["text1"],
                      style: TextStyle(
                        color: Pallet.fontcolornew,
                        fontSize: Pallet.heading1,
                      )),
                  SizedBox(height: 20),
                  Container(
                    // decoration: BoxDecoration(
                    //     color: Pallet.fontcolor,
                    //     boxShadow: [Pallet.shadowEffect]),
                    child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          // Container(
                          //   width: wdgtwidth,
                          //   decoration: BoxDecoration(
                          //     borderRadius:
                          //         BorderRadius.circular(Pallet.radius),
                          //     color: Pallet.docbg,
                          //   ),
                          //   child: SwitchListTile(
                          //     value: Pallet.isSwitched,
                          //     activeThumbImage: NetworkImage(
                          //         'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTFIAWxsU2qEI995BYW36GbbGLS8NzlGTzvvg&usqp=CAU'),
                          //     inactiveThumbImage: NetworkImage(
                          //         'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQd5jIZV7weqa_JiJGbrVPHnCJowW_K5_itkA&usqp=CAU'),
                          //     title: MyText(text:
                          //       pageDetails["text2"],
                          //       style: TextStyle(
                          //         color: Pallet.fontcolornew,
                          //         fontSize: 16.0,
                          //       ),
                          //     ),
                          //     onChanged: (bool value) {
                          //       if (value) {
                          //         setState(() {
                          //           Pallet.isSwitched = value;
                          //           print('yay');
                          //           Pallet.litTheme();
                          //         });
                          //       } else {
                          //         setState(() {
                          //           Pallet.isSwitched = value;
                          //           Pallet.darkTheme();
                          //         });
                          //       }
                          //       Navigator.push(
                          //           context,
                          //           MaterialPageRoute(
                          //               builder: (context) => Home(
                          //                     route: 'my_settings',
                          //                   )));
                          //     },
                          //   ),
                          // ),
                          // SizedBox(height: 20),

                          Container(
                            width: wdgtwidth,
                            decoration: BoxDecoration(
                              borderRadius:
                                  BorderRadius.circular(Pallet.radius),
                              color: Pallet.docbg,
                            ),
                            child: SwitchListTile(
                              value: settingdata.twoEnabled,
                              title: MyText(
                                text: pageDetails["text3"],
                                style: TextStyle(
                                  color: Pallet.fontcolornew,
                                  fontSize: 16.0,
                                ),
                              ),
                              onChanged: (bool value) async {
                                setState(() {
                                  settingdata.twoEnabled = value;
                                });
                                if (value == true)
                                  Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) => Home(
                                              route: 'two_factor_auth',
                                              // param: '',
                                            )),
                                  );
                                else {
                                  print("Diasble 2FA0000000000000000");
                                  // await user.disable2FA();
                                  // var disable2FA = SnackBar(
                                  //     content: MyText(text:pageDetails["text16"]));

                                  // ScaffoldMessenger.of(context)
                                  //     .showSnackBar(disable2FA);
                                  showDialog(
                                      context: context,
                                      barrierDismissible: false,
                                      builder: (_) => StatefulBuilder(
                                            builder: (BuildContext context,
                                                    void Function(
                                                            void Function())
                                                        setState1) =>
                                                AlertDialog(
                                              backgroundColor:
                                                  Pallet.popupcontainerback,
                                              shape: RoundedRectangleBorder(
                                                  borderRadius:
                                                      BorderRadius.circular(
                                                          Pallet.radius)),
                                              title: Row(
                                                children: [
                                                  Image.asset(
                                                    'assets/c-logo.png',
                                                    width: 40,
                                                  ),
                                                  SizedBox(width: 10),
                                                  Align(
                                                    alignment:
                                                        Alignment.topLeft,
                                                    child: MyText(
                                                      text:
                                                          pageDetails['text41'],
                                                      style: TextStyle(
                                                        fontSize: 15,
                                                        color: Pallet.fontcolor,
                                                      ),
                                                    ),
                                                  ),
                                                ],
                                              ),
                                              content: Container(
                                                width: 400,
                                                // height: 50,
                                                child: SingleChildScrollView(
                                                  child: Column(
                                                    children: [
                                                      MyText(
                                                        text: pageDetails[
                                                            'text42'],
                                                        style: TextStyle(
                                                          fontSize: 15,
                                                          height: 1.3,
                                                          color:
                                                              Pallet.fontcolor,
                                                        ),
                                                      ),
                                                      SizedBox(height: 20),
                                                      TextFormField(
                                                        style: TextStyle(
                                                            color: Pallet
                                                                .fontcolor),
                                                        controller:
                                                            disableTwoFA,
                                                        inputFormatters: <
                                                            TextInputFormatter>[
                                                          FilteringTextInputFormatter
                                                              .digitsOnly,
                                                          new LengthLimitingTextInputFormatter(
                                                              8),
                                                        ],
                                                        cursorColor:
                                                            Pallet.fontcolor,
                                                        decoration:
                                                            InputDecoration(
                                                          border: OutlineInputBorder(
                                                              borderSide: BorderSide(
                                                                  color: Pallet
                                                                      .fontcolor,
                                                                  width: 2),
                                                              borderRadius:
                                                                  BorderRadius
                                                                      .circular(
                                                                          Pallet
                                                                              .radius)),
                                                          enabledBorder: OutlineInputBorder(
                                                              borderSide: BorderSide(
                                                                  color: Pallet
                                                                      .fontcolor,
                                                                  width: 2),
                                                              borderRadius:
                                                                  BorderRadius
                                                                      .circular(
                                                                          Pallet
                                                                              .radius)),
                                                          focusedBorder: OutlineInputBorder(
                                                              borderSide: BorderSide(
                                                                  color: Pallet
                                                                      .fontcolor,
                                                                  width: 2),
                                                              borderRadius:
                                                                  BorderRadius
                                                                      .circular(
                                                                          Pallet
                                                                              .radius)),
                                                          errorBorder: OutlineInputBorder(
                                                              borderSide:
                                                                  BorderSide(
                                                                      color: Colors
                                                                          .red,
                                                                      width: 2),
                                                              borderRadius:
                                                                  BorderRadius
                                                                      .circular(
                                                                          Pallet
                                                                              .radius)),
                                                          focusedErrorBorder: OutlineInputBorder(
                                                              borderSide:
                                                                  BorderSide(
                                                                      color: Colors
                                                                          .red,
                                                                      width: 2),
                                                              borderRadius:
                                                                  BorderRadius
                                                                      .circular(
                                                                          Pallet
                                                                              .radius)),
                                                          labelText:
                                                              pageDetails[
                                                                  'text43'],
                                                          labelStyle: TextStyle(
                                                            fontSize: 15,
                                                            color: Pallet
                                                                .fontcolor,
                                                          ),
                                                          errorText:
                                                              disbale2faError,
                                                        ),
                                                        keyboardType:
                                                            TextInputType
                                                                .number,
                                                        autofocus: true,
                                                        onChanged: (value) {
                                                          if (value
                                                              .trim()
                                                              .isEmpty) {
                                                            setState1(() {
                                                              disbale2faError =
                                                                  pageDetails[
                                                                      'text44'];
                                                            });
                                                          } else {
                                                            setState1(() {
                                                              disbale2faError =
                                                                  null;
                                                            });
                                                          }
                                                        },
                                                        // validator: (value) {
                                                        //   if (value
                                                        //       .trim()
                                                        //       .isEmpty) {
                                                        //     setState1(() {
                                                        //       disbale2faError =
                                                        //           'Enter Your Code';
                                                        //     });
                                                        //   } else {
                                                        //     setState1(() {
                                                        //       disbale2faError =
                                                        //           null;
                                                        //     });
                                                        //   }
                                                        // },
                                                      ),
                                                      SizedBox(height: 20),
                                                      CustomButton(
                                                        vpadding: 10,
                                                        buttoncolor:
                                                            Pallet.fontcolor,
                                                        text: pageDetails[
                                                            'text45'],
                                                        onpress: () async {
                                                          // await user.disable2FA();
                                                          //
                                                          //
                                                          String username =
                                                              Utils.fromJSONString(
                                                                          await Utils.getSharedPrefereces(
                                                                              'data'))[
                                                                      'user_name']
                                                                  .toString();
                                                          print('OOOOOOOOOOOO');
                                                          print(username);
                                                          print(disableTwoFA
                                                              .text);
                                                          if (disableTwoFA.text
                                                                  .isEmpty ||
                                                              disableTwoFA
                                                                      .text ==
                                                                  null ||
                                                              disableTwoFA
                                                                      .text ==
                                                                  '') {
                                                            setState1(() {
                                                              disbale2faError =
                                                                  pageDetails[
                                                                      'text44'];
                                                            });
                                                          } else {
                                                            // var _map = {
                                                            //   "product_id": 1,
                                                            //   "username":
                                                            //       username,
                                                            //   "secret":
                                                            //       disableTwoFA
                                                            //           .text
                                                            //           .toString(),
                                                            //   "is_setup": true,
                                                            // };

                                                            // var _result =
                                                            //     await HttpRequest.Post(
                                                            //         'check2FA',
                                                            //         Utils.constructPayload(
                                                            //             _map));
                                                            // print(_map);
                                                            // print(
                                                            //     "zzzzzzzzzzzzzzzzzzzzzzzzzzzz");
                                                            // print(_result);
                                                            // if (Utils
                                                            //     .isServerError(
                                                            //         _result)) {
                                                            //   print(_map);
                                                            //   if (_result['response']
                                                            //           [
                                                            //           'error'] ==
                                                            //       'Invalid Code') {
                                                            //     setState1(() {
                                                            //       disbale2faError =
                                                            //           pageDetails[
                                                            //               'text46'];
                                                            //     });
                                                            //   } else {
                                                            //     snack.snack(
                                                            //         title: pageDetails[
                                                            //             'text39']);
                                                            //     // final snackBar = SnackBar(
                                                            //     //     content: MyText(text:
                                                            //     //         pageDetails[
                                                            //     //             'text39']));
                                                            //     // ScaffoldMessenger.of(
                                                            //     //         context)
                                                            //     //     .showSnackBar(
                                                            //     //         snackBar);
                                                            //   }
                                                            //
                                                            //
                                                            //
                                                            var awaip = await user
                                                                .check2FA(
                                                                    username,
                                                                    disableTwoFA
                                                                        .text
                                                                        .toString());
                                                            if (awaip
                                                                    .toString() ==
                                                                'Valid OTP'
                                                                    .toString()) {
                                                              await user
                                                                  .disable2FA();
                                                              Navigator.pop(
                                                                  context);
                                                              snack.snack(
                                                                  title: pageDetails[
                                                                      'text47']);
                                                            } else {
                                                              snack.snack(
                                                                  title: pageDetails[
                                                                      'text39']);
                                                            }
                                                          }
                                                          // else {
                                                          //   await user
                                                          //       .disable2FA();
                                                          //   Navigator.pop(
                                                          //       context);
                                                          //   snack.snack(
                                                          //       title: pageDetails[
                                                          //           'text47']);
                                                          //   // ScaffoldMessenger
                                                          //   //         .of(
                                                          //   //             context)
                                                          //   //     .showSnackBar(
                                                          //   //         SnackBar(
                                                          //   //             content:
                                                          //   //                 MyText(text:pageDetails['text47'])));
                                                          // }
                                                          // }
                                                        },
                                                      ),
                                                    ],
                                                  ),
                                                ),
                                              ),
                                              actions: [
                                                PopupButton(
                                                  text: pageDetails['text48'],
                                                  onpress: () {
                                                    Navigator.of(context).pop();
                                                    setState1(() {
                                                      disbale2faError = null;
                                                      disableTwoFA.text = '';
                                                    });
                                                    setState(() {
                                                      settingdata.twoEnabled =
                                                          true;
                                                    });
                                                    // twofastatus = true;
                                                    // _formKey.currentState
                                                    //     .validate();
                                                    // check2Fa(disableTwoFA.text);
                                                  },
                                                ),
                                              ],
                                            ),
                                          ));
                                }
                              },
                            ),
                          ),
                          SizedBox(height: 20),
                          MyText(
                              text: pageDetails["text5"],
                              style: TextStyle(
                                color: Pallet.fontcolornew,
                                fontSize: Pallet.heading3,
                                fontWeight: Pallet.subheading1wgt,
                              )),
                          SizedBox(height: 20),
                          Container(
                            width: wdgtwidth,
                            decoration: BoxDecoration(
                              borderRadius:
                                  BorderRadius.circular(Pallet.radius),
                              color: Pallet.docbg,
                            ),
                            child: ListTile(
                              subtitle: Theme(
                                  data: Theme.of(context).copyWith(
                                    canvasColor: Pallet.fontcolor,
                                  ),
                                  child: DropdownButtonHideUnderline(
                                    child: DropdownButton<String>(
                                      isExpanded: true,
                                      value: Pallet.language,
                                      onChanged: (String newValue) {
                                        setState(() {
                                          Pallet.changelang(newValue);
                                          Navigator.push(
                                              context,
                                              MaterialPageRoute(
                                                  builder: (context) => Home(
                                                        route: 'dashboard',
                                                      )));
                                        });
                                      },
                                      items: Pallet.languages
                                          .map<DropdownMenuItem<String>>(
                                              (String value) {
                                        return DropdownMenuItem<String>(
                                          value: value,
                                          child: MyText(
                                            text: value,
                                            style: TextStyle(
                                                fontSize: 16.0,
                                                color: Pallet.fontcolornew),
                                          ),
                                        );
                                      }).toList(),
                                    ),
                                  )),
                            ),
                          ),
                          SizedBox(height: 20),
                          Container(
                            width: wdgtwidth,
                            decoration: BoxDecoration(
                              borderRadius:
                                  BorderRadius.circular(Pallet.radius),
                              color: Pallet.docbg,
                            ),
                            child: ListTile(
                              title: MyText(
                                  text: pageDetails['text19'],
                                  style: TextStyle(
                                    color: Pallet.fontcolornew,
                                    fontSize: 16.0,
                                  )),
                              onTap: () {
                                showDialog(
                                    barrierDismissible: false,
                                    context: context,
                                    builder: (BuildContext context) {
                                      return isMobile == true
                                          ? changepasswordalert(
                                              width: wdgtwidth,
                                              pageDetails: pageDetails)
                                          : changepasswordalert(
                                              width: null,
                                              pageDetails: pageDetails);
                                    });

                                // setState(() {
                                //   isMobile == true ? showAlert() : showAlert();
                                // });
                                // Navigator.push(
                                //   context,
                                //   MaterialPageRoute(
                                //       builder: (context) => Home(
                                //             route: 'account_pwd2',
                                //           )),
                                // );
                              },
                            ),
                          ),
                          SizedBox(height: 20),
                          if (settingskycdata.settingskyc == 'verified')
                            Container(
                              width: wdgtwidth,
                              decoration: BoxDecoration(
                                borderRadius:
                                    BorderRadius.circular(Pallet.radius),
                                color: Pallet.docbg,
                              ),
                              child: ListTile(
                                title: MyText(
                                    text: pageDetails['text35'],
                                    style: TextStyle(
                                      color: Pallet.fontcolornew,
                                      fontSize: 16.0,
                                    )),
                                onTap: () {
                                  Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (BuildContext context) =>
                                              Home(
                                                route: 'individual_profile',
                                                manageaddressonly: false,
                                              )));
                                },
                              ),
                            ),
                          if (settingskycdata.settingskyc == 'verified')
                            SizedBox(height: 20),
                          if (settingskycdata.settingskyc == 'verified')
                            Container(
                              width: wdgtwidth,
                              decoration: BoxDecoration(
                                borderRadius:
                                    BorderRadius.circular(Pallet.radius),
                                color: Pallet.docbg,
                              ),
                              child: ListTile(
                                title: MyText(
                                    text: pageDetails['text36'],
                                    style: TextStyle(
                                      color: Pallet.fontcolornew,
                                      fontSize: 16.0,
                                    )),
                                onTap: () {
                                  Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (BuildContext context) =>
                                              Home(
                                                route: 'individual_profile',
                                                manageaddressonly: true,
                                              )));
                                },
                              ),
                            ),
                          // SizedBox(color: Pallet.fontcolornew, height: 20),
                        ]),
                  )
                  // Container(height: 500),
                ],
              ),
            );
          }
          return Loader();
        });
  }

  // Widget showAlert(double width, setStatee) {
  //   var errorTxt = MyText(text:"fbwe");
  //   bool secureText1 = true;
  //   bool secureText2 = true;
  //   bool secureText3 = true;
  //   var visiblity = Icon(Icons.visibility_off);
  //   // bool _validate = false;
  //   var _oldpwd;
  //   var _newpwd;
  //   var _cnfmpwd;
  //   TextEditingController oldpwdCon = TextEditingController();
  //   TextEditingController newpwdCon = TextEditingController();
  //   TextEditingController cnfmpwdCon = TextEditingController();
  //   final GlobalKey<FormState> _formkey = GlobalKey<FormState>();

  //   return AlertDialog(
  //     backgroundColor: Pallet.popupcontainerback,
  //     shape: RoundedRectangleBorder(
  //         borderRadius: BorderRadius.circular(Pallet.radius)),
  //     // title: MyText(text:"Change Password"),
  //     // actions: <Widget>[
  //     //   new FlatButton(
  //     //     onPressed: () {
  //     //       Navigator.of(context).pop();
  //     //     },
  //     //     textColor: Theme.of(context).primaryColor,
  //     //     child: const MyText(text:
  //     //       'Close',
  //     //       style: TextStyle(color: Colors.white),
  //     //     ),
  //     //   ),
  //     // ],

  //     content: Container(
  //         color: Pallet.popupcontainerback,
  //         width: width == null ? 400 : width,
  //         height: 420,
  //         child: ListView(
  //           children: [
  //             Column(children: [
  //               Form(
  //                 key: _formkey,
  //                 child: Container(
  //                     child: Column(
  //                   children: [
  //                     Row(
  //                       children: [
  //                         Image.asset(
  //                           'assets/c-logo.png',
  //                           width: 40,
  //                         ),
  //                         SizedBox(width: 10),
  //                         Align(
  //                           alignment: Alignment.topLeft,
  //                           child: MyText(text:
  //                             pageDetails['text6'],
  //                             style: TextStyle(
  //                               fontSize: 15,
  //                               color: Pallet.fontcolor,
  //                             ),
  //                           ),
  //                         ),
  //                       ],
  //                     ),
  //                     SizedBox(height: 15),
  //                     // Align(
  //                     //   alignment: Alignment.centerLeft,
  //                     //   child: Container(
  //                     //     child: MyText(text:
  //                     //       'Old Password',
  //                     //       style: TextStyle(
  //                     //         fontSize: 15,
  //                     //         color: Pallet.fontcolor,
  //                     //       ),
  //                     //     ),
  //                     //   ),
  //                     // ),
  //                     SizedBox(
  //                       height: 5,
  //                     ),
  //                     Align(
  //                       alignment: Alignment.centerLeft,
  //                       child: GestureDetector(
  //                         onTap: () {
  //                           FocusScopeNode currentFocus =
  //                               FocusScope.of(context);
  //                           if (!currentFocus.hasPrimaryFocus) {
  //                             currentFocus.unfocus();
  //                           }
  //                         },
  //                         child: TextFormField(
  //                           validator: (String value) {
  //                             if (value.isEmpty) {
  //                               return 'Please a enter your Old Password';
  //                             }

  //                             return null;
  //                           },
  //                           style: TextStyle(color: Pallet.dashcontainerback),
  //                           controller: oldpwdCon,
  //                           decoration: InputDecoration(
  //                             contentPadding:
  //                                 EdgeInsets.fromLTRB(10.0, 10.0, 20.0, 10.0),
  //                             hintText: pageDetails['text7'],
  //                             prefixIcon: Icon(
  //                               Icons.lock_rounded,
  //                               color: Pallet.dashcontainerback,
  //                             ),
  //                             suffixIcon: IconButton(
  //                               color: Pallet.dashcontainerback,
  //                               icon: secureText1 == true
  //                                   ? Icon(Icons.visibility)
  //                                   : Icon(Icons.visibility),
  //                               onPressed: () {
  //                                 setStatee(() {
  //                                   print("printedddddd");

  //                                   if (secureText1 == true) {
  //                                     secureText1 = false;
  //                                   }
  //                                   // secureText1 == true ? false : true;
  //                                 });
  //                               },
  //                             ),
  //                             fillColor: Pallet.fontcolor,
  //                             filled: true,
  //                             hintStyle: TextStyle(
  //                               color: Pallet.dashcontainerback,
  //                             ),
  //                             // enabledBorder: OutlineInputBorder(
  //                             //   borderSide: BorderSide(
  //                             //       color: Pallet.dashcontainerback,
  //                             //       width: 1.5),
  //                             // ),
  //                             border: OutlineInputBorder(
  //                                 borderRadius: BorderRadius.circular(5)),
  //                           ),
  //                           obscureText: secureText1,
  //                         ),
  //                       ),
  //                     ),
  //                     // Align(
  //                     //   alignment: Alignment.centerLeft,
  //                     //   child: Container(
  //                     //     child: MyText(text:
  //                     //       'New Password',
  //                     //       style: TextStyle(
  //                     //         fontSize: 15,
  //                     //         color: Pallet.dashcontainerback,
  //                     //       ),
  //                     //     ),
  //                     //   ),
  //                     // ),
  //                     SizedBox(
  //                       height: 5,
  //                     ),
  //                     Align(
  //                       alignment: Alignment.centerLeft,
  //                       child: GestureDetector(
  //                         onTap: () {
  //                           FocusScopeNode currentFocus =
  //                               FocusScope.of(context);
  //                           if (!currentFocus.hasPrimaryFocus) {
  //                             currentFocus.unfocus();
  //                           }
  //                         },
  //                         child: TextFormField(
  //                           validator: (String value) {
  //                             if (value.isEmpty) {
  //                               return 'Please a enter your New Password';
  //                             } else if (value.length < 8) {
  //                               return 'password must contain minimum of 8 Characters';
  //                             }
  //                             if (!regex.hasMatch(value))
  //                               return 'Enter a valid password (read the note below)';
  //                             else if (oldpwdCon.text == newpwdCon.text) {
  //                               return "Old Password cannot be New Password";
  //                             } else
  //                               return null;
  //                           },
  //                           style: TextStyle(color: Pallet.dashcontainerback),
  //                           controller: newpwdCon,
  //                           decoration: InputDecoration(
  //                             contentPadding:
  //                                 EdgeInsets.fromLTRB(10.0, 10.0, 20.0, 10.0),
  //                             hintText: pageDetails['text8'],
  //                             prefixIcon: Icon(
  //                               Icons.lock_rounded,
  //                               color: Pallet.dashcontainerback,
  //                             ),
  //                             suffixIcon: IconButton(
  //                               color: Pallet.dashcontainerback,
  //                               icon: Icon(secureText2
  //                                   ? Icons.visibility
  //                                   : Icons.visibility_off),
  //                               onPressed: () {
  //                                 setState(() {
  //                                   secureText2 = !secureText2;
  //                                 });
  //                               },
  //                             ),
  //                             hintStyle: TextStyle(
  //                               color: Pallet.dashcontainerback,
  //                             ),
  //                             fillColor: Pallet.fontcolor,
  //                             filled: true,
  //                             // enabledBorder: OutlineInputBorder(
  //                             //   borderSide: BorderSide(
  //                             //       color: Pallet.dashcontainerback,
  //                             //       width: 1.5),
  //                             // ),
  //                             border: OutlineInputBorder(
  //                                 borderRadius: BorderRadius.circular(5)),
  //                           ),
  //                           obscureText: secureText2,
  //                         ),
  //                       ),
  //                     ),
  //                     // Align(
  //                     //   alignment: Alignment.centerLeft,
  //                     //   child: Container(
  //                     //     child: MyText(text:
  //                     //       'Confirm Password',
  //                     //       style: TextStyle(
  //                     //         fontSize: 15,
  //                     //         color: Pallet.dashcontainerback,
  //                     //       ),
  //                     //     ),
  //                     //   ),
  //                     // ),
  //                     SizedBox(
  //                       height: 5,
  //                     ),
  //                     Align(
  //                       alignment: Alignment.centerLeft,
  //                       child: GestureDetector(
  //                         onTap: () {
  //                           FocusScopeNode currentFocus =
  //                               FocusScope.of(context);
  //                           if (!currentFocus.hasPrimaryFocus) {
  //                             currentFocus.unfocus();
  //                           }
  //                         },
  //                         child: TextFormField(
  //                           validator: (String value) {
  //                             if (value.isEmpty) {
  //                               return 'Please re-enter password';
  //                             }
  //                             print(newpwdCon.text);
  //                             print(cnfmpwdCon.text);

  //                             if (newpwdCon.text != cnfmpwdCon.text) {
  //                               return "Passwords does not match";
  //                             }

  //                             return null;
  //                           },
  //                           style: TextStyle(color: Pallet.dashcontainerback),
  //                           controller: cnfmpwdCon,
  //                           decoration: InputDecoration(
  //                             contentPadding:
  //                                 EdgeInsets.fromLTRB(10.0, 10.0, 20.0, 10.0),
  //                             hintText: pageDetails['text9'],
  //                             hintStyle: TextStyle(
  //                               color: Pallet.dashcontainerback,
  //                             ),
  //                             prefixIcon: Icon(
  //                               Icons.lock_rounded,
  //                               color: Pallet.dashcontainerback,
  //                             ),
  //                             suffixIcon: IconButton(
  //                               color: Pallet.dashcontainerback,
  //                               icon: secureText3 == true
  //                                   ? Icon(Icons.visibility)
  //                                   : Icon(Icons.visibility_off),
  //                               onPressed: () {
  //                                 print('secure string ' +
  //                                     secureText3.toString());
  //                                 setStatee(() {
  //                                   secureText3 = !secureText3;

  //                                   // print(secureText3 == true);
  //                                   // if (secureText3 == true) {
  //                                   //   visiblity = Icon(Icons.visibility_off);
  //                                   // } else {
  //                                   //   visiblity = Icon(Icons.visibility);
  //                                   // }
  //                                   // print(visiblity);
  //                                 });
  //                               },
  //                             ),
  //                             // enabledBorder: OutlineInputBorder(
  //                             //   borderSide: BorderSide(
  //                             //       color: Pallet.dashcontainerback,
  //                             //       width: 1.5),
  //                             // ),
  //                             fillColor: Pallet.fontcolor,
  //                             filled: true,
  //                             border: OutlineInputBorder(
  //                                 borderRadius: BorderRadius.circular(5)),
  //                           ),
  //                           obscureText: secureText3,
  //                         ),
  //                       ),
  //                     ),
  //                     SizedBox(
  //                       height: 10,
  //                     ),
  //                     Container(
  //                       decoration: BoxDecoration(
  //                         borderRadius: BorderRadius.circular(10),
  //                         color: Pallet.fontcolor,
  //                       ),
  //                       child: Container(
  //                           padding: EdgeInsets.all(10),
  //                           child: Column(
  //                             crossAxisAlignment: CrossAxisAlignment.start,
  //                             children: [
  //                               MyText(text:pageDetails["text17"],
  //                                   style: TextStyle(
  //                                     color: Pallet.dashcontainerback,
  //                                     fontWeight: FontWeight.w900,
  //                                   )),
  //                               MyText(text:
  //                                 pageDetails["text18"],
  //                                 style: TextStyle(
  //                                   color: Pallet.dashcontainerback,
  //                                   fontSize: 13,
  //                                 ),
  //                               ),
  //                             ],
  //                           )),
  //                       width: width == null ? 400 : width,
  //                       // height: 100,
  //                     ),
  //                     SizedBox(height: 25),
  //                     Row(mainAxisAlignment: MainAxisAlignment.end, children: [
  //                       InkWell(
  //                         child: Container(
  //                           margin: EdgeInsets.only(right: 10, top: 5),
  //                           width: width == null ? 90 : 70,
  //                           height: 35,
  //                           decoration: BoxDecoration(
  //                             borderRadius: BorderRadius.circular(5),
  //                             color: Pallet.fontcolor,
  //                           ),
  //                           child: Container(
  //                               child: Center(
  //                             child: MyText(text:
  //                               'Cancel',
  //                               style: TextStyle(
  //                                 color: Pallet.dashcontainerback,
  //                                 fontSize: 15,
  //                               ),
  //                             ),
  //                           )),
  //                         ),
  //                         onTap: () {
  //                           _oldpwd = oldpwdCon.text;
  //                           oldpwdCon.text = "";
  //                           setState(() {
  //                             _oldpwd = null;
  //                           });

  //                           _newpwd = newpwdCon.text;
  //                           newpwdCon.text = "";
  //                           setState(() {
  //                             _newpwd = null;
  //                           });

  //                           _cnfmpwd = cnfmpwdCon.text;
  //                           cnfmpwdCon.text = "";
  //                           setState(() {
  //                             _cnfmpwd = null;
  //                           });

  //                           Navigator.of(context).pop();
  //                         },
  //                       ),
  //                       SizedBox(width: 10),
  //                       InkWell(
  //                           child: Container(
  //                             margin: EdgeInsets.only(right: 10, top: 5),
  //                             width: width == null ? 70 : 70,
  //                             height: 35,
  //                             decoration: BoxDecoration(
  //                               borderRadius: BorderRadius.circular(5),
  //                               color: Pallet.fontcolor,
  //                             ),
  //                             child: Container(
  //                                 child: Center(
  //                               child: MyText(text:
  //                                 'Save ',
  //                                 style: TextStyle(
  //                                   color: Pallet.dashcontainerback,
  //                                   fontSize: 15,
  //                                 ),
  //                               ),
  //                             )),
  //                           ),
  //                           onTap: () async {
  //                             if (_formkey.currentState.validate()) {
  //                               resetPassword(oldpwdCon.text, cnfmpwdCon.text);
  //                             }
  //                           }),
  //                     ])
  //                   ],
  //                 )),
  //               )
  //             ])
  //           ],
  //         )),
  //   );
  // }

  // void resetPassword(String old_password, String new_password) async {
  //   Map<String, dynamic> data = Utils.getSiteID();
  //   data['payload']['old_password'] = old_password;
  //   data['payload']['new_password'] = new_password;
  //   var snackBar;
  //   Map<String, dynamic> result = await HttpRequest.Post('resetPassword', data);
  //   if (Utils.isServerError(result)) {
  //     print('here');
  //     snackBar = SnackBar(content: MyText(text:'Wrong Password'));
  //     Navigator.of(context).pop();
  //     ScaffoldMessenger.of(context).showSnackBar(snackBar);
  //   } else {
  //     snackBar = SnackBar(content: MyText(text:'Password Changed Successfully'));
  //     Navigator.of(context).pop();
  //     ScaffoldMessenger.of(context).showSnackBar(snackBar);
  //   }
  // }
}

Pattern pattern =
    r'^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[!@#\$&*~]).{8,}$';
RegExp regex = new RegExp(pattern);

// class SettingsOld extends StatefulWidget {
//   final double wdgtWidth, wdgtHeight;

//   const SettingsOld({Key key, this.wdgtWidth, this.wdgtHeight})
//       : super(key: key);
//   @override
//   _SettingsOldState createState() => _SettingsOldState();
// }

// class _SettingsOldState extends State<SettingsOld> {
//   Map<String, dynamic> pageDetails = {};
//   bool is2faSwitched = false;
//   _SettingsOldState() {
//     setPageDetails();
//     SampleClass().login(
//         {"username": "testing", "password": "Eg@email.com1", "product_id": 1},
//         (value) {
//       SampleClass().getDocuments(output);
//     });
//   }
//   output(value) {}

//   void setPageDetails() async {
//     String data = await Utils.getPageDetails('settings');
//     setState(() {
//       pageDetails = Utils.fromJSONString(data);
//     });
//   }

//   @override
//   Widget build(BuildContext context) {
//     return settingscreemn(wdgtwidth: widget.wdgtWidth);
//   }

//   Widget settingscreemn({double wdgtwidth}) {
//     return Container(
//       width: wdgtwidth,
//       child: Center(
//         child: Container(
//           padding: EdgeInsets.all(Pallet.defaultPadding),
//           // color: Pallet.inner1,
//           width: wdgtwidth * 0.7,
//           child:
//               Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
//             MyText(text:pageDetails["text1"],
//                 style: TextStyle(fontSize: 16.0, color: Pallet.fontcolor)),
//             SizedBox(height: wdgtwidth * .05),
//             // Row(
//             //   children: [
//             //     Expanded(
//             //       child: MyText(text:
//             //         pageDetails["text2"],
//             //         style: TextStyle(
//             //           color: Pallet.fontcolor,
//             //           fontSize: 16.0,
//             //         ),
//             //       ),
//             //     ),
//             //     Switch(
//             //         value: Pallet.isSwitched,
//             //         activeThumbImage: NetworkImage(
//             //             'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTFIAWxsU2qEI995BYW36GbbGLS8NzlGTzvvg&usqp=CAU'),
//             //         inactiveThumbImage: NetworkImage(
//             //             'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQd5jIZV7weqa_JiJGbrVPHnCJowW_K5_itkA&usqp=CAU'),
//             //         onChanged: (value) {
//             //           if (value) {
//             //             setState(() {
//             //               Pallet.isSwitched = value;
//             //               print('yay');
//             //               Pallet.litTheme();
//             //             });
//             //           } else {
//             //             setState(() {
//             //               Pallet.isSwitched = value;

//             //               Pallet.darkTheme();
//             //             });
//             //           }
//             //           // Navigator.popAndPushNamed(context, );
//             //           Navigator.pop(context);
//             //           Navigator.push(
//             //               context,
//             //               MaterialPageRoute(
//             //                   builder: (context) => Home(
//             //                         route: 'payout',
//             //                       )));
//             //         }),
//             //   ],
//             // ),
//             SizedBox(height: wdgtwidth * .05),
//             Row(
//               children: [
//                 Expanded(
//                   child: MyText(text:
//                     pageDetails["text3"],
//                     style: TextStyle(
//                       color: Pallet.fontcolor,
//                       fontSize: 16.0,
//                     ),
//                   ),
//                 ),
//                 Switch(
//                     value: is2faSwitched,
//                     onChanged: (value) {
//                       setState(() {
//                         Navigator.push(
//                           context,
//                           MaterialPageRoute(
//                               builder: (context) =>
//                                   Home(route: 'account_pwd2')),
//                         );
//                       });
//                     }),
//               ],
//             ),
//             SizedBox(height: wdgtwidth * .05),
//             MyText(text:
//               pageDetails["text4"],
//               style: TextStyle(fontSize: 16.0, color: Pallet.fontcolor),
//             ),
//             SizedBox(height: wdgtwidth * .05),
//             new Theme(
//                 data: Theme.of(context).copyWith(
//                   canvasColor: Pallet.inner1,
//                 ),
//                 child: DropdownButton<String>(
//                   isExpanded: true,
//                   value: Pallet.option,
//                   onChanged: (String newValue) {
//                     setState(() {
//                       Pallet.changetheme(newValue, context);
//                     });
//                   },
//                   items: Pallet.options
//                       .map<DropdownMenuItem<String>>((String value) {
//                     return DropdownMenuItem<String>(
//                       value: value,
//                       child: MyText(text:
//                         value,
//                         style:
//                             TextStyle(fontSize: 16.0, color: Pallet.fontcolor),
//                       ),
//                     );
//                   }).toList(),
//                 )),
//             SizedBox(height: wdgtwidth * .05),
//             MyText(text:
//               pageDetails["text5"],
//               style: TextStyle(
//                 color: Pallet.fontcolornew,
//                 fontSize: Pallet.heading3,
//                 fontWeight: Pallet.subheading1wgt,
//               ),
//             ),
//             SizedBox(height: wdgtwidth * .05),
//             new Theme(
//                 data: Theme.of(context).copyWith(
//                   canvasColor: Pallet.inner1,
//                 ),
//                 child: DropdownButton<String>(
//                   isExpanded: true,
//                   value: Pallet.language,
//                   onChanged: (String newValue) {
//                     setState(() {
//                       Pallet.changelang(newValue);
//                     });
//                   },
//                   items: Pallet.languages
//                       .map<DropdownMenuItem<String>>((String value) {
//                     return DropdownMenuItem<String>(
//                       value: value,
//                       child: MyText(text:
//                         value,
//                         style:
//                             TextStyle(fontSize: 16.0, color: Pallet.fontcolor),
//                       ),
//                     );
//                   }).toList(),
//                 )),
//           ]),
//         ),
//       ),
//     );
//   }
// }

// ignore: camel_case_types
class changepasswordalert extends StatefulWidget {
  final width;
  final pageDetails;
  const changepasswordalert({
    Key key,
    this.width,
    this.pageDetails,
  }) : super(key: key);
  @override
  _changepasswordalertState createState() => _changepasswordalertState();
}

// ignore: camel_case_types
class _changepasswordalertState extends State<changepasswordalert> {
  var errorTxt = MyText(text: "fbwe");
  bool secureText1 = true;
  bool secureText2 = true;
  bool secureText3 = true;
  var visiblity = Icon(Icons.visibility_off);
  // bool _validate = false;

  TextEditingController oldpwdCon = TextEditingController();
  TextEditingController newpwdCon = TextEditingController();
  TextEditingController cnfmpwdCon = TextEditingController();

  String oldpwdError;
  String newpwdError;
  String cfmpwdError;

  FocusNode oldpassnode = FocusNode();
  FocusNode newpassnode = FocusNode();
  FocusNode cfmpassnode = FocusNode();
  FocusNode submitnode = FocusNode();

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 455,
      child: AlertDialog(
        backgroundColor: Pallet.popupcontainerback,
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(Pallet.radius)),
        title: Row(
          children: [
            Image.asset(
              'assets/c-logo.png',
              width: 40,
            ),
            SizedBox(width: 10),
            Align(
              alignment: Alignment.topLeft,
              child: MyText(
                text: widget.pageDetails['text6'],
                style: TextStyle(
                  fontSize: 15,
                  color: Pallet.fontcolor,
                ),
              ),
            ),
          ],
        ),
        content: Container(
          width: 455,
          child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisSize: MainAxisSize.min,
              children: [
                Column(
                  children: [
                    RawKeyboardListener(
                      focusNode: oldpassnode,
                      onKey: (event) {
                        if (event.isKeyPressed(LogicalKeyboardKey.tab)) {
                          FocusScope.of(context).requestFocus(newpassnode);
                        }
                      },
                      child: TextFormField(
                        onEditingComplete: () {
                          changepass();
                        },
                        // ignore: missing_return
                        validator: (value) {
                          print(value);

                          if (value.trim().isEmpty) {
                            setState(() {
                              oldpwdError = widget.pageDetails['text20'];
                            });
                          } else {
                            setState(() {
                              oldpwdError = null;
                            });
                          }
                        },
                        onChanged: (value) {
                          print(value);
                          if (value.trim().isEmpty) {
                            setState(() {
                              oldpwdError = widget.pageDetails['text20'];
                            });
                          } else {
                            setState(() {
                              oldpwdError = null;
                            });
                          }
                        },
                        style: TextStyle(color: Pallet.dashcontainerback),
                        controller: oldpwdCon,
                        cursorColor: Pallet.fontcolornew,
                        inputFormatters: [
                          FilteringTextInputFormatter.deny(RegExp(' '))
                        ],
                        decoration: InputDecoration(
                          errorText: oldpwdError,
                          errorStyle: TextStyle(
                            color: Pallet.fontcolor,
                          ),

                          contentPadding:
                              EdgeInsets.fromLTRB(10.0, 10.0, 20.0, 10.0),
                          hintText: widget.pageDetails['text7'],
                          prefixIcon: Icon(
                            Icons.lock_rounded,
                            color: Pallet.dashcontainerback,
                          ),
                          suffixIcon: IconButton(
                            color: Pallet.dashcontainerback,
                            icon: Icon(secureText1
                                ? Icons.visibility_off
                                : Icons.visibility),
                            onPressed: () {
                              setState(() {
                                secureText1 = !secureText1;
                              });
                            },
                          ),
                          filled: true,
                          hintStyle: TextStyle(
                            color: Pallet.dashcontainerback,
                            fontSize: Pallet.heading4,
                          ),
                          // enabledBorder: OutlineInputBorder(
                          //   borderSide: BorderSide(
                          //       color: Pallet.dashcontainerback,
                          //       width: 1.5),
                          // ),
                          border: OutlineInputBorder(
                              borderSide: const BorderSide(
                                  color: Color(0XFF1034a6), width: 1.0),
                              borderRadius: BorderRadius.circular(8.0)),
                          errorBorder: OutlineInputBorder(
                              borderSide:
                                  const BorderSide(color: Colors.red, width: 2),
                              borderRadius: BorderRadius.circular(8.0)),
                          fillColor: Pallet.fontcolor,
                          focusedBorder: OutlineInputBorder(
                              borderSide: const BorderSide(
                                  color: Color(0XFF1034a6), width: 1.0),
                              borderRadius: BorderRadius.circular(8.0)),
                          enabledBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                                color: Color(0xFF4570ae), width: 0.0),
                            borderRadius: BorderRadius.circular(8.0),
                          ),
                        ),
                        obscureText: secureText1,
                      ),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    RawKeyboardListener(
                      focusNode: newpassnode,
                      onKey: (event) {
                        if (event.isKeyPressed(LogicalKeyboardKey.tab)) {
                          FocusScope.of(context).requestFocus(cfmpassnode);
                        }
                      },
                      child: TextFormField(
                        onEditingComplete: () {
                          changepass();
                        },
                        // ignore: missing_return
                        validator: (value) {
                          setState(() {
                            checkPassword(value);
                          });
                        },
                        onChanged: (value) {
                          setState(() {
                            checkPassword(value);
                          });
                        },

                        style: TextStyle(color: Pallet.dashcontainerback),
                        controller: newpwdCon,
                        cursorColor: Pallet.fontcolornew,
                        inputFormatters: [
                          FilteringTextInputFormatter.deny(RegExp(' '))
                        ],
                        decoration: InputDecoration(
                          errorText: newpwdError,
                          errorStyle: TextStyle(
                            color: Pallet.fontcolor,
                          ),
                          contentPadding:
                              EdgeInsets.fromLTRB(10.0, 10.0, 20.0, 10.0),
                          hintText: widget.pageDetails['text8'],
                          prefixIcon: Icon(
                            Icons.lock_rounded,
                            color: Pallet.dashcontainerback,
                          ),
                          suffixIcon: IconButton(
                            color: Pallet.dashcontainerback,
                            icon: Icon(secureText2
                                ? Icons.visibility_off
                                : Icons.visibility),
                            onPressed: () {
                              setState(() {
                                secureText2 = !secureText2;
                              });
                            },
                          ),
                          hintStyle: TextStyle(
                            color: Pallet.dashcontainerback,
                            fontSize: Pallet.heading4,
                          ),
                          filled: true,
                          errorBorder: OutlineInputBorder(
                              borderSide:
                                  const BorderSide(color: Colors.red, width: 2),
                              borderRadius: BorderRadius.circular(8.0)),
                          border: OutlineInputBorder(
                              borderSide: const BorderSide(
                                  color: Color(0XFF1034a6), width: 1.0),
                              borderRadius: BorderRadius.circular(8.0)),
                          fillColor: Pallet.fontcolor,
                          focusedBorder: OutlineInputBorder(
                              borderSide: const BorderSide(
                                  color: Color(0XFF1034a6), width: 1.0),
                              borderRadius: BorderRadius.circular(8.0)),
                          enabledBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                                color: Color(0xFF4570ae), width: 0.0),
                            borderRadius: BorderRadius.circular(8.0),
                          ),
                        ),
                        obscureText: secureText2,
                      ),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    RawKeyboardListener(
                      focusNode: cfmpassnode,
                      onKey: (event) {
                        if (event.isKeyPressed(LogicalKeyboardKey.tab)) {
                          FocusScope.of(context).requestFocus(submitnode);
                        }
                      },
                      child: TextFormField(
                        onEditingComplete: () {
                          changepass();
                        },

                        // ignore: missing_return
                        validator: (value) {
                          setState(() {
                            checkReTypePassword(value);
                          });
                        },
                        onChanged: (value) {
                          setState(() {
                            checkReTypePassword(value);
                          });
                        },
                        style: TextStyle(color: Pallet.dashcontainerback),
                        controller: cnfmpwdCon,
                        cursorColor: Pallet.fontcolornew,
                        inputFormatters: [
                          FilteringTextInputFormatter.deny(RegExp(' '))
                        ],
                        decoration: InputDecoration(
                          errorText: cfmpwdError,
                          errorStyle: TextStyle(
                            color: Pallet.fontcolor,
                          ),
                          contentPadding:
                              EdgeInsets.fromLTRB(10.0, 10.0, 20.0, 10.0),
                          hintText: widget.pageDetails['text9'],
                          hintStyle: TextStyle(
                            color: Pallet.dashcontainerback,
                            fontSize: Pallet.heading4,
                          ),
                          prefixIcon: Icon(
                            Icons.lock_rounded,
                            color: Pallet.dashcontainerback,
                          ),
                          suffixIcon: IconButton(
                            color: Pallet.dashcontainerback,
                            icon: secureText3 == true
                                ? Icon(Icons.visibility_off)
                                : Icon(Icons.visibility),
                            onPressed: () {
                              setState(() {
                                secureText3 = !secureText3;
                              });
                            },
                          ),
                          filled: true,
                          errorBorder: OutlineInputBorder(
                              borderSide:
                                  const BorderSide(color: Colors.red, width: 2),
                              borderRadius: BorderRadius.circular(8.0)),
                          border: OutlineInputBorder(
                              borderSide: const BorderSide(
                                  color: Color(0XFF1034a6), width: 1.0),
                              borderRadius: BorderRadius.circular(8.0)),
                          fillColor: Pallet.fontcolor,
                          focusedBorder: OutlineInputBorder(
                              borderSide: const BorderSide(
                                  color: Color(0XFF1034a6), width: 1.0),
                              borderRadius: BorderRadius.circular(8.0)),
                          enabledBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                                color: Color(0xFF4570ae), width: 0.0),
                            borderRadius: BorderRadius.circular(8.0),
                          ),
                        ),
                        obscureText: secureText3,
                      ),
                    ),
                    // SizedBox(
                    //   height: 15,
                    // ),
                    // Container(
                    //   width: 455,
                    //   decoration: BoxDecoration(
                    //     borderRadius: BorderRadius.circular(10),
                    //     color: Pallet.fontcolor,
                    //   ),
                    //   child: Container(
                    //     padding: EdgeInsets.all(10),
                    //     child: Column(
                    //       crossAxisAlignment: CrossAxisAlignment.start,
                    //       children: [
                    //         MyText(text:widget.pageDetails["text17"],
                    //             style: TextStyle(
                    //               color: Pallet.dashcontainerback,
                    //               fontWeight: FontWeight.w900,
                    //             )),
                    //         SizedBox(
                    //           height: 3,
                    //         ),
                    //         MyText(text:
                    //           widget.pageDetails["text18"],
                    //           style: TextStyle(
                    //             color: Pallet.dashcontainerback,
                    //             fontSize: 13,
                    //           ),
                    //         ),
                    //       ],
                    //     ),
                    //   ),
                    // ),
                  ],
                )
              ]),
        ),
        actions: [
          Row(mainAxisAlignment: MainAxisAlignment.end, children: [
            PopupButton(
              text: widget.pageDetails["text27"],
              onpress: () {
                Navigator.of(context).pop();

                setState(() {
                  oldpwdCon.text = null;
                  oldpwdError = null;
                  newpwdCon.text = null;
                  newpwdError = null;
                  cnfmpwdCon.text = null;
                  cfmpwdError = null;
                });
              },
            ),
            SizedBox(width: 10),
            RawKeyboardListener(
              focusNode: submitnode,
              onKey: (event) {
                if (event.isKeyPressed(LogicalKeyboardKey.tab)) {
                  FocusScope.of(context).unfocus();
                }
              },
              child: PopupButton(
                text: widget.pageDetails["text28"],
                onpress: changepass,
              ),
            )
          ])
        ],
      ),
    );
  }

  changepass() async {
    print(",mnbvgfgyhjskl,mnb");
    print(newpwdCon.text);
    print(oldpwdCon.text);
    print(cnfmpwdCon.text);
    print(",msnhgssssssssssssssssssss");
    if (oldpwdCon.text.isEmpty ||
        oldpwdCon.text == null ||
        oldpwdCon.text == '') {
      setState(() {
        oldpwdError = widget.pageDetails['text20'];
      });
    }
    if (newpwdCon.text.isEmpty ||
        newpwdCon.text == null ||
        newpwdCon.text == '') {
      setState(() {
        newpwdError = widget.pageDetails['text21'];
      });
    }
    if (cnfmpwdCon.text.isEmpty ||
        cnfmpwdCon.text == null ||
        cnfmpwdCon.text == '') {
      setState(() {
        cfmpwdError = widget.pageDetails['text25'];
      });
    } else if (newpwdCon.text == cnfmpwdCon.text) {
      newpwdError = null;
      cfmpwdError = null;

      waiting.waitpopup(context);
      Map<String, dynamic> _map = {
        "old_password": oldpwdCon.text,
        "new_password": newpwdCon.text,
        'product_id': 1,
      };
      var _result =
          await HttpRequest.Post('resetPassword', Utils.constructPayload(_map));
      print("zzzzzzzzzzzzzzzzzzzzzzzzzzzz");
      print(_result);
      if (Utils.isServerError(_result)) {
        Navigator.pop(context);
        if (_result['response']['error'] == 'wrong_combination') {
          snack.snack(title: widget.pageDetails['text1']);
          // final snackBar = SnackBar(content: MyText(text:widget.pageDetails['text1']));
          // ScaffoldMessenger.of(context).showSnackBar(snackBar);
        } else {
          snack.snack(title: widget.pageDetails['text39']);

          // final snackBar =
          //     SnackBar(content: MyText(text:widget.pageDetails['text39']));
          // ScaffoldMessenger.of(context).showSnackBar(snackBar);
        }
      } else {
        var val = _result['response']['data'].toString();
        print(val);
        Navigator.pop(context);
        Navigator.of(context).pop();
        snack.snack(title: widget.pageDetails['text40']);

        // snackBar = SnackBar(content: MyText(text:widget.pageDetails['text40']));
        // ScaffoldMessenger.of(context).showSnackBar(snackBar);
        SharedPreferences prefs = await SharedPreferences.getInstance();
        prefs.remove("shopbannershow");
        prefs.remove("reload");
        prefs.remove("newshow");
        prefs.remove("sessiontime");
        Navigator.pushReplacement(context,
            MaterialPageRoute(builder: (BuildContext context) => Login()));
      }
      // resetPassword(oldpwdCon.text, cnfmpwdCon.text);
    }
  }

  checkPassword(value) {
    if (value.trim().isEmpty) {
      newpwdError = widget.pageDetails['text21'];
    } else if (value.length < 8) {
      newpwdError = widget.pageDetails['text22'];
    } else if (!RegExp("^(?=.*[!@#\$%\^&\*])").hasMatch(value)) {
      newpwdError = widget.pageDetails['text31'];
    } else if (!RegExp(".*[0-9].*").hasMatch(value)) {
      newpwdError = widget.pageDetails['text32'];
    } else if (!RegExp(".*[A-Z].*").hasMatch(value)) {
      newpwdError = widget.pageDetails['text33'];
    } else if (!RegExp(".*[a-z].*").hasMatch(value)) {
      newpwdError = widget.pageDetails['text34'];
    } else if (oldpwdCon.text == newpwdCon.text) {
      newpwdError = widget.pageDetails['text24'];
    } else {
      newpwdError = null;
    }
    if (cnfmpwdCon.text.isEmpty) {
    } else {
      if (value == cnfmpwdCon.text) {
        cfmpwdError = null;
      } else if (value != cnfmpwdCon.text) {
        cfmpwdError = widget.pageDetails['text26'];
      }
    }
  }

  checkReTypePassword(value) {
    if (value.trim().isEmpty) {
      cfmpwdError = widget.pageDetails['text25'];
    } else if (newpwdCon.text != cnfmpwdCon.text) {
      cfmpwdError = widget.pageDetails['text37'];
    } else {
      cfmpwdError = null;
    }
  }
}
