import 'package:centurion/config/app_settings.dart';
import 'package:flutter/material.dart';
import '../screens/iframe.dart';
import '../utils/utils.dart' show Utils;
import '../services/communication/index.dart' show HttpRequest;

class KYC extends StatefulWidget {
  final double wdgtWidth, wdgtHeight;

  const KYC({Key key, this.wdgtWidth, this.wdgtHeight}) : super(key: key);
  @override
  _KYCState createState() => _KYCState();
}

class _KYCState extends State<KYC> {
  Future<String> _temp;
  String token;

  @override
  void initState() {
    _temp = getInitToken();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: _temp,
      builder: (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
        if (snapshot.hasData) {
          return Container(
            height: widget.wdgtHeight,
            width: widget.wdgtWidth,
            child: IframeView(link: appSettings['KYC_URL'] + "/" + token),
          );
        } else {
          return Container();
        }
      },
    );
  }

  Future<String> getInitToken() async {
    Map<String, dynamic> result = await HttpRequest.Post('getScreeningToken', {
      "payload": {"system_product_id": 1}
    });
    if (Utils.isServerError(result)) {
      return await Utils.getMessage(result['response']['error']);
    } else {
      token = result['response']['data']['is_token'];
      print('%%%%%%%%%%%%%%%');
      print(token);
      return 'start';
    }
  }
}
// http://192.168.1.131:5001/static/redirect-kyc?token=96b4390b-e256-4785-a46d-82850b7682a0
