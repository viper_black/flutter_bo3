import 'dart:io';
import 'package:centurion/lottery/config.dart';
import 'package:centurion/lottery/dropdown/drop.dart';
import 'package:centurion/services/business/account.dart';
import 'package:centurion/services/communication/index.dart';
import 'package:centurion/utils/utils.dart';
import 'package:dropdown_date_picker/dropdown_date_picker.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:open_file/open_file.dart';
import 'package:path_provider/path_provider.dart';
import 'package:pdf/pdf.dart';
import 'package:printing/printing.dart';
import '../common.dart';
import '../config.dart';
import 'pdf/examples.dart';

class Commissions extends StatefulWidget {
  final double wdgtWidth, wdgtHeight;

  Commissions({Key key, this.wdgtWidth, this.wdgtHeight}) : super(key: key);
  @override
  _CommissionsState createState() => _CommissionsState();

  static final now = DateTime.now();

  final dropdownDatePicker = DropdownDatePicker(
    firstDate: ValidDate(year: now.year - 100, month: 1, day: 1),
    lastDate: ValidDate(year: now.year, month: now.month, day: now.day),
    textStyle: TextStyle(fontWeight: FontWeight.bold),
    dropdownColor: Colors.blue[200],
    dateHint: DateHint(year: 'year', month: 'month', day: 'day'),
    ascending: false,
  );
}

class _CommissionsState extends State<Commissions> {
  Future<String> temp;
  bool isMobile = false;

  var report;
  var profile;
  Future<String> monthlyreport(
    String timeperiod,
  ) async {
    Map<String, dynamic> _map = {'product_id': 1, 'time_period': timeperiod};
    print('TIMEPERIOD: ${timeperiod}');
    Map<String, dynamic> result = await HttpRequest.Post(
        'monthlycommissions', Utils.constructPayload(_map));
    print('xxxxxxxxxxxxxzzzzzzzzzzzzzzzzzzz');
    // ignore: unnecessary_brace_in_string_interps
    print('_MAP: ${_map}');
    print(result);

    if (Utils.isServerError(result)) {
      dropval.statesink.add(dropval.showcard = '');
      snack.snack(title: 'Something Went Wrong');
      throw (await Utils.getMessage(result['response']['error']));
    } else {
      print('fff');
      List list = result['response']['data']['report'];
      list.length == 0 ? print("dddddddddddd") : print("gggggggggggggg");

      report =
          list.length == 0 ? null : result['response']['data']['report'][0];

      profile = result['response']['data']['profile'];
      print('REPORT: ${result.length}');
      print(profile);
      print(result);
      dropval.statesink.add(dropval.showcard = 'yes');

      return 'starrt';
    }
  }

  @override
  void initState() {
    super.initState();

    temp = dashboard.dashboard();
    // dropval();
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
  }

  void _showPrintedToast(BuildContext context) {
    ScaffoldMessenger.of(context).showSnackBar(
      const SnackBar(
        content: Text('Document printed successfully'),
      ),
    );
  }

  void _showSharedToast(BuildContext context) {
    ScaffoldMessenger.of(context).showSnackBar(
      const SnackBar(
        content: Text('Document shared successfully'),
      ),
    );
  }

  Future<void> _saveAsFile(
    BuildContext context,
    LayoutCallback build,
    PdfPageFormat pageFormat,
  ) async {
    final bytes = await build(pageFormat);

    final appDocDir = await getTemporaryDirectory();
    final appDocPath = appDocDir.path;
    final file = File(appDocPath + '/' + 'Commission_Report.pdf');
    print('Save as file ${file.path} ...');
    await file.writeAsBytes(bytes);
    await OpenFile.open(file.path);
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
        future: temp,
        builder: (context, snapshot) {
          if (snapshot.hasError) {
            if (snapshot.error == 'Something Went Wrong') {
              print(snapshot.error);
            } else {
              return Error(
                message: snapshot.error != null ? snapshot.error : '',
              );
            }
          } else if (snapshot.hasData) {
            return LayoutBuilder(builder: (context, constraints) {
              if (constraints.maxWidth <= 800) {
                isMobile = true;
                return SingleChildScrollView(
                  child: Container(
                    width: widget.wdgtWidth,
                    child: Padding(
                      padding: EdgeInsets.only(
                          left: Pallet.defaultPadding,
                          top: Pallet.topPadding1,
                          right: Pallet.defaultPadding),
                      child: Center(
                        child: commission(wdgtWidth: widget.wdgtWidth),
                      ),
                    ),
                  ),
                );
              } else {
                isMobile = false;
                return SingleChildScrollView(
                  child: Container(
                    width: widget.wdgtWidth,
                    child: Padding(
                      padding: EdgeInsets.only(
                          left: Pallet.defaultPadding,
                          top: Pallet.topPadding1,
                          right: Pallet.defaultPadding),
                      child: Center(
                        child: commission(wdgtWidth: widget.wdgtWidth),
                      ),
                    ),
                  ),
                );
              }
            });
          }
          return Loader();
        });
  }

  TextEditingController typecontroller = TextEditingController();
  String typeError;
  DateTime date = DateTime.now();
  String typeval = '';
  // String showcard = '';

  // List<String> typelist = [];

  List month = [
    'jan',
    'feb',
    'mar',
    'apr',
    'may',
    'jun',
    'jul',
    'aug',
    'sep',
    'oct',
    'nov',
    'dec'
  ];
  List monthnumber = [
    {
      'jan': '01',
      'feb': '02',
      'mar': '03',
      'apr': '04',
      'may': '05',
      'jun': '06',
      'jul': '07',
      'aug': '08',
      'sep': '09',
      'oct': '10',
      'nov': '11',
      'dec': '12'
    }
  ];

  // dropval() {
  //   print("gggggggggggggggg");
  //   print(month[0]);
  //   print(month[1]);

  //   DateTime date = DateTime.now();

  //   var currentyear = date.year;
  //   var currentmonth = date.month;

  //   DateTime createddate = DateTime.parse(dashboard.createdate);

  //   while (currentyear >= createddate.year) {
  //     if (currentmonth > 0) {
  //       if (currentyear > createddate.year) {
  //         typelist.add(currentyear.toString() +
  //             ' ' +
  //             month[currentmonth - 1].toString().allInCaps);
  //       } else if (currentyear == createddate.year) {
  //         if (currentmonth >= createddate.month) {
  //           typelist.add(currentyear.toString() +
  //               ' ' +
  //               month[currentmonth - 1].toString().allInCaps);
  //         }
  //       } else {
  //         print(currentmonth >= createddate.month &&
  //             currentyear <= createddate.year);
  //       }

  //       currentmonth--;
  //     } else if (currentmonth == 0) {
  //       currentyear -= 1;
  //       currentmonth = 12;
  //       print(typelist);
  //       print("sssssssssss");
  //     }
  //   }
  // }

  commission({wdgtWidth, wdgtHeight}) {
    final actions = <PdfPreviewAction>[
      if (!kIsWeb)
        PdfPreviewAction(
          icon: const Icon(Icons.save),
          onPressed: _saveAsFile,
        )
    ];
    double width = MediaQuery.of(context).size.width;
    return Container(
      width: isMobile == true
          ? wdgtWidth
          : width > 800 && width < 1600
              ? wdgtWidth / 2.5
              : wdgtWidth / 3,
      child: Column(
        children: [
          Wrap(
            // crossAxisAlignment: CrossAxisAlignment.start,
            // mainAxisAlignment: MainAxisAlignment.end,

            // alignment: WrapAlignment.center,
            children: [
              Container(
                padding: EdgeInsets.only(top: 15),
                child: Text("Select the Statement Month:",
                    style: TextStyle(
                        color: Pallet.fontcolornew,
                        fontSize: 15,
                        fontWeight: FontWeight.bold)),
              ),
              SizedBox(
                width: 15,
              ),
              Container(
                width: isMobile == true ? 400 : 200,
                padding: const EdgeInsets.only(bottom: 5),
                decoration: BoxDecoration(
                  color: Pallet.dashcontainerback,
                  borderRadius: BorderRadius.circular(10.0),
                  // color: Pallet.component,
                ),
                child: DropdownButtonHideUnderline(
                  // child: RawKeyboardListener(
                  //   focusNode: countrynode,
                  child: DropDownField(
                    labelText: "",
                    divcolor: Pallet.fontcolor,
                    labelStyle: TextStyle(
                        color: Pallet.fontcolor,
                        fontSize: 12.0,
                        fontWeight: Pallet.font500),
                    enabled: true,
                    controller: typecontroller,
                    value: typeval,
                    items: dashboard.typelist,
                    onValueChanged: (value) {
                      setState(() {
                        typeval = value;

                        var val = value.toString().split(' ');
                        print('VAL: ${val}');

                        monthlyreport((val[0].toString() +
                                '-' +
                                monthnumber[0][val[1].toString().toLowerCase()]
                                    .toString() +
                                '-' +
                                '01')
                            .toString());
                      });
                    },
                    itemsVisibleInDropdown: 5,
                    textStyle: TextStyle(
                        color: Pallet.fontcolor, fontSize: Pallet.heading5),
                  ),
                ),
              ),
              // ClipOval(
              //   child: Container(
              //     color: Pallet.fontcolornew,
              //     child: IconButton(
              //         icon: Icon(
              //           Icons.calendar_today,
              //           color: Colors.white,
              //         ),
              //         onPressed: () async {
              //           DateTime createddate =
              //               DateTime.parse(dashboard.createdate);
              //           print('CREATEDDATE: ${createddate}');
              //           DateTime datenow = DateTime.now();
              //           print('DATENOW: ${datenow}');

              //           final DateTime date = await showDatePicker(
              //             context: context,
              //             initialDate: typecontroller.text == null ||
              //                     typecontroller.text == ''
              //                 ? DateTime(
              //                     datenow.year,
              //                     datenow.month,
              //                     datenow.day - 1,
              //                   )
              //                 : DateTime(
              //                     typecontroller.text
              //                         .toString()
              //                         .split(' ')[0]
              //                         .toint(),
              //                     DateTime.parse(typecontroller.text),
              //                     datenow.day - 1,
              //                   ),
              //             firstDate: DateTime(createddate.year,
              //                 createddate.month, createddate.day),
              //             initialEntryMode: DatePickerEntryMode.calendar,
              //             lastDate: DateTime(
              //               datenow.year,
              //               datenow.month,
              //               datenow.day - 1,
              //             ),
              //           );
              //           print('DATE: ${date}');

              //           if (date != null) {
              //             monthlyreport((date.year.toString() +
              //                     '-' +
              //                     date.month.toString().toString() +
              //                     '-' +
              //                     '01')
              //                 .toString());

              //             typecontroller.text = date.year.toString() +
              //                 ' ' +
              //                 month[date.month - 1].toString().allInCaps;
              //           }
              //         }),
              //   ),
              // ),
            ],
          ),
          SizedBox(
            height: 15,
          ),
          StreamBuilder(
              stream: dropval.statestream,
              builder: (context, snapshot) {
                if (snapshot.hasData) {
                  print('SNAPSHOT.DATA: ${snapshot.data}');
                  if (snapshot.data == '')
                    return Container();
                  else
                    return Container(
                      width: isMobile == true ? wdgtWidth : wdgtWidth * .7,
                      decoration: BoxDecoration(
                        border: Border.all(color: Pallet.fontcolornew),
                      ),
                      padding: EdgeInsets.all(10),
                      child: Column(
                        children: [
                          Container(
                            decoration: BoxDecoration(
                              color: Pallet.popupcontainerback,
                              borderRadius:
                                  BorderRadius.circular(Pallet.radius),
                            ),
                            height: isMobile == true ? 120 : 150,
                            width: double.infinity,
                            child: Center(
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Text("The Network By ",
                                      style: TextStyle(
                                          color: Pallet.fontcolor,
                                          fontSize: 15)),
                                  Image.asset("c-logo.png", width: 40),
                                  Text(" CENTURION",
                                      style: TextStyle(
                                          color: Pallet.fontcolor,
                                          fontSize: 15)),
                                ],
                              ),
                            ),
                          ),
                          SizedBox(
                            height: isMobile == true ? 10 : 20,
                          ),
                          report != null
                              ? Container(
                                  height: widget.wdgtHeight / 3,
                                  child: content())
                              : Container(
                                  height: widget.wdgtHeight / 3,
                                  child: Center(
                                    child: Text(
                                      "Sorry No Commission Earned for This Month ",
                                      style: TextStyle(
                                        color: Pallet.fontcolornew,
                                        fontSize: Pallet.heading3,
                                      ),
                                    ),
                                  ),
                                ),
                          SizedBox(height: isMobile == true ? 10 : 20),
                          Container(
                            padding: EdgeInsets.symmetric(
                                horizontal: isMobile == true ? 15 : 30,
                                vertical: 10),
                            decoration: BoxDecoration(
                              color: Pallet.popupcontainerback,
                              borderRadius:
                                  BorderRadius.circular(Pallet.radius),
                            ),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              crossAxisAlignment: CrossAxisAlignment.end,
                              children: [
                                Container(
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Text("Centurion Global Limited ",
                                          style: TextStyle(
                                              color: Pallet.fontcolor,
                                              fontSize:
                                                  isMobile == true ? 11 : 13)),
                                      SizedBox(height: 3),
                                      Text("Quality Corporate Services Ltd.,",
                                          style: TextStyle(
                                              color: Pallet.fontcolor,
                                              fontSize:
                                                  isMobile == true ? 11 : 13)),
                                      SizedBox(height: 3),
                                      Text(
                                          "Suite 102, Cannon Place, P.O. Box 712,",
                                          style: TextStyle(
                                              color: Pallet.fontcolor,
                                              fontSize:
                                                  isMobile == true ? 11 : 13)),
                                      SizedBox(height: 3),
                                      Text("North Sound Road, George Town,",
                                          style: TextStyle(
                                              color: Pallet.fontcolor,
                                              fontSize:
                                                  isMobile == true ? 11 : 13)),
                                      SizedBox(height: 3),
                                      Text(
                                          "Grand Cayman, KY1-9006, Cayman Islands",
                                          style: TextStyle(
                                              color: Pallet.fontcolor,
                                              fontSize:
                                                  isMobile == true ? 11 : 13)),
                                    ],
                                  ),
                                ),
                                // Container(
                                //     child: CustomButton(
                                //   buttoncolor: Pallet.dashcontainerback,
                                //   text: 'Download As PDF',
                                //   vpadding: 7,
                                //   hpadding: 10,
                                //   textcolor: Pallet.fontcolor,
                                //   onpress: () async {
                                //     // generateInvoice();
                                //   },
                                // ))
                              ],
                            ),
                          ),
                        ],
                      ),
                    );
                } else if (snapshot.hasError) {
                  return Container();
                } else {
                  return Container();
                }
              }),
          SizedBox(
            height: isMobile == true ? 5 : 10,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              StreamBuilder(
                stream: dropval.statestream,
                builder: (context, snapshot) {
                  if (snapshot.hasData) {
                    print('SNAPSHOT.DATA: ${snapshot.data}');
                    if (snapshot.data == '')
                      return Container();
                    else
                      return report == null
                          ? Container()
                          : Container(
                              // height: 15,
                              child: PdfPreview(
                                maxPageWidth: 600,
                                build: (format) => examples[0]
                                    .builder(format, profile, report),
                                actions: actions,
                                // allowSharing: true,
                                // onPrinted: _showPrintedToast,
                                // onShared: _showSharedToast,
                              ),
                            );
                  } else if (snapshot.hasError) {
                    return Container();
                  } else {
                    return Container();
                  }
                },
              )
            ],
          ),
        ],
      ),
    );
  }

  Widget content() {
    // var diff1 = DateTime.now();
    var diff = DateTime.parse(report['end_of_month'].toString());

    // var diifference=
    DateTime dateval = diff;
    print('DATEVAL: ${dateval}');
    String enddate;

    if (dateval.year == DateTime.now().year) {
      if (dateval.month == DateTime.now().month) {
        enddate = dateval.year.toString() +
            '-' +
            dateval.month.toString() +
            '-' +
            DateTime.now().subtract(Duration(days: 1)).day.toString();
      } else {
        // DateTime dateval = diff.add(Duration(hours: dtime, minutes: dmin));

        enddate = dateval.toString().split(' ')[0].toString();
      }
    } else {
      // DateTime dateval = diff.add(Duration(hours: dtime, minutes: dmin));
      // print('DATEVAL: ${dateval}');

      enddate = dateval.toString().split(' ')[0].toString();
    }

    // .split('T')[0].toString();
    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: [
        Padding(
          padding: EdgeInsets.symmetric(
            horizontal: isMobile == true ? 15 : 30,
          ),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      profile['first_name'].toString() +
                          ' ' +
                          profile['last_name'].toString(),
                      style: TextStyle(
                        color: Pallet.fontcolornew,
                        fontSize: 15,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    if (profile['address_line1'] != null)
                      Text(profile['address_line1'].toString(),
                          style: TextStyle(
                            color: Pallet.fontcolornew,
                            fontSize: 15,
                            fontWeight: FontWeight.bold,
                          )),
                    if (profile['address_line2'] != null &&
                        profile['address_line2'] != '')
                      Text(profile['address_line2'].toString(),
                          style: TextStyle(
                            color: Pallet.fontcolornew,
                            fontSize: 15,
                            fontWeight: FontWeight.bold,
                          )),
                    if (profile['city'] != null)
                      Text(profile['city'].toString(),
                          style: TextStyle(
                            color: Pallet.fontcolornew,
                            fontSize: 15,
                            fontWeight: FontWeight.bold,
                          )),
                    if (report['address'][0]['country'] != null)
                      Text(report['address'][0]['country'],
                          style: TextStyle(
                            color: Pallet.fontcolornew,
                            fontSize: 15,
                            fontWeight: FontWeight.bold,
                          )),
                  ],
                ),
              ),
              Container(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text("Date",
                        style: TextStyle(
                          color: Pallet.fontcolornew,
                          fontSize: 15,
                          fontWeight: FontWeight.bold,
                        )),
                    Text(DateTime.now().toString().split(' ')[0].toString(),
                        style: TextStyle(
                          color: Pallet.fontcolornew,
                          fontSize: 15,
                          fontWeight: FontWeight.bold,
                        )),
                  ],
                ),
              )
            ],
          ),
        ),
        SizedBox(
          height: isMobile == true ? 10 : 20,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            MediaQuery.of(context).size.width < 800
                ? Expanded(
                    child: Center(
                      child: Container(
                        child: Text(
                            "Statement of Commission Earned During The Period From",
                            style: TextStyle(
                              color: Pallet.fontcolornew,
                              fontSize: 14,
                              fontWeight: FontWeight.bold,
                            )),
                      ),
                    ),
                  )
                : Container(
                    child: Text(
                        "Statement of Commission Earned During The Period From",
                        style: TextStyle(
                          color: Pallet.fontcolornew,
                          fontSize: 15,
                          fontWeight: FontWeight.bold,
                        )),
                  ),
          ],
        ),
        SizedBox(
          height: 5,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(
              decoration: BoxDecoration(
                border: Border.all(color: Colors.blue),
              ),
              padding: EdgeInsets.symmetric(vertical: 5, horizontal: 10),
              child: Center(
                child: Text(
                    report['start_of_month']
                        .toString()
                        .split('T')[0]
                        .toString(),
                    style: TextStyle(
                      color: Pallet.fontcolornew,
                      fontSize: 15,
                      fontWeight: FontWeight.bold,
                    )),
              ),
            ),
            SizedBox(
              width: 10,
            ),
            Text("to",
                style: TextStyle(
                  color: Pallet.fontcolornew,
                  fontSize: 15,
                  fontWeight: FontWeight.bold,
                )),
            SizedBox(
              width: 10,
            ),
            Container(
              decoration: BoxDecoration(
                border: Border.all(color: Colors.blue),
              ),
              padding: EdgeInsets.symmetric(vertical: 5, horizontal: 10),
              child: Center(
                child: Text(enddate,
                    style: TextStyle(
                      color: Pallet.fontcolornew,
                      fontSize: 15,
                      fontWeight: FontWeight.bold,
                    )),
              ),
            ),
          ],
        ),
        SizedBox(
          height: 5,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(
              decoration: BoxDecoration(
                border: Border.all(color: Colors.blue),
              ),
              padding: EdgeInsets.symmetric(
                  vertical: isMobile == true ? 5 : 10, horizontal: 20),
              child: Center(
                child: Text(
                    '\$ ' +
                        double.parse((report["total_commission"]).toString())
                            .toStringAsFixed(2)
                            .toString(),
                    style: TextStyle(
                      color: Pallet.fontcolornew,
                      fontSize: 15,
                      fontWeight: FontWeight.bold,
                    )),
              ),
            ),
            // SizedBox(height: isMobile == true ? 10 : 0),
          ],
        ),
      ],
    );
  }
}
