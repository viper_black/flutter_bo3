// import 'package:centurion/screens/myshop.dart';
import 'package:centurion/services/communication/http/index.dart';
import 'package:centurion/utils/utils.dart';
import 'package:flutter/material.dart';
import '../common.dart';
import '../config.dart';
import '../config/index.dart';
import '../home.dart';
// import 'package:url_launcher/url_launcher.dart';
// import 'package:centurion/services/business/account.dart';

class MyOrders extends StatefulWidget {
  final double wdgtWidth, wdgtHeight;

  const MyOrders({Key key, this.wdgtWidth, this.wdgtHeight}) : super(key: key);
  @override
  _DocumentsState createState() => _DocumentsState();
}

class _DocumentsState extends State<MyOrders> {
  Future<String> temp;
  List products = [];
  List categories = [];
  void initState() {
    temp = getProducts();
    super.initState();
  }

  Map<String, dynamic> pageDetails = {};
  _DocumentsState() {
    setPageDetails();
  }

  void setPageDetails() async {
    String data = await Utils.getPageDetails('myshoporders');
    setState(() {
      pageDetails = Utils.fromJSONString(data);
    });
  }

  int selectedIndex;

  bool isMobile = false;
  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
        future: temp,
        builder: (context, snapshot) {
          if (snapshot.hasError) {
            if (snapshot.error == 'Something Went Wrong') {
              return SomethingWentWrongMessage();
            } else {
              return Error(
                message: snapshot.error != null ? snapshot.error : '',
              );
            }
          } else if (snapshot.hasData) {
            //print(snapshot.data);
            return LayoutBuilder(builder: (context, constraints) {
              if (constraints.maxWidth < ScreenSize.verysmall) {
                isMobile = true;
                return Padding(
                  padding: EdgeInsets.only(
                      left: Pallet.defaultPadding,
                      top: Pallet.topPadding1,
                      right: Pallet.defaultPadding),
                  child: Container(
                    child: myordersmobile(
                      wdgtWidth: widget.wdgtWidth,
                      wdgtHeight: widget.wdgtWidth,
                    ),
                  ),
                );
              }
              if (constraints.maxWidth < ScreenSize.iphone) {
                isMobile = true;

                return Padding(
                  padding: EdgeInsets.only(
                      left: Pallet.defaultPadding,
                      top: Pallet.topPadding1,
                      right: Pallet.defaultPadding),
                  child: Container(
                    child: myordersmobile(
                      wdgtWidth: widget.wdgtWidth,
                      wdgtHeight: widget.wdgtWidth,
                    ),
                  ),
                );
              } else if (constraints.maxWidth > ScreenSize.iphone &&
                  constraints.maxWidth < ScreenSize.ipad) {
                return Padding(
                  padding: EdgeInsets.only(
                      left: Pallet.leftPadding, top: Pallet.topPadding1),
                  child: Container(
                    child: myordersmobile(
                      wdgtWidth: widget.wdgtWidth,
                      wdgtHeight: widget.wdgtWidth,
                    ),
                  ),
                );
              } else {
                return Padding(
                  padding: EdgeInsets.only(
                      left: Pallet.leftPadding, top: Pallet.topPadding1),
                  child: Container(
                    child: myorders(
                      wdgtWidth: widget.wdgtWidth * .8,
                      wdgtHeight: widget.wdgtWidth,
                    ),
                  ),
                );
              }
            });
          }
          return Loader();
        });
  }

  Widget myorders({double wdgtWidth, wdgtHeight}) {
    return Container(
        child: products.length == 0
            ? Container(
                padding: EdgeInsets.only(left: Pallet.defaultPadding),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    MyText(text:pageDetails['text1'],
                        style: TextStyle(
                          color: Pallet.fontcolornew,
                          fontSize: Pallet.heading1,
                        )),
                    Expanded(
                      child: Center(
                          child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Image.asset('orderempty.png',
                              height: 300, fit: BoxFit.fill),
                          // Image.network(
                          //   'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSoKtdDm7GMnb1RaSuVMPErLtLKSddB33YFbw&usqp=CAU',
                          //   height: 100,
                          //   width: 100,
                          //   fit: BoxFit.fill,
                          // ),
                          MyText(text:pageDetails['text2'],
                              style: TextStyle(
                                  color: Pallet.dashcontainerback,
                                  fontSize: Pallet.heading2,
                                  fontWeight: Pallet.heading2wgt)),
                          SizedBox(
                            height: 10,
                          ),
                          InkWell(
                            onTap: () {
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => Home(
                                            route: 'centurion_shop',
                                          )));
                            },
                            child: Padding(
                              padding: EdgeInsets.all(Pallet.defaultPadding),
                              child: Container(
                                decoration: BoxDecoration(
                                    boxShadow: [Pallet.shadowEffect],
                                    border:
                                        Border.all(color: Pallet.fontcolornew),
                                    color: Pallet.fontcolornew,
                                    borderRadius:
                                        BorderRadius.circular(Pallet.radius)),
                                child: Padding(
                                  padding:
                                      EdgeInsets.all(Pallet.defaultPadding),
                                  child: MyText(text:pageDetails['text3'],
                                      style:
                                          TextStyle(color: Pallet.fontcolor)),
                                ),
                              ),
                            ),
                          )
                        ],
                      )),
                    )
                  ],
                ),
              )
            : ListView(
                // crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  MyText(text:pageDetails['text1'],
                      style: TextStyle(
                        color: Pallet.fontcolornew,
                        fontSize: Pallet.heading1,
                      )),
                  for (var i = 0; i < products.length; i++)
                    Padding(
                      padding: EdgeInsets.all(Pallet.defaultPadding),
                      child: InkWell(
                        onTap: () {
                          setState(() {
                            if (selectedIndex == i) {
                              selectedIndex = null;
                            } else {
                              selectedIndex = i;
                            }
                          });
                        },
                        child: Container(
                            decoration: BoxDecoration(
                              boxShadow: [Pallet.shadowEffect],
                              borderRadius:
                                  BorderRadius.circular(Pallet.radius),
                              color: Pallet.fontcolor,
                            ),
                            child: Column(
                              children: [
                                Padding(
                                  padding:
                                      EdgeInsets.all(Pallet.defaultPadding),
                                  child: Container(
                                    width:
                                        // isMobile == true
                                        //     ? widget.wdgtWidth
                                        widget.wdgtWidth - 100,
                                    child: Row(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceEvenly,
                                      children: [
                                        Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            MyText(text:
                                              pageDetails['text4'],
                                              style: TextStyle(
                                                  color: Pallet.fontcolornew,
                                                  fontWeight:
                                                      Pallet.heading2wgt,
                                                  fontSize: Pallet.heading3),
                                            ),
                                            SizedBox(
                                              height: 10,
                                            ),
                                            MyText(text:
                                              pageDetails['text5'],
                                              style: TextStyle(
                                                  color: Pallet.fontcolornew,
                                                  fontSize: Pallet.heading3),
                                            ),
                                            SizedBox(
                                              height: 10,
                                            ),
                                            MyText(text:
                                              products[i]["purchase_uuid"],
                                              style: TextStyle(
                                                  color: Pallet.fontcolornew,
                                                  fontSize: Pallet.heading3),
                                            ),
                                            SizedBox(
                                              height: 10,
                                            ),
                                            MyText(text:
                                              pageDetails['text6'],
                                              style: TextStyle(
                                                  color: Pallet.fontcolornew,
                                                  fontSize: Pallet.heading3),
                                            ),
                                            SizedBox(
                                              height: 10,
                                            ),
                                            MyText(text:
                                              products[i]["purchase_date"],
                                              style: TextStyle(
                                                  color: Pallet.fontcolornew,
                                                  fontSize: Pallet.heading3),
                                            ),
                                          ],
                                        ),
                                        // Column(
                                        //   crossAxisAlignment:
                                        //       CrossAxisAlignment.start,
                                        //   children: [
                                        //     MyText(text:
                                        //       'Purchase Date',
                                        //       style: TextStyle(
                                        //           color: Pallet.fontcolornew,
                                        //           fontWeight: Pallet.font600,
                                        //           fontSize: Pallet.heading3),
                                        //     ),
                                        //     SizedBox(
                                        //       height: 10,
                                        //     ),
                                        //     MyText(text:
                                        //       products[i]["purchase_date"],
                                        //       style: TextStyle(
                                        //           color: Pallet.fontcolornew,
                                        //           fontWeight: Pallet.font600,
                                        //           fontSize: Pallet.heading3),
                                        //     ),
                                        //   ],
                                        // ),
                                        // Container(
                                        //     width: 2,
                                        //     height: 100,
                                        //     color: Pallet.fontcolornew),
                                        Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            MyText(text:
                                              pageDetails['text7'],
                                              style: TextStyle(
                                                  color: Pallet.fontcolornew,
                                                  fontWeight:
                                                      Pallet.heading2wgt,
                                                  fontSize: Pallet.heading3),
                                            ),
                                            SizedBox(
                                              height: 10,
                                            ),
                                            MyText(text:
                                              pageDetails['text8'],
                                              style: TextStyle(
                                                  color: Pallet.fontcolornew,
                                                  fontSize: Pallet.heading3),
                                            ),
                                            SizedBox(
                                              height: 10,
                                            ),
                                            MyText(text:
                                              products[i]["amount_payed"]
                                                  .toString(),
                                              style: TextStyle(
                                                  color: Pallet.fontcolornew,
                                                  fontSize: Pallet.heading3),
                                              textAlign: TextAlign.right,
                                            ),
                                            SizedBox(
                                              height: 10,
                                            ),
                                            MyText(text:
                                              pageDetails['text9'],
                                              style: TextStyle(
                                                  color: Pallet.fontcolornew,
                                                  fontSize: Pallet.heading3),
                                            ),
                                            SizedBox(
                                              height: 10,
                                            ),
                                            MyText(text:
                                              products[i]["shipment_cost"]
                                                  .toString(),
                                              style: TextStyle(
                                                  color: Pallet.fontcolornew,
                                                  fontSize: Pallet.heading3),
                                            ),
                                          ],
                                        ),
                                        // Column(
                                        //   crossAxisAlignment:
                                        //       CrossAxisAlignment.start,
                                        //   children: [
                                        //     MyText(text:
                                        //       'Shipping Cost',
                                        //       style: TextStyle(
                                        //           color: Pallet.fontcolornew,
                                        //           fontWeight: Pallet.font600,
                                        //           fontSize: Pallet.heading3),
                                        //     ),
                                        //     SizedBox(
                                        //       height: 10,
                                        //     ),
                                        //     MyText(text:
                                        //       products[i]["shipment_cost"]
                                        //           .toString(),
                                        //       style: TextStyle(
                                        //           color: Pallet.fontcolornew,
                                        //           fontWeight: Pallet.font600,
                                        //           fontSize: Pallet.heading3),
                                        //     ),
                                        //   ],
                                        // ),
                                        Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            MyText(text:
                                              pageDetails['text10'],
                                              style: TextStyle(
                                                  color: Pallet.fontcolornew,
                                                  fontWeight:
                                                      Pallet.heading2wgt,
                                                  fontSize: Pallet.heading3),
                                            ),
                                            SizedBox(
                                              height: 10,
                                            ),
                                            Container(
                                              decoration: BoxDecoration(
                                                borderRadius:
                                                    BorderRadius.circular(
                                                        Pallet.radius),
                                                color: Pallet.fontcolornew,
                                              ),
                                              child: Padding(
                                                padding: EdgeInsets.all(
                                                    Pallet.defaultPadding / 2),
                                                child: MyText(text:
                                                  products[i]["order_status"]
                                                      .toString()
                                                      .capitalizeFirstofEach,
                                                  style: TextStyle(
                                                      color: Pallet.fontcolor,
                                                      fontSize:
                                                          Pallet.heading3),
                                                ),
                                              ),
                                            ),
                                          ],
                                        )
                                      ],
                                    ),
                                  ),
                                ),
                                if (selectedIndex == i)
                                  Padding(
                                    padding:
                                        EdgeInsets.all(Pallet.defaultPadding),
                                    child: Container(
                                      decoration: BoxDecoration(
                                        boxShadow: [Pallet.shadowEffect],
                                        borderRadius: BorderRadius.circular(
                                            Pallet.radius),
                                        color: Pallet.fontcolornew,
                                      ),
                                      child: Container(
                                        width:
                                            // isMobile == true
                                            //     ? widget.wdgtWidth
                                            //     :
                                            widget.wdgtWidth - 100,
                                        child: Row(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceEvenly,
                                          children: [
                                            Padding(
                                              padding: EdgeInsets.all(
                                                  Pallet.defaultPadding),
                                              child: Column(
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.start,
                                                children: [
                                                  MyText(text:
                                                    pageDetails['text11'],
                                                    style: TextStyle(
                                                        color: Pallet.fontcolor,
                                                        fontSize:
                                                            Pallet.heading3),
                                                  ),
                                                  SizedBox(
                                                    height: 20,
                                                  ),
                                                  for (var temp in products[i]
                                                      ['products'])
                                                    Container(
                                                      child: Row(
                                                        crossAxisAlignment:
                                                            CrossAxisAlignment
                                                                .start,
                                                        children: [
                                                          Padding(
                                                            padding: EdgeInsets
                                                                .all(Pallet
                                                                    .defaultPadding),
                                                            child: Container(
                                                                width: 80,
                                                                height: 80,
                                                                child: Image.network(appSettings[
                                                                        'SERVER_URL'] +
                                                                    '/' +
                                                                    temp["image_url"]
                                                                        .toString())
                                                                // child: Image.asset('1.png'),
                                                                ),
                                                          ),
                                                          Column(
                                                            crossAxisAlignment:
                                                                CrossAxisAlignment
                                                                    .start,
                                                            children: [
                                                              MyText(text:
                                                                temp[
                                                                    "product_name"],
                                                                style: TextStyle(
                                                                    color: Pallet
                                                                        .fontcolor,
                                                                    fontSize: Pallet
                                                                        .heading3),
                                                              ),
                                                              SizedBox(
                                                                width: 10,
                                                              ),
                                                              Row(
                                                                children: [
                                                                  MyText(text:
                                                                    pageDetails[
                                                                        'text12'],
                                                                    style: TextStyle(
                                                                        color: Pallet
                                                                            .fontcolor,
                                                                        fontSize:
                                                                            Pallet.heading4),
                                                                  ),
                                                                  SizedBox(
                                                                    width: 10,
                                                                  ),
                                                                  MyText(text:
                                                                    '\$' +
                                                                        ' ' +
                                                                        temp["total"]
                                                                            .toString(),
                                                                    style: TextStyle(
                                                                        color: Pallet
                                                                            .fontcolor,
                                                                        fontSize:
                                                                            Pallet.heading4),
                                                                  ),
                                                                ],
                                                              ),
                                                              SizedBox(
                                                                width: 10,
                                                              ),
                                                              Column(
                                                                crossAxisAlignment:
                                                                    CrossAxisAlignment
                                                                        .start,
                                                                children: [
                                                                  Row(
                                                                    children: [
                                                                      for (var item
                                                                          in temp[
                                                                              "variants"])
                                                                        MyText(text:
                                                                          item["variant_type"].toString().capitalizeFirstofEach +
                                                                              ' : ' +
                                                                              item["value"],
                                                                          style: TextStyle(
                                                                              color: Pallet.fontcolor,
                                                                              fontSize: Pallet.heading3),
                                                                        ),
                                                                    ],
                                                                  ),
                                                                  Row(
                                                                    children: [
                                                                      MyText(text:
                                                                        pageDetails[
                                                                            'text13'],
                                                                        style: TextStyle(
                                                                            color:
                                                                                Pallet.fontcolor,
                                                                            fontSize: Pallet.heading4),
                                                                      ),
                                                                      SizedBox(
                                                                        width:
                                                                            10,
                                                                      ),
                                                                      MyText(text:
                                                                        temp["qty"]
                                                                            .toString(),
                                                                        style: TextStyle(
                                                                            color:
                                                                                Pallet.fontcolor,
                                                                            fontSize: Pallet.heading4),
                                                                      ),
                                                                      SizedBox(
                                                                        height:
                                                                            10,
                                                                      )
                                                                    ],
                                                                  ),
                                                                ],
                                                              )
                                                            ],
                                                          ),
                                                        ],
                                                      ),
                                                    ),
                                                ],
                                              ),
                                            ),
                                            Padding(
                                              padding: EdgeInsets.all(
                                                  Pallet.defaultPadding),
                                              child: Column(
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.start,
                                                children: [
                                                  MyText(text:
                                                    pageDetails['text14'],
                                                    style: TextStyle(
                                                        color: Pallet.fontcolor,
                                                        fontSize:
                                                            Pallet.heading3),
                                                  ),
                                                  SizedBox(
                                                    height: 20,
                                                  ),
                                                  MyText(text:
                                                    products[i]["address_data"]
                                                        ["address_phone"],
                                                    style: TextStyle(
                                                        color: Pallet.fontcolor,
                                                        fontSize:
                                                            Pallet.heading4),
                                                  ),
                                                  MyText(text:
                                                    products[i]["address_data"]
                                                        ["address_line1"],
                                                    //         '' +
                                                    //         products[i][
                                                    //                 "address_data"]
                                                    //             [
                                                    //             "address_line2"] ==
                                                    //     null
                                                    // ? MyText(text:'')
                                                    // : products[i]
                                                    //         ["address_data"]
                                                    //     ["address_line2"],
                                                    style: TextStyle(
                                                        color: Pallet.fontcolor,
                                                        fontSize:
                                                            Pallet.heading4),
                                                  ),
                                                  MyText(text:
                                                    products[i]["address_data"]
                                                        ["country"],
                                                    style: TextStyle(
                                                        color: Pallet.fontcolor,
                                                        fontSize:
                                                            Pallet.heading4),
                                                  ),
                                                  MyText(text:
                                                    products[i]["address_data"]
                                                        ["city"],
                                                    style: TextStyle(
                                                        color: Pallet.fontcolor,
                                                        fontSize:
                                                            Pallet.heading4),
                                                  ),
                                                  MyText(text:
                                                    products[i]["address_data"]
                                                        ["postal_code"],
                                                    style: TextStyle(
                                                        color: Pallet.fontcolor,
                                                        fontSize:
                                                            Pallet.heading4),
                                                  ),
                                                ],
                                              ),
                                            )
                                          ],
                                        ),
                                      ),
                                    ),
                                  )
                              ],
                            )),
                      ),
                    )
                ],
              )

        //  SingleChildScrollView(
        //     child: Column(
        //       children: [
        // MyText(text:pageDetails['text1'],
        //     style: TextStyle(
        //       color: Pallet.fontcolornew,
        //       fontSize: Pallet.heading1,
        //     )),
        //         // for (var product in products)
        //         for (var i = 0; i < products.length; i++)
        //           Padding(
        //             padding: const EdgeInsets.all(8.0),
        //             child: Container(
        // decoration: BoxDecoration(
        //   boxShadow: [Pallet.shadowEffect],
        //   borderRadius: BorderRadius.circular(Pallet.radius),
        //   color: Pallet.fontcolor,
        // ),
        //               child: Row(children: [
        //                 // Padding(
        //                 //   padding: const EdgeInsets.all(8.0),
        //                 //   child: ClipRRect(
        //                 //     borderRadius: BorderRadius.circular(10),
        //                 //     child: Container(
        //                 //         width: 80,
        //                 //         height: 80,
        //                 //         child: Image.network(
        //                 //             appSettings['SERVER_URL'] +
        //                 //                 '/' +
        //                 //                 products[i]["image_url"])
        //                 //         // child: Image.asset('1.png'),
        //                 //         ),
        //                 //   ),
        //                 // ),
        //                 Expanded(
        //                   child: Column(
        //                       crossAxisAlignment: CrossAxisAlignment.start,
        //                       mainAxisAlignment:
        //                           MainAxisAlignment.spaceBetween,
        //                       children: [
        // MyText(text:
        //   // 'order1',
        //   products[i]["purchase_uuid"],
        //   style: TextStyle(
        //       color: Pallet.fontcolornew,
        //       fontWeight: Pallet.font600,
        //       fontSize: Pallet.heading3),
        // ),
        //                         MyText(text:
        //                           // 'order1',
        //                           products[i]["purchase_date"],
        //                           style: TextStyle(
        //                               color: Pallet.fontcolornew,
        //                               fontWeight: Pallet.font600,
        //                               fontSize: Pallet.heading3),
        //                         ),
        //                         SizedBox(height: widget.wdgtHeight * 0.02),
        //                         Row(children: [
        //                           // MyText(text:
        //                           //     "\$ " +
        //                           //         cart.cart[index]["price"]
        //                           //             .toString(),

        //                           CustomButton(
        //                             // textcolor: Pallet.fontcolor,
        //                             buttoncolor: Pallet.fontcolornew,
        //                             child: MyText(text:'Order Details',
        //                                 style: TextStyle(
        //                                     color: Pallet.fontcolor)),
        //                             onpress: () {
        //                               setState(() {
        //                                 if (selectedIndex != i) {
        //                                   selectedIndex = i;
        //                                 } else {
        //                                   selectedIndex = null;
        //                                 }
        //                                 // isOrderDetails = !isOrderDetails;
        //                               });
        //                             },
        //                           ),
        //                           // style: TextStyle(
        //                           //     color: Colors.blue,
        //                           //     fontWeight: FontWeight.bold)),
        //                           SizedBox(width: 5),
        //                           // MyText(text:
        //                           //     "\$ " +
        //                           //         cart.cart[index]["new_price"]
        //                           //             .toString(),
        //                           // MyText(text:'1min ago',
        //                           //     style: TextStyle(
        //                           //         color: Pallet.fontcolornew,
        //                           //         fontWeight: FontWeight.w400,
        //                           //         fontSize: Pallet.heading4)),
        //                           // style: TextStyle(
        //                           //     color: Colors.grey,

        //                           //     fontSize: 10)),
        //                         ]),
        //                         // selectedIndex == i
        //                         //     ? Column(
        //                         //         children: [
        //                         //           MyText(text:
        //                         //             products[i]["address_data"]
        //                         //                 ["address_phone"],
        //                         //             style: TextStyle(
        //                         //                 color: Pallet.fontcolornew,
        //                         //                 fontWeight: Pallet.font600,
        //                         //                 fontSize: Pallet.heading3),
        //                         //           ),
        //                         //         ],
        //                         //       )
        //                         //     : Container()
        //                       ]),
        //                 ),
        //                 Padding(
        //                   padding: const EdgeInsets.only(right: 20),
        //                   child: Container(
        //                       child: Column(
        //                     mainAxisAlignment:
        //                         MainAxisAlignment.spaceEvenly,
        //                     children: [
        //                       MyText(text:'\$ ' + products[i]["total"].toString(),
        //                           style: TextStyle(
        //                               color: Pallet.fontcolornew,
        //                               fontWeight: Pallet.font600,
        //                               fontSize: Pallet.heading2)),
        //                       SizedBox(height: 5),
        //                       Container(
        //                         decoration: BoxDecoration(
        //                           borderRadius: BorderRadius.circular(15),
        //                           color: Pallet.dashcontainerback,
        //                         ),
        //                         padding: EdgeInsets.symmetric(
        //                             horizontal: 10, vertical: 5),
        //                         child: MyText(text:
        //                             products[i]["order_status"]
        //                                     .substring(0, 1)
        //                                     .toUpperCase() +
        //                                 products[i]["order_status"]
        //                                     .substring(1)
        //                                     .toLowerCase()
        //                                     .toString(),
        //                             style:
        //                                 TextStyle(color: Pallet.fontcolor)),
        //                       ),
        //                     ],
        //                   )),
        //                 ),
        //               ]),
        //             ),
        //           ),
        //       ],

        //       //  itemCount: cart.cart.length,
        //     ),
        //   )
        );
  }

  Widget myordersmobile({double wdgtWidth, wdgtHeight}) {
    return Container(
        width: widget.wdgtWidth,
        height: widget.wdgtHeight,
        child: products.length == 0
            ? Container(
                padding: EdgeInsets.only(left: Pallet.defaultPadding),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    MyText(text:pageDetails['text1'],
                        style: TextStyle(
                          color: Pallet.fontcolornew,
                          fontSize: Pallet.heading1,
                        )),
                    Expanded(
                      child: Center(
                          child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Container(
                            child: Image.asset('orderempty.png',
                                height: 300, fit: BoxFit.fill),
                            // child: Image.network(
                            //   'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSoKtdDm7GMnb1RaSuVMPErLtLKSddB33YFbw&usqp=CAU',
                            //   height: 100,
                            //   width: 100,
                            //   fit: BoxFit.fill,
                            // ),
                          ),
                          MyText(text:pageDetails['text2'],
                              style: TextStyle(
                                  color: Pallet.dashcontainerback,
                                  fontSize: Pallet.heading5,
                                  fontWeight: Pallet.heading2wgt)),
                          InkWell(
                            onTap: () {
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => Home(
                                            route: 'centurion_shop',
                                          )));
                            },
                            child: Padding(
                              padding: EdgeInsets.all(Pallet.defaultPadding),
                              child: Container(
                                decoration: BoxDecoration(
                                    boxShadow: [Pallet.shadowEffect],
                                    border:
                                        Border.all(color: Pallet.fontcolornew),
                                    color: Pallet.fontcolornew,
                                    borderRadius:
                                        BorderRadius.circular(Pallet.radius)),
                                child: Padding(
                                  padding:
                                      EdgeInsets.all(Pallet.defaultPadding),
                                  child: MyText(text:pageDetails['text3'],
                                      style:
                                          TextStyle(color: Pallet.fontcolor)),
                                ),
                              ),
                            ),
                          )
                        ],
                      )),
                    )
                  ],
                ),
              )
            : ListView(
                // crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  MyText(text:pageDetails['text1'],
                      style: TextStyle(
                        color: Pallet.fontcolornew,
                        fontSize: Pallet.heading1,
                      )),
                  for (var i = 0; i < products.length; i++)
                    Padding(
                      padding: EdgeInsets.all(Pallet.defaultPadding),
                      child: InkWell(
                        onTap: () {
                          setState(() {
                            if (selectedIndex == i) {
                              selectedIndex = null;
                            } else {
                              selectedIndex = i;
                            }
                          });
                        },
                        child: Container(
                            decoration: BoxDecoration(
                              boxShadow: [Pallet.shadowEffect],
                              borderRadius:
                                  BorderRadius.circular(Pallet.radius),
                              color: Pallet.fontcolor,
                            ),
                            child: Column(
                              children: [
                                Padding(
                                  padding:
                                      EdgeInsets.all(Pallet.defaultPadding),
                                  child: Container(
                                    width:
                                        // isMobile == true
                                        // ?
                                        widget.wdgtWidth,
                                    // : widget.wdgtWidth - 100,
                                    child: SingleChildScrollView(
                                      scrollDirection: Axis.horizontal,
                                      child: Row(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceEvenly,
                                        children: [
                                          Column(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            children: [
                                              MyText(text:
                                                pageDetails['text4'],
                                                style: TextStyle(
                                                    color: Pallet.fontcolornew,
                                                    fontWeight:
                                                        Pallet.heading2wgt,
                                                    fontSize: Pallet.heading4),
                                              ),
                                              SizedBox(
                                                height: 10,
                                              ),
                                              MyText(text:
                                                pageDetails['text5'],
                                                style: TextStyle(
                                                    color: Pallet.fontcolornew,
                                                    fontSize: Pallet.heading4),
                                              ),
                                              SizedBox(
                                                height: 10,
                                              ),
                                              MyText(text:
                                                products[i]["purchase_uuid"],
                                                style: TextStyle(
                                                    color: Pallet.fontcolornew,
                                                    fontSize: Pallet.heading4),
                                              ),
                                              SizedBox(
                                                height: 10,
                                              ),
                                              MyText(text:
                                                pageDetails['text6'],
                                                style: TextStyle(
                                                    color: Pallet.fontcolornew,
                                                    fontSize: Pallet.heading4),
                                              ),
                                              SizedBox(
                                                height: 10,
                                              ),
                                              MyText(text:
                                                products[i]["purchase_date"],
                                                style: TextStyle(
                                                    color: Pallet.fontcolornew,
                                                    fontSize: Pallet.heading4),
                                              ),
                                            ],
                                          ),
                                          SizedBox(width: 10),
                                          // Column(
                                          //   crossAxisAlignment:
                                          //       CrossAxisAlignment.start,
                                          //   children: [
                                          //     MyText(text:
                                          //       'Purchase Date',
                                          //       style: TextStyle(
                                          //           color: Pallet.fontcolornew,
                                          //           fontWeight: Pallet.font600,
                                          //           fontSize: Pallet.heading3),
                                          //     ),
                                          //     SizedBox(
                                          //       height: 10,
                                          //     ),
                                          //     MyText(text:
                                          //       products[i]["purchase_date"],
                                          //       style: TextStyle(
                                          //           color: Pallet.fontcolornew,
                                          //           fontWeight: Pallet.font600,
                                          //           fontSize: Pallet.heading3),
                                          //     ),
                                          //   ],
                                          // ),
                                          // Container(
                                          //     width: 2,
                                          //     height: 100,
                                          //     color: Pallet.fontcolornew),
                                          Column(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            children: [
                                              MyText(text:
                                                pageDetails['text7'],
                                                style: TextStyle(
                                                    color: Pallet.fontcolornew,
                                                    fontWeight:
                                                        Pallet.heading2wgt,
                                                    fontSize: Pallet.heading4),
                                              ),
                                              SizedBox(
                                                height: 10,
                                              ),
                                              MyText(text:
                                                pageDetails['text8'],
                                                style: TextStyle(
                                                    color: Pallet.fontcolornew,
                                                    fontSize: Pallet.heading4),
                                              ),
                                              SizedBox(
                                                height: 10,
                                              ),
                                              MyText(text:
                                                products[i]["amount_payed"]
                                                    .toString(),
                                                style: TextStyle(
                                                    color: Pallet.fontcolornew,
                                                    fontSize: Pallet.heading4),
                                              ),
                                              SizedBox(
                                                height: 10,
                                              ),
                                              MyText(text:
                                                pageDetails['text9'],
                                                style: TextStyle(
                                                    color: Pallet.fontcolornew,
                                                    fontSize: Pallet.heading4),
                                              ),
                                              SizedBox(
                                                height: 10,
                                              ),
                                              MyText(text:
                                                products[i]["shipment_cost"]
                                                    .toString(),
                                                style: TextStyle(
                                                    color: Pallet.fontcolornew,
                                                    fontSize: Pallet.heading4),
                                              ),
                                            ],
                                          ),
                                          SizedBox(width: 10),

                                          // Column(
                                          //   crossAxisAlignment:
                                          //       CrossAxisAlignment.start,
                                          //   children: [
                                          //     MyText(text:
                                          //       'Shipping Cost',
                                          //       style: TextStyle(
                                          //           color: Pallet.fontcolornew,
                                          //           fontWeight: Pallet.font600,
                                          //           fontSize: Pallet.heading3),
                                          //     ),
                                          //     SizedBox(
                                          //       height: 10,
                                          //     ),
                                          //     MyText(text:
                                          //       products[i]["shipment_cost"]
                                          //           .toString(),
                                          //       style: TextStyle(
                                          //           color: Pallet.fontcolornew,
                                          //           fontWeight: Pallet.font600,
                                          //           fontSize: Pallet.heading3),
                                          //     ),
                                          //   ],
                                          // ),
                                          Column(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            children: [
                                              MyText(text:
                                                pageDetails['text10'],
                                                style: TextStyle(
                                                    color: Pallet.fontcolornew,
                                                    fontWeight:
                                                        Pallet.heading2wgt,
                                                    fontSize: Pallet.heading4),
                                              ),
                                              SizedBox(
                                                height: 10,
                                              ),
                                              Container(
                                                decoration: BoxDecoration(
                                                  borderRadius:
                                                      BorderRadius.circular(
                                                          Pallet.radius),
                                                  color: Pallet.fontcolornew,
                                                ),
                                                child: Padding(
                                                  padding: EdgeInsets.all(
                                                      Pallet.defaultPadding /
                                                          2),
                                                  child: MyText(text:
                                                    products[i]["order_status"]
                                                        .toString()
                                                        .capitalizeFirstofEach,
                                                    style: TextStyle(
                                                        color: Pallet.fontcolor,
                                                        fontSize:
                                                            Pallet.heading5),
                                                  ),
                                                ),
                                              ),
                                            ],
                                          )
                                        ],
                                      ),
                                    ),
                                  ),
                                ),
                                if (selectedIndex == i)
                                  Padding(
                                    padding:
                                        EdgeInsets.all(Pallet.defaultPadding),
                                    child: Container(
                                      decoration: BoxDecoration(
                                        boxShadow: [Pallet.shadowEffect],
                                        borderRadius: BorderRadius.circular(
                                            Pallet.radius),
                                        color: Pallet.fontcolornew,
                                      ),
                                      child: Container(
                                        width:
                                            // isMobile == true
                                            // ?
                                            widget.wdgtWidth,
                                        // : widget.wdgtWidth - 100,
                                        child: SingleChildScrollView(
                                          scrollDirection: Axis.horizontal,
                                          child: Row(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            mainAxisAlignment:
                                                MainAxisAlignment.spaceEvenly,
                                            children: [
                                              Padding(
                                                padding: EdgeInsets.all(
                                                    Pallet.defaultPadding),
                                                child: Column(
                                                  crossAxisAlignment:
                                                      CrossAxisAlignment.start,
                                                  children: [
                                                    MyText(text:
                                                      pageDetails['text11'],
                                                      style: TextStyle(
                                                          color:
                                                              Pallet.fontcolor,
                                                          fontSize:
                                                              Pallet.heading4),
                                                    ),
                                                    SizedBox(
                                                      height: 20,
                                                    ),
                                                    for (var temp in products[i]
                                                        ['products'])
                                                      Container(
                                                        child: Row(
                                                          crossAxisAlignment:
                                                              CrossAxisAlignment
                                                                  .start,
                                                          children: [
                                                            Padding(
                                                              padding: EdgeInsets
                                                                  .all(Pallet
                                                                      .defaultPadding),
                                                              child: Container(
                                                                  width: 80,
                                                                  height: 80,
                                                                  child: Image.network(appSettings[
                                                                          'SERVER_URL'] +
                                                                      '/' +
                                                                      temp["image_url"]
                                                                          .toString())
                                                                  // child: Image.asset('1.png'),
                                                                  ),
                                                            ),
                                                            Column(
                                                              crossAxisAlignment:
                                                                  CrossAxisAlignment
                                                                      .start,
                                                              children: [
                                                                MyText(text:
                                                                  temp[
                                                                      "product_name"],
                                                                  style: TextStyle(
                                                                      color: Pallet
                                                                          .fontcolor,
                                                                      fontSize:
                                                                          Pallet
                                                                              .heading4),
                                                                ),
                                                                SizedBox(
                                                                  width: 10,
                                                                ),
                                                                Row(
                                                                  children: [
                                                                    MyText(text:
                                                                      pageDetails[
                                                                          'text12'],
                                                                      style: TextStyle(
                                                                          color: Pallet
                                                                              .fontcolor,
                                                                          fontSize:
                                                                              Pallet.heading5),
                                                                    ),
                                                                    SizedBox(
                                                                      width: 10,
                                                                    ),
                                                                    MyText(text:
                                                                      '\$' +
                                                                          ' ' +
                                                                          temp["total"]
                                                                              .toString(),
                                                                      style: TextStyle(
                                                                          color: Pallet
                                                                              .fontcolor,
                                                                          fontSize:
                                                                              Pallet.heading5),
                                                                    ),
                                                                  ],
                                                                ),
                                                                SizedBox(
                                                                  width: 10,
                                                                ),
                                                                Column(
                                                                  crossAxisAlignment:
                                                                      CrossAxisAlignment
                                                                          .start,
                                                                  children: [
                                                                    Row(
                                                                      children: [
                                                                        for (var item
                                                                            in temp["variants"])
                                                                          MyText(text:
                                                                            item["variant_type"].toString().capitalizeFirstofEach +
                                                                                ' : ' +
                                                                                item["value"],
                                                                            style:
                                                                                TextStyle(color: Pallet.fontcolor, fontSize: Pallet.heading5),
                                                                          ),
                                                                      ],
                                                                    ),
                                                                    Row(
                                                                      children: [
                                                                        MyText(text:
                                                                          pageDetails[
                                                                              'text13'],
                                                                          style: TextStyle(
                                                                              color: Pallet.fontcolor,
                                                                              fontSize: Pallet.heading5),
                                                                        ),
                                                                        SizedBox(
                                                                          width:
                                                                              10,
                                                                        ),
                                                                        MyText(text:
                                                                          temp["qty"]
                                                                              .toString(),
                                                                          style: TextStyle(
                                                                              color: Pallet.fontcolor,
                                                                              fontSize: Pallet.heading5),
                                                                        ),
                                                                        SizedBox(
                                                                          height:
                                                                              10,
                                                                        )
                                                                      ],
                                                                    ),
                                                                  ],
                                                                )
                                                              ],
                                                            ),
                                                          ],
                                                        ),
                                                      ),
                                                    Padding(
                                                      padding: EdgeInsets.all(
                                                          Pallet
                                                              .defaultPadding),
                                                      child: Column(
                                                        crossAxisAlignment:
                                                            CrossAxisAlignment
                                                                .start,
                                                        children: [
                                                          MyText(text:
                                                            pageDetails[
                                                                'text14'],
                                                            style: TextStyle(
                                                                color: Pallet
                                                                    .fontcolor,
                                                                fontSize: Pallet
                                                                    .heading4),
                                                          ),
                                                          SizedBox(
                                                            height: 20,
                                                          ),
                                                          MyText(text:
                                                            products[i][
                                                                    "address_data"]
                                                                [
                                                                "address_phone"],
                                                            style: TextStyle(
                                                                color: Pallet
                                                                    .fontcolor,
                                                                fontSize: Pallet
                                                                    .heading5),
                                                          ),
                                                          MyText(text:
                                                            products[i][
                                                                    "address_data"]
                                                                [
                                                                "address_line1"],
                                                            //         '' +
                                                            //         products[i][
                                                            //                 "address_data"]
                                                            //             [
                                                            //             "address_line2"] ==
                                                            //     null
                                                            // ? MyText(text:'')
                                                            // : products[i]
                                                            //         ["address_data"]
                                                            //     ["address_line2"],
                                                            style: TextStyle(
                                                                color: Pallet
                                                                    .fontcolor,
                                                                fontSize: Pallet
                                                                    .heading5),
                                                          ),
                                                          MyText(text:
                                                            products[i][
                                                                    "address_data"]
                                                                ["country"],
                                                            style: TextStyle(
                                                                color: Pallet
                                                                    .fontcolor,
                                                                fontSize: Pallet
                                                                    .heading5),
                                                          ),
                                                          MyText(text:
                                                            products[i][
                                                                    "address_data"]
                                                                ["city"],
                                                            style: TextStyle(
                                                                color: Pallet
                                                                    .fontcolor,
                                                                fontSize: Pallet
                                                                    .heading5),
                                                          ),
                                                          MyText(text:
                                                            products[i][
                                                                    "address_data"]
                                                                ["postal_code"],
                                                            style: TextStyle(
                                                                color: Pallet
                                                                    .fontcolor,
                                                                fontSize: Pallet
                                                                    .heading5),
                                                          ),
                                                        ],
                                                      ),
                                                    )
                                                  ],
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                      ),
                                    ),
                                  )
                              ],
                            )),
                      ),
                    )
                ],
              ));
  }

  Future<String> getProducts() async {
    Map<String, dynamic> _map;

    Map<String, dynamic> _result;
    _map = {
      "product_id": 1,
      "store_id": 0,
      "seller_id": 0,
      "purchase_id": 1,
      "warehouse_id": 0,
      "enable_filter_protocol": false
    };
    _result = await HttpRequest.Post(
        'c_ps_get_product_list', Utils.constructPayload(_map));
    if (Utils.isServerError(_result)) {
      return throw (await Utils.getMessage(_result['response']['error']));
    } else {
      print('@@@@@@@@@@@@@@@@@@@@@');
      products = _result['response']['data']['products'];
      print(products);
      print('**********************');
    }
    //print(products);

    return "start";
  }
}

// ListView.builder(
//             itemCount: products.length,
//             itemBuilder: (BuildContext context, int index) {
//               return Padding(
//                 padding: const EdgeInsets.all(8.0),
//                 child: Container(
//                   decoration: BoxDecoration(
//                     color: Pallet.inner2,
//                     borderRadius: BorderRadius.circular(10),
//                   ),
//                   child: Row(children: [
//                     Padding(
//                       padding: const EdgeInsets.all(8.0),
//                       child: ClipRRect(
//                         borderRadius: BorderRadius.circular(10),
//                         child: Container(
//                             width: 80,
//                             height: 80,
//                             child: Image.network(appSettings['SERVER_URL'] +
//                                 '/' +
//                                 products[index]["image_url"])
//                             //child: Image.asset('1.png'),
//                             ),
//                       ),
//                     ),
//                     Expanded(
//                       child: Column(
//                           crossAxisAlignment: CrossAxisAlignment.start,
//                           mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                           children: [
//                             MyText(text:
//                               // 'order1',
//                               products[index]["product_name"],
//                               style: TextStyle(
//                                   color: Colors.blue,
//                                   fontWeight: FontWeight.w400,
//                                   fontSize: 15),
//                             ),
//                             SizedBox(height: widget.wdgtHeight * 0.02),
//                             Row(children: [
//                               // MyText(text:
//                               //     "\$ " +
//                               //         cart.cart[index]["price"]
//                               //             .toString(),
//                               MyText(text:'order id'),
//                               // style: TextStyle(
//                               //     color: Colors.blue,
//                               //     fontWeight: FontWeight.bold)),
//                               SizedBox(width: 5),
//                               // MyText(text:
//                               //     "\$ " +
//                               //         cart.cart[index]["new_price"]
//                               //             .toString(),
//                               MyText(text:'2 hr ago'),
//                               // style: TextStyle(
//                               //     color: Colors.grey,

//                               //     fontSize: 10)),
//                             ])
//                           ]),
//                     ),
//                     Padding(
//                       padding: const EdgeInsets.only(right: 20),
//                       child: Container(
//                           child: Column(
//                         mainAxisAlignment: MainAxisAlignment.spaceEvenly,
//                         children: [
//                           MyText(text:'1000'),
//                           Container(
//                             decoration: BoxDecoration(
//                               borderRadius: BorderRadius.circular(15),
//                               color: Colors.blue,
//                             ),
//                             padding: EdgeInsets.symmetric(
//                                 horizontal: 10, vertical: 5),
//                             child: MyText(text:'Pending'),
//                           ),
//                           // MyText(text:cart.cart[index]["qty"].toString(),
//                           //     style: TextStyle(color: Colors.white)),
//                         ],
//                       )),
//                     ),
//                   ]),
//                 ),
//               );
//             }
//             //  itemCount: cart.cart.length,
//             )
