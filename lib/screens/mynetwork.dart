import 'package:centurion/common.dart';
import 'package:centurion/config/app_settings.dart';
import 'package:flutter/material.dart';
import '../screens/iframe.dart';
import '../utils/utils.dart' show Utils;

// ignore: camel_case_types
class My_network extends StatefulWidget {
  final double wdgtWidth, wdgtHeight;

  const My_network({Key key, this.wdgtWidth, this.wdgtHeight})
      : super(key: key);
  @override
  _My_networkState createState() => _My_networkState();
}

// ignore: camel_case_types
class _My_networkState extends State<My_network> {
  Future<String> _temp;
  @override
  void initState() {
    _temp = Utils.getSharedPrefereces('data');

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: _temp,
      builder: (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
        if (snapshot.hasError) {
          return SomethingWentWrongMessage();
        } else if (snapshot.hasData) {
          //print("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
          var test = Utils.fromJSONString(snapshot.data)['token'];
          print(test);
          return Container(
            height: widget.wdgtHeight,
            width: widget.wdgtWidth,
            child: IframeView(
              link:
                  appSettings['NETWORK_TREE_URL'] + "/?auth=" + test.toString(),
            ),
            // child: Center(
            //     child: Image.asset('under_construction.png',
            //         width: 500, fit: BoxFit.fill)),
          );
        } else {
          return CircularProgressIndicator();
        }
      },
    );
  }
}
