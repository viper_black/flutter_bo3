// import 'dart:html';
// import 'dart:ui' as ui;

// import 'package:flutter/material.dart';

// class IframeView extends StatefulWidget {
//   final String link;

//   const IframeView({Key key, @required this.link}) : super(key: key);

//   @override
//   _IframeViewState createState() => _IframeViewState();
// }

// class _IframeViewState extends State<IframeView> {
//   final IFrameElement _iframeElement = IFrameElement();

//   @override
//   void initState() {
//     super.initState();
//     _iframeElement.src = widget.link;
//     _iframeElement.style.border = 'none';

//     ui.platformViewRegistry.registerViewFactory(
//       widget.link,
//       (int viewId) => _iframeElement,
//     );
//   }

//   @override
//   Widget build(BuildContext context) {
//     return HtmlElementView(
//       key: UniqueKey(),
//       viewType: widget.link,
//     );
//   }
// }
// ignore: avoid_web_libraries_in_flutter
import 'dart:ui' as ui;
import 'package:flutter/material.dart';

import '../home.dart';

class IframeView extends StatelessWidget {
  final String link;

  const IframeView({Key key, this.link}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    //register view factory
//     ui.platformViewRegistry.registerViewFactory(link, (int viewId) {
//       IFrameElement element = IFrameElement();

//       // -------------------------------------------------------don't change anything here-----------------------------------------------
//       window.onMessage.forEach((element) {
//         print('Event Received in callback: ${element.data}');

//         if (element.data != null) {
//           // ignore: unused_local_variable
//           if (element.data == "kyc_finished") {
//             Navigator.of(context).push(MaterialPageRoute(
//                 builder: (BuildContext context) => Home(
//                       route: 'dashboard',
//                     )));
//           } else if (element.data == "invite") {
//             // var invitername = element.data;
//             Navigator.of(context).push(MaterialPageRoute(
//                 builder: (BuildContext context) => Home(
//                       route: 'invite_member',
//                       // mynetworktreeinvite: invitername,
//                     )));
//           }
//           if (element.data == "goback") {
//             Navigator.of(context).push(MaterialPageRoute(
//                 builder: (BuildContext context) => Home(
//                       route: 'dashboard',
//                     )));
//           }
//         }

//         //   print("llllllllllllllllllllllll");
//         //   print(element.data);
//       });
// // --------------------------------------------------------------------------------------------------------------------------------------
//       element.requestFullscreen();
//       element.src = link;
//       element.style.border = 'none';
//       return element;
//     });
    return HtmlElementView(viewType: link);
  }
}
