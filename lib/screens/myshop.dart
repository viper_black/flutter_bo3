import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../utils/utils.dart';
import '../config/app_settings.dart';
import '../common.dart';
import '../config.dart';
import '../home.dart';
import 'cataegory.dart';
import '../services/business/cart.dart';
// import 'package:flutter/services.dart';

// import '../utils/utils.dart';
import '../services/communication/index.dart' show HttpRequest;

Map<String, dynamic> pageDetails = {};
double cashbalance;
double dsvpbalance;
bool verify = false;
Future<String> temp1;

// ignore: must_be_immutable
class Products extends StatefulWidget {
  Products(
      {Key key,
      this.wdgtWidth,
      this.wdgtHeight,
      this.wdgtRadius,
      this.wdgtPading,
      this.setData,
      this.shopSet,
      this.fontsize,
      this.storeId})
      : super(key: key);

  final double wdgtWidth, wdgtHeight, wdgtRadius, wdgtPading;
  final setData;
  final double fontsize;
  final int storeId;
  bool isFullProduct = false;
  Product product;
  final shopSet;
  List products = [];

  @override
  _ShopnowState createState() => _ShopnowState();
}

class _ShopnowState extends State<Products> {
  Future<String> temp;
  Map<String, dynamic> getRegionsMap = {
    'system_product_id': 1,
    'region_level': 2,
    'parent_region': 'null'
  };
  List products = [];
  shopbanner(context) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    bool isbannerenabled = prefs.getBool('shopbannershow');
    print("ssssssstestlllllll");
    print(prefs.getBool('shopbannershow'));
    if (isbannerenabled == null) {
      print("lllllllllll");
      await shopDetailAlert(context);
      prefs.setBool('shopbannershow', false);
      print(prefs.getBool('shopbannershow'));
    } else if (isbannerenabled == true) {
      print("bbbbbbbbbbbbbbbbblllllllllll");
      await shopDetailAlert(context);
      prefs.setBool('shopbannershow', false);
      // }
      //  else if (isbannerenabled == false) {
      //   print("iiiiiiiiisssssssssssss");
      //   // if (cart.address.length == 0 || cart.address.length == null) {
      //   //   await showDialog(
      //   //       barrierDismissible: false,
      //   //       context: context,
      //   //       builder: (BuildContext context) {
      //   //         return AddressProcessPopUp(
      //   //           isAdd: 'true',
      //   //           // index: 0,
      //   //           // setData: setState,
      //   //         );
      //   //       });
      //   }

      print("done");
    }
  }

  List categories = [];
  @override
  void initState() {
    //print('000000000000000000000000000000000000000000000000000000000');
    temp = getCategoryProducts(widget.storeId);
    cart.getAddressInfo(setState);
    cart.getRegions(getRegionsMap);

    super.initState();

    WidgetsBinding.instance.addPostFrameCallback((_) async {
      shopbanner(context);
    });
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
        future: temp,
        builder: (context, snapshot) {
          if (snapshot.hasError) {
            return SomethingWentWrongMessage();
          } else if (snapshot.hasData) {
            return Container(
                padding: EdgeInsets.all(10),
                width: widget.wdgtWidth,
                child: LayoutBuilder(builder: (context, constraints) {
                  if (constraints.maxWidth < 350) {
                    return Padding(
                      padding: EdgeInsets.all(Pallet.leftPadding / 5),
                      child: SingleChildScrollView(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            title(),
                            SizedBox(height: 10),
                            gridfunc(gridcount: 1, fontsize: 12),
                          ],
                        ),
                      ),
                    );
                  } else if (constraints.maxWidth < 600 &&
                      constraints.maxWidth > 350) {
                    return Padding(
                      padding: EdgeInsets.all(Pallet.leftPadding / 5),
                      child: SingleChildScrollView(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            title(),
                            SizedBox(height: 10),
                            gridfunc(gridcount: 2, fontsize: 13),
                          ],
                        ),
                      ),
                    );
                  } else if (constraints.maxWidth < 900 &&
                      constraints.maxWidth > 600) {
                    return Padding(
                      padding: EdgeInsets.all(Pallet.leftPadding / 2),
                      child: SingleChildScrollView(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            title(),
                            SizedBox(height: 10),
                            gridfunc(gridcount: 3, fontsize: 14),
                          ],
                        ),
                      ),
                    );
                  } else if (constraints.maxWidth < 1100 &&
                      constraints.maxWidth > 900) {
                    return Padding(
                      padding: EdgeInsets.only(
                          left: Pallet.leftPadding, top: Pallet.topPadding1),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          title(),
                          SizedBox(height: 10),
                          gridfunc(gridcount: 4, fontsize: 16),
                        ],
                      ),
                    );
                  } else if (constraints.maxWidth < 1400 &&
                      constraints.maxWidth > 1100) {
                    return Padding(
                      padding: EdgeInsets.only(
                          left: Pallet.leftPadding, top: Pallet.topPadding1),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          title(),
                          SizedBox(height: 10),
                          gridfunc(gridcount: 5, fontsize: 17),
                        ],
                      ),
                    );
                  } else {
                    return Padding(
                      padding: EdgeInsets.only(
                          left: Pallet.leftPadding, top: Pallet.topPadding1),
                      child: Container(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            title(),
                            SizedBox(height: 10),
                            gridfunc(gridcount: 6),
                          ],
                        ),
                      ),
                    );
                  }
                }));
          }
          return Loader();
        });
  }

  Widget title() {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        IconButton(
          icon: Icon(
            Icons.arrow_back,
            color: Pallet.fontcolornew,
          ),
          onPressed: () {
            Navigator.pushReplacement(
                context,
                MaterialPageRoute(
                    builder: (context) => Home(route: 'centurion_shop')));
          },
        ),
        MyText(
            text: cart.categoryName,
            style: TextStyle(
              color: Pallet.fontcolornew,
              fontSize: Pallet.heading1,
            )),
      ],
    );
  }

  // ignore: missing_return
  gridfunc({int gridcount, double fontsize}) {
    print('qqqqqqqqqwwwwwwww');
    print(fontsize);
    if (productListOfCategory.length > 0) {
      return Container(
        // color: Colors.pink,
        width: widget.wdgtWidth,
        height: widget.wdgtHeight - (Pallet.leftPadding * 4.2),
        // (widget.wdgtWidth * 0.70 - 10) *
        //     (product['products'].length / gridcount).ceil(),
        child: GridView.builder(
            itemCount: productListOfCategory.length,
            gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                crossAxisCount: gridcount,
                childAspectRatio: 0.70,
                mainAxisSpacing: 5,
                crossAxisSpacing: 10),
            itemBuilder: (context, index) => Product(
                  setData: widget.setData,
                  product: productListOfCategory[index],
                  shopSet: widget.shopSet,
                  wdgtPading: widget.wdgtPading,
                  fontsize: fontsize,
                  press: () {
                    print('ttttttttttttttttttttttttt');
                    print(fontsize);
                    widget.shopSet(() {
                      productId = productListOfCategory[index]['product_id'];
                      shopRoute = 'details';
                    });
                    // Navigator.push(
                    //   context,
                    //   MaterialPageRoute(
                    //       builder: (context) => Home(
                    //             route: 'product_details',
                    //             // param: 1,
                    //             param: productListOfCategory[index]
                    //                     ['product_id']
                    //                 .toString(),
                    //           )),
                    // );
                  },
                )),
      );
    } else {
      //print(
      // 'ssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssss');
      return Expanded(
        child: Container(
          child: Center(
            child: MyText(
             text: "No Product Found",
              style: TextStyle(
                fontSize: Pallet.heading3,
                fontWeight: Pallet.heading2wgt,
                color: Pallet.fontcolornew,
              ),
            ),
          ),
        ),
      );
    }
  }

  Future<String> getCategoryProducts(int storeId) async {
    Map<String, dynamic> _map;
    Map<String, dynamic> _result;

    _map = {
      "product_id": 1,
      "store_id": storeId,
      "seller_id": 0,
      "purchase_id": 0,
      "warehouse_id": 0,
      "enable_filter_protocol": false
    };
    _result = await HttpRequest.Post(
        'c_ps_get_product_list', Utils.constructPayload(_map));
    if (Utils.isServerError(_result)) {
      return throw (await Utils.getMessage(_result['response']['error']));
    } else {
      print('vvvvvvvvvvvvvvvvvv');
      print(_result['response']['data']);
      productListOfCategory = _result['response']['data']['products'];
    }

    return "start";
  }
}

class Product extends StatefulWidget {
  Product(
      {Key key,
      this.product,
      this.press,
      this.wdgtWidth,
      this.wdgtPading,
      this.shopSet,
      this.setData,
      this.fontsize})
      : super(key: key);
  final Map<String, dynamic> product;
  final Function press;
  final shopSet;
  final double fontsize;
  final double wdgtWidth, wdgtPading;
  final setData;
  @override
  _ProductState createState() => _ProductState();
}

class _ProductState extends State<Product> {
  // ignore: unused_field
  Future<String> _temp;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.all(Pallet.defaultPadding),
      child: InkWell(
        onTap: widget.press,
        child: Container(
            width: widget.wdgtWidth,
            height: 300,
            padding: EdgeInsets.all(Pallet.defaultPadding),
            decoration: BoxDecoration(
              boxShadow: [Pallet.shadowEffect],
              borderRadius: BorderRadius.circular(10),
              color: Colors.white,
            ),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Image.network(appSettings['SERVER_URL'] +
                    '/' +
                    widget.product["image_url"]),
                Wrap(
                  children: [
                    MyText(
                      text: widget.product["product_name"],
                      overflow: TextOverflow.ellipsis,
                      // maxLines: 1,
                      isselectable: false,
                      style: TextStyle(
                          color: Pallet.dashcontainerback,
                          fontSize:
                              widget.fontsize == null ? 18 : widget.fontsize,
                          fontWeight: FontWeight.w400),
                    ),
                  ],
                ),
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: 5),
                  child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Row(
                          children: [
                            MyText(
                                text: "\$ ${widget.product["new_price"]} ",
                                style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontSize: 18,
                                  color: Colors.red[500],
                                )),
                            SizedBox(width: 5),
                            // if (widget.product["price"] >
                            //     widget.product["new_price"])
                            //   MyText("\$ ${widget.product["price"]}",
                            //       style: TextStyle(
                            //           fontWeight: FontWeight.normal,
                            //           fontSize: 16,
                            //           decoration: TextDecoration.lineThrough)),
                          ],
                        ),
                        // IconButton(
                        //   icon: Icon(
                        //     Icons.shopping_bag_outlined,
                        //     size: 20,
                        //     color: Colors.black,
                        //   ),
                        //   onPressed: () {
                        //     // await cart.getAddressInfo(setState);
                        //     // if (cart.address.isEmpty) {
                        //     //   await showDialog(
                        //     //       context: context,
                        //     //       builder: (context) {
                        //     //         return AddressPopUp(
                        //     //           setData: setState,
                        //     //         );
                        //     //       });
                        //     // } else {

                        //     // }
                        //     cart.addToCart(widget.product, setState,
                        //         widget.setData, context);
                        //   },
                        // ),
                      ]),
                ),
              ],
            )),
      ),
    );
  }
}

shopDetailAlert(context) async {
  final prefs = await SharedPreferences.getInstance();

  return showDialog(
      barrierDismissible: false,
      context: context,
      builder: (BuildContext context) {
        return Container(
          // decoration: BoxDecoration(border: Border.all(color: Colors.pink)),
          // height: 600,
          child: Padding(
            padding: EdgeInsets.all(Pallet.defaultPadding),
            child: Center(
              child: Stack(
                children: [
                  Image.asset(
                    "newcenturionpromotion.png",
                    // height: 800,
                    // width: 900,
                  ),

                  Positioned(
                    top: 0,
                    right: 0,
                    child: Container(
                      decoration: BoxDecoration(
                          shape: BoxShape.circle, color: Colors.white),
                      width: 50,
                      height: 50,
                      // ignore: deprecated_member_use
                      child: FlatButton(
                        // decoration: BoxDecoration(
                        //   shape: BoxShape.circle,
                        // ),
                        // width: 50,
                        // height: 50,
                        onPressed: () {
                          Navigator.of(context).pop();
                          prefs.setBool('myshopbanner', true);
                          // if (cart.address.length == 0 ||
                          //     cart.address.length == null) {
                          //   showDialog(
                          //       barrierDismissible: false,
                          //       context: context,
                          //       builder: (BuildContext context) {
                          //         return AddressProcessPopUp(
                          //           isAdd: 'true',
                          //           // index: 0,
                          //           // setData: setState,
                          //         );
                          //       });
                          // }
                        },
                        child: Icon(Icons.close, color: Colors.red),
                      ),
                    ),
                  )
                  // CircleAvatar(
                  // child: InkWell(
                  //     onTap: () {
                  //       Navigator.of(context).pop();
                  //     },
                  //     child: Icon(Icons.close, color: Colors.red)),
                  // )
                ],
              ),
            ),
          ),
          // backgroundColor: Colors.transparent,
          // shape: RoundedRectangleBorder(
          //     borderRadius: BorderRadius.circular(Pallet.radius)),
          // content: Image.asset(
          //   "centurionshop.png",
          // ),
          // actions: [
          //   PopupButton(
          //     text: 'Continue',
          //     onpress: () {
          //       Navigator.of(context).pop();
          //       // return Products(
          //       //   wdgtWidth: wdgtWidth,
          //       //   wdgtHeight: wdgtHeight,
          //       //   productId: widget.param,
          //       //   setData: setState,
          //       // );
          //     },
          //   )
          // ],
        );
      });
}
// class Product extends StatelessWidget {
//   const Product(
//       {Key key, this.product, this.press, this.wdgtWidth, this.wdgtPading})
//       : super(key: key);
//   final Map<String, dynamic> product;
//   final Function press;
//   final double wdgtWidth, wdgtPading;
//   @override
//   Widget build(BuildContext context) {
//     return Padding(
//       padding: EdgeInsets.all(Pallet.defaultPadding),
//       child: Container(
//           width: wdgtWidth,
//           padding: EdgeInsets.all(Pallet.defaultPadding),
//           decoration: BoxDecoration(
//             boxShadow: [Pallet.shadowEffect],
//             borderRadius: BorderRadius.circular(10),
//             color: Colors.white,
//           ),
//           child: Column(
//             mainAxisAlignment: MainAxisAlignment.spaceBetween,
//             children: [
//               GestureDetector(
//                 onTap: press,
//                 child: Image.network(
//                     appSettings['SERVER_URL'] + '/' + product["image_url"]),
//               ),
//               MyText(product["product_name"],
//                   overflow: TextOverflow.ellipsis,
//                   maxLines: 1,
//                   style: TextStyle(
//                       color: Colors.black,
//                       fontSize: 18,
//                       fontWeight: FontWeight.w400)),
//               Padding(
//                 padding: EdgeInsets.symmetric(horizontal: 5),
//                 child: Row(
//                     mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                     children: [
//                       Row(
//                         children: [
//                           MyText("${product["price"]} \$",
//                               style: TextStyle(
//                                 fontWeight: FontWeight.bold,
//                                 fontSize: 18,
//                                 color: Colors.red[500],
//                               )),
//                           SizedBox(width: 5),
//                           if (product["price"] != product["new_price"])
//                             MyText("${product["new_price"]}\$",
//                                 style: TextStyle(
//                                     fontWeight: FontWeight.normal,
//                                     fontSize: 16,
//                                     decoration: TextDecoration.lineThrough)),
//                         ],
//                       ),
//                       IconButton(
//                         icon: Icon(
//                           Icons.shopping_bag_outlined,
//                           size: 20,
//                           color: Colors.black,
//                         ),
//                         onPressed: () {
//                           // //print(product);

//                           cart.add(product, setState);
//                         },
//                       ),
//                     ]),
//               ),
//             ],
//           )),
//     );
//   }
// }
