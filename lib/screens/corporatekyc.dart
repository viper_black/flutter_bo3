import 'package:centurion/common.dart';
import 'package:flutter/material.dart';
import 'package:universal_html/html.dart' hide Text, Navigator;
import 'dart:convert';
import 'dart:typed_data';
import '../config.dart';
import '../home.dart';

class CorporateKyc extends StatefulWidget {
  final double wdgtWidth, wdgtHeight;

  const CorporateKyc({Key key, this.wdgtWidth, this.wdgtHeight})
      : super(key: key);
  @override
  _CorporateKycState createState() => _CorporateKycState();
}

class _CorporateKycState extends State<CorporateKyc> {
  bool ismobile = false;
  // bool companycer = false;
  Image file;
  Uint8List uploadedImage;
  FileReader reader = FileReader();
  uploadImage() {
    InputElement uploadInput = FileUploadInputElement()..accept = 'image/*';
    uploadInput.click();
    uploadInput.onChange.listen((event) {
      final file = uploadInput.files.first;
      reader.readAsArrayBuffer(file);
      reader.onLoadEnd.listen((event) {
        setState(() {
          uploadedImage = reader.result;
          // companycer = true;
        });
        // ignore: unused_local_variable
        var test = base64.encode(uploadedImage);
      });
    });
  }

  List<dynamic> shareholders = [
    {'name': 'tde8ru', 'lastname': 'ssss', 'share': '20', 'action': 'Verified'},
    {
      'name': 'tde8ru',
      'lastname': 'ssss',
      'share': '20',
      'action': 'Not Verified'
    },
    {
      'name': 'tde8ru',
      'lastname': 'ssss',
      'share': '20',
      'action': 'Not Verified'
    },
    {
      'name': 'tde8ru',
      'lastname': 'ssss',
      'share': '20',
      'action': 'Not Verified'
    },
    {
      'name': 'tde8ru',
      'lastname': 'ssss',
      'share': '20',
      'action': 'Not Verified'
    },
  ];
  Image file1;
  Uint8List uploadedImage1;
  FileReader reader1 = FileReader();
  uploadImage1() {
    InputElement uploadInput1 = FileUploadInputElement()..accept = 'image/*';
    uploadInput1.click();
    uploadInput1.onChange.listen((event) {
      final file1 = uploadInput1.files.first;
      reader1.readAsArrayBuffer(file1);
      reader1.onLoadEnd.listen((event) {
        setState(() {
          uploadedImage1 = reader1.result;
          // companycer = true;
        });
        // ignore: unused_local_variable
        var test = base64.encode(uploadedImage1);
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
      builder: (context, constraints) {
        if (constraints.maxWidth <= 768) {
          ismobile = true;

          return Padding(
            padding: EdgeInsets.symmetric(
              horizontal: Pallet.defaultPadding,
            ),
            child: SingleChildScrollView(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: ismobile != true
                    ? CrossAxisAlignment.start
                    : CrossAxisAlignment.center,
                children: [
                  notice(
                    width: widget.wdgtWidth,
                  ),
                  SizedBox(height: 10),
                  Text(
                    "Corporate KYC",
                    style: TextStyle(
                      color: Pallet.fontcolornew,
                      fontSize: Pallet.heading1,
                      fontWeight: Pallet.font600,
                    ),
                  ),
                  SizedBox(height: 10),
                  certificates(
                    width: widget.wdgtWidth,
                  ),
                  SizedBox(height: 10),
                  shareholderslist(
                    width: widget.wdgtWidth,
                  ),
                  SizedBox(height: 10),
                  finalsubmit(
                    width: widget.wdgtWidth,
                  ),
                  SizedBox(height: 25),
                ],
              ),
            ),
          );
        } else if (constraints.maxWidth > 768 && constraints.maxWidth < 1024) {
          return Padding(
            padding: EdgeInsets.symmetric(
              horizontal: Pallet.defaultPadding,
            ),
            child: SingleChildScrollView(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: notice(
                      width: widget.wdgtWidth * 0.8,
                    ),
                  ),
                  SizedBox(height: 10),
                  Text(
                    "Corporate KYC",
                    style: TextStyle(
                      color: Pallet.fontcolornew,
                      fontSize: Pallet.heading1,
                      fontWeight: Pallet.font600,
                    ),
                  ),
                  SizedBox(height: 10),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: certificates(
                      width: widget.wdgtWidth * 0.8,
                    ),
                  ),
                  SizedBox(height: 10),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: shareholderslist(
                      width: widget.wdgtWidth * 0.8,
                    ),
                  ),
                  SizedBox(height: 10),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: finalsubmit(
                      width: widget.wdgtWidth * 0.8,
                    ),
                  ),
                  SizedBox(height: 25),
                ],
              ),
            ),
          );
        } else if (constraints.maxWidth >= 1024 &&
            constraints.maxWidth < 1366) {
          return Padding(
            padding: EdgeInsets.symmetric(
              horizontal: Pallet.leftPadding,
            ),
            child: SingleChildScrollView(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: ismobile != true
                    ? CrossAxisAlignment.start
                    : CrossAxisAlignment.center,
                children: [
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: notice(
                      width: widget.wdgtWidth * 0.73,
                    ),
                  ),
                  SizedBox(height: 10),
                  Text(
                    "Corporate KYC",
                    style: TextStyle(
                      color: Pallet.fontcolornew,
                      fontSize: Pallet.heading1,
                      fontWeight: Pallet.font600,
                    ),
                  ),
                  SizedBox(height: 10),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: certificates(
                      width: widget.wdgtWidth * 0.73,
                    ),
                  ),
                  SizedBox(height: 10),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: shareholderslist(
                      width: widget.wdgtWidth * 0.73,
                    ),
                  ),
                  SizedBox(height: 10),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: finalsubmit(
                      width: widget.wdgtWidth * 0.73,
                    ),
                  ),
                  SizedBox(height: 25),
                ],
              ),
            ),
          );
        } else {
          ismobile = false;

          return Padding(
            padding: EdgeInsets.symmetric(
              horizontal: Pallet.leftPadding,
            ),
            child: SingleChildScrollView(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: notice(
                      width: widget.wdgtWidth * 0.5,
                    ),
                  ),
                  SizedBox(height: 10),
                  Text(
                    "Corporate KYC",
                    style: TextStyle(
                      color: Pallet.fontcolornew,
                      fontSize: Pallet.heading1,
                      fontWeight: Pallet.font600,
                    ),
                  ),
                  ismobile == false
                      ? SizedBox(height: 20)
                      : SizedBox(height: 10),
                  SizedBox(height: 15),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: certificates(
                      width: widget.wdgtWidth * 0.5,
                    ),
                  ),
                  SizedBox(height: 15),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: shareholderslist(
                      width: widget.wdgtWidth * 0.5,
                    ),
                  ),
                  SizedBox(height: 15),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: finalsubmit(
                      width: widget.wdgtWidth * 0.5,
                    ),
                  ),
                  SizedBox(height: 25),
                ],
              ),
            ),
          );
        }
      },
    );
  }

  Widget notice({double width}) {
    return Padding(
      padding: const EdgeInsets.only(
        top: 5.0,
      ),
      child: Container(
          decoration: BoxDecoration(
            color: Colors.white,
            boxShadow: [
              BoxShadow(
                color: Colors.black12,
                blurRadius: 3,
                spreadRadius: 5,
              )
            ],
            borderRadius: BorderRadius.circular(10),
          ),
          // color: Colors.pink,
          padding: EdgeInsets.all(10),
          width: width,
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Container(
                padding: EdgeInsets.all(Pallet.defaultPadding / 2),
                decoration: BoxDecoration(
                  color: Pallet.fontcolornew,
                  borderRadius: BorderRadius.circular(Pallet.radius),
                ),
                child: Center(
                  child: Icon(
                    Icons.info_outline,
                    color: Pallet.fontcolor,
                    size: 30,
                  ),
                ),
              ),
              Container(
                width: width - 80,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    Text(
                      'KYC Status',
                      style: Pallet.profiletext,
                      // textAlign: TextAlign.justify,
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Text(
                      'Your KYC is not verified. Please upload the required documents and ID',
                      style: TextStyle(
                        color: Pallet.fontcolornew,
                      ),
                      // textAlign: TextAlign.justify,
                    ),
                  ],
                ),
              )
            ],
          )),
    );
  }

  Widget certificates({double width}) {
    return Container(
        padding: EdgeInsets.all(10),
        width: width,
        alignment: Alignment.topLeft,
        decoration: BoxDecoration(
          color: Colors.white,
          boxShadow: [
            BoxShadow(
              color: Colors.black12,
              blurRadius: 3,
              spreadRadius: 5,
            )
          ],
          borderRadius: BorderRadius.circular(10),
        ),
        child: width < 1366
            ? Wrap(
                children: [
                  Container(
                    width: 200,
                    padding: EdgeInsets.all(10),
                    child: Column(
                      children: [
                        Text(
                          "Company Certificate",
                          style: Pallet.profiletext,
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        if (uploadedImage == null)
                          Container(
                            height: 150,
                            width: 130,
                            child: Image.asset("corp_cert.png"),
                          ),
                        if (uploadedImage != null)
                          Container(
                            height: 150,
                            width: 130,
                            child: Image.memory(uploadedImage),
                          ),
                        SizedBox(
                          height: 10,
                        ),
                        uploadedImage == null
                            ? CustomButton(
                                buttoncolor: Pallet.fontcolornew,
                                textcolor: Pallet.fontcolor,
                                fontweight: Pallet.font600,
                                text: "Attach File",
                                onpress: () {
                                  setState(() {
                                    uploadImage();
                                  });
                                },
                              )
                            : Center(
                                child: IconButton(
                                    icon: Icon(Icons.delete),
                                    onPressed: () {
                                      setState(() {
                                        uploadedImage = null;
                                      });
                                    }),
                              ),
                      ],
                    ),
                  ),
                  Container(
                    width: 200,
                    // alignment: Alignment.center,
                    padding: EdgeInsets.all(10),
                    child: Column(
                      children: [
                        Text(
                          "Tax Certificate",
                          style: Pallet.profiletext,
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        if (uploadedImage1 == null)
                          Container(
                            height: 150,
                            width: 130,
                            child: Image.asset("tax.png"),
                          ),
                        if (uploadedImage1 != null)
                          Container(
                            height: 150,
                            width: 130,
                            child: Image.memory(uploadedImage1),
                          ),
                        SizedBox(
                          height: 10,
                        ),
                        uploadedImage1 == null
                            ? CustomButton(
                                buttoncolor: Pallet.fontcolornew,
                                textcolor: Pallet.fontcolor,
                                fontweight: Pallet.font600,
                                text: "Attach File",
                                onpress: () {
                                  setState(() {
                                    uploadImage1();
                                  });
                                },
                              )
                            : Center(
                                child: IconButton(
                                    icon: Icon(Icons.delete),
                                    onPressed: () {
                                      setState(() {
                                        uploadedImage1 = null;
                                      });
                                    }),
                              ),
                      ],
                    ),
                  ),
                  Padding(
                    padding: ismobile != true
                        ? EdgeInsets.all(30)
                        : EdgeInsets.only(left: 1),
                    child: Container(
                      // alignment: Alignment.topRight,
                      child: Container(
                        padding: EdgeInsets.all(10),
                        decoration: BoxDecoration(
                          color: Colors.grey[100],
                          borderRadius: BorderRadius.circular(10),
                        ),
                        width: 250,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              "Note:",
                              style: TextStyle(
                                color: Pallet.fontcolornew,
                                fontWeight: FontWeight.w700,
                              ),
                            ),
                            SizedBox(
                              height: 8,
                            ),
                            Text(
                              "   1.All fieds are required",
                              style: Pallet.profiletextsubheading,
                            ),
                            SizedBox(
                              height: 5,
                            ),
                            Text(
                              "   2.Max size per file: 2MB",
                              style: Pallet.profiletextsubheading,
                            ),
                            SizedBox(
                              height: 5,
                            ),
                            Text(
                              "   3.File formats support: gif,jpg, png, jpeg, pdf",
                              style: Pallet.profiletextsubheading,
                            ),
                            SizedBox(
                              height: 5,
                            ),
                            Text(
                              "   4.Identity proof should be valid",
                              style: Pallet.profiletextsubheading,
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                ],
              )
            : Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Row(
                    children: [
                      Container(
                        width: 200,
                        padding: EdgeInsets.all(10),
                        child: Column(
                          children: [
                            Text(
                              "Company Certificate",
                              style: Pallet.profiletext,
                            ),
                            SizedBox(
                              height: 10,
                            ),
                            if (uploadedImage == null)
                              Container(
                                height: 150,
                                width: 130,
                                child: Image.asset("corp_cert.png"),
                              ),
                            if (uploadedImage != null)
                              Container(
                                height: 150,
                                width: 130,
                                child: Image.memory(uploadedImage),
                              ),
                            SizedBox(
                              height: 10,
                            ),
                            uploadedImage == null
                                ? CustomButton(
                                    buttoncolor: Pallet.fontcolornew,
                                    textcolor: Pallet.fontcolor,
                                    fontweight: Pallet.font600,
                                    text: "Attach File",
                                    onpress: () {
                                      setState(() {
                                        uploadImage();
                                      });
                                    },
                                  )
                                : Center(
                                    child: IconButton(
                                        icon: Icon(Icons.delete),
                                        onPressed: () {
                                          setState(() {
                                            uploadedImage = null;
                                          });
                                        }),
                                  ),
                          ],
                        ),
                      ),
                      Container(
                        width: 200,
                        // alignment: Alignment.center,
                        padding: EdgeInsets.all(10),
                        child: Column(
                          children: [
                            Text(
                              "Tax Certificate",
                              style: Pallet.profiletext,
                            ),
                            SizedBox(
                              height: 10,
                            ),
                            if (uploadedImage1 == null)
                              Container(
                                height: 150,
                                width: 130,
                                child: Image.asset("tax.png"),
                              ),
                            if (uploadedImage1 != null)
                              Container(
                                height: 150,
                                width: 130,
                                child: Image.memory(uploadedImage1),
                              ),
                            SizedBox(
                              height: 10,
                            ),
                            uploadedImage1 == null
                                ? CustomButton(
                                    buttoncolor: Pallet.fontcolornew,
                                    textcolor: Pallet.fontcolor,
                                    fontweight: Pallet.font600,
                                    text: "Attach File",
                                    onpress: () {
                                      setState(() {
                                        uploadImage1();
                                      });
                                    },
                                  )
                                : Center(
                                    child: IconButton(
                                        icon: Icon(Icons.delete),
                                        onPressed: () {
                                          setState(() {
                                            uploadedImage1 = null;
                                          });
                                        }),
                                  ),
                          ],
                        ),
                      ),
                    ],
                  ),
                  Padding(
                    padding: EdgeInsets.only(left: 1),
                    child: Container(
                      // alignment: Alignment.topRight,
                      child: Container(
                        padding: EdgeInsets.all(10),
                        decoration: BoxDecoration(
                          color: Colors.grey[100],
                          borderRadius: BorderRadius.circular(10),
                        ),
                        width: 250,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              "Note:",
                              style: TextStyle(
                                color: Pallet.fontcolornew,
                                fontWeight: FontWeight.w700,
                              ),
                            ),
                            SizedBox(
                              height: 8,
                            ),
                            Text(
                              "   1.All fieds are required",
                              style: Pallet.profiletextsubheading,
                            ),
                            SizedBox(
                              height: 5,
                            ),
                            Text(
                              "   2.Max size per file: 2MB",
                              style: Pallet.profiletextsubheading,
                            ),
                            SizedBox(
                              height: 5,
                            ),
                            Text(
                              "   3.File formats support: gif,jpg, png, jpeg, pdf",
                              style: Pallet.profiletextsubheading,
                            ),
                            SizedBox(
                              height: 5,
                            ),
                            Text(
                              "   4.Identity proof should be valid",
                              style: Pallet.profiletextsubheading,
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                ],
              ));
  }

  Widget shareholderslist({double width}) {
    return Padding(
      padding: const EdgeInsets.only(top: 20),
      child: Container(
          padding: EdgeInsets.all(10),
          decoration: BoxDecoration(
            color: Colors.white,
            boxShadow: [
              BoxShadow(
                color: Colors.black12,
                blurRadius: 3,
                spreadRadius: 5,
              )
            ],
            borderRadius: BorderRadius.circular(10),
          ),
          width: width,
          child: Column(
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    'Name',
                    style: Pallet.profiletext,
                  ),
                  Text(
                    'Shares',
                    style: Pallet.profiletext,
                  ),
                  Text(
                    'KYC Status',
                    style: Pallet.profiletext,
                  ),
                  Text(
                    'Action',
                    style: Pallet.profiletext,
                  )
                ],
              ),
              SizedBox(
                height: 10,
              ),
              Divider(),
              shareholders.length == 0
                  ? SizedBox(
                      height: 5,
                    )
                  : SizedBox(
                      height: 10,
                    ),
              for (var i = 0; i < shareholders.length; i++)
                Column(
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Container(
                          width: 50,
                          child: Text(
                            shareholders[i]['name'] +
                                ' ' +
                                shareholders[i]['lastname'],
                            overflow: TextOverflow.ellipsis,
                            style: Pallet.profiletextsubheading,
                          ),
                        ),
                        Text(
                          shareholders[i]['share'] + "%",
                          overflow: TextOverflow.ellipsis,
                          style: Pallet.profiletextsubheading,
                        ),
                        Text(
                          shareholders[i]['action'],
                          overflow: TextOverflow.ellipsis,
                          style: Pallet.profiletextsubheading,
                        ),
                        shareholders[i]['action'] != "Verified"
                            ? InkWell(
                                onTap: () {
                                  setState(() {
                                    Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                        builder: (BuildContext context) => Home(
                                          route: 'kyc_page',
                                        ),
                                      ),
                                    );
                                  });
                                },
                                child: Container(
                                    decoration: BoxDecoration(
                                      color: Pallet.fontcolornew,
                                      borderRadius: BorderRadius.circular(
                                        5,
                                      ),
                                    ),
                                    padding: EdgeInsets.all(5),
                                    child: Center(
                                      child: Icon(
                                        Icons.arrow_forward,
                                        color: Pallet.fontcolor,
                                      ),
                                    )),
                              )
                            : InkWell(
                                onTap: () {
                                  ScaffoldMessenger.of(context)
                                      .showSnackBar(SnackBar(
                                    content: Text(shareholders[i]['name'] +
                                        "'s KYC is Already Verified"),
                                  ));
                                },
                                child: Container(
                                    decoration: BoxDecoration(
                                      color: Colors.green,
                                      borderRadius: BorderRadius.circular(
                                        5,
                                      ),
                                    ),
                                    padding: EdgeInsets.all(5),
                                    child: Center(
                                      child: Icon(
                                        Icons.check,
                                        color: Pallet.fontcolor,
                                      ),
                                    )),
                              ),
                      ],
                    ),
                    Divider(),
                  ],
                ),
            ],
          )),
    );
  }

  Widget finalsubmit({double width}) {
    return Container(
      width: width,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          // ignore: deprecated_member_use
          RaisedButton(
              color: Pallet.fontcolornew,
              child: Center(
                  child: Text('Submit',
                      style: TextStyle(
                        color: Pallet.fontcolor,
                        fontWeight: Pallet.font600,
                      ))),
              onPressed: () {
                var status = true;

                for (var i = 0; i < shareholders.length; i++) {
                  if (shareholders[i]['action'] != 'Verified') {
                    status = false;
                  }
                }

                setState(() {
                  if (uploadedImage == null) {
                    ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                        content: Text("Please Upload Company Cerificate ")));
                  } else if (uploadedImage1 == null) {
                    ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                        content: Text("Please Upload Tax Cerificate ")));
                  } else if (status != true) {
                    ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                        content:
                            Text("Please Complete All ShareHolder's KYC")));
                  } else {
                    ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                        content: Text("Successfully Corporate KYC Verified!")));
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (BuildContext context) => Home(
                                  route: 'dashboard',
                                )));
                  }
                });
              }),
        ],
      ),
    );
  }
}
