import 'package:centurion/config.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../config/app_settings.dart';
import '../home.dart';
import '../services/communication/index.dart' show HttpRequest;
import '../utils/utils.dart';
import 'package:carousel_slider/carousel_slider.dart';
import '../common.dart';
import '../services/business/cart.dart';
import 'cataegory.dart';

Map data = {};
// ignore: non_constant_identifier_names
String sample_value;

class NewProducts extends StatefulWidget {
  const NewProducts(
      {Key key,
      this.wdgtWidth,
      this.wdgtHeight,
      this.productId,
      this.product,
      this.setData,
      this.setShop})
      : super(key: key);
  final double wdgtWidth, wdgtHeight;
  final Map<String, dynamic> product;
  final setShop;
  final setData;
  final int productId;
  // static _NewProductsState of(BuildContext context) =>
  //     context.findAncestorStateOfType<_NewProductsState>();
  @override
  _NewProductsState createState() => _NewProductsState();
}

class _NewProductsState extends State<NewProducts> {
  bool singleTap = true;
  int quantity;
  int merchantProductID;

  shopDetailAlert(context) async {
    final prefs = await SharedPreferences.getInstance();

    return showDialog(
        barrierDismissible: false,
        context: context,
        builder: (BuildContext context) {
          return Container(
            // decoration: BoxDecoration(border: Border.all(color: Colors.pink)),
            // height: 600,
            child: Padding(
              padding: EdgeInsets.all(Pallet.defaultPadding),
              child: Center(
                child: Stack(
                  children: [
                    Image.asset(
                      "newcenturionpromotion.png",
                      // height: 800,
                      // width: 900,
                    ),

                    Positioned(
                      top: 0,
                      right: 0,
                      child: Container(
                        decoration: BoxDecoration(
                            shape: BoxShape.circle, color: Colors.white),
                        width: 50,
                        height: 50,
                        // ignore: deprecated_member_use
                        child: FlatButton(
                          // decoration: BoxDecoration(
                          //   shape: BoxShape.circle,
                          // ),
                          // width: 50,
                          // height: 50,
                          onPressed: () {
                            Navigator.of(context).pop();
                            prefs.setBool('myshopbanner', true);
                            if (cart.askpopup == false) {
                              if (cart.address.length == 0 ||
                                  cart.address.length == null) {
                                showDialog(
                                    barrierDismissible: false,
                                    context: context,
                                    builder: (BuildContext context) {
                                      return AddressProcessPopUp(
                                        isAdd: 'true',
                                        // shopContext: context,
                                        // index: 0,
                                        // setData: setState,
                                      );
                                    });
                              }
                            }
                          },
                          child: Icon(Icons.close, color: Colors.red),
                        ),
                      ),
                    )
                    // CircleAvatar(
                    // child: InkWell(
                    //     onTap: () {
                    //       Navigator.of(context).pop();
                    //     },
                    //     child: Icon(Icons.close, color: Colors.red)),
                    // )
                  ],
                ),
              ),
            ),
          );
        });
  }

  shopbanner(context) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setInt('product', widget.productId);
    bool isbannerenabled = prefs.getBool('shopbannershow');
    print("sssssssssssssssslllllllllll");
    print(prefs.getBool('shopbannershow'));
    if (isbannerenabled == null) {
      print("lllllllllll");
      await shopDetailAlert(context);
      prefs.setBool('shopbannershow', false);
      print(prefs.getBool('shopbannershow'));
    } else if (isbannerenabled == true) {
      print("bbbbbbbbbbbbbbbbblllllllllll");
      await shopDetailAlert(context);
      prefs.setBool('shopbannershow', false);
    } else if (isbannerenabled == false) {
      print("iiiiiiiiisssssssssssss");
      if (cart.askpopup == false) {
        if (cart.address.length == 0 || cart.address.length == null) {
          // await showDialog(
          //     barrierDismissible: false,
          //     context: context,
          //     builder: (BuildContext context) {
          //       return AddressProcessPopUp(
          //         // shopContext: context,
          //         isAdd: 'true',
          //         notifyParent: refresh,
          //         // index: 0,
          //         // setData: setState,
          //       );
          //     });
        }
      }

      print("done");
    }
  }

  refresh() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();

    print("mnmnmnmnmbbnvnvhvj");
    //cart.getAddressInfo(setState);
    //cart.country.text = await cart.address[cart.selectRadio]["address_name"];
    //print(cart.address[cart.selectRadio]["address_name"]);
    print("uobdhknksnhbgyf");
    //await cart.setDefaultAddress(cart.country.text);
    setState(() {
      getVariants();
    });
    int pid = prefs.getInt('product');
    shopRoute = 'details';
    productId = pid;
    storeId = 1;
    cart.categoryName = 'Aspire';
    //getProductDetail(widget.productId, 1, true);
    Navigator.push(
      context,
      MaterialPageRoute(
          builder: (context) => Home(
                route: 'centurion_shop',
                param: 'true',
              )),
    );

    print("dddddddddfgggtre");
  }

  @override
  void initState() {
    setPageDetails();
    _temp = getProductDetail(widget.productId, 1, true);
    if (cart.selectRadio == null || cart.selectRadio == 0) {
      cart.getAddressInfo(setState);
    } else {
      cart.country.text = cart.address[cart.selectRadio]["address_name"];
    }
    cart.getAddressInfo(setState);
    // allVariantsList = [];
    // availableCombinations = {};
    // allVariants = {};
    //print(isDescription);

    super.initState();

    WidgetsBinding.instance.addPostFrameCallback((_) async {
      shopbanner(context);
    });
    // getAvailableVariantCombination({'color': 'black', 'size': 'XL'});
    super.initState();
  }

  Map<String, dynamic> pageDetails = {};

  void setPageDetails() async {
    String data = await Utils.getPageDetails('detailspage');
    setState(() {
      pageDetails = Utils.fromJSONString(data);
    });
  }

  // ignore: non_constant_identifier_names
  int final_variant_id = 0;

  final CarouselController _controller = CarouselController();
  bool isDescription = true;
  bool canAddToCart = false;
  int isSelected;
  int storeID;

  Future<String> _temp;
  // String selectedImage;

  Map<String, dynamic> variants = {};
  static Map<String, dynamic> allVariants = {};
  static Map<String, dynamic> _selectedVariants = {};
  static Map<String, dynamic> availableCombinations = {};
  // static Map<String, dynamic> _sele = {};
  static List<dynamic> allVariantsList = [];
  // ignore: non_constant_identifier_names
  static List<dynamic> varient_id_list = [];
  // ignore: non_constant_identifier_names

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
        future: _temp,
        builder: (context, snapshot) {
          if (snapshot.hasError) {
            return SomethingWentWrongMessage();
          } else if (snapshot.hasData) {
            // ignore: missing_return
            return LayoutBuilder(builder: (context, constraints) {
              if (constraints.maxWidth <= 600)
                return SingleChildScrollView(
                    padding: EdgeInsets.all(10),
                    child: Column(
                      children: [
                        Padding(
                          padding: EdgeInsets.only(bottom: Pallet.leftPadding),
                          child: buildTitle(context),
                        ),
                        image(wdgtwidth: widget.wdgtWidth),
                        Padding(
                          padding: EdgeInsets.symmetric(
                              vertical: Pallet.defaultPadding),
                          child: details(wdgtWidth: widget.wdgtWidth),
                        ),
                        // Padding(
                        //   padding: EdgeInsets.symmetric(
                        //       vertical: Pallet.defaultPadding),
                        //   child: description(wdgtWidth: widget.wdgtWidth),
                        // )
                        // Padding(
                        //   padding: EdgeInsets.symmetric(
                        //       vertical: Pallet.defaultPadding),
                        //   child: detail2(wdgtwidth: widget.wdgtWidth),
                        // )
                      ],
                    ));
              else if (constraints.maxWidth > 600 &&
                  constraints.maxWidth < 1024) {
                return Padding(
                  padding: EdgeInsets.all(Pallet.leftPadding),
                  child: Container(
                      width: widget.wdgtWidth,
                      height: widget.wdgtHeight,
                      child: SingleChildScrollView(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            buildTitle(context),
                            SizedBox(
                              height: 20,
                            ),
                            Container(
                              width: widget.wdgtWidth,
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  image(
                                      wdgtwidth: widget.wdgtWidth * .4,
                                      height: widget.wdgtWidth * .3),
                                  // Padding(
                                  //   padding: EdgeInsets.symmetric(
                                  //       vertical: Pallet.defaultPadding),
                                  //   child: detail2(
                                  //       wdgtwidth: widget.wdgtWidth * .4),
                                  // ),
                                  details(
                                      wdgtWidth: widget.wdgtWidth * .45,
                                      height: widget.wdgtWidth * .3)
                                ],
                              ),
                            ),
                            // SizedBox(height: 20),
                            // description(wdgtWidth: widget.wdgtWidth * .9),
                            // SizedBox(height: 20),
                          ],
                        ),
                      )),
                );
              } else if (constraints.maxWidth > 1024 &&
                  constraints.maxWidth < 1540) {
                return Padding(
                  padding: EdgeInsets.symmetric(horizontal: Pallet.topPadding),
                  child: Container(
                      width: widget.wdgtWidth,
                      height: widget.wdgtHeight,
                      child: SingleChildScrollView(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            buildTitle(context),

                            SizedBox(
                              height: 30,
                            ),
                            Container(
                              width: widget.wdgtWidth,
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  image(
                                      wdgtwidth: widget.wdgtWidth * .4,
                                      height: widget.wdgtWidth * .3),
                                  // Padding(
                                  //   padding: EdgeInsets.symmetric(
                                  //       vertical: Pallet.defaultPadding),
                                  //   child: detail2(
                                  //       wdgtwidth: widget.wdgtWidth * .4),
                                  // ),
                                  SizedBox(height: 20),
                                  details(
                                      wdgtWidth: widget.wdgtWidth * .45,
                                      height: widget.wdgtWidth * .3)
                                ],
                              ),
                            ),
                            // SizedBox(height: 30),
                            // description(wdgtWidth: widget.wdgtWidth * .9),
                            // SizedBox(height: 20),
                          ],
                        ),
                      )),
                );
              } else {
                return Padding(
                  padding: EdgeInsets.all(Pallet.leftPadding),
                  child: Container(
                      width: widget.wdgtWidth,
                      height: widget.wdgtHeight,
                      child: SingleChildScrollView(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            buildTitle(context),

                            SizedBox(
                              height: 20,
                            ),
                            Container(
                              width: widget.wdgtWidth,
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  image(
                                      wdgtwidth: widget.wdgtWidth * .4,
                                      height: widget.wdgtWidth * .3),
                                  // Padding(
                                  //   padding: EdgeInsets.symmetric(
                                  //       vertical: Pallet.defaultPadding),
                                  //   child: detail2(
                                  //       wdgtwidth: widget.wdgtWidth * .4),
                                  // ),
                                  details(
                                      wdgtWidth: widget.wdgtWidth * .45,
                                      height: widget.wdgtWidth * .3)
                                ],
                              ),
                            ),
                            // SizedBox(height: 20),
                            // description(wdgtWidth: widget.wdgtWidth * .9),
                            // SizedBox(height: 20),
                          ],
                        ),
                      )),
                );
              }
            });
          }
          return Loader();
        });
  }

  Row buildTitle(BuildContext context) {
    return Row(
      children: [
        IconButton(
          icon: Icon(
            Icons.arrow_back,
            color: Pallet.fontcolornew,
          ),
          onPressed: () {
            widget.setShop(() {
              shopRoute = 'products';
            });
            // Navigator.pushReplacement(
            //     context,
            //     MaterialPageRoute(
            //         builder: (context) => Home(route: 'centurion_shop')));
          },
        ),
        Expanded(
          child: MyText(
              text: data["products"]["product_name"],
              overflow: TextOverflow.ellipsis,
              style: TextStyle(
                color: Pallet.fontcolornew,
                fontSize: Pallet.heading1,
              )),
        )
      ],
    );
  }

  Widget description({
    double wdgtWidth,
  }) {
    return Container(
        width: wdgtWidth,
        decoration: BoxDecoration(
            border: Border.all(color: Pallet.dashcontainerback),
            borderRadius: BorderRadius.circular(Pallet.radius)),
        // padding: EdgeInsets.all(Pallet.defaultPadding),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(Pallet.radius),
                border: Border.all(color: Pallet.dashcontainerback),
              ),
              child: Row(
                children: [
                  InkWell(
                    onTap: () {
                      setState(() {
                        isDescription = true;
                      });
                    },
                    child: Container(
                      // margin: EdgeInsets.all(Pallet.defaultPadding),
                      decoration: BoxDecoration(
                          color: isDescription == true
                              ? Pallet.dashcontainerback
                              : Pallet.fontcolor,
                          borderRadius: BorderRadius.circular(Pallet.radius)),
                      child: Center(
                        child: Padding(
                          padding: EdgeInsets.all(Pallet.defaultPadding),
                          child: MyText(
                            text: pageDetails['text1'],
                            style: TextStyle(
                                fontSize: Pallet.heading4,
                                color: isDescription == true
                                    ? Pallet.fontcolor
                                    : Pallet.dashcontainerback,
                                fontWeight: Pallet.bold),
                          ),
                        ),
                      ),
                    ),
                  ),
                  InkWell(
                    onTap: () {
                      setState(() {
                        isDescription = false;
                      });
                    },
                    child: Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(Pallet.radius),
                        color: isDescription == false
                            ? Pallet.dashcontainerback
                            : Pallet.fontcolor,
                      ),

                      // margin: EdgeInsets.all(Pallet.defaultPadding),

                      child: Padding(
                        padding: EdgeInsets.all(Pallet.defaultPadding),
                        child: MyText(
                          text: pageDetails['text2'],
                          style: TextStyle(
                              fontSize: Pallet.heading4,
                              color: isDescription == false
                                  ? Pallet.fontcolor
                                  : Pallet.dashcontainerback,
                              fontWeight: Pallet.bold),
                        ),
                      ),
                    ),
                  ),
                  Expanded(child: Container())
                ],
              ),
            ),
            Container(
                padding: EdgeInsets.all(Pallet.defaultPadding / 2),
                child:
                    // isDescription == true?
                    MyText(
                  text: isDescription == true
                      ? data["products"]["description"]
                      : data["products"]["notes"],
                  style: TextStyle(
                      color: Pallet.dashcontainerback,
                      fontSize: Pallet.heading4),
                  textAlign: TextAlign.justify,
                )
                // : MyText(
                //   text:  data["products"]["notes"],
                //     // maxLines: 1,
                //     style: TextStyle(
                //         color: Pallet.dashcontainerback,
                //         fontSize: Pallet.heading4),
                //     textAlign: TextAlign.justify,
                //   ),
                )
          ],
        ));
  }

  Widget image({double wdgtwidth, height}) {
    return Container(
      width: wdgtwidth,

      // height: height,
      // color: Colors.black,
      child: Container(
          child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          ClipRRect(
            borderRadius: BorderRadius.circular(10),
            child: Container(
              // width: wdgtwidth,
              // height: height,
              decoration: BoxDecoration(
                color: Colors.transparent,
              ),
              child: Stack(
                children: [
                  InteractiveViewer(
                      maxScale: 2.0,
                      scaleEnabled: true,
                      child: CarouselSlider.builder(
                          carouselController: _controller,
                          itemCount: data["products"]["images"].length,
                          itemBuilder: (BuildContext context, int index, _) {
                            return Image.network(
                              appSettings['SERVER_URL'] +
                                  '/' +
                                  data["products"]["images"][index]["image_url"]
                                      .toString(),
                              width: wdgtwidth,
                              height: height,
                            );
                          },
                          options: CarouselOptions(
                            autoPlay: false,
                            enableInfiniteScroll: false,
                          ))),
                  data["products"]["images"].length > 1
                      ? Container(
                          padding: EdgeInsets.symmetric(
                              horizontal: Pallet.defaultPadding),
                          width: wdgtwidth,
                          height: height,
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              IconButton(
                                icon: Icon(Icons.arrow_back_ios,
                                    color: Pallet.dashcontainerback),
                                onPressed: () {
                                  _controller.previousPage();
                                },
                              ),
                              IconButton(
                                icon: Icon(
                                  Icons.arrow_forward_ios,
                                  color: Pallet.dashcontainerback,
                                ),
                                onPressed: () {
                                  _controller.nextPage();
                                },
                              ),
                            ],
                          ),
                        )
                      : Container()
                ],
              ),
            ),
          ),
          Container(
            height: 120,
            child: ListView.builder(
                scrollDirection: Axis.horizontal,
                itemCount: data["products"]["images"].length,
                itemBuilder: (BuildContext context, int index) {
                  return Padding(
                    padding: const EdgeInsets.all(
                      10,
                    ),
                    child: InkWell(
                      onTap: () {
                        _controller.jumpToPage(index);
                      },
                      child: Padding(
                        padding: EdgeInsets.only(top: Pallet.defaultPadding),
                        child: Container(
                          decoration: BoxDecoration(
                            boxShadow: [Pallet.shadowEffect],
                            //  borderRadius: BorderRadius.circular(Pallet.radius),
                            // border: index == null
                            //     ? ''
                            //     : Border.all(
                            //         color: Pallet.dashcontainerback)
                          ),
                          child: Image.network(appSettings['SERVER_URL'] +
                              '/' +
                              data["products"]["images"][index]["image_url"]
                                  .toString()),
                        ),
                      ),
                    ),
                  );
                }),
          )
        ],
      )),
    );
  }

  Widget details({double wdgtWidth, height}) {
    // ignore: unused_local_variable
    Size size = MediaQuery.of(context).size;

    return Container(
      width: wdgtWidth,
      // height: height,
      child:
          Column(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
        // SizedBox(height: 15),
        Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              MyText(
                text: data["products"]["description"],
                style: TextStyle(
                    color: Pallet.dashcontainerback, fontSize: Pallet.heading4),
                textAlign: TextAlign.justify,
              ),
              SizedBox(height: 15),
              MyText(
                text: data["products"]["product_name"],
                style: TextStyle(
                    fontSize: Pallet.heading4 + 15,
                    color: Pallet.dashcontainerback,
                    fontWeight: Pallet.bold),
              ),
              SizedBox(height: 15),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  // MyText(
                  //   'Price',
                  //   style: TextStyle(
                  //     color: Pallet.dashcontainerback,
                  //     fontSize: Pallet.heading4,
                  //     fontWeight: Pallet.bold,
                  //   ),
                  // ),
                  Row(
                    children: [
                      MyText(
                        text: '\$ ' + data["products"]["new_price"].toString(),
                        style: TextStyle(
                            color: Pallet.dashcontainerback,
                            fontSize: Pallet.heading4 + 15,
                            fontWeight: Pallet.bold),
                      ),
                      SizedBox(width: 10),
                      SizedBox(width: 15),
                    ],
                  ),
                ],
              ),
              SizedBox(height: 15),
              Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    MyText(
                      text: pageDetails['text3'],
                      style: TextStyle(
                        color: Pallet.dashcontainerback,
                        fontSize: 20,
                        fontWeight: Pallet.bold,
                      ),
                    ),
                    SizedBox(height: 5),
                    data["products"]["is_avalable"] == true
                        ? cart.categoryName == 'Aspire'
                            ? MyText(
                                text: pageDetails['text4'],
                                style: TextStyle(
                                    color: Pallet.dashcontainerback,
                                    fontSize: 15),
                              )
                            : MyText(
                                text: pageDetails['text12'],
                                style: TextStyle(
                                    color: Pallet.dashcontainerback,
                                    fontSize: 15),
                              )
                        : cart.address.length > 0 &&
                                data["products"]["variants"].length > 0
                            ? MyText(
                                text: pageDetails['text6'],
                                style: TextStyle(
                                    color: Pallet.failed, fontSize: 15))
                            : cart.address.length == 0 ||
                                    data["products"]["is_avalable"] == false
                                ? MyText(
                                    text: pageDetails['text13'],
                                    style: TextStyle(
                                        color: Pallet.failed, fontSize: 15),
                                  )
                                : MyText(
                                    text: pageDetails['text5'],
                                    style: TextStyle(
                                        color: Pallet.failed, fontSize: 15),
                                  ),
                  ]),
              SizedBox(height: 15),
              Container(
                // width: 300,
                // height: 70 * double.parse(variants.length.toString()),
                child: Column(
                  children: [
                    for (var variant in variants.keys)
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          MyText(
                            text: variant,
                            style: TextStyle(
                              color: Pallet.dashcontainerback,
                              fontSize: 20,
                              fontWeight: Pallet.bold,
                            ),
                          ),
                          Container(
                            margin: EdgeInsets.only(top: 10),
                            height: 40,
                            child: ListView(
                                scrollDirection: Axis.horizontal,
                                children: [
                                  for (Map<String, dynamic> temp
                                      in variants[variant])
                                    InkWell(
                                      // onHover: (isHover) {
                                      //   // if (temp['isEnabled'])
                                      //   setState(() {
                                      //     prepareInput(
                                      //         {variant: temp['value']});
                                      //     sample_value = temp['value'];
                                      //     for (var variant_id_val
                                      //         in varient_id_list) {
                                      //       if (sample_value ==
                                      //           variant_id_val[
                                      //               'variantValue']) {
                                      //         final_variant_id =
                                      //             variant_id_val['variantId'];
                                      //       }
                                      //     }
                                      //     getProductDetail(widget.productId,
                                      //         final_variant_id, false);
                                      //   });
                                      //   // setState(() {
                                      //   //   // getProductDetail(
                                      //   //   //   widget.productId,
                                      //   //   //   final_variant_id,
                                      //   //   // );
                                      //   // });
                                      // },
                                      onTap: () {
                                        // if (temp['isEnabled'])
                                        setState(() {
                                          prepareInput(
                                              {variant: temp['value']});
                                          sample_value = temp['value'];
                                          for (var variant_id_val
                                              in varient_id_list) {
                                            if (sample_value ==
                                                variant_id_val[
                                                    'variantValue']) {
                                              final_variant_id =
                                                  variant_id_val['variantId'];
                                            }
                                          }
                                          getProductDetail(widget.productId,
                                              final_variant_id, false);
                                        });
                                        // setState(() {
                                        //   // getProductDetail(
                                        //   //   widget.productId,
                                        //   //   final_variant_id,
                                        //   // );
                                        // });
                                      },
                                      child: Container(
                                        margin: EdgeInsets.only(right: 10),
                                        padding: EdgeInsets.symmetric(
                                            horizontal: 15, vertical: 0),
                                        decoration: BoxDecoration(
                                            borderRadius:
                                                BorderRadius.circular(5),
                                            color: temp['isSelected']
                                                ? Pallet.dashcontainerback
                                                : Pallet.fontcolor,
                                            border: Border.all(
                                                color: temp['isSelected']
                                                    ? Colors.blue
                                                    : Colors.white)),
                                        child: Center(
                                          child: MyText(
                                            text: temp['value'].toString(),
                                            style: TextStyle(
                                              color: temp['isSelected']
                                                  ? Pallet.fontcolor
                                                  : Pallet.dashcontainerback,
                                              fontWeight: Pallet.bold,
                                            ),
                                          ),
                                        ),
                                      ),
                                    ),
                                ]),
                          )
                        ],
                      )
                  ],
                ),
              ),
              Row(
                // mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                    width: wdgtWidth * .35,
                    child: CustomButton(
                      onpress: () {
                        //print(widget.product);
                        //print('======================================');
                        // setState(() {
                        //   cart.add(widget.product, setState, widget.setData, context);

                        // });
                        // await cart.getAddressInfo(setState);

                        // if (cart.address.isEmpty) {
                        //   showDialog(
                        //       context: context,
                        //       builder: (context) {
                        //         return AddressPopUp(
                        //           setData: setState,
                        //         );
                        //       });
                        // } else {

                        // }
                        setState(() {
                          for (var item in cart.cart) {
                            storeID = item['store_id'];
                            print(storeID);
                          }
                          if (storeID == storeId || storeID == null) {
                            print('11111111111111');
                            print(storeId);
                            //------------conditions for merchantproduct flow------------
                            if (storeId == 3) {
                              print('222222222222222');

                              for (var item in cart.cart) {
                                quantity = item['qty'];
                                merchantProductID = item['product_id'];
                              }
                              if (cart.cart.isEmpty) {
                                print('33333333333');

                                canAddToCart = true;
                              } else {
                                print('444444444');

                                canAddToCart = false;
                              }
                            } else {
                              print('55555555');

                              canAddToCart = true;
                            }
                          }

                          if (canAddToCart == true) {
                            print('66666666666');

                            cartAddition();
                          } else {
                            print('7777777777');

                            if (storeId == 3) {
                              print(storeId);

                              print('8888888888');

                              if (merchantProductID != productId) {
                                print('9999999999999');

                                showDialogReplace();
                              } else if (cart.cart.isEmpty) {
                                cartAddition();
                              } else {
                                print('00000000000000');

                                alreadyInCart();
                              }
                            } else if (storeId != 3) {
                              print('101010101010101010101');
                              print(storeId);

                              if (data['products']['variants'].length < 1) {
                                print('qqqqqqqqqqqqqqqqqqq');

                                showDialogReplace();
                              } else if (final_variant_id == 0) {
                                print('wwwwwwwwwwwwwwwwwww');
                                snack.snack(title: pageDetails['text6']);

                                // final snackBar =
                                //     SnackBar(content: MyText(pageDetails['text6']));
                                // ScaffoldMessenger.of(context)
                                //     .showSnackBar(snackBar);
                              } else {
                                print('eeeeeeeeeeeeeeeee');
                                showDialogReplace();
                              }
                            }
                          }
                        });
                      },
                      vpadding: 10,
                      text: pageDetails['text7'],
                      buttoncolor: Pallet.fontcolornew,
                      textcolor: Pallet.fontcolor,
                      textsize: Pallet.heading3,
                    ),
                  ),
                  SizedBox(width: 20),
                  // InkWell(
                  //     onTap: () {},
                  //     child: Container(
                  //       padding: EdgeInsets.all(Pallet.defaultPadding),
                  //       decoration: BoxDecoration(
                  //         borderRadius: BorderRadius.circular(Pallet.radius),
                  //         color: Pallet.dashcontainerback,
                  //       ),
                  //       width: wdgtWidth * .3,
                  //       child: Center(
                  //         child: MyText(
                  //           'Buynow',
                  //           style: TextStyle(
                  //             color: Pallet.fontcolor,
                  //           ),
                  //         ),
                  //       ),
                  //     ))
                ],
              ),
            ]),

        SizedBox(height: 20),

        // description(wdgtWidth: wdgtWidth),
      ]),
    );
  }

  alreadyInCart() async {
    showDialog(
        barrierDismissible: false,
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            backgroundColor: Pallet.fontcolor,
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(Pallet.radius)),
            title: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Image.asset("c-logo.png",
                    width: 30, color: Pallet.fontcolornew),
                SizedBox(width: 10),
                Expanded(
                  child: MyText(
                      text: 'Already in cart',
                      style: TextStyle(
                          fontSize: Pallet.heading2,
                          color: Pallet.fontcolornew,
                          fontWeight: Pallet.font500)),
                ),
              ],
            ),
            content: Container(
              width: 350,
              child: (MyText(
                  text: 'This product has already added on your cart.',
                  style: TextStyle(
                      height: 1.2,
                      fontSize: Pallet.heading3,
                      color: Pallet.fontcolornew,
                      fontWeight: Pallet.font500))),
            ),
            actions: [
              PopupButton(
                  textcolor: Pallet.fontcolor,
                  buttoncolor: Pallet.fontcolornew,
                  text: pageDetails['text10'],
                  onpress: () {
                    Navigator.of(context).pop();
                  }),
            ],
          );
        });
  }

  showDialogReplace() async {
    showDialog(
        barrierDismissible: false,
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            backgroundColor: Pallet.fontcolor,
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(Pallet.radius)),
            title: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Image.asset("c-logo.png",
                    width: 30, color: Pallet.fontcolornew),
                SizedBox(width: 10),
                Expanded(
                  child: MyText(
                      text: pageDetails['text8'],
                      style: TextStyle(
                          fontSize: Pallet.heading2,
                          color: Pallet.fontcolornew,
                          fontWeight: Pallet.font500)),
                ),
              ],
            ),
            content: Container(
              width: 350,
              child: (MyText(
                  text: pageDetails['text9'],
                  style: TextStyle(
                      height: 1.2,
                      fontSize: Pallet.heading3,
                      color: Pallet.fontcolornew,
                      fontWeight: Pallet.font500))),
            ),
            actions: [
              PopupButton(
                  textcolor: Pallet.fontcolor,
                  buttoncolor: Pallet.fontcolornew,
                  text: pageDetails['text10'],
                  onpress: () {
                    Navigator.of(context).pop();
                  }),
              PopupButton(
                  textcolor: Pallet.fontcolor,
                  buttoncolor: Pallet.fontcolornew,
                  text: pageDetails['text11'],
                  onpress: () async {
                    print(cart.cart);
                    print(cart.cart.length);
                    await cart.removeAll(
                        setState,
                        widget.setData,
                        {
                          'product_id': data['products']['product_id'],
                          'product_name': data['products']['product_name'],
                          'new_price': data['products']['new_price'],
                          'price': data['products']['price'],
                          'shipping_cost': data['products']['shipping_cost'],
                          'variant_id':
                              final_variant_id == 0 ? 1 : final_variant_id,
                        },
                        context);

                    Navigator.of(context).pop();
                  }),
            ],
          );
        });
  }

  Widget detail2({double wdgtwidth}) {
    return Container(
        width: wdgtwidth,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            MyText(
              text: 'VEGAN & GLUNTERFREI',
              style: TextStyle(color: Pallet.dashcontainerback, fontSize: 15),
            ),
            MyText(
              text: 'doc vita toning sponge is 100% natural',
              style: TextStyle(color: Pallet.dashcontainerback, fontSize: 15),
            ),
            MyText(
              text: 'MADE IN GERMANY',
              style: TextStyle(color: Pallet.dashcontainerback, fontSize: 15),
            ),
          ],
        ));
  }

  // List getVariantId1 = [
  //   {'variantId': 'int', 'variantValue': 'String'},
  // ];
  Map<String, dynamic> getVariantId1 = {
    'variantId': 'int',
    'variantValue': 'String'
  };

  getVariants() {
    allVariantsList = [];
    allVariants = {};
    List<dynamic> variants = data["variants"];
    for (var variant in variants) {
      var option = {};
      for (var type in variant["variants"]) {
        String key = type["variant_type"];
        String value = type["value"];
        option[key] = value;
        getVariantId1 = {
          'variantId': variant["variant_id"],
          'variantValue': type["value"]
        };
        varient_id_list.add(getVariantId1);
        //print(getVariantId1);

        if (allVariants.keys.length == 0) {
          allVariants[key] = [value];
        } else {
          if (allVariants.keys.contains(key)) {
            if (!allVariants[key].contains(value)) {
              allVariants[key].add(value);
            }
          } else {
            allVariants[key] = [value];
          }
        }
      }
      allVariantsList.add(option);
    }
    //print(getVariantId1);
    //print(varient_id_list);

    availableCombinations = allVariants;
    addExtraAttributes(null);
  }

  prepareInput(Map<String, dynamic> _map) {
    _selectedVariants = {};
    if (_selectedVariants.keys.length == 0) {
      _selectedVariants = _map;
    } else {
      for (var _key in _map.keys) {
        if (_selectedVariants.keys.contains(_key) &&
            _selectedVariants.values.contains(_map[_key])) {
          _selectedVariants.remove(_key);
        } else {
          _selectedVariants[_key] = _map[_key];
        }
      }
    }
    getAvailableVariantCombination(_selectedVariants);
    // _input =
  }

  bool areAllElementsPresents(var list1, var list2) {
    for (var elem in list2) {
      if (!list1.contains(elem)) {
        return false;
      }
    }
    return true;
  }

  bool areListsEqual(var list1, var list2) {
    // check if both are lists
    if (!(list1 is List && list2 is List)
        // check if both have same length
        ||
        list1.length != list2.length) {
      return false;
    }

    // check if elements are equal
    for (int i = 0; i < list1.length; i++) {
      if (list1[i] != list2[i]) {
        return false;
      }
    }
    return true;
  }

  Future<String> getProductDetail(productId, variantID, init) async {
    Map<String, dynamic> _map;

    _map = {
      "system_product_id": 1,
      "product_id": productId,
      "variant_id": variantID,
      "enable_filter_protocol": true
    };

    Map<String, dynamic> _result = await HttpRequest.Post(
        'c_ps_get_product_details', Utils.constructPayload(_map));
    if (Utils.isServerError(_result)) {
      return throw (await Utils.getMessage(_result['response']['error']));
    } else {
      print(_result['response']['data']);
      print('lllllllllllllll');
      setState(() {
        data = _result['response']['data'];
        print('*******************************');
        print(data["products"]);
        print('*******************************');
      });
      // selectedImage =
      //     _result['response']['data']["products"]["images"][0]["image_url"];
      if (init) {
        getVariants();
      }
      return "start";
    }
  }

  getVariantId(Map<String, dynamic> input) {}

  addExtraAttributes(Map<String, dynamic> input) {
    for (var vKey in allVariants.keys) {
      variants[vKey] = [];
      for (var vValue in allVariants[vKey]) {
        variants[vKey]
            .add({"value": vValue, "isEnabled": false, "isSelected": false});
      }
      for (var vValue in variants[vKey]) {
        if (availableCombinations[vKey].contains(vValue['value'])) {
          vValue['isEnabled'] = true;
        }
        if (input != null) {
          if (vValue['value'] == input[vKey]) {
            vValue['isSelected'] = true;
          }
        }
      }
    }
  }

  getAvailableVariantCombination([Map<String, dynamic> input]) {
    availableCombinations = {};
    for (var val in allVariantsList) {
      if (areAllElementsPresents(val.keys, input.keys) &&
          areAllElementsPresents(val.values, input.values)) {
        if (availableCombinations.keys.length == 0) {
          for (var vKey in val.keys) {
            availableCombinations[vKey] = [val[vKey]];
          }
        } else {
          for (var vKey in val.keys) {
            if (availableCombinations.keys.contains(vKey)) {
              if (!availableCombinations[vKey].contains(val[vKey])) {
                availableCombinations[vKey].add(val[vKey]);
              }
            } else {
              // ignore: unnecessary_statements
              availableCombinations[vKey].add[val[vKey]];
            }
          }
        }
      }
    }
    addExtraAttributes(input);
  }

  cartAddition() {
    if (data['products']['variants'].length < 1) {
      //print(data['products']['variants'].length);

      cart.addToCart({
        'product_id': data['products']['product_id'],
        'product_name': data['products']['product_name'],
        'new_price': data['products']['new_price'],
        'price': data['products']['price'],
        'shipping_cost': data['products']['shipping_cost'],
        'variant_id': 1,
        // 'image_url': data['products']['images'][0]['image_url']
      }, setState, widget.setData, context);
    } else {
      //print(data['products']['variants'].length);
      //print('lllllllllllllllllllllllllllllll');

      if (final_variant_id == 0) {
        snack.snack(title: pageDetails['text6']);

        // final snackBar = SnackBar(content: MyText(pageDetails['text6']));
        // ScaffoldMessenger.of(context).showSnackBar(snackBar);
      } else {
        cart.addToCart({
          'product_id': data['products']['product_id'],
          'product_name': data['products']['product_name'],
          'new_price': data['products']['new_price'],
          'price': data['products']['price'],
          'shipping_cost': data['products']['shipping_cost'],
          'variant_id': final_variant_id,
          // 'image_url': data['products']['images'][0]['image_url']
        }, setState, widget.setData, context);
      }
    }
  }
}
