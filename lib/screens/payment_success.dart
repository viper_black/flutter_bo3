import 'package:flutter/material.dart';

import '../config.dart';
import '../home.dart';
// import '../login.dart';
// import 'dashboard.dart';

class Success extends StatefulWidget {
  final String value;

  const Success({Key key, this.value}) : super(key: key);
  @override
  _SuccessState createState() => _SuccessState();
}

class _SuccessState extends State<Success> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    // double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;
    return LayoutBuilder(
      builder: (context, constraints) {
        if (constraints.maxWidth <= ScreenSize.iphone) {
          return Scaffold(
            backgroundColor: Colors.white,
            body: Center(
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: success(
                  wdgtWidth: width,
                ),
              ),
            ),
          );
        } else if (constraints.maxWidth > ScreenSize.iphone &&
            constraints.maxWidth < ScreenSize.ipad) {
          return Scaffold(
            backgroundColor: Colors.white,
            body: Padding(
              padding: EdgeInsets.all(Pallet.leftPadding),
              child: success(
                wdgtWidth: width,
              ),
            ),
          );
        } else {
          return Scaffold(
            backgroundColor: Colors.white,
            body: Center(
              child: Padding(
                padding: EdgeInsets.all(Pallet.leftPadding),
                child: success(
                  wdgtWidth: width,
                ),
              ),
            ),
          );
        }
      },
    );
  }

  Widget success({double wdgtWidth, double wdgtHeight}) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Image.asset(
          "success.gif",
          //color: Colors.white.withOpacity(0.1),
          height: MediaQuery.of(context).size.height * 0.3,
          width: MediaQuery.of(context).size.width * 0.3,
        ),
        SizedBox(height: 15),
        InkWell(
          onTap: () {
            Navigator.pushReplacement(
                context,
                MaterialPageRoute(
                  builder: (BuildContext context) => Home(
                    route: 'dashboard',
                  ),
                ));
          },
          child: Text(
            "Payment Success Click here to continue....",
            style: TextStyle(
              color: Pallet.dashcontainerback,
              fontSize: 16,
            ),
          ),
        )
      ],
    );
  }
}

// class Snackbar{
// snack(context){
// ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text("")));
// }

// }
