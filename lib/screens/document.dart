import 'package:centurion/utils/utils.dart';
import 'package:flutter/material.dart';
import '../common.dart';
import '../config.dart';
import '../config/index.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:centurion/services/business/account.dart';

class Documents extends StatefulWidget {
  final double wdgtWidth, wdgtHeight;

  const Documents({Key key, this.wdgtWidth, this.wdgtHeight}) : super(key: key);
  @override
  _DocumentsState createState() => _DocumentsState();
}

class _DocumentsState extends State<Documents> {
  Future<String> temp;
  void initState() {
    temp = documentsData.getDocumentsdata(setState);
    super.initState();
  }

  Map<String, dynamic> pageDetails = {};
  _DocumentsState() {
    setPageDetails();
  }

  void setPageDetails() async {
    String data = await Utils.getPageDetails('documentation');
    setState(() {
      pageDetails = Utils.fromJSONString(data);
    });
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
        future: temp,
        builder: (context, snapshot) {
          if (snapshot.hasError) {
            if (snapshot.error == 'Something Went Wrong') {
              return SomethingWentWrongMessage();
            } else {
              return Error(
                message: snapshot.error != null ? snapshot.error : '',
              );
            }
          } else if (snapshot.hasData) {
            //print(snapshot.data);
            return LayoutBuilder(builder: (context, constraints) {
              if (constraints.maxWidth < ScreenSize.iphone) {
                return Container(
                  padding: EdgeInsets.only(
                      left: Pallet.defaultPadding,
                      top: Pallet.topPadding1,
                      right: Pallet.defaultPadding),
                  child: SingleChildScrollView(
                    child: docs(
                        wdgtWidth: widget.wdgtWidth,
                        wdgtHeight: widget.wdgtWidth,
                        popheading: Pallet.heading4),
                  ),
                );
              } else if (constraints.maxWidth > ScreenSize.iphone &&
                  constraints.maxWidth < ScreenSize.ipad) {
                return Padding(
                  padding: EdgeInsets.only(
                      left: Pallet.leftPadding, top: Pallet.topPadding1),
                  child: Container(
                    child: docs(
                        wdgtWidth: widget.wdgtWidth,
                        wdgtHeight: widget.wdgtWidth,
                        popheading: Pallet.heading3),
                  ),
                );
              } else {
                return Padding(
                  padding: EdgeInsets.only(
                      left: Pallet.leftPadding, top: Pallet.topPadding1),
                  child: Container(
                    child: docs(
                        wdgtWidth: widget.wdgtWidth * .8,
                        wdgtHeight: widget.wdgtWidth,
                        popheading: Pallet.heading2),
                  ),
                );
              }
            });
          }
          return Loader();
        });
  }

  Widget docs({double wdgtWidth, wdgtHeight, popheading}) {
    return SingleChildScrollView(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: EdgeInsets.only(bottom: Pallet.defaultPadding),
            child: MyText(text:pageDetails["text1"],
                style: TextStyle(
                  color: Pallet.fontcolornew,
                  fontSize: Pallet.heading1,
                )),
          ),
          Container(
            width: wdgtWidth,
            child: Container(
              // padding: EdgeInsets.all(Pallet.defaultPadding),
              width: 700,
              height: 750,
              decoration: BoxDecoration(
                // boxShadow: [Pallet.shadowEffect],
                borderRadius: BorderRadius.circular(Pallet.radius),
                color: Pallet.fontcolor,
              ),
              child: ListView.separated(
                itemCount: documentsData.documents.length,
                separatorBuilder: (BuildContext context, int index) => Divider(
                  height: 10,
                  // thickness: .5,
                  // color: Pallet.dashcontainerback,
                ),
                itemBuilder: (BuildContext context, int index) {
                  return InkWell(
                    onTap: () {
                      print('HHHHHHHHHHHHHH');
                      if (documentsData.documents[index]['id'] == 1) {
                        launch(
                          appSettings['SERVER_URL'] +
                              '/' +
                              documentsData.documents[index]["path"],
                        );
                      } else {
                        print('JJJJJJJJJJJJJJJJJJ');

                        showDialog(
                          context: context,
                          builder: (BuildContext context) {
                            return AlertDialog(
                              elevation: 24.0,
                              scrollable: true,
                              backgroundColor: Pallet.popupcontainerback,
                              shape: RoundedRectangleBorder(
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(15.0))),
                              title: Row(
                                children: [
                                  Image.asset("c-logo.png"),
                                  SizedBox(width: 10),
                                  MyText(text:
                                      documentsData.documents[index]["name"]
                                          .toString(),
                                      style: TextStyle(
                                          fontSize: popheading,
                                          color: Pallet.fontcolor,
                                          fontWeight: Pallet.font500)),
                                ],
                              ),
                              content: Container(
                                width: 400,
                                child: Column(
                                  mainAxisSize: MainAxisSize.min,
                                  children: [
                                    for (var temp in documentsData
                                        .documents[index]["path"])
                                      networkcomensation(
                                          popheading: popheading,
                                          heading: temp["name"],
                                          path: temp["path"]),
                                  ],
                                ),
                              ),
                              actions: [
                                PopupButton(
                                  text: dashboard.pageDetails["text78"],
                                  onpress: () {
                                    Navigator.of(context).pop();
                                  },
                                ),
                                SizedBox(height: 10),
                              ],
                            );
                          },
                        );
                      }
                    },
                    child: Container(
                      decoration: BoxDecoration(
                          color: Pallet.docbg,
                          borderRadius: BorderRadius.circular(Pallet.radius)),
                      child: Padding(
                        padding: EdgeInsets.all(Pallet.leftPadding / 2.5),
                        child: MyText(text:
                          documentsData.documents[index]["name"],
                          style: TextStyle(
                              color: Pallet.fontcolornew,
                              fontSize: Pallet.heading2),
                        ),
                      ),
                    ),
                  );
                },
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget networkcomensation({double popheading, String heading, String path}) {
    return InkWell(
        onTap: () {
          launch(
            appSettings['SERVER_URL'] + '/' + path,
          );
        },
        child: Container(
          width: double.infinity,
          margin: EdgeInsets.only(bottom: 10),
          decoration: BoxDecoration(
              color: Pallet.docbg,
              borderRadius: BorderRadius.circular(Pallet.radius)),
          child: Padding(
            padding: EdgeInsets.all(Pallet.defaultPadding),
            child: MyText(text:
              heading,
              style: TextStyle(
                  color: Pallet.fontcolornew, fontSize: Pallet.heading3),
            ),
          ),
        ));
  }
}
