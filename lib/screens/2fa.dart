// ignore: unused_import
import 'package:centurion/config/app_settings.dart';
// import 'package:centurion/data.dart';
// import 'package:centurion/screens/api.dart';
import 'package:centurion/utils/utils.dart';
import 'package:qr_flutter/qr_flutter.dart';
import 'package:flutter/services.dart';
import 'package:flutter/material.dart';
import '../common.dart';
import '../config.dart';
import '../home.dart';
import '../services/business/user.dart';

import 'package:url_launcher/url_launcher.dart';

class AccountPwd2 extends StatefulWidget {
  AccountPwd2({
    Key key,
    this.wdgtWidth,
    this.wdgtHeight,
  }) : super(key: key);
  final double wdgtWidth, wdgtHeight;

  @override
  _AccountPwd2State createState() => _AccountPwd2State();
}

class _AccountPwd2State extends State<AccountPwd2> {
  var errorTxt = MyText(text:"fbwe");
  bool secureText1 = true;
  bool secureText2 = true;
  bool secureText3 = true;
  // bool _validate = false;
  FocusNode myFocusNode = new FocusNode();
  String passwordError;

  // var _oldpwd;
  // var _newpwd;
  // var _cnfmpwd;
  TextEditingController oldpwdCon = TextEditingController();
  TextEditingController newpwdCon = TextEditingController();
  TextEditingController cnfmpwdCon = TextEditingController();
  // final GlobalKey<FormState> _formkey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(builder: (context, constraints) {
      if (constraints.maxWidth <= ScreenSize.iphone)
        return Padding(
          padding: EdgeInsets.all(Pallet.defaultPadding),
          child: accpass(wdgtwidth: widget.wdgtWidth, nwidth: widget.wdgtWidth),
        );
      else if (constraints.maxWidth > ScreenSize.iphone &&
          constraints.maxWidth <= ScreenSize.ipad) {
        return Padding(
          padding: EdgeInsets.all(Pallet.defaultPadding),
          child: accpass(wdgtwidth: widget.wdgtWidth, nwidth: widget.wdgtWidth),
        );
      } else
        return Padding(
          padding: EdgeInsets.all(Pallet.leftPadding),
          child: accpass(
              wdgtwidth: widget.wdgtWidth * .3, nwidth: widget.wdgtWidth * .4),
        );
    });
  }

  final oldpass = FocusNode();
  final newpass = FocusNode();
  final cnfrmpass = FocusNode();
  Widget accpass({double wdgtwidth, nwidth}) {
    return Container(
        width: wdgtwidth,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            MyText(text:'Account Password',
                style: TextStyle(
                    color: Pallet.fontcolornew,
                    fontSize: Pallet.heading1,
                    fontWeight: Pallet.heading1wgt)),
            SizedBox(height: 30),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                SignupTextBox(
                    isPrefixIcon: true,
                    isSuffixIcon: true,
                    isPassword: true,
                    tabpress: (event) {
                      if (event.isKeyPressed(LogicalKeyboardKey.tab)) {
                        FocusScope.of(context).requestFocus(oldpass);
                      }
                    },
                    icon: Icons.lock,
                    focusnode: oldpass,
                    onsubmit: () {
                      FocusScope.of(context).requestFocus(oldpass);
                    },
                    label: 'Old Password',
                    errorText: passwordError,
                    controller: oldpwdCon,
                    validation: (value) {
                      if (value.isEmpty) {
                        setState(() {
                          passwordError = 'Invalid Format';
                        });
                        // return "Required";
                      } else {
                        setState(() {
                          passwordError = null;
                        });
                        // return null;
                      }
                    }),
                SizedBox(height: 30),
                SignupTextBox(
                    isPassword: true,
                    tabpress: (event) {
                      if (event.isKeyPressed(LogicalKeyboardKey.tab)) {
                        FocusScope.of(context).requestFocus(newpass);
                      }
                    },
                    icon: Icons.lock,
                    focusnode: newpass,
                    onsubmit: () {
                      FocusScope.of(context).requestFocus(newpass);
                    },
                    label: 'New Password',
                    errorText: passwordError,
                    isPrefixIcon: true,
                    isSuffixIcon: true,
                    controller: newpwdCon,
                    validation: (value) {
                      if (value.isEmpty) {
                        setState(() {
                          passwordError = 'Invalid Format';
                        });
                        // return "Required";
                      } else {
                        setState(() {
                          passwordError = null;
                        });
                        // return null;
                      }
                    }),
                SizedBox(height: 30),
                SignupTextBox(
                    isPassword: true,
                    tabpress: (event) {
                      if (event.isKeyPressed(LogicalKeyboardKey.tab)) {
                        FocusScope.of(context).requestFocus(cnfrmpass);
                      }
                    },
                    icon: Icons.lock,
                    focusnode: cnfrmpass,
                    onsubmit: () {
                      FocusScope.of(context).requestFocus(cnfrmpass);
                    },
                    label: 'Confirm Password',
                    errorText: passwordError,
                    controller: oldpwdCon,
                    isPrefixIcon: true,
                    isSuffixIcon: true,
                    validation: (value) {
                      if (value.isEmpty) {
                        setState(() {
                          passwordError = 'Invalid Format';
                        });
                        // return "Required";
                      } else {
                        setState(() {
                          passwordError = null;
                        });
                        // return null;
                      }
                    }),
              ],
            ),
          ],
        ));
    // Container(
    //     // color: Pallet.dashcontainerback,
    //     child: ClipRRect(
    //   borderRadius: BorderRadius.circular(5),
    //   child: SingleChildScrollView(
    //     child: Container(
    //       // color: Pallet.dashcontainerback,
    //       width: wdgtwidth,
    //       height: 600,
    //       child: Center(
    //         child: Form(
    //           key: _formkey,
    //           child: Padding(
    //             padding: const EdgeInsets.all(25),
    //             child: Container(
    //                 child: Column(
    //               children: [
    //                 Align(
    //                   alignment: Alignment.topLeft,
    //                   child: MyText(text:
    //                     'Account Password',
    //                     style: TextStyle(
    //                       fontSize: 25,
    //                       color: Pallet.dashcontainerback,
    //                     ),
    //                   ),
    //                 ),
    //                 SizedBox(height: 15),
    //                 Align(
    //                   alignment: Alignment.centerLeft,
    //                   child: Container(
    //                     child: MyText(text:
    //                       'Old Password',
    //                       style: TextStyle(
    //                         fontSize: 15,
    //                         color: Pallet.dashcontainerback,
    //                       ),
    //                     ),
    //                   ),
    //                 ),
    //                 SizedBox(
    //                   height: 5,
    //                 ),
    //                 Align(
    //                   alignment: Alignment.centerLeft,
    //                   child: Container(
    //                     child: GestureDetector(
    //                       onTap: () {
    //                         FocusScopeNode currentFocus =
    //                             FocusScope.of(context);
    //                         if (!currentFocus.hasPrimaryFocus) {
    //                           currentFocus.unfocus();
    //                         }
    //                       },
    //                       child: Container(
    //                         width: nwidth,
    //                         height: 35,
    //                         child: TextFormField(
    //                           validator: (String value) {
    //                             if (value.isEmpty) {
    //                               return 'Please a enter your Old Password';
    //                             }
    //                           },
    //                           style: TextStyle(color: Pallet.dashcontainerback),
    //                           controller: oldpwdCon,
    //                           decoration: InputDecoration(
    //                             contentPadding:
    //                                 EdgeInsets.fromLTRB(10.0, 10.0, 20.0, 10.0),
    //                             hintText: 'Enter old password',
    //                             suffixIcon: IconButton(
    //                               color: Pallet.dashcontainerback,
    //                               icon: Icon(secureText1
    //                                   ? Icons.visibility
    //                                   : Icons.visibility_off),
    //                               onPressed: () {
    //                                 setState(() {
    //                                   secureText1 = !secureText1;
    //                                 });
    //                               },
    //                             ),
    //                             focusedBorder: OutlineInputBorder(
    //                               borderSide: BorderSide(
    //                                   color: Pallet.dashcontainerback,
    //                                   width: 2.0),
    //                             ),
    //                             hintStyle: TextStyle(
    //                               color: Pallet.fontcolor,
    //                             ),
    //                             enabledBorder: OutlineInputBorder(
    //                               borderSide: BorderSide(
    //                                   color: Pallet.dashcontainerback,
    //                                   width: 1.5),
    //                             ),
    //                             border: OutlineInputBorder(
    //                                 borderRadius: BorderRadius.circular(5)),
    //                           ),
    //                           obscureText: secureText1,
    //                         ),
    //                       ),
    //                     ),
    //                     width: nwidth,
    //                     height: 70,
    //                   ),
    //                 ),
    //                 Align(
    //                   alignment: Alignment.centerLeft,
    //                   child: Container(
    //                     child: MyText(text:
    //                       'New Password',
    //                       style: TextStyle(
    //                         fontSize: 15,
    //                         color: Pallet.dashcontainerback,
    //                       ),
    //                     ),
    //                   ),
    //                 ),
    //                 SizedBox(
    //                   height: 5,
    //                 ),
    //                 Align(
    //                   alignment: Alignment.centerLeft,
    //                   child: Container(
    //                     child: GestureDetector(
    //                       onTap: () {
    //                         FocusScopeNode currentFocus =
    //                             FocusScope.of(context);
    //                         if (!currentFocus.hasPrimaryFocus) {
    //                           currentFocus.unfocus();
    //                         }
    //                       },
    //                       child: Container(
    //                         width: nwidth,
    //                         height: 35,
    //                         child: TextFormField(
    //                           validator: (String value) {
    //                             if (value.isEmpty) {
    //                               return 'Please a enter your New Password';
    //                             } else if (value.length < 8) {
    //                               return 'password must contain minimum of 8 Characters';
    //                             }
    //                             if (!regex.hasMatch(value))
    //                               return 'Enter a valid password (read the note below)';
    //                             else
    //                               return null;
    //                           },
    //                           style: TextStyle(color: Pallet.dashcontainerback),
    //                           controller: newpwdCon,
    //                           decoration: InputDecoration(
    //                             contentPadding:
    //                                 EdgeInsets.fromLTRB(10.0, 10.0, 20.0, 10.0),
    //                             hintText: 'Enter new password',
    //                             suffixIcon: IconButton(
    //                               color: Pallet.dashcontainerback,
    //                               icon: Icon(secureText2
    //                                   ? Icons.visibility
    //                                   : Icons.visibility_off),
    //                               onPressed: () {
    //                                 setState(() {
    //                                   secureText2 = !secureText2;
    //                                 });
    //                               },
    //                             ),
    //                             hintStyle: TextStyle(
    //                               color: Pallet.dashcontainerback,
    //                             ),
    //                             focusedBorder: OutlineInputBorder(
    //                               borderSide: BorderSide(
    //                                   color: Pallet.dashcontainerback,
    //                                   width: 2.0),
    //                             ),
    //                             enabledBorder: OutlineInputBorder(
    //                               borderSide: BorderSide(
    //                                   color: Pallet.dashcontainerback,
    //                                   width: 1.5),
    //                             ),
    //                             border: OutlineInputBorder(
    //                                 borderRadius: BorderRadius.circular(5)),
    //                           ),
    //                           obscureText: secureText2,
    //                         ),
    //                       ),
    //                     ),
    //                     width: nwidth,
    //                     height: 70,
    //                   ),
    //                 ),
    //                 Align(
    //                   alignment: Alignment.centerLeft,
    //                   child: Container(
    //                     child: MyText(text:
    //                       'Confirm Password',
    //                       style: TextStyle(
    //                         fontSize: 15,
    //                         color: Pallet.dashcontainerback,
    //                       ),
    //                     ),
    //                   ),
    //                 ),
    //                 SizedBox(
    //                   height: 5,
    //                 ),
    //                 Align(
    //                   alignment: Alignment.centerLeft,
    //                   child: Container(
    //                     child: GestureDetector(
    //                       onTap: () {
    //                         FocusScopeNode currentFocus =
    //                             FocusScope.of(context);
    //                         if (!currentFocus.hasPrimaryFocus) {
    //                           currentFocus.unfocus();
    //                         }
    //                       },
    //                       child: Container(
    //                         width: nwidth,
    //                         height: 35,
    //                         child: TextFormField(
    //                           validator: (String value) {
    //                             if (value.isEmpty) {
    //                               return 'Please re-enter password';
    //                             }
    //                             //print(newpwdCon.text);
    //                             //print(cnfmpwdCon.text);

    //                             if (newpwdCon.text != cnfmpwdCon.text) {
    //                               return "Passwords does not match";
    //                             }

    //                             return null;
    //                           },
    //                           style: TextStyle(color: Pallet.dashcontainerback),
    //                           controller: cnfmpwdCon,
    //                           decoration: InputDecoration(
    //                             contentPadding:
    //                                 EdgeInsets.fromLTRB(10.0, 10.0, 20.0, 10.0),
    //                             hintText: 'Re-enter new password',
    //                             hintStyle: TextStyle(
    //                               color: Pallet.dashcontainerback,
    //                             ),
    //                             suffixIcon: IconButton(
    //                               color: Pallet.dashcontainerback,
    //                               icon: Icon(secureText3
    //                                   ? Icons.visibility
    //                                   : Icons.visibility_off),
    //                               onPressed: () {
    //                                 setState(() {
    //                                   secureText3 = !secureText3;
    //                                 });
    //                               },
    //                             ),
    //                             focusedBorder: OutlineInputBorder(
    //                               borderSide: BorderSide(
    //                                   color: Pallet.dashcontainerback,
    //                                   width: 2.0),
    //                             ),
    //                             enabledBorder: OutlineInputBorder(
    //                               borderSide: BorderSide(
    //                                   color: Pallet.dashcontainerback,
    //                                   width: 1.5),
    //                             ),
    //                             border: OutlineInputBorder(
    //                                 borderRadius: BorderRadius.circular(5)),
    //                           ),
    //                           obscureText: secureText3,
    //                         ),
    //                       ),
    //                     ),
    //                     width: nwidth,
    //                     height: 70,
    //                   ),
    //                 ),
    //                 Row(
    //                   crossAxisAlignment: CrossAxisAlignment.start,
    //                   children: [
    //                     Container(
    //                       decoration: BoxDecoration(
    //                         borderRadius: BorderRadius.circular(10),
    //                         color: Color.fromARGB(30, 255, 255, 255),
    //                       ),
    //                       child: Container(
    //                           decoration: BoxDecoration(
    //                             boxShadow: [Pallet.shadowEffect],
    //                             color: Pallet.fontcolor,
    //                             // border: Border(left: BorderSide(color: Colors.pink)),
    //                             borderRadius: BorderRadius.only(
    //                                 topLeft: Radius.circular(
    //                                   Pallet.radius,
    //                                 ),
    //                                 bottomRight: Radius.circular(
    //                                   Pallet.radius,
    //                                 )),
    //                           ),
    //                           padding: EdgeInsets.all(10),
    //                           child: MyText(text:
    //                             'Note : Password must contain atleast one Numeral, one Symbol, one Uppercase, one Lowercase and minimum of 8 Characters',
    //                             style: TextStyle(
    //                               color: Pallet.fontcolor,
    //                               fontSize: 13,
    //                             ),
    //                             textAlign: TextAlign.justify,
    //                           )),
    //                       // width: nwidth,
    //                       height: 90,
    //                     ),
    //                   ],
    //                 ),
    //                 SizedBox(height: 25),
    //                 Row(children: [
    //                   InkWell(
    //                     child: Container(
    //                       margin: EdgeInsets.only(right: 10, top: 5),
    //                       width: 90,
    //                       height: 35,
    //                       decoration: BoxDecoration(
    //                           borderRadius: BorderRadius.circular(5),
    //                           border:
    //                               Border.all(color: Pallet.dashcontainerback)),
    //                       child: Container(
    //                           padding: EdgeInsets.only(
    //                               left: 25, top: 5, bottom: 5, right: 15),
    //                           child: MyText(text:
    //                             'Clear',
    //                             style: TextStyle(
    //                               color: Pallet.dashcontainerback,
    //                               fontSize: 15,
    //                             ),
    //                           )),
    //                     ),
    //                     onTap: () {
    //                       _oldpwd = oldpwdCon.text;
    //                       oldpwdCon.text = "";
    //                       setState(() {
    //                         _oldpwd = null;
    //                       });

    //                       _newpwd = newpwdCon.text;
    //                       newpwdCon.text = "";
    //                       setState(() {
    //                         _newpwd = null;
    //                       });

    //                       _cnfmpwd = cnfmpwdCon.text;
    //                       cnfmpwdCon.text = "";
    //                       setState(() {
    //                         _cnfmpwd = null;
    //                       });
    //                     },
    //                   ),
    //                   SizedBox(width: 10),
    //                   InkWell(
    //                       child: Container(
    //                         margin: EdgeInsets.only(right: 10, top: 5),
    //                         width: 90,
    //                         height: 35,
    //                         decoration: BoxDecoration(
    //                           borderRadius: BorderRadius.circular(5),
    //                           color: Pallet.dashcontainerback,
    //                         ),
    //                         child: Container(
    //                             padding: EdgeInsets.only(
    //                                 left: 25, top: 7, bottom: 5, right: 15),
    //                             child: MyText(text:
    //                               'Save ',
    //                               style: TextStyle(
    //                                 color: Pallet.fontcolor,
    //                                 fontSize: 15,
    //                               ),
    //                             )),
    //                       ),
    //                       onTap: () {
    //                         if (_formkey.currentState.validate()) {
    //                           //print("successful");
    //                           return;
    //                         } else {
    //                           //print("UnSuccessfull");
    //                         }
    //                       }),
    //                 ])
    //               ],
    //             )),
    //           ),
    //         ),
    //       ),
    //     ),
    //   ),
    // ));
  }
}

Pattern pattern =
    r'^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[!@#\$&*~]).{8,}$';
RegExp regex = new RegExp(pattern);

class TwoFactorAuth extends StatefulWidget {
  TwoFactorAuth({Key key, this.wdgtWidth, this.wdgtHeight, this.code})
      : super(key: key);
  final String code;
  final double wdgtWidth, wdgtHeight;

  @override
  _TwoFactorAuthState createState() => _TwoFactorAuthState();
}

class _TwoFactorAuthState extends State<TwoFactorAuth> {
  Future<String> temp;
  String username;
  Map<String, dynamic> factordetails = {};

  initState() {
    setPageDetails();
    super.initState();
  }

  Map<String, dynamic> pageDetails = {};

  void setPageDetails() async {
    temp = Utils.getPageDetails('2fa');
    username = Utils.fromJSONString(
        await Utils.getSharedPrefereces('data'))['user_name'];
    var data = Utils.fromJSONString(await user.setup2FA(username));
    setState(() {
      factordetails = data;
    });
    //print(factordetails);
    //print('@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@');
  }

  // ignore: non_constant_identifier_names
  final GlobalKey<FormState> _1formKey = GlobalKey<FormState>();

  bool secureText1 = true;
  bool secureText2 = true;
  bool secureText3 = true;

  TextEditingController twofaCon = TextEditingController();
  // TextEditingController oldpwdCon = TextEditingController();
  // TextEditingController newpwdCon = TextEditingController();
  // TextEditingController cnfmpwdCon = TextEditingController();
  showAlertDialog(BuildContext context, msg) {
    // set up the button
    // ignore: deprecated_member_use
    Widget okButton = PopupButton(
      onpress: () {
        Navigator.push(context,
            MaterialPageRoute(builder: (context) => Home(route: 'Dashboard')));
      },
      text: pageDetails['text1'],
      buttoncolor: Pallet.fontcolor,
      textcolor: Pallet.fontcolornew,
    );
    // ignore: deprecated_member_use
    Widget cancl = PopupButton(
      onpress: () {
        Navigator.of(context).pop();
      },
      text: pageDetails['text2'],
      buttoncolor: Pallet.fontcolor,
      textcolor: Pallet.fontcolornew,
    );
    AlertDialog alert = AlertDialog(
      title: MyText(text:pageDetails["text3"]),
      content: MyText(text:msg),
      actions: [okButton, cancl],
    );
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
        future: temp,
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            //print(snapshot.data);
            pageDetails = Utils.fromJSONString(snapshot.data);
            return ListView(children: [
              LayoutBuilder(builder: (context, constraints) {
                if (constraints.maxWidth <= 600)
                  return Column(children: [
                    twofactor(
                      wdgtwidth: widget.wdgtWidth,
                    ),
                    installG(
                      wdgtwidth: widget.wdgtWidth,
                    ),
                    opengoogle(
                      wdgtwidth: widget.wdgtWidth,
                    ),
                    input(
                      wdgtwidth: widget.wdgtWidth,
                    ),
                  ]);
                else if (constraints.maxWidth > 600 &&
                    constraints.maxWidth <= 900)
                  return Padding(
                    padding: EdgeInsets.all(Pallet.defaultPadding),
                    child: Container(
                      child: Column(children: [
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Row(
                            children: [
                              twofactor(
                                wdgtwidth: widget.wdgtWidth * .4,
                              ),
                            ],
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceAround,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              installG(
                                wdgtwidth: widget.wdgtWidth * .25,
                              ),
                              opengoogle(
                                wdgtwidth: widget.wdgtWidth * .5,
                              ),
                            ],
                          ),
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children: [
                            input(
                              wdgtwidth: widget.wdgtWidth * .55,
                            ),
                            Container(
                              width: widget.wdgtWidth * .25,
                            )
                          ],
                        ),
                      ]),
                    ),
                  );
                else
                  return Padding(
                    padding: EdgeInsets.all(Pallet.leftPadding),
                    child: Container(
                      width: widget.wdgtWidth,
                      height: widget.wdgtHeight,
                      color: Pallet.fontcolor,
                      child: Wrap(
                        // crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Row(
                            children: [
                              twofactor(
                                wdgtwidth: widget.wdgtWidth * .4,
                              ),
                            ],
                          ),
                          installG(
                            wdgtwidth: widget.wdgtWidth * .25,
                          ),
                          opengoogle(
                            wdgtwidth: widget.wdgtWidth * .3,
                          ),
                          input(
                            wdgtwidth: widget.wdgtWidth * .35,
                          )
                        ],
                      ),
                    ),
                  );
              })
            ]);
          } else {
            return Container();
          }
        });
  }

  Widget twofactor({double wdgtwidth, double fontsize}) {
    return Padding(
        padding: EdgeInsets.all(10),
        child: Container(
          width: wdgtwidth,
          child:
              Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
            MyText(text:
              pageDetails["text4"],
              style: TextStyle(
                fontSize: 20,
                color: Pallet.dashcontainerback,
              ),
            ),
            SizedBox(height: 15),
            MyText(text:
              pageDetails["text6"],
              style: TextStyle(
                fontSize: 15,
                color: Pallet.dashcontainerback,
              ),
            ),
            SizedBox(height: 25),
          ]),
        ));
  }

  Widget installG({double wdgtwidth, double fontsize}) {
    return Padding(
      padding: const EdgeInsets.all(04),
      child: Container(
        width: wdgtwidth,
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              child: MyText(text:
                '\u2776',
                style: TextStyle(
                  fontSize: 30,
                  color: Pallet.dashcontainerback,
                ),
              ),
            ),
            SizedBox(width: 6),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  child: Container(
                    width: wdgtwidth * 0.65,
                    child: MyText(text:
                      pageDetails["text7"],
                      style: TextStyle(
                        fontSize: 15,
                        color: Pallet.dashcontainerback,
                      ),
                    ),
                  ),
                ),
                SizedBox(height: 20),
                Padding(
                  padding: EdgeInsets.all(10),
                  child: Container(
                    child: Column(
                      children: [
                        InkWell(
                          onTap: () {
                            openApp();
                          },
                          child: Container(
                            child: Image.asset('appstore.png',
                                width: wdgtwidth * 0.55),
                            // child: Image.network(
                            //   'https://i.ibb.co/PGsWG3S/Appstore.png',
                            //   width: wdgtwidth * 0.55,
                            // ),
                          ),
                        ),
                        SizedBox(height: 20),
                        InkWell(
                          onTap: () {
                            openPlay();
                          },
                          child: Container(
                            child: Image.asset(
                              'playstore.png',
                              width: wdgtwidth * 0.55,
                            ),
                            // child: Image.network(
                            //   'https://i.ibb.co/P55M2fr/Google-Play.png',
                            //   width: wdgtwidth * 0.55,
                            // ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                SizedBox(height: 10),
              ],
            )
          ],
        ),
      ),
    );
  }

  openPlay() {
    String url = appSettings['OPEN_PLAY'];
    launch(url);
  }

  openApp() {
    String url = appSettings['OPEN_APP'];
    launch(url);
  }

  Widget opengoogle({double wdgtwidth, double fontsize}) {
    return Padding(
      padding: const EdgeInsets.all(04),
      child: Container(
        width: wdgtwidth,
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              child: MyText(text:
                '\u2777',
                style: TextStyle(
                  fontSize: 30,
                  color: Pallet.dashcontainerback,
                ),
              ),
            ),
            SizedBox(width: 6),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  child: MyText(text:
                    pageDetails["text8"],
                    style: TextStyle(
                      fontSize: 15,
                      color: Pallet.dashcontainerback,
                    ),
                  ),
                ),
                SizedBox(height: 20),
                MyText(text:
                  pageDetails["text9"],
                  style: TextStyle(
                    fontSize: 15,
                    color: Pallet.dashcontainerback,
                  ),
                ),
                SizedBox(height: 7),
                Container(
                    padding: EdgeInsets.all(5),
                    width: 180,
                    height: 60,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(Pallet.radius),
                        border: Border.all(color: Pallet.dashcontainerback)),
                    child: Wrap(
                      children: [
                        MyText(text:
                          factordetails["key_secret"] == null
                              ? ""
                              : factordetails["key_secret"],
                          style: TextStyle(
                            fontSize: 15,
                            color: Pallet.dashcontainerback,
                          ),
                        ),
                      ],
                    )),
                SizedBox(height: 7),
                MyText(text:
                  pageDetails["text10"],
                  style: TextStyle(
                    fontSize: 15,
                    color: Pallet.dashcontainerback,
                  ),
                ),
                SizedBox(height: 20),
                Container(
                  width: 100,
                  height: 100,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(3),
                    color: Pallet.fontcolor,
                  ),
                  child: QrImage(
                    data: factordetails["barcode_url"] == null
                        ? ""
                        : factordetails["barcode_url"],
                    version: QrVersions.auto,
                    size: 100.0,
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }

  twofasuccess(BuildContext context) {
    // set up the button
    // ignore: deprecated_member_use
    Widget okButton = PopupButton(
      onpress: () {
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => Home(
                      route: 'dashboard',
                    )));
      },
      text: pageDetails['text2'],
      buttoncolor: Pallet.fontcolor,
      textcolor: Pallet.fontcolornew,
    );

    // set up the AlertDialog
    AlertDialog alert = AlertDialog(
      backgroundColor: Pallet.popupcontainerback,
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(Pallet.radius)),
      title: Row(
        children: [
          Image.asset(
            'assets/c-logo.png',
            width: 40,
          ),
          SizedBox(width: 10),
          Align(
            alignment: Alignment.topLeft,
            child: MyText(text:
              pageDetails['text28'],
              style: TextStyle(
                fontSize: 15,
                color: Pallet.fontcolor,
              ),
            ),
          ),
        ],
      ),
      content: MyText(text:
        pageDetails['text29'],
        style: TextStyle(
          fontSize: 15,
          color: Pallet.fontcolor,
        ),
      ),
      actions: [
        okButton,
      ],
    );

    // show the dialog
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }

  Widget input({double wdgtwidth, double fontsize}) {
    return Container(
      width: wdgtwidth,
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            child: MyText(text:
              '\u2778',
              style: TextStyle(
                fontSize: 30,
                color: Pallet.dashcontainerback,
              ),
            ),
          ),
          SizedBox(width: 6),
          Container(
            // width: size.width * 0.65,
            child: Column(
              // mainAxisAlignment: MainAxisAlignment.spaceAround,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  child: MyText(text:
                    pageDetails["text11"],
                    style: TextStyle(
                      fontSize: 15,
                      color: Pallet.dashcontainerback,
                    ),
                  ),
                ),
                SizedBox(height: 20),
                Container(
                  width: wdgtwidth * .6,
                  height: wdgtwidth * .25,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10),
                      color: Pallet.fontcolor),
                  child: Container(
                      padding: EdgeInsets.all(5),
                      child: Center(
                        child: MyText(text:
                          pageDetails["text12"],
                          style: TextStyle(
                            color: Pallet.dashcontainerback,
                            fontSize: 14,
                          ),
                        ),
                      )),
                ),
                SizedBox(height: 10),
                Container(
                  child: MyText(text:
                    pageDetails["text13"],
                    style: TextStyle(
                      fontSize: 12,
                      color: Pallet.dashcontainerback,
                    ),
                  ),
                ),
                SizedBox(height: 10),
                Form(
                  // ignore: deprecated_member_use
                  // autovalidate: true,
                  key: _1formKey,
                  child: Container(
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(Pallet.radius)),
                    width: 230.0,
                    child: TextFormField(
                      style: TextStyle(color: Pallet.dashcontainerback),
                      inputFormatters: <TextInputFormatter>[
                        FilteringTextInputFormatter.digitsOnly,
                        new LengthLimitingTextInputFormatter(8),
                      ],
                      validator: (value) {
                        if (value.isEmpty) {
                          return pageDetails["text14"];
                        } else
                          return null;
                      },
                      // onSaved: (value) {
                      //   var _name = int.parse(value);
                      // },
                      controller: twofaCon,
                      keyboardType: TextInputType.number,
                      decoration: InputDecoration(
                        errorStyle: TextStyle(color: Pallet.errortxt),
                        errorBorder: OutlineInputBorder(
                            borderSide: BorderSide(color: Pallet.errorborder)),
                        enabledBorder: OutlineInputBorder(
                          borderSide: BorderSide(
                              color: Pallet.dashcontainerback, width: 1.5),
                        ),
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(5)),
                      ),
                    ),
                  ),
                ),
                SizedBox(height: 10),
                Container(
                  child: MyText(text:
                    pageDetails["text15"],
                    style: TextStyle(
                      fontSize: 12.0,
                      color: Pallet.dashcontainerback,
                    ),
                  ),
                ),
                SizedBox(height: 10),
                InkWell(
                    child: Container(
                        decoration: BoxDecoration(
                            color: Pallet.dashcontainerback,
                            borderRadius: BorderRadius.circular(Pallet.radius)),
                        width: wdgtwidth * .7,
                        height: wdgtwidth * .07,
                        child: Center(
                          child: MyText(text:
                            pageDetails["text16"],
                            style: TextStyle(
                              color: Pallet.fontcolor,
                              fontSize: 15,
                            ),
                          ),
                        )),
                    onTap: () async {
                      try {
                        //print('jkhasdjkfhlsakjdjkh');
                        if (_1formKey.currentState.validate()) {
                          //print(
                          // '^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^');
String a='0756';
                          print(a.toint());

                          print(twofaCon.text);
                          print(double.parse(twofaCon.text));

                          var awaip = await user.check2FA(
                              username, twofaCon.text);
                              print("hhhhhhhhhhhhhhhh");
                          print(twofaCon.text);
                          print(int.parse(twofaCon.text));

                          //print(awaip);
                          //print(
                          // 'LLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLL');
                          //print(awaip.toString() == 'Valid OTP'.toString());
                          if (awaip.toString() == 'Valid OTP'.toString()) {
                            //print(
                            // 'ppppppppppppppppppppppppppppppppppppppppppppppppppppppppppppppppppppppppppppppppp');
                            twofasuccess(context);
                          } else {
                            //print(
                            // 'oooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo');
                            snack.snack(title: pageDetails['text30']);

                            // final snackBar =
                            //     SnackBar(content: MyText(text:pageDetails["text30"]));

                            // ScaffoldMessenger.of(context)
                            //     .showSnackBar(snackBar);
                          }
                        }
                      } catch (e) {
                        //print(e);
                      }

                      // _twofa = twofaCon.text;
                      // SampleClass().check2fa(
                      //     int.parse(_twofa),
                      //     (value) => {
                      //           //print(
                      //               '$value, hdfhasdkfjhaskdljfhaksjdhfkjahs'),
                      //           showAlertDialog(context, value),
                      //         });

                      // //print('2-FA verification code: $_twofa');
                      // twofaCon.text = "";
                    }),
                SizedBox(height: 20),
              ],
            ),
          )
        ],
      ),
    );
  }
}
