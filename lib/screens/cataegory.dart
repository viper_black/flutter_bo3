// ignore: avoid_web_libraries_in_flutter
import 'package:universal_html/html.dart' hide HttpRequest;
import 'package:centurion/config/app_settings.dart';
import 'package:centurion/services/business/cart.dart';
import 'package:flutter/material.dart';
import '../common.dart';
import '../utils/utils.dart';
import '../config.dart';
import '../services/communication/index.dart' show HttpRequest;
import 'detail_page.dart';
import 'myshop.dart';

Map<String, dynamic> map = {
  "product_id": 1,
  "category_id": 1,
  "seller_id": 0,
  "purchase_id": 0,
  "warehouse_id": 0,
  "enable_filter_protocol": false
};
List categories = [];
List productListOfCategory = [];
int productId;
String shopRoute = 'stores';
int storeId;

class Categories extends StatefulWidget {
  const Categories(
      {Key key, this.wdgtWidth, this.wdgtHeight, this.setData, this.goToShop})
      : super(key: key);
  final double wdgtWidth, wdgtHeight;
  final setData;
  final String goToShop;

  @override
  _CategoriesState createState() => _CategoriesState();
}

class _CategoriesState extends State<Categories> {
  Future<String> temp;
  bool isMobile = false;
  Map<String, dynamic> getRegionsMap = {
    'system_product_id': 1,
    'region_level': 2,
    'parent_region': 'null'
  };

  shopbanner(context) async {
    // SharedPreferences prefs = await SharedPreferences.getInstance();
    // bool isbannerenabled = prefs.getBool('shopbannershow');
    // print("sssssssssssssssslllllllllll");
    // print(prefs.getBool('shopbannershow'));
    // if (isbannerenabled == null) {
    //   print("lllllllllll");
    //   // await shopDetailAlert(context);
    //   // prefs.setBool('shopbannershow', false);
    //   print(prefs.getBool('shopbannershow'));
    // } else if (isbannerenabled == true) {
    //   print("bbbbbbbbbbbbbbbbblllllllllll");
    //   // await shopDetailAlert(context);
    //   // prefs.setBool('shopbannershow', false);
    // } else if (isbannerenabled == false) {
    //   print("iiiiiiiiisssssssssssss");
    //   if (cart.address.length == 0 || cart.address.length == null) {
    //     await showDialog(
    //         barrierDismissible: false,
    //         context: context,
    //         builder: (BuildContext context) {
    //           return AddressProcessPopUp(
    //             isAdd: 'true',
    //             // index: 0,
    //             // setData: setState,
    //           );
    //         });
    //   }

    //   print("done");
    // }
    await cart.getAddressInfo(setState);
    // if (cart.address.length == 0 || cart.address.length == null) {
    //   await showDialog(
    //       barrierDismissible: false,
    //       context: context,
    //       builder: (BuildContext context) {
    //         return AddressProcessPopUp(
    //           notifyParent: refresh,
    //           isAdd: 'true',
    //           // index: 0,
    //           // setData: setState,
    //         );
    //       });
    // }
  }

  // shopbanner(context) async {
  //   SharedPreferences prefs = await SharedPreferences.getInstance();
  //   bool isbannerenabled = prefs.getBool('shopbannershow');
  //   print("sssssssssssssssslllllllllll");
  //   print(prefs.getBool('shopbannershow'));
  //   if (isbannerenabled == null) {
  //     if (cart.address.length == 0 || cart.address.length == null) {
  //       await showDialog(
  //           barrierDismissible: false,
  //           context: context,
  //           builder: (BuildContext context) {
  //             return AddressProcessPopUp(
  //               isAdd: 'true',
  //               // index: 0,
  //               // setData: setState,
  //             );
  //           });
  //     }
  //     print(prefs.getBool('shopbannershow'));
  //   } else if (isbannerenabled == true) {
  //     if (cart.address.length == 0 || cart.address.length == null) {
  //       await showDialog(
  //           barrierDismissible: false,
  //           context: context,
  //           builder: (BuildContext context) {
  //             return AddressProcessPopUp(
  //               isAdd: 'true',
  //               // index: 0,
  //               // setData: setState,
  //             );
  //           });
  //     }
  //   } else if (isbannerenabled == false) {
  //     if (cart.address.length == 0 || cart.address.length == null) {
  //       await showDialog(
  //           barrierDismissible: false,
  //           context: context,
  //           builder: (BuildContext context) {
  //             return AddressProcessPopUp(
  //               isAdd: 'true',
  //               // index: 0,
  //               // setData: setState,
  //             );
  //           });
  //     }

  //     print("done");
  //   }
  // }
  refresh() {
    setState(() {});
  }

  Map<String, dynamic> pageDetails = {};

  void setPageDetails() async {
    String data = await Utils.getPageDetails('categories');
    setState(() {
      pageDetails = Utils.fromJSONString(data);
    });
  }

  void initState() {
    setPageDetails();
    setState(() {
      // shopRoute = widget.route;
      if (widget.goToShop != 'true') {
        shopRoute = 'stores';
        WidgetsBinding.instance.addPostFrameCallback((_) async {
          shopbanner(context);
        });
      }
    });
    temp = getProducts();
    // cart.getAddressInfo(setState);

    print('000000000000000000000000000000000000000000000000000000000');
    cart.getRegions(getRegionsMap);

    print(categories);
    print('f');
    super.initState();

    // WidgetsBinding.instance.addPostFrameCallback((_) async {
    //   if (cart.address.length == 0 || cart.address.length == null) {
    //     await showDialog(
    //         barrierDismissible: false,
    //         context: context,
    //         builder: (BuildContext context) {
    //           return AddressProcessPopUp(
    //             isAdd: 'true',
    //             // index: 0,
    //             setData: setState,
    //           );
    //         });
    //   }
    // });
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
        future: temp,
        builder: (context, snapshot) {
          if (snapshot.hasError) {
            return SomethingWentWrongMessage();
          } else if (snapshot.hasData) {
            return screens();
          }
          return Loader();
        });
  }

  Widget screens() {
    if (shopRoute == 'stores') {
      return Container(
          padding: EdgeInsets.all(10),
          width: widget.wdgtWidth,
          child: LayoutBuilder(builder: (context, constraints) {
            if (constraints.maxWidth <= ScreenSize.iphone) {
              isMobile = true;
              return Padding(
                padding: EdgeInsets.all(Pallet.defaultPadding),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    buildTitleText(),
                    Padding(
                      padding: EdgeInsets.only(top: Pallet.leftPadding),
                      child: buildCategoryContainer(),
                    )
                  ],
                ),
              );
            } else if (constraints.maxWidth > ScreenSize.verysmall &&
                constraints.maxWidth <= ScreenSize.ipad) {
              return Padding(
                padding: EdgeInsets.all(Pallet.defaultPadding),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    buildTitleText(),
                    Padding(
                      padding: EdgeInsets.only(top: Pallet.leftPadding),
                      child: buildCategoryContainer(),
                    )
                  ],
                ),
              );
            } else
            // if (constraints.maxWidth > ScreenSize.ipad)
            {
              return Padding(
                padding: EdgeInsets.all(Pallet.leftPadding),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    buildTitleText(),
                    Padding(
                      padding: EdgeInsets.only(top: Pallet.leftPadding),
                      child: buildCategoryContainer(),
                    )
                  ],
                ),
              );
            }
          }));
    } else if (shopRoute == 'products') {
      return Products(
        wdgtWidth: widget.wdgtWidth,
        wdgtHeight: widget.wdgtHeight,
        storeId: storeId,
        setData: setState,
        shopSet: setState,
      );
    } else {
      return NewProducts(
        wdgtWidth: widget.wdgtWidth,
        wdgtHeight: widget.wdgtHeight,
        productId: productId,
        setData: widget.setData,
        setShop: setState,
      );
    }
  }

  Widget buildTitleText() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        MyText(text:pageDetails['text1'],
            style: TextStyle(
              color: Pallet.fontcolornew,
              fontSize: Pallet.heading1,
            )),
        SizedBox(
          height: 20,
        ),
        MyText(text:pageDetails['text2'],
            style: TextStyle(
              color: Pallet.fontcolornew,
              fontSize: Pallet.heading2,
            )),
      ],
    );
  }

  Widget buildCategoryContainer() {
    return Wrap(
      spacing:
          isMobile == true ? Pallet.defaultPadding : Pallet.defaultPadding * 2,
      runSpacing: Pallet.defaultPadding,
      children: [
        for (var i = 0; i < categories.length; i++)
          InkWell(
            onTap: () {
              // productsOfCategory.getCategoryProducts(categories[i]['store_id']);
              setState(() {
                print('storeId Aspire');
                print(storeId);
                cart.categoryName = categories[i]['store_name'];
                storeId = categories[i]['store_id'];
                shopRoute = 'products';
              });
              print(shopRoute);
            },
            child: Stack(
              children: [
                Container(
                  decoration: BoxDecoration(
                    boxShadow: [Pallet.shadowEffect],
                    borderRadius: BorderRadius.circular(10),
                    color: Colors.white,
                  ),
                  height: isMobile == true ? 110 : 250,
                  width: isMobile == true ? 125 : 250,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      Container(
                        child: Image.network(appSettings['SERVER_URL'] +
                            '/' +
                            categories[i]['store_image'].toString()),
                      ),
                      Center(
                        child: MyText(text:categories[i]['store_name'],
                            style: TextStyle(
                              color: Pallet.fontcolornew,
                              fontSize: Pallet.heading2,
                            )),
                      )
                    ],
                  ),
                ),
              ],
            ),
          )
      ],
    );
  }

  Future<String> getProducts() async {
    // Map<String, dynamic> _map;

    Map<String, dynamic> _result =
        await HttpRequest.Post('c_ps_get_categories', Utils.getSiteID());
    if (Utils.isServerError(_result)) {
      print('dddweed');
      return throw (await Utils.getMessage(_result['response']['error']));
    } else {
      print('llwlwlwwleeeelweed3e3d');
      categories = _result['response']['data'];
      print('22222222222222222222222222222222');

      print(categories);

      return "start";
    }
  }
}

ProductsOfCategory productsOfCategory = ProductsOfCategory();

class ProductsOfCategory {}
