import 'package:centurion/common.dart';
// import 'package:centurion/screens/myshop.dart';
import 'package:centurion/utils/utils.dart';
import 'package:flutter/material.dart';
import 'package:centurion/services/business/account.dart';
// import 'package:intl/intl.dart';

import '../config.dart';
// import 'package:centurion/screens/menu.dart';

// ignore: must_be_immutable
class News extends StatefulWidget {
  final double wdgtWidth, wdgtHeight;
  News({Key key, this.wdgtWidth, this.wdgtHeight}) : super(key: key);
  @override
  _NewsState createState() => _NewsState();
}

class _NewsState extends State<News> {
  // DateTime dateParse;
  // String time;
  Future<String> temp;
  Map<String, dynamic> pageDetails = {};

  @override
  void initState() {
    setPageDetails();
    temp = newsData.getNews(setState);
    super.initState();
  }

  void setPageDetails() async {
    String data = await Utils.getPageDetails('news');
    setState(() {
      pageDetails = Utils.fromJSONString(data);
    });
  }

  // ignore: unused_field
  int _selected;

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
        future: temp,
        builder: (context, snapshot) {
          if (snapshot.hasError) {
            if (snapshot.error == 'Something Went Wrong') {
              return SomethingWentWrongMessage();
            } else {
              return Error(
                message: snapshot.error != null ? snapshot.error : '',
              );
            }
          } else if (snapshot.hasData) {
            return LayoutBuilder(builder: (context, constraints) {
              if (constraints.maxWidth < ScreenSize.iphone) {
                return Center(
                  child: Padding(
                    padding: EdgeInsets.only(
                        left: Pallet.defaultPadding,
                        top: Pallet.topPadding1,
                        right: Pallet.defaultPadding),
                    child: mynewsphone(
                      wdgtWidth: widget.wdgtWidth,
                      wdgtHeight: widget.wdgtWidth,
                      popheading: Pallet.heading6,
                    ),
                  ),
                );
              } else if (constraints.maxWidth > ScreenSize.iphone &&
                  constraints.maxWidth < ScreenSize.ipad) {
                return Padding(
                  padding: EdgeInsets.only(
                      left: Pallet.leftPadding, top: Pallet.topPadding1),
                  child: mynews(
                    wdgtWidth: widget.wdgtWidth,
                    wdgtHeight: widget.wdgtWidth,
                    popheading: Pallet.heading3,
                  ),
                );
              } else {
                return Padding(
                  padding: EdgeInsets.only(
                      left: Pallet.leftPadding, top: Pallet.topPadding1),
                  child: mynews(
                    wdgtWidth: widget.wdgtWidth,
                    wdgtHeight: widget.wdgtWidth,
                    popheading: Pallet.heading3,
                  ),
                );
              }
            });
          } else
            return Loader();
        });
  }

  Widget mynews({double wdgtWidth, wdgtHeight, popheading}) {
    return Container(
      width: widget.wdgtWidth * 0.8,
      child: ListView(children: [
        MyText(text:pageDetails['text1'],
            style: TextStyle(
              color: Pallet.fontcolornew,
              fontSize: Pallet.heading1,
            )),
        SizedBox(height: 10.0),
        for (var i = 0; i < newsData.news.length; i++)
          InkWell(
            child: Padding(
              padding: EdgeInsets.only(bottom: Pallet.defaultPadding),
              child: Container(
                padding: EdgeInsets.all(Pallet.defaultPadding + 5),
                width: wdgtWidth,
                // height: 50.0,
                // padding: EdgeInsets.symmetric(horizontal: 15, vertical: 15),
                // margin: EdgeInsets.symmetric(horizontal: 15, vertical: 0.5),
                decoration: BoxDecoration(
                  // boxShadow: [Pallet.shadowEffect],
                  color: Pallet.docbg,
                  borderRadius: BorderRadius.circular(Pallet.radius),
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Container(
                      width: 250,
                      child: MyText(text:
                          newsData.news[i]['created_at'].toString() + '(UTC)',
                          textAlign: TextAlign.left,
                          style: TextStyle(
                            fontSize: Pallet.heading3,
                            color: Pallet.fontcolornew,
                          )),
                    ),
                    Container(
                      width: 250,
                      child: MyText(text:
                        newsData.news[i]['heading'],
                        textAlign: TextAlign.left,
                        style: TextStyle(
                          fontSize: Pallet.heading3,
                          color: Pallet.fontcolornew,
                        ),
                      ),
                    ),
                  ],
                ),
                // child: Center(
                //   child: Wrap(spacing: wdgtWidth * .08, runSpacing: 10,
                //       // crossAxisAlignment: CrossAxisAlignment.start,
                //       // mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                //       children: [
                //         // SizedBox(width: 10),
                //         MyText(text:newsData.news[i]['created_at'].toString(),
                //             style: TextStyle(
                //                 fontSize: Pallet.heading2,
                //                 color: Pallet.dashcontainerback,
                //                 fontWeight: Pallet.font500)),
                //         MyText(text:
                //           newsData.news[i]['heading'],
                //           style: TextStyle(
                //               fontSize: Pallet.heading2,
                //               color: Pallet.dashcontainerback,
                //               fontWeight: Pallet.font500),
                //         ),
                //       ]),
                // )
              ),
            ),
            onTap: () async {
              await showDialog(
                  context: context,
                  builder: (BuildContext context) {
                    return AlertDialog(
                      elevation: 24.0,
                      backgroundColor: Pallet.popupcontainerback,
                      shape: RoundedRectangleBorder(
                          borderRadius:
                              BorderRadius.all(Radius.circular(15.0))),
                      title: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Image.asset("c-logo.png", width: 30),
                          SizedBox(width: 10),
                          MyText(text:newsData.news[i]['heading'],
                              maxLines: 1,
                              style: TextStyle(
                                  fontSize: popheading,
                                  color: Pallet.fontcolor,
                                  fontWeight: Pallet.font500)),
                        ],
                      ),
                      content: Container(
                        width: 450.0,
                        child: SingleChildScrollView(
                          child: ListBody(
                            children: <Widget>[
                              MyText(text:newsData.news[i]['body'],
                                  style: TextStyle(
                                    height: 1.5,
                                    fontSize: popheading / 1.03,
                                    color: Pallet.fontcolor,
                                  ))
                            ],
                          ),
                        ),
                      ),
                      actions: [
                        PopupButton(
                          onpress: () {
                            Navigator.of(context).pop();
                          },
                          text: pageDetails['text2'],
                          buttoncolor: Pallet.fontcolor,
                          textcolor: Pallet.fontcolornew,
                        ),
                        SizedBox(height: 10),
                      ],
                    );
                  });
            },
          ),
      ]),
    );
  }

  Widget mynewsphone({double wdgtWidth, wdgtHeight, popheading}) {
    return Container(
      width: widget.wdgtWidth,
      child: ListView(children: [
        MyText(text:pageDetails['text1'],
            style: TextStyle(
              color: Pallet.fontcolornew,
              fontSize: Pallet.heading1,
            )),
        SizedBox(height: 10.0),
        for (var i = 0; i < newsData.news.length; i++)
          InkWell(
            child: Padding(
              padding: EdgeInsets.only(bottom: Pallet.defaultPadding),
              child: Container(
                padding: EdgeInsets.all(Pallet.defaultPadding + 5),
                width: wdgtWidth,
                // height: 50.0,
                // padding: EdgeInsets.symmetric(horizontal: 15, vertical: 15),
                // margin: EdgeInsets.symmetric(horizontal: 15, vertical: 0.5),
                decoration: BoxDecoration(
                  boxShadow: [Pallet.shadowEffect],
                  color: Pallet.fontcolor,
                  borderRadius: BorderRadius.circular(Pallet.radius),
                ),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Container(
                      width: 250,
                      child: MyText(text:newsData.news[i]['created_at'].toString(),
                          textAlign: TextAlign.left,
                          style: TextStyle(
                              fontSize: Pallet.heading2,
                              color: Pallet.dashcontainerback,
                              fontWeight: Pallet.font500)),
                    ),
                    Container(
                      width: 250,
                      child: MyText(text:
                        newsData.news[i]['heading'],
                        textAlign: TextAlign.left,
                        style: TextStyle(
                            fontSize: Pallet.heading2,
                            color: Pallet.dashcontainerback,
                            fontWeight: Pallet.font500),
                      ),
                    ),
                  ],
                ),
                // child: Center(
                //   child: Wrap(spacing: wdgtWidth * .08, runSpacing: 10,
                //       // crossAxisAlignment: CrossAxisAlignment.start,
                //       // mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                //       children: [
                //         // SizedBox(width: 10),
                //         MyText(text:newsData.news[i]['created_at'].toString(),
                //             style: TextStyle(
                //                 fontSize: Pallet.heading2,
                //                 color: Pallet.dashcontainerback,
                //                 fontWeight: Pallet.font500)),
                //         MyText(text:
                //           newsData.news[i]['heading'],
                //           style: TextStyle(
                //               fontSize: Pallet.heading2,
                //               color: Pallet.dashcontainerback,
                //               fontWeight: Pallet.font500),
                //         ),
                //       ]),
                // )
              ),
            ),
            onTap: () async {
              await showDialog(
                  context: context,
                  builder: (BuildContext context) {
                    return AlertDialog(
                      elevation: 24.0,
                      backgroundColor: Pallet.popupcontainerback,
                      shape: RoundedRectangleBorder(
                          borderRadius:
                              BorderRadius.all(Radius.circular(15.0))),
                      title: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Image.asset("c-logo.png", width: 30),
                          SizedBox(width: 10),
                          MyText(text:newsData.news[i]['heading'],
                              maxLines: 1,
                              style: TextStyle(
                                  fontSize: popheading,
                                  color: Pallet.fontcolor,
                                  fontWeight: Pallet.font500)),
                        ],
                      ),
                      content: Container(
                        width: 450.0,
                        child: SingleChildScrollView(
                          child: ListBody(
                            children: <Widget>[
                              MyText(text:newsData.news[i]['body'],
                                  style: TextStyle(
                                    height: 1.5,
                                    fontSize: popheading,
                                    color: Pallet.fontcolor,
                                  ))
                            ],
                          ),
                        ),
                      ),
                      actions: [
                        PopupButton(
                          onpress: () {
                            Navigator.of(context).pop();
                          },
                          text: pageDetails['text2'],
                          buttoncolor: Pallet.fontcolor,
                          textcolor: Pallet.fontcolornew,
                        ),
                        SizedBox(height: 10),
                      ],
                    );
                  });
            },
          ),
      ]),
    );
  }

  Widget newspopup({double popheading}) {
    return DataTable(
      columns: [
        DataColumn(
            label: MyText(text:dashboard.pageDetails["text57"],
                style: TextStyle(
                  fontSize: popheading / 1.09,
                  color: Pallet.fontcolor,
                ))),
        DataColumn(
            label: MyText(text:dashboard.pageDetails["text58"],
                style: TextStyle(
                  fontSize: popheading / 1.09,
                  color: Pallet.fontcolor,
                ))),
      ],
      rows: [
        DataRow(cells: [
          DataCell(MyText(text:dashboard.pageDetails["text59"],
              style: TextStyle(
                fontSize: popheading / 1.15,
                color: Pallet.fontcolor,
              ))),
          DataCell(MyText(text:dashboard.pageDetails["text60"],
              style: TextStyle(
                fontSize: popheading / 1.25,
                color: Pallet.fontcolor,
              ))),
        ]),
        DataRow(cells: [
          DataCell(MyText(text:dashboard.pageDetails["text61"],
              style: TextStyle(
                fontSize: popheading / 1.25,
                color: Pallet.fontcolor,
              ))),
          DataCell(MyText(text:dashboard.pageDetails["text62"],
              style: TextStyle(
                fontSize: popheading / 1.25,
                color: Pallet.fontcolor,
              ))),
        ]),
        DataRow(cells: [
          DataCell(MyText(text:dashboard.pageDetails["text63"],
              style: TextStyle(
                fontSize: popheading / 1.25,
                color: Pallet.fontcolor,
              ))),
          DataCell(MyText(text:dashboard.pageDetails["text64"],
              style: TextStyle(
                fontSize: popheading / 1.25,
                color: Pallet.fontcolor,
              ))),
        ]),
        DataRow(cells: [
          DataCell(MyText(text:dashboard.pageDetails["text65"],
              style: TextStyle(
                fontSize: popheading / 1.25,
                color: Pallet.fontcolor,
              ))),
          DataCell(MyText(text:dashboard.pageDetails["text66"],
              style: TextStyle(
                fontSize: popheading / 1.25,
                color: Pallet.fontcolor,
              ))),
        ]),
      ],
    );
  }
}
