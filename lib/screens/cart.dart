import 'package:centurion/utils/utils.dart';
import 'package:flutter/material.dart';
import '../config.dart';
import '../common.dart';
import '../home.dart';
// import '../screens/detail_page.dart';
// import '../utils/utils.dart';
import '../services/business/cart.dart';
import '../config/app_settings.dart';
import 'cataegory.dart';
// import '../screens/checkout.dart';
// import 'myshop.dart';
// import '../screens/checkout.dart';

int qtyindex;

class Newcart extends StatefulWidget {
  const Newcart(
      {Key key,
      this.wdgtWidth,
      this.wdgtHeight,
      this.wdgtRadius,
      this.setData,
      this.shopSet})
      : super(key: key);
  final double wdgtWidth, wdgtHeight, wdgtRadius;
  final setData;
  final shopSet;
  createState() => NewcartState();
}

class NewcartState extends State<Newcart> {
  Future<String> _temp;
  int selectRadio = 0;

  final addressform = GlobalKey<FormState>();
  TextEditingController addressname = TextEditingController();
  TextEditingController addressline1 = TextEditingController();
  TextEditingController editaddressname = TextEditingController();
  TextEditingController editaddressline1 = TextEditingController();
  // ignore: non_constant_identifier_names
  String variant_value_final = '';

  @override
  void initState() {
    setPageDetails();
    _temp = cart.myCart(setState, widget.setData);
    super.initState();
  }

  Map<String, dynamic> pageDetails = {};

  void setPageDetails() async {
    String data = await Utils.getPageDetails('cart');
    setState(() {
      pageDetails = Utils.fromJSONString(data);
    });
  }

  @override
  FutureBuilder build(BuildContext context) {
    return FutureBuilder(
        future: _temp,
        builder: (context, snapshot) {
          if (snapshot.hasError) {
            //print(snapshot.error);
            return SomethingWentWrongMessage();
          } else if (snapshot.hasData) {
            return LayoutBuilder(
              builder: (context, constraints) {
                if (constraints.maxWidth <= ScreenSize.verysmall) {
                  print('VVVVVVVV');
                  return addToCart(context, widget.wdgtWidth * 0.17);
                } else if (constraints.maxWidth > ScreenSize.verysmall &&
                    constraints.maxWidth <= ScreenSize.iphone) {
                  print('IIIIIIIII');
                  return addToCart(context, widget.wdgtWidth * 0.30);
                } else {
                  return addToCart(context, widget.wdgtWidth * 0.42);
                }
              },
            );
          }
          return Loader();
        });
  }

  Container addToCart(BuildContext context, width) {
    return Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(widget.wdgtRadius),
          color: Pallet.fontcolor,
        ),
        width: widget.wdgtWidth,
        height: widget.wdgtHeight,
        child: cart.cart.isEmpty == false
            ? Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                    Padding(
                      padding: const EdgeInsets.all(10),
                      child: MyText(text:pageDetails['text1'],
                          style: TextStyle(
                              fontWeight: Pallet.bold,
                              fontSize: Pallet.heading1,
                              color: Pallet.fontcolornew)),
                    ),
                    Expanded(
                      child: Container(
                          width: widget.wdgtWidth,
                          height: widget.wdgtHeight * 0.55 - 30,
                          child: ListView.builder(
                            itemCount: cart.cart.length,
                            itemBuilder: (context, index) {
                              return Dismissible(
                                background: stackBehindDismiss(),
                                key: ObjectKey(cart.cart[index]),
                                child: Padding(
                                  padding:
                                      EdgeInsets.all(Pallet.defaultPadding),
                                  child: Container(
                                    decoration: BoxDecoration(
                                      color: Pallet.inner2,
                                      borderRadius: BorderRadius.circular(10),
                                    ),
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.spaceEvenly,
                                            children: [
                                              Expanded(
                                                child: Row(
                                                  children: [
                                                    Padding(
                                                      padding:
                                                          const EdgeInsets.all(
                                                              8.0),
                                                      child: InkWell(
                                                        onTap: () {
                                                          shopRoute = 'details';
                                                          productId = cart
                                                                  .cart[index]
                                                              ["product_id"];
                                                          storeId =
                                                              cart.cart[index]
                                                                  ["store_id"];
                                                          cart.categoryName =
                                                              cart.cart[index][
                                                                  "store_name"];

                                                          Navigator.push(
                                                            context,
                                                            MaterialPageRoute(
                                                                builder:
                                                                    (context) =>
                                                                        Home(
                                                                          route:
                                                                              'centurion_shop',
                                                                          param:
                                                                              'true',
                                                                        )),
                                                          );
                                                        },
                                                        child: ClipRRect(
                                                          borderRadius:
                                                              BorderRadius
                                                                  .circular(10),
                                                          child: Container(
                                                              width: 80,
                                                              height: 80,
                                                              child: Image.network(appSettings[
                                                                      'SERVER_URL'] +
                                                                  '/' +
                                                                  cart.cart[
                                                                          index]
                                                                      [
                                                                      "image_url"])),
                                                        ),
                                                      ),
                                                    ),
                                                    Column(
                                                        crossAxisAlignment:
                                                            CrossAxisAlignment
                                                                .start,
                                                        mainAxisAlignment:
                                                            MainAxisAlignment
                                                                .spaceEvenly,
                                                        children: [
                                                          Container(
                                                            width: width,
                                                            child: Column(
                                                              crossAxisAlignment:
                                                                  CrossAxisAlignment
                                                                      .start,
                                                              mainAxisAlignment:
                                                                  MainAxisAlignment
                                                                      .start,
                                                              children: [
                                                                InkWell(
                                                                  onTap: () {
                                                                    shopRoute =
                                                                        'details';
                                                                    productId =
                                                                        cart.cart[index]
                                                                            [
                                                                            "product_id"];
                                                                    storeId = cart
                                                                            .cart[index]
                                                                        [
                                                                        "store_id"];
                                                                    cart.categoryName =
                                                                        cart.cart[index]
                                                                            [
                                                                            "store_name"];

                                                                    Navigator
                                                                        .push(
                                                                      context,
                                                                      MaterialPageRoute(
                                                                          builder: (context) =>
                                                                              Home(
                                                                                route: 'centurion_shop',
                                                                                param: 'true',
                                                                              )),
                                                                    );
                                                                  },
                                                                  child: MyText(text:
                                                                    cart.cart[
                                                                            index]
                                                                        [
                                                                        "product_name"],
                                                                    style: TextStyle(
                                                                        color: Pallet
                                                                            .fontcolornew,
                                                                        fontWeight:
                                                                            FontWeight
                                                                                .w400,
                                                                        fontSize:
                                                                            15),
                                                                  ),
                                                                ),
                                                                SizedBox(
                                                                    height: 8),
                                                                MyText(text:
                                                                    "\$ " +
                                                                        cart.cart[index]["new_price"]
                                                                            .toString(),
                                                                    textAlign:
                                                                        TextAlign
                                                                            .start,
                                                                    style: TextStyle(
                                                                        color: Pallet
                                                                            .fontcolornew,
                                                                        fontWeight:
                                                                            FontWeight.bold)),
                                                              ],
                                                            ),
                                                          ),
                                                          // Row(
                                                          //   mainAxisAlignment:
                                                          //       MainAxisAlignment
                                                          //           .spaceBetween,
                                                          //   children: [
                                                          //     MyText(text:
                                                          //       cart.cart[index]
                                                          //           [
                                                          //           "product_name"],
                                                          //       style: TextStyle(
                                                          //           color: Pallet
                                                          //               .fontcolornew,
                                                          //           fontWeight:
                                                          //               FontWeight
                                                          //                   .w400,
                                                          //           fontSize:
                                                          //               15),
                                                          //     ),
                                                          //   ],
                                                          // ),
                                                          // SizedBox(height: 5),
                                                          // Row(children: [
                                                          //   MyText(text:
                                                          //       "\$ " +
                                                          //           cart.cart[
                                                          //                   index]
                                                          //                   [
                                                          //                   "new_price"]
                                                          //               .toString(),
                                                          //       style: TextStyle(
                                                          //           color: Pallet
                                                          //               .fontcolornew,
                                                          //           fontWeight:
                                                          //               FontWeight
                                                          //                   .bold)),
                                                          //   SizedBox(width: 5),

                                                          //   // MyText(text:
                                                          //   //     "\$ " +
                                                          //   //         cart.cart[index][
                                                          //   //                 "new_price"]
                                                          //   //             .toString(),
                                                          //   //     style: TextStyle(
                                                          //   //         color: Colors.red,
                                                          //   //         decoration:
                                                          //   //             TextDecoration
                                                          //   //                 .lineThrough,
                                                          //   //         fontSize: 10)),
                                                          // ]),
                                                          SizedBox(height: 5),
                                                          Column(
                                                            children: [
                                                              for (var value
                                                                  in cart.cart[
                                                                          index]
                                                                      [
                                                                      'variants'])

                                                                // for (var variantvalue
                                                                //     in newProducts
                                                                //         .varient_id_list)
                                                                //   if (cart.cart[index][
                                                                //           'variant_id'] ==
                                                                //       variantvalue[
                                                                //           'variantId'])
                                                                //     variant_value_final =
                                                                //         variantvalue[
                                                                //             'variantValue'],
                                                                Container(
                                                                  padding: EdgeInsets.symmetric(
                                                                      horizontal:
                                                                          Pallet.defaultPadding /
                                                                              2),
                                                                  child: MyText(text:value[
                                                                          'value']
                                                                      .toString()),
                                                                  decoration: BoxDecoration(
                                                                      borderRadius:
                                                                          BorderRadius.circular(Pallet
                                                                              .radius),
                                                                      border: Border.all(
                                                                          color:
                                                                              Pallet.fontcolornew)),
                                                                ),
                                                            ],
                                                          )
                                                        ]),
                                                  ],
                                                ),
                                              ),
                                              Padding(
                                                padding: const EdgeInsets.only(
                                                    right: 20),
                                                child: storeId == 3
                                                    ? Container()
                                                    : Container(
                                                        width: 70,
                                                        height: 25,
                                                        decoration:
                                                            BoxDecoration(
                                                          borderRadius:
                                                              BorderRadius
                                                                  .circular(15),
                                                          color: Pallet
                                                              .dashcontainerback,
                                                        ),
                                                        child: Row(
                                                          mainAxisAlignment:
                                                              MainAxisAlignment
                                                                  .spaceEvenly,
                                                          children: [
                                                            InkWell(
                                                              child: Icon(
                                                                  Icons.remove,
                                                                  size: 10,
                                                                  color: Colors
                                                                      .white),
                                                              onTap: () {
                                                                widget.setData(
                                                                    () {
                                                                  setState(() {
                                                                    cart.sub(
                                                                        index,
                                                                        setState,
                                                                        widget
                                                                            .setData);
                                                                  });
                                                                });
                                                              },
                                                            ),
                                                            MyText(text:
                                                                cart.cart[index]
                                                                        ["qty"]
                                                                    .toString(),
                                                                style: TextStyle(
                                                                    color: Colors
                                                                        .white)),
                                                            InkWell(
                                                              child: Icon(
                                                                  Icons.add,
                                                                  size: 10,
                                                                  color: Colors
                                                                      .white),
                                                              onTap: () {
                                                                //print(
                                                                // '_________________');
                                                                //print(
                                                                // cart.cart[index]
                                                                // ["value"]);
                                                                widget.setData(
                                                                    () {
                                                                  cart.addToCart(
                                                                    cart.cart[
                                                                        index],
                                                                    setState,
                                                                    widget
                                                                        .setData,
                                                                    context,
                                                                  );
                                                                });
                                                              },
                                                            ),
                                                          ],
                                                        )),
                                              ),
                                              InkWell(
                                                  onTap: () {
                                                    widget.setData(() {
                                                      setState(() {
                                                        cart.dismiss(
                                                            index,
                                                            setState,
                                                            widget.setData);
                                                      });
                                                    });
                                                  },
                                                  child: Icon(
                                                    Icons.delete,
                                                    color: Pallet
                                                        .dashcontainerback,
                                                  )),
                                            ]),
                                      ],
                                    ),
                                  ),
                                ),
                                onDismissed: (direction) {
                                  // deleteItem(index);
                                  // widget.setData(() {
                                  //   setState(() {
                                  //     cart.sub(index, setState, widget.setData);
                                  //   });
                                  // });
                                  widget.setData(() {
                                    setState(() {
                                      cart.dismiss(
                                          index, setState, widget.setData);
                                    });
                                  });
                                },
                              );
                            },
                          )),
                    ),
                    Container(
                        padding: EdgeInsets.symmetric(
                            horizontal: widget.wdgtWidth * 0.05),
                        height: widget.wdgtHeight * .25,
                        child: Column(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    MyText(text:pageDetails['text2'],
                                        style: TextStyle(
                                            fontSize: 15,
                                            color: Pallet.fontcolornew,
                                            fontWeight: FontWeight.w400)),
                                    MyText(text:'\$ ' + cart.total.toString(),
                                        style: TextStyle(
                                            fontSize: 20,
                                            color: Pallet.fontcolornew,
                                            fontWeight: FontWeight.w600)),
                                  ]),
                              Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    MyText(text:pageDetails['text3'],
                                        style: TextStyle(
                                            fontSize: 15,
                                            color: Pallet.fontcolornew,
                                            fontWeight: FontWeight.w400)),
                                    MyText(text:
                                        '\$ ' + (cart.shippingCosts).toString(),
                                        style: TextStyle(
                                            fontSize: 20,
                                            color: Pallet.fontcolornew,
                                            fontWeight: FontWeight.w600)),
                                  ]),
                              Divider(
                                height: 6,
                                color: Colors.black,
                              ),
                              Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    MyText(text:pageDetails['text4'],
                                        style: TextStyle(
                                            fontSize: 15,
                                            color: Pallet.fontcolornew,
                                            fontWeight: FontWeight.w400)),
                                    MyText(text:
                                        '\$ ' +
                                            (cart.total + cart.shippingCosts)
                                                .toString(),
                                        style: TextStyle(
                                            fontSize: 20,
                                            color: Pallet.fontcolornew,
                                            fontWeight: FontWeight.w600)),
                                  ]),
                              Padding(
                                padding: EdgeInsets.only(
                                    top: 10,
                                    right: widget.wdgtWidth * 0.05,
                                    left: widget.wdgtWidth * 0.05),
                                child: ClipRRect(
                                  borderRadius: BorderRadius.circular(10),
                                  child: CustomButton(
                                    buttoncolor: Pallet.dashcontainerback,
                                    vpadding: 8,
                                    hpadding: 10,
                                    text: pageDetails['text5'],
                                    textcolor: Pallet.fontcolor,
                                    onpress: () {
                                      setState(() {
                                        if (cart.total == 0 ||
                                            cart.total == null) {
                                          snack.snack(
                                              title: pageDetails['text6']);

                                          // final snackBar = SnackBar(
                                          //     content:
                                          //         MyText(text:pageDetails['text6']));
                                          // ScaffoldMessenger.of(context)
                                          //     .showSnackBar(snackBar);
                                        } else {
                                          Navigator.push(
                                              context,
                                              MaterialPageRoute(
                                                  builder: (context) =>
                                                      Home(route: 'checkout')));
                                        }
                                      });
                                    },
                                  ),
                                ),
                              ),
                              SizedBox(height: 25)
                            ]))
                  ])
            : Padding(
                padding: EdgeInsets.all(Pallet.defaultPadding),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    MyText(text:pageDetails['text1'],
                        style: TextStyle(
                            fontWeight: Pallet.bold,
                            fontSize: Pallet.heading1,
                            color: Pallet.fontcolornew)),
                    Center(
                      child: Column(children: [
                        Image.asset('cartempty.png', width: 300),
                        MyText(text:pageDetails['text6'],
                            style: TextStyle(
                                fontWeight: Pallet.font500,
                                fontSize: Pallet.heading2,
                                color: Pallet.fontcolornew)),
                        InkWell(
                          onTap: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => Home(
                                          route: 'centurion_shop',
                                        )));
                          },
                          child: Padding(
                            padding: EdgeInsets.all(Pallet.defaultPadding),
                            child: Container(
                              decoration: BoxDecoration(
                                  boxShadow: [Pallet.shadowEffect],
                                  border:
                                      Border.all(color: Pallet.fontcolornew),
                                  color: Pallet.fontcolornew,
                                  borderRadius:
                                      BorderRadius.circular(Pallet.radius)),
                              child: Padding(
                                padding: EdgeInsets.all(Pallet.defaultPadding),
                                child: MyText(text:pageDetails['text7'],
                                    style: TextStyle(color: Pallet.fontcolor)),
                              ),
                            ),
                          ),
                        )
                      ]),
                    )
                  ],
                ),
              ));
  }

  void deleteItem(index) {
    setState(() {
      cart.cart.removeAt(index);
    });
  }

  void undoDeletion(index, item) {
    setState(() {
      cart.cart.insert(index, item);
    });
  }

  Container stackBehindDismiss() {
    return Container(
      alignment: Alignment.centerRight,
      padding: EdgeInsets.only(right: 20.0),
      color: Colors.red,
      child: Icon(
        Icons.delete,
        color: Colors.white,
      ),
    );
  }
}
