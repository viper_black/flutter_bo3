import 'package:centurion/utils/utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:shared_preferences/shared_preferences.dart';
// ignore: unused_import
import '../data.dart';
import '../login.dart';
import 'package:carousel_slider/carousel_slider.dart';
import '../config.dart';
import '../services/communication/http/index.dart' show HttpRequest;
import 'package:percent_indicator/percent_indicator.dart';
import '../config/index.dart';
import 'package:syncfusion_flutter_charts/charts.dart';
import '../home.dart';
import 'package:blinking_text/blinking_text.dart';
import 'cataegory.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:flutter/services.dart';
import '../common.dart';
import 'package:centurion/services/business/account.dart';
import 'package:flutter_countdown_timer/countdown_timer_controller.dart';
import '../services/business/cart.dart';
import '../services/business/cart.dart' as cart_log;
// import 'package:hovering/hovering.dart';

class Dashboard extends StatefulWidget {
  const Dashboard({
    Key key,
    this.wdgtWidth,
    this.wdgtHeight,
    this.wdgtRadius,
    this.setData,
    // this.newshow,
    this.parrent,
    this.twofapopup,
  }) : super(key: key);
  final double wdgtWidth, wdgtHeight, wdgtRadius;
  final bool twofapopup;
  // final bool newshow;
  final parrent, setData;

  @override
  DashboardState createState() => DashboardState();
}

class DashboardState extends State<Dashboard> {
  double dsvpBalance;
  double cashBalance;
  String isKycStatus;
  bool rank = false;

  CountdownTimerController controller;
  TextEditingController adddsv = TextEditingController();
  TextEditingController dsvpcontroller = TextEditingController();
  String dsvpError;
  ScrollController dashboardScrollController = ScrollController();
  TextEditingController placement = TextEditingController();
  String placeError;
  bool place = false;
  Future<String> temp;
  ScrollController assetsListViewController = ScrollController();
  void setPageDetails() async {
    String data = await Utils.getPageDetails('dashboard');
    setState(() {
      dashboard.pageDetails = Utils.fromJSONString(data);
    });
  }

  newspopupshow() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    if (prefs.getBool('newshow') == false) {
    } else {
      prefs.setBool("newshow", true);
    }
  }

  news() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();

    twofa == true
        ? Future.delayed(
            Duration.zero,
            () => prefs.getBool('newshow') == true
                ? latestnews(context)
                : print('jk'))
        : print("sss");
  }

  // bool isnewsclosed = false;
  bool twofa;

  // ignore: non_constant_identifier_names
  List<dynamic> myteam_inside_data = [];
  // ignore: non_constant_identifier_names
  List<dynamic> myteam_inside_data1 = [];

  int endTime = DateTime.now().millisecondsSinceEpoch + 1000 * 300000;
  void initState() {
    newspopupshow();
    news();
    super.initState();
    setPageDetails();
    cart_log.cart.setPageDetails1();
    temp = dashboard.dashboard();

    controller = CountdownTimerController(endTime: endTime, onEnd: onEnd);
    twofa = widget.twofapopup;
    print(twofa);
    // Future.delayed(Duration.zero, () => latestnews(context));
    print('TTTTTTTTT');
    if (twofa == false) {
      print('111111111111');
      Future.delayed(Duration.zero, () => twofasetuppopup(context));
    }

    // Future.delayed(Duration(seconds: 5), () {
    //   setState(() {
    //     endTime = DateTime.now().millisecondsSinceEpoch + 1000 * 300000;
    //     controller.endTime = endTime;
    //   });
    // });
    getDsvPData();
  }

  networkPartner(context, setState1) async {
    var accountType = 1;
    bool tc = false;
    bool pp = false;
    await showDialog(
        context: context,
        // barrierDismissible: true,
        // barrierColor: Colors.red,
        builder: (context) => StatefulBuilder(
              builder: (context, setState1) => AlertDialog(
                  backgroundColor: Pallet.popupcontainerback,
                  title: Column(
                    children: [
                      Row(
                        children: [
                          Image.asset("c-logo.png", width: 40),
                          SizedBox(width: 10),
                          MyText(
                              text: dashboard.pageDetails['text141'],
                              style: TextStyle(
                                  fontSize: Pallet.heading4,
                                  color: Pallet.fontcolor,
                                  fontWeight: Pallet.font500)),
                        ],
                      ),
                      SizedBox(
                        height: 15,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          GestureDetector(
                            onTap: () {
                              setState1(() {
                                accountType = 1;
                              });
                            },
                            child: MouseRegion(
                              cursor: SystemMouseCursors.click,
                              child: Container(
                                padding: EdgeInsets.all(20),
                                decoration: BoxDecoration(
                                  border: Border.all(
                                      color: accountType == 1
                                          ? Pallet.fontcolor
                                          : Pallet.fontcolor),
                                  color: accountType == 1
                                      ? Pallet.fontcolor
                                      : Pallet.popupcontainerback,
                                  borderRadius: BorderRadius.circular(10),
                                ),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  // mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Icon(
                                      Icons.person,
                                      size: 25,
                                      color: accountType == 1
                                          ? Pallet.fontcolornew
                                          : Pallet.fontcolor,
                                    ),
                                    SizedBox(height: 10),
                                    MyText(
                                      text: dashboard.pageDetails['text142'],
                                      style: TextStyle(
                                        color: accountType == 1
                                            ? Pallet.fontcolornew
                                            : Pallet.fontcolor,
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ),
                          // SizedBox(
                          //   width: 15,
                          // ),
                          // GestureDetector(
                          //   onTap: () {
                          //     setState1(() {
                          //       accountType = 2;
                          //     });
                          //   },
                          //   child: MouseRegion(
                          //     cursor: SystemMouseCursors.click,
                          //                                 child: Container(
                          //       padding: EdgeInsets.all(20),
                          //       decoration: BoxDecoration(
                          //         border: Border.all(
                          //             color: accountType == 2
                          //                 ? Pallet.fontcolor
                          //                 : Pallet.fontcolor),
                          //         color: accountType == 2
                          //             ? Pallet.fontcolor
                          //             : Pallet.popupcontainerback,
                          //         borderRadius: BorderRadius.circular(10),
                          //       ),
                          //       child: Column(
                          //         crossAxisAlignment: CrossAxisAlignment.center,
                          //         // mainAxisAlignment: MainAxisAlignment.center,
                          //         children: [
                          //           Icon(
                          //             Icons.people_outline_rounded,
                          //             size: 25,
                          //             color: accountType == 2
                          //                 ? Pallet.fontcolornew
                          //                 : Pallet.fontcolor,
                          //           ),
                          //           SizedBox(height: 10),
                          //           MyText(
                          //             "Corporate",
                          //             style: TextStyle(
                          //               color: accountType == 2
                          //                   ? Pallet.fontcolornew
                          //                   : Pallet.fontcolor,
                          //             ),
                          //           ),
                          //         ],
                          //       ),
                          //     ),
                          //   ),
                          // ),
                        ],
                      ),
                      SizedBox(
                        height: 15,
                      ),
                      Container(
                        child: Row(
                          children: [
                            Checkbox(
                              overlayColor: MaterialStateColor.resolveWith(
                                  (states) => Pallet.fontcolornew),
                              fillColor: MaterialStateColor.resolveWith(
                                  (states) => Pallet.fontcolor),
                              hoverColor: Pallet.fontcolor,
                              checkColor: Pallet.fontcolornew,
                              value: tc,
                              onChanged: (bool value) {
                                setState1(() {
                                  tc = value;
                                });
                              },
                            ),
                            SizedBox(
                              width: 5,
                            ),
                            MouseRegion(
                              cursor: SystemMouseCursors.click,
                              child: GestureDetector(
                                onTap: () {
                                  launch(appSettings['SERVER_URL'] +
                                      '/document_ducatus/Terms_and_Conditions.pdf');
                                },
                                child: MyText(
                                  text: dashboard.pageDetails['text143'],
                                  style: TextStyle(
                                      fontSize: 12,
                                      decoration: TextDecoration.underline,
                                      color: Pallet.fontcolor,
                                      fontWeight: Pallet.font500,
                                      letterSpacing: 0.5),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Container(
                        child: Row(
                          children: [
                            Checkbox(
                              overlayColor: MaterialStateColor.resolveWith(
                                  (states) => Pallet.fontcolornew),
                              fillColor: MaterialStateColor.resolveWith(
                                  (states) => Pallet.fontcolor),
                              hoverColor: Pallet.fontcolor,
                              checkColor: Pallet.fontcolornew,
                              value: pp,
                              onChanged: (bool value) {
                                setState1(() {
                                  pp = value;
                                });
                              },
                            ),
                            SizedBox(
                              width: 5,
                            ),
                            MouseRegion(
                              cursor: SystemMouseCursors.click,
                              child: GestureDetector(
                                onTap: () {
                                  launch(appSettings['SERVER_URL'] +
                                      '/document_ducatus/Privacy_Policy.pdf');
                                },
                                child: MyText(
                                  text: dashboard.pageDetails['text144'],
                                  style: TextStyle(
                                      fontSize: 12,
                                      decoration: TextDecoration.underline,
                                      color: Pallet.fontcolor,
                                      fontWeight: Pallet.font500,
                                      letterSpacing: 0.5),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          CustomButton(
                            text: dashboard.pageDetails['text78'],
                            textsize: Pallet.heading5,
                            radius: 7,
                            fontweight: Pallet.font600,
                            onpress: () {
                              Navigator.of(context).pop();
                            },
                          ),
                          AbsorbPointer(
                            absorbing: tc == true && pp == true ? false : true,
                            child: CustomButton(
                              text: dashboard.pageDetails['text82'],
                              radius: 7,
                              textsize: Pallet.heading5,
                              fontweight: Pallet.font600,
                              onpress: () {
                                Navigator.of(context).pop();
                                showDialog(
                                  context: context,
                                  // barrierDismissible: true,
                                  // barrierColor: Colors.red,
                                  builder: (context) => StatefulBuilder(
                                    builder: (context, setState1) =>
                                        AlertDialog(
                                      backgroundColor:
                                          Pallet.popupcontainerback,
                                      title: Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.center,
                                        children: [
                                          Row(
                                            children: [
                                              Image.asset("c-logo.png",
                                                  width: 30),
                                              SizedBox(width: 10),
                                              MyText(
                                                  text: dashboard
                                                      .pageDetails['text145'],
                                                  style: TextStyle(
                                                      fontSize: Pallet.heading4,
                                                      color: Pallet.fontcolor,
                                                      fontWeight:
                                                          Pallet.font500)),
                                            ],
                                          ),
                                          SizedBox(
                                            height: 15,
                                          ),
                                          MyText(
                                            text: dashboard
                                                .pageDetails['text146'],
                                            style: TextStyle(
                                              color: Pallet.fontcolor,
                                              fontSize: Pallet.heading4,
                                            ),
                                          ),
                                          SizedBox(
                                            height: 25,
                                          ),
                                          Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.spaceEvenly,
                                            children: [
                                              CustomButton(
                                                  // width: 10,
                                                  text: dashboard
                                                      .pageDetails['text148'],
                                                  hpadding: 15,
                                                  radius: 7,
                                                  textsize: Pallet.heading5,
                                                  fontweight: Pallet.font600,
                                                  onpress: () {
                                                    Navigator.of(context).pop();
                                                  }),
                                              CustomButton(
                                                  text: dashboard
                                                      .pageDetails['text147'],
                                                  // width: 10,
                                                  hpadding: 15,
                                                  radius: 7,
                                                  textsize: Pallet.heading5,
                                                  fontweight: Pallet.font600,
                                                  onpress: () async {
                                                    Navigator.of(context).pop();

                                                    Map<String, dynamic> _map;
                                                    Map<String, dynamic>
                                                        _result;

                                                    _map = {
                                                      'system_product_id': 1,
                                                      'account_type_id':
                                                          accountType
                                                    };
                                                    _result =
                                                        await HttpRequest.Post(
                                                            'networkpartner',
                                                            Utils
                                                                .constructPayload(
                                                                    _map));

                                                    if (Utils.isServerError(
                                                        _result)) {
                                                      Navigator.of(context)
                                                          .pop();
                                                      snack.snack(
                                                          title: dashboard
                                                                  .pageDetails[
                                                              'text96']);

                                                      throw (await Utils
                                                          .getMessage(_result[
                                                                  'response']
                                                              ['error']));
                                                    } else {
                                                      Navigator.of(context)
                                                          .pushReplacement(
                                                              MaterialPageRoute(
                                                        builder: (BuildContext
                                                                context) =>
                                                            Login(
                                                          accountswitchstatus:
                                                              true,
                                                        ),
                                                      ));
                                                    }

                                                    // var status =
                                                    //     Cart.networkpartner(context, accountType);

                                                    // if (status == true) {
                                                    //   Navigator.of(context).push(MaterialPageRoute(
                                                    //     builder: (BuildContext context) => Login(),
                                                    //   ));
                                                    // } else {}
                                                  }),
                                            ],
                                          )
                                        ],
                                      ),
                                    ),
                                  ),
                                );
                              },
                            ),
                          )
                        ],
                      ),
                    ],
                  )),
            ));
  }

  getDsvPData() async {
    Map<String, dynamic> _map;
    Map<String, dynamic> _result;
    print('JJJJJJJJJJJ');

    _map = {'system_product_id': 1};
    _result = await HttpRequest.Post('getdsv_p', Utils.constructPayload(_map));

    cashBalance =
        double.parse(_result['response']['data']['user_data']['cash_balance']);
    dsvpBalance =
        double.parse(_result['response']['data']['user_data']['dsv_p']);
    isKycStatus =
        (_result['response']['data']['user_data']['kyc_status']).toString();
    return "start";
  }

  void onEnd() {}

  double spaceLeft = 10;
  int _current = 0;

  // ignore: non_constant_identifier_names
  int myteam_current = 0;
  TextEditingController dsvcontroller = TextEditingController();
  String dsvError;
  double width;
  double cal;
  // GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey();
  @override
  Widget build(BuildContext context) {
    width = MediaQuery.of(context).size.width;
    return FutureBuilder(
        future: temp,
        builder: (context, snapshot) {
          if (snapshot.hasError) {
            return SomethingWentWrongMessage();
          } else if (snapshot.hasData) {
            if (dashboard.accountType == 1) {
              //CustomerDashboard
              return WillPopScope(
                  onWillPop: () async => false,
                  child: SingleChildScrollView(
                      controller: dashboardScrollController,
                      child: Container(
                          padding: EdgeInsets.all(5),
                          color: Pallet.inner2,
                          width: widget.wdgtWidth,
                          child: LayoutBuilder(builder: (context, constraints) {
                            if (constraints.maxWidth < ScreenSize.verysmall) {
                              return Wrap(
                                spacing: spaceLeft,
                                runSpacing: spaceLeft,
                                children: [
                                  // pagetitle(
                                  //   wdgtWidth: widget.wdgtWidth - spaceLeft,
                                  //   wdgtHeight: 60 + widget.wdgtWidth * 0.02,
                                  //   popheading: Pallet.heading3,
                                  // ),
                                  greetings(
                                      wdgtWidth: widget.wdgtWidth - spaceLeft,
                                      wdgtHeight: 100 + widget.wdgtWidth * 0.05,
                                      userwidth: 230,
                                      cradius: 20),
                                  // attendwebinar(
                                  //   wdgtWidth: widget.wdgtWidth * 0.50 - spaceLeft,
                                  //   wdgtHeight: 80 + widget.wdgtWidth * 0.05,
                                  //   popheading: Pallet.heading4,
                                  // ),
                                  invitelinkcustomer(
                                    wdgtWidth: widget.wdgtWidth - spaceLeft,
                                    wdgtHeight: 80 + widget.wdgtWidth * 0.05,
                                    myfont: Pallet.heading6,
                                  ),
                                  assets(
                                    customermobile: true,
                                    wdgtWidth: widget.wdgtWidth - spaceLeft,
                                    wdgtHeight: 100 + widget.wdgtWidth * 0.03,
                                    viewbox: .6,
                                    // marginright: 20,
                                  ),
                                  // assets(
                                  //   wdgtWidth: widget.wdgtWidth - spaceLeft,
                                  //   wdgtHeight: 120 + widget.wdgtWidth * 0.05,
                                  //   astWidgt: 220,
                                  //   viewbox: .7,
                                  // ),
                                  ducatusmessage(
                                    wdgtWidth: widget.wdgtWidth - spaceLeft,
                                    wdgtHeight: 220 + widget.wdgtWidth * 0.1,
                                    popheading: Pallet.heading4,
                                  ),
                                  buypackages(
                                    wdgtWidth: widget.wdgtWidth - spaceLeft,
                                    wdgtHeight: 220 + widget.wdgtWidth * 0.1,
                                    setState1: setState,
                                    context: context,
                                  ),
                                  mostusedpackages(
                                    wdgtWidth: widget.wdgtWidth,
                                    wdgtHeight: 240 + widget.wdgtWidth * 0.01,
                                    // myviewbox: 0.9,
                                    // prodWidth: 250,
                                  ),
                                  priceofcoinmobile(
                                    wdgtWidth: widget.wdgtWidth,
                                    wdgtHeight: 310 + widget.wdgtWidth * 0.15,
                                    popheading: Pallet.heading4,
                                  ),
                                  myrefferedcustomersmobile(
                                    wdgtWidth: widget.wdgtWidth,
                                    wdgtHeight: widget.wdgtHeight * 0.40,
                                    popheading: Pallet.heading4,
                                  ),
                                  startgetting(
                                    wdgtWidth: widget.wdgtWidth,
                                    wdgtHeight: widget.wdgtHeight * 0.42,
                                    popheading: Pallet.heading4,
                                  ),

                                  // applycctmobile(
                                  //   wdgtWidth: widget.wdgtWidth,
                                  //   wdgtHeight: widget.wdgtHeight * 0.50,
                                  // ),
                                  // latestmembermobile(
                                  //   wdgtWidth: widget.wdgtWidth,
                                  //   wdgtHeight: widget.wdgtHeight * 0.50,
                                  // ),
                                  // latestpackagemobile(
                                  //   wdgtWidth: widget.wdgtWidth,
                                  //   wdgtHeight: widget.wdgtHeight * 0.50,
                                  // ),
                                  // latestproductsmobile(
                                  //   wdgtWidth: widget.wdgtWidth,
                                  //   wdgtHeight: widget.wdgtHeight * 0.50,
                                  // ),
                                  // latestmember(
                                  //   wdgtWidth: widget.wdgtWidth,
                                  //   wdgtHeight: widget.wdgtHeight * 0.50,
                                  // ),
                                ],
                              );
                            } else if (constraints.maxWidth <
                                ScreenSize.iphone) {
                              return Wrap(
                                spacing: spaceLeft,
                                runSpacing: spaceLeft,
                                children: [
                                  // pagetitle(
                                  //   wdgtWidth: widget.wdgtWidth - spaceLeft,
                                  //   wdgtHeight: 60 + widget.wdgtWidth * 0.02,
                                  //   popheading: Pallet.heading3,
                                  // ),
                                  greetings(
                                      wdgtWidth: widget.wdgtWidth,
                                      wdgtHeight: 80 + widget.wdgtWidth * 0.05,
                                      userwidth: widget.wdgtWidth * 0.70,
                                      cradius: 20),
                                  // attendwebinar(
                                  //   wdgtWidth: widget.wdgtWidth * 0.50 - spaceLeft,
                                  //   wdgtHeight: 80 + widget.wdgtWidth * 0.05,
                                  //   popheading: Pallet.heading3,
                                  // ),
                                  invitelinkcustomer(
                                    wdgtWidth: widget.wdgtWidth,
                                    wdgtHeight: 80 + widget.wdgtWidth * 0.05,
                                    myfont: Pallet.heading4,
                                  ),
                                  assets(
                                    customermobile: true,
                                    wdgtWidth: widget.wdgtWidth - spaceLeft,
                                    wdgtHeight: 100 + widget.wdgtWidth * 0.03,
                                    viewbox: .5,
                                    // marginright: 20,
                                  ),
                                  // assets(
                                  //   wdgtWidth: widget.wdgtWidth - spaceLeft,
                                  //   wdgtHeight: 120 + widget.wdgtWidth * 0.05,
                                  //   astWidgt: 220,
                                  //   viewbox: .5,
                                  // ),

                                  ducatusmessage(
                                    wdgtWidth: widget.wdgtWidth - spaceLeft,
                                    wdgtHeight: 220 + widget.wdgtWidth * 0.1,
                                    popheading: Pallet.heading3,
                                  ),
                                  buypackages(
                                    wdgtWidth: widget.wdgtWidth - spaceLeft,
                                    wdgtHeight: 220 + widget.wdgtWidth * 0.1,
                                    setState1: setState,
                                    context: context,
                                  ),
                                  mostusedpackages(
                                    wdgtWidth: widget.wdgtWidth,
                                    wdgtHeight: 220 + widget.wdgtWidth * 0.01,
                                    // myviewbox: 0.800,
                                    // prodWidth: 300,
                                  ),
                                  priceofcoinmobile(
                                    wdgtWidth: widget.wdgtWidth,
                                    wdgtHeight: 310 + widget.wdgtWidth * 0.15,
                                    popheading: Pallet.heading3,
                                  ),
                                  myrefferedcustomersmobile(
                                    wdgtWidth: widget.wdgtWidth,
                                    wdgtHeight: 310 + widget.wdgtWidth * 0.15,
                                    popheading: Pallet.heading3,
                                  ),
                                  startgetting(
                                    wdgtWidth: widget.wdgtWidth,
                                    wdgtHeight: widget.wdgtHeight * 0.40,
                                    popheading: Pallet.heading3,
                                  ),

                                  // applycctmobile(
                                  //   wdgtWidth: widget.wdgtWidth,
                                  //   wdgtHeight: widget.wdgtHeight * 0.50,
                                  // ),
                                  // latestmembermobile(
                                  //   wdgtWidth: widget.wdgtWidth,
                                  //   wdgtHeight: widget.wdgtHeight * 0.50,
                                  // ),
                                  // latestpackagemobile(
                                  //   wdgtWidth: widget.wdgtWidth,
                                  //   wdgtHeight: widget.wdgtHeight * 0.50,
                                  // ),
                                  // latestproductsmobile(
                                  //   wdgtWidth: widget.wdgtWidth,
                                  //   wdgtHeight: widget.wdgtHeight * 0.50,
                                  // ),
                                  // latestmember(
                                  //   wdgtWidth: widget.wdgtWidth,
                                  //   wdgtHeight: widget.wdgtHeight * 0.50,
                                  // ),
                                ],
                              );
                            } else if (constraints.maxWidth >
                                    ScreenSize.iphone &&
                                constraints.maxWidth < ScreenSize.ipad) {
                              return Wrap(
                                spacing: spaceLeft,
                                runSpacing: spaceLeft,
                                children: [
                                  // pagetitle(
                                  //   wdgtWidth: widget.wdgtWidth - spaceLeft,
                                  //   wdgtHeight: 60 + widget.wdgtWidth * 0.01,
                                  //   popheading: Pallet.heading3,
                                  // ),
                                  greetings(
                                    wdgtWidth:
                                        widget.wdgtWidth * 0.50 - spaceLeft,
                                    wdgtHeight: 80 + widget.wdgtWidth * 0.05,
                                    userwidth: 260,
                                    cradius: 25,
                                  ),
                                  // attendwebinar(
                                  //   wdgtWidth: widget.wdgtWidth * 0.50 - spaceLeft,
                                  //   wdgtHeight: 80 + widget.wdgtWidth * 0.05,
                                  //   popheading: Pallet.heading3,
                                  // ),
                                  invitelinkcustomer(
                                    wdgtWidth:
                                        widget.wdgtWidth * 0.50 - spaceLeft,
                                    wdgtHeight: 80 + widget.wdgtWidth * 0.05,
                                    myfont: Pallet.heading4,
                                  ),
                                  assets(
                                    wdgtWidth: widget.wdgtWidth - spaceLeft,
                                    wdgtHeight: 100 + widget.wdgtWidth * 0.03,
                                    astfont: Pallet.heading3,
                                    customermobile: true,

                                    viewbox: 0.333,
                                    // marginright: 20,
                                  ),
                                  // assets(
                                  //   wdgtWidth: widget.wdgtWidth - spaceLeft,
                                  //   wdgtHeight: 110 + widget.wdgtWidth * 0.05,
                                  //   astWidgt: 220,
                                  //   viewbox: .3,
                                  // ),
                                  ducatusmessage(
                                    wdgtWidth:
                                        widget.wdgtWidth * 0.50 - spaceLeft,
                                    wdgtHeight: 200 + widget.wdgtWidth * 0.08,
                                    popheading: Pallet.heading3,
                                  ),
                                  buypackages(
                                    wdgtWidth:
                                        widget.wdgtWidth * 0.50 - spaceLeft,
                                    wdgtHeight: 200 + widget.wdgtWidth * 0.08,
                                    setState1: setState,
                                    context: context,
                                  ),
                                  mostusedpackages(
                                    wdgtWidth: widget.wdgtWidth,
                                    wdgtHeight: 200 + widget.wdgtWidth * 0.08,
                                    // myviewbox: 0.880,
                                    // prodWidth: 600,
                                  ),
                                  // mystatusphone(
                                  //   wdgtWidth: widget.wdgtWidth,
                                  //   wdgtHeight: 300 + widget.wdgtWidth * 0.08,
                                  //   myteamstatus: myteamstatus,
                                  // ),

                                  priceofcoin(
                                    wdgtWidth: widget.wdgtWidth,
                                    wdgtHeight: 250 + widget.wdgtWidth * 0.15,
                                  ),
                                  myrefferedcustomers(
                                    wdgtWidth: widget.wdgtWidth,
                                    wdgtHeight: 250 + widget.wdgtWidth * 0.15,
                                    popheading: Pallet.heading3,
                                  ),
                                  startgetting(
                                    wdgtWidth: widget.wdgtWidth,
                                    wdgtHeight: 250 + widget.wdgtWidth * 0.05,
                                    popheading: Pallet.heading3,
                                  ),
                                  // applycctmobile(
                                  //   wdgtWidth: widget.wdgtWidth,
                                  //   wdgtHeight: 250 + widget.wdgtWidth * 0.15,
                                  // ),
                                  // latestmember(
                                  //   wdgtWidth: widget.wdgtWidth,
                                  //   wdgtHeight: widget.wdgtHeight * 0.60,
                                  // ),
                                  // latestpackage(
                                  //   wdgtWidth: widget.wdgtWidth,
                                  //   // wdgtHeight: 250 + widget.wdgtWidth * 0.15,
                                  //   wdgtHeight: widget.wdgtHeight * 0.60,
                                  // ),
                                  // latestmembermobile(
                                  //   wdgtWidth: widget.wdgtWidth,
                                  //   wdgtHeight: widget.wdgtHeight * 0.70,
                                  // ),
                                  // latestpackagemobile(
                                  //   wdgtWidth: widget.wdgtWidth,
                                  //   // wdgtHeight: 250 + widget.wdgtWidth * 0.15,
                                  //   wdgtHeight: widget.wdgtHeight * 0.70,
                                  // ),
                                  // latestproducts(
                                  //   wdgtWidth: widget.wdgtWidth,
                                  //   wdgtHeight: widget.wdgtHeight * 0.60,
                                  // ),
                                  // latestmember(
                                  //   wdgtWidth: widget.wdgtWidth,
                                  //   wdgtHeight: 250 + widget.wdgtWidth * 0.15,
                                  // ),
                                ],
                              );
                            } else if (constraints.maxWidth > ScreenSize.ipad &&
                                constraints.maxWidth < ScreenSize.tab) {
                              return Wrap(
                                spacing: spaceLeft,
                                runSpacing: spaceLeft,
                                children: [
                                  // pagetitle(
                                  //   wdgtWidth: widget.wdgtWidth - spaceLeft,
                                  //   wdgtHeight: 60 + widget.wdgtWidth * 0.01,
                                  //   popheading: Pallet.heading3,
                                  // ),
                                  greetings(
                                    wdgtWidth:
                                        widget.wdgtWidth * 0.25 - spaceLeft,
                                    wdgtHeight: 100 + widget.wdgtWidth * 0.03,
                                    userwidth: 140,
                                    cradius: 20,
                                  ),
                                  // attendwebinar(
                                  //   wdgtWidth: widget.wdgtWidth * 0.25 - spaceLeft,
                                  //   wdgtHeight: 100 + widget.wdgtWidth * 0.03,
                                  //   popheading: Pallet.heading3,
                                  // ),
                                  invitelinkcustomer(
                                    wdgtWidth:
                                        widget.wdgtWidth * 0.25 - spaceLeft,
                                    wdgtHeight: 100 + widget.wdgtWidth * 0.03,
                                    myfont: Pallet.heading4,
                                  ),
                                  assets(
                                    wdgtWidth:
                                        widget.wdgtWidth * 0.50 - spaceLeft,
                                    wdgtHeight: 100 + widget.wdgtWidth * 0.03,
                                    astfont: Pallet.heading4,
                                    viewbox: 0.333,
                                    // marginright: 20,
                                  ),
                                  // assets(
                                  //   wdgtWidth: widget.wdgtWidth * 0.76 - spaceLeft,
                                  //   wdgtHeight: 100 + widget.wdgtWidth * 0.03,
                                  //   astWidgt: 200,
                                  //   viewbox: .3,
                                  //   astfont: Pallet.heading4,
                                  // ),
                                  ducatusmessage(
                                    wdgtWidth:
                                        widget.wdgtWidth * 0.30 - spaceLeft,
                                    wdgtHeight: 200 + widget.wdgtWidth * 0.05,
                                    popheading: Pallet.heading3,
                                  ),
                                  buypackages(
                                    wdgtWidth:
                                        widget.wdgtWidth * 0.30 - spaceLeft,
                                    wdgtHeight: 200 + widget.wdgtWidth * 0.05,
                                    setState1: setState,
                                    context: context,
                                  ),
                                  mostusedpackages(
                                    wdgtWidth:
                                        widget.wdgtWidth * 0.40 - spaceLeft,
                                    wdgtHeight: 200 + widget.wdgtWidth * 0.05,
                                    // myviewbox: 0.830,
                                    // prodWidth: 330,
                                  ),
                                  priceofcoin(
                                    wdgtWidth:
                                        widget.wdgtWidth * 0.36 - spaceLeft,
                                    wdgtHeight: 300 + widget.wdgtWidth * 0.05,
                                  ),
                                  myrefferedcustomers(
                                    wdgtWidth:
                                        widget.wdgtWidth * 0.32 - spaceLeft,
                                    wdgtHeight: 300 + widget.wdgtWidth * 0.05,
                                    popheading: Pallet.heading3,
                                  ),
                                  // applycct(
                                  //   // wdgtWidth: widget.wdgtWidth * 0.33 - spaceLeft,
                                  //   // wdgtHeight: 222 + widget.wdgtWidth * 0.06,
                                  //   // mypriceofcoin: mypriceofcoin,
                                  //   wdgtWidth: widget.wdgtWidth * 0.33,
                                  //   wdgtHeight: 230 + widget.wdgtWidth * 0.05,
                                  // ),
                                  startgetting(
                                    wdgtWidth: widget.wdgtWidth * 0.31,
                                    wdgtHeight: 300 + widget.wdgtWidth * 0.05,
                                    popheading: Pallet.heading3,
                                  ),

                                  // latestmember(
                                  //   wdgtWidth: widget.wdgtWidth * 0.33,
                                  //   wdgtHeight: 300 + widget.wdgtWidth * 0.05,
                                  // ),
                                  // latestpackage(
                                  //   wdgtWidth: widget.wdgtWidth * 0.32,
                                  //   wdgtHeight: 300 + widget.wdgtWidth * 0.05,
                                  // ),
                                  // latestproducts(
                                  //   wdgtWidth: widget.wdgtWidth * 0.32,
                                  //   wdgtHeight: 300 + widget.wdgtWidth * 0.05,
                                  // ),
                                ],
                              );
                            } else {
                              return Wrap(
                                spacing: spaceLeft,
                                runSpacing: spaceLeft,
                                children: [
                                  // pagetitle(
                                  //   wdgtWidth: widget.wdgtWidth - spaceLeft,
                                  //   wdgtHeight: 60 + widget.wdgtWidth * 0.01,
                                  //   popheading: Pallet.heading3,
                                  // ),
                                  greetings(
                                    wdgtWidth:
                                        widget.wdgtWidth * 0.25 - spaceLeft,
                                    wdgtHeight: 100 + widget.wdgtWidth * 0.02,
                                    userwidth: 280,
                                    cradius: 30,
                                  ),
                                  // attendwebinar(
                                  //   wdgtWidth: widget.wdgtWidth * 0.18 - spaceLeft,
                                  //   wdgtHeight: 100 + widget.wdgtWidth * 0.02,
                                  //   popheading: Pallet.heading3,
                                  // ),
                                  invitelinkcustomer(
                                    wdgtWidth:
                                        widget.wdgtWidth * 0.25 - spaceLeft,
                                    wdgtHeight: 100 + widget.wdgtWidth * 0.02,
                                    myfont: Pallet.heading5,
                                  ),
                                  assets(
                                    wdgtWidth:
                                        widget.wdgtWidth * 0.50 - spaceLeft,
                                    wdgtHeight: 100 + widget.wdgtWidth * 0.02,
                                    astfont: Pallet.heading3,
                                    viewbox: 0.333,
                                    // marginright: 20,
                                  ),
                                  // assets(
                                  //   wdgtWidth: widget.wdgtWidth * 0.75 - spaceLeft,
                                  //   wdgtHeight: 100 + widget.wdgtWidth * 0.02,
                                  //   astWidgt: 200,
                                  //   viewbox: .2,
                                  //   astfont: Pallet.heading3,
                                  // ),
                                  ducatusmessage(
                                    wdgtWidth:
                                        widget.wdgtWidth * 0.25 - spaceLeft,
                                    wdgtHeight: 200 + widget.wdgtWidth * 0.05,
                                    popheading: Pallet.heading3,
                                  ),
                                  buypackages(
                                    wdgtWidth:
                                        widget.wdgtWidth * 0.25 - spaceLeft,
                                    wdgtHeight: 200 + widget.wdgtWidth * 0.05,
                                    setState1: setState,
                                    context: context,
                                  ),
                                  mostusedpackages(
                                    wdgtWidth:
                                        widget.wdgtWidth * 0.50 - spaceLeft,
                                    wdgtHeight: 200 + widget.wdgtWidth * 0.05,
                                    // myviewbox: 0.650,
                                    // prodWidth: 480,
                                  ),
                                  priceofcoin(
                                    wdgtWidth: widget.wdgtWidth * 0.33,
                                    // wdgtHeight: 230 + widget.wdgtWidth * 0.05,
                                    wdgtHeight: widget.wdgtWidth * 0.20,
                                  ),
                                  myrefferedcustomers(
                                    wdgtWidth: widget.wdgtWidth * 0.33,
                                    // wdgtHeight: 230 + widget.wdgtWidth * 0.05,
                                    wdgtHeight: widget.wdgtWidth * 0.20,
                                  ),

                                  // applycct(
                                  //   // wdgtWidth: widget.wdgtWidth * 0.33 - spaceLeft,
                                  //   // wdgtHeight: 222 + widget.wdgtWidth * 0.06,
                                  //   // mypriceofcoin: mypriceofcoin,
                                  //   wdgtWidth: widget.wdgtWidth * 0.33,
                                  //   wdgtHeight: widget.wdgtWidth * 0.25,
                                  // ),
                                  startgetting(
                                    wdgtWidth: widget.wdgtWidth * 0.320,
                                    // wdgtHeight: 230 + widget.wdgtWidth * 0.05,
                                    wdgtHeight: widget.wdgtWidth * 0.20,
                                    popheading: Pallet.heading3,
                                  ),
                                ],
                              );
                            }
                          }))));
            } else {
              return SingleChildScrollView(
                  controller: dashboardScrollController,
                  child: Container(
                      padding: EdgeInsets.all(5),
                      //  padding: EdgeInsets.symmetric(
                      //     vertical: Pallet.leftPadding,
                      //     horizontal: Pallet.leftPadding),
                      color: Pallet.inner2,
                      width: widget.wdgtWidth,
                      child: LayoutBuilder(builder: (context, constraints) {
                        if (constraints.maxWidth < ScreenSize.verysmall) {
                          return Wrap(
                            spacing: spaceLeft,
                            runSpacing: spaceLeft,
                            children: [
                              // pagetitle(
                              //   wdgtWidth: widget.wdgtWidth - spaceLeft,
                              //   wdgtHeight: 65 + widget.wdgtWidth * 0.01,
                              //   popheading: Pallet.heading3,
                              // ),
                              greetings(
                                  wdgtWidth: widget.wdgtWidth - spaceLeft,
                                  wdgtHeight: 100 + widget.wdgtWidth * 0.05,
                                  userwidth: 200,
                                  cradius: 20),
                              // attendwebinar(
                              //   wdgtWidth: widget.wdgtWidth * 0.50 - spaceLeft,
                              //   wdgtHeight: 80 + widget.wdgtWidth * 0.05,
                              //   popheading: Pallet.heading4,
                              // ),
                              assets(
                                wdgtWidth: widget.wdgtWidth - spaceLeft,
                                wdgtHeight: 120 + widget.wdgtWidth * 0.05,
                                astWidgt: 220,
                                viewbox: .7,
                              ),
                              ducatusmessage(
                                wdgtWidth: widget.wdgtWidth - spaceLeft,
                                wdgtHeight: 220 + widget.wdgtWidth * 0.1,
                                popheading: Pallet.heading4,
                              ),
                              buypackages(
                                wdgtWidth: widget.wdgtWidth - spaceLeft,
                                wdgtHeight: 220 + widget.wdgtWidth * 0.1,
                                setState1: setState,
                                context: context,
                              ),
                              mostusedpackages(
                                wdgtWidth: widget.wdgtWidth,
                                wdgtHeight: 240 + widget.wdgtWidth * 0.01,
                                // myviewbox: 0.9,
                                // prodWidth: 250,
                              ),
// mystatusmobile(
// wdgtWidth: widget.wdgtWidth,
// wdgtHeight: 300 + widget.wdgtWidth * 0.08,
// myteamstatus: myteamstatus,
// ),
// mystatusmobile(
// wdgtWidth: widget.wdgtWidth,
// wdgtHeight: widget.wdgtHeight,
// myteamstatus: myteamstatus,
// ),
                              mystatus(
                                wdgtWidth: widget.wdgtWidth,
                                wdgtHeight: widget.wdgtHeight,
                              ),
                              teaminfo(
                                wdgtWidth: widget.wdgtWidth,
                                wdgtHeight: 300 + widget.wdgtHeight * 0.09,
                                popheading: Pallet.heading5,
                                rankwidth: widget.wdgtWidth * 0.33,
                              ),
                              bonus(
                                wdgtWidth: widget.wdgtWidth,
                                wdgtHeight: 680 + widget.wdgtWidth * 0.20,
                              ),
                              // performancemobile(
                              //   wdgtWidth: widget.wdgtWidth,
                              //   // wdgtHeight: widget.wdgtHeight * 0.56,
                              //   wdgtHeight: 330 + widget.wdgtWidth * 0.11,
                              // ),
                              priceofcoinmobile(
                                wdgtWidth: widget.wdgtWidth,
                                wdgtHeight: 550 + widget.wdgtWidth * 0.21,
                                popheading: Pallet.heading4,
                              ),
                              // applycctmobile(
                              //   wdgtWidth: widget.wdgtWidth,
                              //   wdgtHeight: widget.wdgtHeight * 0.50,
                              // ),
                              latestmember(
                                wdgtWidth: widget.wdgtWidth,
                                wdgtHeight: widget.wdgtHeight * 0.43,
                              ),
                              latestpackage(
                                wdgtWidth: widget.wdgtWidth,
                                wdgtHeight: widget.wdgtHeight * 0.43,
                              ),

                              // latestmembermobile(
                              //   wdgtWidth: widget.wdgtWidth,
                              //   wdgtHeight: widget.wdgtHeight * 0.70,
                              // ),
                              latestproducts(
                                wdgtWidth: widget.wdgtWidth,
                                wdgtHeight: widget.wdgtHeight * 0.43,
                              ),
                              // latestproducts(
                              //   wdgtWidth: widget.wdgtWidth,
                              //   wdgtHeight: widget.wdgtHeight * 0.50,
                              // ),
                            ],
                          );
                        } else if (constraints.maxWidth < ScreenSize.iphone) {
                          return Wrap(
                            spacing: spaceLeft,
                            runSpacing: spaceLeft,
                            children: [
                              // pagetitle(
                              //   wdgtWidth: widget.wdgtWidth - spaceLeft,
                              //   wdgtHeight: 65 + widget.wdgtWidth * 0.01,
                              //   popheading: Pallet.heading3,
                              // ),
                              greetings(
                                  wdgtWidth: widget.wdgtWidth - spaceLeft,
                                  wdgtHeight: 80 + widget.wdgtWidth * 0.05,
                                  userwidth: 300,
                                  cradius: 20),
                              // attendwebinar(
                              //   wdgtWidth: widget.wdgtWidth * 0.50 - spaceLeft,
                              //   wdgtHeight: 80 + widget.wdgtWidth * 0.05,
                              //   popheading: Pallet.heading3,
                              // ),
                              assets(
                                wdgtWidth: widget.wdgtWidth - spaceLeft,
                                wdgtHeight: 120 + widget.wdgtWidth * 0.05,
                                astWidgt: 220,
                                viewbox: .5,
                                astfont: Pallet.heading5,
                              ),
                              ducatusmessage(
                                wdgtWidth: widget.wdgtWidth - spaceLeft,
                                wdgtHeight: 220 + widget.wdgtWidth * 0.1,
                                popheading: Pallet.heading3,
                              ),
                              buypackages(
                                wdgtWidth: widget.wdgtWidth - spaceLeft,
                                wdgtHeight: 220 + widget.wdgtWidth * 0.1,
                                setState1: setState,
                                context: context,
                              ),
                              mostusedpackages(
                                wdgtWidth: widget.wdgtWidth,
                                wdgtHeight: 220 + widget.wdgtWidth * 0.01,
                                // myviewbox: 0.800,
                                // prodWidth: 300,
                              ),
                              mystatus(
                                wdgtWidth: widget.wdgtWidth,
                                wdgtHeight: widget.wdgtHeight,
                              ),
                              teaminfo(
                                wdgtWidth: widget.wdgtWidth,
                                wdgtHeight: 300 + widget.wdgtHeight * 0.09,
                                popheading: Pallet.heading4,
                                rankwidth: widget.wdgtWidth * 0.37,
                              ),
                              bonus(
                                wdgtWidth: widget.wdgtWidth,
                                wdgtHeight: 630 + widget.wdgtWidth * 0.20,
                              ),
                              // performancemobile(
                              //   wdgtWidth: widget.wdgtWidth,
                              //   wdgtHeight: 330 + widget.wdgtWidth * 0.10,
                              // ),
                              priceofcoinmobile(
                                wdgtWidth: widget.wdgtWidth,
                                wdgtHeight: 600 + widget.wdgtWidth * 0.25,
                                popheading: Pallet.heading3,
                              ),
                              // applycctmobile(
                              //   wdgtWidth: widget.wdgtWidth,
                              //   wdgtHeight: widget.wdgtHeight * 0.50,
                              // ),
                              latestmember(
                                wdgtWidth: widget.wdgtWidth,
                                wdgtHeight: widget.wdgtHeight * 0.43,
                              ),
                              latestpackage(
                                wdgtWidth: widget.wdgtWidth,
                                wdgtHeight: widget.wdgtHeight * 0.43,
                              ),
                              latestproducts(
                                wdgtWidth: widget.wdgtWidth,
                                wdgtHeight: widget.wdgtHeight * 0.43,
                              ),
                            ],
                          );
                        } else if (constraints.maxWidth > ScreenSize.iphone &&
                            constraints.maxWidth < ScreenSize.ipad) {
                          return Wrap(
                            spacing: spaceLeft,
                            runSpacing: spaceLeft,
                            children: [
                              // pagetitle(
                              //   wdgtWidth: widget.wdgtWidth - spaceLeft,
                              //   wdgtHeight: 60 + widget.wdgtWidth * 0.01,
                              //   popheading: Pallet.heading3,
                              // ),
                              greetings(
                                wdgtWidth: widget.wdgtWidth - spaceLeft,
                                wdgtHeight: 80 + widget.wdgtWidth * 0.05,
                                userwidth: 600,
                                cradius: 25,
                              ),
                              // attendwebinar(
                              // //   wdgtWidth: widget.wdgtWidth * 0.50 - spaceLeft,
                              // //   wdgtHeight: 80 + widget.wdgtWidth * 0.05,
                              // //   popheading: Pallet.heading3,
                              // // ),
                              assets(
                                wdgtWidth: widget.wdgtWidth - spaceLeft,
                                wdgtHeight: 110 + widget.wdgtWidth * 0.05,
                                astWidgt: 220,
                                viewbox: 0.333,
                                astfont: Pallet.heading4,
                              ),
                              ducatusmessage(
                                wdgtWidth: widget.wdgtWidth * 0.50 - spaceLeft,
                                wdgtHeight: 200 + widget.wdgtWidth * 0.08,
                                popheading: Pallet.heading3,
                              ),
                              buypackages(
                                wdgtWidth: widget.wdgtWidth * 0.50 - spaceLeft,
                                wdgtHeight: 200 + widget.wdgtWidth * 0.08,
                                setState1: setState,
                                context: context,
                              ),
                              mostusedpackages(
                                wdgtWidth: widget.wdgtWidth,
                                wdgtHeight: 300 + widget.wdgtWidth * 0.08,
                                // myviewbox: 0.880,
                                // prodWidth: 600,
                              ),
                              // mystatusmobile(
                              //   wdgtWidth: widget.wdgtWidth,
                              //   wdgtHeight: 300 + widget.wdgtWidth * 0.08,
                              //   myteamstatus: myteamstatus,
                              // ),
                              mystatus(
                                wdgtWidth: widget.wdgtWidth,
                                wdgtHeight: widget.wdgtHeight,
                              ),
                              teaminfo(
                                wdgtWidth: widget.wdgtWidth,
                                wdgtHeight: 225 + widget.wdgtWidth * 0.10,
                                popheading: Pallet.heading3,
                                rankwidth: widget.wdgtWidth * 0.185,
                              ),
                              bonus(
                                wdgtWidth: widget.wdgtWidth,
                                // wdgtHeight: widget.wdgtHeight * 0.65,
                                wdgtHeight: 340 + widget.wdgtWidth * 0.10,
                              ),
                              // performance(
                              //   wdgtWidth: widget.wdgtWidth,
                              //   wdgtHeight: 300 + widget.wdgtWidth * 0.10,
                              // ),
                              priceofcoin(
                                wdgtWidth: widget.wdgtWidth,
                                wdgtHeight: 300 + widget.wdgtWidth * 0.15,
                                popheading: Pallet.heading3,
                              ),
                              // applycct(
                              //   wdgtWidth: widget.wdgtWidth,
                              //   wdgtHeight: 250 + widget.wdgtWidth * 0.15,
                              // ),
                              latestmember(
                                wdgtWidth: widget.wdgtWidth,
                                // wdgtHeight: 250 + widget.wdgtWidth * 0.15,
                                wdgtHeight: widget.wdgtHeight * 0.43,
                              ),
                              latestpackage(
                                wdgtWidth: widget.wdgtWidth,
                                // wdgtHeight: 250 + widget.wdgtWidth * 0.15,
                                wdgtHeight: widget.wdgtHeight * 0.43,
                              ),
                              latestproducts(
                                wdgtWidth: widget.wdgtWidth,
                                // wdgtHeight: 250 + widget.wdgtWidth * 0.15,
                                wdgtHeight: widget.wdgtHeight * 0.43,
                              ),
                            ],
                          );
                        } else if (constraints.maxWidth > ScreenSize.ipad &&
                            constraints.maxWidth < ScreenSize.tab) {
                          return Wrap(
                            spacing: spaceLeft,
                            runSpacing: spaceLeft,
                            children: [
                              // pagetitle(
                              //   wdgtWidth: widget.wdgtWidth - spaceLeft,
                              //   wdgtHeight: 60 + widget.wdgtWidth * 0.01,
                              //   popheading: Pallet.heading3,
                              // ),
                              greetings(
                                wdgtWidth: widget.wdgtWidth * 0.42 - spaceLeft,
                                wdgtHeight: 100 + widget.wdgtWidth * 0.03,
                                userwidth: 300,
                                cradius: 20,
                              ),
                              // attendwebinar(
                              //   wdgtWidth: widget.wdgtWidth * 0.25 - spaceLeft,
                              //   wdgtHeight: 100 + widget.wdgtWidth * 0.03,
                              //   popheading: Pallet.heading3,
                              // ),
                              assets(
                                wdgtWidth: widget.wdgtWidth * 0.58 - spaceLeft,
                                wdgtHeight: 100 + widget.wdgtWidth * 0.03,
                                astWidgt: 200,
                                viewbox: 0.34,
                                astfont: Pallet.heading6,
                              ),
                              ducatusmessage(
                                wdgtWidth: widget.wdgtWidth * 0.30 - spaceLeft,
                                wdgtHeight: 200 + widget.wdgtWidth * 0.05,
                                popheading: Pallet.heading3,
                              ),
                              buypackages(
                                wdgtWidth: widget.wdgtWidth * 0.30 - spaceLeft,
                                wdgtHeight: 200 + widget.wdgtWidth * 0.05,
                                setState1: setState,
                                context: context,
                              ),
                              mostusedpackages(
                                wdgtWidth: widget.wdgtWidth * 0.40 - spaceLeft,
                                wdgtHeight: 200 + widget.wdgtWidth * 0.05,
                                // myviewbox: .5,
                                // prodWidth: 500,
                                // myviewbox: 0.830,
                                // prodWidth: 330,
                              ),
                              mystatus(
                                wdgtWidth: widget.wdgtWidth * 0.50 - spaceLeft,
                                wdgtHeight: 300 + widget.wdgtWidth * 0.05,
                              ),
                              teaminfo(
                                wdgtWidth: widget.wdgtWidth * 0.50 - spaceLeft,
                                wdgtHeight: 300 + widget.wdgtWidth * 0.05,
                                popheading: Pallet.heading3,
                                // rankwidth: 100,
                                rankwidth: widget.wdgtWidth * 0.118,
                              ),
                              bonus(
                                wdgtWidth: widget.wdgtWidth * 0.50 - spaceLeft,
                                // wdgtHeight: 220 + widget.wdgtWidth * 0.07,
                                wdgtHeight: 380 + widget.wdgtWidth * 0.07,
                              ),
                              // performance(
                              //   wdgtWidth: widget.wdgtWidth * 0.33 - spaceLeft,
                              //   wdgtHeight: 380 + widget.wdgtWidth * 0.07,
                              // ),
                              priceofcoin(
                                wdgtWidth: widget.wdgtWidth * 0.50 - spaceLeft,
                                wdgtHeight: 380 + widget.wdgtWidth * 0.07,
                                popheading: Pallet.heading3,
                              ),
                              // applycct(
                              //   // wdgtWidth: widget.wdgtWidth * 0.33 - spaceLeft,
                              //   // wdgtHeight: 222 + widget.wdgtWidth * 0.06,
                              //   // mypriceofcoin: mypriceofcoin,
                              //   wdgtWidth: widget.wdgtWidth * 0.32,
                              //   wdgtHeight: 320 + widget.wdgtWidth * 0.06,
                              // ),
                              // latestmembertotal(
                              //   wdgtWidth: widget.wdgtWidth,
                              //   wdgtHeight: widget.wdgtWidth * 0.40,
                              //   tableheadingwth: 450,
                              //   tableheadinghth: 35,
                              // ),
                              latestmember(
                                wdgtWidth: widget.wdgtWidth * 0.33,
                                // wdgtHeight: widget.wdgtWidth * 0.32,
                                wdgtHeight: 320 + widget.wdgtWidth * 0.06,
                              ),
                              latestpackage(
                                wdgtWidth: widget.wdgtWidth * 0.32,
                                wdgtHeight: 320 + widget.wdgtWidth * 0.06,
                              ),
                              latestproducts(
                                wdgtWidth: widget.wdgtWidth * 0.32,
                                wdgtHeight: 320 + widget.wdgtWidth * 0.06,
                              ),
                            ],
                          );
                        } else {
                          return Wrap(
                            spacing: spaceLeft,
                            runSpacing: spaceLeft,
                            children: [
                              // pagetitle(
                              //   wdgtWidth: widget.wdgtWidth - spaceLeft,
                              //   wdgtHeight: 50 + widget.wdgtWidth * 0.01,
                              //   popheading: Pallet.heading3,
                              // ),
                              greetings(
                                wdgtWidth: widget.wdgtWidth * 0.25 - spaceLeft,
                                wdgtHeight: 100 + widget.wdgtWidth * 0.02,
                                userwidth: 280,
                                cradius: 30,
                              ),
                              // attendwebinar(
                              //   wdgtWidth: widget.wdgtWidth * 0.18 - spaceLeft,
                              //   wdgtHeight: 100 + widget.wdgtWidth * 0.02,
                              //   popheading: Pallet.heading3,
                              // ),
                              assets(
                                wdgtWidth: widget.wdgtWidth * 0.75 - spaceLeft,
                                wdgtHeight: 100 + widget.wdgtWidth * 0.02,
                                astWidgt: 280,
                                viewbox: .2,
                                astfont: Pallet.heading3,
                              ),
                              ducatusmessage(
                                wdgtWidth: widget.wdgtWidth * 0.25 - spaceLeft,
                                wdgtHeight: 200 + widget.wdgtWidth * 0.05,
                                popheading: Pallet.heading3,
                              ),
                              buypackages(
                                wdgtWidth: widget.wdgtWidth * 0.25 - spaceLeft,
                                wdgtHeight: 200 + widget.wdgtWidth * 0.05,
                                setState1: setState,
                                context: context,
                              ),
                              mostusedpackages(
                                wdgtWidth: widget.wdgtWidth * 0.50 - spaceLeft,
                                wdgtHeight: 200 + widget.wdgtWidth * 0.05,
                                // myviewbox: 0.650,
                                // prodWidth: 480,
                              ),
                              mystatus(
                                wdgtWidth: widget.wdgtWidth * 0.50 - spaceLeft,
                                wdgtHeight: 230 + widget.wdgtWidth * 0.05,
                              ),
                              teaminfo(
                                wdgtWidth: widget.wdgtWidth * 0.50 - spaceLeft,
                                wdgtHeight: 230 + widget.wdgtWidth * 0.05,
                                popheading: Pallet.heading3,
                                rankwidth: widget.wdgtWidth * 0.086,
                              ),
                              bonus(
                                wdgtWidth: widget.wdgtWidth * 0.50 - spaceLeft,
                                // wdgtHeight: 220 + widget.wdgtWidth * 0.07,
                                wdgtHeight: 280 + widget.wdgtWidth * 0.09,
                              ),
                              // performance(
                              //   wdgtWidth: widget.wdgtWidth * 0.33,
                              //   wdgtHeight: 265 + widget.wdgtWidth * 0.09,
                              // ),
                              priceofcoin(
                                wdgtWidth: widget.wdgtWidth * 0.50 - spaceLeft,
                                wdgtHeight: 280 + widget.wdgtWidth * 0.09,
                                popheading: Pallet.heading3,
                              ),
                              // applycct(
                              //   // wdgtWidth: widget.wdgtWidth * 0.33 - spaceLeft,
                              //   // wdgtHeight: 222 + widget.wdgtWidth * 0.06,
                              //   // mypriceofcoin: mypriceofcoin,
                              //   wdgtWidth: widget.wdgtWidth * 0.33,
                              //   wdgtHeight: widget.wdgtWidth * 0.25,
                              // ),

                              // latestmembertotal(
                              //   wdgtWidth: widget.wdgtWidth,
                              //   wdgtHeight: widget.wdgtWidth * 0.45,
                              //   tableheadingwth: 450,
                              //   tableheadinghth: 35,
                              // ),
                              latestmember(
                                wdgtWidth: widget.wdgtWidth * 0.33,
                                wdgtHeight: widget.wdgtWidth * 0.248,
                              ),
                              latestpackage(
                                wdgtWidth: widget.wdgtWidth * 0.33,
                                wdgtHeight: widget.wdgtWidth * 0.248,
                              ),
                              latestproducts(
                                wdgtWidth: widget.wdgtWidth * 0.318,
                                wdgtHeight: widget.wdgtWidth * 0.248,
                              ),
                            ],
                          );
                        }
                      })));
            }
          }
          return Loader();
        });
  }

  // Widget pagetitle({double wdgtWidth, wdgtHeight, popheading}) {
  //   return Container(
  //     padding: EdgeInsets.all(10),
  //     decoration: BoxDecoration(
  //         // boxShadow: [Pallet.shadowEffect],
  //         // color: Pallet.fontcolor,
  //         borderRadius: BorderRadius.circular(10)),
  //     width: wdgtWidth,
  //     height: wdgtHeight,
  //     child: Column(
  //         // crossAxisAlignment: CrossAxisAlignment.start,
  //         // mainAxisAlignment: MainAxisAlignment.center,
  //         children: [
  //           Row(
  //             mainAxisAlignment: MainAxisAlignment.spaceBetween,
  //             children: [
  //               MyText('Dashboard',
  //                   style: TextStyle(
  //                       fontWeight: Pallet.font600,
  //                       fontSize: Pallet.heading2,
  //                       color: Pallet.fontcolornew)),
  //               PopupButton(
  //                 onpress: () {
  //                   showDialog(
  //                     context: context,
  //                     builder: (BuildContext context) {
  //                       return AlertDialog(
  //                         elevation: 24.0,
  //                         backgroundColor: Pallet.popupcontainerback,
  //                         shape: RoundedRectangleBorder(
  //                             borderRadius:
  //                                 BorderRadius.all(Radius.circular(15.0))),
  //                         title: Row(
  //                           children: [
  //                             Image.asset("c-logo.png", width: 40),
  //                             SizedBox(width: 10),
  //                             MyText(dashboard.pageDetails["text2"],
  //                                 style: TextStyle(
  //                                     fontSize: popheading,
  //                                     color: Pallet.fontcolor,
  //                                     fontWeight: Pallet.font500)),
  //                           ],
  //                         ),
  //                         content: Container(
  //                           width: 455.0,
  //                           child: SingleChildScrollView(
  //                             child: ListBody(
  //                               children: <Widget>[
  //                                 MyText(
  //                                     'For the latest news and updates about our Ducatus community, join us EVERY SATURDAY at the following times:',
  //                                     textAlign: TextAlign.left,
  //                                     style: TextStyle(
  //                                       fontSize: popheading / 1.03,
  //                                       height: 1.5,
  //                                       color: Pallet.fontcolor,
  //                                     )),
  //                                 Center(
  //                                     child: attendwebinarpopup(
  //                                         popheading: popheading)),
  //                                 MyText(
  //                                     'NOTE: Please note that the timings above are based on Singapore Time (SGT)',
  //                                     textAlign: TextAlign.left,
  //                                     style: TextStyle(
  //                                         fontSize: popheading / 1.03,
  //                                         color: Pallet.fontcolor,
  //                                         fontWeight: Pallet.font500)),
  //                               ],
  //                             ),
  //                           ),
  //                         ),
  //                         actions: [
  //                           Row(
  //                             children: [
  //                               PopupButton(
  //                                 onpress: () {
  //                                   launch(dashboard.zoomlink.toString());
  //                                 },
  //                                 child: Center(
  //                                   child: Row(
  //                                     children: [
  //                                       Icon(
  //                                         Icons.video_call,
  //                                         color: Pallet.fontcolornew,
  //                                         size: 18.0,
  //                                       ),
  //                                       SizedBox(width: 10.0),
  //                                       MyText(
  //                                         dashboard.pageDetails["text2"],
  //                                         style: TextStyle(
  //                                           fontSize: Pallet.heading6,
  //                                           color: Pallet.fontcolornew,
  //                                           fontWeight: Pallet.font500,
  //                                         ),
  //                                       ),
  //                                     ],
  //                                   ),
  //                                 ),
  //                               ),
  //                               SizedBox(width: 9),
  //                               PopupButton(
  //                                 MyText: dashboard.pageDetails["text78"],
  //                                 onpress: () {
  //                                   Navigator.of(context).pop();
  //                                 },
  //                               ),
  //                               // SizedBox(height: 9),
  //                             ],
  //                           ),
  //                         ],
  //                       );
  //                     },
  //                   );
  //                 },
  //                 child: CustomButton(
  //                   buttoncolor: Pallet.fontcolornew,
  //                   vpadding: 8,
  //                   //textcolor: Pallet.fontcolor,
  //                   child: Row(
  //                     children: [
  //                       Icon(
  //                         Icons.video_call,
  //                         color: Pallet.fontcolor,
  //                         size: 18.0,
  //                       ),
  //                       SizedBox(width: 8.0),
  //                       MyText(
  //                         dashboard.pageDetails["text2"],
  //                         style: TextStyle(
  //                           fontSize: Pallet.heading4,
  //                           color: Pallet.fontcolor,
  //                           fontWeight: Pallet.font500,
  //                         ),
  //                       ),
  //                     ],
  //                   ),
  //                 ),
  //               ),
  //             ],
  //           ),
  //         ]),
  //   );
  // }

  Widget greetings({double wdgtWidth, wdgtHeight, userwidth, cradius}) {
    return Container(
      padding: EdgeInsets.only(left: 10),
      decoration: BoxDecoration(
          color: Pallet.dashcontainerback,
          borderRadius: BorderRadius.circular(10)),
      width: wdgtWidth,
      height: wdgtHeight,
      child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Container(
                    // decoration: BoxDecoration(
                    //     border: Border.all(width: 1, color: Pallet.fontcolor)),
                    width: wdgtWidth * 0.650,
                    height: wdgtHeight * 0.8,
                    child: Row(children: [
                      CircleAvatar(
                        backgroundColor: Colors.transparent,
                        child: ClipOval(
                          clipBehavior: Clip.antiAlias,
                          // borderRadius: BorderRadius.circular(70),
                          child: dashboard.userprofile != null
                              ? Image.network(
                                  appSettings['SERVER_URL'] +
                                      '/' +
                                      dashboard.userprofile,
                                  fit: BoxFit.contain,
                                )
                              : Image.asset(
                                  'temp-profile.png',
                                  fit: BoxFit.contain,
                                ),
                        ),
                        radius: cradius,
                      ),
                      SizedBox(width: 10),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          MyText(
                              text: dashboard.time,
                              style: TextStyle(
                                  fontWeight: Pallet.font500,
                                  fontSize: Pallet.heading7,
                                  color: Pallet.fontcolor)),
                          Container(
                            width: wdgtWidth * 0.460,
                            child: MyText(
                              // 'jagan jagadhess rajendran Jagan Jagadhess Rajendran'
                              // .capitalizeFirstofEach,

                              text: dashboard.username == null
                                  ? ""
                                  : dashboard.pageDetails["text1"] +
                                      ' ' +
                                      dashboard.username.capitalizeFirstofEach,
                              overflow: TextOverflow.ellipsis,

                              isselectable: false,
                              style: TextStyle(
                                  fontWeight: Pallet.font500,
                                  fontSize: Pallet.heading2,
                                  color: Pallet.fontcolor),
                            ),
                          ),
                        ],
                      ),
                    ])),
                Container(
                  width: wdgtWidth * 0.3,
                  height: wdgtHeight * 0.8,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    // crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      CircleAvatar(
                        backgroundColor: Colors.transparent,
                        child: ClipOval(
                          // borderRadius: BorderRadius.circular(70),

                          clipBehavior: Clip.antiAlias,
                          child: dashboard
                              .rankLogo(dashboard.myteamstatus['rank']),
                        ),
                        radius: cradius,
                      ),
                      SizedBox(height: wdgtHeight * 0.1),
                      MyText(
                        text: dashboard.rankCurrentName(
                                    dashboard.myteamstatus['rank_name']) ==
                                ''
                            ? ''
                            : dashboard
                                .rankCurrentName(
                                    dashboard.myteamstatus['rank_name'])
                                .toUpperCase(),
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            fontSize: Pallet.heading6, color: Pallet.fontcolor),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ]),
    );
  }

  //  Widget greetings({double wdgtWidth, wdgtHeight, userwidth, cradius}) {
  //   return Container(
  //     padding: EdgeInsets.only(left: 30),
  //     decoration: BoxDecoration(
  //         boxShadow: [Pallet.shadowEffect],
  //         color: Pallet.dashcontainerback,
  //         borderRadius: BorderRadius.circular(10)),
  //     width: wdgtWidth,
  //     height: wdgtHeight,
  //     child: Column(
  //         crossAxisAlignment: CrossAxisAlignment.start,
  //         mainAxisAlignment: MainAxisAlignment.center,
  //         children: [
  //           Row(
  //             mainAxisAlignment: MainAxisAlignment.spaceEvenly,
  //             children: [
  //               CircleAvatar(
  //                 backgroundColor: Colors.transparent,
  //                 child: ClipRRect(
  //                   borderRadius: BorderRadius.circular(70),
  //                   child: dashboard.userprofile != null
  //                       ? Image.network(
  //                           appSettings['SERVER_URL'] +
  //                               '/' +
  //                               dashboard.userprofile,
  //                           fit: BoxFit.contain,
  //                         )
  //                       : Image.asset(
  //                           'temp-profile.png',
  //                           fit: BoxFit.contain,
  //                         ),
  //                 ),
  //                 radius: cradius,
  //               ),
  //               SizedBox(width: 10),
  //               Container(
  //                 child: Column(
  //                   crossAxisAlignment: CrossAxisAlignment.start,
  //                   mainAxisAlignment: MainAxisAlignment.center,
  //                   children: [
  //                     MyText(dashboard.time,
  //                         style: TextStyle(
  //                             fontWeight: Pallet.font500,
  //                             fontSize: Pallet.heading7,
  //                             color: Pallet.fontcolor)),
  //                     Container(
  //                       width: userwidth,
  //                       child: MyText(
  //                         // 'Jagan Jagadhess Rajendran Jagan Jagadhess Rajendran',

  //                         dashboard.username == null
  //                             ? ""
  //                             : dashboard.pageDetails["text1"] +
  //                                 dashboard.username
  //                                     .substring(0, 1)
  //                                     .toUpperCase() +
  //                                 dashboard.username.substring(1).toLowerCase(),
  //                         overflow: TextOverflow.ellipsis,
  //                         style: TextStyle(
  //                             fontWeight: Pallet.font500,
  //                             fontSize: Pallet.heading2,
  //                             color: Pallet.fontcolor),
  //                       ),
  //                     ),
  //                   ],
  //                 ),
  //               ),
  //               CircleAvatar(
  //                 backgroundColor: Colors.transparent,
  //                 child: ClipRRect(
  //                   borderRadius: BorderRadius.circular(40),
  //                   child: Image.asset(
  //                     'gold.png',
  //                     fit: BoxFit.fitWidth,
  //                   ),
  //                 ),
  //                 radius: cradius,
  //               ),
  //             ],
  //           ),
  //         ]),
  //   );
  // }

  Widget invitelinkcustomer({double wdgtWidth, wdgtHeight, myfont}) {
    String link = appSettings["CLIENT_URL"] + '/invite/' + dashboard.username;
    return Container(
      padding: EdgeInsets.only(left: 30),
      decoration: BoxDecoration(
          boxShadow: [Pallet.shadowEffect],
          color: Pallet.dashcontainerback,
          borderRadius: BorderRadius.circular(10)),
      width: wdgtWidth,
      height: wdgtHeight,
      child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            MyText(
              text: dashboard.pageDetails["text83"],
              style: TextStyle(
                  fontWeight: Pallet.font500,
                  fontSize: myfont,
                  color: Pallet.fontcolor),
            ),
            SizedBox(height: 10),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                CustomButton(
                  vpadding: 10,
                  text: dashboard.pageDetails["text84"],
                  fontweight: Pallet.subheading1wgt,
                  textcolor: Pallet.fontcolornew,
                  textsize: Pallet.heading6,
                  onpress: () {
                    Clipboard.setData(ClipboardData(text: "$link"))
                        .then((result) {
                      snack.snack(title: dashboard.pageDetails['text85']);
                      // ScaffoldMessenger.of(context).showSnackBar(
                      //   SnackBar(
                      //     content: MyText(dashboard.pageDetails["text85"]),
                      //   ),
                      // );
                    });
                  },
                ),
              ],
            ),
          ]),
    );
  }

  Widget assets(
      {double wdgtWidth,
      wdgtHeight,
      astfont,
      double viewbox,
      bool customermobile,
      double astWidgt}) {
    return Container(
        decoration: BoxDecoration(borderRadius: BorderRadius.circular(20)),
        width: wdgtWidth,
        height: wdgtHeight,
        child: CarouselSlider(
            // scrollDirection: Axis.horizontal,
            // controller: assetsListViewController,
            options: CarouselOptions(
              autoPlayInterval: Duration(seconds: 4),
              autoPlayCurve: Curves.fastOutSlowIn,
              scrollDirection: Axis.horizontal,
              scrollPhysics: dashboard.accountType == 1
                  ? customermobile == true
                      ? null
                      : NeverScrollableScrollPhysics()
                  : null,
              autoPlayAnimationDuration: Duration(milliseconds: 1000),
              autoPlay: dashboard.accountType == 2 ? true : false,
              // aspectRatio: 2.0,
              // enlargeCenterPage: true,
              viewportFraction: viewbox,
            ),
            items: [
              for (var i = 0; i < dashboard.assets.length; i++)
                InkWell(
                  onTap: () {
                    setState(() {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => Home(
                                    route: 'my_accounts',
                                    intialPage: i,
                                    param: dashboard.assets[i]['asset_type']
                                        .toString(),
                                  )));
                    });
                  },
                  child: Container(
                    margin: EdgeInsets.symmetric(horizontal: 2.0),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(15),
                      color: Pallet.dashcontainerback,
                    ),
                    padding: EdgeInsets.symmetric(horizontal: 20),
                    width: astWidgt,
                    height: wdgtHeight,
                    child: Container(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        // mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          SizedBox(height: wdgtHeight * 0.1),
                          ImageIcon(
                            NetworkImage(appSettings['SERVER_URL'] +
                                '/' +
                                dashboard.assets[i]['asset_icon']),
                            color: Pallet.fontcolor,
                            size: 30,
                          ),
                          SizedBox(height: wdgtHeight * 0.08),
                          MyText(
                              text: dashboard.assets[i]['asset_val'].toString(),
                              style: TextStyle(
                                color: Pallet.fontcolor,
                                fontSize: Pallet.heading2,
                              )),
                          SizedBox(height: wdgtHeight * 0.12),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              if (dashboard.assets[i]['asset_type'] == 'cash')
                                MyText(
                                  text: dashboard.pageDetails["text149"],
                                  style: TextStyle(
                                      color: Pallet.fontcolor,
                                      fontSize: astfont,
                                      fontWeight: Pallet.font500),
                                ),
                              if (dashboard.assets[i]['asset_type'] ==
                                  'trading')
                                MyText(
                                  text: dashboard.pageDetails["text150"],
                                  style: TextStyle(
                                      color: Pallet.fontcolor,
                                      fontSize: astfont,
                                      fontWeight: Pallet.font500),
                                ),
                              if (dashboard.assets[i]['asset_type'] ==
                                  'shopping')
                                MyText(
                                  text: dashboard.pageDetails["text151"],
                                  style: TextStyle(
                                      color: Pallet.fontcolor,
                                      fontSize: astfont,
                                      fontWeight: Pallet.font500),
                                ),
                              if (dashboard.assets[i]['asset_type'] == 'duc')
                                MyText(
                                  text: dashboard.pageDetails["text153"],
                                  style: TextStyle(
                                      color: Pallet.fontcolor,
                                      fontSize: astfont,
                                      fontWeight: Pallet.font500),
                                ),
                              if (dashboard.assets[i]['asset_type'] ==
                                  'charity')
                                MyText(
                                  text: dashboard.pageDetails["text152"],
                                  style: TextStyle(
                                      color: Pallet.fontcolor,
                                      fontSize: astfont,
                                      fontWeight: Pallet.font500),
                                ),
                              if (dashboard.assets[i]['asset_type'] == 'dsv')
                                MyText(
                                  text: dashboard.pageDetails["text154"],
                                  style: TextStyle(
                                      color: Pallet.fontcolor,
                                      fontSize: astfont,
                                      fontWeight: Pallet.font500),
                                ),
                              if (dashboard.assets[i]['asset_type'] == 'dsv_p')
                                MyText(
                                  text: dashboard.pageDetails["text155"],
                                  style: TextStyle(
                                      color: Pallet.fontcolor,
                                      fontSize: astfont,
                                      fontWeight: Pallet.font500),
                                ),

                              // InkWell(
                              //   onTap: () {
                              //     setState(() {
                              //       Navigator.push(
                              //           context,
                              //           MaterialPageRoute(
                              //               builder: (context) => Home(
                              //                     route: 'my_accounts',
                              //                     intialPage: i,
                              //                     param: dashboard.assets[i]
                              //                         ['asset_type'],
                              //                   )));
                              //     });
                              //   },
                              //   child: Container(
                              //     width: 15,
                              //     height: 15,
                              //     decoration: BoxDecoration(
                              //         color: Color(0XFFebebeb),
                              //         borderRadius: BorderRadius.circular(15)),
                              //     child: Icon(
                              //       Icons.arrow_forward_ios,
                              //       size: 11,
                              //       color: Color(0XFF5a7eb4),
                              //     ),
                              //   ),
                              // ),
                            ],
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
            ]));
  }

  Widget ducatusmessage(
      {double wdgtWidth, double wdgtHeight, double popheading}) {
    return Container(
      padding: EdgeInsets.symmetric(
          horizontal: 10 + wdgtWidth * 0.05, vertical: 5 + wdgtWidth * 0.02),
      decoration: BoxDecoration(
          boxShadow: [Pallet.shadowEffect],
          color: Pallet.dashcontainerback,
          borderRadius: BorderRadius.circular(10)),
      width: wdgtWidth,
      height: wdgtHeight,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          CarouselSlider.builder(
              options: CarouselOptions(
                  autoPlay: false,
                  initialPage: 0,
                  // scrollPhysics: dashboard.ducmessages.length == 1
                  //     ? NeverScrollableScrollPhysics()
                  //     : BouncingScrollPhysics(),
                  // enableInfiniteScroll: true,
                  enableInfiniteScroll:
                      dashboard.ducmessages.length == 1 ? false : true,
                  viewportFraction: 1,
                  height: wdgtHeight * 0.6,
                  // enlargeCenterPage: true,
                  // autoPlayInterval: Duration(seconds: 0),
                  // autoPlayCurve: Curves.fastOutSlowIn,
                  scrollDirection: Axis.horizontal,
                  autoPlayAnimationDuration: Duration(milliseconds: 0),
                  onPageChanged: (index, reason) {
                    setState(() {
                      _current = index;
                    });
                  }),
              itemCount: dashboard.ducmessages.length,
              itemBuilder: (BuildContext context, int index, _) => MouseRegion(
                    cursor: dashboard.ducmessages.length > 1
                        ? SystemMouseCursors.click
                        : SystemMouseCursors.basic,
                    child: GestureDetector(
                      child: Container(
                        width: wdgtWidth,
                        height: wdgtHeight,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            MyText(
                              text: dashboard.ducmessages[index]["header"],
                              style: TextStyle(
                                fontWeight: Pallet.font500,
                                fontSize: Pallet.heading3,
                                color: Pallet.fontcolor,
                              ),
                            ),
                            SizedBox(height: wdgtHeight * 0.05),
                            Container(
                              child: MyText(
                                  text: dashboard.ducmessages[index]["body"],
                                  overflow: TextOverflow.ellipsis,
                                  isselectable: false,
                                  maxLines: 4,
                                  style: TextStyle(
                                    height: 1.6,
                                    fontSize: Pallet.heading5,
                                    color: Pallet.fontcolor,
                                  )),
                            ),
                          ],
                        ),
                      ),
                    ),
                  )),
          Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            // mainAxisAlignment: MainAxisAlignment.end,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: dashboard.ducmessages.map((url) {
                  int index = dashboard.ducmessages.indexOf(url);
                  return Container(
                    width: 8,
                    height: 8,
                    margin:
                        EdgeInsets.symmetric(vertical: 10.0, horizontal: 2.0),
                    decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      color: dashboard.ducmessages.length > 1
                          ? _current == index
                              ? Pallet.fontcolor
                              : Color(0XFF698bbf)
                          : Colors.transparent,
                    ),
                  );
                }).toList(),
              ),
              // HoverButton(
              //   // minWidth: 100,
              //   shape: RoundedRectangleBorder(
              //       side: BorderSide(
              //         width: 1,
              //         color: Pallet.fontcolor,
              //       ),
              //       borderRadius: BorderRadius.all(Radius.circular(5))),
              //   hoverShape: RoundedRectangleBorder(
              //       side: BorderSide(width: 0, color: Pallet.fontcolor),
              //       borderRadius: BorderRadius.all(Radius.circular(5))),
              //   // focusElevation: 2,
              //   onpressed: () {},
              //   color: Pallet.fontcolornew,
              //   hoverColor: Pallet.fontcolor,
              //   textColor: Pallet.fontcolor,
              //   hoverTextColor: Pallet.fontcolornew,
              //   child: MyText('Details'),
              // ),
              CustomButton(
                // onhover: (buttoncolor, textcolor) {
                //   setState(() {
                //     buttoncolor = Pallet.fontcolor;
                //     textcolor = Pallet.fontcolornew;
                //   });
                // },
                // onexit: (buttoncolor, textcolor) {
                //   setState(() {
                //     buttoncolor = Colors.transparent;
                //     textcolor = Pallet.fontcolor;
                //   });
                // },
                text: dashboard.pageDetails["text4"],
                onpress: () async {
                  await custompopup.showdialog(
                    context: context,
                    title: dashboard.ducmessages[_current]["header"],
                    content: Center(
                      child: MyText(
                        text: dashboard.ducmessages[_current]["body"],
                        style: TextStyle(
                          fontSize: popheading / 1.03,
                          height: 1.5,
                          color: Pallet.fontcolor,
                        ),
                      ),
                    ),
                    actions: [
                      PopupButton(
                        text: dashboard.pageDetails["text78"],
                        onpress: () {
                          Navigator.of(context).pop();
                        },
                      ),
                      // SizedBox(height: 10),
                    ],
                    scrollable: true,
                  );
                },
              ),
            ],
          ),
        ],
      ),
    );
  }

  Widget buypackages(
      {double wdgtWidth, double wdgtHeight, setState1, BuildContext context}) {
    double dsvp;
    return Container(
        width: wdgtWidth,
        height: wdgtHeight,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                InkWell(
                  onTap: () async {
                    await custompopup.showdialog(
                      title: dashboard.pageDetails["text86"],
                      context: context,
                      barrierDismissible: false,
                      scrollable: true,
                      content: StatefulBuilder(
                        builder: (BuildContext context, setState1) => Column(
                          mainAxisSize: MainAxisSize.min,
                          children: [
                            Container(
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  MyText(
                                    text: dashboard.pageDetails["text87"],
                                    style: TextStyle(
                                      fontSize: 15,
                                      color: Pallet.fontcolor,
                                      fontWeight: Pallet.font600,
                                    ),
                                  ),
                                  SizedBox(height: 10),
                                  Container(
                                    width: 240,
                                    decoration: BoxDecoration(
                                      color: Pallet.fontcolornew,
                                      borderRadius:
                                          BorderRadius.circular(Pallet.radius),
                                      // border: Border.all(
                                      //     color: Pallet
                                      //         .fontcolornew)
                                    ),
                                    child: Padding(
                                      padding:
                                          EdgeInsets.all(Pallet.defaultPadding),
                                      child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.end,
                                        children: [
                                          MyText(
                                            text: cashBalance.toString(),
                                            style: TextStyle(
                                              color: Pallet.fontcolor,
                                              fontWeight: Pallet.font600,
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                  SizedBox(
                                    height: 20,
                                  ),
                                  Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      MyText(
                                          text: dashboard.pageDetails["text88"],
                                          style: TextStyle(
                                            color: Pallet.fontcolor,
                                          )),
                                      MyText(
                                          text: dashboard.pageDetails["text89"],
                                          style: TextStyle(
                                            color: Pallet.fontcolor,
                                            fontSize: 12,
                                          )),
                                    ],
                                  ),
                                  SizedBox(height: 5),
                                  TextFormField(
                                    autofocus: true,
                                    controller: dsvpcontroller,
                                    textAlign: TextAlign.right,

                                    // initialValue:
                                    //     (total - dsvpBalance).toString(),
                                    style: TextStyle(color: Pallet.fontcolor),
                                    cursorColor: Pallet.fontcolor,
                                    onChanged: (value) {
                                      dsvp = double.parse((value));
                                      if (value.isEmpty ||
                                          value == null ||
                                          value == '') {
                                        setState1(() {
                                          dsvpError =
                                              dashboard.pageDetails["text90"];
                                        });
                                      } else if (double.parse((value)) <= 0) {
                                        setState1(() {
                                          dsvpError =
                                              dashboard.pageDetails["text90"];
                                        });
                                      } else if (double.parse((value)) >
                                          (cashBalance)) {
                                        setState1(() {
                                          dsvpError =
                                              dashboard.pageDetails["text91"];
                                        });
                                      } else if (double.parse((value)) % 5 !=
                                          0) {
                                        setState1(() {
                                          dsvpError =
                                              dashboard.pageDetails["text92"];
                                        });
                                      } else if (double.parse((value)) <=
                                          (cashBalance)) {
                                        setState1(() {
                                          dsvpError = null;
                                        });
                                      }
                                    },
                                    keyboardType:
                                        TextInputType.numberWithOptions(
                                            decimal: true),
                                    inputFormatters: <TextInputFormatter>[
                                      FilteringTextInputFormatter.digitsOnly,
                                    ],
                                    decoration: InputDecoration(
                                      suffixIcon: Column(
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        children: [
                                          InkWell(
                                            onTap: () {
                                              print(
                                                  "AAAAAAAAAAAAAAAAAAAAAAAAA");
                                              String amt = dsvpcontroller.text;

                                              if (amt == null || amt == '') {
                                                print("BBBBBBBBBBBBBBBB");
                                                setState1(() {
                                                  dsvp = 5;
                                                });
                                                dsvpcontroller.text = "5";
                                              } else if (double.parse(
                                                          (dsvpcontroller
                                                              .text)) +
                                                      5 >
                                                  (cashBalance)) {
                                                setState1(() {
                                                  dsvpError = dashboard
                                                      .pageDetails["text91"];
                                                });
                                              } else {
                                                var addval =
                                                    amt.addamt(roundofvalue: 5);
                                                dsvp = double.parse((int.parse(
                                                            dsvp.toString()) +
                                                        int.parse(addval))
                                                    .toString());
                                                setState1(() {
                                                  dsvpcontroller.text =
                                                      dsvp.toString();
                                                  dsvpError = null;
                                                });
                                              }
                                            },
                                            child: Container(
                                              decoration: BoxDecoration(
                                                borderRadius:
                                                    BorderRadius.circular(5),
                                                color: Pallet.fontcolor,
                                              ),
                                              padding: EdgeInsets.only(
                                                left: 2,
                                                right: 2,
                                              ),
                                              child: Icon(Icons.arrow_drop_up,
                                                  size: 15,
                                                  color: Pallet.fontcolornew),
                                            ),
                                          ),
                                          SizedBox(
                                            height: 3,
                                          ),
                                          InkWell(
                                            onTap: () {
                                              String amt = dsvpcontroller.text;
                                              if (dsvpcontroller.text == null ||
                                                  dsvpcontroller.text == "0") {
                                                setState1(() {
                                                  dsvp = 0;
                                                });
                                                dsvpcontroller.text = "0";
                                              } else if (int.parse(
                                                          (dsvpcontroller
                                                              .text)) -
                                                      5 <=
                                                  0) {
                                                dsvpcontroller.text = '0';
                                                dsvpError = dashboard
                                                    .pageDetails["text93"];
                                                setState1(() {
                                                  dsvp = 0;
                                                });
                                              } else {
                                                var addval = amt.addamt(
                                                    roundofvalue: 5,
                                                    issubract: true);
                                                // ignore: unnecessary_brace_in_string_interps
                                                print('ADDVAL: ${addval}');

                                                dsvp = double.parse((int.parse(
                                                            dsvp.toString()) -
                                                        int.parse(addval))
                                                    .toString());
                                                setState1(() {
                                                  dsvpcontroller.text =
                                                      dsvp.toString();
                                                  dsvpError = null;
                                                });
                                              }
                                            },
                                            child: Container(
                                              decoration: BoxDecoration(
                                                borderRadius:
                                                    BorderRadius.circular(5),
                                                color: Pallet.fontcolor,
                                              ),
                                              padding: EdgeInsets.only(
                                                left: 2,
                                                right: 2,
                                                // top: 5,
                                              ),
                                              child: Icon(Icons.arrow_drop_down,
                                                  size: 15,
                                                  color: Pallet.fontcolornew),
                                            ),
                                          ),
                                        ],
                                      ),
                                      errorText: dsvpError,
                                      errorStyle:
                                          TextStyle(color: Colors.white),
                                      border: OutlineInputBorder(
                                        borderSide: BorderSide(
                                            color: Pallet.fontcolor, width: 2),
                                      ),
                                      focusedBorder: OutlineInputBorder(
                                          borderSide: BorderSide(
                                              color: Pallet.fontcolor,
                                              width: 2),
                                          borderRadius: BorderRadius.circular(
                                              Pallet.radius)),
                                      enabledBorder: OutlineInputBorder(
                                          borderSide: BorderSide(
                                              color: Pallet.fontcolor,
                                              width: 2),
                                          borderRadius: BorderRadius.circular(
                                              Pallet.radius)),
                                      errorBorder: OutlineInputBorder(
                                          borderSide: BorderSide(
                                              color: Colors.red, width: 2),
                                          borderRadius: BorderRadius.circular(
                                              Pallet.radius)),
                                      focusedErrorBorder: OutlineInputBorder(
                                          borderSide: BorderSide(
                                              color: Colors.red, width: 2),
                                          borderRadius: BorderRadius.circular(
                                              Pallet.radius)),
                                      filled: true,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            SizedBox(
                              height: 10,
                            ),
                            dsvpError == null
                                ? dsvp != null
                                    ? Padding(
                                        padding:
                                            const EdgeInsets.only(left: 8.0),
                                        child: MyText(
                                          text: dashboard
                                                  .pageDetails["text94"] +
                                              ' ' +
                                              '\$' +
                                              dsvp.toString() +
                                              ' ' +
                                              dashboard.pageDetails["text95"],
                                          // "You Get $dsvp DSV-P",
                                          style: TextStyle(
                                            color: Pallet.fontcolor,
                                            fontSize: Pallet.heading5,
                                            fontWeight: FontWeight.w400,
                                          ),
                                        ),
                                      )
                                    : Container()
                                : Container(),
                            SizedBox(
                              height: 15,
                            ),
                            AbsorbPointer(
                              absorbing: (cashBalance) <= 0 ? true : false,
                              child: CustomButton(
                                vpadding: 10,
                                buttoncolor: Pallet.fontcolor,
                                text: dashboard.pageDetails["text5"],
                                textcolor: Pallet.fontcolornew,
                                onpress: () async {
                                  if (dsvpcontroller.text == null ||
                                      dsvpcontroller.text == '') {
                                    setState1(() {
                                      dsvpError =
                                          dashboard.pageDetails["text93"];
                                    });
                                  } else if (double.parse(
                                          (dsvpcontroller.text)) <=
                                      0) {
                                    setState1(() {
                                      dsvpError =
                                          dashboard.pageDetails["text90"];
                                    });
                                  } else if (dsvpError == null) {
                                    if (dsvpcontroller.text != null &&
                                        (cashBalance) > 0) {
                                      double amount =
                                          double.parse(dsvpcontroller.text);
                                      setState1(() {
                                        cashBalance = (cashBalance - amount);

                                        dsvpBalance = (dsvpBalance + amount);

                                        // dsvpcontroller.MyText = "";
                                      });
                                      Map<String, dynamic> _map = {
                                        "payload": {
                                          "product_id": 1,
                                          "split_type": 'none',
                                          "is_dsv_p": true,
                                          "total": amount,
                                          "cash_account_used": amount,
                                          "trading_account_used": 0,
                                          "packages": [],
                                        }
                                      };
                                      waiting.waitpopup(context);

                                      Map<String, dynamic> _result =
                                          await HttpRequest.Post(
                                              'buyPackages', _map);
                                      if (Utils.isServerError(_result)) {
                                        Navigator.pop(context);

                                        Navigator.of(context).pop();
                                        snack.snack(
                                            title: dashboard
                                                .pageDetails['text96']);

                                        return await Utils.getMessage(
                                            _result['response']['error']);
                                      } else {
                                        getDsvPData();
                                        // Navigator.pop(context);

                                        Navigator.of(context).pop();
                                        return custompopup.showdialog(
                                          context: context,
                                          title:
                                              dashboard.pageDetails["text97"],
                                          scrollable: true,
                                          popupwidth: 250,
                                          content: Container(
                                            height: 150,
                                            child: Column(
                                              mainAxisAlignment:
                                                  MainAxisAlignment.spaceEvenly,
                                              children: [
                                                CircleAvatar(
                                                  backgroundColor: Colors.white,
                                                  radius: 30,
                                                  child: Icon(Icons.done,
                                                      color: Pallet
                                                          .dashcontainerback,
                                                      size: 40),
                                                ),
                                                MyText(
                                                    text: dashboard.pageDetails[
                                                            "text98"] +
                                                        dsvpcontroller.text,
                                                    style: TextStyle(
                                                        fontSize:
                                                            Pallet.normalfont,
                                                        color: Pallet.fontcolor,
                                                        fontWeight:
                                                            Pallet.font500)),
                                                MyText(
                                                    text: dashboard.pageDetails[
                                                            "text99"] +
                                                        ' ' +
                                                        dsvpBalance.toString(),
                                                    style: TextStyle(
                                                        fontSize:
                                                            Pallet.normalfont,
                                                        color: Pallet.fontcolor,
                                                        fontWeight:
                                                            Pallet.font500))
                                              ],
                                            ),
                                          ),
                                          actions: [
                                            PopupButton(
                                              text: dashboard
                                                  .pageDetails["text100"],
                                              onpress: () {
                                                Navigator.of(context).pop();

                                                Navigator.pushReplacement(
                                                  context,
                                                  MaterialPageRoute(
                                                      builder: (context) => Home(
                                                          route:
                                                              'centurion_shop')),
                                                );
                                              },
                                            ),
                                            PopupButton(
                                              text: dashboard
                                                  .pageDetails["text82"],
                                              onpress: () {
                                                Navigator.of(context).pop();

                                                Navigator.pushReplacement(
                                                  context,
                                                  MaterialPageRoute(
                                                      builder: (context) =>
                                                          Home(
                                                              route:
                                                                  'dashboard')),
                                                );
                                              },
                                            ),
                                          ],
                                        );

                                        //
                                      }
                                    } else if ((cashBalance) <= 0) {
                                      setState1(() {
                                        dsvpError =
                                            dashboard.pageDetails["text91"];
                                      });
                                    }
                                  }
                                },
                                fontweight: Pallet.font600,
                              ),
                            ),
                            // SizedBox(
                            //   height: 15,
                            // ),
                            // MyText(
                            //   "Insufficient Cash Balance to Get DSV-P?",
                            //   style: TextStyle(
                            //     color: Pallet.dashcontainerback,
                            //     fontSize: 11,
                            //   ),
                            // ),
                            // SizedBox(
                            //   height: 9,
                            // ),
                            // AbsorbPointer(
                            //   absorbing: (cashBalance) <= 0 ? false : true,
                            //   child: CustomButton(
                            //     vpadding: 10,
                            //     MyText: "ADD FUND",
                            //     buttoncolor: Pallet.dashcontainerback,
                            //     textcolor: Pallet.fontcolor,
                            //     onpress: () {
                            //       Navigator.push(
                            //         context,
                            //         MaterialPageRoute(
                            //             builder: (context) =>
                            //                 Home(route: 'add_funding')),
                            //       );
                            //     },
                            //     fontweight: Pallet.font600,
                            //   ),
                            // ),
                          ],
                        ),
                      ),
                      actions: [
                        PopupButton(
                          text: dashboard.pageDetails["text78"],
                          onpress: () {
                            dsvpcontroller.text = '';
                            dsvpError = null;
                            dsvp = null;
                            Navigator.of(context).pop();
                          },
                        ),
                        SizedBox(height: 10),
                      ],
                    );
                  },
                  child: Container(
                    width: wdgtWidth * 0.50,
                    height: wdgtHeight * 0.45,
                    decoration: BoxDecoration(
                        boxShadow: [Pallet.shadowEffect],
                        color: Pallet.specialdashcontainerback,
                        borderRadius: BorderRadius.circular(10)),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Image.asset('shop_now_with_dsvp.png', width: 50),
                        SizedBox(height: 10),
                        MyText(
                          text: dashboard.pageDetails["text5"],
                          textAlign: TextAlign.center,
                          style: TextStyle(
                              fontSize: wdgtWidth * 0.04,
                              color: Pallet.fontcolor),
                        )
                      ],
                    ),
                  ),
                ),
                Container(
                  child: Image.asset(
                    'buy_now1.png',
                    width: wdgtWidth * 0.50,
                    height: wdgtHeight * 0.50,
                  ),
                ),
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                Container(
                  child: Image.asset(
                    'final_add.png',
                    width: wdgtWidth * 0.50,
                    height: wdgtHeight * 0.50,
                  ),
                ),
                InkWell(
                  onTap: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => Home(
                                route: 'buy_dsv',
                              )),
                    );
                  },
                  child: Container(
                    width: wdgtWidth * 0.45,
                    height: wdgtHeight * 0.45,
                    decoration: BoxDecoration(
                        color: Pallet.specialdashcontainerback,
                        borderRadius: BorderRadius.circular(10)),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Image.asset('buy_dsv_packages.png', width: 50),
                        SizedBox(height: 10),
                        MyText(
                          text: dashboard.pageDetails["text6"],
                          textAlign: TextAlign.center,
                          style: TextStyle(
                              fontSize: wdgtWidth * 0.04,
                              color: Pallet.fontcolor),
                        )
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ],
        ));
  }

  Widget mostusedpackages({double wdgtWidth, wdgtHeight, prodWidth}) {
    return Container(
      padding: EdgeInsets.all(wdgtWidth * 0.03),
      decoration: BoxDecoration(
          boxShadow: [Pallet.shadowEffect],
          color: Pallet.dashcontainerback,
          borderRadius: BorderRadius.circular(10)),
      width: wdgtWidth,
      height: wdgtHeight,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          MyText(
            text: dashboard.pageDetails["text101"],
            style: TextStyle(
              color: Pallet.fontcolor,
              fontSize: Pallet.heading3,
              fontWeight: Pallet.font500,
            ),
          ),
          SizedBox(height: 10),
          Container(
            // decoration: BoxDecoration(
            //   border: Border.all(width: 1, color: Pallet.fontcolor),
            // ),
            width: wdgtWidth,
            height: wdgtHeight * 0.7,
            child: InkWell(
              //Product details page
              onTap: () async {
                print('STORE_ID_DOC');
                print(storeId);
                shopRoute = 'details';
                productId = dashboard.stores[0]['product_id'];
                // unique_product_id: 29;
                storeId = dashboard.stores[0]['category_id'];
                cart.categoryName = dashboard.stores[0]['category_name'];

                Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => Home(
                            route: 'centurion_shop',
                            param: 'true',
                          )),
                );
              },
              //Product Page
              // onTap: () async {
              //   shopRoute = 'products';
              //   storeId = 1;
              //   cart.categoryName = "Aspire";

              //   Navigator.push(
              //     context,
              //     MaterialPageRoute(
              //         builder: (context) => Home(
              //               route: 'centurion_shop',
              //               param: 'true',
              //             )),
              //   );
              // },

//Shop page
              // onTap: () {

              //   Navigator.push(
              //     context,
              //     MaterialPageRoute(
              //         builder: (context) => Home(route: 'centurion_shop')),
              //   );
              // },
              child: Image.network(
                appSettings['SERVER_URL'] +
                    '/' +
                    dashboard.stores[0]['product_image'],
// width: wdgtWidth,
// height: 400,
                fit: BoxFit.contain,
              ),
            ),
          ),
        ],
      ),
// child: Column(
// mainAxisAlignment: MainAxisAlignment.spaceEvenly,
// crossAxisAlignment: CrossAxisAlignment.start,
// children: [
// MyText(
// 'Exclusive products',
// style: TextStyle(
// color: Pallet.fontcolor,
// fontSize: 20,
// fontWeight: Pallet.font500,
// ),
// ),
// Container(
// width: wdgtWidth,
// height: wdgtHeight * 0.7,
// child: Image.asset(
// 'exclproduct.jpg',
// width: wdgtWidth,
// height: wdgtHeight,
// ),
// // child: ListView.builder(
// // scrollDirection: Axis.horizontal,
// // // itemCount: products.length,
// // itemBuilder: (context, index) => Padding(
// // padding: EdgeInsets.only(right: 10),
// // child: Product(
// // wdgtWidth: prodWidth,
// // wdgtPading: 0,
// // // product: products[index],
// // press: () {
// // setState(() {});
// // },
// // ),
// // ))
// ),
// ],
// )
    );
  }
//   Widget mostusedpackages({double wdgtWidth, wdgtHeight, prodWidth}) {
//     return Container(
//       padding: EdgeInsets.all(wdgtWidth * 0.03),
//       decoration: BoxDecoration(
//           boxShadow: [Pallet.shadowEffect],
//           color: Pallet.dashcontainerback,
//           borderRadius: BorderRadius.circular(10)),
//       width: wdgtWidth,
//       height: wdgtHeight,
//       child: Column(
//         mainAxisAlignment: MainAxisAlignment.spaceEvenly,
//         crossAxisAlignment: CrossAxisAlignment.start,
//         children: [
//           MyText(
//             'Product of the Week',
//             style: TextStyle(
//               color: Pallet.fontcolor,
//               fontSize: Pallet.heading3,
//               fontWeight: Pallet.font500,
//             ),
//           ),
//           SizedBox(height: 10),
//           Container(
//             width: wdgtWidth,
//             height: wdgtHeight * 0.7,
//             decoration: BoxDecoration(
//                 // border: Border.all(color: Pallet.fontcolor, width: 2),
//                 ),
//             child: Row(
//               mainAxisAlignment: MainAxisAlignment.spaceEvenly,
//               children: [
//                 for (var store in dashboard.stores)
//                   InkWell(
//                     onTap: () async {
//                       shopRoute = 'products';
//                       storeId = store['store_id'];
//                       cart.categoryName = store['store_name'];

//                       Navigator.push(
//                         context,
//                         MaterialPageRoute(
//                             builder: (context) => Home(
//                                   route: 'centurion_shop',
//                                   param: 'true',
//                                 )),
//                       );
//                     },
//                     child: Container(
//                       width: wdgtWidth * 0.44,
//                       height: wdgtHeight * 0.8,
//                       decoration: BoxDecoration(
//                         color: Pallet.specialdashcontainerback,
//                         borderRadius: BorderRadius.circular(20),
//                         image: DecorationImage(
//                           image: NetworkImage(
//                             appSettings['SERVER_URL'] +
//                                 '/' +
//                                 store['store_image'].toString(),
//                           ),
//                           fit: BoxFit.fitWidth,
//                         ),
//                       ),
//                     ),
//                   ),
//               ],
//             ),
//           ),
//         ],
//       ),
// // child: Column(
// // mainAxisAlignment: MainAxisAlignment.spaceEvenly,
// // crossAxisAlignment: CrossAxisAlignment.start,
// // children: [
// // MyText(
// // 'Exclusive products',
// // style: TextStyle(
// // color: Pallet.fontcolor,
// // fontSize: 20,
// // fontWeight: Pallet.font500,
// // ),
// // ),
// // Container(
// // width: wdgtWidth,
// // height: wdgtHeight * 0.7,
// // child: Image.asset(
// // 'exclproduct.jpg',
// // width: wdgtWidth,
// // height: wdgtHeight,
// // ),
// // // child: ListView.builder(
// // // scrollDirection: Axis.horizontal,
// // // // itemCount: products.length,
// // // itemBuilder: (context, index) => Padding(
// // // padding: EdgeInsets.only(right: 10),
// // // child: Product(
// // // wdgtWidth: prodWidth,
// // // wdgtPading: 0,
// // // // product: products[index],
// // // press: () {
// // // setState(() {});
// // // },
// // // ),
// // // ))
// // ),
// // ],
// // )
//     );
//   }
//category widgets
//   Widget mostusedpackages({double wdgtWidth, wdgtHeight, prodWidth}) {
//     return Container(
//       padding: EdgeInsets.all(wdgtWidth * 0.03),
//       decoration: BoxDecoration(
//           boxShadow: [Pallet.shadowEffect],
//           color: Pallet.dashcontainerback,
//           borderRadius: BorderRadius.circular(10)),
//       width: wdgtWidth,
//       height: wdgtHeight,
//       child: Column(
//         mainAxisAlignment: MainAxisAlignment.spaceEvenly,
//         crossAxisAlignment: CrossAxisAlignment.start,
//         children: [
//           MyText(
//             'Product of the Week',
//             style: TextStyle(
//               color: Pallet.fontcolor,
//               fontSize: Pallet.heading3,
//               fontWeight: Pallet.font500,
//             ),
//           ),
//           SizedBox(height: 10),
//           Container(
//             width: wdgtWidth,
//             height: wdgtHeight * 0.7,
//             decoration: BoxDecoration(
//                 // border: Border.all(color: Pallet.fontcolor, width: 2),
//                 ),
//             child: Row(
//               mainAxisAlignment: MainAxisAlignment.spaceEvenly,
//               children: [
//                 for (var store in dashboard.stores)
//                   InkWell(
//                     onTap: () {
//                       if (store['store_id'] == 1) {
//                         print('Yes One');
//                         print(store['store_id']);
//                       } else {
//                         print('Yes Two');
//                         print(store['store_id']);
//                       }
//                       // Navigator.push(
//                       //   context,
//                       //   MaterialPageRoute(
//                       //       builder: (context) => Home(
//                       //             route: 'buy_dsv',
//                       //           )),
//                       // );
//                     },
//                     child: Container(
//                       width: wdgtWidth * 0.44,
//                       height: wdgtHeight * 0.8,
//                       decoration: BoxDecoration(
//                         color: Pallet.specialdashcontainerback,
//                         borderRadius: BorderRadius.circular(10),
//                         image: DecorationImage(
//                           image: NetworkImage(
//                             appSettings['SERVER_URL'] +
//                                 '/' +
//                                 store['store_image'].toString(),
//                           ),
//                           fit: BoxFit.fill,
//                         ),
//                       ),
//                     ),
//                   ),
//               ],
//             ),
//           ),
//         ],
//       ),
// // child: Column(
// // mainAxisAlignment: MainAxisAlignment.spaceEvenly,
// // crossAxisAlignment: CrossAxisAlignment.start,
// // children: [
// // MyText(
// // 'Exclusive products',
// // style: TextStyle(
// // color: Pallet.fontcolor,
// // fontSize: 20,
// // fontWeight: Pallet.font500,
// // ),
// // ),
// // Container(
// // width: wdgtWidth,
// // height: wdgtHeight * 0.7,
// // child: Image.asset(
// // 'exclproduct.jpg',
// // width: wdgtWidth,
// // height: wdgtHeight,
// // ),
// // // child: ListView.builder(
// // // scrollDirection: Axis.horizontal,
// // // // itemCount: products.length,
// // // itemBuilder: (context, index) => Padding(
// // // padding: EdgeInsets.only(right: 10),
// // // child: Product(
// // // wdgtWidth: prodWidth,
// // // wdgtPading: 0,
// // // // product: products[index],
// // // press: () {
// // // setState(() {});
// // // },
// // // ),
// // // ))
// // ),
// // ],
// // )
//     );
//   }

  // Widget mostusedpackages(
  //     {double wdgtWidth, wdgtHeight, prodWidth, myviewbox}) {
  //   return Container(
  //       padding: EdgeInsets.all(wdgtWidth * 0.02),
  //       decoration: BoxDecoration(
  //           color: Pallet.dashcontainerback,
  //           borderRadius: BorderRadius.circular(10)),
  //       width: wdgtWidth,
  //       height: wdgtHeight,
  //       child: Column(
  //         crossAxisAlignment: CrossAxisAlignment.start,
  //         mainAxisAlignment: MainAxisAlignment.start,
  //         children: [
  //           MyText(
  //             'Product of the Week',
  //             style: TextStyle(
  //               color: Pallet.fontcolor,
  //               fontSize: Pallet.heading3,
  //               fontWeight: Pallet.font500,
  //             ),
  //           ),
  //           SizedBox(height: 10),
  //           Container(
  //             width: wdgtWidth,
  //             height: wdgtHeight * 0.750,
  //             // decoration: BoxDecoration(
  //             //     border: Border.all(width: 1, color: Pallet.fontcolor)),
  //             child: CarouselSlider(
  //                 // scrollDirection: Axis.horizontal,
  //                 // controller: assetsListViewController,
  //                 options: CarouselOptions(
  //                   autoPlayInterval: Duration(seconds: 2),
  //                   autoPlayCurve: Curves.fastOutSlowIn,
  //                   scrollDirection: Axis.horizontal,
  //                   autoPlayAnimationDuration: Duration(milliseconds: 800),
  //                   autoPlay: false,
  //                   // aspectRatio: 2.0,
  //                   // enlargeCenterPage: true,
  //                   viewportFraction: myviewbox,
  //                 ),
  //                 items: [
  //                   for (var i = 0; i < dashboard.stores.length; i++)
  //                     // for (var product in dashboard.myproducts)
  //                     InkWell(
  //                       onTap: () {
  //                         shopRoute = 'products';
  //                         storeId = dashboard.stores[i]['store_id'];
  //                         cart.categoryName = dashboard.stores[i]['store_name'];

  //                         Navigator.push(
  //                           context,
  //                           MaterialPageRoute(
  //                               builder: (context) => Home(
  //                                     route: 'centurion_shop',
  //                                     param: 'true',
  //                                   )),
  //                         );
  //                       },
  //                       child: Container(
  //                         width: prodWidth,
  //                         height: wdgtHeight * 0.7,
  //                         decoration: BoxDecoration(
  //                           color: Pallet.specialdashcontainerback,
  //                           borderRadius: BorderRadius.circular(10),
  //                           image: DecorationImage(
  //                             image: NetworkImage(
  //                               appSettings['SERVER_URL'] +
  //                                   '/' +
  //                                   dashboard.stores[i]['store_image']
  //                                       .toString(),
  //                             ),
  //                             fit: BoxFit.fill,
  //                           ),
  //                         ),
  //                       ),
  //                     ),
  //                 ]),
  //           ),
  //         ],
  //       ));
  // }

  Widget mystatus({double wdgtWidth, double wdgtHeight}) {
    double width = MediaQuery.of(context).size.width;

    return Container(
      padding: EdgeInsets.symmetric(horizontal: wdgtWidth * 0.03, vertical: 20),
      width: wdgtWidth,
      height: wdgtHeight,
      decoration: BoxDecoration(
          boxShadow: [Pallet.shadowEffect],
          color: Pallet.specialdashcontainerback,
          borderRadius: BorderRadius.circular(10)),
      child: Column(
        children: [
          Row(
            children: [
              MyText(
                  text: dashboard.pageDetails["text7"],
                  style: TextStyle(
                      fontSize: Pallet.heading3,
                      fontWeight: Pallet.font500,
                      color: Pallet.fontcolor)),
            ],
          ),
          SizedBox(
            height: 20,
          ),
          if (width > 930)
            Row(
              children: [
                Expanded(
                    flex: 3,
                    child: tteaminfoBox(
                      wdgtHeight: wdgtHeight,
                      wdgtWidth: wdgtWidth,
                    )),
                Expanded(
                  flex: 1,
                  child: mystatusindicator(
                    wdgtWidth: wdgtWidth,
                  ),
                ),
              ],
            ),
          if (width < 930)
            Expanded(
                flex: 3,
                child: tteaminfoBox(
                  wdgtHeight: wdgtHeight,
                  wdgtWidth: wdgtWidth,
                )),
          if (width < 930)
            Expanded(
              flex: 1,
              child: mystatusindicator(
                wdgtWidth: wdgtWidth,
              ),
            ),
        ],
      ),
    );
  }

  Widget tteaminfoBox({double wdgtWidth, double wdgtHeight}) {
    double width = MediaQuery.of(context).size.width;

    return Wrap(
      spacing: width > 930 ? 5 : 10,
      runSpacing: width > 930 ? 5 : 10,
      children: [
        if (dashboard.myteamstatus['status_params'] != null)
          for (var data in dashboard.myteamstatus['status_params'])
            Container(
              padding: EdgeInsets.only(left: 10),
              decoration: BoxDecoration(
                  color: Pallet.dashsmallcontainerback,
                  borderRadius: BorderRadius.circular(10)),
              width: width > 930 ? wdgtWidth * 0.195 : wdgtWidth / 2.5,
              height: width > 930 ? wdgtHeight * 0.32 : wdgtHeight * 0.15,
              child: Stack(
                children: [
                  Positioned(
                    right: 10,
                    top: 10,
                    child: data['enabled'].toString().toLowerCase() == 'f'
                        ? Container(
                            decoration: BoxDecoration(
                                color: Pallet.error,
                                borderRadius: BorderRadius.circular(50)),
                            width: 9,
                            height: 9,

                            // decoration: BoxDecoration(
                            //     color: data['enabled']
                            //                 .toString()
                            //                 .toLowerCase() ==
                            //             'f'
                            //         ? Pallet.error
                            //         : data['enabled']
                            //                     .toString()
                            //                     .toLowerCase() ==
                            //                 'null'
                            //             ? Colors.transparent
                            //             : Pallet.success,
                            //     borderRadius: BorderRadius.circular(10)),
                          )
                        : data['enabled'].toString().toLowerCase() == 'null'
                            ? Container()
                            : Container(
                                decoration: BoxDecoration(
                                    color: Color(0xFF41ad49),
                                    borderRadius: BorderRadius.circular(50)),
                                width: 15,
                                height: 15,
                                child: Icon(Icons.done,
                                    color: Pallet.fontcolor, size: 10)),
                  ),
                  Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      if (data['key'].toString() == 'kyc')
                        MyText(
                            text: dashboard.pageDetails["text169"],
                            style: TextStyle(
                                fontSize: Pallet.heading6,
                                fontWeight: Pallet.font500,
                                color: Pallet.fontcolor)),
                      if (data['key'].toString() == '2fa')
                        MyText(
                            text: dashboard.pageDetails["text170"],
                            style: TextStyle(
                                fontSize: Pallet.heading6,
                                fontWeight: Pallet.font500,
                                color: Pallet.fontcolor)),
                      if (data['key'].toString() == 'tb')
                        MyText(
                            text: dashboard.pageDetails["text171"],
                            style: TextStyle(
                                fontSize: Pallet.heading6,
                                fontWeight: Pallet.font500,
                                color: Pallet.fontcolor)),
                      if (data['key'].toString() == 'mb')
                        MyText(
                            text: dashboard.pageDetails["text172"],
                            style: TextStyle(
                                fontSize: Pallet.heading6,
                                fontWeight: Pallet.font500,
                                color: Pallet.fontcolor)),
                      if (data['key'].toString() == 'bv')
                        MyText(
                            text: dashboard.pageDetails["text173"],
                            style: TextStyle(
                                fontSize: Pallet.heading6,
                                fontWeight: Pallet.font500,
                                color: Pallet.fontcolor)),
                      if (data['key'].toString() == 'rank')
                        MyText(
                            text: dashboard.pageDetails["text174"],
                            style: TextStyle(
                                fontSize: Pallet.heading6,
                                fontWeight: Pallet.font500,
                                color: Pallet.fontcolor)),
                      // MyText(
                      //     data['status_name'] == null
                      //         ? ""
                      //         : data['status_name'],
                      //     style: TextStyle(
                      //         fontSize: Pallet.heading6,
                      //         fontWeight: Pallet.font500,
                      //         color: Pallet.fontcolor)),
                      SizedBox(height: 8),
                      MyText(
                          text: data['status_value'],
                          style: TextStyle(
                              fontSize: Pallet.heading4,
                              fontWeight: Pallet.font500,
                              color: Pallet.fontcolor)),
                    ],
                  ),
                ],
              ),
            ),
        SizedBox(height: 5),
        Wrap(
          children: [
            // CountdownTimer(
            //   textStyle: TextStyle(
            //     fontSize: Pallet.heading2,
            //     fontWeight: Pallet.font500,
            //     color: Pallet.error,
            //   ),
            //   onEnd: onEnd,
            //   endTime: endTime,
            // ),
            if (width > 930)
              if (dashboard.myteamstatus['show_count_down'] == 't')
                BlinkText(
                  dashboard.myteamstatus['days_remaining'].toString() +
                      ' ' +
                      dashboard.pageDetails["text102"],
                  beginColor: Colors.orange,
                  endColor: Pallet.fontcolor,
                  times: 10000,
                  style: new TextStyle(
                      fontWeight: Pallet.font500,
                      fontSize: Pallet.heading6,
                      color: Colors.red),
                ),
          ],
        ),
      ],
    );
  }

  Widget mystatusindicator({
    double wdgtWidth,
  }) {
    double width = MediaQuery.of(context).size.width;

    return Container(
      // width: width > 930 ? wdgtWidth * 0.24 : wdgtWidth * 0.10,
      // height: wdgtHeight * 0.75,
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          CircularPercentIndicator(
            radius: width > 600 ? wdgtWidth * 0.1 : 90,
            lineWidth: width > 600 ? wdgtWidth * 0.012 : 10,
            animation: true,
            // percent: 0.8,
            animationDuration: 2500,
            percent: double.parse((double.parse(
                        dashboard.myteamstatus['rank'] == ""
                            ? ""
                            : dashboard.myteamstatus['rank']) /
                    13)
                .toString()),
            center: new MyText(
              // '5',
              text: dashboard.myteamstatus['rank'] == ""
                  ? ""
                  : dashboard.myteamstatus['rank'],
              style: new TextStyle(
                  fontWeight: Pallet.font500,
                  fontSize: Pallet.heading1,
                  color: Pallet.fontcolor),
            ),

            footer: Column(
              children: [
                Padding(
                  padding: const EdgeInsets.only(top: 5),
                  child: MyText(
                    text: dashboard.rankCurrentName(
                                dashboard.myteamstatus['rank_name']) ==
                            ''
                        ? dashboard.pageDetails["text9"].toUpperCase()
                        : dashboard
                            .rankCurrentName(
                                dashboard.myteamstatus['rank_name'])
                            .toUpperCase(),
                    textAlign: TextAlign.center,
                    style: new TextStyle(
                        fontWeight: Pallet.font500,
                        fontSize: Pallet.heading4,
                        color: Pallet.fontcolor),
                  ),
                ),
                if (width < 930)
                  if (dashboard.myteamstatus['show_count_down'] == 't')
                    BlinkText(
                      dashboard.myteamstatus['days_remaining'].toString() +
                          ' ' +
                          dashboard.pageDetails["text102"],
                      beginColor: Colors.orange,
                      endColor: Pallet.fontcolor,
                      times: 10000,
                      style: new TextStyle(
                          fontWeight: Pallet.font500,
                          fontSize: Pallet.heading6,
                          color: Colors.red),
                    ),
              ],
            ),
            circularStrokeCap: CircularStrokeCap.round,
            progressColor: Pallet.fontcolor,
            // linearGradient:
            //     dashboard.rankColor(dashboard.myteamstatus['rank']),
            // progressColor:
            //     dashboard.rankColor(dashboard.myteamstatus['rank']),
            backgroundColor: Color(0XFF2e5ea6),
          ),
          width < 600
              ? SizedBox(height: 5)
              : width >= 600 && width < 930
                  ? SizedBox(height: 15)
                  : SizedBox(height: 20),
          // Wrap(
          //   children: [
          //     MyText('Needed to Rank up - 2 Silver',
          //         style: TextStyle(
          //             // fontWeight: Pallet.font500,
          //             height: 1.4,
          //             fontSize: Pallet.heading6,
          //             color: Pallet.fontcolor)),
          //   ],
          // ),
          // SizedBox(height: 5),
          // LinearPercentIndicator(
          //   animation: true,
          //   lineHeight: 20.0,
          //   animationDuration: 2500,
          //   center: MyText('80%',
          //       style: TextStyle(
          //           fontWeight: Pallet.font600,
          //           fontSize: Pallet.heading4,
          //           color: Pallet.fontcolornew)),
          //   percent: 0.8,
          //   linearStrokeCap: LinearStrokeCap.roundAll,
          //   progressColor: Pallet.fontcolor,
          //   backgroundColor: Color(0XFF2e5ea6),
          // ),
          // SizedBox(height: 5),
          Wrap(
            children: [
              MyText(
                  text: dashboard.pageDetails["text103"] +
                      ' ' +
                      dashboard.myteamstatus['next_rank'].toString() +
                      ' - ' +
                      dashboard.pageDetails["text104"] +
                      ' ' +
                      dashboard.myteamstatus['next_bonus'].toString() +
                      ' ' +
                      '- BV' +
                      ' ' +
                      dashboard.myteamstatus['top'].toString() +
                      ' / ' +
                      dashboard.myteamstatus['down'].toString(),
                  style: TextStyle(
                      // fontWeight: Pallet.font500,
                      height: 1.4,
                      fontSize: Pallet.heading6,
                      color: Pallet.fontcolor)),
            ],
          )
        ],
      ),
    );
  }

  String teaminfofunc(String serverval) {
    if (serverval == 'performance') {
      return dashboard.pageDetails["text157"].toString();
    } else if (serverval == 'purchasing_partners') {
      return dashboard.pageDetails["text158"].toString();
    } else if (serverval == 'new_partners') {
      return dashboard.pageDetails["text159"].toString();
    } else if (serverval == 'waiting_partners') {
      return dashboard.pageDetails["text160"].toString();
    } else if (serverval == 'my_team') {
      return dashboard.pageDetails["text156"].toString();
    } else if (serverval == 'rank_partners') {
      return dashboard.pageDetails["text161"].toString();
    } else if (serverval == 'rank_qualifying_members') {
      return 'Yes'.toString();
    }

    return '';
  }

  Widget teaminfo(
      {double wdgtWidth, double wdgtHeight, popheading, rankwidth}) {
    double width = MediaQuery.of(context).size.width;

    return Container(
      decoration: BoxDecoration(
          boxShadow: [Pallet.shadowEffect],
          color: Pallet.specialdashcontainerback,
          borderRadius: BorderRadius.circular(10)),
      width: wdgtWidth,
      height: wdgtHeight,
      child: Padding(
        padding: const EdgeInsets.all(11.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Padding(
              padding: const EdgeInsets.only(left: 10.0),
              child: MyText(
                text: dashboard.pageDetails["text15"],
                style: TextStyle(
                    fontSize: Pallet.heading3,
                    fontWeight: Pallet.font500,
                    color: Pallet.fontcolor),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(left: 10.0),
              child: MyText(
                text: dashboard.team_bv['total_customers'] == null
                    ? ''
                    : dashboard.pageDetails["text105"] +
                        ' ' +
                        dashboard.team_bv['total_customers'].toString(),
                style: TextStyle(
                    fontSize: Pallet.heading6,
                    fontWeight: Pallet.font500,
                    color: Pallet.fontcolor),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(left: 10.0),
              child: MyText(
                text: dashboard.team_bv['total_networkers'] == null
                    ? ''
                    : dashboard.pageDetails["text106"] +
                        ' ' +
                        dashboard.team_bv['total_networkers'].toString(),
                style: TextStyle(
                    fontSize: Pallet.heading6,
                    fontWeight: Pallet.font500,
                    color: Pallet.fontcolor),
              ),
              // child: RichText(
              //   MyText: TextSpan(
              //     MyText: dashboard.team_bv['team_bv'].toString() + ' ',
              //     style: TextStyle(
              //         fontSize: 35,
              //         fontFamily: 'Montserrat',
              //         fontWeight: Pallet.font500,
              //         color: Pallet.fontcolor),
              //     children: <TextSpan>[
              //       TextSpan(
              //           MyText: dashboard.pageDetails["text16"],
              //           style: TextStyle(
              //               fontSize: Pallet.heading6,
              //               fontFamily: 'Montserrat',
              //               fontWeight: Pallet.font500,
              //               color: Pallet.fontcolor)),
              //     ],
              //   ),
              // ),
            ),
            Container(
              padding: const EdgeInsets.only(left: 10.0),
              width: wdgtWidth,
              child: Wrap(
                runSpacing: 5,
                spacing: 5,
                children: [
                  for (var i = 0; i < dashboard.myteam1.length; i++)
                    InkWell(
                      onTap: () async {
                        if (dashboard.myteam1[i]['id'] == 2) {
                          myteam_inside_data = dashboard.myteam1[i]['data'];

                          if (myteam_inside_data.isEmpty ||
                              myteam_inside_data == null) {
                            var result = teaminfofunc(
                                dashboard.myteam1[i]['key'].toString());

                            if (result != '') {
                              snack.snack(
                                  title: result.toString() +
                                      ' ' +
                                      dashboard.pageDetails["text175"]);
                            } else {
                              print("Something went wrong please define me!!");
                            }
                          } else {
                            return custompopup.showdialog(
                              scrollable: true,
                              context: context,
                              title: teaminfofunc(
                                  dashboard.myteam1[i]['key'].toString()),
                              content: StatefulBuilder(
                                builder: (BuildContext context,
                                        void Function(void Function())
                                            setState3) =>
                                    Column(
                                  mainAxisSize: MainAxisSize.min,
                                  children: [
                                    myteammebers(
                                        popheading: popheading,
                                        setState3: setState3,
                                        i: i),
                                  ],
                                ),
                              ),
                              actions: [
                                PopupButton(
                                  text: dashboard.pageDetails["text78"],
                                  onpress: () {
                                    place = false;
                                    placeError = null;
                                    placement.text = '';
                                    Navigator.of(context).pop();
                                  },
                                ),
                              ],
                            );
                          }
                        } else if (dashboard.myteam1[i]['id'] == 3) {
                          print('@@@@@@@@@@@@@');
                          myteam_inside_data = dashboard.myteam1[i]['data']
                              ['rank_pending_members'];
                          myteam_inside_data1 = dashboard.myteam1[i]['data']
                              ['rank_qualifying_members'];
                          print(myteam_inside_data);

                          if (myteam_inside_data.isEmpty ||
                              myteam_inside_data == null ||
                              myteam_inside_data1.isEmpty ||
                              myteam_inside_data1 == null) {
                            var result = teaminfofunc(
                                dashboard.myteam1[i]['key'].toString());
                            if (result != '') {
                              snack.snack(title: result);
                            }
                          } else {
                            await rankqualifypartnerspopup(myteam_inside_data1,
                                myteam_inside_data, context, rankwidth);
                          }
                        } else {
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) =>
                                    Home(route: 'my_network')),
                          );
                        }
                      },
                      child: Container(
                        padding: EdgeInsets.symmetric(horizontal: 15),
                        decoration: BoxDecoration(
                            color: Pallet.dashsmallcontainerback,
                            borderRadius: BorderRadius.circular(10)),
                        //   width: wdgtWidth * 0.43,
                        // height: wdgtHeight * 0.20,
                        width:
                            width > 530 ? wdgtWidth * 0.30 : wdgtWidth * 0.41,
                        height:
                            width > 530 ? wdgtHeight * 0.30 : wdgtHeight * 0.20,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            MyText(
                                text: teaminfofunc(
                                    dashboard.myteam1[i]['key'].toString()),
                                style: TextStyle(
                                    fontSize: Pallet.heading6,
                                    fontWeight: Pallet.font500,
                                    color: Pallet.fontcolor)),
                            SizedBox(height: 10),
                          ],
                        ),
                      ),
                    ),

                  // Container(
                  //   padding: EdgeInsets.symmetric(horizontal: 15),
                  //   decoration: BoxDecoration(
                  //       color: Pallet.dashsmallcontainerback,
                  //       borderRadius: BorderRadius.circular(10)),
                  //   width: wdgtWidth * 0.30,
                  //   height: wdgtHeight * 0.30,
                  //   child: Column(
                  //     mainAxisAlignment: MainAxisAlignment.center,
                  //     crossAxisAlignment: CrossAxisAlignment.start,
                  //     children: [
                  //       MyText('Top Performers',
                  //           style: TextStyle(
                  //               fontSize: Pallet.heading6,
                  //               fontWeight: Pallet.font500,
                  //               color: Pallet.fontcolor)),
                  //       SizedBox(height: 10),
                  //       MyText('5',
                  //           overflow: TextOverflow.ellipsis,
                  //           style: TextStyle(
                  //               fontSize: Pallet.heading4,
                  //               fontWeight: Pallet.font500,
                  //               color: Pallet.fontcolor)),
                  //     ],
                  //   ),
                  // ),
                  // Container(
                  //   padding: EdgeInsets.symmetric(horizontal: 15),
                  //   decoration: BoxDecoration(
                  //       color: Pallet.dashsmallcontainerback,
                  //       borderRadius: BorderRadius.circular(10)),
                  //   width: wdgtWidth * 0.30,
                  //   height: wdgtHeight * 0.30,
                  //   child: Column(
                  //     mainAxisAlignment: MainAxisAlignment.center,
                  //     crossAxisAlignment: CrossAxisAlignment.start,
                  //     children: [
                  //       MyText('Last Purchase Partner',
                  //           style: TextStyle(
                  //               fontSize: Pallet.heading6,
                  //               fontWeight: Pallet.font500,
                  //               color: Pallet.fontcolor)),
                  //       SizedBox(height: 10),
                  //       MyText('5',
                  //           overflow: TextOverflow.ellipsis,
                  //           style: TextStyle(
                  //               fontSize: Pallet.heading4,
                  //               fontWeight: Pallet.font500,
                  //               color: Pallet.fontcolor)),
                  //     ],
                  //   ),
                  // ),
                  // Container(
                  //   padding: EdgeInsets.symmetric(horizontal: 15),
                  //   decoration: BoxDecoration(
                  //       color: Pallet.dashsmallcontainerback,
                  //       borderRadius: BorderRadius.circular(10)),
                  //   width: wdgtWidth * 0.30,
                  //   height: wdgtHeight * 0.30,
                  //   child: Column(
                  //     mainAxisAlignment: MainAxisAlignment.center,
                  //     crossAxisAlignment: CrossAxisAlignment.start,
                  //     children: [
                  //       MyText('Rank Qualifying Partner',
                  //           style: TextStyle(
                  //               fontSize: Pallet.heading6,
                  //               fontWeight: Pallet.font500,
                  //               color: Pallet.fontcolor)),
                  //       SizedBox(height: 10),
                  //       MyText('0',
                  //           style: TextStyle(
                  //               fontSize: Pallet.heading4,
                  //               fontWeight: Pallet.font500,
                  //               color: Pallet.fontcolor)),
                  //     ],
                  //   ),
                  // ),
                  // Container(
                  //   padding: EdgeInsets.symmetric(horizontal: 15),
                  //   decoration: BoxDecoration(
                  //       color: Pallet.dashsmallcontainerback,
                  //       borderRadius: BorderRadius.circular(10)),
                  //   width: wdgtWidth * 0.30,
                  //   height: wdgtHeight * 0.30,
                  //   child: Column(
                  //     mainAxisAlignment: MainAxisAlignment.center,
                  //     crossAxisAlignment: CrossAxisAlignment.start,
                  //     children: [
                  //       MyText('Latest Partner Registered',
                  //           style: TextStyle(
                  //               fontSize: Pallet.heading6,
                  //               fontWeight: Pallet.font500,
                  //               color: Pallet.fontcolor)),
                  //       SizedBox(height: 10),
                  //       MyText('Shyam',
                  //           overflow: TextOverflow.ellipsis,
                  //           style: TextStyle(
                  //               fontSize: Pallet.heading4,
                  //               fontWeight: Pallet.font500,
                  //               color: Pallet.fontcolor)),
                  //     ],
                  //   ),
                  // ),
                  // Container(
                  //   padding: EdgeInsets.symmetric(horizontal: 15),
                  //   decoration: BoxDecoration(
                  //       color: Pallet.dashsmallcontainerback,
                  //       borderRadius: BorderRadius.circular(10)),
                  //   width: wdgtWidth * 0.30,
                  //   height: wdgtHeight * 0.30,
                  //   child: Column(
                  //     mainAxisAlignment: MainAxisAlignment.center,
                  //     crossAxisAlignment: CrossAxisAlignment.start,
                  //     children: [
                  //       MyText('New Partners in Waiting Room',
                  //           style: TextStyle(
                  //               fontSize: Pallet.heading6,
                  //               fontWeight: Pallet.font500,
                  //               color: Pallet.fontcolor)),
                  //       SizedBox(height: 10),
                  //       MyText('8',
                  //           style: TextStyle(
                  //               fontSize: Pallet.heading4,
                  //               fontWeight: Pallet.font500,
                  //               color: Pallet.fontcolor)),
                  //     ],
                  //   ),
                  // ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  Container bonuschart() {
    return Container(
      // width: wdgtWidth * 0.6,
      // height: wdgtHeight * 0.888,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        children: [
          SfCartesianChart(
              legend: Legend(
                  // title: LegendTitle(
                  //     MyText: 'Country',
                  //     textStyle: ChartTextStyle(
                  //       color: Pallet.fontcolor,
                  //       fontSize: 10,
                  //     )),
                  isVisible: true,
                  iconWidth: 20,
                  padding: 10,
                  iconHeight: 20,
                  overflowMode: LegendItemOverflowMode.wrap,
                  alignment: ChartAlignment.center,
                  position: LegendPosition.bottom,
                  // backgroundColor: Colors.green,
                  // iconBorderColor: Pallet.fontcolornew,
                  textStyle: TextStyle(
                      color: Pallet.fontcolor,
                      fontFamily: 'Montserrat',
                      fontSize: Pallet.heading7),
                  borderColor: Pallet.fontcolor,
                  borderWidth: 0),
              tooltipBehavior: TooltipBehavior(
                  enable: true,
                  tooltipPosition: TooltipPosition.pointer,
                  animationDuration: 350,
                  elevation: 20,
                  duration: 3000,
                  shouldAlwaysShow: true,
                  canShowMarker: true),
              enableAxisAnimation: true,
              primaryXAxis: CategoryAxis(
                majorGridLines: MajorGridLines(width: 0),
                labelStyle: TextStyle(
                  fontFamily: 'Montserrat',
                  color: Pallet.fontcolor,
                  fontSize: Pallet.heading7,
                  fontWeight: Pallet.font500,
                ),
              ),
              primaryYAxis: NumericAxis(
                majorGridLines: MajorGridLines(width: 0),
                labelStyle: TextStyle(
                  fontFamily: 'Montserrat',
                  color: Pallet.fontcolor,
                  fontSize: Pallet.heading7,
                  fontWeight: Pallet.font500,
                ),
              ),
              series: <ChartSeries>[
                ColumnSeries<Map, String>(
                  name: dashboard.pageDetails["text109"],
                  dataLabelSettings: DataLabelSettings(
                    isVisible: true,
                    useSeriesColor: true,
                    showZeroValue: false,
                    color: Pallet.fontcolor,
                    alignment: ChartAlignment.center,
                    // labelPosition: ChartDataLabelPosition.inside,
                  ),
                  isTrackVisible: true,
                  trackColor: Pallet.bonuscolor,
                  gradient: Pallet.linearGradient,
                  animationDuration: 3000,
                  color: Pallet.dashcontainerback,
                  dataSource: dashboard.bonuschart,
                  xValueMapper: (Map bonuschart, _) =>
                      bonuschart["bonuscategory"],
                  yValueMapper: (Map bonuschart, _) => bonuschart["data"],
                )
              ]),
          Padding(
            padding: const EdgeInsets.only(left: 10),
            child: Wrap(
              spacing: 5,
              runSpacing: 5,
              children: [
                MyText(
                    text: dashboard.pageDetails["text110"],
                    style: TextStyle(
                      fontSize: Pallet.heading7,
                      color: Pallet.fontcolor,
                    )),
                MyText(
                    text: dashboard.pageDetails["text111"],
                    style: TextStyle(
                      fontSize: Pallet.heading7,
                      color: Pallet.fontcolor,
                    )),
                MyText(
                    text: dashboard.pageDetails["text112"],
                    style: TextStyle(
                      fontSize: Pallet.heading7,
                      color: Pallet.fontcolor,
                    )),
                MyText(
                    text: dashboard.pageDetails["text113"],
                    style: TextStyle(
                      fontSize: Pallet.heading7,
                      color: Pallet.fontcolor,
                    )),
                MyText(
                    text: dashboard.pageDetails["text114"],
                    style: TextStyle(
                      fontSize: Pallet.heading7,
                      color: Pallet.fontcolor,
                    )),
              ],
            ),
          )
        ],
      ),
    );
  }

  Widget bonuscontainer({double wdgtWidth, double wdgtHeight}) {
    return Container(
      // padding: EdgeInsets.only(left: 10),
      width: wdgtWidth * 0.33,
      // height: wdgtHeight * 0.888,
      child: Wrap(
        runSpacing: 5,
        spacing: 5,
        children: [
          for (var data in dashboard.mybonus2)
            Container(
              margin: EdgeInsets.only(bottom: 5),
              padding: EdgeInsets.only(left: 15),
              decoration: BoxDecoration(
                  color: Pallet.dashsmallcontainerback,
                  borderRadius: BorderRadius.circular(10)),
              width: wdgtWidth * 0.25,
              height: wdgtHeight * 0.2,
              child: Stack(
                children: [
                  Positioned(
                    right: 10,
                    top: 10,
                    child: data['eligible'].toString().toLowerCase() == 'true'
                        ? Container(
                            decoration: BoxDecoration(
                                color: Color(0xFF41ad49),
                                borderRadius: BorderRadius.circular(50)),
                            width: 15,
                            height: 15,
                            child: Icon(Icons.done,
                                color: Pallet.fontcolor, size: 10))
                        : Container(
                            decoration: BoxDecoration(
                                color: Pallet.error,
                                borderRadius: BorderRadius.circular(50)),
                            width: 9,
                            height: 9,
                          ),
                    // child: Container(
                    //   decoration: BoxDecoration(
                    //       color: data['eligible'] == 'true'
                    //           ? Pallet.success
                    //           : Pallet.error,
                    //       borderRadius: BorderRadius.circular(10)),
                    //   width: 8,
                    //   height: 8,
                    // ),
                  ),
                  Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      if (data['key'].toString() == 'fast_start_bonus')
                        MyText(
                            text: dashboard.pageDetails["text162"],
                            style: TextStyle(
                                fontSize: Pallet.heading6,
                                fontWeight: Pallet.font500,
                                color: Pallet.fontcolor)),
                      if (data['key'].toString() == 'ruler_start_bonus')
                        MyText(
                            text: dashboard.pageDetails["text163"],
                            style: TextStyle(
                                fontSize: Pallet.heading6,
                                fontWeight: Pallet.font500,
                                color: Pallet.fontcolor)),
                      if (data['key'].toString() == 'world_start_bonus')
                        MyText(
                            text: dashboard.pageDetails["text164"],
                            style: TextStyle(
                                fontSize: Pallet.heading6,
                                fontWeight: Pallet.font500,
                                color: Pallet.fontcolor)),
                      SizedBox(height: 10),
                      MyText(
                          text: data['amount'].toString(),
                          style: TextStyle(
                              fontSize: Pallet.heading4,
                              fontWeight: Pallet.font500,
                              color: Pallet.fontcolor)),
                    ],
                  ),
                ],
              ),
            ),
        ],
      ),
    );
  }

  Widget bonus({double wdgtWidth, double wdgtHeight}) {
    return Container(
      decoration: BoxDecoration(
          boxShadow: [Pallet.shadowEffect],
          color: Pallet.dashcontainerback,
          borderRadius: BorderRadius.circular(10)),
      width: wdgtWidth,
      height: wdgtHeight,
      child: Padding(
        padding: EdgeInsets.fromLTRB(wdgtWidth * 0.03, 20, wdgtWidth * 0.03, 5),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          // mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          // mainAxisSize: MainAxisSize.min,
          children: [
            Row(
              children: [
                MyText(
                  text: dashboard.pageDetails["text72"] +
                      ' - ' +
                      dashboard.pageDetails["text108"] +
                      ' ' +
                      ('\$ ' + dashboard.mybonus3['total'].toString()),
                  // 'hello',
                  style: TextStyle(
                      fontSize: Pallet.heading3,
                      fontWeight: Pallet.font500,
                      color: Pallet.fontcolor),
                ),
              ],
            ),
            Expanded(
              child: Center(
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Expanded(
                      flex: 3,
                      child: bonuschart(),
                    ),
                    Expanded(
                      flex: 1,
                      child: bonuscontainer(
                        wdgtHeight: wdgtHeight,
                        wdgtWidth: wdgtWidth,
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget bonusmobile({double wdgtWidth, double wdgtHeight}) {
    return Container(
      decoration: BoxDecoration(
          boxShadow: [Pallet.shadowEffect],
          color: Pallet.dashcontainerback,
          borderRadius: BorderRadius.circular(20)),
      width: wdgtWidth,
      height: wdgtHeight,
      child: Column(
        children: [
          Padding(
            padding: const EdgeInsets.only(left: 20.0, bottom: 5.0, top: 20.0),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Expanded(
                  child: MyText(
                    text: dashboard.pageDetails["text72"] +
                        ' - ' +
                        dashboard.pageDetails["text108"] +
                        ' ' +
                        ('\$ ' + dashboard.mybonus3['total'].toString()),
                    style: new TextStyle(
                        fontSize: Pallet.heading3,
                        fontWeight: Pallet.font500,
                        color: Pallet.fontcolor),
                  ),
                ),
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(5.0),
            child: SfCartesianChart(
                legend: Legend(
                    // title: LegendTitle(
                    //     MyText: 'Country',
                    //     textStyle: ChartTextStyle(
                    //       color: Pallet.fontcolor,
                    //       fontSize: 10,
                    //     )),
                    isVisible: true,
                    iconWidth: 20,
                    padding: 10,
                    iconHeight: 20,
                    overflowMode: LegendItemOverflowMode.wrap,
                    alignment: ChartAlignment.center,
                    position: LegendPosition.bottom,
                    // backgroundColor: Colors.green,
                    // iconBorderColor: Pallet.fontcolornew,
                    textStyle: TextStyle(
                        color: Pallet.fontcolor,
                        fontFamily: 'Montserrat',
                        fontSize: Pallet.heading7),
                    borderColor: Pallet.fontcolor,
                    borderWidth: 0),
                tooltipBehavior: TooltipBehavior(
                    enable: true,
                    tooltipPosition: TooltipPosition.pointer,
                    animationDuration: 350,
                    elevation: 20,
                    duration: 3000,
                    shouldAlwaysShow: true,
                    canShowMarker: true),
                enableAxisAnimation: true,
                primaryXAxis: CategoryAxis(
                  majorGridLines: MajorGridLines(width: 0),
                  labelStyle: TextStyle(
                    fontFamily: 'Montserrat',
                    color: Pallet.fontcolor,
                    fontSize: Pallet.heading7,
                    fontWeight: Pallet.font500,
                  ),
                ),
                primaryYAxis: NumericAxis(
                  majorGridLines: MajorGridLines(width: 0),
                  labelStyle: TextStyle(
                    fontFamily: 'Montserrat',
                    color: Pallet.fontcolor,
                    fontSize: Pallet.heading7,
                    fontWeight: Pallet.font500,
                  ),
                ),
                series: <ChartSeries>[
                  ColumnSeries<Map, String>(
                    name: dashboard.pageDetails["text109"],
                    dataLabelSettings: DataLabelSettings(
                      isVisible: true,
                      useSeriesColor: true,
                      showZeroValue: false,
                      color: Pallet.fontcolor,
                      alignment: ChartAlignment.center,
                      // labelPosition: ChartDataLabelPosition.inside,
                    ),
                    isTrackVisible: true,
                    trackColor: Pallet.bonuscolor,
                    gradient: Pallet.linearGradient,
                    animationDuration: 3000,
                    color: Pallet.dashcontainerback,
                    dataSource: dashboard.bonuschart,
                    xValueMapper: (Map bonuschart, _) =>
                        bonuschart["bonuscategory"],
                    yValueMapper: (Map bonuschart, _) => bonuschart["data"],
                  )
                ]),
          ),
          Padding(
            padding: const EdgeInsets.all(5.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Wrap(
                  spacing: 8,
                  runSpacing: 8,
                  children: [
                    MyText(
                        text: dashboard.pageDetails["text110"],
                        style: TextStyle(
                          fontSize: Pallet.heading7,
                          color: Pallet.fontcolor,
                        )),
                    MyText(
                        text: dashboard.pageDetails["text111"],
                        style: TextStyle(
                          fontSize: Pallet.heading7,
                          color: Pallet.fontcolor,
                        )),
                    MyText(
                        text: dashboard.pageDetails["text112"],
                        style: TextStyle(
                          fontSize: Pallet.heading7,
                          color: Pallet.fontcolor,
                        )),
                    MyText(
                        text: dashboard.pageDetails["text113"],
                        style: TextStyle(
                          fontSize: Pallet.heading7,
                          color: Pallet.fontcolor,
                        )),
                    MyText(
                        text: dashboard.pageDetails["text114"],
                        style: TextStyle(
                          fontSize: Pallet.heading7,
                          color: Pallet.fontcolor,
                        )),
                  ],
                ),
              ],
            ),
          ),
          SizedBox(height: 5),
          Container(
            padding: EdgeInsets.only(left: 15),
            width: wdgtWidth,
            child: Wrap(
              runSpacing: 5,
              spacing: 5,
              children: [
                for (var data in dashboard.mybonus2)
                  Container(
                    margin: EdgeInsets.only(bottom: 5),
                    padding: EdgeInsets.only(left: 15),
                    decoration: BoxDecoration(
                        color: Pallet.dashsmallcontainerback,
                        borderRadius: BorderRadius.circular(10)),
                    width: wdgtWidth * 0.43,
                    height: wdgtHeight * 0.18,
                    child: Stack(
                      children: [
                        Positioned(
                          right: 10,
                          top: 10,
                          child: data['eligible'].toString().toLowerCase() ==
                                  'true'
                              ? Container(
                                  decoration: BoxDecoration(
                                      color: Color(0xFF41ad49),
                                      borderRadius: BorderRadius.circular(50)),
                                  width: 15,
                                  height: 15,
                                  child: Icon(Icons.done,
                                      color: Pallet.fontcolor, size: 10))
                              : Container(
                                  decoration: BoxDecoration(
                                      color: Pallet.error,
                                      borderRadius: BorderRadius.circular(50)),
                                  width: 9,
                                  height: 9,
                                ),
                        ),
                        Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            if (data['key'].toString() == 'fast_start_bonus')
                              MyText(
                                  text: dashboard.pageDetails["text162"],
                                  style: TextStyle(
                                      fontSize: Pallet.heading6,
                                      fontWeight: Pallet.font500,
                                      color: Pallet.fontcolor)),
                            if (data['key'].toString() == 'ruler_start_bonus')
                              MyText(
                                  text: dashboard.pageDetails["text163"],
                                  style: TextStyle(
                                      fontSize: Pallet.heading6,
                                      fontWeight: Pallet.font500,
                                      color: Pallet.fontcolor)),
                            if (data['key'].toString() == 'world_start_bonus')
                              MyText(
                                  text: dashboard.pageDetails["text164"],
                                  style: TextStyle(
                                      fontSize: Pallet.heading6,
                                      fontWeight: Pallet.font500,
                                      color: Pallet.fontcolor)),
                            SizedBox(height: 10),
                            MyText(
                                text: data['amount'].toString(),
                                style: TextStyle(
                                    fontSize: Pallet.heading4,
                                    fontWeight: Pallet.font500,
                                    color: Pallet.fontcolor)),
                          ],
                        ),
                      ],
                    ),
                  ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget performance({
    double wdgtWidth,
    double wdgtHeight,
  }) {
    return Container(
      decoration: BoxDecoration(
          boxShadow: [Pallet.shadowEffect],
          color: Pallet.dashcontainerback,
          borderRadius: BorderRadius.circular(10)),
      width: wdgtWidth,
      height: wdgtHeight,
      child: Column(
        children: [
          Padding(
            padding: const EdgeInsets.only(left: 20.0, bottom: 5.0, top: 20.0),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                MyText(
                  text: dashboard.pageDetails["text73"],
                  style: new TextStyle(
                      fontSize: Pallet.heading3,
                      fontWeight: Pallet.font500,
                      color: Pallet.fontcolor),
                ),
              ],
            ),
          ),
          Padding(
              padding: const EdgeInsets.all(10.0),
              child: SfCartesianChart(
                  enableAxisAnimation: true,
                  legend: Legend(
                      // title: LegendTitle(
                      //     MyText: 'Country',
                      //     textStyle: ChartTextStyle(
                      //         color: Colors.red,
                      //         fontSize: 15,
                      //         fontStyle: FontStyle.italic,
                      //         fontWeight: FontWeight.w900)),
                      isVisible: true,
                      iconWidth: 20,
                      padding: 10,
                      // itemPadding: 20,
                      // iconHeight: 20,
                      overflowMode: LegendItemOverflowMode.wrap,
                      alignment: ChartAlignment.center,
                      position: LegendPosition.bottom,
                      // backgroundColor: Colors.green,
                      // iconBorderColor: Pallet.fontcolornew,
                      textStyle: TextStyle(
                        color: Pallet.fontcolor,
                        fontFamily: 'Montserrat',
                        fontSize: Pallet.heading7,
                      ),
                      // Border color and border width of legend
                      borderColor: Pallet.fontcolor,
                      borderWidth: 0),
                  tooltipBehavior: TooltipBehavior(
                      enable: true,
                      tooltipPosition: TooltipPosition.pointer,
                      animationDuration: 350,
                      elevation: 20,
                      duration: 3000,
                      shouldAlwaysShow: true,
                      canShowMarker: true),
                  primaryXAxis: CategoryAxis(
                    title: AxisTitle(
                        text: dashboard.pageDetails["text115"],
                        textStyle: TextStyle(
                          fontFamily: 'Montserrat',
                          color: Pallet.fontcolor,
                          fontSize: Pallet.heading7,
                        )),
                    majorGridLines: MajorGridLines(width: 0),
                    labelStyle: TextStyle(
                      color: Pallet.fontcolor,
                      fontSize: Pallet.heading7,
                    ),
                  ),
                  primaryYAxis: NumericAxis(
                    title: AxisTitle(
                        text: dashboard.pageDetails["text116"],
                        textStyle: TextStyle(
                          fontFamily: 'Montserrat',
                          color: Pallet.fontcolor,
                          fontSize: Pallet.heading7,
                        )),
                    isVisible: true,
                    majorGridLines: MajorGridLines(width: 0),
                    labelStyle: TextStyle(
                      color: Pallet.fontcolor,
                      fontSize: Pallet.heading7,
                      fontFamily: 'Montserrat',
                    ),
                  ),
                  series: <ChartSeries>[
                    SplineAreaSeries<dynamic, dynamic>(
                        name: dashboard.pageDetails["text117"],
                        animationDuration: 1500,
                        borderWidth: 2.5,
                        enableTooltip: true,
                        dataSource: dashboard.performancechart1,
                        xValueMapper: (dynamic performancechart1, _) =>
                            performancechart1['category'],
                        gradient: Pallet.performanceGradient1,
                        borderColor: Color(0XFF1269e9),
                        yValueMapper: (dynamic performancechart1, _) =>
                            performancechart1['data']),
                    SplineAreaSeries<dynamic, dynamic>(
                        name: dashboard.pageDetails["text118"],
                        borderWidth: 2.5,
                        borderColor: Color(0XFF1269e9),
                        enableTooltip: true,
                        dataSource: dashboard.performancechart2,
                        xValueMapper: (dynamic performancechart2, _) =>
                            performancechart2['category'],
                        gradient: Pallet.performanceGradient2,
                        yValueMapper: (dynamic performancechart2, _) =>
                            performancechart2['data']),
                    SplineAreaSeries<dynamic, dynamic>(
                        name: dashboard.pageDetails["text119"],
                        dataSource: dashboard.performancechart3,
                        xValueMapper: (dynamic performancechart3, _) =>
                            performancechart3['category'],
                        borderColor: Color(0XFFbabcc1),
                        borderWidth: 2.5,
                        enableTooltip: true,
                        gradient: Pallet.performanceGradient3,
                        yValueMapper: (dynamic performancechart3, _) =>
                            performancechart3['data']),
                  ])),
        ],
      ),
    );
  }

  Widget performancemobile({
    double wdgtWidth,
    double wdgtHeight,
  }) {
    return Container(
      decoration: BoxDecoration(
          boxShadow: [Pallet.shadowEffect],
          color: Pallet.dashcontainerback,
          borderRadius: BorderRadius.circular(20)),
      width: wdgtWidth,
      height: wdgtHeight,
      child: Column(
        children: [
          Padding(
            padding: const EdgeInsets.only(left: 20.0, bottom: 5.0, top: 20.0),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                MyText(
                  text: dashboard.pageDetails["text73"],
                  style: new TextStyle(
                      fontSize: Pallet.heading3,
                      fontWeight: Pallet.font500,
                      color: Pallet.fontcolor),
                ),
              ],
            ),
          ),
          Padding(
              padding: const EdgeInsets.all(10.0),
              child: SfCartesianChart(
                  enableAxisAnimation: true,
                  legend: Legend(
                      // title: LegendTitle(
                      //     MyText: 'Country',
                      //     textStyle: ChartTextStyle(
                      //         color: Colors.red,
                      //         fontSize: 15,
                      //         fontStyle: FontStyle.italic,
                      //         fontWeight: FontWeight.w900)),
                      isVisible: true,
                      iconWidth: 20,
                      padding: 10,
                      // itemPadding: 20,
                      // iconHeight: 20,
                      overflowMode: LegendItemOverflowMode.wrap,
                      alignment: ChartAlignment.center,
                      position: LegendPosition.bottom,
                      // backgroundColor: Colors.green,
                      // iconBorderColor: Pallet.fontcolornew,
                      textStyle: TextStyle(
                        color: Pallet.fontcolor,
                        fontFamily: 'Montserrat',
                        fontSize: Pallet.heading7,
                      ),
                      // Border color and border width of legend
                      borderColor: Pallet.fontcolor,
                      borderWidth: 0),
                  tooltipBehavior: TooltipBehavior(
                      enable: true,
                      tooltipPosition: TooltipPosition.pointer,
                      animationDuration: 350,
                      elevation: 20,
                      duration: 3000,
                      shouldAlwaysShow: true,
                      canShowMarker: true),
                  primaryXAxis: CategoryAxis(
                    title: AxisTitle(
                        text: dashboard.pageDetails["text115"],
                        textStyle: TextStyle(
                          fontFamily: 'Montserrat',
                          color: Pallet.fontcolor,
                          fontSize: Pallet.heading6,
                        )),
                    majorGridLines: MajorGridLines(width: 0),
                    labelStyle: TextStyle(
                      color: Pallet.fontcolor,
                      fontSize: Pallet.heading7,
                    ),
                  ),
                  primaryYAxis: NumericAxis(
                    title: AxisTitle(
                        text: dashboard.pageDetails["text116"],
                        textStyle: TextStyle(
                          fontFamily: 'Montserrat',
                          color: Pallet.fontcolor,
                          fontSize: Pallet.heading7,
                        )),
                    isVisible: true,
                    majorGridLines: MajorGridLines(width: 0),
                    labelStyle: TextStyle(
                      color: Pallet.fontcolor,
                      fontSize: Pallet.heading7,
                      fontFamily: 'Montserrat',
                    ),
                  ),
                  series: <ChartSeries>[
                    SplineAreaSeries<dynamic, dynamic>(
                        name: dashboard.pageDetails["text117"],
                        animationDuration: 1500,
                        borderWidth: 2.5,
                        enableTooltip: true,
                        dataSource: dashboard.performancechart1,
                        xValueMapper: (dynamic performancechart1, _) =>
                            performancechart1['category'],
                        gradient: Pallet.performanceGradient1,
                        borderColor: Color(0XFF1269e9),
                        yValueMapper: (dynamic performancechart1, _) =>
                            performancechart1['data']),
                    SplineAreaSeries<dynamic, dynamic>(
                        name: dashboard.pageDetails["text118"],
                        borderWidth: 2.5,
                        borderColor: Color(0XFF1269e9),
                        enableTooltip: true,
                        dataSource: dashboard.performancechart2,
                        xValueMapper: (dynamic performancechart2, _) =>
                            performancechart2['category'],
                        gradient: Pallet.performanceGradient2,
                        yValueMapper: (dynamic performancechart2, _) =>
                            performancechart2['data']),
                    SplineAreaSeries<dynamic, dynamic>(
                        name: dashboard.pageDetails["text119"],
                        dataSource: dashboard.performancechart3,
                        xValueMapper: (dynamic performancechart3, _) =>
                            performancechart3['category'],
                        borderColor: Color(0XFFbabcc1),
                        borderWidth: 2.5,
                        enableTooltip: true,
                        gradient: Pallet.performanceGradient3,
                        yValueMapper: (dynamic performancechart3, _) =>
                            performancechart3['data']),
                  ])),
        ],
      ),
    );
  }

  Widget priceofcoin({double wdgtWidth, double wdgtHeight, double popheading}) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
      decoration: BoxDecoration(
          boxShadow: [Pallet.shadowEffect],
          color: Pallet.dashcontainerback,
          borderRadius: BorderRadius.circular(10)),
      width: wdgtWidth,
      height: wdgtHeight,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              CircleAvatar(
                backgroundColor: Pallet.dashsmallcontainerback,
                child: Image.asset('c-logo.png', width: 30, fit: BoxFit.cover),
                radius: 25,
              ),
              SizedBox(width: 10),
              MyText(
                text: dashboard.ducprice == null
                    ? '-'
                    : dashboard.ducprice.toString(),
                // dashboard.mypriceofcoin['price_of_coin'] == null
                //     ? ""
                //     : dashboard.mypriceofcoin['price_of_coin'],
                style: new TextStyle(
                    fontWeight: Pallet.font500,
                    fontSize: Pallet.subheading1,
                    color: Pallet.fontcolor),
              ),
              SizedBox(width: 10),
              MyText(
                text: dashboard.pageDetails["text20"],
                style: new TextStyle(
                    fontSize: Pallet.heading3,
                    fontWeight: Pallet.font500,
                    color: Pallet.fontcolor),
              ),
            ],
          ),
          SizedBox(height: 70),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                width: dashboard.accountType == 2
                    ? wdgtWidth * 0.585
                    : wdgtWidth * 0.4,
                child: Wrap(
                  runSpacing: 5,
                  spacing: 5,
                  children: [
                    for (var data in dashboard.mypriceofcoin['boxes'])
                      Container(
                        padding: EdgeInsets.symmetric(horizontal: 15),
                        decoration: BoxDecoration(
                            color: Pallet.dashsmallcontainerback,
                            borderRadius: BorderRadius.circular(10)),
                        width: dashboard.accountType == 2
                            ? wdgtWidth * 0.25
                            : wdgtWidth * 0.60,
                        height: wdgtHeight * 0.25,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            if (data['key'].toString() == 'duc_in_vault')
                              MyText(
                                  text: dashboard.pageDetails["text165"],
                                  style: TextStyle(
                                      fontSize: Pallet.heading6,
                                      fontWeight: Pallet.font500,
                                      color: Pallet.fontcolor)),
                            if (data['key'].toString() == 'cct')
                              MyText(
                                  text: dashboard.pageDetails["text166"],
                                  style: TextStyle(
                                      fontSize: Pallet.heading6,
                                      fontWeight: Pallet.font500,
                                      color: Pallet.fontcolor)),
                            if (data['key'].toString() == 'bonus_credits')
                              MyText(
                                  text: dashboard.pageDetails["text167"],
                                  style: TextStyle(
                                      fontSize: Pallet.heading6,
                                      fontWeight: Pallet.font500,
                                      color: Pallet.fontcolor)),
                            if (data['key'].toString() == 'aap')
                              MyText(
                                  text: dashboard.pageDetails["text168"],
                                  style: TextStyle(
                                      fontSize: Pallet.heading6,
                                      fontWeight: Pallet.font500,
                                      color: Pallet.fontcolor)),
                            if (data['key'].toString() == 'dsv_earned')
                              MyText(
                                  text: dashboard.pageDetails["text176"],
                                  style: TextStyle(
                                      fontSize: Pallet.heading6,
                                      fontWeight: Pallet.font500,
                                      color: Pallet.fontcolor)),
                            SizedBox(height: 10),
                            MyText(
                                text: data['content'].toString(),
                                style: TextStyle(
                                    fontSize: Pallet.heading4,
                                    fontWeight: Pallet.font500,
                                    color: Pallet.fontcolor)),
                          ],
                        ),
                      ),
                  ],
                ),
              ),
              Container(
                width: dashboard.accountType == 2
                    ? wdgtWidth * 0.35
                    : wdgtWidth * 0.4,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    if (dashboard.accountType == 2)
                      InkWell(
                        onTap: dashboard.accountType == 2
                            ? () {
                                showDialog(
                                  context: context,
                                  builder: (BuildContext context) {
                                    return AlertDialog(
                                      elevation: 24.0,
                                      backgroundColor:
                                          Pallet.popupcontainerback,
                                      shape: RoundedRectangleBorder(
                                          borderRadius: BorderRadius.all(
                                              Radius.circular(15.0))),
                                      title: Row(
                                        children: [
                                          Image.asset("c-logo.png", width: 40),
                                          SizedBox(width: 10),
                                          MyText(
                                              text: dashboard
                                                  .pageDetails["text27"],
                                              style: TextStyle(
                                                  fontSize: popheading,
                                                  color: Pallet.fontcolor,
                                                  fontWeight: Pallet.font500)),
                                        ],
                                      ),
                                      content: Container(
                                        width: 450.0,
                                        child: SingleChildScrollView(
                                          child: ListBody(
                                            children: <Widget>[
                                              Center(
                                                  child: ducvaultpopup(
                                                      popheading: popheading)),
                                            ],
                                          ),
                                        ),
                                      ),
                                      actions: [
                                        PopupButton(
                                          text: dashboard.pageDetails["text78"],
                                          onpress: () {
                                            Navigator.of(context).pop();
                                          },
                                        ),
                                        SizedBox(height: 10),
                                      ],
                                    );
                                  },
                                );
                              }
                            : () {},
                        child: Container(
                          child: Row(
                            children: [
                              MyText(
                                  text: dashboard.accountType == 2
                                      ? dashboard.pageDetails["text24"]
                                      : dashboard.pageDetails["text4"],
                                  style: TextStyle(
                                      fontSize: Pallet.heading4,
                                      fontWeight: Pallet.font500,
                                      color: Pallet.fontcolor)),
                              SizedBox(width: 5.0),
                              Icon(
                                Icons.arrow_forward,
                                color: Pallet.fontcolor,
                                size: 18.0,
                              ),
                            ],
                          ),
                        ),
                      ),
                    SizedBox(height: dashboard.accountType == 2 ? 20 : 0),
                    if (dashboard.accountType == 2)
                      InkWell(
                        onTap: () {
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) =>
                                    Home(route: 'request_payout')),
                          );
                        },
                        child: Container(
                          child: Row(
                            children: [
                              MyText(
                                  text: dashboard.pageDetails["text25"],
                                  style: TextStyle(
                                      fontSize: Pallet.heading4,
                                      fontWeight: Pallet.font500,
                                      color: Pallet.fontcolor)),
                              SizedBox(width: 5.0),
                              Icon(
                                Icons.arrow_forward,
                                color: Pallet.fontcolor,
                                size: 18.0,
                              ),
                            ],
                          ),
                        ),
                      ),
                    SizedBox(height: 20.0),
                    //Showing only Customer
                    if (dashboard.accountType == 2 ||
                        dashboard.accountType == 1)
                      CustomButton(
                        vpadding: 10,
                        width: 150,
                        text: dashboard.pageDetails["text26"],
                        fontweight: Pallet.subheading1wgt,
                        textcolor: Pallet.fontcolornew,
                        textsize: Pallet.heading6,
                        onpress: () async {
                          String ducError;
                          await showDialog(
                              context: context,
                              builder: (BuildContext context) {
                                return StatefulBuilder(
                                    builder: (BuildContext context,
                                            void Function(void Function())
                                                setState1) =>
                                        AlertDialog(
                                          elevation: 24.0,
                                          backgroundColor:
                                              Pallet.popupcontainerback,
                                          shape: RoundedRectangleBorder(
                                              borderRadius: BorderRadius.all(
                                                  Radius.circular(15.0))),
                                          title: Row(
                                            children: [
                                              Image.asset("c-logo.png",
                                                  width: 40),
                                              SizedBox(width: 10),
                                              MyText(
                                                  text: dashboard
                                                      .pageDetails["text120"],
                                                  style: TextStyle(
                                                      fontSize: popheading,
                                                      color: Pallet.fontcolor,
                                                      fontWeight:
                                                          Pallet.font500)),
                                            ],
                                          ),
                                          content: Container(
                                            width: 400.0,
                                            child: SingleChildScrollView(
                                              child: ListBody(
                                                children: <Widget>[
                                                  Column(
                                                    mainAxisAlignment:
                                                        MainAxisAlignment
                                                            .center,
                                                    children: [
                                                      MyText(
                                                        text: dashboard
                                                                .pageDetails[
                                                            "text46"],
                                                        style: TextStyle(
                                                            fontWeight:
                                                                Pallet.font500,
                                                            fontSize:
                                                                Pallet.heading2,
                                                            color: Pallet
                                                                .fontcolor),
                                                      ),
                                                      SizedBox(height: 20.0),
                                                      TextFormField(
                                                        controller: adddsv,
                                                        textAlign:
                                                            TextAlign.left,
                                                        keyboardType:
                                                            TextInputType
                                                                .number,
                                                        inputFormatters: <
                                                            TextInputFormatter>[
                                                          FilteringTextInputFormatter
                                                              .digitsOnly
                                                        ],
                                                        style: TextStyle(
                                                            color: Pallet
                                                                .fontcolor),
                                                        cursorColor:
                                                            Pallet.fontcolor,
                                                        decoration:
                                                            InputDecoration(
                                                          errorText: ducError,
                                                          border: OutlineInputBorder(
                                                              borderSide: BorderSide(
                                                                  color: Pallet
                                                                      .fontcolor,
                                                                  width: 2.0),
                                                              borderRadius:
                                                                  BorderRadius
                                                                      .circular(
                                                                          8.0)),
                                                          // labelText: '1 DSV = ' +
                                                          //     dashboard.ducprice
                                                          //         .toString(),
                                                          hintText: dashboard
                                                                  .pageDetails[
                                                              "text47"],
                                                          hintStyle: TextStyle(
                                                              fontSize: 14,
                                                              color: Color(
                                                                  0xFFb6c3db)),
                                                          fillColor:
                                                              Color(0xFF4570ae),
                                                          focusedBorder: OutlineInputBorder(
                                                              borderSide: BorderSide(
                                                                  color: Pallet
                                                                      .fontcolor,
                                                                  width: 2.0),
                                                              borderRadius:
                                                                  BorderRadius
                                                                      .circular(
                                                                          8.0)),
                                                          enabledBorder: OutlineInputBorder(
                                                              borderSide: BorderSide(
                                                                  color: Pallet
                                                                      .fontcolor,
                                                                  width: 2.0),
                                                              borderRadius:
                                                                  BorderRadius
                                                                      .circular(
                                                                          8.0)),
                                                          labelStyle: TextStyle(
                                                            color: Color(
                                                                0xFFb6c3db),
                                                            fontSize: 14,
                                                          ),
                                                        ),
                                                        validator: (input) {
                                                          final isDigitsOnly =
                                                              int.tryParse(
                                                                  input);
                                                          return isDigitsOnly ==
                                                                  null
                                                              ? dashboard
                                                                      .pageDetails[
                                                                  "text48"]
                                                              : null;
                                                        },
                                                        onChanged: (value) {
                                                          if (value.isEmpty) {
                                                            setState1(() {
                                                              ducError = dashboard
                                                                      .pageDetails[
                                                                  "text93"];
                                                            });
                                                          } else if (double
                                                                  .parse(
                                                                      value) ==
                                                              0) {
                                                            setState1(() {
                                                              ducError = dashboard
                                                                      .pageDetails[
                                                                  "text93"];
                                                            });
                                                          } else if (double
                                                                  .parse(
                                                                      value) >
                                                              dashboard
                                                                      .mypriceofcoin[
                                                                  'dsv_balance']) {
                                                            setState1(() {
                                                              ducError = dashboard
                                                                      .pageDetails[
                                                                  "text91"];
                                                            });
                                                          } else {
                                                            setState1(() {
                                                              ducError = null;
                                                            });
                                                          }
                                                        },
                                                      ),
                                                      SizedBox(height: 10.0),
                                                      Container(
                                                        width: double.infinity,
                                                        // ignore: deprecated_member_use
                                                        child: CustomButton(
                                                          onpress: () async {
                                                            if (adddsv
                                                                .text.isEmpty) {
                                                              setState1(() {
                                                                ducError = dashboard
                                                                        .pageDetails[
                                                                    "text93"];
                                                              });
                                                            } else if (double
                                                                    .parse(adddsv
                                                                        .text) ==
                                                                0) {
                                                              setState1(() {
                                                                ducError = dashboard
                                                                        .pageDetails[
                                                                    "text93"];
                                                              });
                                                            } else if (double
                                                                    .parse(adddsv
                                                                        .text) >
                                                                dashboard
                                                                        .mypriceofcoin[
                                                                    'dsv_balance']) {
                                                              setState1(() {
                                                                ducError = dashboard
                                                                        .pageDetails[
                                                                    "text91"];
                                                              });
                                                            } else {
                                                              Map<String,
                                                                      dynamic>
                                                                  _map = {
                                                                "product_id": 1,
                                                                "amount": double
                                                                    .parse(adddsv
                                                                        .text),
                                                              };
                                                              waiting.waitpopup(
                                                                  context);

                                                              Map<String,
                                                                      dynamic>
                                                                  result =
                                                                  await HttpRequest.Post(
                                                                      'redeemduc',
                                                                      Utils.constructPayload(
                                                                          _map));
                                                              print(
                                                                  '$result, region');
                                                              if (Utils
                                                                  .isServerError(
                                                                      result)) {
                                                                Navigator.pop(
                                                                    context);

                                                                var error = await Utils
                                                                    .getMessage(
                                                                        result['response']
                                                                            [
                                                                            'error']);
                                                                setState(() {
                                                                  ducError =
                                                                      error;
                                                                });
                                                                throw (error);
                                                              } else {
                                                                Navigator.pop(
                                                                    context);

                                                                print(result[
                                                                        'response']
                                                                    ['data']);
                                                                setState1(() {
                                                                  ducError =
                                                                      null;
                                                                  Navigator.of(
                                                                          context)
                                                                      .pop();
                                                                  return showDialog(
                                                                      barrierDismissible:
                                                                          false,
                                                                      context:
                                                                          context,
                                                                      builder:
                                                                          (BuildContext
                                                                              context) {
                                                                        return AlertDialog(
                                                                          backgroundColor:
                                                                              Pallet.popupcontainerback,
                                                                          shape:
                                                                              RoundedRectangleBorder(borderRadius: BorderRadius.circular(Pallet.radius)),
                                                                          title:
                                                                              Row(
                                                                            mainAxisAlignment:
                                                                                MainAxisAlignment.start,
                                                                            children: [
                                                                              Image.asset("c-logo.png", width: 30),
                                                                              SizedBox(width: 10),
                                                                              Expanded(
                                                                                child: MyText(
                                                                                  text: dashboard.pageDetails["text26"],
                                                                                  style: TextStyle(
                                                                                    fontSize: Pallet.normalfont + 10,
                                                                                    color: Pallet.fontcolor,
                                                                                    fontWeight: Pallet.font500,
                                                                                  ),
                                                                                ),
                                                                              ),
                                                                            ],
                                                                          ),
                                                                          content:
                                                                              Container(
                                                                            width:
                                                                                350,
                                                                            height:
                                                                                150,
                                                                            child:
                                                                                Column(
                                                                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                                                              children: [
                                                                                CircleAvatar(
                                                                                  backgroundColor: Colors.white,
                                                                                  radius: 30,
                                                                                  child: Icon(Icons.done, color: Pallet.dashcontainerback, size: 40),
                                                                                ),
                                                                                MyText(text: dashboard.pageDetails["text121"], textAlign: TextAlign.center, style: TextStyle(fontSize: Pallet.normalfont, color: Pallet.fontcolor, fontWeight: Pallet.font500, height: 1.5)),
                                                                              ],
                                                                            ),
                                                                          ),
                                                                          actions: [
                                                                            PopupButton(
                                                                              text: dashboard.pageDetails["text82"],
                                                                              onpress: () {
                                                                                Navigator.push(
                                                                                  context,
                                                                                  MaterialPageRoute(builder: (context) => Home(route: 'dashboard')),
                                                                                );
                                                                              },
                                                                            ),
                                                                          ],
                                                                        );
                                                                      });
                                                                });
                                                                return Utils.toJSONString(
                                                                    await Utils.convertMessage(
                                                                        result['response']
                                                                            [
                                                                            'data']));
                                                              }
                                                            }
                                                          },
                                                          vpadding: 12,
                                                          text: dashboard
                                                                  .pageDetails[
                                                              "text49"],
                                                          buttoncolor:
                                                              Pallet.fontcolor,
                                                          textcolor: Pallet
                                                              .fontcolornew,
                                                          textsize:
                                                              Pallet.heading4,
                                                        ),
                                                      ),
                                                    ],
                                                  ),
                                                ],
                                              ),
                                            ),
                                          ),
                                          actions: [
                                            PopupButton(
                                              text: dashboard
                                                  .pageDetails["text78"],
                                              onpress: () {
                                                Navigator.of(context).pop();
                                              },
                                            ),
                                            SizedBox(height: 10),
                                          ],
                                        ));
                              });
                        },
                      ),
                    SizedBox(height: 20.0),
                    if (dashboard.accountType == 2)
                      CustomButton(
                        vpadding: 10,
                        width: 180,
                        text: dashboard.pageDetails["text122"],
                        fontweight: Pallet.subheading1wgt,
                        textcolor: Pallet.fontcolornew,
                        textsize: Pallet.heading7,
                        onpress: () async {
                          await DSVPRedeem(context, setState);
                        },
                      ),
                    SizedBox(height: 20.0),
                    //Showing only Networker
                    if (dashboard.accountType == 2)
                      CustomButton(
                        vpadding: 10,
                        width: 150,
                        fontweight: Pallet.subheading1wgt,
                        textcolor: Pallet.fontcolornew,
                        textsize: Pallet.heading6,
                        child: Container(
                          child: Row(
                            // mainAxisAlignment: MainAxisAlignment.center,
                            // // crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Icon(
                                Icons.video_call,
                                color: Pallet.fontcolornew,
                                size: 15.0,
                              ),
                              SizedBox(width: 3.0),
                              Flexible(
                                child: MyText(
                                  text: dashboard.pageDetails["text2"],
                                  maxLines: 1,
                                  softWrap: false,
                                  // overflow: TextOverflow.ellipsis,
                                  isselectable: false,

                                  style: TextStyle(
                                      color: Pallet.fontcolornew,
                                      fontSize: Pallet.heading6,
                                      fontWeight: Pallet.subheading1wgt),
                                ),
                              ),
                            ],
                          ),
                        ),
                        onpress: () {
                          showDialog(
                            context: context,
                            builder: (BuildContext context) {
                              return AlertDialog(
                                elevation: 24.0,
                                backgroundColor: Pallet.popupcontainerback,
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.all(
                                        Radius.circular(15.0))),
                                title: Row(
                                  children: [
                                    Image.asset("c-logo.png", width: 40),
                                    SizedBox(width: 10),
                                    MyText(
                                        text: dashboard.pageDetails["text2"],
                                        style: TextStyle(
                                            fontSize: popheading,
                                            color: Pallet.fontcolor,
                                            fontWeight: Pallet.font500)),
                                  ],
                                ),
                                content: Container(
                                  width: 455.0,
                                  child: SingleChildScrollView(
                                    child: ListBody(
                                      children: <Widget>[
                                        MyText(
                                            text: dashboard
                                                .pageDetails["text123"],
                                            textAlign: TextAlign.left,
                                            style: TextStyle(
                                              fontSize: popheading / 1.03,
                                              height: 1.5,
                                              color: Pallet.fontcolor,
                                            )),
                                        Center(
                                            child: attendwebinarpopup(
                                                popheading: popheading)),
                                        MyText(
                                            text: dashboard
                                                .pageDetails["text124"],
                                            textAlign: TextAlign.left,
                                            style: TextStyle(
                                                fontSize: popheading / 1.03,
                                                color: Pallet.fontcolor,
                                                fontWeight: Pallet.font500)),
                                      ],
                                    ),
                                  ),
                                ),
                                actions: [
                                  Row(
                                    children: [
                                      PopupButton(
                                        onpress: () {
                                          launch(dashboard.zoomlink.toString());
                                        },
                                        child: Center(
                                          child: Row(
                                            children: [
                                              Icon(
                                                Icons.video_call,
                                                color: Pallet.fontcolornew,
                                                size: 18.0,
                                              ),
                                              SizedBox(width: 10.0),
                                              MyText(
                                                text: dashboard
                                                    .pageDetails["text2"],
                                                style: TextStyle(
                                                  fontSize: Pallet.heading6,
                                                  color: Pallet.fontcolornew,
                                                  fontWeight: Pallet.font500,
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                      ),
                                      SizedBox(width: 9),
                                      PopupButton(
                                        text: dashboard.pageDetails["text78"],
                                        onpress: () {
                                          Navigator.of(context).pop();
                                        },
                                      ),
                                      // SizedBox(height: 9),
                                    ],
                                  ),
                                ],
                              );
                            },
                          );
                        },
                      ),
                  ],
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }

  Widget priceofcoinmobile(
      {double wdgtWidth, double wdgtHeight, double popheading}) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 20),
      decoration: BoxDecoration(
          boxShadow: [Pallet.shadowEffect],
          color: Pallet.dashcontainerback,
          borderRadius: BorderRadius.circular(20)),
      width: wdgtWidth,
      height: wdgtHeight,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              CircleAvatar(
                backgroundColor: Pallet.dashsmallcontainerback,
                child: Image.asset('c-logo.png', width: 30, fit: BoxFit.cover),
                radius: 25,
              ),
              SizedBox(width: 10),
              MyText(
                text: dashboard.ducprice == null
                    ? '-'
                    : dashboard.ducprice.toString(),
                // dashboard.mypriceofcoin['price_of_coin'] == null
                //     ? ""
                //     : dashboard.mypriceofcoin['price_of_coin'],
                style: new TextStyle(
                    fontWeight: Pallet.font500,
                    fontSize: Pallet.subheading1,
                    color: Pallet.fontcolor),
              ),
              SizedBox(width: 10),
              Expanded(
                child: MyText(
                  text: dashboard.pageDetails["text20"],
                  style: new TextStyle(
                      fontSize: Pallet.heading3,
                      fontWeight: Pallet.font500,
                      color: Pallet.fontcolor),
                ),
              ),
            ],
          ),
          Container(
            width: wdgtWidth,
            child: Wrap(
              runSpacing: 5,
              spacing: 5,
              children: [
                for (var data in dashboard.mypriceofcoin['boxes'])
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 15),
                    decoration: BoxDecoration(
                        color: Pallet.dashsmallcontainerback,
                        borderRadius: BorderRadius.circular(10)),
                    width: wdgtWidth * 0.40,
                    height: wdgtHeight * 0.25,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        if (data['key'].toString() == 'duc_in_vault')
                          MyText(
                              text: dashboard.pageDetails["text165"],
                              style: TextStyle(
                                  fontSize: Pallet.heading6,
                                  fontWeight: Pallet.font500,
                                  color: Pallet.fontcolor)),
                        if (data['key'].toString() == 'cct')
                          MyText(
                              text: dashboard.pageDetails["text166"],
                              style: TextStyle(
                                  fontSize: Pallet.heading6,
                                  fontWeight: Pallet.font500,
                                  color: Pallet.fontcolor)),
                        if (data['key'].toString() == 'bonus_credits')
                          MyText(
                              text: dashboard.pageDetails["text167"],
                              style: TextStyle(
                                  fontSize: Pallet.heading6,
                                  fontWeight: Pallet.font500,
                                  color: Pallet.fontcolor)),
                        if (data['key'].toString() == 'aap')
                          MyText(
                              text: dashboard.pageDetails["text168"],
                              style: TextStyle(
                                  fontSize: Pallet.heading6,
                                  fontWeight: Pallet.font500,
                                  color: Pallet.fontcolor)),
                        if (data['key'].toString() == 'dsv_earned')
                          MyText(
                              text: dashboard.pageDetails["text176"],
                              style: TextStyle(
                                  fontSize: Pallet.heading6,
                                  fontWeight: Pallet.font500,
                                  color: Pallet.fontcolor)),
                        SizedBox(height: 10),
                        MyText(
                            text: data['content'].toString(),
                            style: TextStyle(
                                fontSize: Pallet.heading4,
                                fontWeight: Pallet.font500,
                                color: Pallet.fontcolor)),
                      ],
                    ),
                  ),
              ],
            ),
          ),
          if (dashboard.accountType == 2)
            // SizedBox(height: dashboard.accountType == 2 ? 5 : 0),
            if (dashboard.accountType == 2)
              InkWell(
                onTap: dashboard.accountType == 2
                    ? () {
                        showDialog(
                          context: context,
                          builder: (BuildContext context) {
                            return AlertDialog(
                              elevation: 24.0,
                              backgroundColor: Pallet.popupcontainerback,
                              shape: RoundedRectangleBorder(
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(15.0))),
                              title: Row(
                                children: [
                                  Image.asset("c-logo.png", width: 40),
                                  SizedBox(width: 10),
                                  MyText(
                                      text: dashboard.pageDetails["text27"],
                                      style: TextStyle(
                                          fontSize: popheading,
                                          color: Pallet.fontcolor,
                                          fontWeight: Pallet.font500)),
                                ],
                              ),
                              content: Container(
                                width: 450.0,
                                child: SingleChildScrollView(
                                  child: ListBody(
                                    children: <Widget>[
                                      Center(
                                          child: ducvaultpopup(
                                              popheading: popheading)),
                                    ],
                                  ),
                                ),
                              ),
                              actions: [
                                PopupButton(
                                  text: dashboard.pageDetails["text78"],
                                  onpress: () {
                                    Navigator.of(context).pop();
                                  },
                                ),
                                SizedBox(height: 10),
                              ],
                            );
                          },
                        );
                      }
                    : () {},
                child: Container(
                  child: Row(
                    children: [
                      MyText(
                          text: dashboard.accountType == 2
                              ? dashboard.pageDetails["text24"]
                              : dashboard.pageDetails["text4"],
                          style: TextStyle(
                              fontSize: Pallet.heading4,
                              fontWeight: Pallet.font500,
                              color: Pallet.fontcolor)),
                      SizedBox(width: 5.0),
                      Icon(
                        Icons.arrow_forward,
                        color: Pallet.fontcolor,
                        size: 18.0,
                      ),
                    ],
                  ),
                ),
              ),
          if (dashboard.accountType == 2)
            // SizedBox(width: dashboard.accountType == 2 ? 5 : 0),
            if (dashboard.accountType == 2)
              InkWell(
                onTap: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => Home(route: 'request_payout')),
                  );
                },
                child: Container(
                  child: Row(
                    children: [
                      MyText(
                          text: dashboard.pageDetails["text25"],
                          style: TextStyle(
                              fontSize: Pallet.heading4,
                              fontWeight: Pallet.font500,
                              color: Pallet.fontcolor)),
                      SizedBox(width: 5.0),
                      Icon(
                        Icons.arrow_forward,
                        color: Pallet.fontcolor,
                        size: 18.0,
                      ),
                    ],
                  ),
                ),
              ),
          if (dashboard.accountType == 2 || dashboard.accountType == 1)
            SizedBox(width: dashboard.accountType == 2 ? 5 : 0),
          CustomButton(
            width: 180,
            text: dashboard.pageDetails["text26"],
            vpadding: 10,
            fontweight: Pallet.subheading1wgt,
            textcolor: Pallet.fontcolornew,
            textsize: Pallet.heading6,
            onpress: () async {
              String ducError;
              await showDialog(
                  context: context,
                  builder: (BuildContext context) {
                    return StatefulBuilder(
                        builder: (BuildContext context,
                                void Function(void Function()) setState1) =>
                            AlertDialog(
                              elevation: 24.0,
                              backgroundColor: Pallet.popupcontainerback,
                              shape: RoundedRectangleBorder(
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(15.0))),
                              title: Row(
                                children: [
                                  Image.asset("c-logo.png", width: 40),
                                  SizedBox(width: 10),
                                  MyText(
                                      text: dashboard.pageDetails["text120"],
                                      style: TextStyle(
                                          fontSize: popheading,
                                          color: Pallet.fontcolor,
                                          fontWeight: Pallet.font500)),
                                ],
                              ),
                              content: Container(
                                width: 400.0,
                                child: SingleChildScrollView(
                                  child: ListBody(
                                    children: <Widget>[
                                      Column(
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        children: [
                                          MyText(
                                            text:
                                                dashboard.pageDetails["text46"],
                                            style: TextStyle(
                                                fontWeight: Pallet.font500,
                                                fontSize: Pallet.heading2,
                                                color: Pallet.fontcolor),
                                          ),
                                          SizedBox(height: 20.0),
                                          TextFormField(
                                            controller: adddsv,
                                            textAlign: TextAlign.left,
                                            keyboardType: TextInputType.number,
                                            inputFormatters: <
                                                TextInputFormatter>[
                                              FilteringTextInputFormatter
                                                  .digitsOnly
                                            ],
                                            style: TextStyle(
                                                color: Pallet.fontcolor),
                                            cursorColor: Pallet.fontcolor,
                                            decoration: InputDecoration(
                                              errorText: ducError,
                                              border: OutlineInputBorder(
                                                  borderSide: BorderSide(
                                                      color: Pallet.fontcolor,
                                                      width: 2.0),
                                                  borderRadius:
                                                      BorderRadius.circular(
                                                          8.0)),
                                              // labelText: 'Enter DSV',
                                              hintText: dashboard
                                                  .pageDetails["text47"],
                                              hintStyle: TextStyle(
                                                  fontSize: 14,
                                                  color: Color(0xFFb6c3db)),
                                              fillColor: Color(0xFF4570ae),
                                              focusedBorder: OutlineInputBorder(
                                                  borderSide: BorderSide(
                                                      color: Pallet.fontcolor,
                                                      width: 2.0),
                                                  borderRadius:
                                                      BorderRadius.circular(
                                                          8.0)),
                                              enabledBorder: OutlineInputBorder(
                                                  borderSide: BorderSide(
                                                      color: Pallet.fontcolor,
                                                      width: 2.0),
                                                  borderRadius:
                                                      BorderRadius.circular(
                                                          8.0)),
                                              labelStyle: TextStyle(
                                                color: Color(0xFFb6c3db),
                                                fontSize: 14,
                                              ),
                                            ),
                                            validator: (input) {
                                              final isDigitsOnly =
                                                  int.tryParse(input);
                                              return isDigitsOnly == null
                                                  ? dashboard
                                                      .pageDetails["text48"]
                                                  : null;
                                            },
                                            onChanged: (value) {
                                              if (value.isEmpty) {
                                                setState1(() {
                                                  ducError = dashboard
                                                      .pageDetails["text93"];
                                                });
                                              } else if (double.parse(value) ==
                                                  0) {
                                                setState1(() {
                                                  ducError = dashboard
                                                      .pageDetails["text93"];
                                                });
                                              } else if (double.parse(value) >
                                                  dashboard.mypriceofcoin[
                                                      'dsv_balance']) {
                                                setState1(() {
                                                  ducError = dashboard
                                                      .pageDetails["text91"];
                                                });
                                              } else {
                                                setState1(() {
                                                  ducError = null;
                                                });
                                              }
                                            },
                                          ),
                                          SizedBox(height: 10.0),
                                          Container(
                                            width: double.infinity,
                                            // ignore: deprecated_member_use
                                            child: CustomButton(
                                              onpress: () async {
                                                if (adddsv.text.isEmpty) {
                                                  setState1(() {
                                                    ducError = dashboard
                                                        .pageDetails["text93"];
                                                  });
                                                } else if (double.parse(
                                                        adddsv.text) ==
                                                    0) {
                                                  setState1(() {
                                                    ducError = dashboard
                                                        .pageDetails["text93"];
                                                  });
                                                } else if (double.parse(
                                                        adddsv.text) >
                                                    dashboard.mypriceofcoin[
                                                        'dsv_balance']) {
                                                  setState1(() {
                                                    ducError = dashboard
                                                        .pageDetails["text91"];
                                                  });
                                                } else {
                                                  Map<String, dynamic> _map = {
                                                    "product_id": 1,
                                                    "amount": double.parse(
                                                        adddsv.text),
                                                  };
                                                  waiting.waitpopup(context);

                                                  Map<String, dynamic> result =
                                                      await HttpRequest.Post(
                                                          'redeemduc',
                                                          Utils
                                                              .constructPayload(
                                                                  _map));
                                                  print('$result, region');
                                                  if (Utils.isServerError(
                                                      result)) {
                                                    Navigator.pop(context);

                                                    var error =
                                                        await Utils.getMessage(
                                                            result['response']
                                                                ['error']);
                                                    setState(() {
                                                      ducError = error;
                                                    });
                                                    throw (error);
                                                  } else {
                                                    Navigator.pop(context);

                                                    print(result['response']
                                                        ['data']);
                                                    setState1(() {
                                                      ducError = null;
                                                      Navigator.of(context)
                                                          .pop();
                                                      return showDialog(
                                                          barrierDismissible:
                                                              false,
                                                          context: context,
                                                          builder: (BuildContext
                                                              context) {
                                                            return AlertDialog(
                                                              backgroundColor:
                                                                  Pallet
                                                                      .popupcontainerback,
                                                              shape: RoundedRectangleBorder(
                                                                  borderRadius:
                                                                      BorderRadius.circular(
                                                                          Pallet
                                                                              .radius)),
                                                              title: Row(
                                                                mainAxisAlignment:
                                                                    MainAxisAlignment
                                                                        .start,
                                                                children: [
                                                                  Image.asset(
                                                                      "c-logo.png",
                                                                      width:
                                                                          30),
                                                                  SizedBox(
                                                                      width:
                                                                          10),
                                                                  Expanded(
                                                                    child: MyText(
                                                                        text: dashboard.pageDetails[
                                                                            "text26"],
                                                                        style: TextStyle(
                                                                            fontSize: Pallet.normalfont +
                                                                                10,
                                                                            color:
                                                                                Pallet.fontcolor,
                                                                            fontWeight: Pallet.font500)),
                                                                  ),
                                                                ],
                                                              ),
                                                              content:
                                                                  Container(
                                                                width: 350,
                                                                height: 150,
                                                                child: Column(
                                                                  mainAxisAlignment:
                                                                      MainAxisAlignment
                                                                          .spaceEvenly,
                                                                  children: [
                                                                    CircleAvatar(
                                                                      backgroundColor:
                                                                          Colors
                                                                              .white,
                                                                      radius:
                                                                          30,
                                                                      child: Icon(
                                                                          Icons
                                                                              .done,
                                                                          color: Pallet
                                                                              .dashcontainerback,
                                                                          size:
                                                                              40),
                                                                    ),
                                                                    MyText(
                                                                        text: dashboard.pageDetails[
                                                                            "text121"],
                                                                        textAlign:
                                                                            TextAlign
                                                                                .center,
                                                                        style: TextStyle(
                                                                            fontSize:
                                                                                Pallet.normalfont,
                                                                            color: Pallet.fontcolor,
                                                                            fontWeight: Pallet.font500,
                                                                            height: 1.5)),
                                                                  ],
                                                                ),
                                                              ),
                                                              actions: [
                                                                PopupButton(
                                                                  text: dashboard
                                                                          .pageDetails[
                                                                      "text82"],
                                                                  onpress: () {
                                                                    Navigator
                                                                        .push(
                                                                      context,
                                                                      MaterialPageRoute(
                                                                          builder: (context) =>
                                                                              Home(route: 'dashboard')),
                                                                    );
                                                                  },
                                                                ),
                                                              ],
                                                            );
                                                          });
                                                    });
                                                    return Utils.toJSONString(
                                                        await Utils
                                                            .convertMessage(
                                                                result['response']
                                                                    ['data']));
                                                  }
                                                }
                                              },
                                              vpadding: 12,
                                              text: dashboard
                                                  .pageDetails["text49"],
                                              buttoncolor: Pallet.fontcolor,
                                              textcolor: Pallet.fontcolornew,
                                              textsize: Pallet.heading4,
                                            ),
                                          ),
                                        ],
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                              actions: [
                                PopupButton(
                                  text: dashboard.pageDetails["text78"],
                                  onpress: () {
                                    Navigator.of(context).pop();
                                  },
                                ),
                                SizedBox(height: 10),
                              ],
                            ));
                  });
            },
          ),
          SizedBox(height: 20.0),
          if (dashboard.accountType == 2)
            CustomButton(
              vpadding: 10,
              width: 170,
              text: dashboard.pageDetails["text122"],
              fontweight: Pallet.subheading1wgt,
              textcolor: Pallet.fontcolornew,
              textsize: Pallet.heading6,
              onpress: () async {
                await DSVPRedeem(context, setState);
              },
            ),
          // SizedBox(height: 20.0),
          if (dashboard.accountType == 2)
            CustomButton(
              width: 180,
              // hpadding: 1,
              fontweight: Pallet.subheading1wgt,
              textcolor: Pallet.fontcolornew,
              textsize: Pallet.heading6,
              onpress: () {
                showDialog(
                  context: context,
                  builder: (BuildContext context) {
                    return AlertDialog(
                      elevation: 24.0,
                      backgroundColor: Pallet.popupcontainerback,
                      shape: RoundedRectangleBorder(
                          borderRadius:
                              BorderRadius.all(Radius.circular(15.0))),
                      title: Row(
                        children: [
                          Image.asset("c-logo.png", width: 40),
                          SizedBox(width: 10),
                          MyText(
                              text: dashboard.pageDetails["text2"],
                              style: TextStyle(
                                  fontSize: popheading,
                                  color: Pallet.fontcolor,
                                  fontWeight: Pallet.font500)),
                        ],
                      ),
                      content: Container(
                        width: 455.0,
                        child: SingleChildScrollView(
                          child: ListBody(
                            children: <Widget>[
                              MyText(
                                  text: dashboard.pageDetails["text123"],
                                  textAlign: TextAlign.left,
                                  style: TextStyle(
                                    fontSize: popheading / 1.03,
                                    height: 1.5,
                                    color: Pallet.fontcolor,
                                  )),
                              Center(
                                  child: attendwebinarpopup(
                                      popheading: popheading)),
                              MyText(
                                  text: dashboard.pageDetails["text124"],
                                  textAlign: TextAlign.left,
                                  style: TextStyle(
                                      fontSize: popheading / 1.03,
                                      color: Pallet.fontcolor,
                                      fontWeight: Pallet.font500)),
                            ],
                          ),
                        ),
                      ),
                      actions: [
                        Row(
                          children: [
                            PopupButton(
                              onpress: () {
                                launch(dashboard.zoomlink.toString());
                              },
                              child: Center(
                                child: Row(
                                  children: [
                                    Icon(
                                      Icons.video_call,
                                      color: Pallet.fontcolornew,
                                      size: 18.0,
                                    ),
                                    SizedBox(width: 10.0),
                                    MyText(
                                      text: dashboard.pageDetails["text2"],
                                      style: TextStyle(
                                        fontSize: Pallet.heading6,
                                        color: Pallet.fontcolornew,
                                        fontWeight: Pallet.font500,
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            SizedBox(width: 9),
                            PopupButton(
                              text: dashboard.pageDetails["text78"],
                              onpress: () {
                                Navigator.of(context).pop();
                              },
                            ),
                            // SizedBox(height: 9),
                          ],
                        ),
                      ],
                    );
                  },
                );
              },
              child: CustomButton(
                // buttoncolor: Pallet.fontcolor,
                // textcolor: Pallet.fontcolornew,
                // textsize: Pallet.heading6,
                child: Row(
                  children: [
                    Icon(
                      Icons.video_call,
                      color: Pallet.fontcolornew,
                      size: 15.0,
                    ),
                    SizedBox(width: 3.0),
                    Expanded(
                      child: MyText(
                        text: dashboard.pageDetails["text2"],
                        style: TextStyle(
                            color: Pallet.fontcolornew,
                            fontSize: Pallet.heading6,
                            fontWeight: Pallet.subheading1wgt),
                      ),
                    ),
                  ],
                ),
              ),
            ),
        ],
      ),
    );
  }

  Widget myrefferedcustomers(
      {double wdgtWidth, double wdgtHeight, popheading}) {
    return Container(
      decoration: BoxDecoration(
          boxShadow: [Pallet.shadowEffect],
          color: Pallet.specialdashcontainerback,
          borderRadius: BorderRadius.circular(10)),
      width: wdgtWidth,
      height: wdgtHeight,
      child: Padding(
        padding: const EdgeInsets.all(11.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  CircleAvatar(
                    backgroundColor: Pallet.dashsmallcontainerback,
                    child:
                        Image.asset('c-logo.png', width: 30, fit: BoxFit.cover),
                    radius: 25,
                  ),
                  SizedBox(width: 10),
                  MyText(
                    text: dashboard.pageDetails["text125"],
                    style: new TextStyle(
                        fontSize: Pallet.heading3,
                        fontWeight: Pallet.font500,
                        color: Pallet.fontcolor),
                  ),
                ],
              ),
            ),
            SizedBox(height: 70),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Container(
                  width: wdgtWidth * 0.4,
                  child: Wrap(
                    runSpacing: 5,
                    spacing: 5,
                    children: [
                      for (var i = 0; i < dashboard.myteam1.length; i++)
                        InkWell(
                          onTap: () {
                            if (dashboard.myteam1[i]['id'] == 2) {
                              myteam_inside_data = dashboard.myteam1[i]['data'];
                              if (myteam_inside_data.isEmpty ||
                                  myteam_inside_data == null) {
                                if (dashboard.myteam1[i]['key'].toString() ==
                                    'my_preferred_customer')
                                  // MyText(dashboard.pageDetails["text177"],
                                  //     style: TextStyle(
                                  //         fontSize: Pallet.heading6,
                                  //         fontWeight: Pallet.font500,
                                  //         color: Pallet.fontcolor)),
                                  snack.snack(
                                      title: dashboard.pageDetails["text177"]
                                              .toString() +
                                          ' ' +
                                          dashboard.pageDetails["text175"]);
                              } else {
                                showDialog(
                                  context: context,
                                  builder: (BuildContext context) {
                                    return AlertDialog(
                                      elevation: 24.0,
                                      backgroundColor:
                                          Pallet.popupcontainerback,
                                      shape: RoundedRectangleBorder(
                                          borderRadius: BorderRadius.all(
                                              Radius.circular(15.0))),
                                      title: Row(
                                        children: [
                                          Image.asset("c-logo.png"),
                                          SizedBox(width: 10),
                                          // MyText(
                                          //     dashboard.myteam1[i]['heading']
                                          //         .toString(),
                                          //     style: TextStyle(
                                          //         fontSize: popheading,
                                          //         color: Pallet.fontcolor,
                                          //         fontWeight: Pallet.font500)),
                                          if (dashboard.myteam1[i]['key']
                                                  .toString() ==
                                              'my_preferred_customer')
                                            MyText(
                                                text: dashboard
                                                    .pageDetails["text177"],
                                                style: TextStyle(
                                                    fontSize: Pallet.heading6,
                                                    fontWeight: Pallet.font500,
                                                    color: Pallet.fontcolor)),
                                        ],
                                      ),
                                      content: Container(
                                        width: 400,
                                        child: SingleChildScrollView(
                                          scrollDirection: Axis.vertical,
                                          child: ListBody(
                                            children: <Widget>[
                                              myteammebers(
                                                  popheading: popheading)
                                            ],
                                          ),
                                        ),
                                      ),
                                      actions: [
                                        PopupButton(
                                          text: dashboard.pageDetails["text78"],
                                          onpress: () {
                                            Navigator.of(context).pop();
                                          },
                                        ),
                                        SizedBox(height: 10),
                                      ],
                                    );
                                  },
                                );
                              }
                            } else {
                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) =>
                                        Home(route: 'my_network')),
                              );
                            }
                          },
                          child: Container(
                            padding: EdgeInsets.symmetric(horizontal: 15),
                            decoration: BoxDecoration(
                                color: Pallet.dashsmallcontainerback,
                                borderRadius: BorderRadius.circular(10)),
                            width: wdgtWidth * 0.45,
                            height: wdgtHeight * 0.25,
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                if (dashboard.myteam1[i]['key'].toString() ==
                                    'my_preferred_customer')
                                  MyText(
                                      text: dashboard.pageDetails["text177"],
                                      style: TextStyle(
                                          fontSize: Pallet.heading6,
                                          fontWeight: Pallet.font500,
                                          color: Pallet.fontcolor)),

                                // MyText(dashboard.myteam1[i]['heading'].toString(),
                                //     style: TextStyle(
                                //         fontSize: Pallet.heading6,
                                //         fontWeight: Pallet.font500,
                                //         color: Pallet.fontcolor)),
                                SizedBox(height: 10),
                                MyText(
                                    text: dashboard.myteam1[i]['content']
                                        .toString(),
                                    style: TextStyle(
                                        fontSize: Pallet.heading4,
                                        fontWeight: Pallet.font500,
                                        color: Pallet.fontcolor)),
                              ],
                            ),
                          ),
                        ),
                    ],
                  ),
                ),
                Container(
                  width: wdgtWidth * 0.4,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      // InkWell(
                      //   // onTap: () {
                      //   //   showDialog(
                      //   //     context: context,
                      //   //     builder: (BuildContext context) {
                      //   //       return AlertDialog(
                      //   //         elevation: 24.0,
                      //   //         backgroundColor: Pallet.popupcontainerback,
                      //   //         shape: RoundedRectangleBorder(
                      //   //             borderRadius: BorderRadius.all(
                      //   //                 Radius.circular(15.0))),
                      //   //         title: Row(
                      //   //           children: [
                      //   //             Image.asset("c-logo.png",
                      //   //                 width: wdgtWidth * 0.1),
                      //   //             SizedBox(width: 10),
                      //   //             MyText(dashboard.pageDetails["text27"],
                      //   //                 style: TextStyle(
                      //   //                     fontSize: popheading,
                      //   //                     color: Pallet.fontcolor,
                      //   //                     fontWeight: Pallet.font500)),
                      //   //           ],
                      //   //         ),
                      //   //         content: Container(
                      //   //           width: 450.0,
                      //   //           child: SingleChildScrollView(
                      //   //             child: ListBody(
                      //   //               children: <Widget>[
                      //   //                 Center(
                      //   //                     child: ducvaultpopup(
                      //   //                         popheading: popheading)),
                      //   //               ],
                      //   //             ),
                      //   //           ),
                      //   //         ),
                      //   //         actions: [
                      //   //           PopupButton(
                      //   //             MyText: dashboard.pageDetails["text78"],
                      //   //             onpress: () {
                      //   //               Navigator.of(context).pop();
                      //   //             },
                      //   //           ),
                      //   //           SizedBox(height: 10),
                      //   //         ],
                      //   //       );
                      //   //     },
                      //   //   );
                      //   // },
                      //   child: Container(
                      //     child: Row(
                      //       children: [
                      //         MyText(
                      //             dashboard.accountType == 2
                      //                 ? dashboard.pageDetails["text24"]
                      //                 : 'Details',
                      //             style: TextStyle(
                      //                 fontSize: Pallet.heading4,
                      //                 fontWeight: Pallet.font500,
                      //                 color: Pallet.fontcolor)),
                      //         SizedBox(width: 5.0),
                      //         Icon(
                      //           Icons.arrow_forward,
                      //           color: Pallet.fontcolor,
                      //           size: 18.0,
                      //         ),
                      //       ],
                      //     ),
                      //   ),
                      // ),
                      // SizedBox(height: 20.0),
                      CustomButton(
                        vpadding: 10,
                        width: 170,
                        text: dashboard.pageDetails["text122"],
                        fontweight: Pallet.subheading1wgt,
                        textcolor: Pallet.fontcolornew,
                        textsize: Pallet.heading7,
                        onpress: () async {
                          await DSVPRedeem(context, setState);
                        },
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }

  Widget myrefferedcustomersmobile(
      {double wdgtWidth, double wdgtHeight, popheading}) {
    return Container(
      decoration: BoxDecoration(
          boxShadow: [Pallet.shadowEffect],
          color: Pallet.specialdashcontainerback,
          borderRadius: BorderRadius.circular(10)),
      width: wdgtWidth,
      height: wdgtHeight,
      child: Padding(
        padding: const EdgeInsets.all(11.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                CircleAvatar(
                  backgroundColor: Pallet.dashsmallcontainerback,
                  child:
                      Image.asset('c-logo.png', width: 30, fit: BoxFit.cover),
                  radius: 25,
                ),
                SizedBox(width: 10),
                MyText(
                  text: dashboard.pageDetails["text125"],
                  style: new TextStyle(
                      fontSize: Pallet.heading3,
                      fontWeight: Pallet.font500,
                      color: Pallet.fontcolor),
                ),
              ],
            ),
            SizedBox(height: 30),
            Wrap(
              runSpacing: 5,
              spacing: 5,
              children: [
                for (var i = 0; i < dashboard.myteam1.length; i++)
                  InkWell(
                    onTap: () {
                      if (dashboard.myteam1[i]['id'] == 2) {
                        myteam_inside_data = dashboard.myteam1[i]['data'];
                        if (myteam_inside_data.isEmpty ||
                            myteam_inside_data == null) {
                          if (dashboard.myteam1[i]['key'].toString() ==
                              'my_preferred_customer')
                            // MyText(dashboard.pageDetails["text177"],
                            //     style: TextStyle(
                            //         fontSize: Pallet.heading6,
                            //         fontWeight: Pallet.font500,
                            //         color: Pallet.fontcolor)),
                            snack.snack(
                                title: dashboard.pageDetails["text177"]
                                        .toString() +
                                    ' ' +
                                    dashboard.pageDetails["text175"]);
                        } else {
                          showDialog(
                            context: context,
                            builder: (BuildContext context) {
                              return AlertDialog(
                                elevation: 24.0,
                                backgroundColor: Pallet.popupcontainerback,
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.all(
                                        Radius.circular(15.0))),
                                title: Row(
                                  children: [
                                    Image.asset("c-logo.png"),
                                    SizedBox(width: 10),
                                    // MyText(
                                    //     dashboard.myteam1[i]['heading']
                                    //         .toString(),
                                    //     style: TextStyle(
                                    //         fontSize: popheading,
                                    //         color: Pallet.fontcolor,
                                    //         fontWeight: Pallet.font500)),
                                    if (dashboard.myteam1[i]['key']
                                            .toString() ==
                                        'my_preferred_customer')
                                      MyText(
                                          text:
                                              dashboard.pageDetails["text177"],
                                          style: TextStyle(
                                              fontSize: Pallet.heading6,
                                              fontWeight: Pallet.font500,
                                              color: Pallet.fontcolor)),
                                  ],
                                ),
                                content: Container(
                                  width: 400,
                                  child: SingleChildScrollView(
                                    scrollDirection: Axis.vertical,
                                    child: ListBody(
                                      children: <Widget>[
                                        myteammebers(popheading: popheading)
                                      ],
                                    ),
                                  ),
                                ),
                                actions: [
                                  PopupButton(
                                    text: dashboard.pageDetails["text78"],
                                    onpress: () {
                                      Navigator.of(context).pop();
                                    },
                                  ),
                                  SizedBox(height: 10),
                                ],
                              );
                            },
                          );
                        }
                      } else {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => Home(route: 'my_network')),
                        );
                      }
                    },
                    child: Container(
                      padding: EdgeInsets.symmetric(horizontal: 15),
                      decoration: BoxDecoration(
                          color: Pallet.dashsmallcontainerback,
                          borderRadius: BorderRadius.circular(10)),
                      width: wdgtWidth,
                      height: wdgtHeight * 0.25,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          // MyText(dashboard.myteam1[i]['heading'].toString(),
                          //     style: TextStyle(
                          //         fontSize: Pallet.heading6,
                          //         fontWeight: Pallet.font500,
                          //         color: Pallet.fontcolor)),
                          if (dashboard.myteam1[i]['key'].toString() ==
                              'my_preferred_customer')
                            MyText(
                                text: dashboard.pageDetails["text177"],
                                style: TextStyle(
                                    fontSize: Pallet.heading6,
                                    fontWeight: Pallet.font500,
                                    color: Pallet.fontcolor)),
                          SizedBox(height: 10),
                          MyText(
                              text: dashboard.myteam1[i]['content'].toString(),
                              style: TextStyle(
                                  fontSize: Pallet.heading4,
                                  fontWeight: Pallet.font500,
                                  color: Pallet.fontcolor)),
                        ],
                      ),
                    ),
                  ),
              ],
            ),
            SizedBox(height: 30),
            // InkWell(
            //   // onTap: () {
            //   //   showDialog(
            //   //     context: context,
            //   //     builder: (BuildContext context) {
            //   //       return AlertDialog(
            //   //         elevation: 24.0,
            //   //         backgroundColor: Pallet.popupcontainerback,
            //   //         shape: RoundedRectangleBorder(
            //   //             borderRadius:
            //   //                 BorderRadius.all(Radius.circular(15.0))),
            //   //         title: Row(
            //   //           children: [
            //   //             Image.asset("c-logo.png", width: wdgtWidth * 0.1),
            //   //             SizedBox(width: 10),
            //   //             MyText(dashboard.pageDetails["text27"],
            //   //                 style: TextStyle(
            //   //                     fontSize: popheading,
            //   //                     color: Pallet.fontcolor,
            //   //                     fontWeight: Pallet.font500)),
            //   //           ],
            //   //         ),
            //   //         content: Container(
            //   //           width: 450.0,
            //   //           child: SingleChildScrollView(
            //   //             child: ListBody(
            //   //               children: <Widget>[
            //   //                 Center(
            //   //                     child: ducvaultpopup(popheading: popheading)),
            //   //               ],
            //   //             ),
            //   //           ),
            //   //         ),
            //   //         actions: [
            //   //           PopupButton(
            //   //             MyText: dashboard.pageDetails["text78"],
            //   //             onpress: () {
            //   //               Navigator.of(context).pop();
            //   //             },
            //   //           ),
            //   //           SizedBox(height: 10),
            //   //         ],
            //   //       );
            //   //     },
            //   //   );
            //   // },
            //   child: Container(
            //     child: Row(
            //       children: [
            //         MyText(
            //             dashboard.accountType == 2
            //                 ? dashboard.pageDetails["text24"]
            //                 : 'Details',
            //             style: TextStyle(
            //                 fontSize: Pallet.heading4,
            //                 fontWeight: Pallet.font500,
            //                 color: Pallet.fontcolor)),
            //         SizedBox(width: 5.0),
            //         Icon(
            //           Icons.arrow_forward,
            //           color: Pallet.fontcolor,
            //           size: 18.0,
            //         ),
            //       ],
            //     ),
            //   ),
            // ),
            // SizedBox(height: 20.0),
            CustomButton(
              vpadding: 10,
              width: 170,
              text: dashboard.pageDetails["text122"],
              fontweight: Pallet.subheading1wgt,
              textcolor: Pallet.fontcolornew,
              textsize: Pallet.heading6,
              onpress: () async {
                await DSVPRedeem(context, setState);
              },
            ),
          ],
        ),
      ),
    );
  }

  // Widget applycct({double wdgtWidth, double wdgtHeight}) {
  //   return Container(
  //     padding: EdgeInsets.symmetric(horizontal: 20),
  //     decoration: BoxDecoration(
  //         boxShadow: [Pallet.shadowEffect],
  //         color: Pallet.dashcontainerback,
  //         borderRadius: BorderRadius.circular(10)),
  //     width: wdgtWidth,
  //     height: wdgtHeight,
  //     child: Column(
  //       mainAxisAlignment: MainAxisAlignment.spaceEvenly,
  //       crossAxisAlignment: CrossAxisAlignment.start,
  //       children: [
  //         Row(
  //           crossAxisAlignment: CrossAxisAlignment.start,
  //           mainAxisAlignment: MainAxisAlignment.start,
  //           children: [
  //             MyText(
  //               dashboard.pageDetails["text28"],
  //               style: new TextStyle(
  //                   fontSize: Pallet.heading3,
  //                   fontWeight: Pallet.font500,
  //                   color: Pallet.fontcolor),
  //             ),
  //           ],
  //         ),
  //         Row(
  //           mainAxisAlignment: MainAxisAlignment.spaceAround,
  //           crossAxisAlignment: CrossAxisAlignment.start,
  //           children: [
  //             Container(
  //               padding: EdgeInsets.symmetric(horizontal: 15),
  //               decoration: BoxDecoration(
  //                   color: Pallet.dashsmallcontainerback,
  //                   borderRadius: BorderRadius.circular(10)),
  //               width: wdgtWidth * 0.40,
  //               height: wdgtHeight * 0.30,
  //               child: Column(
  //                 mainAxisAlignment: MainAxisAlignment.center,
  //                 crossAxisAlignment: CrossAxisAlignment.start,
  //                 children: [
  //                   MyText(dashboard.pageDetails["text29"],
  //                       style: TextStyle(
  //                           fontSize: Pallet.heading6,
  //                           fontWeight: Pallet.font500,
  //                           color: Pallet.fontcolor)),
  //                   SizedBox(height: 10),
  //                   MyText("3393/3400",

  //                       ///server pending
  //                       style: TextStyle(
  //                           fontSize: Pallet.heading4,
  //                           fontWeight: Pallet.font500,
  //                           color: Pallet.fontcolor)),
  //                 ],
  //               ),
  //             ),
  //             Container(
  //               padding: EdgeInsets.symmetric(horizontal: 15),
  //               decoration: BoxDecoration(
  //                   color: Pallet.dashsmallcontainerback,
  //                   borderRadius: BorderRadius.circular(10)),
  //               width: wdgtWidth * 0.40,
  //               height: wdgtHeight * 0.30,
  //               child: Column(
  //                 mainAxisAlignment: MainAxisAlignment.center,
  //                 crossAxisAlignment: CrossAxisAlignment.start,
  //                 children: [
  //                   MyText(dashboard.pageDetails["text30"],
  //                       style: TextStyle(
  //                           fontSize: Pallet.heading6,
  //                           fontWeight: Pallet.font500,
  //                           color: Pallet.fontcolor)),
  //                   SizedBox(height: 10),
  //                   MyText("\$ 1,000",

  //                       ///server
  //                       style: TextStyle(
  //                           fontSize: Pallet.heading4,
  //                           fontWeight: Pallet.font500,
  //                           color: Pallet.fontcolor)),
  //                 ],
  //               ),
  //             ),
  //           ],
  //         ),
  //         Row(
  //           mainAxisAlignment: MainAxisAlignment.spaceAround,
  //           crossAxisAlignment: CrossAxisAlignment.start,
  //           children: [
  //             Container(
  //               padding: EdgeInsets.symmetric(horizontal: 15),
  //               decoration: BoxDecoration(
  //                   color: Pallet.dashsmallcontainerback,
  //                   borderRadius: BorderRadius.circular(10)),
  //               width: wdgtWidth * 0.40,
  //               height: wdgtHeight * 0.30,
  //               child: Column(
  //                 mainAxisAlignment: MainAxisAlignment.center,
  //                 crossAxisAlignment: CrossAxisAlignment.start,
  //                 children: [
  //                   MyText(dashboard.pageDetails["text31"],
  //                       style: TextStyle(
  //                           fontSize: Pallet.heading6,
  //                           fontWeight: Pallet.font500,
  //                           color: Pallet.fontcolor)),
  //                   SizedBox(height: 10),
  //                   MyText("416",

  //                       ///server
  //                       style: TextStyle(
  //                           fontSize: Pallet.heading4,
  //                           fontWeight: Pallet.font500,
  //                           color: Pallet.fontcolor)),
  //                 ],
  //               ),
  //             ),
  //             Container(
  //               padding: EdgeInsets.symmetric(horizontal: 15),
  //               decoration: BoxDecoration(
  //                   color: Pallet.dashsmallcontainerback,
  //                   borderRadius: BorderRadius.circular(10)),
  //               width: wdgtWidth * 0.40,
  //               height: wdgtHeight * 0.30,
  //               child: Column(
  //                 mainAxisAlignment: MainAxisAlignment.center,
  //                 crossAxisAlignment: CrossAxisAlignment.start,
  //                 children: [
  //                   MyText(dashboard.pageDetails["text32"],
  //                       style: TextStyle(
  //                           fontSize: Pallet.heading6,
  //                           fontWeight: Pallet.font500,
  //                           color: Pallet.fontcolor)),
  //                   SizedBox(height: 10),
  //                   MyText("251",

  //                       ///server
  //                       style: TextStyle(
  //                           fontSize: Pallet.heading4,
  //                           fontWeight: Pallet.font500,
  //                           color: Pallet.fontcolor)),
  //                 ],
  //               ),
  //             ),
  //           ],
  //         ),
  //         // Row(
  //         //   mainAxisAlignment: MainAxisAlignment.center,
  //         //   children: [
  //         //     FlatButton(
  //         //       minWidth: 150.0,
  //         //       color: Pallet.buttonback,
  //         //       textColor: Pallet.fontcolornew,
  //         //       splashColor: Pallet.buttonsplashcolor,
  //         //       onPressed: () {
  //         //         Navigator.push(
  //         //           context,
  //         //           MaterialPageRoute(
  //         //               builder: (context) => Home(route: 'applycct')),
  //         //         );
  //         //       },
  //         //       // onPressed: () async {
  //         //       //   await showDialog(context: context, builder: (_) => Redeem());
  //         //       // },
  //         //       shape: RoundedRectangleBorder(
  //         //         borderRadius: BorderRadius.circular(5.0),
  //         //       ),
  //         //       child: MyText(
  //         //         dashboard.pageDetails["text33"],
  //         //         style: TextStyle(
  //         //             fontSize: Pallet.heading4, fontWeight: Pallet.font500),
  //         //       ),
  //         //     ),
  //         //   ],
  //         // ),
  //       ],
  //     ),
  //   );
  // }

  // Widget applycctmobile({double wdgtWidth, double wdgtHeight}) {
  //   return Container(
  //     padding: EdgeInsets.symmetric(horizontal: 20),
  //     decoration: BoxDecoration(
  //         boxShadow: [Pallet.shadowEffect],
  //         color: Pallet.dashcontainerback,
  //         borderRadius: BorderRadius.circular(20)),
  //     width: wdgtWidth,
  //     height: wdgtHeight,
  //     child: Column(
  //       mainAxisAlignment: MainAxisAlignment.spaceEvenly,
  //       crossAxisAlignment: CrossAxisAlignment.start,
  //       children: [
  //         Row(
  //           crossAxisAlignment: CrossAxisAlignment.start,
  //           mainAxisAlignment: MainAxisAlignment.start,
  //           children: [
  //             MyText(
  //               dashboard.pageDetails["text28"],
  //               style: new TextStyle(
  //                   fontSize: Pallet.heading3,
  //                   fontWeight: Pallet.font500,
  //                   color: Pallet.fontcolor),
  //             ),
  //           ],
  //         ),
  //         Row(
  //           mainAxisAlignment: MainAxisAlignment.spaceAround,
  //           crossAxisAlignment: CrossAxisAlignment.start,
  //           children: [
  //             Container(
  //               padding: EdgeInsets.symmetric(horizontal: 15),
  //               decoration: BoxDecoration(
  //                   color: Pallet.dashsmallcontainerback,
  //                   borderRadius: BorderRadius.circular(10)),
  //               width: wdgtWidth * 0.40,
  //               height: wdgtHeight * 0.30,
  //               child: Column(
  //                 mainAxisAlignment: MainAxisAlignment.center,
  //                 crossAxisAlignment: CrossAxisAlignment.start,
  //                 children: [
  //                   MyText(dashboard.pageDetails["text29"],
  //                       style: TextStyle(
  //                           fontSize: Pallet.heading6,
  //                           fontWeight: Pallet.font500,
  //                           color: Pallet.fontcolor)),
  //                   SizedBox(height: 10),
  //                   MyText("3393/3400",

  //                       ///server pending
  //                       style: TextStyle(
  //                           fontSize: Pallet.heading4,
  //                           fontWeight: Pallet.font500,
  //                           color: Pallet.fontcolor)),
  //                 ],
  //               ),
  //             ),
  //             Container(
  //               padding: EdgeInsets.symmetric(horizontal: 15),
  //               decoration: BoxDecoration(
  //                   color: Pallet.dashsmallcontainerback,
  //                   borderRadius: BorderRadius.circular(10)),
  //               width: wdgtWidth * 0.40,
  //               height: wdgtHeight * 0.30,
  //               child: Column(
  //                 mainAxisAlignment: MainAxisAlignment.center,
  //                 crossAxisAlignment: CrossAxisAlignment.start,
  //                 children: [
  //                   MyText(dashboard.pageDetails["text30"],
  //                       style: TextStyle(
  //                           fontSize: Pallet.heading6,
  //                           fontWeight: Pallet.font500,
  //                           color: Pallet.fontcolor)),
  //                   SizedBox(height: 10),
  //                   MyText("\$ 1,000",

  //                       ///server
  //                       style: TextStyle(
  //                           fontSize: Pallet.heading4,
  //                           fontWeight: Pallet.font500,
  //                           color: Pallet.fontcolor)),
  //                 ],
  //               ),
  //             ),
  //           ],
  //         ),
  //         Row(
  //           mainAxisAlignment: MainAxisAlignment.spaceAround,
  //           crossAxisAlignment: CrossAxisAlignment.start,
  //           children: [
  //             Container(
  //               padding: EdgeInsets.symmetric(horizontal: 15),
  //               decoration: BoxDecoration(
  //                   color: Pallet.dashsmallcontainerback,
  //                   borderRadius: BorderRadius.circular(10)),
  //               width: wdgtWidth * 0.40,
  //               height: wdgtHeight * 0.30,
  //               child: Column(
  //                 mainAxisAlignment: MainAxisAlignment.center,
  //                 crossAxisAlignment: CrossAxisAlignment.start,
  //                 children: [
  //                   MyText(dashboard.pageDetails["text31"],
  //                       style: TextStyle(
  //                           fontSize: Pallet.heading6,
  //                           fontWeight: Pallet.font500,
  //                           color: Pallet.fontcolor)),
  //                   SizedBox(height: 10),
  //                   MyText("416",

  //                       ///server
  //                       style: TextStyle(
  //                           fontSize: Pallet.heading4,
  //                           fontWeight: Pallet.font500,
  //                           color: Pallet.fontcolor)),
  //                 ],
  //               ),
  //             ),
  //             Container(
  //               padding: EdgeInsets.symmetric(horizontal: 15),
  //               decoration: BoxDecoration(
  //                   color: Pallet.dashsmallcontainerback,
  //                   borderRadius: BorderRadius.circular(10)),
  //               width: wdgtWidth * 0.40,
  //               height: wdgtHeight * 0.30,
  //               child: Column(
  //                 mainAxisAlignment: MainAxisAlignment.center,
  //                 crossAxisAlignment: CrossAxisAlignment.start,
  //                 children: [
  //                   MyText(dashboard.pageDetails["text32"],
  //                       style: TextStyle(
  //                           fontSize: Pallet.heading6,
  //                           fontWeight: Pallet.font500,
  //                           color: Pallet.fontcolor)),
  //                   SizedBox(height: 10),
  //                   MyText("251",

  //                       ///server
  //                       style: TextStyle(
  //                           fontSize: Pallet.heading4,
  //                           fontWeight: Pallet.font500,
  //                           color: Pallet.fontcolor)),
  //                 ],
  //               ),
  //             ),
  //           ],
  //         ),
  //         // Row(
  //         //   mainAxisAlignment: MainAxisAlignment.center,
  //         //   children: [
  //         //     FlatButton(
  //         //       minWidth: 150.0,
  //         //       color: Pallet.buttonback,
  //         //       textColor: Pallet.fontcolornew,
  //         //       splashColor: Pallet.buttonsplashcolor,
  //         //       onPressed: () {
  //         //         Navigator.push(
  //         //           context,
  //         //           MaterialPageRoute(
  //         //               builder: (context) => Home(route: 'applycct')),
  //         //         );
  //         //       },
  //         //       // onPressed: () async {
  //         //       //   await showDialog(context: context, builder: (_) => Redeem());
  //         //       // },
  //         //       shape: RoundedRectangleBorder(
  //         //         borderRadius: BorderRadius.circular(5.0),
  //         //       ),
  //         //       child: MyText(
  //         //         dashboard.pageDetails["text33"],
  //         //         style: TextStyle(
  //         //             fontSize: Pallet.heading4, fontWeight: Pallet.font500),
  //         //       ),
  //         //     ),
  //         //   ],
  //         // ),
  //       ],
  //     ),
  //   );
  // }

  Widget startgetting({double wdgtWidth, double wdgtHeight, popheading}) {
    return Container(
        // padding: EdgeInsets.symmetric(horizontal: 15),
        decoration: BoxDecoration(
            boxShadow: [Pallet.shadowEffect],
            color: Pallet.dashcontainerback,
            borderRadius: BorderRadius.circular(10)),
        width: wdgtWidth,
        height: wdgtHeight,
        child: Container(
          padding: EdgeInsets.all(20.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              // MyText(
              //   'Network Partner',
              //   style: TextStyle(
              //       fontSize: Pallet.heading3,
              //       fontWeight: Pallet.font500,
              //       color: Pallet.fontcolor),
              // ),
              // SizedBox(height: 10),
              MyText(
                text: dashboard.pageDetails["text75"],
                style: TextStyle(
                  fontSize: Pallet.heading5,
                  height: 1.6,
                  // fontWeight: Pallet.font500,
                  color: Pallet.fontcolor,
                ),
              ),
              SizedBox(height: 20),
              InkWell(
                // onTap: () {
                //   launch("https://google.com");
                // },
                child: MyText(
                  text: dashboard.pageDetails["text76"],
                  style: TextStyle(
                    fontSize: Pallet.heading5,
                    fontWeight: Pallet.font500,
                    color: Pallet.fontcolor,
                    decoration: TextDecoration.underline,
                  ),
                ),
              ),
              SizedBox(height: 20),
              Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  CustomButton(
                    vpadding: 10,
                    // width: 150,
                    fontweight: Pallet.subheading1wgt,
                    textcolor: Pallet.fontcolornew,
                    textsize: Pallet.heading6,
                    child: Row(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        Icon(
                          Icons.video_call,
                          color: Pallet.fontcolornew,
                          size: 15.0,
                        ),
                        SizedBox(width: 3.0),
                        MyText(
                          text: dashboard.pageDetails["text2"],
                          style: TextStyle(
                              color: Pallet.fontcolornew,
                              fontSize: Pallet.heading6,
                              fontWeight: Pallet.subheading1wgt),
                        ),
                      ],
                    ),
                    onpress: () {
                      showDialog(
                        context: context,
                        builder: (BuildContext context) {
                          return AlertDialog(
                            elevation: 24.0,
                            backgroundColor: Pallet.popupcontainerback,
                            shape: RoundedRectangleBorder(
                                borderRadius:
                                    BorderRadius.all(Radius.circular(15.0))),
                            title: Row(
                              children: [
                                Image.asset("c-logo.png", width: 40),
                                SizedBox(width: 10),
                                MyText(
                                    text: dashboard.pageDetails["text2"],
                                    style: TextStyle(
                                        fontSize: popheading,
                                        color: Pallet.fontcolor,
                                        fontWeight: Pallet.font500)),
                              ],
                            ),
                            content: Container(
                              width: 455.0,
                              child: SingleChildScrollView(
                                child: ListBody(
                                  children: <Widget>[
                                    MyText(
                                        text: dashboard.pageDetails["text123"],
                                        textAlign: TextAlign.left,
                                        style: TextStyle(
                                          fontSize: popheading / 1.03,
                                          height: 1.5,
                                          color: Pallet.fontcolor,
                                        )),
                                    Center(
                                        child: attendwebinarpopup(
                                            popheading: popheading)),
                                    MyText(
                                        text: dashboard.pageDetails["text124"],
                                        textAlign: TextAlign.left,
                                        style: TextStyle(
                                            fontSize: popheading / 1.03,
                                            color: Pallet.fontcolor,
                                            fontWeight: Pallet.font500)),
                                  ],
                                ),
                              ),
                            ),
                            actions: [
                              Row(
                                children: [
                                  PopupButton(
                                    onpress: () {
                                      launch(dashboard.zoomlink.toString());
                                    },
                                    child: Center(
                                      child: Row(
                                        children: [
                                          Icon(
                                            Icons.video_call,
                                            color: Pallet.fontcolornew,
                                            size: 18.0,
                                          ),
                                          SizedBox(width: 10.0),
                                          MyText(
                                            text:
                                                dashboard.pageDetails["text2"],
                                            style: TextStyle(
                                              fontSize: Pallet.heading6,
                                              color: Pallet.fontcolornew,
                                              fontWeight: Pallet.font500,
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                  SizedBox(width: 9),
                                  PopupButton(
                                    text: dashboard.pageDetails["text78"],
                                    onpress: () {
                                      Navigator.of(context).pop();
                                    },
                                  ),
                                  // SizedBox(height: 9),
                                ],
                              ),
                            ],
                          );
                        },
                      );
                    },
                  ),
                  SizedBox(height: 10),
                  CustomButton(
                    width: 220,
                    vpadding: 10,
                    text: dashboard.pageDetails["text77"],
                    fontweight: Pallet.subheading1wgt,
                    textcolor: Pallet.fontcolornew,
                    textsize: Pallet.heading6,
                    onpress: () {
                      networkPartner(context, setState);
                    },
                  ),
                ],
              ),
            ],
          ),
        ));
  }

  Widget latestpackage(
      {double wdgtWidth, double wdgtHeight, List latestpackages}) {
    return Container(
        padding: EdgeInsets.only(
          right: 20,
          left: 20,
          bottom: 20,
          top: 0,
        ),
        decoration: BoxDecoration(
            boxShadow: [Pallet.shadowEffect],
            color: Pallet.dashcontainerback,
            borderRadius: BorderRadius.circular(10)),
        width: wdgtWidth,
        height: wdgtHeight,
        child: Container(
          height: wdgtHeight - 0.003,
          child: Dlist(
            headtitle: dashboard.pageDetails["text34"],
            listofvalues: dashboard.lastPackages,
            listvalkey1: 'price',
            listvalkey2: 'country_name',
            title1: dashboard.pageDetails["text52"],
            title2: dashboard.pageDetails["text36"],
            // img1: 'profile_image',
            img2: 'country_flag',
            footer1: dashboard.pageDetails["text37"],
            footer2: dashboard.totalPackagesCount
                .toString()
                .split('.')[0]
                .toString(),
            // firsticonenable: true,
            secondiconenable: true,
          ),
        )

        // child: Column(
        //   crossAxisAlignment: CrossAxisAlignment.start,
        //   // mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        //   children: [
        //     Padding(
        //       padding: const EdgeInsets.only(left: 20.0, bottom: 20.0, top: 20),
        //       child: Row(
        //         children: [
        //           MyText(dashboard.pageDetails["text34"],
        //               style: TextStyle(
        //                   fontSize: Pallet.heading3,
        //                   fontWeight: Pallet.font500,
        //                   color: Pallet.fontcolor))
        //         ],
        //       ),
        //     ),
        //     Row(
        //       mainAxisAlignment: MainAxisAlignment.center,
        //       children: [
        //         Container(
        //           decoration: BoxDecoration(
        //               color: Pallet.lastpackheadingback,
        //               borderRadius: BorderRadius.circular(5)),
        //           width: wdgtWidth * 0.90,
        //           height: wdgtWidth * 0.07,
        //           child: Row(
        //             mainAxisAlignment: MainAxisAlignment.spaceAround,
        //             children: [
        //               Container(
        //                 child: MyText(dashboard.pageDetails["text52"],
        //                     style: TextStyle(
        //                         fontSize: Pallet.heading5,
        //                         fontWeight: Pallet.font500,
        //                         color: Pallet.fontcolor)),
        //               ),
        //               Container(
        //                 child: MyText(dashboard.pageDetails["text36"],
        //                     style: TextStyle(
        //                         fontSize: Pallet.heading5,
        //                         fontWeight: Pallet.font500,
        //                         color: Pallet.fontcolor)),
        //               ),
        //             ],
        //           ),
        //         ),
        //       ],
        //     ),
        //     SizedBox(height: 13),
        //     Row(
        //       mainAxisAlignment: MainAxisAlignment.center,
        //       children: [
        //         Container(
        //           decoration: BoxDecoration(
        //               // color: Pallet.lastpackheadingback,
        //               borderRadius: BorderRadius.circular(5)),
        //           width: wdgtWidth * 0.90,
        //           height: wdgtHeight * 0.55,
        //           child: Column(
        //             children: [
        //               for (var pack in dashboard.lastPackages)
        //                 Padding(
        //                   padding: EdgeInsets.only(bottom: wdgtHeight * 0.03),
        //                   child: Container(
        //                     decoration: BoxDecoration(
        //                         color: Pallet.lastpackback,
        //                         borderRadius: BorderRadius.circular(5)),
        //                     width: wdgtWidth * 0.90,
        //                     height: wdgtHeight * 0.08,
        //                     child: Row(
        //                       mainAxisAlignment: MainAxisAlignment.spaceAround,
        //                       children: [
        //                         Container(
        //                           width: wdgtWidth * 0.15,
        //                           child: Row(
        //                             children: [
        //                               MyText(pack['price'].toString(),
        //                                   style: TextStyle(
        //                                       fontSize: Pallet.heading5,
        //                                       fontWeight: Pallet.font500,
        //                                       color: Pallet.fontcolor)),
        //                             ],
        //                           ),
        //                         ),
        //                         Container(
        //                           width: wdgtWidth * 0.21,
        //                           child: Row(
        //                             mainAxisAlignment: MainAxisAlignment.start,
        //                             children: [
        //                               Container(
        //                                 width: 25,
        //                                 height: 25,
        //                                 child: Image.network(
        //                                     appSettings['SERVER_URL'] +
        //                                         '/' +
        //                                         pack['country_flag']),
        //                               ),
        //                               SizedBox(width: 10),
        //                               MyText(pack['country_name'],
        //                                   style: TextStyle(
        //                                       fontSize: Pallet.heading5,
        //                                       fontWeight: Pallet.font500,
        //                                       color: Pallet.fontcolor)),
        //                             ],
        //                           ),
        //                         ),
        //                       ],
        //                     ),
        //                   ),
        //                 ),
        //             ],
        //           ),
        //         ),
        //       ],
        //     ),
        //     Row(
        //       mainAxisAlignment: MainAxisAlignment.center,
        //       children: [
        //         Container(
        //           decoration: BoxDecoration(
        //               color: Pallet.lastpackfooterback,
        //               borderRadius: BorderRadius.circular(5)),
        //           width: wdgtWidth * 0.90,
        //           height: wdgtHeight * 0.09,
        //           child: Row(
        //             mainAxisAlignment: MainAxisAlignment.spaceAround,
        //             children: [
        //               Container(
        //                 child: MyText(dashboard.pageDetails["text37"],
        //                     style: TextStyle(
        //                         fontSize: Pallet.heading3,
        //                         fontWeight: Pallet.font500,
        //                         color: Pallet.fontcolornew)),
        //               ),
        //               Container(
        //                 child: MyText(
        //                     dashboard.totalPackagesCount.toString().split('.')[0],
        //                     style: TextStyle(
        //                         fontSize: Pallet.heading3,
        //                         fontWeight: Pallet.font500,
        //                         color: Pallet.fontcolornew)),
        //               ),
        //             ],
        //           ),
        //         ),
        //       ],
        //     ),
        //   ],
        // ),
        );
  }

  // Widget latestpackagemobile(
  //     {double wdgtWidth, double wdgtHeight, List latestpackages}) {
  //   return Container(
  //     padding: EdgeInsets.all(10.0),
  //     decoration: BoxDecoration(
  //         boxShadow: [Pallet.shadowEffect],
  //         color: Pallet.dashcontainerback,
  //         borderRadius: BorderRadius.circular(20)),
  //     width: wdgtWidth,
  //     height: wdgtHeight,
  //     child: Column(
  //       crossAxisAlignment: CrossAxisAlignment.start,
  //       // mainAxisAlignment: MainAxisAlignment.spaceEvenly,
  //       children: [
  //         Padding(
  //           padding: const EdgeInsets.only(left: 20.0, bottom: 20.0, top: 20),
  //           child: Row(
  //             children: [
  //               MyText(dashboard.pageDetails["text34"],
  //                   style: TextStyle(
  //                       fontSize: Pallet.heading3,
  //                       fontWeight: Pallet.font500,
  //                       color: Pallet.fontcolor))
  //             ],
  //           ),
  //         ),
  //         Row(
  //           mainAxisAlignment: MainAxisAlignment.center,
  //           children: [
  //             Container(
  //               decoration: BoxDecoration(
  //                   color: Pallet.lastpackheadingback,
  //                   borderRadius: BorderRadius.circular(5)),
  //               width: wdgtWidth * 0.90,
  //               height: wdgtWidth * 0.09,
  //               child: Row(
  //                 mainAxisAlignment: MainAxisAlignment.spaceAround,
  //                 children: [
  //                   Container(
  //                     child: MyText(dashboard.pageDetails["text52"],
  //                         style: TextStyle(
  //                             fontSize: Pallet.heading5,
  //                             fontWeight: Pallet.font500,
  //                             color: Pallet.fontcolor)),
  //                   ),
  //                   Container(
  //                     child: MyText(dashboard.pageDetails["text36"],
  //                         style: TextStyle(
  //                             fontSize: Pallet.heading5,
  //                             fontWeight: Pallet.font500,
  //                             color: Pallet.fontcolor)),
  //                   ),
  //                 ],
  //               ),
  //             ),
  //           ],
  //         ),
  //         SizedBox(height: 13),
  //         Row(
  //           mainAxisAlignment: MainAxisAlignment.center,
  //           children: [
  //             Container(
  //               decoration: BoxDecoration(
  //                   // color: Pallet.lastpackback,
  //                   borderRadius: BorderRadius.circular(5)),
  //               width: wdgtWidth * 0.90,
  //               height: wdgtHeight * 0.55,
  //               child: Column(
  //                 children: [
  //                   for (var pack in dashboard.lastPackages)
  //                     Padding(
  //                       padding: EdgeInsets.only(bottom: wdgtHeight * 0.03),
  //                       child: Container(
  //                         decoration: BoxDecoration(
  //                             color: Pallet.lastpackback,
  //                             borderRadius: BorderRadius.circular(5)),
  //                         width: wdgtWidth * 0.90,
  //                         height: wdgtHeight * 0.08,
  //                         child: Row(
  //                           mainAxisAlignment: MainAxisAlignment.spaceAround,
  //                           children: [
  //                             Container(
  //                               width: wdgtWidth * 0.15,
  //                               child: Row(
  //                                 children: [
  //                                   MyText(pack['price'].toString(),
  //                                       style: TextStyle(
  //                                           fontSize: Pallet.heading5,
  //                                           fontWeight: Pallet.font500,
  //                                           color: Pallet.fontcolor)),
  //                                 ],
  //                               ),
  //                             ),
  //                             Container(
  //                               width: wdgtWidth * 0.21,
  //                               child: Row(
  //                                 mainAxisAlignment: MainAxisAlignment.start,
  //                                 children: [
  //                                   Container(
  //                                     width: 25,
  //                                     height: 25,
  //                                     child: Image.network(
  //                                         appSettings['SERVER_URL'] +
  //                                             '/' +
  //                                             pack['country_flag']),
  //                                   ),
  //                                   SizedBox(width: 10),
  //                                   MyText(pack['country_name'],
  //                                       style: TextStyle(
  //                                           fontSize: Pallet.heading5,
  //                                           fontWeight: Pallet.font500,
  //                                           color: Pallet.fontcolor)),
  //                                 ],
  //                               ),
  //                             ),
  //                           ],
  //                         ),
  //                       ),
  //                     ),
  //                 ],
  //               ),
  //             ),
  //           ],
  //         ),
  //         Row(
  //           mainAxisAlignment: MainAxisAlignment.center,
  //           children: [
  //             Container(
  //               decoration: BoxDecoration(
  //                   color: Pallet.lastpackfooterback,
  //                   borderRadius: BorderRadius.circular(5)),
  //               width: wdgtWidth * 0.90,
  //               height: wdgtHeight * 0.09,
  //               child: Row(
  //                 mainAxisAlignment: MainAxisAlignment.spaceAround,
  //                 children: [
  //                   Container(
  //                     child: MyText(dashboard.pageDetails["text37"],
  //                         style: TextStyle(
  //                             fontSize: Pallet.heading4,
  //                             fontWeight: Pallet.font500,
  //                             color: Pallet.fontcolornew)),
  //                   ),
  //                   Container(
  //                     child: MyText(
  //                         dashboard.totalPackagesCount.toString().split('.')[0],
  //                         style: TextStyle(
  //                             fontSize: Pallet.heading4,
  //                             fontWeight: Pallet.font500,
  //                             color: Pallet.fontcolornew)),
  //                   ),
  //                 ],
  //               ),
  //             ),
  //           ],
  //         ),
  //       ],
  //     ),
  //   );
  // }

  // Widget latestmembertotal(
  //     {double wdgtWidth, double wdgtHeight, tableheadingwth, tableheadinghth}) {
  //   return Container(
  //     padding: EdgeInsets.all(10.0),
  //     decoration: BoxDecoration(
  //         boxShadow: [Pallet.shadowEffect],
  //         color: Pallet.dashcontainerback,
  //         borderRadius: BorderRadius.circular(10)),
  //     width: wdgtWidth,
  //     height: wdgtHeight,
  //     child: Column(
  //       crossAxisAlignment: CrossAxisAlignment.start,
  //       mainAxisAlignment: MainAxisAlignment.spaceEvenly,
  //       children: [
  //         Row(
  //           mainAxisAlignment: MainAxisAlignment.spaceEvenly,
  //           crossAxisAlignment: CrossAxisAlignment.start,
  //           children: [
  //             Container(
  //               padding: EdgeInsets.all(10.0),
  //               decoration: BoxDecoration(
  //                   color: Pallet.dashcontainerback,
  //                   borderRadius: BorderRadius.circular(10)),
  //               width: wdgtWidth * 0.3,
  //               height: wdgtHeight * 0.9,
  //               child: Column(
  //                 crossAxisAlignment: CrossAxisAlignment.start,
  //                 mainAxisAlignment: MainAxisAlignment.spaceEvenly,
  //                 children: [
  //                   Padding(
  //                     padding: const EdgeInsets.only(left: 10.0, bottom: 10.0),
  //                     child: Row(
  //                       children: [
  //                         MyText(dashboard.pageDetails["text38"],
  //                             style: TextStyle(
  //                                 fontSize: Pallet.heading3,
  //                                 fontWeight: Pallet.font500,
  //                                 color: Pallet.fontcolor))
  //                       ],
  //                     ),
  //                   ),
  //                   Row(
  //                     mainAxisAlignment: MainAxisAlignment.center,
  //                     children: [
  //                       Container(
  //                         decoration: BoxDecoration(
  //                             color: Pallet.lastpackheadingback,
  //                             borderRadius: BorderRadius.circular(5)),
  //                         width: tableheadingwth,
  //                         height: tableheadinghth,
  //                         child: Row(
  //                           mainAxisAlignment: MainAxisAlignment.spaceAround,
  //                           children: [
  //                             Container(
  //                               child: MyText(dashboard.pageDetails["text39"],
  //                                   style: TextStyle(
  //                                       fontSize: Pallet.heading5,
  //                                       fontWeight: Pallet.font500,
  //                                       color: Pallet.fontcolor)),
  //                             ),
  //                             Container(
  //                               child: MyText(dashboard.pageDetails["text36"],
  //                                   style: TextStyle(
  //                                       fontSize: Pallet.heading5,
  //                                       fontWeight: Pallet.font500,
  //                                       color: Pallet.fontcolor)),
  //                             ),
  //                           ],
  //                         ),
  //                       ),
  //                     ],
  //                   ),
  //                   if (dashboard.latestmember.length > 0)
  //                     for (var member in dashboard.latestmember)
  //                       SingleChildScrollView(
  //                         child: Row(
  //                           mainAxisAlignment: MainAxisAlignment.center,
  //                           children: [
  //                             Container(
  //                               decoration: BoxDecoration(
  //                                   color: Pallet.lastpackback,
  //                                   borderRadius: BorderRadius.circular(5)),
  //                               width: wdgtWidth * 0.28,
  //                               height: wdgtHeight * 0.05,
  //                               child: Row(
  //                                 mainAxisAlignment:
  //                                     MainAxisAlignment.spaceAround,
  //                                 children: [
  //                                   Column(
  //                                     mainAxisAlignment:
  //                                         MainAxisAlignment.center,
  //                                     crossAxisAlignment:
  //                                         CrossAxisAlignment.start,
  //                                     children: [
  //                                       Container(
  //                                         width: wdgtWidth * 0.10,
  //                                         child: Row(
  //                                           children: [
  //                                             CircleAvatar(
  //                                               backgroundColor:
  //                                                   Colors.transparent,
  //                                               backgroundImage: NetworkImage(
  //                                                   'https://widgetwhats.com/app/uploads/2019/11/free-profile-photo-whatsapp-4.png'),
  //                                               radius: 13,
  //                                             ),
  //                                             SizedBox(width: 10.0),
  //                                             Container(
  //                                               child: MyText(
  //                                                   member["user_name"]
  //                                                           .substring(0, 1)
  //                                                           .toUpperCase() +
  //                                                       member["user_name"]
  //                                                           .substring(1)
  //                                                           .toLowerCase(),
  //                                                   overflow:
  //                                                       TextOverflow.ellipsis,
  //                                                   style: TextStyle(
  //                                                       fontSize:
  //                                                           Pallet.heading5,
  //                                                       fontWeight:
  //                                                           Pallet.font500,
  //                                                       color:
  //                                                           Pallet.fontcolor)),
  //                                             ),
  //                                           ],
  //                                         ),
  //                                       ),
  //                                     ],
  //                                   ),
  //                                   Column(
  //                                     mainAxisAlignment:
  //                                         MainAxisAlignment.center,
  //                                     crossAxisAlignment:
  //                                         CrossAxisAlignment.start,
  //                                     children: [
  //                                       Container(
  //                                         width: wdgtWidth * 0.10,
  //                                         child: Row(
  //                                           mainAxisAlignment:
  //                                               MainAxisAlignment.start,
  //                                           children: [
  //                                             Container(
  //                                               width: 25,
  //                                               height: 25,
  //                                               child: Image.network(
  //                                                   appSettings['SERVER_URL'] +
  //                                                       '/' +
  //                                                       member['country_flag']),
  //                                             ),
  //                                             SizedBox(width: 10),
  //                                             Container(
  //                                               child: MyText(
  //                                                   member["country_name"],
  //                                                   overflow:
  //                                                       TextOverflow.ellipsis,
  //                                                   style: TextStyle(
  //                                                       fontSize:
  //                                                           Pallet.heading5,
  //                                                       fontWeight:
  //                                                           Pallet.font500,
  //                                                       color:
  //                                                           Pallet.fontcolor)),
  //                                             ),
  //                                           ],
  //                                         ),
  //                                       ),
  //                                     ],
  //                                   ),
  //                                 ],
  //                               ),
  //                             ),
  //                           ],
  //                         ),
  //                       ),
  //                   Row(
  //                     mainAxisAlignment: MainAxisAlignment.center,
  //                     children: [
  //                       Container(
  //                         decoration: BoxDecoration(
  //                             color: Pallet.lastpackfooterback,
  //                             borderRadius: BorderRadius.circular(5)),
  //                         width: wdgtWidth * 0.28,
  //                         height: wdgtHeight * 0.05,
  //                         child: Row(
  //                           mainAxisAlignment: MainAxisAlignment.spaceAround,
  //                           children: [
  //                             Container(
  //                               child: MyText(dashboard.pageDetails["text41"],
  //                                   style: TextStyle(
  //                                       fontSize: Pallet.heading2,
  //                                       fontWeight: Pallet.font500,
  //                                       color: Pallet.fontcolornew)),
  //                             ),
  //                             Container(
  //                               child: MyText(
  //                                   dashboard.totalPartnersCount.toString(),
  //                                   style: TextStyle(
  //                                       fontSize: Pallet.heading2,
  //                                       fontWeight: Pallet.font500,
  //                                       color: Pallet.fontcolornew)),
  //                             ),
  //                           ],
  //                         ),
  //                       ),
  //                     ],
  //                   ),
  //                 ],
  //               ),
  //             ),
  //             Container(
  //               padding: EdgeInsets.all(10.0),
  //               decoration: BoxDecoration(
  //                   color: Pallet.dashcontainerback,
  //                   borderRadius: BorderRadius.circular(10)),
  //               width: wdgtWidth * 0.3,
  //               height: wdgtHeight * 0.9,
  //               child: Column(
  //                 crossAxisAlignment: CrossAxisAlignment.start,
  //                 mainAxisAlignment: MainAxisAlignment.spaceEvenly,
  //                 children: [
  //                   Padding(
  //                     padding: const EdgeInsets.only(left: 10.0, bottom: 10.0),
  //                     child: Row(
  //                       children: [
  //                         MyText(dashboard.pageDetails["text38"],
  //                             style: TextStyle(
  //                                 fontSize: Pallet.heading3,
  //                                 fontWeight: Pallet.font500,
  //                                 color: Pallet.fontcolor))
  //                       ],
  //                     ),
  //                   ),
  //                   Row(
  //                     mainAxisAlignment: MainAxisAlignment.center,
  //                     children: [
  //                       Container(
  //                         decoration: BoxDecoration(
  //                             color: Pallet.lastpackheadingback,
  //                             borderRadius: BorderRadius.circular(5)),
  //                         width: wdgtWidth * 0.28,
  //                         height: wdgtHeight * 0.05,
  //                         child: Row(
  //                           mainAxisAlignment: MainAxisAlignment.spaceAround,
  //                           children: [
  //                             Container(
  //                               child: MyText(dashboard.pageDetails["text39"],
  //                                   style: TextStyle(
  //                                       fontSize: Pallet.heading5,
  //                                       fontWeight: Pallet.font500,
  //                                       color: Pallet.fontcolor)),
  //                             ),
  //                             Container(
  //                               child: MyText(dashboard.pageDetails["text36"],
  //                                   style: TextStyle(
  //                                       fontSize: Pallet.heading5,
  //                                       fontWeight: Pallet.font500,
  //                                       color: Pallet.fontcolor)),
  //                             ),
  //                           ],
  //                         ),
  //                       ),
  //                     ],
  //                   ),
  //                   if (dashboard.latestmember.length > 0)
  //                     for (var member in dashboard.latestmember)
  //                       SingleChildScrollView(
  //                         child: Row(
  //                           mainAxisAlignment: MainAxisAlignment.center,
  //                           children: [
  //                             Container(
  //                               decoration: BoxDecoration(
  //                                   color: Pallet.lastpackback,
  //                                   borderRadius: BorderRadius.circular(5)),
  //                               width: wdgtWidth * 0.28,
  //                               height: wdgtHeight * 0.05,
  //                               child: Row(
  //                                 mainAxisAlignment:
  //                                     MainAxisAlignment.spaceAround,
  //                                 children: [
  //                                   Column(
  //                                     mainAxisAlignment:
  //                                         MainAxisAlignment.center,
  //                                     crossAxisAlignment:
  //                                         CrossAxisAlignment.start,
  //                                     children: [
  //                                       Container(
  //                                         width: wdgtWidth * 0.10,
  //                                         child: Row(
  //                                           children: [
  //                                             CircleAvatar(
  //                                               backgroundColor:
  //                                                   Colors.transparent,
  //                                               backgroundImage: NetworkImage(
  //                                                   'https://widgetwhats.com/app/uploads/2019/11/free-profile-photo-whatsapp-4.png'),
  //                                               radius: 13,
  //                                             ),
  //                                             SizedBox(width: 10.0),
  //                                             Container(
  //                                               child: MyText(member["user_name"],
  //                                                   overflow:
  //                                                       TextOverflow.ellipsis,
  //                                                   style: TextStyle(
  //                                                       fontSize:
  //                                                           Pallet.heading5,
  //                                                       fontWeight:
  //                                                           Pallet.font500,
  //                                                       color:
  //                                                           Pallet.fontcolor)),
  //                                             ),
  //                                           ],
  //                                         ),
  //                                       ),
  //                                     ],
  //                                   ),
  //                                   Column(
  //                                     mainAxisAlignment:
  //                                         MainAxisAlignment.center,
  //                                     crossAxisAlignment:
  //                                         CrossAxisAlignment.start,
  //                                     children: [
  //                                       Container(
  //                                         width: wdgtWidth * 0.10,
  //                                         child: Row(
  //                                           mainAxisAlignment:
  //                                               MainAxisAlignment.start,
  //                                           children: [
  //                                             Container(
  //                                               width: 25,
  //                                               height: 25,
  //                                               child: Image.network(
  //                                                   appSettings['SERVER_URL'] +
  //                                                       '/' +
  //                                                       member['country_flag']),
  //                                             ),
  //                                             SizedBox(width: 10),
  //                                             Container(
  //                                               child: MyText(
  //                                                   member["country_name"],
  //                                                   overflow:
  //                                                       TextOverflow.ellipsis,
  //                                                   style: TextStyle(
  //                                                       fontSize:
  //                                                           Pallet.heading5,
  //                                                       fontWeight:
  //                                                           Pallet.font500,
  //                                                       color:
  //                                                           Pallet.fontcolor)),
  //                                             ),
  //                                           ],
  //                                         ),
  //                                       ),
  //                                     ],
  //                                   ),
  //                                 ],
  //                               ),
  //                             ),
  //                           ],
  //                         ),
  //                       ),
  //                   Row(
  //                     mainAxisAlignment: MainAxisAlignment.center,
  //                     children: [
  //                       Container(
  //                         decoration: BoxDecoration(
  //                             color: Pallet.lastpackfooterback,
  //                             borderRadius: BorderRadius.circular(5)),
  //                         width: wdgtWidth * 0.28,
  //                         height: wdgtHeight * 0.05,
  //                         child: Row(
  //                           mainAxisAlignment: MainAxisAlignment.spaceAround,
  //                           children: [
  //                             Container(
  //                               child: MyText(dashboard.pageDetails["text41"],
  //                                   style: TextStyle(
  //                                       fontSize: Pallet.heading2,
  //                                       fontWeight: Pallet.font500,
  //                                       color: Pallet.fontcolornew)),
  //                             ),
  //                             Container(
  //                               child: MyText(
  //                                   dashboard.totalPartnersCount.toString(),
  //                                   style: TextStyle(
  //                                       fontSize: Pallet.heading2,
  //                                       fontWeight: Pallet.font500,
  //                                       color: Pallet.fontcolornew)),
  //                             ),
  //                           ],
  //                         ),
  //                       ),
  //                     ],
  //                   ),
  //                 ],
  //               ),
  //             ),
  //             Container(
  //               padding: EdgeInsets.all(10.0),
  //               decoration: BoxDecoration(
  //                   color: Pallet.dashcontainerback,
  //                   borderRadius: BorderRadius.circular(10)),
  //               width: wdgtWidth * 0.3,
  //               height: wdgtHeight * 0.9,
  //               child: Column(
  //                 crossAxisAlignment: CrossAxisAlignment.start,
  //                 mainAxisAlignment: MainAxisAlignment.spaceEvenly,
  //                 children: [
  //                   Padding(
  //                     padding: const EdgeInsets.only(left: 10.0, bottom: 10.0),
  //                     child: Row(
  //                       children: [
  //                         MyText(dashboard.pageDetails["text38"],
  //                             style: TextStyle(
  //                                 fontSize: Pallet.heading3,
  //                                 fontWeight: Pallet.font500,
  //                                 color: Pallet.fontcolor))
  //                       ],
  //                     ),
  //                   ),
  //                   Row(
  //                     mainAxisAlignment: MainAxisAlignment.center,
  //                     children: [
  //                       Container(
  //                         decoration: BoxDecoration(
  //                             color: Pallet.lastpackheadingback,
  //                             borderRadius: BorderRadius.circular(5)),
  //                         width: wdgtWidth * 0.28,
  //                         height: wdgtHeight * 0.05,
  //                         child: Row(
  //                           mainAxisAlignment: MainAxisAlignment.spaceAround,
  //                           children: [
  //                             Container(
  //                               child: MyText(dashboard.pageDetails["text39"],
  //                                   style: TextStyle(
  //                                       fontSize: Pallet.heading5,
  //                                       fontWeight: Pallet.font500,
  //                                       color: Pallet.fontcolor)),
  //                             ),
  //                             Container(
  //                               child: MyText(dashboard.pageDetails["text36"],
  //                                   style: TextStyle(
  //                                       fontSize: Pallet.heading5,
  //                                       fontWeight: Pallet.font500,
  //                                       color: Pallet.fontcolor)),
  //                             ),
  //                           ],
  //                         ),
  //                       ),
  //                     ],
  //                   ),
  //                   if (dashboard.latestmember.length > 0)
  //                     for (var member in dashboard.latestmember)
  //                       SingleChildScrollView(
  //                         child: Row(
  //                           mainAxisAlignment: MainAxisAlignment.center,
  //                           children: [
  //                             Container(
  //                               decoration: BoxDecoration(
  //                                   color: Pallet.lastpackback,
  //                                   borderRadius: BorderRadius.circular(5)),
  //                               width: wdgtWidth * 0.28,
  //                               height: wdgtHeight * 0.05,
  //                               child: Row(
  //                                 mainAxisAlignment:
  //                                     MainAxisAlignment.spaceAround,
  //                                 children: [
  //                                   Column(
  //                                     mainAxisAlignment:
  //                                         MainAxisAlignment.center,
  //                                     crossAxisAlignment:
  //                                         CrossAxisAlignment.start,
  //                                     children: [
  //                                       Container(
  //                                         width: wdgtWidth * 0.10,
  //                                         child: Row(
  //                                           children: [
  //                                             CircleAvatar(
  //                                               backgroundColor:
  //                                                   Colors.transparent,
  //                                               backgroundImage: NetworkImage(
  //                                                   'https://widgetwhats.com/app/uploads/2019/11/free-profile-photo-whatsapp-4.png'),
  //                                               radius: 13,
  //                                             ),
  //                                             SizedBox(width: 10.0),
  //                                             Container(
  //                                               child: MyText(member["user_name"],
  //                                                   overflow:
  //                                                       TextOverflow.ellipsis,
  //                                                   style: TextStyle(
  //                                                       fontSize:
  //                                                           Pallet.heading5,
  //                                                       fontWeight:
  //                                                           Pallet.font500,
  //                                                       color:
  //                                                           Pallet.fontcolor)),
  //                                             ),
  //                                           ],
  //                                         ),
  //                                       ),
  //                                     ],
  //                                   ),
  //                                   Column(
  //                                     mainAxisAlignment:
  //                                         MainAxisAlignment.center,
  //                                     crossAxisAlignment:
  //                                         CrossAxisAlignment.start,
  //                                     children: [
  //                                       Container(
  //                                         width: wdgtWidth * 0.10,
  //                                         child: Row(
  //                                           mainAxisAlignment:
  //                                               MainAxisAlignment.start,
  //                                           children: [
  //                                             Container(
  //                                               width: 25,
  //                                               height: 25,
  //                                               child: Image.network(
  //                                                   appSettings['SERVER_URL'] +
  //                                                       '/' +
  //                                                       member['country_flag']),
  //                                             ),
  //                                             SizedBox(width: 10),
  //                                             Container(
  //                                               child: MyText(
  //                                                   member["country_name"],
  //                                                   overflow:
  //                                                       TextOverflow.ellipsis,
  //                                                   style: TextStyle(
  //                                                       fontSize:
  //                                                           Pallet.heading5,
  //                                                       fontWeight:
  //                                                           Pallet.font500,
  //                                                       color:
  //                                                           Pallet.fontcolor)),
  //                                             ),
  //                                           ],
  //                                         ),
  //                                       ),
  //                                     ],
  //                                   ),
  //                                 ],
  //                               ),
  //                             ),
  //                           ],
  //                         ),
  //                       ),
  //                   Row(
  //                     mainAxisAlignment: MainAxisAlignment.center,
  //                     children: [
  //                       Container(
  //                         decoration: BoxDecoration(
  //                             color: Pallet.lastpackfooterback,
  //                             borderRadius: BorderRadius.circular(5)),
  //                         width: wdgtWidth * 0.28,
  //                         height: wdgtHeight * 0.05,
  //                         child: Row(
  //                           mainAxisAlignment: MainAxisAlignment.spaceAround,
  //                           children: [
  //                             Container(
  //                               child: MyText(dashboard.pageDetails["text41"],
  //                                   style: TextStyle(
  //                                       fontSize: Pallet.heading2,
  //                                       fontWeight: Pallet.font500,
  //                                       color: Pallet.fontcolornew)),
  //                             ),
  //                             Container(
  //                               child: MyText(
  //                                   dashboard.totalPartnersCount.toString(),
  //                                   style: TextStyle(
  //                                       fontSize: Pallet.heading2,
  //                                       fontWeight: Pallet.font500,
  //                                       color: Pallet.fontcolornew)),
  //                             ),
  //                           ],
  //                         ),
  //                       ),
  //                     ],
  //                   ),
  //                 ],
  //               ),
  //             ),
  //           ],
  //         ),
  //       ],
  //     ),
  //   );
  // }

  Widget latestmember({double wdgtWidth, double wdgtHeight}) {
    return Container(
        padding: EdgeInsets.only(
          right: 20,
          left: 20,
          bottom: 20,
          top: 0,
        ),
        decoration: BoxDecoration(
            boxShadow: [Pallet.shadowEffect],
            color: Pallet.dashcontainerback,
            borderRadius: BorderRadius.circular(10)),
        width: wdgtWidth,
        height: wdgtHeight,
        child: Container(
          child: Dlist(
            headtitle: dashboard.pageDetails["text38"],
            listofvalues: dashboard.latestmember,
            listvalkey1: 'user_name',
            listvalkey2: 'country_name',
            title1: dashboard.pageDetails["text39"],
            title2: dashboard.pageDetails["text36"],
            img1: 'profile_image',
            img2: 'country_flag',
            footer1: dashboard.pageDetails["text41"],
            footer2: dashboard.totalPartnersCount.toString(),
            firsticonenable: true,
            secondiconenable: true,
          ),
        ));
  }

  // Widget latestmembermobile({double wdgtWidth, double wdgtHeight}) {
  //   return Container(
  //     padding: EdgeInsets.all(10.0),
  //     decoration: BoxDecoration(
  //         boxShadow: [Pallet.shadowEffect],
  //         color: Pallet.dashcontainerback,
  //         borderRadius: BorderRadius.circular(20)),
  //     width: wdgtWidth,
  //     height: wdgtHeight,
  //     child: Column(
  //       crossAxisAlignment: CrossAxisAlignment.start,
  //       mainAxisAlignment: MainAxisAlignment.spaceEvenly,
  //       children: [
  //         Padding(
  //           padding: const EdgeInsets.only(left: 20.0, bottom: 20.0, top: 20),
  //           child: Row(
  //             children: [
  //               MyText(dashboard.pageDetails["text38"],
  //                   style: TextStyle(
  //                       fontSize: Pallet.heading3,
  //                       fontWeight: Pallet.font500,
  //                       color: Pallet.fontcolor))
  //             ],
  //           ),
  //         ),
  //         Row(
  //           mainAxisAlignment: MainAxisAlignment.center,
  //           children: [
  //             Container(
  //               decoration: BoxDecoration(
  //                   color: Pallet.lastpackheadingback,
  //                   borderRadius: BorderRadius.circular(5)),
  //               width: wdgtWidth * 0.90,
  //               height: wdgtWidth * 0.09,
  //               child: Row(
  //                 mainAxisAlignment: MainAxisAlignment.spaceAround,
  //                 children: [
  //                   Container(
  //                     child: MyText(dashboard.pageDetails["text39"],
  //                         style: TextStyle(
  //                             fontSize: Pallet.heading5,
  //                             fontWeight: Pallet.font500,
  //                             color: Pallet.fontcolor)),
  //                   ),
  //                   Container(
  //                     child: MyText(dashboard.pageDetails["text36"],
  //                         style: TextStyle(
  //                             fontSize: Pallet.heading5,
  //                             fontWeight: Pallet.font500,
  //                             color: Pallet.fontcolor)),
  //                   ),
  //                 ],
  //               ),
  //             ),
  //           ],
  //         ),
  //         // if (dashboard.latestmember.length > 0)
  //         Row(
  //           mainAxisAlignment: MainAxisAlignment.center,
  //           children: [
  //             Container(
  //               decoration: BoxDecoration(
  //                   // color: Pallet.lastpackback,
  //                   borderRadius: BorderRadius.circular(5)),
  //               width: wdgtWidth * 0.90,
  //               height: wdgtHeight * 0.55,
  //               child: Column(
  //                 children: [
  //                   for (var member in dashboard.latestmember)
  //                     Padding(
  //                       padding: EdgeInsets.only(bottom: wdgtHeight * 0.03),
  //                       child: Container(
  //                         decoration: BoxDecoration(
  //                             color: Pallet.lastpackback,
  //                             borderRadius: BorderRadius.circular(5)),
  //                         width: wdgtWidth * 0.90,
  //                         height: wdgtHeight * 0.08,
  //                         child: Row(
  //                           mainAxisAlignment: MainAxisAlignment.spaceAround,
  //                           children: [
  //                             Column(
  //                               mainAxisAlignment: MainAxisAlignment.center,
  //                               crossAxisAlignment: CrossAxisAlignment.start,
  //                               children: [
  //                                 Container(
  //                                   width: wdgtWidth * 0.4,
  //                                   padding: const EdgeInsets.only(left: 20.0),
  //                                   child: Row(
  //                                     children: [
  //                                       CircleAvatar(
  //                                         backgroundColor: Colors.transparent,
  //                                         backgroundImage: NetworkImage(
  //                                             appSettings['SERVER_URL'] +
  //                                                 '/' +
  //                                                 member['profile_image']),
  //                                         radius: 13,
  //                                       ),
  //                                       SizedBox(width: 10.0),
  //                                       Container(
  //                                         width: wdgtWidth * 0.2,
  //                                         child: MyText(
  //                                             member["user_name"]
  //                                                 .toString()
  //                                                 .capitalizeFirstofEach,
  //                                             overflow: TextOverflow.ellipsis,
  //                                             style: TextStyle(
  //                                                 fontSize: Pallet.heading5,
  //                                                 fontWeight: Pallet.font500,
  //                                                 color: Pallet.fontcolor)),
  //                                       ),
  //                                     ],
  //                                   ),
  //                                 ),
  //                               ],
  //                             ),
  //                             Column(
  //                               mainAxisAlignment: MainAxisAlignment.center,
  //                               crossAxisAlignment: CrossAxisAlignment.start,
  //                               children: [
  //                                 Container(
  //                                   width: wdgtWidth * 0.4,
  //                                   padding: const EdgeInsets.only(left: 55.0),
  //                                   child: Row(
  //                                     mainAxisAlignment:
  //                                         MainAxisAlignment.start,
  //                                     children: [
  //                                       Container(
  //                                         width: 25,
  //                                         height: 25,
  //                                         child: Image.network(
  //                                             appSettings['SERVER_URL'] +
  //                                                 '/' +
  //                                                 member['country_flag']),
  //                                       ),

  //                                       // ImageIcon(
  //                                       //   NetworkImage(appSettings['SERVER_URL'] +
  //                                       //       '/' +
  //                                       //       member['country_flag']),
  //                                       //   color: Pallet.fontcolor,
  //                                       //   size: 30,
  //                                       // ),
  //                                       SizedBox(width: 10),
  //                                       Container(
  //                                         width: wdgtWidth * 0.1,
  //                                         child: MyText(member["country_name"],
  //                                             overflow: TextOverflow.ellipsis,
  //                                             style: TextStyle(
  //                                                 fontSize: Pallet.heading5,
  //                                                 fontWeight: Pallet.font500,
  //                                                 color: Pallet.fontcolor)),
  //                                       ),
  //                                     ],
  //                                   ),
  //                                 ),
  //                               ],
  //                             ),
  //                           ],
  //                         ),
  //                       ),
  //                     ),
  //                 ],
  //               ),
  //             ),
  //           ],
  //         ),
  //         Row(
  //           mainAxisAlignment: MainAxisAlignment.center,
  //           children: [
  //             Container(
  //               decoration: BoxDecoration(
  //                   color: Pallet.lastpackfooterback,
  //                   borderRadius: BorderRadius.circular(5)),
  //               width: wdgtWidth * 0.90,
  //               height: wdgtHeight * 0.09,
  //               child: Row(
  //                 mainAxisAlignment: MainAxisAlignment.spaceAround,
  //                 children: [
  //                   Container(
  //                     child: MyText(dashboard.pageDetails["text41"],
  //                         style: TextStyle(
  //                             fontSize: Pallet.heading4,
  //                             fontWeight: Pallet.font500,
  //                             color: Pallet.fontcolornew)),
  //                   ),
  //                   Container(
  //                     child: MyText(dashboard.totalPartnersCount.toString(),
  //                         style: TextStyle(
  //                             fontSize: Pallet.heading4,
  //                             fontWeight: Pallet.font500,
  //                             color: Pallet.fontcolornew)),
  //                   ),
  //                 ],
  //               ),
  //             ),
  //           ],
  //         ),
  //       ],
  //     ),
  //   );
  // }

  Widget latestproducts(
      {double wdgtWidth, double wdgtHeight, List latestpackages}) {
    return Container(
        padding: EdgeInsets.only(
          right: 20,
          left: 20,
          bottom: 20,
          top: 0,
        ),
        decoration: BoxDecoration(
            boxShadow: [Pallet.shadowEffect],
            color: Pallet.dashcontainerback,
            borderRadius: BorderRadius.circular(10)),
        width: wdgtWidth,
        height: wdgtHeight,
        child: Container(
          height: wdgtHeight - 0.003,
          child: Dlist(
            headtitle: dashboard.pageDetails["text79"],
            listofvalues: dashboard.lastPPackages,

            title1: dashboard.pageDetails["text52"],
            title2: dashboard.pageDetails["text36"],
            listvalkey1: 'price',
            listvalkey2: 'country_name',
            // img1: 'profile_image',
            img2: 'country_flag',
            footer1: dashboard.pageDetails["text80"],
            footer2: dashboard.totalDsvpCount.toString(),
            // firsticonenable: true,
            secondiconenable: true,
          ),
        ));

    // return Container(
    //   padding: EdgeInsets.all(10.0),
    //   decoration: BoxDecoration(
    //       boxShadow: [Pallet.shadowEffect],
    //       color: Pallet.dashcontainerback,
    //       borderRadius: BorderRadius.circular(10)),
    //   width: wdgtWidth,
    //   height: wdgtHeight,
    //   child: Column(
    //     crossAxisAlignment: CrossAxisAlignment.start,
    //     // mainAxisAlignment: MainAxisAlignment.spaceEvenly,
    //     children: [
    //       Padding(
    //         padding: const EdgeInsets.only(left: 20.0, bottom: 20.0, top: 20),
    //         child: Row(
    //           children: [
    //             MyText(dashboard.pageDetails["text79"],
    //                 style: TextStyle(
    //                     fontSize: Pallet.heading3,
    //                     fontWeight: Pallet.font500,
    //                     color: Pallet.fontcolor))
    //           ],
    //         ),
    //       ),
    //       Row(
    //         mainAxisAlignment: MainAxisAlignment.center,
    //         children: [
    //           Container(
    //             decoration: BoxDecoration(
    //                 color: Pallet.lastpackheadingback,
    //                 borderRadius: BorderRadius.circular(5)),
    //             width: wdgtWidth * 0.90,
    //             height: wdgtWidth * 0.07,
    //             child: Row(
    //               mainAxisAlignment: MainAxisAlignment.spaceAround,
    //               children: [
    //                 Container(
    //                   child: MyText(dashboard.pageDetails["text52"],
    //                       style: TextStyle(
    //                           fontSize: Pallet.heading5,
    //                           fontWeight: Pallet.font500,
    //                           color: Pallet.fontcolor)),
    //                 ),
    //                 Container(
    //                   child: MyText(dashboard.pageDetails["text36"],
    //                       style: TextStyle(
    //                           fontSize: Pallet.heading5,
    //                           fontWeight: Pallet.font500,
    //                           color: Pallet.fontcolor)),
    //                 ),
    //               ],
    //             ),
    //           ),
    //         ],
    //       ),
    //       SizedBox(height: 13),
    //       Row(
    //         mainAxisAlignment: MainAxisAlignment.center,
    //         children: [
    //           Container(
    //             decoration: BoxDecoration(
    //                 // color: Pallet.lastpackheadingback,
    //                 borderRadius: BorderRadius.circular(5)),
    //             width: wdgtWidth * 0.90,
    //             height: wdgtHeight * 0.55,
    //             child: Column(
    //               children: [
    //                 for (var pack in dashboard.lastPPackages)
    //                   Padding(
    //                     padding: EdgeInsets.only(bottom: wdgtHeight * 0.03),
    //                     child: Row(
    //                       mainAxisAlignment: MainAxisAlignment.center,
    //                       children: [
    //                         Container(
    //                           decoration: BoxDecoration(
    //                               color: Pallet.lastpackback,
    //                               borderRadius: BorderRadius.circular(5)),
    //                           width: wdgtWidth * 0.90,
    //                           height: wdgtHeight * 0.08,
    //                           child: Row(
    //                             mainAxisAlignment:
    //                                 MainAxisAlignment.spaceAround,
    //                             children: [
    //                               Container(
    //                                 width: wdgtWidth * 0.18,
    //                                 child: Row(
    //                                   mainAxisAlignment:
    //                                       MainAxisAlignment.start,
    //                                   children: [
    //                                     MyText(pack['price'].toString(),
    //                                         style: TextStyle(
    //                                             fontSize: Pallet.heading5,
    //                                             fontWeight: Pallet.font500,
    //                                             color: Pallet.fontcolor)),
    //                                   ],
    //                                 ),
    //                               ),
    //                               Container(
    //                                 width: wdgtWidth * 0.21,
    //                                 child: Row(
    //                                   mainAxisAlignment:
    //                                       MainAxisAlignment.start,
    //                                   children: [
    //                                     Container(
    //                                       width: 25,
    //                                       height: 25,
    //                                       child: Image.network(
    //                                           appSettings['SERVER_URL'] +
    //                                               '/' +
    //                                               pack['country_flag']),
    //                                     ),
    //                                     SizedBox(width: 10),
    //                                     MyText(pack['country_name'],
    //                                         style: TextStyle(
    //                                             fontSize: Pallet.heading5,
    //                                             fontWeight: Pallet.font500,
    //                                             color: Pallet.fontcolor)),
    //                                   ],
    //                                 ),
    //                               ),
    //                             ],
    //                           ),
    //                         ),
    //                       ],
    //                     ),
    //                   ),
    //               ],
    //             ),
    //           ),
    //         ],
    //       ),
    //       SizedBox(height: 5),
    //       Row(
    //         mainAxisAlignment: MainAxisAlignment.center,
    //         children: [
    //           Container(
    //             decoration: BoxDecoration(
    //                 color: Pallet.lastpackfooterback,
    //                 borderRadius: BorderRadius.circular(5)),
    //             width: wdgtWidth * 0.90,
    //             height: wdgtHeight * 0.09,
    //             child: Row(
    //               mainAxisAlignment: MainAxisAlignment.spaceAround,
    //               children: [
    //                 Container(
    //                   child: MyText(dashboard.pageDetails["text80"],
    //                       style: TextStyle(
    //                           fontSize: Pallet.heading3,
    //                           fontWeight: Pallet.font500,
    //                           color: Pallet.fontcolornew)),
    //                 ),
    //                 Container(
    //                   child: MyText(dashboard.totalDsvpCount.toString(),
    //                       style: TextStyle(
    //                           fontSize: Pallet.heading3,
    //                           fontWeight: Pallet.font500,
    //                           color: Pallet.fontcolornew)),
    //                 ),
    //               ],
    //             ),
    //           ),
    //         ],
    //       ),
    //     ],
    //   ),
    // );
  }

  // Widget latestproductsmobile(
  //     {double wdgtWidth, double wdgtHeight, List latestpackages}) {
  //   return Container(
  //     padding: EdgeInsets.all(10.0),
  //     decoration: BoxDecoration(
  //         boxShadow: [Pallet.shadowEffect],
  //         color: Pallet.dashcontainerback,
  //         borderRadius: BorderRadius.circular(20)),
  //     width: wdgtWidth,
  //     height: wdgtHeight,
  //     child: Column(
  //       crossAxisAlignment: CrossAxisAlignment.start,
  //       mainAxisAlignment: MainAxisAlignment.spaceEvenly,
  //       children: [
  //         Padding(
  //           padding: const EdgeInsets.only(left: 20.0, bottom: 20.0, top: 20),
  //           child: Row(
  //             children: [
  //               MyText(dashboard.pageDetails["text79"],
  //                   style: TextStyle(
  //                       fontSize: Pallet.heading3,
  //                       fontWeight: Pallet.font500,
  //                       color: Pallet.fontcolor))
  //             ],
  //           ),
  //         ),
  //         Row(
  //           mainAxisAlignment: MainAxisAlignment.center,
  //           children: [
  //             Container(
  //               decoration: BoxDecoration(
  //                   color: Pallet.lastpackheadingback,
  //                   borderRadius: BorderRadius.circular(5)),
  //               width: wdgtWidth * 0.90,
  //               height: wdgtWidth * 0.09,
  //               child: Row(
  //                 mainAxisAlignment: MainAxisAlignment.spaceAround,
  //                 children: [
  //                   Container(
  //                     child: MyText(dashboard.pageDetails["text52"],
  //                         style: TextStyle(
  //                             fontSize: Pallet.heading5,
  //                             fontWeight: Pallet.font500,
  //                             color: Pallet.fontcolor)),
  //                   ),
  //                   Container(
  //                     child: MyText(dashboard.pageDetails["text36"],
  //                         style: TextStyle(
  //                             fontSize: Pallet.heading5,
  //                             fontWeight: Pallet.font500,
  //                             color: Pallet.fontcolor)),
  //                   ),
  //                 ],
  //               ),
  //             ),
  //           ],
  //         ),
  //         SizedBox(height: 13),
  //         Row(
  //           mainAxisAlignment: MainAxisAlignment.center,
  //           children: [
  //             Container(
  //               decoration: BoxDecoration(
  //                   // color: Pallet.lastpackback,
  //                   borderRadius: BorderRadius.circular(5)),
  //               width: wdgtWidth * 0.90,
  //               height: wdgtHeight * 0.55,
  //               child: Column(
  //                 children: [
  //                   for (var pack in dashboard.lastPPackages)
  //                     Padding(
  //                       padding: EdgeInsets.only(bottom: wdgtHeight * 0.03),
  //                       child: Container(
  //                         decoration: BoxDecoration(
  //                             color: Pallet.lastpackback,
  //                             borderRadius: BorderRadius.circular(5)),
  //                         width: wdgtWidth * 0.90,
  //                         height: wdgtHeight * 0.08,
  //                         child: Row(
  //                           mainAxisAlignment: MainAxisAlignment.spaceAround,
  //                           children: [
  //                             Container(
  //                               width: wdgtWidth * 0.15,
  //                               child: Row(
  //                                 children: [
  //                                   MyText(pack['price'].toString(),
  //                                       style: TextStyle(
  //                                           fontSize: Pallet.heading5,
  //                                           fontWeight: Pallet.font500,
  //                                           color: Pallet.fontcolor)),
  //                                 ],
  //                               ),
  //                             ),
  //                             Container(
  //                               width: wdgtWidth * 0.21,
  //                               child: Row(
  //                                 mainAxisAlignment: MainAxisAlignment.start,
  //                                 children: [
  //                                   Container(
  //                                     width: 25,
  //                                     height: 25,
  //                                     child: Image.network(
  //                                         appSettings['SERVER_URL'] +
  //                                             '/' +
  //                                             pack['country_flag']),
  //                                   ),
  //                                   SizedBox(width: 10),
  //                                   MyText(pack['country_name'],
  //                                       style: TextStyle(
  //                                           fontSize: Pallet.heading5,
  //                                           fontWeight: Pallet.font500,
  //                                           color: Pallet.fontcolor)),
  //                                 ],
  //                               ),
  //                             ),
  //                           ],
  //                         ),
  //                       ),
  //                     ),
  //                 ],
  //               ),
  //             ),
  //           ],
  //         ),
  //         Row(
  //           mainAxisAlignment: MainAxisAlignment.center,
  //           children: [
  //             Container(
  //               decoration: BoxDecoration(
  //                   color: Pallet.lastpackfooterback,
  //                   borderRadius: BorderRadius.circular(5)),
  //               width: wdgtWidth * 0.90,
  //               height: wdgtHeight * 0.09,
  //               child: Row(
  //                 mainAxisAlignment: MainAxisAlignment.spaceAround,
  //                 children: [
  //                   Container(
  //                     child: MyText(dashboard.pageDetails["text80"],
  //                         style: TextStyle(
  //                             fontSize: Pallet.heading4,
  //                             fontWeight: Pallet.font500,
  //                             color: Pallet.fontcolornew)),
  //                   ),
  //                   Container(
  //                     child: MyText(dashboard.totalDsvpCount.toString(),
  //                         style: TextStyle(
  //                             fontSize: Pallet.heading4,
  //                             fontWeight: Pallet.font500,
  //                             color: Pallet.fontcolornew)),
  //                   ),
  //                 ],
  //               ),
  //             ),
  //           ],
  //         ),
  //       ],
  //     ),
  //   );
  // }

  Widget attendwebinarpopup({double popheading}) {
    return DataTable(
      columns: [
        DataColumn(
            label: MyText(
                text: dashboard.pageDetails["text57"],
                style: TextStyle(
                  fontSize: popheading / 1.09,
                  color: Pallet.fontcolor,
                ))),
        DataColumn(
            label: MyText(
                text: dashboard.pageDetails["text58"],
                style: TextStyle(
                  fontSize: popheading / 1.09,
                  color: Pallet.fontcolor,
                ))),
      ],
      rows: [
        DataRow(cells: [
          DataCell(MyText(
              text: dashboard.pageDetails["text59"],
              style: TextStyle(
                fontSize: popheading / 1.15,
                color: Pallet.fontcolor,
              ))),
          DataCell(MyText(
              text: dashboard.pageDetails["text60"],
              style: TextStyle(
                fontSize: popheading / 1.25,
                color: Pallet.fontcolor,
              ))),
        ]),
        DataRow(cells: [
          DataCell(MyText(
              text: dashboard.pageDetails["text61"],
              style: TextStyle(
                fontSize: popheading / 1.25,
                color: Pallet.fontcolor,
              ))),
          DataCell(MyText(
              text: dashboard.pageDetails["text62"],
              style: TextStyle(
                fontSize: popheading / 1.25,
                color: Pallet.fontcolor,
              ))),
        ]),
        DataRow(cells: [
          DataCell(MyText(
              text: dashboard.pageDetails["text63"],
              style: TextStyle(
                fontSize: popheading / 1.25,
                color: Pallet.fontcolor,
              ))),
          DataCell(MyText(
              text: dashboard.pageDetails["text64"],
              style: TextStyle(
                fontSize: popheading / 1.25,
                color: Pallet.fontcolor,
              ))),
        ]),
        DataRow(cells: [
          DataCell(MyText(
              text: dashboard.pageDetails["text65"],
              style: TextStyle(
                fontSize: popheading / 1.25,
                color: Pallet.fontcolor,
              ))),
          DataCell(MyText(
              text: dashboard.pageDetails["text66"],
              style: TextStyle(
                fontSize: popheading / 1.25,
                color: Pallet.fontcolor,
              ))),
        ]),
      ],
    );
  }

  Widget redeempopup({double popheading}) {
    var _formKey = GlobalKey<FormState>();

    _adddsv() {
      final isValid = _formKey.currentState.validate();
      if (!isValid) {
        return dashboard.pageDetails["text48"];
      } else if (dashboard.mypriceofcoin['dsv_balance'] == 0) {
        return dashboard.pageDetails["text91"];
      } else if (dashboard.mypriceofcoin['dsv_balance'] >= adddsv.value) {
        return dashboard.pageDetails["text91"];
      }
      _formKey.currentState.save();
    }

    return Form(
      key: _formKey,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          MyText(
            text: dashboard.pageDetails["text46"],
            style: TextStyle(
                fontWeight: Pallet.font500,
                fontSize: Pallet.heading1,
                color: Pallet.fontcolor),
          ),
          SizedBox(height: 20.0),
          TextFormField(
            controller: adddsv,
            textAlign: TextAlign.left,
            keyboardType: TextInputType.number,
            inputFormatters: <TextInputFormatter>[
              FilteringTextInputFormatter.digitsOnly
            ],
            style: TextStyle(color: Pallet.fontcolor),
            cursorColor: Pallet.fontcolor,
            decoration: InputDecoration(
              border: OutlineInputBorder(
                  borderSide: BorderSide(color: Pallet.fontcolor, width: 2.0),
                  borderRadius: BorderRadius.circular(8.0)),
              labelText: '(1DSV = \$ 0.05)',
              hintText: dashboard.pageDetails["text47"],
              hintStyle:
                  TextStyle(fontSize: popheading, color: Color(0xFFb6c3db)),
              fillColor: Color(0xFF4570ae),
              focusedBorder: OutlineInputBorder(
                  borderSide: BorderSide(color: Pallet.fontcolor, width: 2.0),
                  borderRadius: BorderRadius.circular(8.0)),
              enabledBorder: OutlineInputBorder(
                  borderSide: BorderSide(color: Pallet.fontcolor, width: 2.0),
                  borderRadius: BorderRadius.circular(8.0)),
              labelStyle: TextStyle(
                color: Color(0xFFb6c3db),
                fontSize: 14,
              ),
            ),
            validator: (input) {
              final isDigitsOnly = int.tryParse(input);
              return isDigitsOnly == null
                  ? dashboard.pageDetails["text48"]
                  : null;
            },
            onChanged: (value) {},
          ),
          SizedBox(height: 10.0),
          Container(
            width: double.infinity,
            // ignore: deprecated_member_use
            child: CustomButton(
              onpress: () {
                _adddsv();
              },
              vpadding: 12,
              text: dashboard.pageDetails["text49"],
              buttoncolor: Pallet.fontcolor,
              textcolor: Pallet.fontcolornew,
              textsize: Pallet.heading4,
            ),
          ),
          // Container(
          //   width: double.infinity,
          //   // ignore: deprecated_member_use
          //   child: RaisedButton(
          //     shape: RoundedRectangleBorder(
          //       borderRadius: BorderRadius.circular(8.0),
          //     ),
          //     padding: const EdgeInsets.fromLTRB(0, 18, 0, 18),
          //     child: MyText(
          //       dashboard.pageDetails["text49"],
          //       style: TextStyle(fontSize: 18, fontWeight: FontWeight.w700),
          //     ),
          //     color: Pallet.fontcolor,
          //     textColor: Pallet.fontcolornew,
          //     onPressed: () {
          //       _adddsv();
          //     },
          //   ),
          // ),
        ],
      ),
    );
  }

  int sponsorid;
  placechangetorder(setState3) {
    return Container(
      width: 300,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          MyText(
              text: dashboard.pageDetails['text178'],
              style: TextStyle(
                fontSize: Pallet.heading4,
                color: Pallet.fontcolor,
              )),
          SizedBox(height: 10),
          SignupTextBox(
            isPrefixIcon: false,
            isSuffixIcon: false,
            isPassword: false,
            digitsOnlyPhone: false,
            label: dashboard.pageDetails['text179'],
            errorText: placeError,
            autofocus: true,
            controller: placement,
            validation: (value) {
              print('WHAT IS VALUE');
              print(value);

              setState3(() {
                checSponsor(value, setState3);
              });
            },
          ),
          SizedBox(height: 10),
          CustomButton(
            onpress: () async {
              print('LLLLLLLLLLLL');
              print(sponsorid);
              print('SELECTED NUMBER');
              print(selectedmember);
              if (placement.text.trim().isEmpty || placement.text == '') {
                setState3(() {
                  placeError = dashboard.pageDetails['text48'];
                });
              } else if (placeError == null) {
                place = false;
                snack.snack(title: 'Placement Changed Successfully');
                Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => Home(
                            route: 'my_network',
                          )),
                );
                var map6 = {
                  "system_product_id": 1,
                  "account_id": selectedmember['account_id'],
                  "placement_id": sponsorid,
                };
                print('MYMAP');
                print(map6);
                print("kkkkkkkkkkk");

                Map<String, dynamic> result = await HttpRequest.Post(
                    'change_placement', Utils.constructPayload(map6));

                print('$result, change_placement');
                if (Utils.isServerError(result)) {
                  snack.snack(title: dashboard.pageDetails['text96']);
                  // Navigator.pop(context);

                  throw (await Utils.getMessage(result['response']['error']));
                } else {
                  print('mkkkmkmkmk');
                  print(result);
                }
              } else {}
            },
            vpadding: 10,
            text: dashboard.pageDetails['text180'],
            buttoncolor: Pallet.fontcolor,
            textcolor: Pallet.fontcolornew,
            textsize: Pallet.heading4,
          ),
          SizedBox(height: 10),
        ],
      ),
    );
  }

  Map selectedmember;
  int accounttype;
  checSponsor(value, setState3) {
    // sponsorError = null;
    if (value.length < 6 && value.length > 0) {
      print('10');
      setState3(() {
        placeError = dashboard.pageDetails['text181'];
      });
    } else if (value.trim().isEmpty || value == '') {
      setState3(() {
        placeError = dashboard.pageDetails['text48'];
        value = appSettings['ROOT'];
      });
    } else {
      print('30');

      // if (value.trim().isEmpty || value == '') {
      //   // sponsorError = "Sponsor Required";
      //   setState3(() {
      //     placeError = 'Required';
      //     value = appSettings['ROOT'];
      //   });
      // }
      output(value) {
        Map<String, dynamic> _map;
        if (value == 'Sponsor Not Found') {
          print('40');

          setState3(() {
            placeError = dashboard.pageDetails['text182'];
            // if (value.trim().isEmpty || value == '') {
            //   print('50');

            //   // sponsorError = "Sponsor Required";
            //   placeError = null;
            //   value = appSettings['ROOT'];
            // } else {
            //   print('60');
            //   placeError = 'Name Not Found';
            // }
          });
        } else {
          print('70');
          _map = Utils.fromJSONString(value);
          sponsorid = _map['sponsor_id'];
          accounttype = _map['account_type'];
          if (accounttype != 2) {
            setState3(() {
              placeError = dashboard.pageDetails['text183'];
            });
          } else {
            setState3(() {
              placeError = null;
            });
          }
        }
      }

      SampleClass().checksponsor(value, (output));
    }
  }

  Widget myteammebers({double popheading, setState3, i}) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        place == true ? placechangetorder(setState3) : Container(),
        DataTable(
          columns: [
            DataColumn(
                label: MyText(
                    text: dashboard.pageDetails["text39"],
                    style: TextStyle(
                      fontSize: popheading / 1.09,
                      color: Pallet.fontcolor,
                    ))),
            DataColumn(
                label: MyText(
                    text: dashboard.pageDetails["text36"],
                    style: TextStyle(
                      fontSize: popheading / 1.09,
                      color: Pallet.fontcolor,
                    ))),
          ],
          rows: [
            if (myteam_inside_data.length > 0)
              for (var member in myteam_inside_data ?? [])
                DataRow(cells: [
                  DataCell(InkWell(
                    onTap: () {
                      if (dashboard.myteam1[i]['key'] == 'waiting_partners') {
                        print('PLACEMENT');
                        print(place);
                        setState3(() {
                          place = true;
                          selectedmember = member;
                          // placechangetorder();
                        });
                      } else {}
                    },
                    child: dashboard.myteam1[i]['key'] == 'waiting_partners'
                        //Put Tooltip Here
                        //Tooltip(
                        //  message: dashboard.pageDetails["text184"],
                        ? Tooltip(
                            message: dashboard.pageDetails["text184"],
                            child: Container(
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Container(
                                    // width: 120,
                                    child: Row(
                                      children: [
                                        CircleAvatar(
                                          backgroundColor: Colors.transparent,
                                          backgroundImage: NetworkImage(
                                              appSettings['SERVER_URL'] +
                                                  '/' +
                                                  member['profile_image']
                                                      .toString()),
                                          radius: 12,
                                        ),
                                        SizedBox(width: 5.0),
                                        Container(
                                          child: MyText(
                                              text: member["user_name"]
                                                  .toString()
                                                  .capitalizeFirstofEach,
                                              overflow: TextOverflow.ellipsis,
                                              isselectable: false,
                                              style: TextStyle(
                                                  fontSize: popheading / 1.25,
                                                  fontWeight: Pallet.font500,
                                                  color: Pallet.fontcolor)),
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          )
                        : Container(
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Container(
                                  // width: 120,
                                  child: Row(
                                    children: [
                                      CircleAvatar(
                                        backgroundColor: Colors.transparent,
                                        backgroundImage: NetworkImage(
                                            appSettings['SERVER_URL'] +
                                                '/' +
                                                member['profile_image']
                                                    .toString()),
                                        radius: 12,
                                      ),
                                      SizedBox(width: 5.0),
                                      Container(
                                        child: MyText(
                                            text: member["user_name"]
                                                .toString()
                                                .capitalizeFirstofEach,
                                            overflow: TextOverflow.ellipsis,
                                            isselectable: false,
                                            style: TextStyle(
                                                fontSize: popheading / 1.25,
                                                fontWeight: Pallet.font500,
                                                color: Pallet.fontcolor)),
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ),
                  )),
                  DataCell(Container(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Container(
                            // width: 120,
                            child: Row(
                          children: [
                            Container(
                              width: 25,
                              height: 25,
                              child: Image.network(appSettings['SERVER_URL'] +
                                  '/' +
                                  member['country_flag'].toString()),
                            ),
                            SizedBox(width: 10),
                            Container(
                              child: MyText(
                                  text: member["country_iso_code"].toString(),
                                  overflow: TextOverflow.ellipsis,
                                  isselectable: false,
                                  style: TextStyle(
                                      fontSize: popheading / 1.25,
                                      fontWeight: Pallet.font500,
                                      color: Pallet.fontcolor)),
                            ),
                          ],
                        ))
                      ],
                    ),
                  )),
                ])
          ],
        ),
      ],
    );
  }

  Widget ducvaultpopup({double popheading}) {
    return PaginatedDataTable(
      header: MyText(
          text: dashboard.pageDetails["text27"],
          style: TextStyle(
              color: Pallet.popupcontainerback,
              fontWeight: Pallet.font500,
              fontSize: popheading)),
      rowsPerPage: 5,
      columns: [
        DataColumn(
            label: MyText(
                text: dashboard.pageDetails["text52"],
                style: TextStyle(
                  color: Pallet.popupcontainerback,
                ))),
        DataColumn(
            label: MyText(
                text: dashboard.pageDetails["text53"],
                style: TextStyle(
                  color: Pallet.popupcontainerback,
                ))),
        DataColumn(
            label: MyText(
                text: dashboard.pageDetails["text54"],
                style: TextStyle(
                  color: Pallet.popupcontainerback,
                ))),
        DataColumn(
            label: MyText(
                text: dashboard.pageDetails["text55"],
                style: TextStyle(
                  color: Pallet.popupcontainerback,
                ))),
      ],
      source: _DataSource(context),
    );
  }

  void twofasetuppopup(BuildContext context) async {
    await showDialog(
        context: context,
        barrierDismissible: false,
        // barrierColor: Colors.red,
        builder: (context) => AlertDialog(
              backgroundColor: Pallet.inner1,
              elevation: 0,
              title: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Image.asset("c-logo.png", width: 40),
                  SizedBox(width: 10),
                  MyText(
                      text: dashboard.pageDetails["text126"],
                      style: TextStyle(
                          fontSize: Pallet.heading5,
                          color: Pallet.fontcolor,
                          fontWeight: Pallet.font500)),
                ],
              ),
              content: Container(
                width: 400,
                child: SingleChildScrollView(
                  child: ListBody(
                    children: <Widget>[
                      Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Image.asset('2fa.png', width: 100),
                          SizedBox(height: 10),
                          MyText(
                            text: dashboard.pageDetails["text127"],
                            style: TextStyle(
                                fontSize: Pallet.heading4,
                                color: Pallet.fontcolor),
                          ),
                          SizedBox(height: 10),
                          MyText(
                            text: dashboard.pageDetails["text128"],
                            style: TextStyle(
                                fontSize: Pallet.heading4,
                                color: Pallet.fontcolor),
                          ),
                          SizedBox(height: 10),
                          CustomButton(
                            onpress: () {
                              // twofa = true;
                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => Home(
                                          route: 'two_factor_auth',
                                          twofapopup: true,
                                        )),
                              );
                            },
                            vpadding: 10,
                            text: dashboard.pageDetails["text129"],
                            buttoncolor: Pallet.dashcontainerback,
                            textcolor: Pallet.fontcolor,
                            textsize: Pallet.heading5,
                            fontweight: Pallet.font500,
                          ),
                          SizedBox(height: 20),
                          CustomButton(
                            onpress: () async {
                              SharedPreferences prefs =
                                  await SharedPreferences.getInstance();

                              // twofa = true;
                              Navigator.pop(context);
                              prefs.getBool('newshow') == true
                                  ? latestnews(context)
                                  : print("ss");
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => Home(
                                            route: 'dashboard',
                                            twofapopup: true,
                                          )));
                            },
                            vpadding: 10,
                            text: dashboard.pageDetails["text130"],
                            buttoncolor: Pallet.fontcolor,
                            textcolor: Pallet.fontcolornew,
                            textsize: Pallet.heading5,
                            fontweight: Pallet.font500,
                          ),
                          SizedBox(height: 20),
                          MyText(
                              text: dashboard.pageDetails["text131"],
                              style: TextStyle(
                                  fontSize: Pallet.heading6,
                                  height: 1.5,
                                  color: Pallet.fontcolor)),
                        ],
                      ),
                    ],
                  ),
                ),
              ),
              // onPressed: () {
              //   Navigator.of(context).pop();
              //   Navigator.of(context).pop();
              // },
              // actions: [
              //   PopupButton(
              //     onpress: () {
              //       twofa = false;
              //       Navigator.of(context).pop();
              //     },
              //     MyText: 'Close',
              //   ),
              // ],
            ));
  }

  void latestnews(BuildContext context) async {
    print('SSSSSSSSSSSSS');
    print(dashboard.ducmessages[0]["header"]);
    await showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return AlertDialog(
          elevation: 24.0,
          backgroundColor: Pallet.popupcontainerback,
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(15.0))),
          title: Row(
            children: [
              Image.asset("c-logo.png", width: 40),
              SizedBox(width: 10),
              Expanded(
                child: MyText(
                    text: dashboard.ducmessages[0]["header"].toString() == null
                        ? 'Heading'
                        : dashboard.ducmessages[0]["header"].toString(),
                    maxLines: 2,
                    style: TextStyle(
                        fontSize: Pallet.heading3,
                        color: Pallet.fontcolor,
                        fontWeight: Pallet.font500)),
              ),
            ],
          ),
          content: Container(
            width: 450.0,
            child: SingleChildScrollView(
              child: ListBody(
                children: <Widget>[
                  Center(
                      child: MyText(
                          text: dashboard.ducmessages[0]["body"].toString() ==
                                  null
                              ? 'Content'
                              : dashboard.ducmessages[0]["body"].toString(),
                          style: TextStyle(
                            fontSize: Pallet.heading3 / 1.03,
                            height: 1.5,
                            color: Pallet.fontcolor,
                          ))),
                ],
              ),
            ),
          ),
          actions: [
            PopupButton(
              text: dashboard.pageDetails["text78"],
              onpress: () async {
                SharedPreferences prefs = await SharedPreferences.getInstance();
                prefs.setBool("newshow", false);
                Navigator.pop(context);
              },
            ),
            SizedBox(height: 10),
          ],
        );
      },
    );
  }

  confirmcorporate(
    // double wdgtWidth,
    dialogwidth,
  ) {
    return showDialog(
        barrierDismissible: false,
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            backgroundColor: Pallet.popupcontainerback,
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(Pallet.radius)),
            title: Row(
              // mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                Image.asset(
                  'assets/c-logo.png',
                  width: 40,
                ),
                SizedBox(width: 10),
                Expanded(
                  child: Align(
                    alignment: Alignment.topLeft,
                    child: MyText(
                      text: dashboard.pageDetails["text69"],
                      style: TextStyle(
                        fontSize: Pallet.heading3,
                        // fontWeight: FontWeight.bold,
                        color: Pallet.fontcolor,
                      ),
                    ),
                  ),
                ),
              ],
            ),
            content: SingleChildScrollView(
              child: Container(
                  width: dialogwidth,
                  // width: ismobile == null ? size.width : 400,
                  height: 320,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      MyText(
                        text: dashboard.pageDetails["text70"],
                        style: TextStyle(
                          fontSize: Pallet.heading4,
                          color: Pallet.fontcolor,
                        ),
                      ),
                      SizedBox(height: 5),
                      MyText(
                        text: dashboard.pageDetails["text71"],
                        style: TextStyle(
                          fontSize: Pallet.heading4,
                          color: Pallet.fontcolor,
                        ),
                      ),
                      SizedBox(height: 5),
                      MyText(
                          text: dashboard.pageDetails["text72"],
                          style: TextStyle(
                              fontSize: Pallet.heading4,
                              color: Pallet.fontcolor,
                              fontWeight: Pallet.font500)),
                      SizedBox(height: 5),
                      MyText(
                        text: dashboard.pageDetails["text73"],
                        style: TextStyle(
                          fontSize: Pallet.heading4,
                          // fontWeight: FontWeight.bold,
                          color: Pallet.fontcolor,
                        ),
                      ),
                      SizedBox(height: 5),
                      MyText(
                        text: dashboard.pageDetails["text74"],
                        style: TextStyle(
                            fontSize: Pallet.heading4,
                            color: Colors.yellow,
                            fontWeight: Pallet.font500
                            // color: Pallet.fontcolor,
                            ),
                      ),
                    ],
                  )),
            ),
            actions: [
              PopupButton(
                text: dashboard.pageDetails['text78'],
                onpress: () {
                  Navigator.of(context).pop();
                },
              ),
              PopupButton(
                text: dashboard.pageDetails["text132"],
                onpress: () {
                  networkPartner(context, setState);
                },
              ),
            ],
          );
        });
  }

  partnerscontainer(
      double rankwidth, String rank, String profile, String username) {
    return Container(
      width: rankwidth,

      // height: 100,
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(Pallet.radius),
          boxShadow: [Pallet.shadowEffect],
          color: Pallet.fontcolor),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        // crossAxisAlignment:
        //     CrossAxisAlignment
        //         .start,
        children: [
          SizedBox(height: 10),
          Container(
            padding: EdgeInsets.all(5),
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(5),
                border: Border.all(color: Pallet.fontcolornew, width: 0.5)),
            child: MyText(
              text: rank,
              style: TextStyle(
                color: Pallet.fontcolornew,
                fontSize: Pallet.heading7,
                fontWeight: Pallet.heading1wgt,
              ),
            ),
          ),
          SizedBox(height: 10),
          CircleAvatar(
            backgroundColor: Colors.transparent,
            child: ClipRRect(
              borderRadius: BorderRadius.circular(70),
              child: Image.network(
                appSettings['SERVER_URL'] + '/' + profile,
                fit: BoxFit.contain,
              ),
            ),
            radius: 15,
          ),
          // Spacer(
          //   flex: 1,
          // ),
          SizedBox(height: 10),
          MyText(
            text: username,
            overflow: TextOverflow.ellipsis,
            isselectable: false,
            textAlign: TextAlign.center,
            style: TextStyle(
                fontSize: Pallet.heading7,
                color: Pallet.fontcolornew,
                fontWeight: Pallet.heading1wgt),
          ),
          SizedBox(height: 10),
        ],
      ),
    );
  }

// ignore: non_constant_identifier_names
  rankqualifypartnerspopup(List myteam_inside_data1, List myteam_inside_data,
      BuildContext context1, rankwidth) {
    double width = MediaQuery.of(context).size.width;
    return custompopup.showdialog(
      barrierColor: Colors.transparent,
      context: context,
      barrierDismissible: false,
      backgroundcolor: Pallet.fontcolor,
      titlepadding: EdgeInsets.all(0),
      scrollable: true,
      customtitle: StatefulBuilder(
        builder:
            (BuildContext context, void Function(void Function()) setState5) =>
                Container(
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(0),
            // color: Colors.white,
          ),
          width: 455,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Stack(
                children: [
                  ClipRRect(
                    borderRadius: BorderRadius.circular(0),
                    child: Image.network(
                      appSettings['SERVER_URL'] + '/rank_ai/header.png',
                      height: 70,
                      fit: BoxFit.fill,
                      width: double.infinity,
                    ),
                  ),
                  Center(
                    child: Padding(
                      padding: const EdgeInsets.only(top: 10.0),
                      child: MyText(
                        text: rank == false
                            ? dashboard.pageDetails["text185"]
                            : dashboard.pageDetails["text186"],
                        style: TextStyle(
                          color: Pallet.fontcolor,
                          fontSize: Pallet.heading4,
                        ),
                      ),
                    ),
                  ),
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  InkWell(
                    onTap: () {
                      setState5(() {
                        print('YYYYYYY');
                        // print(rank);
                        rank = !rank;
                        print(rank);
                      });
                    },
                    child: Padding(
                        padding: const EdgeInsets.only(
                            left: 8.0, right: 13, bottom: 8),
                        child: MyText(
                            text: rank == false
                                ? dashboard.pageDetails["text186"]
                                : dashboard.pageDetails["text185"],
                            style: TextStyle(
                                decoration: TextDecoration.underline,
                                fontSize: Pallet.heading5,
                                color: Pallet.fontcolornew))),
                  ),
                ],
              ),
              rank == false
                  ? myteam_inside_data1.length != 1
                      ? SingleChildScrollView(
                          child: Container(
                            width: 455,
                            child: Wrap(
                              spacing: 10,
                              runSpacing: 10,
                              children: [
                                for (var i = 0;
                                    i < myteam_inside_data1.length;
                                    i++)
                                  partnerscontainer(
                                      rankwidth,
                                      myteam_inside_data1[i]['rank'],
                                      myteam_inside_data1[i]['profile'],
                                      myteam_inside_data1[i]['username'])
                              ],
                            ),
                          ),
                        )
                      : Column(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            for (var i = 0; i < myteam_inside_data1.length; i++)
                              partnerscontainer(
                                  rankwidth,
                                  myteam_inside_data1[i]['rank'],
                                  myteam_inside_data1[i]['profile'],
                                  myteam_inside_data1[i]['username'])
                          ],
                        )
                  : Column(
                      children: [
                        for (var i = 0; i < myteam_inside_data.length; i++)
                          Stack(
                            children: [
                              ClipRRect(
                                borderRadius: BorderRadius.circular(10),
                                child: Container(
                                  // margin: EdgeInsets.symmetric(
                                  //   horizontal: 40,
                                  //   vertical: 100,
                                  // ),
                                  padding: EdgeInsets.symmetric(
                                    horizontal: 10,
                                  ),
                                  child: Column(
                                    children: [
                                      Stack(
                                        children: [
                                          Padding(
                                            padding: const EdgeInsets.symmetric(
                                              vertical: 20,
                                              horizontal: 20,
                                            ),
                                            child: Container(
                                                height: 90,
                                                padding: EdgeInsets.only(
                                                  top: 10,
                                                  left: 15,
                                                  right: 15,
                                                  bottom: 25,
                                                ),
                                                decoration: BoxDecoration(
                                                    color: Colors.white,
                                                    boxShadow: [
                                                      BoxShadow(
                                                        color: Colors.black12,
                                                        blurRadius: 4,
                                                        spreadRadius: 6,
                                                      )
                                                    ],
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                      20,
                                                    )),
                                                child: Row(
                                                    //   mainAxisAlignment: MainAxisAlignment.end,
                                                    children: [
                                                      // Image.asset(
                                                      //   "Punchhole.png",
                                                      //   height:
                                                      //       20,
                                                      // ),
                                                      Image.network(
                                                        appSettings[
                                                                'SERVER_URL'] +
                                                            '/rank_ai/punchhole.png',
                                                        height: 20,
                                                      ),
                                                      SizedBox(
                                                        width: 20,
                                                      ),
                                                      Expanded(
                                                        child: MyText(
                                                          text:
                                                              myteam_inside_data[
                                                                          i][
                                                                      'content']
                                                                  .toString(),
                                                          style: TextStyle(
                                                              color:
                                                                  Colors.black,
                                                              fontSize: Pallet
                                                                  .heading5,
                                                              fontWeight:
                                                                  FontWeight
                                                                      .w300),
                                                        ),
                                                      ),
                                                    ])),
                                          ),
                                          myteam_inside_data[i]['type'] ==
                                                  "invite"
                                              ? Positioned(
                                                  bottom: 0,
                                                  left: 0,
                                                  child: Container(
                                                    child: Image.network(
                                                      appSettings[
                                                              'SERVER_URL'] +
                                                          '/rank_ai/invite.png',
                                                      height: 60,
                                                    ),
                                                    // child: Image
                                                    //     .asset(
                                                    //   "invite.png",
                                                    //   height: 60,
                                                    // ),
                                                  ),
                                                )
                                              : Container(),
                                          myteam_inside_data[i]['type'] ==
                                                  "motivate"
                                              ? Positioned(
                                                  top: 0,
                                                  left: 0,
                                                  child: Container(
                                                      child: CircleAvatar(
                                                    radius: 25,
                                                    backgroundImage:
                                                        NetworkImage(appSettings[
                                                                'SERVER_URL'] +
                                                            '/' +
                                                            myteam_inside_data[
                                                                i]['profile']),
                                                  )))
                                              : Container(),
                                          myteam_inside_data[i]['type'] == "bv"
                                              ? Positioned(
                                                  bottom: 0,
                                                  left: 0,
                                                  child: Container(
                                                    child: Image.network(
                                                      appSettings[
                                                              'SERVER_URL'] +
                                                          '/rank_ai/bv.png',
                                                      height: 60,
                                                    ),
                                                    // child: Image
                                                    //     .asset(
                                                    //   "bv.png",
                                                    //   height: 60,
                                                    // ),
                                                  ))
                                              : Container(),
                                          myteam_inside_data[i]['type'] ==
                                                  "motivate"
                                              ? Positioned(
                                                  bottom: 0,
                                                  right: 0,
                                                  child: Container(
                                                    child: Image.network(
                                                      appSettings[
                                                              'SERVER_URL'] +
                                                          '/rank_ai/motivate.png',
                                                      height: 60,
                                                    ),
                                                    // child: Image
                                                    //     .asset(
                                                    //   "motivate.png",
                                                    //   height: 60,
                                                    // ),
                                                  ),
                                                )
                                              : Container(),
                                        ],
                                      ),
                                      SizedBox(
                                        height: width <= 425 ? 10 : 20,
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ],
                          ),
                      ],
                    ),
            ],
          ),
        ),
      ),
      actions: [
        PopupButton(
          buttoncolor: Pallet.buttonclr,
          textcolor: Pallet.fontcolor,
          text: dashboard.pageDetails["text78"],
          onpress: () {
            print('Close');
            print(rank);
            rank = false;
            Navigator.of(context).pop();
          },
        )
      ],
    );
  }

  // ignore: non_constant_identifier_names
  DSVPRedeem(BuildContext context, setState1) async {
    await showDialog(
      context: context,
      barrierDismissible: false,
      // barrierColor: Colors.red,
      builder: (context) {
        return StatefulBuilder(
          builder: (BuildContext context, setState1) {
            return AlertDialog(
              backgroundColor: Pallet.inner1,
              title: Column(
                children: [
                  Row(children: [
                    Image.asset("c-logo.png", width: 40),
                    SizedBox(width: 10),
                    Expanded(
                      child: MyText(
                          text: dashboard.pageDetails["text122"],
                          style: TextStyle(
                              fontSize: Pallet.heading3,
                              color: Pallet.fontcolor,
                              fontWeight: Pallet.font500)),
                    ),
                  ]),
                  SizedBox(height: 10),
                  MyText(
                      text: dashboard.pageDetails["text133"],
                      style: TextStyle(
                          fontSize: Pallet.heading4,
                          color: Pallet.fontcolor,
                          fontWeight: Pallet.font500)),
                ],
              ),
              content: Container(
                // height: 340,
                child: SingleChildScrollView(
                  child: Column(
                    // mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      MyText(
                        text: dashboard.pageDetails["text134"],
                        style: TextStyle(
                          fontSize: Pallet.heading4,
                          color: Pallet.fontcolor,
                          fontWeight: Pallet.font500,
                        ),
                      ),
                      SizedBox(height: 10),
                      Container(
                        width: double.infinity,
                        decoration: BoxDecoration(
                            color: Pallet.dashcontainerback,
                            borderRadius: BorderRadius.circular(Pallet.radius),
                            border:
                                Border.all(color: Pallet.dashcontainerback)),
                        child: Padding(
                          padding: EdgeInsets.all(Pallet.defaultPadding),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              MyText(
                                text: dashboard.mypriceofcoin['dsv_balance']
                                    .toString(),
                                style: TextStyle(
                                  color: Pallet.fontcolor,
                                  fontWeight: Pallet.font500,
                                  fontSize: Pallet.heading3,
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                      SizedBox(height: 10),
                      MyText(
                        text: dashboard.pageDetails["text135"],
                        style: TextStyle(
                            fontWeight: Pallet.font500,
                            fontSize: Pallet.heading4,
                            color: Pallet.fontcolor),
                      ),
                      SizedBox(height: 20.0),
                      TextFormField(
                        controller: dsvcontroller,
                        textAlign: TextAlign.left,
                        style: TextStyle(color: Pallet.fontcolor),
                        cursorColor: Pallet.fontcolor,
                        autofocus: true,
                        onChanged: (value) {
                          if (value.isEmpty) {
                            setState1(() {
                              dsvError = dashboard.pageDetails["text93"];
                            });
                          } else if (double.parse(value) <= 0) {
                            setState1(() {
                              dsvError = dashboard.pageDetails["text93"];
                            });
                          } else if (double.parse(value) >
                              dashboard.mypriceofcoin['dsv_balance']) {
                            setState1(() {
                              dsvError = dashboard.pageDetails["text91"];
                            });
                          } else if (double.parse(value) <=
                              dashboard.mypriceofcoin['dsv_balance']) {
                            setState1(() {
                              cal = int.parse(value) / 2;
                              dsvError = null;
                            });
                          }
                        },
                        keyboardType:
                            TextInputType.numberWithOptions(decimal: true),
                        inputFormatters: <TextInputFormatter>[
                          FilteringTextInputFormatter.digitsOnly,
                        ],
                        decoration: InputDecoration(
                          //hintText: "Enter Amount",
                          errorText: dsvError,
                          border: OutlineInputBorder(
                            borderSide:
                                BorderSide(color: Pallet.fontcolor, width: 2),
                          ),
                          focusedBorder: OutlineInputBorder(
                              borderSide:
                                  BorderSide(color: Pallet.fontcolor, width: 2),
                              borderRadius:
                                  BorderRadius.circular(Pallet.radius)),
                          enabledBorder: OutlineInputBorder(
                              borderSide:
                                  BorderSide(color: Pallet.fontcolor, width: 2),
                              borderRadius:
                                  BorderRadius.circular(Pallet.radius)),
                          errorBorder: OutlineInputBorder(
                              borderSide:
                                  BorderSide(color: Colors.red, width: 2),
                              borderRadius:
                                  BorderRadius.circular(Pallet.radius)),
                          focusedErrorBorder: OutlineInputBorder(
                              borderSide:
                                  BorderSide(color: Colors.red, width: 2),
                              borderRadius:
                                  BorderRadius.circular(Pallet.radius)),
                          filled: true,
                        ),
                      ),
                      SizedBox(height: 10.0),
                      dsvError != null
                          ? Container()
                          : (cal == null ||
                                  cal == 0 ||
                                  dsvcontroller.text == null ||
                                  dsvcontroller.text == '')
                              ? Container()
                              : MyText(
                                  text: dashboard.pageDetails["text94"] +
                                      ' ' +
                                      '\$' +
                                      cal.toString() +
                                      ' ' +
                                      dashboard.pageDetails["text95"],
                                  // "You Get $cal DSV-P",
                                  style: TextStyle(
                                      fontWeight: Pallet.font500,
                                      fontSize: Pallet.heading5,
                                      color: Pallet.fontcolor),
                                ),
                      SizedBox(height: 10.0),
                      CustomButton(
                        vpadding: 10,
                        textsize: Pallet.heading4,
                        fontweight: Pallet.font600,
                        text: dashboard.pageDetails["text136"],
                        onpress: () async {
                          int _amount;
                          if (dsvcontroller.text != null ||
                              dsvcontroller.text != '') {
                            _amount = int.parse(dsvcontroller.text);
                          } else {
                            _amount = 0;
                          }
                          if (double.parse(dsvcontroller.text) <= 0) {
                            setState1(() {
                              dsvError = dashboard.pageDetails["text93"];
                            });
                          }
                          if (_amount <= 0) {
                            setState1(() {
                              dsvError = dashboard.pageDetails["text93"];
                            });
                          } else if ((dashboard.mypriceofcoin['dsv_balance']) <
                              _amount) {
                            setState1(() {
                              dsvError = dashboard.pageDetails["text91"];
                            });
                          } else if (dsvError == null) {
                            waiting.waitpopup(context);

                            //-----------------------final api call-------------------
                            Map<String, dynamic> payload = {
                              'system_product_id': appSettings['SITE_ID'],
                              'amount': _amount,
                            };

                            Map<String, dynamic> result =
                                await HttpRequest.Post('dsvtodsvp',
                                    Utils.constructPayload(payload));
                            if (Utils.isServerError(result)) {
                              // Navigator.of(context).pop();
                              Navigator.pop(context);

                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) =>
                                          Home(route: 'dashboard')));
                              snack.snack(
                                  title: dashboard.pageDetails["text137"]);
                              // ScaffoldMessenger.of(context).showSnackBar(
                              //     SnackBar(
                              //         content: MyText(
                              //             dashboard.pageDetails["text137"])));
                              // Navigator.push(
                              //     context,
                              //     MaterialPageRoute(
                              //         builder: (context) =>
                              //             Home(route: 'dashboard')));
                              // final snackBar = SnackBar(
                              //     content: MyText('DSV-P Redeem failed!'));
                              // ScaffoldMessenger.of(context)
                              //     .showSnackBar(snackBar);
                            } else {
                              Navigator.pop(context);
                              Navigator.of(context).pop();
                              showDialog(
                                context: context,
                                builder: (BuildContext context) {
                                  return AlertDialog(
                                    elevation: 24.0,
                                    backgroundColor: Pallet.popupcontainerback,
                                    shape: RoundedRectangleBorder(
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(15.0))),
                                    title: Row(
                                      children: [
                                        Image.asset("c-logo.png", width: 40),
                                        SizedBox(width: 10),
                                        MyText(
                                            text: dashboard
                                                .pageDetails["text139"],
                                            style: TextStyle(
                                                fontSize: Pallet.heading3,
                                                color: Pallet.fontcolor,
                                                fontWeight: Pallet.font500)),
                                      ],
                                    ),
                                    content: Container(
                                      width: 250,
                                      child: SingleChildScrollView(
                                        child: ListBody(
                                          children: <Widget>[
                                            MyText(
                                                text: dashboard
                                                    .pageDetails["text140"],
                                                textAlign: TextAlign.center,
                                                style: TextStyle(
                                                  fontSize: Pallet.heading2,
                                                  fontWeight: Pallet.font500,
                                                  height: 1.5,
                                                  color: Pallet.fontcolor,
                                                )),
                                          ],
                                        ),
                                      ),
                                    ),
                                    actions: [
                                      PopupButton(
                                        text: dashboard.pageDetails["text82"],
                                        onpress: () {
                                          Navigator.push(
                                              context,
                                              MaterialPageRoute(
                                                  builder: (context) => Home(
                                                      route: 'dashboard')));
                                        },
                                      ),
                                    ],
                                  );
                                },
                              );
                            }

                            dsvcontroller.text = "";
                          }
                        },
                      ),
                    ],
                  ),
                ),
              ),
              actions: [
                PopupButton(
                  text: dashboard.pageDetails["text78"],
                  onpress: () {
                    Navigator.of(context).pop();
                    // Navigator.of(context).pop();
                  },
                ),
              ],
            );
          },
        );
      },
    );
  }
}

class _Row {
  _Row(
    this.valueA,
    this.valueB,
    this.valueC,
    this.valueD,
  );

  final String valueA;
  final String valueB;
  final String valueC;
  final String valueD;

  bool selected = false;
}

class _DataSource extends DataTableSource {
  _DataSource(this.context) {
    _rows = <_Row>[
      for (var rec in dashboard.lockinCoins)
        if (rec['has_intrest'] == true)
          _Row(rec['amount'].toString(), rec['months'].toString(),
              rec['completed_at'].toString(), rec['intrest'].toString())
        else
          _Row(rec['amount'].toString(), rec['months'].toString(),
              rec['completed_at'].toString(), '0'),
    ];
  }

  final BuildContext context;
  List<_Row> _rows;

  int _selectedCount = 0;

  @override
  DataRow getRow(int index) {
    assert(index >= 0);
    if (index >= _rows.length) return null;
    final row = _rows[index];
    return DataRow.byIndex(
      index: index,
      cells: [
        DataCell(Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [MyText(text: row.valueA)],
        )),
        DataCell(MyText(text: row.valueB)),
        DataCell(MyText(text: row.valueC)),
        DataCell(MyText(text: row.valueD.toString())),
      ],
    );
  }

  @override
  int get rowCount => _rows.length;

  @override
  bool get isRowCountApproximate => false;

  @override
  int get selectedRowCount => _selectedCount;
}
