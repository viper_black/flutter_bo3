import 'package:centurion/config/app_settings.dart';

import '../common.dart';
import '../services/communication/http/index.dart' show HttpRequest;
import 'package:centurion/utils/utils.dart';
import 'package:flutter/material.dart';
// ignore: avoid_web_libraries_in_flutter
import 'package:universal_html/html.dart' as html;
import '../config.dart';
import '../login.dart';
// import '../login.dart';
// import 'dashboard.dart';

class Success extends StatefulWidget {
  final String value;

  const Success({Key key, this.value}) : super(key: key);
  @override
  _SuccessState createState() => _SuccessState();
}

class _SuccessState extends State<Success> {
  bool loginstatus = false;

  @override
  void initState() {
    print(widget.value);
    checkloginstatus();
    print("cccccxzzzkkkk");
    super.initState();
  }

  checkloginstatus() async {
    Map<String, dynamic> _map;
    Map<String, dynamic> _result;
    print('JJJJJJJJJJJ');

    _map = {'system_product_id': 1};
    _result = await HttpRequest.Post('getdsv_p', Utils.constructPayload(_map));
    if (Utils.isServerError(_result)) {
      setState(() {
        loginstatus = false;
      });
    } else {
      setState(() {
        loginstatus = true;
      });
      return "start";
    }
  }

  @override
  Widget build(BuildContext context) {
    // double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;
    return LayoutBuilder(
      builder: (context, constraints) {
        if (constraints.maxWidth <= ScreenSize.iphone) {
          return Scaffold(
            backgroundColor: Colors.white,
            body: Center(
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    widget.value == 'success'
                        ? success(
                            wdgtWidth: width,
                          )
                        : failure(
                            wdgtWidth: width,
                          ),
                  ],
                ),
              ),
            ),
          );
        } else if (constraints.maxWidth > ScreenSize.iphone &&
            constraints.maxWidth < ScreenSize.ipad) {
          return Scaffold(
            backgroundColor: Colors.white,
            body: Padding(
              padding: EdgeInsets.all(Pallet.leftPadding),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  widget.value == 'success'
                      ? success(
                          wdgtWidth: width,
                        )
                      : failure(
                          wdgtWidth: width,
                        ),
                ],
              ),
            ),
          );
        } else {
          return Scaffold(
            backgroundColor: Colors.white,
            body: Center(
              child: Padding(
                padding: EdgeInsets.all(Pallet.leftPadding),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    widget.value == 'success'
                        ? success(
                            wdgtWidth: width,
                          )
                        : failure(
                            wdgtWidth: width,
                          ),
                  ],
                ),
              ),
            ),
          );
        }
      },
    );
  }

  Widget success({double wdgtWidth, double wdgtHeight}) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Image.asset(
          "success.gif",
          //color: Colors.white.withOpacity(0.1),
          height: MediaQuery.of(context).size.height * 0.3,
          width: MediaQuery.of(context).size.width * 0.3,
        ),
        SizedBox(height: 15),
        InkWell(
          onTap: () {
            if (loginstatus == true) {
              html.window.location.href = appSettings["CLIENT_URL"];
            } else if (loginstatus == false || loginstatus == null) {
              Navigator.pushReplacement(
                context,
                MaterialPageRoute(
                  builder: (BuildContext context) => Login(),
                ),
              );
            }
          },
          child: MyText(text:
            "Payment Success Click here to continue....",
            style: TextStyle(
              color: Pallet.dashcontainerback,
              fontSize: 16,
            ),
          ),
        )
      ],
    );
  }

  Widget failure({double wdgtWidth, double wdgtHeight}) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Image.asset(
          "oops1.gif",
          //color: Colors.white.withOpacity(0.1),
          height: MediaQuery.of(context).size.height * 0.3,
          width: MediaQuery.of(context).size.width * 0.3,
        ),
        SizedBox(height: 15),
        InkWell(
          onTap: () {
            if (loginstatus = true) {
              html.window.location.href = appSettings["CLIENT_URL"];
            } else if (loginstatus == false || loginstatus == null) {
              Navigator.pushReplacement(
                context,
                MaterialPageRoute(
                  builder: (BuildContext context) => Login(),
                ),
              );
            }
          },
          child: MyText(text:
            "Payment failed Click here to continue....",
            style: TextStyle(
              color: Pallet.dashcontainerback,
              fontSize: 16,
            ),
          ),
        )
      ],
    );
  }
}
