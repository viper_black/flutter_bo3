import '../services/communication/index.dart' show HttpRequest;

// import 'package:centurion/services/communication/http/index.dart';
import 'package:centurion/utils/utils.dart';
import 'package:flutter/material.dart';
// import 'package:centurion/screens/menu.dart';
import 'package:centurion/services/business/account.dart';

import 'package:url_launcher/url_launcher.dart';

import '../common.dart';
import '../config.dart';

class Help extends StatefulWidget {
  final double wdgtWidth, wdgtHeight;

  const Help({Key key, this.wdgtWidth, this.wdgtHeight}) : super(key: key);
  @override
  _HelpState createState() => _HelpState();
}

class _HelpState extends State<Help> {
  List list = [];
  List expansionList = [];

  bool showQA = false;

  Future<String> temp;
  String faqHeadingId = 'null';
  String heading;

  Map<String, dynamic> pageDetails = {};
  initState() {
    temp = getFAQ();
    // Utils.getPageDetails('help');

    setPageDetails();
    super.initState();
  }

  void setPageDetails() async {
    String data = await Utils.getPageDetails('help');
    setState(() {
      pageDetails = Utils.fromJSONString(data);
    });
  }

  Future<String> getFAQ() async {
    String value = await Utils.getSharedPrefereces('language');
    String language = await Utils.fromJSONString(value)['language'];
    Map<String, dynamic> map;
    print('LANGUAGEddddddd: $language');
    if (language == 'Vietnamese' ||
        language == 'English' ||
        language == 'German') {
      map = {
        'faq_heading_id': faqHeadingId,
        'faq_data_id': 'null',
        'language': language
      };
    } else {
      map = {
        'faq_heading_id': faqHeadingId,
        'faq_data_id': 'null',
        'language': 'English',
      };
    }
    list = [];
    Map<String, dynamic> result =
        await HttpRequest.Post('FAQ', Utils.constructPayload(map));
    print('$result, FAQ');
    if (Utils.isServerError(result)) {
      print(result['response']['error']);

      throw (await Utils.getMessage(
        result['response']['error'],
      ));
    } else {
      print('Data');
      setState(() {
        list = result['response']['data'];
      });
      expansionList = List.generate(list.length, (i) {
        return false;
      });
      print(list);
      return 'Start';
    }
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
        future: temp,
        builder: (context, snapshot) {
          if (snapshot.hasError) {
            if (snapshot.error == 'Something Went Wrong') {
              return SomethingWentWrongMessage();
            } else {
              return Error(
                message: snapshot.error != null ? snapshot.error : '',
              );
            }
          } else if (snapshot.hasData) {
            return LayoutBuilder(
              builder: (context, constraints) {
                if (constraints.maxWidth <= ScreenSize.iphone) {
                  return Center(
                    child: Padding(
                      padding: EdgeInsets.only(
                          left: Pallet.defaultPadding,
                          top: Pallet.topPadding1,
                          right: Pallet.defaultPadding),
                      child: SingleChildScrollView(
                        child: Column(
                          children: [
                            fAQ(widget.wdgtWidth),
                            SizedBox(height: 20),
                            help(wdgtWidth: widget.wdgtWidth),
                          ],
                        ),
                      ),
                    ),
                  );
                } else if (constraints.maxWidth > ScreenSize.iphone &&
                    constraints.maxWidth < ScreenSize.ipad) {
                  return Padding(
                    padding: EdgeInsets.only(
                        left: Pallet.leftPadding, top: Pallet.topPadding1),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: [
                        Container(
                          height: widget.wdgtHeight * .65,
                          child: SingleChildScrollView(
                              child: fAQ(widget.wdgtWidth * 0.80)),
                        ),
                        // SizedBox(height: 20),
                        help(wdgtWidth: widget.wdgtWidth * 0.80),
                      ],
                    ),
                  );
                } else {
                  return Padding(
                    padding: EdgeInsets.only(
                        left: Pallet.leftPadding, top: Pallet.topPadding1),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: [
                        Container(
                            height: widget.wdgtHeight * .7,
                            child: SingleChildScrollView(
                                child: fAQ(widget.wdgtWidth * 0.80))),
                        // SizedBox(height: 20),
                        help(wdgtWidth: widget.wdgtWidth * 0.80),
                      ],
                    ),
                  );
                }
              },
            );
          }
          return Loader();
        });
  }

  openurl() {
    String url = dashboard.accountType == 2
        ? "mailto:" + Pallet.helpmailnetwork
        : "mailto:" + Pallet.helpmailcustomer;
    launch(url);
  }

  Widget help({double wdgtWidth, double wdgtHeight}) {
    return Container(
      width: wdgtWidth,
      // height: wdgtHeight,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              // MyText(
              //     text: pageDetails["text1"],
              //     style: TextStyle(
              //       color: Pallet.fontcolornew,
              //       fontSize: Pallet.heading1,
              //     )),
            ],
          ),
          SizedBox(height: 30),
          Container(
              alignment: Alignment.center,
              // width: widget.wdgtWidth,
              decoration: BoxDecoration(
                  color: Pallet.fontcolor,
                  // border: Border.all(color: Pallet.fontcolornew),ss
                  boxShadow: [Pallet.shadowEffect],
                  borderRadius: BorderRadius.circular(Pallet.radius)),
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Wrap(
                    // mainAxisAlignment: MainAxisAlignment.center,
                    // crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      // Padding(
                      //   padding: EdgeInsets.all(Pallet.defaultPadding),
                      //   child: MyText(
                      //       text: pageDetails["text2"],
                      //       style: TextStyle(
                      //           color: Pallet.fontcolornew,
                      //           fontSize: Pallet.heading2)),
                      // ),
                      // Padding(
                      //   padding: EdgeInsets.all(Pallet.defaultPadding),
                      //   child: MyText(
                      //       text: pageDetails["text3"],
                      //       style: TextStyle(
                      //         color: Pallet.fontcolornew,
                      //         fontSize: Pallet.heading3,
                      //       )),
                      // ),
                      // SizedBox(height: 10),
                      Padding(
                        padding: const EdgeInsets.symmetric(vertical: 8),
                        child: MyText(
                          text: 'Need Assitance? ',
                          style: TextStyle(
                              color: Pallet.fontcolornew,
                              fontWeight: Pallet.bold,
                              fontSize: Pallet.heading2),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(vertical: 8),
                        child: MyText(
                          text:
                              'Send Your Queries By Clicking On This E-Mail ID ',
                          style: TextStyle(
                              color: Pallet.fontcolornew,
                              fontSize: Pallet.heading3),
                        ),
                      ),
                      InkWell(
                        // child: Padding(
                        //   padding: EdgeInsets.all(Pallet.defaultPadding),
                        child: Container(
                          padding: const EdgeInsets.all(8),

                          decoration: BoxDecoration(
                              borderRadius:
                                  BorderRadius.circular(Pallet.radius),
                              border: Border.all(color: Pallet.fontcolornew
                                  // bottom:
                                  //     BorderSide(color: Pallet.fontcolornew)
                                  )
                              // only(color: Pallet.fontcolornew)
                              ),
                          child: MyText(
                            text: dashboard.accountType == 2
                                ? Pallet.helpmailnetwork
                                : Pallet.helpmailcustomer,
                            style: TextStyle(
                                color: Pallet.fontcolornew,
                                fontSize: Pallet.heading3),
                          ),
                          // ),
                        ),
                        onTap: () {
                          openurl();
                        },
                      ),
                      SizedBox(height: 15)
                    ]),
              )),
        ],
      ),
    );
  }

  Widget fAQ(width) {
    return Container(
      width: width,
      child: Column(
        children: [
          Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    MyText(
                      text: 'Frequently asked questions',
                      style: TextStyle(
                        color: Pallet.fontcolornew,
                        fontSize: Pallet.heading1,
                      ),
                    ),
                    SizedBox(height: 20),
                    MyText(
                      text:
                          'Most frequently asked questions and answers to them',
                      style: TextStyle(
                        color: Pallet.fontcolornew,
                        fontSize: Pallet.heading3,
                      ),
                    ),
                    // MyText(
                    //   text: 'answers to them',
                    //   style: TextStyle(
                    //     color: Pallet.fontcolornew,
                    //     fontSize: Pallet.heading2,
                    //   ),
                    // ),
                  ],
                ),
              ),
              Container(
                  width: 80,
                  height: 80,
                  child: Image.asset(
                    'FAQ1.png',
                  ))
              // Icon(
              //   Icons.,
              //   color: Colors.black,
              //   size: 50,
              // )
            ],
          ),
          SizedBox(
            height: 20,
          ),
          showQA == false
              ? FutureBuilder<Object>(
                  future: temp,
                  builder: (context, snapshot) {
                    return Wrap(
                      spacing: 10,
                      runSpacing: 20,
                      children: [
                        for (var i = 0; i < list.length; i++)
                          InkWell(
                            onTap: () {
                              setState(() {
                                faqHeadingId = list[i]['heading_id'].toString();
                                showQA = true;
                                heading = list[i]['heading_name'].toString();
                              });
                              getFAQ();
                              print(list[i]['heading_id']);
                            },
                            child: Container(
                              decoration: BoxDecoration(
                                color: Pallet.fontcolornew,
                                borderRadius: BorderRadius.circular(
                                  10,
                                ),
                              ),
                              constraints: BoxConstraints(
                                minHeight: 100,
                                maxWidth: 200,
                              ),
                              padding: EdgeInsets.all(20),
                              child: Center(
                                child: Text(
                                  list[i]['heading_name'].toString(),
                                  style: TextStyle(
                                    color: Pallet.fontcolor,
                                    fontSize: Pallet.heading2,
                                  ),
                                ),
                              ),
                            ),
                          ),
                      ],
                    );
                  })
              : FutureBuilder<Object>(
                  future: temp,
                  builder: (context, snapshot) {
                    return Column(
                      children: [
                        Row(
                          children: [
                            IconButton(
                              onPressed: () {
                                setState(() {
                                  faqHeadingId = 'null';
                                  showQA = false;
                                });
                                getFAQ();
                              },
                              icon: Icon(Icons.arrow_back),
                              color: Colors.black,
                            ),
                            Expanded(
                              child: Text(
                                heading,
                                style: TextStyle(
                                    color: Pallet.fontcolornew,
                                    fontSize: Pallet.heading2,
                                    fontWeight: FontWeight.bold),
                              ),
                            ),
                          ],
                        ),
                        ExpansionPanelList(
                          children: [
                            for (var i = 0; i < list.length; i++)
                              ExpansionPanel(
                                canTapOnHeader: true,
                                body: Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      for (var item in list[i]['answer'])
                                        Padding(
                                          padding: const EdgeInsets.all(8.0),
                                          child: Row(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            children: [
                                              // SizedBox(
                                              //   height: 4,
                                              // ),
                                              Padding(
                                                padding: const EdgeInsets.only(
                                                    top: 1.0),
                                                child: Container(
                                                  width: 18,
                                                  height: 18,
                                                  child: Image.asset(
                                                    'bullet2.png',
                                                  ),
                                                  // decoration: BoxDecoration(
                                                  //     color: Colors.black,
                                                  //     shape: BoxShape.circle),
                                                ),
                                              ),
                                              SizedBox(
                                                width: 10,
                                              ),
                                              Expanded(
                                                child: Text(
                                                  item.toString(),
                                                  style: TextStyle(
                                                    color: Pallet.fontcolornew,
                                                    fontSize: Pallet.heading3,
                                                  ),
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                    ],
                                  ),
                                ),
                                isExpanded: expansionList[i],
                                headerBuilder:
                                    (BuildContext context, bool isExpanded) {
                                  return Padding(
                                    padding: const EdgeInsets.all(8.0),
                                    child: Text(
                                      list[i]['questions'].toString(),
                                      style: TextStyle(
                                          color: Colors.black,
                                          fontSize: Pallet.heading2,
                                          fontWeight: FontWeight.bold),
                                    ),
                                  );
                                },
                              ),
                          ],
                          expansionCallback: (int item, bool status) {
                            for (var i = 0; i < expansionList.length; i++) {
                              expansionList[i] = false;
                            }
                            setState(
                              () {
                                expansionList[item] = !expansionList[item];
                              },
                            );
                          },
                        ),
                      ],
                    );
                  },
                ),
        ],
      ),
    );
  }
}
