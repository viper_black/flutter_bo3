import 'package:centurion/config/index.dart';
import 'package:centurion/utils/utils.dart';
import 'package:flutter/material.dart';
// import 'package:carousel_slider/carousel_slider.dart';
import '../common.dart';
import '../config.dart';
// import 'package:flutter_icons/flutter_icons.dart';

// import '../screens/myshop.dart';
// import 'package:percent_indicator/percent_indicator.dart';
// import 'package:intl/intl.dart';
// import '../config/index.dart';
import '../services/business/account.dart';

Map<String, dynamic> pageDetails = {};

class Bonus extends StatefulWidget {
  const Bonus({Key key, this.wdgtWidth, this.wdgtHeight, this.assetType})
      : super(key: key);
  final double wdgtWidth, wdgtHeight;
  final String assetType;
  @override
  _BonusState createState() => _BonusState();
}

class _BonusState extends State<Bonus> {
  void setPageDetails() async {
    String data = await Utils.getPageDetails('bonus');
    setState(() {
      pageDetails = Utils.fromJSONString(data);
    });
  }

  Future<String> temp;
  double sample = 0;
  void initState() {
    setPageDetails();
    temp = bonus.statement('null', setState);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
        future: temp,
        builder: (context, snapshot) {
          if (snapshot.hasError) {
            return SomethingWentWrongMessage();
          } else if (snapshot.hasData) {
            return LayoutBuilder(builder: (context, constraints) {
              if (constraints.maxWidth < ScreenSize.iphone) {
                return SingleChildScrollView(
                  child: Padding(
                    padding: EdgeInsets.only(
                        left: Pallet.defaultPadding,
                        top: Pallet.topPadding1,
                        right: Pallet.defaultPadding),
                    child: Column(
                      children: [
                        bonusPage(),
                      ],
                    ),
                  ),
                );
              } else if (constraints.maxWidth > ScreenSize.iphone &&
                  constraints.maxWidth < ScreenSize.ipad) {
                return SingleChildScrollView(
                  child: Padding(
                    padding: EdgeInsets.all(Pallet.leftPadding),
                    child: Column(
                      children: [
                        bonusPage(),
                      ],
                    ),
                  ),
                );
              } else {
                return SingleChildScrollView(
                  child: Padding(
                    padding: EdgeInsets.only(
                        left: Pallet.leftPadding, top: Pallet.topPadding1),
                    child: Column(
                      children: [
                        bonusPage(),
                      ],
                    ),
                  ),
                );
              }
            });
          }
          return Loader();
        });
  }

  Container bonusPage() {
    return Container(
      width: widget.wdgtWidth,
      height: widget.wdgtHeight,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          if (bonus.name == 'direct_bonus')
            Padding(
                padding: EdgeInsets.only(bottom: Pallet.defaultPadding),
                child: MyText(
                  text: pageDetails["text8"],
                  style: TextStyle(
                    fontSize: Pallet.heading1,
                    color: Pallet.fontcolornew,
                  ),
                )),
          if (bonus.name == 'team_bonus')
            Padding(
                padding: EdgeInsets.only(bottom: Pallet.defaultPadding),
                child: MyText(
                  text: pageDetails["text9"],
                  style: TextStyle(
                    fontSize: Pallet.heading1,
                    color: Pallet.fontcolornew,
                  ),
                )),
          if (bonus.name == 'matching_bonus')
            Padding(
                padding: EdgeInsets.only(bottom: Pallet.defaultPadding),
                child: MyText(
                  text: pageDetails["text10"],
                  style: TextStyle(
                    fontSize: Pallet.heading1,
                    color: Pallet.fontcolornew,
                  ),
                )),
          if (bonus.name == 'rank_bonus')
            Padding(
                padding: EdgeInsets.only(bottom: Pallet.defaultPadding),
                child: MyText(
                  text: pageDetails["text11"],
                  style: TextStyle(
                    fontSize: Pallet.heading1,
                    color: Pallet.fontcolornew,
                  ),
                )),
          if (bonus.name == 'fast_start_bonus')
            Padding(
                padding: EdgeInsets.only(bottom: Pallet.defaultPadding),
                child: MyText(
                 text:  pageDetails["text12"],
                  style: TextStyle(
                    fontSize: Pallet.heading1,
                    color: Pallet.fontcolornew,
                  ),
                )),
          if (bonus.name == 'life_style_bonus')
            Padding(
                padding: EdgeInsets.only(bottom: Pallet.defaultPadding),
                child: MyText(
                  text: pageDetails["text13"],
                  style: TextStyle(
                    fontSize: Pallet.heading1,
                    color: Pallet.fontcolornew,
                  ),
                )),
          if (bonus.name == 'ruler_bonus')
            Padding(
                padding: EdgeInsets.only(bottom: Pallet.defaultPadding),
                child: MyText(
                 text:  pageDetails["text14"],
                  style: TextStyle(
                    fontSize: Pallet.heading1,
                    color: Pallet.fontcolornew,
                  ),
                )),
          if (bonus.name == 'world_bonus')
            Padding(
                padding: EdgeInsets.only(bottom: Pallet.defaultPadding),
                child: MyText(
                  text: pageDetails["text15"],
                  style: TextStyle(
                    fontSize: Pallet.heading1,
                    color: Pallet.fontcolornew,
                  ),
                )),
          if (bonus.name == 'fund_transfer')
            Padding(
                padding: EdgeInsets.only(bottom: Pallet.defaultPadding),
                child: MyText(
                 text:  pageDetails["text16"],
                  style: TextStyle(
                    fontSize: Pallet.heading1,
                    color: Pallet.fontcolornew,
                  ),
                )),
          Container(
              width: 250,
              padding: EdgeInsets.symmetric(
                  horizontal: Pallet.defaultPadding,
                  vertical: Pallet.defaultPadding),
              decoration: BoxDecoration(
                  boxShadow: [Pallet.shadowEffect],
                  color: Pallet.fontcolor,
                  borderRadius: BorderRadius.circular(Pallet.radius)),
              // margin: EdgeInsets.only(
              //     left: 8 + wdgtWidth * 0.01, top: 10, bottom: 30),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  MyText(
                   text:  pageDetails["text1"],
                    style: TextStyle(
                        fontSize: Pallet.heading2,
                        color: Pallet.dashcontainerback),
                  ),
                  MyText(
                      text: '\$ ' + bonus.total.toString() == null
                          ? ''
                          : bonus.total.toString(),
                      style: TextStyle(
                          fontSize: 25, color: Pallet.dashcontainerback))
                ],
              )),
          SizedBox(height: 15.0),
          Container(
            padding: EdgeInsets.all(Pallet.defaultPadding),
            decoration: BoxDecoration(
                color: Pallet.fontcolor,
                boxShadow: [Pallet.shadowEffect],
                borderRadius: BorderRadius.circular(Pallet.radius)),
            height: widget.wdgtHeight - 250,
            child: DefaultTabController(
              length: bonus.bonusTrans.length,
              child: Scaffold(
                backgroundColor: Colors.transparent,
                appBar: AppBar(
                  automaticallyImplyLeading: false,
                  elevation: 0,
                  backgroundColor: Colors.transparent,
                  flexibleSpace: TabBar(
                    isScrollable: true,
                    indicator: BoxDecoration(
                        borderRadius: BorderRadius.circular(Pallet.radius),
                        color: Pallet.component),
                    tabs: [
                      for (var data in bonus.bonusTrans)
                        Tab(
                            icon: Row(
                          children: [
                            ImageIcon(
                              NetworkImage(appSettings['SERVER_URL'] +
                                  '/' +
                                  data["trans_icon"]),
                              color: Pallet.dashcontainerback,
                              size: 30,
                            ),
                            SizedBox(width: 10),
                            if (data['trans_type'] == 'direct_bonus')
                              MyText(
                               text:  pageDetails["text8"],
                                style: TextStyle(
                                    fontSize: 16.0, color: Pallet.fontcolornew),
                              ),
                            if (data['trans_type'] == 'team_bonus')
                              MyText(
                                text: pageDetails["text9"],
                                style: TextStyle(
                                    fontSize: 16.0, color: Pallet.fontcolornew),
                              ),
                            if (data['trans_type'] == 'matching_bonus')
                              MyText(
                              text:   pageDetails["text10"],
                                style: TextStyle(
                                    fontSize: 16.0, color: Pallet.fontcolornew),
                              ),
                            if (data['trans_type'] == 'rank_bonus')
                              MyText(
                               text:  pageDetails["text11"],
                                style: TextStyle(
                                    fontSize: 16.0, color: Pallet.fontcolornew),
                              ),
                            if (data['trans_type'] == 'fast_start_bonus')
                              MyText(
                                text: pageDetails["text12"],
                                style: TextStyle(
                                    fontSize: 16.0, color: Pallet.fontcolornew),
                              ),
                            if (data['trans_type'] == 'life_style_bonus')
                              MyText(
                                text: pageDetails["text13"],
                                style: TextStyle(
                                    fontSize: 16.0, color: Pallet.fontcolornew),
                              ),
                            if (data['trans_type'] == 'ruler_bonus')
                              MyText(
                                text: pageDetails["text14"],
                                style: TextStyle(
                                    fontSize: 16.0, color: Pallet.fontcolornew),
                              ),
                            if (data['trans_type'] == 'world_bonus')
                              MyText(
                                text: pageDetails["text15"],
                                style: TextStyle(
                                    fontSize: 16.0, color: Pallet.fontcolornew),
                              ),
                            if (data['trans_type'] == 'fund_transfer')
                              MyText(
                                text: pageDetails["text16"],
                                style: TextStyle(
                                    fontSize: 16.0, color: Pallet.fontcolornew),
                              ),
                            // MyText(
                            //   data["trans_name"],
                            //   style: TextStyle(
                            //       fontSize: 16.0, color: Pallet.fontcolornew),
                            // )
                          ],
                        )),
                    ],
                  ),
                ),
                body: TabBarView(
                  children: [
                    for (var data in bonus.bonusTrans)
                      Trans(transType: data["trans_type"]),
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class Trans extends StatefulWidget {
  Trans({
    Key key,
    this.transType,
    this.wdgtWidth,
    this.wdgtHeight,
  }) : super(key: key);
  final String transType;

  final double wdgtWidth, wdgtHeight;
  @override
  _TransState createState() => _TransState();
}

class _TransState extends State<Trans> {
  // Map<String, dynamic> pageDetails = {};

  // void setPageDetails() async {
  //   String data = await Utils.getPageDetails('bonus');
  //   setState(() {
  //     pageDetails = Utils.fromJSONString(data);
  //   });
  // }

  @override
  void initState() {
    //  setPageDetails();

    bonus.statement(widget.transType, setState);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      width: widget.wdgtWidth,
      height: widget.wdgtHeight,
      child: ListView(
        children: [
          PaginatedDataTable(
            header: MyText(
              text: pageDetails["text2"],
              style: TextStyle(
                color: Pallet.fontcolornew,
                fontSize: Pallet.heading2,
                fontWeight: Pallet.subheading1wgt,
              ),
            ),
            rowsPerPage: 10,
            columns: [
              DataColumn(
                  label: MyText( text:'Date',
                      style: TextStyle(
                          color: Pallet.dashcontainerback,
                          fontSize: Pallet.heading3,
                          fontWeight: Pallet.heading2wgt))),
              DataColumn(
                  label: MyText( text:pageDetails["text4"],
                      style: TextStyle(
                          color: Pallet.dashcontainerback,
                          fontSize: Pallet.heading3,
                          fontWeight: Pallet.heading2wgt))),
              DataColumn(
                  label: MyText( text:pageDetails["text5"],
                      style: TextStyle(
                          color: Pallet.dashcontainerback,
                          fontSize: Pallet.heading3,
                          fontWeight: Pallet.heading2wgt))),
              // DataColumn(
              //     label: MyText(pageDetails["text6"],
              //         style: TextStyle(
              //             color: Pallet.dashcontainerback,
              //             fontSize: Pallet.heading3,
              //             fontWeight: Pallet.heading2wgt))),
              // DataColumn(
              //     label: MyText('Remarks',
              //         style: TextStyle(
              //             color: Pallet.dashcontainerback,
              //             fontSize: Pallet.heading3,
              //             fontWeight: Pallet.heading2wgt))),
              DataColumn(
                  label: MyText( text:pageDetails["text7"],
                      style: TextStyle(
                          color: Pallet.dashcontainerback,
                          fontSize: Pallet.heading3,
                          fontWeight: Pallet.heading2wgt))),
            ],
            source: _DataSource(context),
          ),
        ],
      ),
    );
  }
}

class _Row {
  _Row(
    this.valueA,
    this.valueB,
    this.valueC,
    // this.valueD,
    // this.valueE,
    this.valueF,
  );

  final String valueA;
  final String valueB;
  final String valueC;
  // final String valueD;
  // final String valueE;
  final String valueF;

  bool selected = false;
}

class _DataSource extends DataTableSource {
  _DataSource(this.context) {
    // ignore: unused_local_variable
    var localCastingCaps = {
      "credit": "Credit",
      "success": "Success",
    };

    var localNarration = {
      "direct bonus credited to charity account":
          "Direct Bonus credited to charity account",
    };
    _rows = <_Row>[
      for (var trans in bonus.bonusState)
        _Row(
          // localCastingCaps[trans["updated_at"].toString()] == null
          //     ? null
          //     : localCastingCaps[trans["updated_at"].toString()],
          trans["updated_at"].toString() == null
              ? 'None'
              : trans["updated_at"].toString(),
          trans["from_user"].toString() == null
              ? 'None'
              : trans["from_user"].toString(),
          trans["amount"].toString() == null
              ? 'None'
              : trans["amount"].toString(),
          // localCastingCaps[trans["trans_type"].toString()] == null
          //     ? null
          //     : localCastingCaps[trans["trans_type"].toString()],
          // trans["remarks"].toString() == 'null'
          //     ? ' '
          //     : trans["remarks"].toString(),
          localNarration[trans["naration"].toString()] == null
              ? 'None'
              : localNarration[trans["naration"].toString()],
        ),
    ];
  }

  final BuildContext context;
  List<_Row> _rows;

  int _selectedCount = 0;

  @override
  DataRow getRow(int index) {
    assert(index >= 0);
    if (index >= _rows.length) return null;
    final row = _rows[index];
    return DataRow.byIndex(
      index: index,
      cells: [
        // DataCell(row.valueA.toString() == 'Credit'
        //     ? MyText(row.valueA, style: TextStyle(color: Colors.green))
        //     : MyText(row.valueA, style: TextStyle(color: Colors.red))),
        DataCell(
            MyText( text:row.valueA, style: TextStyle(color: Pallet.fontcolornew))),
        DataCell(
            MyText( text:row.valueB, style: TextStyle(color: Pallet.fontcolornew))),
        DataCell(
            MyText( text:row.valueC, style: TextStyle(color: Pallet.fontcolornew))),
        // DataCell(row.valueA.toString() == 'Credit'
        //     ? MyText(row.valueC, style: TextStyle(color: Colors.green))
        //     : MyText(row.valueC, style: TextStyle(color: Colors.red))),
        // // if (row.valueD.toString() == "Success")
        // //   DataCell(MyText(row.valueD.toString(),
        // //       style: TextStyle(color: Colors.green)))
        // // else if (row.valueD.toString() == "pending")
        // //   DataCell(MyText(row.valueD.toString(),
        // //       style: TextStyle(color: Colors.yellow[700]))),
        // // else if (row.valueD.toString() == "Success")
        // //   DataCell(
        // //       MyText(row.valueD.toString(), style: TextStyle(color: Colors.red))),
        // DataCell(
        //     MyText(row.valueE, style: TextStyle(color: Pallet.fontcolornew))),
        DataCell(
            MyText( text:row.valueF, style: TextStyle(color: Pallet.fontcolornew))),
      ],
    );
  }

  @override
  int get rowCount => _rows.length;

  @override
  bool get isRowCountApproximate => false;

  @override
  int get selectedRowCount => _selectedCount;
}
