import 'package:centurion/utils/utils.dart';
import 'package:flutter/rendering.dart';
import '../common.dart';
import 'package:flutter/material.dart';
import '../config.dart';
import '../services/business/cart.dart';
import '../config/app_settings.dart';
import '../home.dart';
import 'cataegory.dart';

class CheckoutPage extends StatefulWidget {
  CheckoutPage({
    Key key,
    this.wdgtWidth,
    this.setShop,
    this.wdgtHeight,
    this.setData,
  }) : super(key: key);

  final double wdgtWidth, wdgtHeight;
  final setData;
  final setShop;
  @override
  _CheckoutPageState createState() => _CheckoutPageState();
}

class _CheckoutPageState extends State<CheckoutPage> {
  Future<String> _temp;
  final addressform = GlobalKey<FormState>();
  TextEditingController addressline1 = TextEditingController();
  TextEditingController addressline2 = TextEditingController();
  TextEditingController addressname = TextEditingController();
  TextEditingController phonenumber = TextEditingController();
  TextEditingController country = TextEditingController();
  TextEditingController state = TextEditingController();
  TextEditingController district = TextEditingController();
  TextEditingController postalcode = TextEditingController();
  String nameError;
  String addressline1Error;
  String addressline2Error;
  String phonenumberError;
  String countryError;
  String stateError;
  String districtError;

  String postalcodeError;

  String dropDownCountry = '';
  String dropDownCity = '';
  String dropDownZip = '';

  FocusNode statefocus = FocusNode();
  FocusNode postalcodefocus = FocusNode();

  FocusNode districtfocus = FocusNode();
  FocusNode countryfocus = FocusNode();
  FocusNode phonenumberfocus = FocusNode();
  FocusNode addressline1focus = FocusNode();
  FocusNode addressline2focus = FocusNode();
  FocusNode addressnamefocus = FocusNode();
  String isMobile;
  String isAdd;
  bool isRequired = false;
  bool enablecity = false;
  bool enablezipcode = false;
  FocusNode continuenode = FocusNode();
  Fund fund = Fund();
  Map<String, dynamic> getRegionsMap = {
    'system_product_id': 1,
    'region_level': 2,
    'parent_region': 'null'
  };

  List<DropdownMenuItem<String>> countryList = [];
  setSelectedRadio(int val) async {
    setState(() {
      cart.selectRadio = val;
    });
    cart.country.text = await cart.address[val]["address_name"];
    await cart.setDefaultAddress(cart.country.text);
    // await cart.setDefaultAddress(cart.address[val]["address_name"]);
    setState(() {
      cart.myCart(setState, widget.setData);
    });
  }

  refresh() {
    cart.myCart(setState, widget.setData);
    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (BuildContext context) => Home(
                  route: 'checkout',
                )));
  }

  void initState() {
    // print(countryList);
    setPageDetails();
    cart.setPageDetails1();

    _temp = cart.getAddressInfo(setState);
    cart.getRegions(getRegionsMap);
    cart.getDsvPData();
    setSelectedRadio(cart.selectRadio);
    // cart.getCountries();
    super.initState();
  } 

  Map<String, dynamic> pageDetails = {};

  void setPageDetails() async {
    String data = await Utils.getPageDetails('checkout');
    setState(() {
      pageDetails = Utils.fromJSONString(data);
    });
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
        future: _temp,
        builder: (context, snapshot) {
          if (snapshot.hasError) {
            return SomethingWentWrongMessage();
          } else if (snapshot.hasData) {
            return LayoutBuilder(builder: (context, constraints) {
              if (constraints.maxWidth <= 550) {
                isMobile = 'false';
                return SingleChildScrollView(
                  child: Padding(
                    padding: EdgeInsets.all(Pallet.defaultPadding),
                    child: Column(
                      children: [
                        // MyText(text:'Delivery Address',
                        //     style: TextStyle(
                        //       fontSize: Pallet.heading1,
                        //       color: Pallet.fontcolornew,
                        //     )),
                        buildTitle(context),

                        addressListContainer(
                            wdgtWidth: widget.wdgtWidth,
                            wdgtHeight: widget.wdgtHeight / 3),
                        cartList(
                            proName: widget.wdgtWidth * 0.350,
                            wdgtWidth: widget.wdgtWidth,
                            wdgtHeight: widget.wdgtHeight / 3),
                        paymentContainer(
                            wdgtWidth: widget.wdgtWidth,
                            wdgtHeight: widget.wdgtHeight / 3)
                      ],
                    ),
                  ),
                );
              } else if (constraints.maxWidth > 550 &&
                  constraints.maxWidth < 750) {
                isMobile = 'true';
                return SingleChildScrollView(
                  child: Padding(
                    padding: EdgeInsets.all(Pallet.defaultPadding),
                    child: Column(
                      children: [
                        // MyText(text:'Delivery Address',
                        //     style: TextStyle(
                        //       fontSize: Pallet.heading1,
                        //       color: Pallet.fontcolornew,
                        //     )),
                        buildTitle(context),

                        addressListContainer(
                            wdgtWidth: widget.wdgtWidth,
                            wdgtHeight: widget.wdgtHeight / 3),
                        cartList(
                            proName: widget.wdgtWidth * 0.350,
                            wdgtWidth: widget.wdgtWidth,
                            wdgtHeight: widget.wdgtHeight / 3),
                        paymentContainer(
                            wdgtWidth: widget.wdgtWidth,
                            wdgtHeight: widget.wdgtHeight / 3)
                      ],
                    ),
                  ),
                );
              } else {
                isMobile = 'false';
                return Padding(
                  padding: EdgeInsets.only(top: 30, left: Pallet.leftPadding),
                  child: Container(
                    height: widget.wdgtHeight,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      // mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          // crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                buildTitle(context),

                                // MyText(text:'Delivery Address',
                                //     style: TextStyle(
                                //       fontSize: Pallet.heading1,
                                //       color: Pallet.fontcolornew,
                                //     )),
                                Column(
                                  children: [
                                    addressListContainer(
                                        wdgtWidth: widget.wdgtWidth / 2.5,
                                        wdgtHeight: widget.wdgtHeight / 2),
                                    paymentContainer(
                                        wdgtWidth: widget.wdgtWidth / 2.5,
                                        wdgtHeight: widget.wdgtHeight / 4)
                                  ],
                                ),
                              ],
                            ),
                            SingleChildScrollView(
                              child: Column(
                                children: [
                                  cartList(
                                      proName: widget.wdgtWidth * 0.180,
                                      wdgtWidth: widget.wdgtWidth / 2.5,
                                      wdgtHeight: widget.wdgtHeight * .8),
                                ],
                              ),
                            ),
                          ],
                        )
                      ],
                    ),
                  ),
                );
              }
            });
          }
          return Loader();
        });
  }

  Row buildTitle(BuildContext context) {
    return Row(
      children: [
        // IconButton(
        //   icon: Icon(
        //     Icons.arrow_back,
        //     color: Pallet.fontcolornew,
        //   ),
        //   onPressed: () {
        //     widget.setShop(() {
        //       shopRoute = 'products';
        //     });
        //     // Navigator.pushReplacement(
        //     //     context,
        //     //     MaterialPageRoute(
        //     //         builder: (context) =>
        //     //             Home(param: 'false', route: 'centurion_shop')));
        //   },
        // ),
        MyText(
            text: pageDetails['text1'],
            style: TextStyle(
              color: Pallet.fontcolornew,
              fontSize: Pallet.heading1,
            ))
      ],
    );
  }

  Widget addressListContainer({
    double wdgtWidth,
    wdgtHeight,
  }) {
    return Container(
      width: wdgtWidth,
      height: wdgtHeight,
      child: Column(
        // mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              Icon(
                Icons.add,
                color: Pallet.dashcontainerback,
              ),
              InkWell(
                onTap: () {
                  isAdd = 'true';
                  isRequired = false;
                  print('22222222222222222222222222222');
                  print(postalcode.text);
                  // setState(() {
                  addaddres();
                  // showDialog(
                  //     context: context,
                  //     builder: (context) {
                  // return
                  // addressProcessPopup(
                  //   index: 0,
                  // );
                  showDialog(
                      barrierDismissible: false,
                      context: context,
                      builder: (BuildContext context) {
                        return AddressProcessPopUp(
                          isAdd: isAdd,
                          // index: 0,
                          setData: setState,
                        );
                      });

                  // AddressPopUp(
                  //   isMobile: isMobile,
                  //   setData: setState,
                  // );
                  // });
                  // });
                },
                child: MyText(
                    text: pageDetails['text2'],
                    style: TextStyle(
                        fontSize: Pallet.normalfont,
                        color: Pallet.fontcolornew,
                        fontWeight: Pallet.font500)),
              ),
            ],
          ),
          Container(
            width: wdgtWidth,
            height: wdgtHeight / 1.1,
            child: ListView.builder(
              itemCount: cart.address.length,
              scrollDirection: Axis.vertical,
              itemBuilder: (BuildContext context, int index) {
                return Container(
                    padding: EdgeInsets.all(Pallet.defaultPadding / 2),
                    decoration: BoxDecoration(
                        border: Border.all(color: Colors.grey[300]),
                        borderRadius: BorderRadius.circular(Pallet.radius),
                        color: Colors.grey[300]),
                    // height: 100,
                    margin: EdgeInsets.all(2),
                    child: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          MyText(
                            text: cart.address[index]["address_name"],
                          ),
                          MyText(text: cart.address[index]["address_line1"]),
                          Row(
                            crossAxisAlignment: CrossAxisAlignment.end,
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: [
                              Padding(
                                padding: EdgeInsets.symmetric(
                                    horizontal: Pallet.defaultPadding),
                                child: IconButton(
                                  icon: Icon(Icons.delete,
                                      color: Pallet.fontcolornew),
                                  onPressed: () {
                                    setState(() {
                                      Map<String, dynamic> _map = {
                                        "product_id": 1,
                                        "address_id": cart.address[index]
                                            ["address_id"],
                                        "address_name": "null",
                                        "address_line1": "null",
                                        "address_line2": "null",
                                        "country": "null",
                                        "region": "null",
                                        "city": "null",
                                        "postal_code": "null",
                                        "address_phone": "null",
                                        "delete": true
                                      };
                                      cart.deleteAddress(_map, setState);
                                    });
                                    setState(() {
                                      cart.myCart(setState, widget.setData);
                                    });
                                    setState(() {
                                      if (cart.address.length == 1) {
                                        cart.askpopup = true;
                                      }
                                    });
                                  },
                                ),
                              ),
                              Padding(
                                padding: EdgeInsets.symmetric(
                                    horizontal: Pallet.defaultPadding),
                                child: IconButton(
                                  icon: Icon(Icons.edit,
                                      color: Pallet.fontcolornew),
                                  onPressed: () {
                                    isAdd = 'false';

                                    setState(() {
                                      editaddres(index);
                                      // setState(() {
                                      //   setSelectedRadio(index);
                                      // });
                                      // cart.myCart(setState, widget.setData);

                                      // isMobile == true
                                      //     ?

                                      // addressProcessPopup(
                                      //   index: index,
                                      // );
                                      // : addressProcessPopup(
                                      //     index,
                                      //   );
                                    });
                                    showDialog(
                                        barrierDismissible: false,
                                        context: context,
                                        builder: (BuildContext context) {
                                          return AddressProcessPopUp(
                                            isAdd: isAdd,
                                            index: index,
                                            setData: setState,
                                            shopContext: context,
                                          );
                                        });
                                  },
                                ),
                              ),
                              Radio(
                                value: index,
                                activeColor: Pallet.fontcolornew,
                                // fillColor: Colors.green,
                                groupValue: cart.selectRadio,
                                onChanged: (int val) {
                                  print("xccccccxxxxzzz");
                                  setState(() {
                                    setSelectedRadio(val);
                                  });
                                  print("bzvsfdkdefgd");
                                },
                              )
                            ],
                          ),
                        ]));
              },
            ),
          ),
        ],
      ),
    );
  }

  // void addaddres() {
  //   setState(() {
  //     cart.address.add({
  //       'address1': '',
  //       'address_name': '',
  //       'address2': '',
  //       "country": '',
  //       "region": '',
  //       "city": '',
  //       "postal_code": '',
  //       "address_phone": ''
  //     });
  //   });
  // }

  // addressProcessPopup({int index}) {
  //   print('______________________________________________________________');
  //   print(index);
  //   return showDialog(
  //       context: context,
  //       barrierDismissible: false,
  //       builder: (BuildContext context) {
  //         return StatefulBuilder(builder: (context, setStater) {
  //           return AlertDialog(
  //             backgroundColor: Pallet.fontcolor,
  //             shape: RoundedRectangleBorder(
  //                 borderRadius: BorderRadius.circular(Pallet.radius)),
  //             title: Row(
  //               mainAxisAlignment: MainAxisAlignment.start,
  //               children: [
  //                 Image.asset("c-logo.png",
  //                     width: 30, color: Pallet.fontcolornew),
  //                 SizedBox(width: 10),
  //                 Expanded(
  //                   child: MyText(text:
  //                       isAdd == 'true' ? 'Add Address' : 'Edit Address',
  //                       style: TextStyle(
  //                           fontSize: Pallet.heading2,
  //                           color: Pallet.fontcolornew,
  //                           fontWeight: Pallet.font500)),
  //                 ),
  //               ],
  //             ),
  //             content: ConstrainedBox(
  //               constraints:
  //                   BoxConstraints(maxWidth: isMobile == 'true' ? 450 : 650),
  //               child: Container(
  //                 // width: 400,
  //                 // height: 400,
  //                 child: SingleChildScrollView(
  //                   child: Wrap(
  //                     // mainAxisAlignment: MainAxisAlignment.spaceEvenly,

  //                     spacing: Pallet.defaultPadding / 2,
  //                     runSpacing: Pallet.defaultPadding,
  //                     children: [
  //                       Container(
  //                         width: isMobile == 'true' ? 200 : 300,
  //                         child: Form(
  //                           key: addressform,
  //                           child: Column(
  //                             crossAxisAlignment: CrossAxisAlignment.start,
  //                             children: [
  //                               MyText(text:'Name',
  //                                   style: TextStyle(
  //                                       fontSize: Pallet.normalfont,
  //                                       color: Pallet.fontcolornew,
  //                                       fontWeight: Pallet.font500)),
  //                               SizedBox(height: 5),
  //                               SignupTextBox(
  //                                   tabpress: (event) {
  //                                     if (event.isKeyPressed(
  //                                         LogicalKeyboardKey.tab)) {
  //                                       FocusScope.of(context)
  //                                           .requestFocus(addressline1focus);
  //                                     }
  //                                   },
  //                                   focusnode: addressnamefocus,
  //                                   onsubmit: () {
  //                                     FocusScope.of(context)
  //                                         .requestFocus(addressline1focus);
  //                                   },
  //                                   errorText: nameError,
  //                                   isPassword: false,
  //                                   digitsOnlyPhone: false,

  //                                   // header: 'llllllllll',
  //                                   // label: ' Addressname',
  //                                   controller: addressname,
  //                                   validation: (value) {
  //                                     setStater(() {
  //                                       if (value.isEmpty) {
  //                                         nameError = 'Required';
  //                                       } else {
  //                                         nameError = null;
  //                                       }
  //                                     });
  //                                   }),
  //                             ],
  //                           ),
  //                         ),
  //                       ),
  //                       SizedBox(height: 10),
  //                       Container(
  //                         width: isMobile == 'true' ? 200 : 300,
  //                         child: Column(
  //                           crossAxisAlignment: CrossAxisAlignment.start,
  //                           children: [
  //                             MyText(text:'Street and Building No',
  //                                 style: TextStyle(
  //                                     fontSize: Pallet.normalfont,
  //                                     color: Pallet.fontcolornew,
  //                                     fontWeight: Pallet.font500)),
  //                             SizedBox(height: 5),
  //                             SignupTextBox(
  //                                 tabpress: (event) {
  //                                   if (event
  //                                       .isKeyPressed(LogicalKeyboardKey.tab)) {
  //                                     FocusScope.of(context)
  //                                         .requestFocus(addressline2focus);
  //                                   }
  //                                 },
  //                                 focusnode: addressline1focus,
  //                                 onsubmit: () {
  //                                   FocusScope.of(context)
  //                                       .requestFocus(addressline2focus);
  //                                 },
  //                                 errorText: addressline1Error,
  //                                 isPassword: false,
  //                                 digitsOnlyPhone: false,

  //                                 // header: 'llllllllll',
  //                                 // label: ' Addressline1',
  //                                 controller: addressline1,
  //                                 validation: (value) {
  //                                   setStater(() {
  //                                     if (value.isEmpty) {
  //                                       addressline1Error = 'Required';
  //                                     } else {
  //                                       addressline1Error = null;
  //                                     }
  //                                   });
  //                                 }),
  //                           ],
  //                         ),
  //                       ),
  //                       // SizedBox(height: 10),
  //                       // Container(
  //                       //   width: isMobile == 'true' ? 200 : 300,
  //                       //   child: Column(
  //                       //     crossAxisAlignment: CrossAxisAlignment.start,
  //                       //     children: [
  //                       //       MyText(text:'Address Line2',
  //                       //           style: TextStyle(
  //                       //               fontSize: Pallet.normalfont,
  //                       //               color: Pallet.fontcolornew,
  //                       //               fontWeight: Pallet.font500)),
  //                       //       SizedBox(height: 5),
  //                       //       SignupTextBox(
  //                       //           tabpress: (event) {
  //                       //             if (event
  //                       //                 .isKeyPressed(LogicalKeyboardKey.tab)) {
  //                       //               FocusScope.of(context)
  //                       //                   .requestFocus(phonenumberfocus);
  //                       //             }
  //                       //           },
  //                       //           focusnode: addressline2focus,
  //                       //           onsubmit: () {
  //                       //             FocusScope.of(context)
  //                       //                 .requestFocus(phonenumberfocus);
  //                       //           },
  //                       //           errorText: addressline2Error,
  //                       //           isPassword: false,
  //                       //           digitsOnlyPhone: false,

  //                       //           // header: 'llllllllll',
  //                       //           // label: ' addressline2',
  //                       //           controller: addressline2,
  //                       //           validation: (value) {
  //                       //             setStater(() {
  //                       //               if (value.isEmpty) {
  //                       //                 addressline2Error = 'Required';
  //                       //               } else {
  //                       //                 addressline2Error = null;
  //                       //               }
  //                       //             });
  //                       //           }),
  //                       //     ],
  //                       //   ),
  //                       // ),
  //                       SizedBox(height: 10),
  //                       Container(
  //                         width: isMobile == 'true' ? 200 : 300,
  //                         child: Column(
  //                           crossAxisAlignment: CrossAxisAlignment.start,
  //                           children: [
  //                             MyText(text:'Phone Number',
  //                                 style: TextStyle(
  //                                     fontSize: Pallet.normalfont,
  //                                     color: Pallet.fontcolornew,
  //                                     fontWeight: Pallet.font500)),
  //                             SizedBox(height: 5),
  //                             SignupTextBox(
  //                                 tabpress: (event) {
  //                                   if (event
  //                                       .isKeyPressed(LogicalKeyboardKey.tab)) {
  //                                     FocusScope.of(context)
  //                                         .requestFocus(countryfocus);
  //                                   }
  //                                 },
  //                                 focusnode: phonenumberfocus,
  //                                 onsubmit: () {
  //                                   FocusScope.of(context)
  //                                       .requestFocus(countryfocus);
  //                                 },
  //                                 errorText: phonenumberError,
  //                                 isPassword: false,
  //                                 digitsOnlyPhone: true,
  //                                 // header: 'llllllllll',
  //                                 // label: 'addressLine12',
  //                                 controller: phonenumber,
  //                                 validation: (value) {
  //                                   setStater(() {
  //                                     if (value.isEmpty) {
  //                                       phonenumberError = 'Required';
  //                                     } else {
  //                                       phonenumberError = null;
  //                                     }
  //                                   });
  //                                 }),
  //                           ],
  //                         ),
  //                       ),
  //                       SizedBox(height: 10),
  //                       Container(
  //                         width: isMobile == 'true' ? 200 : 300,
  //                         child: Column(
  //                           crossAxisAlignment: CrossAxisAlignment.start,
  //                           children: [
  //                             MyText(text:'Country',
  //                                 style: TextStyle(
  //                                     fontSize: Pallet.normalfont,
  //                                     color: Pallet.fontcolornew,
  //                                     fontWeight: Pallet.font500)),
  //                             SizedBox(height: 5),
  //                             // DropDown(
  //                             //     label: 'Select Country',
  //                             //     labeltext: 'Select Country',
  //                             //     items: cart.countries,
  //                             //     value: dropDownCountry,
  //                             //     // controller: country,
  //                             //     onChange: (newValue) {
  //                             //       setState(() {
  //                             //         dropDownCountry = newValue;
  //                             //         getRegionsMap['region_level'] = 4;
  //                             //         getRegionsMap['parent_region'] =
  //                             //             dropDownCountry.toUpperCase();

  //                             //         cart.getCities(getRegionsMap);
  //                             //       });
  //                             //     },
  //                             //     errortext: countryError),

  //                             DropDownField(
  //                                 // hintText: 'Please choose country',
  //                                 required: isRequired,
  //                                 textStyle:
  //                                     TextStyle(color: Pallet.fontcolornew),
  //                                 value: dropDownCountry,
  //                                 onValueChanged: (newValue) {
  //                                   setStater(() {
  //                                     cart.cities = [];
  //                                     cart.zipCode = [];
  //                                     district.text = '';
  //                                     postalcode.text = '';
  //                                     enablecity = true;
  //                                     dropDownCountry = newValue;
  //                                     getRegionsMap['region_level'] = 4;
  //                                     getRegionsMap['parent_region'] =
  //                                         dropDownCountry.toUpperCase();

  //                                     cart.getCities(getRegionsMap);
  //                                   });
  //                                 },
  //                                 items: cart.countries),
  //                             // SignupTextBox(
  //                             //     tabpress: (event) {2S22w2s21 2q2 `w2wc.
  //                             //             .requestFocus(statefocus);
  //                             //       }
  //                             //     },
  //                             //     focusnode: countryfocus,
  //                             //     onsubmit: () {
  //                             //       FocusScope.of(context)
  //                             //           .requestFocus(statefocus);
  //                             //     },
  //                             //     errorText: countryError,
  //                             //     isPassword: false,
  //                             //     digitsOnlyPhone: false,
  //                             //     // header: 'llllllllll',1dw`2Q
  //                             //     // label: 'addressLine12',
  //                             //     controller: country,
  //                             //     validation: (value) {
  //                             //       setStater(() {
  //                             //         if (value.isEmpty) {
  //                             //           countryError = 'Required';
  //                             //         } else {
  //                             //           countryError = null;
  //                             //         }12
  //                             //       });
  //                             //     }),
  //                           ],
  //                         ),
  //                       ),
  //                       // SizedBox(height: 10),
  //                       // Container(
  //                       //   width: isMobile == 'true' ? 200 : 300,
  //                       //   child: Column(
  //                       //     crossAxisAlignment: CrossAxisAlignment.start,
  //                       //     children: [
  //                       //       MyText(text:'State',
  //                       //           style: TextStyle(
  //                       //               fontSize: Pallet.normalfont,
  //                       //               color: Pallet.fontcolornew,
  //                       //               fontWeight: Pallet.font500)),
  //                       //       SizedBox(height: 5),
  //                       //       SignupTextBox(
  //                       //           tabpress: (event) {
  //                       //             if (event
  //                       //                 .isKeyPressed(LogicalKeyboardKey.tab)) {
  //                       //               FocusScope.of(context)
  //                       //                   .requestFocus(districtfocus);
  //                       //             }
  //                       //           },
  //                       //           focusnode: statefocus,
  //                       //           onsubmit: () {
  //                       //             FocusScope.of(context)
  //                       //                 .requestFocus(districtfocus);
  //                       //           },
  //                       //           errorText: stateError,
  //                       //           isPassword: false,
  //                       //           digitsOnlyPhone: false,
  //                       //           // header: 'llllllllll',
  //                       //           // label: 'addressLine12',
  //                       //           controller: state,
  //                       //           validation: (value) {
  //                       //             setStater(() {
  //                       //               if (value.isEmpty) {
  //                       //                 stateError = 'Required';
  //                       //               } else {
  //                       //                 stateError = null;
  //                       //               }
  //                       //             });
  //                       //           }),
  //                       //     ],
  //                       //   ),
  //                       // ),
  //                       SizedBox(height: 10),
  //                       Container(
  //                         width: isMobile == 'true' ? 200 : 300,
  //                         child: Column(
  //                           crossAxisAlignment: CrossAxisAlignment.start,
  //                           children: [
  //                             MyText(text:'City',
  //                                 style: TextStyle(
  //                                     fontSize: Pallet.normalfont,
  //                                     color: Pallet.fontcolornew,
  //                                     fontWeight: Pallet.font500)),
  //                             SizedBox(height: 5),

  //                             AbsorbPointer(
  //                               absorbing: !enablecity ? true : false,
  //                               child: TypeAheadFormField(
  //                                 errortext: districtError,
  //                                 textFieldConfiguration:
  //                                     TextFieldConfiguration(
  //                                   cursorColor: Pallet.fontcolornew,
  //                                   style: TextStyle(
  //                                       color: Pallet.fontcolornew,
  //                                       fontSize: Pallet.heading3,
  //                                       fontWeight: Pallet.font500),
  //                                   controller: district,
  //                                 ),
  //                                 suggestionsCallback: (pattern) {
  //                                   return cart.cities;
  //                                 },
  //                                 itemBuilder: (context, suggestion) {
  //                                   return ListTile(
  //                                     title: MyText(text:
  //                                       suggestion,
  //                                       style: TextStyle(
  //                                           color: Pallet.fontcolornew),
  //                                     ),
  //                                   );
  //                                 },
  //                                 transitionBuilder:
  //                                     (context, suggestionsBox, controller) {
  //                                   return suggestionsBox;
  //                                 },
  //                                 onSuggestionSelected: (suggestion) {
  //                                   setStater(() {
  //                                     print('lllllllllllllllllllllllllllll');
  //                                     cart.zipCode.clear();
  //                                     this.district.text = suggestion;
  //                                     districtError = null;
  //                                     postalcode.text = '';
  //                                     enablezipcode = true;
  //                                     // postalcodeError = "Required";
  //                                     getRegionsMap['region_level'] = 5;
  //                                     getRegionsMap['parent_region'] =
  //                                         district.text;
  //                                     // isRequired = false;

  //                                     // print(isRequired);
  //                                     print(
  //                                         'VVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVV');

  //                                     cart.getZipCode(getRegionsMap);
  //                                   });
  //                                 },
  //                                 // ignore: missing_return
  //                                 onchanged: (value) {
  //                                   if (value.isEmpty) {
  //                                     setStater(() {
  //                                       districtError = 'Required';
  //                                     });
  //                                   } else {
  //                                     setStater(() {
  //                                       enablezipcode = true;
  //                                       districtError = null;
  //                                     });
  //                                   }
  //                                 },
  //                                 onSaved: (value) {
  //                                   setStater(() {
  //                                     if (value.isEmpty) {
  //                                       districtError = 'Required';
  //                                     } else {
  //                                       districtError = null;
  //                                     }
  //                                   });
  //                                 },
  //                                 // ignore: missing_return
  //                                 validator: (value) {
  //                                   if (value.isEmpty) {
  //                                     setStater(() {
  //                                       districtError = 'Required';
  //                                     });
  //                                   } else {
  //                                     setStater(() {
  //                                       districtError = null;
  //                                     });
  //                                   }
  //                                 },
  //                               ),
  //                             ),

  //                             // this.district.text == ''
  //                             //     ? Padding(
  //                             //         padding:
  //                             //             EdgeInsets.all(Pallet.defaultPadding),
  //                             //         child: Container(
  //                             //             child: isRequired == true
  //                             //                 ? MyText(text:
  //                             //                     'Required',
  //                             //                     style: TextStyle(
  //                             //                         color: Pallet.failed,
  //                             //                         fontSize:
  //                             //                             Pallet.normalfont),
  //                             //                   )
  //                             //                 : MyText(text:
  //                             //                     '',
  //                             //                   )),
  //                             //       )
  //                             //     : Container()
  //                             // InkWell(
  //                             //   onTap: () {
  //                             //     print(district.text);
  //                             //   },
  //                             //   child: Container(
  //                             //     child: districtError != null
  //                             //         ? MyText(text:'Required')
  //                             //         : MyText(text:''),
  //                             //   ),
  //                             // )
  //                             // DropDown(
  //                             //     label: 'Select City',
  //                             //     labeltext: 'Select City',
  //                             //     items: cart.cities,
  //                             //     value: dropDownCity,
  //                             //     controller: district,
  //                             //     onChange: (value) {
  //                             // setState(() {
  //                             //   dropDownCity = value;

  //                             //   getRegionsMap['region_level'] = 5;
  //                             //   getRegionsMap['parent_region'] =
  //                             //       dropDownCity;

  //                             //   cart.getZipCode(getRegionsMap);
  //                             // });
  //                             //     },
  //                             //     errortext: districtError),
  //                             // DropDownField(
  //                             //     required: true,
  //                             //     hintText: 'Please choose city',
  //                             //     value: dropDownCity,
  //                             //     onValueChanged: (value) {
  //                             //       setState(() {
  //                             //         dropDownCity = value;

  //                             //         getRegionsMap['region_level'] = 5;
  //                             //         getRegionsMap['parent_region'] =
  //                             //             dropDownCity;

  //                             //         cart.getZipCode(getRegionsMap);
  //                             //       });
  //                             //     },
  //                             //     items: cart.cities),
  //                             // SignupTextBox(
  //                             //     tabpress: (event) {
  //                             //       if (event
  //                             //           .isKeyPressed(LogicalKeyboardKey.tab)) {
  //                             //         FocusScope.of(context)
  //                             //             .requestFocus(postalcodefocus);
  //                             //       }
  //                             //     },
  //                             //     focusnode: districtfocus,
  //                             //     onsubmit: () {
  //                             //       FocusScope.of(context)
  //                             //           .requestFocus(postalcodefocus);
  //                             //     },
  //                             //     errorText: districtError,
  //                             //     isPassword: false,
  //                             //     digitsOnlyPhone: false,
  //                             //     // header: 'llllllllll',
  //                             //     // label: 'addressLine12',
  //                             //     controller: district,
  //                             //     validation: (value) {
  //                             //       setStater(() {
  //                             //         if (value.isEmpty) {
  //                             //           districtError = 'Required';
  //                             //         } else {
  //                             //           districtError = null;
  //                             //         }
  //                             //       });
  //                             //     }),
  //                           ],
  //                         ),
  //                       ),
  //                       SizedBox(height: 10),
  //                       Container(
  //                         width: isMobile == 'true' ? 200 : 300,
  //                         child: Column(
  //                           crossAxisAlignment: CrossAxisAlignment.start,
  //                           children: [
  //                             MyText(text:'Zip Code',
  //                                 style: TextStyle(
  //                                     fontSize: Pallet.normalfont,
  //                                     color: Pallet.fontcolornew,
  //                                     fontWeight: Pallet.font500)),
  //                             SizedBox(height: 5),
  //                             Container(
  //                               // decoration: BoxDecoration(
  //                               //     border:
  //                               //         Border.all(color: Pallet.fontcolornew)),
  //                               child: AbsorbPointer(
  //                                 absorbing: !enablezipcode ? true : false,
  //                                 child: TypeAheadFormField(
  //                                   // ignore: missing_return
  //                                   validator: (val) {
  //                                     if (val.isEmpty) {}
  //                                   },
  //                                   errortext: postalcodeError,
  //                                   textFieldConfiguration:
  //                                       TextFieldConfiguration(
  //                                           cursorColor: Pallet.fontcolornew,
  //                                           style: TextStyle(
  //                                               color: Pallet.fontcolornew,
  //                                               fontSize: Pallet.heading3,
  //                                               fontWeight: Pallet.font500),
  //                                           controller: postalcode,
  //                                           decoration: InputDecoration(
  //                                             // labelText: 'City',
  //                                             border: OutlineInputBorder(
  //                                                 borderSide: const BorderSide(
  //                                                     color: Color(0XFF1034a6),
  //                                                     width: 1.0),
  //                                                 borderRadius:
  //                                                     BorderRadius.circular(
  //                                                         8.0)),
  //                                             errorBorder: OutlineInputBorder(
  //                                                 borderSide: BorderSide(
  //                                                     color: Colors.red,
  //                                                     width: 2),
  //                                                 borderRadius:
  //                                                     BorderRadius.circular(
  //                                                         Pallet.radius)),
  //                                             focusedErrorBorder:
  //                                                 OutlineInputBorder(
  //                                                     borderSide: BorderSide(
  //                                                         color: Colors.red,
  //                                                         width: 2),
  //                                                     borderRadius:
  //                                                         BorderRadius.circular(
  //                                                             Pallet.radius)),
  //                                             focusedBorder: OutlineInputBorder(
  //                                                 borderSide: const BorderSide(
  //                                                     color: Color(0XFF1034a6),
  //                                                     width: 1.0),
  //                                                 borderRadius:
  //                                                     BorderRadius.circular(
  //                                                         8.0)),
  //                                             enabledBorder: OutlineInputBorder(
  //                                                 borderSide: BorderSide(
  //                                                     color: Color(0xFF4570ae),
  //                                                     width: 0.0),
  //                                                 borderRadius:
  //                                                     BorderRadius.circular(
  //                                                         8.0)),
  //                                           )),
  //                                   suggestionsCallback: (pattern) {
  //                                     return cart.zipCode;
  //                                   },

  //                                   itemBuilder: (context, suggestion) {
  //                                     return ListTile(
  //                                       title: MyText(text:
  //                                         suggestion,
  //                                         style: TextStyle(
  //                                             color: Pallet.fontcolornew),
  //                                       ),
  //                                     );
  //                                   },
  //                                   transitionBuilder:
  //                                       (context, suggestionsBox, controller) {
  //                                     return suggestionsBox;
  //                                   },
  //                                   onSuggestionSelected: (suggestion) {
  //                                     setStater(() {
  //                                       // isRequired = false;
  //                                       postalcodeError = null;

  //                                       this.postalcode.text = suggestion;
  //                                     });
  //                                   },

  //                                   // autovalidateMode: AutovalidateMode.always,
  //                                   // hideOnError: true,
  //                                   // noItemsFoundBuilder: (BuildContext context) {
  //                                   //   return Container(
  //                                   //       color: Pallet.fontcolor,
  //                                   //       child: MyText(text:
  //                                   //         'Required',
  //                                   //         style: TextStyle(
  //                                   //             color: Pallet.failed,
  //                                   //             fontSize: Pallet.normalfont),
  //                                   //       ));
  //                                   // },
  //                                 ),
  //                               ),
  //                             ),
  //                             // this.postalcode.text == ''
  //                             //     ? Padding(
  //                             //         padding:
  //                             //             EdgeInsets.all(Pallet.defaultPadding),
  //                             //         child: Container(
  //                             //             child: isRequired == true
  //                             //                 ? MyText(text:
  //                             //                     'Required',
  //                             //                     style: TextStyle(
  //                             //                         color: Pallet.failed,
  //                             //                         fontSize:
  //                             //                             Pallet.normalfont),
  //                             //                   )
  //                             //                 : MyText(text:
  //                             //                     '',
  //                             //                   )),
  //                             //       )
  //                             //     : Container()
  //                             // DropDown(
  //                             //     label: 'Select ZipCode',
  //                             //     labeltext: 'Select ZipCode',
  //                             //     items: cart.zipCode,
  //                             //     value: dropDownZip,
  //                             //     controller: postalcode,
  //                             //     onChange: (value) {
  //                             //       setState(() {
  //                             //         dropDownZip = value;
  //                             //       });
  //                             //     },
  //                             //     errortext: postalcodeError),

  //                             // DropDownField(
  //                             //     hintText: 'Please choose ZipCode',
  //                             //     value: dropDownZip,
  //                             //     required: true,
  //                             //     onValueChanged: (value) {
  //                             //       setState(() {
  //                             //         dropDownZip = value;

  //                             //         // getRegionsMap['region_level'] = 5;
  //                             //         // getRegionsMap['parent_region'] =
  //                             //         //     dropDownCity;

  //                             //         // cart.getRegions(getRegionsMap);
  //                             //       });
  //                             //     },
  //                             //     items: cart.zipCode),

  //                             // SignupTextBox(
  //                             //     tabpress: (event) {
  //                             //       if (event
  //                             //           .isKeyPressed(LogicalKeyboardKey.tab)) {
  //                             //         FocusScope.of(context)
  //                             //             .requestFocus(continuenode);
  //                             //       }
  //                             //     },
  //                             //     focusnode: postalcodefocus,
  //                             //     onsubmit: () {
  //                             //       FocusScope.of(context)
  //                             //           .requestFocus(continuenode);
  //                             //     },
  //                             //     errorText: postalcodeError,
  //                             //     isPassword: false,
  //                             //     digitsOnlyPhone: true,
  //                             //     // header: 'llllllllll',
  //                             //     // label: 'addressLine12',
  //                             //     controller: postalcode,
  //                             //     validation: (value) {
  //                             //       setStater(() {
  //                             //         if (value.isEmpty) {
  //                             //           postalcodeError = 'Required';
  //                             //         } else {
  //                             //           postalcodeError = null;
  //                             //         }
  //                             //       });
  //                             //     }),
  //                             //
  //                           ],
  //                         ),
  //                       ),
  //                     ],
  //                   ),
  //                 ),
  //               ),
  //             ),
  //             actions: [
  //               PopupButton(
  //                   textcolor: Pallet.fontcolor,
  //                   buttoncolor: Pallet.fontcolornew,
  //                   text: 'Close',
  //                   onpress: () {
  //                     nameError = null;
  //                     addressline1Error = null;
  //                     addressline2Error = null;
  //                     phonenumberError = null;
  //                     countryError = null;
  //                     stateError = null;
  //                     districtError = null;

  //                     postalcodeError = null;

  //                     Navigator.of(context).pop();
  //                   }),
  //               PopupButton(
  //                   textcolor: Pallet.fontcolor,
  //                   buttoncolor: Pallet.fontcolornew,
  //                   text: isAdd == 'true' ? 'Add' : 'Update',
  //                   onpress: () {
  //                     setState(() {
  //                       isRequired = true;
  //                     });
  //                     if (addressline1.text.isEmpty ||
  //                         // addressline2.text.isEmpty ||
  //                         addressname.text.isEmpty ||
  //                         phonenumber.text.isEmpty ||
  //                         // state.text.isEmpty ||
  //                         dropDownCountry.isEmpty ||
  //                         district.text.isEmpty ||
  //                         postalcode.text.isEmpty) {
  //                       if (district.text.length < 1) {
  //                         setStater(() {
  //                           districtError = 'Required';
  //                         });
  //                       }
  //                       if (addressline1.text.length < 1) {
  //                         setStater(() {
  //                           addressline1Error = 'Required';
  //                         });
  //                       }
  //                       // if (addressline2.text.length < 1) {
  //                       //   setStater(() {
  //                       //     addressline2Error = 'Required';
  //                       //   });
  //                       // }
  //                       if (addressname.text.length < 1) {
  //                         setStater(() {
  //                           nameError = 'Required';
  //                         });
  //                       }
  //                       if (phonenumber.text.length < 1) {
  //                         setStater(() {
  //                           phonenumberError = 'Required';
  //                         });
  //                       }
  //                       // if (state.text.length < 1) {
  //                       //   setState(() {
  //                       //     stateError = 'Required';
  //                       //   });
  //                       // }
  //                       if (postalcode.text.length < 1) {
  //                         setStater(() {
  //                           postalcodeError = 'Required';
  //                         });
  //                       }
  //                       if (dropDownCountry.length < 1) {
  //                         setStater(() {
  //                           countryError = 'Required';
  //                         });
  //                       }
  //                     } else {
  //                       setStater(() {
  //                         // cart.countries = [];
  //                         cart.cities = [];
  //                         cart.zipCode = [];
  //                         enablecity = false;
  //                         enablezipcode = false;
  //                         if (isAdd == 'true') {
  //                           // if (addressform.currentState.validate()) {
  //                           Map<String, dynamic> _map = {
  //                             "product_id": 1,
  //                             "address_id": 0,
  //                             "address_name": addressname.text,
  //                             "address_line1": addressline1.text,
  //                             "address_line2": 'null',
  //                             "country": dropDownCountry,
  //                             "region": 'null',
  //                             "city": district.text,
  //                             "postal_code": postalcode.text,
  //                             "address_phone": phonenumber.text,
  //                             "delete": false
  //                           };
  //                           cart.addAddress(_map, widget.setData);

  //                           // cart.getAddressInfo(setState);

  //                           Navigator.of(context).pop();
  //                           // print(_map);
  //                           // print(cart.address);
  //                           // }
  //                         } else {
  //                           // if (addressform.currentState.validate()) {
  //                           print('========================');
  //                           Map<String, dynamic> _map = {
  //                             "product_id": 1,
  //                             "address_id": cart.address[index]['address_id'],
  //                             "address_name": addressname.text,
  //                             "address_line1": addressline1.text,
  //                             "address_line2": 'null',
  //                             "country": dropDownCountry,
  //                             "region": 'null',
  //                             "city": district.text,
  //                             "postal_code": postalcode.text,
  //                             "address_phone": phonenumber.text,
  //                             "delete": false
  //                           };
  //                           print('.........................');
  //                           cart.updateAddress(
  //                               _map, cart.address[index], setState);
  //                           Navigator.of(context).pop();
  //                           print('________________________');
  //                           // }
  //                         }
  //                       });
  //                     }
  //                   }),
  //             ],
  //           );
  //         });
  //       });
  // }

  void editaddres(index) {
    widget.setData(() {
      addressname.text = cart.address[index]['address_name'];
      addressline1.text = cart.address[index]['address_line1'];
      addressline2.text = cart.address[index]['address_line2'];
      phonenumber.text = cart.address[index]['address_phone'];
      dropDownCountry = cart.address[index]['country'];
      state.text = cart.address[index]['region'];
      district.text = cart.address[index]['city'];
      postalcode.text = cart.address[index]['postal_code'];
    });
  }

  emptyCartPopup() {
    return showDialog(
        barrierDismissible: true,
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            backgroundColor: Pallet.fontcolor,
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(Pallet.radius)),
            title: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Image.asset(
                  "c-logo.png",
                  width: 30,
                  color: Pallet.fontcolornew,
                ),
                SizedBox(width: 10),
                Expanded(
                  child: MyText(
                      text: pageDetails['text3'],
                      style: TextStyle(
                          fontSize: Pallet.normalfont + 10,
                          color: Pallet.fontcolornew,
                          fontWeight: Pallet.font500)),
                ),
              ],
            ),
            content: Image.asset(
              'cartempty.png',
              width: 150,
              height: 150,
            ),
            // MyText(text:'Your Cart Is Empty!\n',
            //     style: TextStyle(
            //         fontSize: Pallet.normalfont,
            //         color: Pallet.fontcolor,
            //         fontWeight: Pallet.font500)),
            actions: [
              PopupButton(
                buttoncolor: Pallet.fontcolornew,
                text: pageDetails['text4'],
                textcolor: Pallet.fontcolor,
                onpress: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => Home(route: 'centurion_shop')));
                },
              )
            ],
          );
        });
  }

  Widget paymentContainer({double wdgtWidth, wdgtHeight}) {
    return Container(
        width: wdgtWidth,
        height: wdgtHeight,
        child: Column(mainAxisAlignment: MainAxisAlignment.end, children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              MyText(
                  text: pageDetails['text5'] +
                      ' ' +
                      (cart.totalCount.toString()) +
                      ' ' +
                      pageDetails['text6'],
                  style: TextStyle(color: Pallet.fontcolornew)),
              MyText(
                text: '\$ ' + cart.total.toString(),
                style:
                    TextStyle(color: Colors.red, fontWeight: FontWeight.bold),
              )
            ],
          ),
          SizedBox(height: 10),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  MyText(
                      text: pageDetails['text7'],
                      style: TextStyle(color: Pallet.fontcolornew)),
                  MyText(
                      text: pageDetails['text8'],
                      style:
                          TextStyle(color: Pallet.fontcolornew, fontSize: 10)),
                ],
              ),
              MyText(
                  text: '\$ ' + cart.shippingCosts.toString(),
                  style:
                      TextStyle(color: Colors.red, fontWeight: FontWeight.bold))
            ],
          ),
          SizedBox(height: 10),
          // Row(
          //   mainAxisAlignment: MainAxisAlignment.spaceBetween,
          //   children: [
          //     MyText(text:
          //       'Est Tax',
          //       style: TextStyle(color: Pallet.dashcontainerback),
          //     ),
          //     MyText(text:'\$10.00',
          //         style:
          //             TextStyle(color: Colors.red, fontWeight: FontWeight.bold))
          //   ],
          // ),
          Divider(
            height: 30,
            thickness: 1,
            color: Colors.black,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              MyText(
                text: pageDetails['text9'],
                style: TextStyle(color: Pallet.fontcolornew),
              ),
              MyText(
                  text: '\$ ' + (cart.total + cart.shippingCosts).toString(),
                  style: TextStyle(
                      color: Colors.red, fontWeight: FontWeight.bold)),
            ],
          ),
          SizedBox(height: 20),
          CustomButton(
              width: 250,
              text: pageDetails["text10"],
              vpadding: 10,
              buttoncolor: Pallet.fontcolornew,
              textcolor: Pallet.fontcolor,
              textsize: Pallet.heading6,
              onpress: () async {
               if  (cart.userActiveStatus == false)
                    {          snack.snack(
                                  title:
                                      'Admin has blocked your transfer permissions');}
                                      else if(cart.negativeAmount=='true'){
snack.snack(
              title:
                  "Blocked because of Negative balance in any of your account");
                                      }
                              
               else {var result = restrictUnavailableProducts();
                // cart.add(data, setState, widget.setData, context);
                isRequired = false;
                isAdd = 'true';
                if (result == true) {
                  if (cart.cart.isEmpty) {
                    emptyCartPopup();
                  } else if (cart.address.isEmpty) {
                    // addaddres();
                    addaddres();
                    showDialog(
                        barrierDismissible: false,
                        context: context,
                        builder: (BuildContext context) {
                          return AddressProcessPopUp(
                            isAdd: isAdd,
                            // index: 0,
                            setData: setState,
                          );
                        });
                    // showDialog(
                    //     context: context,
                    //     builder: (BuildContext context) {
                    //       return AddressPopUp(
                    //         isMobile: isMobile,
                    //         setData: setState,
                    //       );
                    //     });
                  } else {
// if(){

// }

                    await cart.redeemDSVP(context, setState, widget.setData);

                    // await cart.myCart(setState, widget.setData);
                    // await cart.proceedcheckout(context);

                    // await cart.buyProducts(context);
                  }
                } else {
                  snack.snack(title: pageDetails['text11']);
                  // ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                  //   content: MyText(text:pageDetails['text11']),
                  // ));
                }}
              })
        ]));
  }

  void addaddres() {
    setState(() {
      // cart.address.add({
      dropDownCountry = null;

      addressname.text = '' == null ? '' : '';
      addressline1.text = '' == null ? '' : '';
      addressline2.text = '' == null ? '' : '';
      phonenumber.text = '' == null ? '' : '';
      country.text = '' == null ? '' : '';
      state.text = '' == null ? '' : '';
      district.text = '' == null ? '' : '';
      postalcode.text = '' == null ? '' : '';
      // });
    });
  }

  Widget cartList({double wdgtWidth, wdgtHeight, proName}) {
    return Container(
        width: wdgtWidth,
        height: wdgtHeight,
        padding: EdgeInsets.all(Pallet.defaultPadding),
        child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              SingleChildScrollView(
                child: Container(
                    width: wdgtWidth,
                    height: wdgtHeight / 1.1,
                    child: ListView.builder(
                      itemCount: cart.cart.length,
                      itemBuilder: (context, index) {
                        return Padding(
                          padding: EdgeInsets.all(Pallet.defaultPadding),
                          child: Container(
                            decoration: BoxDecoration(
                              color: Pallet.inner2,
                              borderRadius: BorderRadius.circular(10),
                            ),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceEvenly,
                                    children: [
                                      Expanded(
                                        child: Row(
                                          children: [
                                            InkWell(
                                              onTap: () {
                                                shopRoute = 'details';
                                                productId = cart.cart[index]
                                                    ["product_id"];
                                                storeId = cart.cart[index]
                                                    ["store_id"];
                                                cart.categoryName = cart
                                                    .cart[index]["store_name"];

                                                Navigator.push(
                                                  context,
                                                  MaterialPageRoute(
                                                      builder: (context) =>
                                                          Home(
                                                            route:
                                                                'centurion_shop',
                                                            param: 'true',
                                                          )),
                                                );
                                              },
                                              child: Padding(
                                                padding:
                                                    const EdgeInsets.all(8.0),
                                                child: ClipRRect(
                                                  borderRadius:
                                                      BorderRadius.circular(10),
                                                  child: Container(
                                                      width: 80,
                                                      height: 80,
                                                      child: Image.network(
                                                          appSettings[
                                                                  'SERVER_URL'] +
                                                              '/' +
                                                              cart.cart[index][
                                                                  "image_url"])),
                                                ),
                                              ),
                                            ),
                                            Column(
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.start,
                                                mainAxisAlignment:
                                                    MainAxisAlignment
                                                        .spaceEvenly,
                                                children: [
                                                  Row(
                                                    mainAxisAlignment:
                                                        MainAxisAlignment
                                                            .spaceBetween,
                                                    children: [
                                                      InkWell(
                                                        onTap: () {
                                                          shopRoute = 'details';
                                                          productId = cart
                                                                  .cart[index]
                                                              ["product_id"];
                                                          storeId =
                                                              cart.cart[index]
                                                                  ["store_id"];
                                                          cart.categoryName =
                                                              cart.cart[index][
                                                                  "store_name"];
                                                          Navigator.push(
                                                            context,
                                                            MaterialPageRoute(
                                                                builder:
                                                                    (context) =>
                                                                        Home(
                                                                          route:
                                                                              'centurion_shop',
                                                                          param:
                                                                              'true',
                                                                        )),
                                                          );
                                                        },
                                                        child: Container(
                                                          width: proName,
                                                          child: MyText(
                                                            text: cart
                                                                    .cart[index]
                                                                [
                                                                "product_name"],
                                                            style: TextStyle(
                                                                color: Pallet
                                                                    .fontcolornew,
                                                                fontWeight:
                                                                    FontWeight
                                                                        .w400,
                                                                fontSize: 15),
                                                          ),
                                                        ),
                                                      ),
                                                    ],
                                                  ),
                                                  SizedBox(height: 5),
                                                  Wrap(
                                                    runSpacing: 5,
                                                    children: [
                                                      MyText(
                                                          text: "\$ " +
                                                              cart.cart[index][
                                                                      "new_price"]
                                                                  .toString() +
                                                              ' * ' +
                                                              cart.cart[index]
                                                                      ["qty"]
                                                                  .toString() +
                                                              ' = ' +
                                                              "\$ " +
                                                              (cart.cart[index]["new_price"] *
                                                                      cart.cart[index]
                                                                          [
                                                                          "qty"])
                                                                  .toString(),
                                                          style: TextStyle(
                                                              color: Pallet
                                                                  .fontcolornew,
                                                              fontWeight:
                                                                  FontWeight
                                                                      .bold)),
                                                    ],
                                                  ),
                                                  SizedBox(height: 5),
                                                  Column(
                                                    children: [
                                                      for (var value
                                                          in cart.cart[index]
                                                              ['variants'])

                                                        // for (var variantvalue
                                                        //     in newProducts
                                                        //         .varient_id_list)
                                                        //   if (cart.cart[index][
                                                        //           'variant_id'] ==
                                                        //       variantvalue[
                                                        //           'variantId'])
                                                        //     variant_value_final =
                                                        //         variantvalue[
                                                        //             'variantValue'],
                                                        Container(
                                                          padding: EdgeInsets
                                                              .symmetric(
                                                                  horizontal:
                                                                      Pallet.defaultPadding /
                                                                          2),
                                                          child: MyText(
                                                              text: value[
                                                                      'value']
                                                                  .toString()),
                                                          decoration: BoxDecoration(
                                                              borderRadius:
                                                                  BorderRadius
                                                                      .circular(
                                                                          Pallet
                                                                              .radius),
                                                              border: Border.all(
                                                                  color: Pallet
                                                                      .fontcolornew)),
                                                        ),
                                                    ],
                                                  )
                                                ]),
                                          ],
                                        ),
                                      ),
                                      InkWell(
                                          onTap: () {
                                            widget.setData(() {
                                              setState(() {
                                                cart.dismiss(index, setState,
                                                    widget.setData);
                                              });
                                            });
                                          },
                                          child: Icon(
                                            Icons.delete,
                                            color: Pallet.fontcolornew,
                                          )),
                                    ]),
                                cart.address.length != 0
                                    ? cart.cart[index]['is_available'] == false
                                        ? MyText(
                                            text: pageDetails['text12'],
                                            style: TextStyle(
                                                color: Pallet.failed,
                                                fontSize: 15),
                                          )
                                        : MyText(text: '')
                                    : MyText(
                                        text: pageDetails['text12'],
                                        style: TextStyle(
                                            color: Pallet.failed, fontSize: 15),
                                      ),
                              ],
                            ),
                          ),
                        );
                      },
                    )),
              ),
            ]));
  }

  void deleteItem(index) {
    setState(() {
      cart.cart.removeAt(index);
    });
  }

  void undoDeletion(index, item) {
    setState(() {
      cart.cart.insert(index, item);
    });
  }

  restrictUnavailableProducts() {
    bool isAvalable = true;
    for (var temp in cart.cart) {
      if (temp['is_available'] == false) {
        isAvalable = false;
      }
    }
    return isAvalable;
  }

  Container stackBehindDismiss() {
    return Container(
      alignment: Alignment.centerRight,
      padding: EdgeInsets.only(right: 20.0),
      color: Colors.red,
      child: Icon(
        Icons.delete,
        color: Colors.white,
      ),
    );
  }
}
