import 'dart:async';

import 'package:centurion/config/app_settings.dart';
import 'package:centurion/lottery/config.dart';
import 'package:centurion/services/business/account.dart';
import 'package:centurion/utils/utils.dart';
import 'package:flutter/material.dart';
// import 'package:get/get_rx/src/rx_typedefs/rx_typedefs.dart';

import '../common.dart';
import '../config.dart';

bool isMobile = false;
int selectedIndex;
Map<String, dynamic> pageDetails = {};

class Invoices extends StatefulWidget {
  const Invoices({Key key, this.wdgtWidth, this.wdgtHeight, this.assetType})
      : super(key: key);
  final double wdgtWidth, wdgtHeight;
  final bool assetType;

  @override
  _InvoicesState createState() => _InvoicesState();
}

class _InvoicesState extends State<Invoices> {
  void setPageDetails() async {
    String data = await Utils.getPageDetails('invoices');
    setState(() {
      pageDetails = Utils.fromJSONString(data);
    });
  }

  // double sample = 0;
  void initState() {
    setPageDetails();

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(builder: (context, constraints) {
      if (constraints.maxWidth < ScreenSize.iphone) {
        isMobile = true;
        return SingleChildScrollView(
          child: Padding(
            padding: EdgeInsets.only(
                left: Pallet.defaultPadding,
                top: Pallet.topPadding1,
                right: Pallet.defaultPadding),
            child: Column(
              children: [
                invoicesPage(),
                // Container(child: Text(" running"))
              ],
            ),
          ),
        );
      } else if (constraints.maxWidth > ScreenSize.iphone &&
          constraints.maxWidth < ScreenSize.ipad) {
        isMobile = true;

        return SingleChildScrollView(
          child: Padding(
            padding: EdgeInsets.fromLTRB(30, 10, 30, 10),
            child: Column(
              children: [
                invoicesPage(),
                //Container(child: Text(" running"))
              ],
            ),
          ),
        );
      } else {
        isMobile = false;

        return Padding(
          padding: EdgeInsets.only(left: Pallet.leftPadding, top: 10),
          child: invoicesPage(),
        );
      }
    });
  }

  Container invoicesPage() {
    return Container(
        width: widget.wdgtWidth - 15,
        height: widget.wdgtHeight,
        child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
          Text(
            "History",
            style: TextStyle(fontSize: 30, color: Pallet.dashcontainerback),
          ),
          // Text(
          //   "Testing..",
          //   style: TextStyle(fontSize: 30, color: Pallet.dashcontainerback),
          // ),
          SizedBox(
            height: 20,
          ),
          Container(
            padding: EdgeInsets.all(Pallet.defaultPadding),
            decoration: BoxDecoration(
                //color: Pallet.fontcolor,
                boxShadow: [Pallet.shadowEffect],
                borderRadius: BorderRadius.circular(Pallet.radius)),
            height: widget.wdgtHeight - 150,
            width: widget.wdgtWidth - 50,
            child: TabPageState(
              setState,
            ),
          )
        ]));
  }
}

class TabPageState extends StatefulWidget {
  TabPageState(
    setState,
  );

  @override
  _TabPageState createState() => _TabPageState();
}

class _TabPageState extends State<TabPageState>
    with SingleTickerProviderStateMixin {
  TabController _controller;
  int _currentIndex = 0;
  Future<dynamic> temp;
  @override
  void initState() {
    super.initState();
    temp = invoices.statement(setState);
    _controller = TabController(vsync: this, length: 2);
    _controller.addListener(_handleTabSelection);
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          automaticallyImplyLeading: false,
          backgroundColor: Colors.white,
          flexibleSpace: TabBar(
            labelColor: Colors.white,
            indicator: BoxDecoration(
                borderRadius: BorderRadius.circular(Pallet.radius),
                color: Colors.lightBlue[700]),
            controller: _controller,
            tabs: <Widget>[
              Padding(
                padding: const EdgeInsets.all(10),
                child: Tab(
                  icon: Row(children: [
                    ImageIcon(
                      NetworkImage(
                          appSettings['SERVER_URL'] + '/' + 'assets/dsv.png'),
                      size: isMobile == true ? 25 : 40,
                      color: Pallet1.fontcolornew,
                    ),
                    SizedBox(width: 10),
                    Text(
                      "DSV",
                      style: TextStyle(
                          fontSize: isMobile == true ? 15 : 25,
                          color: Pallet1.fontcolornew),
                    ),
                  ]),
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(10),
                child: Tab(
                  icon: Row(children: [
                    ImageIcon(
                      NetworkImage(
                          appSettings['SERVER_URL'] + '/' + 'assets/dsv_p.png'),
                      size: isMobile == true ? 25 : 40,
                      color: Pallet1.fontcolornew,
                    ),
                    SizedBox(width: 10),
                    Text("DSV-P",
                        style: TextStyle(
                            fontSize: isMobile == true ? 15 : 25,
                            color: Pallet1.fontcolornew)),
                  ]),
                ),
              ),
            ],
          )),
      body: TabBarView(
        physics: NeverScrollableScrollPhysics(),
        controller: _controller,
        children: <Widget>[
          Container(
            child: Padding(
              padding: EdgeInsets.all(
                isMobile == true ? 0 : 20,
              ),
              child: purchasesDetils(),
            ),
          ),
          Container(
              child: Padding(
            padding: EdgeInsets.all(
              isMobile == true ? 0 : 20,
            ),
            child: purchasesDetils(),
          )),
        ],
      ),
    );
  }

  _handleTabSelection() async {
    setState(() {
      // ignore: unnecessary_statements
      temp = null;
      if (_currentIndex == 0) {
        // ignore: unnecessary_statements
        invoices.type = false;
        temp = invoices.statement(setState);
      } else {
        // ignore: unnecessary_statements
        invoices.type = true;
        temp = invoices.statement(setState);
      }
      print("invoices.type");
      print(invoices.type);

      _currentIndex = _controller.index;
      // temp = invoices.statement(setState);
    });
  }

  Widget purchasesDetils() {
    return StreamBuilder(
      stream: invoices.invoicestream,
      // ignore: missing_return
      builder: (context, snapshot) {
        if (snapshot.hasError) {
          return SomethingWentWrongMessage();
        } else if (snapshot.hasData) {
          print("pageDetails.length");
          print(snapshot.data.length);
          // if (snapshot.data.length != 0) {
          return Invoicehistory(snapshot.data);
          // } else {
          // return Expanded(
          //   child: Center(
          //     child: Text("No Purchases Found",
          //         style: TextStyle(
          //           color: Pallet1.fontcolornew,
          //           fontSize: Pallet1.normalfont,
          //         )),
          //   ),
          // );
          // ListView(children: [
          //   Container(
          //     color: Pallet1.fontcolornew,
          //     width: width.wdgtWidth,
          //     height: 50,
          //     // decoration: BoxDecoration(
          //     //     border: Border.all(color: Colors.black),
          //     //     borderRadius: BorderRadius.circular(5)),
          //     child: Padding(
          //       padding: const EdgeInsets.all(5),
          //       child: Row(
          //         children: [
          //           Expanded(
          //               child: Text(" Invoice Id ",
          //                   style:
          //                       TextStyle(color: Colors.white, fontSize: 20))),
          //           Expanded(
          //             child: Text(" Date & Time of Perchas ",
          //                 style: TextStyle(color: Colors.white, fontSize: 20)),
          //           ),
          //           Expanded(
          //             child: Text(" Grand Total ",
          //                 style: TextStyle(color: Colors.white, fontSize: 20)),
          //           ),
          //           Expanded(
          //             child: Text(" Bonus Credits ",
          //                 style: TextStyle(color: Colors.white, fontSize: 20)),
          //           ),
          //           Expanded(
          //             child: Text(" Payment Method ",
          //                 style: TextStyle(color: Colors.white, fontSize: 20)),
          //           ),
          //         ],
          //       ),
          //     ),
          //   ),
          //   snapshot.data.length == 0
          //       ? Expanded(
          //           child: Center(
          //             child: Text("No Purchases Found",
          //                 style: TextStyle(
          //                   color: Pallet1.fontcolornew,
          //                   fontSize: 30,
          //                 )),
          //           ),
          //         )
          //       : Container(),
          //   for (int i = 0; i < snapshot.data.length; i++)
          //     InkWell(
          //       onTap: () {
          //         setState(() {
          //           if (selectedIndex == i) {
          //             selectedIndex = null;
          //           } else {
          //             selectedIndex = i;
          //             print("package 5u 7jh56fujh");
          //             print(snapshot.data[i]['package_list'].length);
          //           }
          //         });
          //       },
          //       child: Padding(
          //         padding: EdgeInsets.all(Pallet.defaultPadding),
          //         child: Card(
          //             shadowColor: Colors.black,
          //             elevation: 5,
          //             child: Padding(
          //               padding: const EdgeInsets.all(10),
          //               child: Column(
          //                 mainAxisAlignment: MainAxisAlignment.spaceBetween,
          //                 children: [
          //                   Padding(
          //                     padding: const EdgeInsets.all(10),
          //                     child: Row(children: [
          //                       Expanded(
          //                         flex: 1,
          //                         child: Text(
          //                             snapshot.data[i]['invoice_id'].toString(),
          //                             style: TextStyle(
          //                                 color: Pallet1.fontcolornew,
          //                                 fontSize: 18)),
          //                       ),
          //                       Expanded(
          //                         flex: 1,
          //                         child: Text(
          //                             snapshot.data[i]['date'].toString() +
          //                                 " / " +
          //                                 snapshot.data[i]['time'].toString(),
          //                             style: TextStyle(
          //                                 color: Pallet1.fontcolornew,
          //                                 fontSize: 18)),
          //                       ),
          //                       Expanded(
          //                         child: Text(
          //                             "\$ " +
          //                                 snapshot.data[i]['total_amount']
          //                                     .toString(),
          //                             style: TextStyle(
          //                                 color: Pallet1.fontcolornew,
          //                                 fontSize: 18)),
          //                       ),
          //                       Expanded(
          //                         flex: 1,
          //                         child: Text(
          //                             snapshot.data[i][''].toString(), ///
          //                             style: TextStyle(
          //                                 color: Pallet1.fontcolornew,
          //                                 fontSize: 18)),
          //                       ),
          //                       Expanded(
          //                         child: Text(
          //                             text(snapshot.data[i]['cash_account'],
          //                                 snapshot.data[i]['trade_account']),
          //                             style: TextStyle(
          //                                 color: Pallet1.fontcolornew,
          //                                 fontSize: 18)),
          //                       ),
          //                     ]),
          //                   ),
          //                   selectedIndex == i
          //                       ? Container(
          //                           color: Colors.grey[300],
          //                           child: Column(children: [
          //                             Container(
          //                                 // color: Colors.green,
          //                                 child: Padding(
          //                                     padding: const EdgeInsets.all(10),
          //                                     child: Column(
          //                                         crossAxisAlignment:
          //                                             CrossAxisAlignment.start,
          //                                         children: [
          //                                           Card(
          //                                             shadowColor: Colors.black,
          //                                             elevation: 5,
          //                                             margin: EdgeInsets.all(2),
          //                                             color:
          //                                                 Pallet1.fontcolornew,
          //                                             child: Padding(
          //                                               padding:
          //                                                   const EdgeInsets
          //                                                       .all(10),
          //                                               child: Row(
          //                                                 children: [
          //                                                   Expanded(
          //                                                       flex: 1,
          //                                                       child: Text(
          //                                                           "Package",
          //                                                           style: TextStyle(
          //                                                               color: Colors
          //                                                                   .white,
          //                                                               fontSize:
          //                                                                   18))),
          //                                                   Expanded(
          //                                                     flex: 1,
          //                                                     child: Row(
          //                                                       children: [
          //                                                         SizedBox(
          //                                                           width: 30,
          //                                                         ),
          //                                                         Container(
          //                                                           width: 60,
          //                                                           child: Text(
          //                                                               "Price",
          //                                                               style: TextStyle(
          //                                                                   color:
          //                                                                       Colors.white,
          //                                                                   fontSize: 18)),
          //                                                         ),
          //                                                         SizedBox(
          //                                                           width: 30,
          //                                                         ),
          //                                                         Container(
          //                                                           width: 100,
          //                                                           child: Text(
          //                                                               "Quantity",
          //                                                               style: TextStyle(
          //                                                                   color:
          //                                                                       Colors.white,
          //                                                                   fontSize: 18)),
          //                                                         ),
          //                                                         SizedBox(
          //                                                           width: 30,
          //                                                         ),
          //                                                         Container(
          //                                                           width: 50,
          //                                                           child: Text(
          //                                                               "Total",
          //                                                               style: TextStyle(
          //                                                                   color:
          //                                                                       Colors.white,
          //                                                                   fontSize: 18)),
          //                                                         ),
          //                                                         SizedBox(
          //                                                           width: 30,
          //                                                         ),
          //                                                       ],
          //                                                     ),
          //                                                   ),
          //                                                   Expanded(
          //                                                     flex: 1,
          //                                                     child: Container(
          //                                                       child: Text(
          //                                                           "Bonus Percentage",
          //                                                           style: TextStyle(
          //                                                               color: Colors
          //                                                                   .white,
          //                                                               fontSize:
          //                                                                   18)),
          //                                                     ),
          //                                                   ),
          //                                                 ],
          //                                               ),
          //                                             ),
          //                                           ),
          //                                           for (int j = 0;
          //                                               j <
          //                                                   snapshot
          //                                                       .data[i][
          //                                                           'package_list']
          //                                                       .length;
          //                                               j++)
          //                                             Container(
          //                                                 // shadowColor:
          //                                                 //     Colors.black,
          //                                                 // elevation: 5,
          //                                                 child:
          //                                                     isMobile == true
          //                                                         ? Column(
          //                                                             crossAxisAlignment:
          //                                                                 CrossAxisAlignment
          //                                                                     .start,
          //                                                             children: [
          //
          //                                                               Text(
          //                                                                   "jhjafehjifbsgws"),
          //                                                             ],
          //                                                           )
          //                                                         : Padding(
          //                                                             padding:
          //                                                                 const EdgeInsets.all(
          //                                                                     10),
          //                                                             child:
          //                                                                 Card(
          //                                                               margin:
          //                                                                   EdgeInsets.all(10),
          //                                                               child:
          //                                                                   Padding(
          //                                                                 padding:
          //                                                                     const EdgeInsets.all(10),
          //                                                                 child:
          //                                                                     Row(
          //                                                                   mainAxisAlignment:
          //                                                                       MainAxisAlignment.spaceBetween,
          //                                                                   children: [
          //                                                                     SizedBox(
          //                                                                       width: 30,
          //                                                                     ),
          //                                                                     // ignore: missing_required_param
          //                                                                     Expanded(
          //                                                                       flex: 1,
          //                                                                       child: Text(snapshot.data[i]['package_list'][j]['name'].toString(), style: TextStyle(color: Pallet1.fontcolornew, fontSize: 18)),
          //                                                                     ),
          //                                                                     Expanded(
          //                                                                       flex: 1,
          //                                                                       child: Row(
          //                                                                         crossAxisAlignment: CrossAxisAlignment.start,
          //                                                                         children: [
          //                                                                           SizedBox(
          //                                                                             width: 30,
          //                                                                           ),
          //                                                                           Container(width: 60, child: Text(snapshot.data[i]['package_list'][j]['amount'].toString(), style: TextStyle(color: Pallet1.fontcolornew, fontSize: 18))),
          //                                                                           SizedBox(
          //                                                                             width: 30,
          //                                                                           ),
          //                                                                           Container(width: 100, child: Text(snapshot.data[i]['package_list'][j]['quantity'].toString(), style: TextStyle(color: Pallet1.fontcolornew, fontSize: 18))),
          //                                                                           SizedBox(
          //                                                                             width: 30,
          //                                                                           ),
          //                                                                           Container(width: 50, child: Text(snapshot.data[i]['package_list'][j]['sub_total'].toString(), style: TextStyle(color: Pallet1.fontcolornew, fontSize: 18))),
          //                                                                           SizedBox(
          //                                                                             width: 30,
          //                                                                           ),
          //                                                                         ],
          //                                                                       ),
          //                                                                     ),
          //                                                                     Expanded(
          //                                                                       flex: 1,
          //                                                                       child: Text(snapshot.data[i]['package_list'][j]['bonus_percentage'].toString() + " DUC", style: TextStyle(color: Pallet1.fontcolornew, fontSize: 18)),
          //                                                                     ),
          //                                                                   ],
          //                                                                 ),
          //                                                               ),
          //                                                             ),
          //                                                           )),
          //                                           Padding(
          //                                             padding:
          //                                                 const EdgeInsets.all(
          //                                                     10),
          //                                             child: Row(
          //                                               crossAxisAlignment:
          //                                                   CrossAxisAlignment
          //                                                       .start,
          //                                               children: [
          //                                                 Expanded(
          //                                                   child: Container(
          //                                                     height: 30,
          //                                                     width: 30,
          //                                                     child: Text(
          //                                                         'Total Cash Debit  :\$ ' +
          //                                                             snapshot
          //                                                                 .data[
          //                                                                     i]
          //                                                                     [
          //                                                                     'cash_account']
          //                                                                 .toString(),
          //                                                         style: TextStyle(
          //                                                             color: Pallet1
          //                                                                 .fontcolornew,
          //                                                             fontSize:
          //                                                                 18)),
          //                                                   ),
          //                                                 ),
          //                                                 Expanded(
          //                                                   flex: 1,
          //                                                   child: Container(
          //                                                     height: 30,
          //                                                     width: 30,
          //                                                     child: Text(
          //                                                         'Total Trading Tokens Debit  :\$ ' +
          //                                                             snapshot
          //                                                                 .data[
          //                                                                     i]
          //                                                                     [
          //                                                                     'trade_account']
          //                                                                 .toString(),
          //                                                         style: TextStyle(
          //                                                             color: Pallet1
          //                                                                 .fontcolornew,
          //                                                             fontSize:
          //                                                                 18)),
          //                                                   ),
          //                                                 ),
          //                                               ],
          //                                             ),
          //                                           )
          //                                         ])))
          //                           ]))
          //                       : Container(
          //                           color: Colors.yellow,
          //                         )
          //                 ],
          //               ),
          //             )),
          //       ),
          //     )
          // }
        } else
          return Loader();
      },
    );
  }
}

// ignore: missing_return
text(var data, var data2) {
  String val;
  if (data == "0") {
    val = "Trading Transaction";
  } else if (data2 == "0") {
    val = "Cash Transaction";
  } else {
    val = "Cash and Trading Transaction";
  }
  return val;
}

// ignore: must_be_immutable
// class ListDetails extends StatefulWidget {
//   ListDetails(
//     data,
//     length, {
//     Key key,
//   }) : super(key: key);
//   List data;
//   int length;
//   _ListDetailsState createState() => _ListDetailsState();
// }
// class _ListDetailsState extends State<ListDetails> {
//   Invoices width = Invoices();
//   int selected;
//   @override
//   Widget build(BuildContext context) {
//     return Container(
//       color: Colors.white,
//       child: SingleChildScrollView(
//           child: Column(children: [
//         Container(
//           color: Pallet1.fontcolornew,
//           width: width.wdgtWidth,
//           height: 50,
//           // decoration: BoxDecoration(
//           //     border: Border.all(color: Colors.black),
//           //     borderRadius: BorderRadius.circular(5)),
//           child: Padding(
//             padding: const EdgeInsets.all(5),
//             child: Row(
//               children: [
//                 Expanded(
//                     flex: 1,
//                     child: Text(" Invoice Id ",
//                         style: TextStyle(color: Colors.white))),
//                 Expanded(
//                   flex: 1,
//                   child: Text(" Date & Time of Perchas ",
//                       style: TextStyle(color: Colors.white)),
//                 ),
//                 Expanded(
//                   flex: 1,
//                   child: Text(" Grand Total ",
//                       style: TextStyle(color: Colors.white)),
//                 ),
//                 Expanded(
//                   flex: 1,
//                   child: Text("  ", style: TextStyle(color: Colors.white)),
//                 ),
//               ],
//             ),
//           ),
//         ),
//         widget.length == 0
//             ? Expanded(
//                 child: Center(
//                   child: Text("No Purchases Found",
//                       style: TextStyle(
//                         color: Pallet1.fontcolornew,
//                         fontSize: Pallet1.normalfont,
//                       )),
//                 ),
//               )
//             : Container(),
//         for (var i = 0; i < widget.length; i++)
//           ExpansionTile(
//               //attention
//               initiallyExpanded: selected == i, //attention
//               title: Padding(
//                 padding: const EdgeInsets.all(10),
//                 child: Row(children: [
//                   Expanded(
//                     flex: 1,
//                     child: Container(
//                       height: 30,
//                       width: 60,
//                       child: Text(invoices.indata[i]['invoice_id'].toString(),
//                           style: TextStyle(
//                               color: Pallet1.fontcolornew,
//                               fontSize: Pallet1.heading4)),
//                     ),
//                   ),
//                   Expanded(
//                     flex: 1,
//                     child: Container(
//                       height: 30,
//                       width: 240,
//                       child: Text(invoices.indata[i]['date'].toString(),
//                           style: TextStyle(
//                               color: Pallet1.fontcolornew,
//                               fontSize: Pallet1.heading4)),
//                     ),
//                   ),
//                   Expanded(
//                     flex: 1,
//                     child: Container(
//                       height: 30,
//                       width: 60,
//                       child: Text(invoices.indata[i]['total_amount'].toString(),
//                           style: TextStyle(
//                               color: Pallet1.fontcolornew,
//                               fontSize: Pallet1.heading4)),
//                     ),
//                   ),
//                   Expanded(
//                     flex: 1,
//                     child: Container(
//                       height: 30,
//                       width: 60,
//                       child: Text(' ',
//                           style: TextStyle(
//                               color: Pallet1.fontcolornew,
//                               fontSize: Pallet1.heading4)),
//                     ),
//                   ),
//                 ]),
//               ),
//               children: <Widget>[
//                 Padding(
//                   padding: const EdgeInsets.all(10),
//                   child: Container(
//                     color: Colors.blue,
//                     child: Column(
//                       children: [
//                         Container(
//                           // color: Colors.green,
//                           child: Column(
//                             crossAxisAlignment: CrossAxisAlignment.start,
//                             children: [
//                               Padding(
//                                 padding: const EdgeInsets.all(5),
//                                 child: Row(
//                                   children: [
//                                     Expanded(
//                                         flex: 1,
//                                         child: Padding(
//                                           padding: const EdgeInsets.fromLTRB(
//                                               20, 5, 20, 5),
//                                           child: Text(" Prodect Name ",
//                                               style: TextStyle(
//                                                   color: Colors.white)),
//                                         )),
//                                     Row(
//                                       children: [
//                                         Padding(
//                                           padding: const EdgeInsets.fromLTRB(
//                                               20, 5, 20, 5),
//                                           child: Text(" Price ",
//                                               style: TextStyle(
//                                                   color: Colors.white)),
//                                         ),
//                                       ],
//                                     ),
//                                     Padding(
//                                       padding: const EdgeInsets.fromLTRB(
//                                           20, 5, 20, 5),
//                                       child: Text(" Quantity ",
//                                           style:
//                                               TextStyle(color: Colors.white)),
//                                     ),
//                                     Padding(
//                                       padding: const EdgeInsets.fromLTRB(
//                                           20, 5, 20, 5),
//                                       child: Text(" Total ",
//                                           style:
//                                               TextStyle(color: Colors.white)),
//                                     ),
//                                   ],
//                                 ),
//                               ),
//                               for (int j = 0;
//                                   j < invoices.indata[i]['package_list'].length;
//                                   j++)
//                                 Padding(
//                                   padding:
//                                       EdgeInsets.all(Pallet1.defaultPadding),
//                                   child: Container(
//                                       child: Padding(
//                                     padding:
//                                         EdgeInsets.all(Pallet1.defaultPadding),
//                                     child: isMobile == true
//                                         ? Column(
//                                             crossAxisAlignment:
//                                                 CrossAxisAlignment.start,
//                                             children: [
//
//                                               Text("jhjafehjifbsgws")
//                                             ],
//                                           )
//                                         : Container(
//                                             decoration: BoxDecoration(
//                                                 border: Border.all(
//                                                     color: Colors.black),
//                                                 borderRadius:
//                                                     BorderRadius.circular(
//                                                         Pallet1.radius)),
//                                             child: Row(
//                                               mainAxisAlignment:
//                                                   MainAxisAlignment
//                                                       .spaceBetween,
//                                               children: [
//                                                 // ignore: missing_required_param
//                                                 Expanded(
//                                                   flex: 3,
//                                                   child: Text(
//                                                       invoices.indata[i]
//                                                               ['package_list']
//                                                               [j]['name']
//                                                           .toString(),
//                                                       style: TextStyle(
//                                                           color: Pallet1
//                                                               .fontcolornew,
//                                                           fontSize: Pallet1
//                                                               .heading4)),
//                                                 ),
//                                                 Expanded(
//                                                   flex: 1,
//                                                   child: Row(
//                                                     crossAxisAlignment:
//                                                         CrossAxisAlignment
//                                                             .start,
//                                                     children: [
//                                                       Padding(
//                                                         padding:
//                                                             const EdgeInsets
//                                                                     .fromLTRB(
//                                                                 20, 5, 20, 5),
//                                                         child: Text(
//                                                             invoices.indata[i][
//                                                                     'package_list']
//                                                                     [j]
//                                                                     ['amount']
//                                                                 .toString(),
//                                                             style: TextStyle(
//                                                                 color: Pallet1
//                                                                     .fontcolornew,
//                                                                 fontSize: Pallet1
//                                                                     .heading4)),
//                                                       ),
//                                                       Padding(
//                                                         padding:
//                                                             const EdgeInsets
//                                                                     .fromLTRB(
//                                                                 20, 5, 20, 5),
//                                                         child: Text(
//                                                             invoices.indata[i][
//                                                                     'package_list']
//                                                                     [j]
//                                                                     ['quantity']
//                                                                 .toString(),
//                                                             style: TextStyle(
//                                                                 color: Pallet1
//                                                                     .fontcolornew,
//                                                                 fontSize: Pallet1
//                                                                     .heading4)),
//                                                       ),
//                                                       Padding(
//                                                         padding:
//                                                             const EdgeInsets
//                                                                     .fromLTRB(
//                                                                 20, 5, 20, 5),
//                                                         child: Text(
//                                                             invoices.indata[i][
//                                                                     'package_list']
//                                                                     [j][
//                                                                     'sub_total']
//                                                                 .toString(),
//                                                             style: TextStyle(
//                                                                 color: Pallet1
//                                                                     .fontcolornew,
//                                                                 fontSize: Pallet1
//                                                                     .heading4)),
//                                                       ),
//                                                     ],
//                                                   ),
//                                                 ),
//                                               ],
//                                             ),
//                                           ),
//                                   )),
//                                 ),
//                             ],
//                           ),
//                         ),
//                         // for (var item in )
//                         SizedBox(height: 50),
//                       ],
//                     ),
//                   ),
//                 )
//               ],
//               onExpansionChanged: ((newState) {
//                 if (newState)
//                   setState(() {
//                     Duration(seconds: 20000);
//                     selected = i;
//                     print("package 5u 7jh56fujh");
//                     print(invoices.indata[i]['package_list'].length);
//                   });
//                 else
//                   setState(() {
//                     selected = -1;
//                   });
//               })),
//       ])),
//     );
//   }
// }

class Invoicehistory extends StatefulWidget {
  Invoicehistory(
    this.data1, {
    Key key,
    this.wdgtWidth,
    this.wdgtHeight,
  }) : super(key: key);
  final double wdgtWidth, wdgtHeight;
  var data1;
  @override
  _InvoicehistoryState createState() => _InvoicehistoryState();
}

class _InvoicehistoryState extends State<Invoicehistory> {
  bool loader = true;
  @override
  void initState() {
    super.initState();
    // _DataSource();
    Timer(Duration(seconds: 3), callback1);
  }

  callback1() {
    setState(() {});
    loader = false;
  }

  @override
  // ignore: missing_return
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Container(
      child: loader == true
          ? Container(
              child: Center(
                child: Loader(),
              ),
            )
          : widget.data1.length == 0
              ? Center(
                  child: Text("No Purchases Found",
                      style: TextStyle(
                        color: Pallet1.fontcolornew,
                        fontSize: Pallet1.normalfont,
                      )),
                )
              : SingleChildScrollView(
                  child: PaginatedDataTable(
                    columnSpacing: 20,
                    showCheckboxColumn: false,
                    rowsPerPage: 10,
                    columns: [
                      DataColumn(
                          label: MyText(
                              text: 'Invoice Id',
                              style: TextStyle(
                                  color: Pallet.dashcontainerback,
                                  fontSize: isMobile == true
                                      ? Pallet.heading9
                                      : Pallet.heading3,
                                  fontWeight: Pallet.heading2wgt))),
                      DataColumn(
                          label: MyText(
                              text: isMobile == true
                                  ? "Date"
                                  : "Date & Time of Purchase",
                              style: TextStyle(
                                  color: Pallet.dashcontainerback,
                                  fontSize: isMobile == true
                                      ? Pallet.heading9
                                      : Pallet.heading3,
                                  fontWeight: Pallet.heading2wgt))),
                      DataColumn(
                          label: MyText(
                              text: "Grand Total",
                              style: TextStyle(
                                  color: Pallet.dashcontainerback,
                                  fontSize: isMobile == true
                                      ? Pallet.heading9
                                      : Pallet.heading3,
                                  fontWeight: Pallet.heading2wgt))),
                      DataColumn(
                          label: MyText(
                              text: "Payment Method",
                              style: TextStyle(
                                  color: Pallet.dashcontainerback,
                                  fontSize: isMobile == true
                                      ? Pallet.heading9
                                      : Pallet.heading3,
                                  fontWeight: Pallet.heading2wgt))),
                    ],
                    source: _DataSource(context, widget.data1, size),
                  ),
                ),
    );
  }
}

class _Row {
  _Row(
    this.valueA,
    this.valueB,
    this.valueC,
    this.valueD,
  );

  final String valueA;
  final String valueB;
  final String valueC;
  final String valueD;
  bool selected = false;
}

class _DataSource extends DataTableSource {
  _DataSource(
    this.context,
    this.data,
    this.size,
  ) {
    _rows = <_Row>[
      for (var history in data)
        _Row(
          history["invoice_id"].toString(),
          isMobile == true
              ? history["date"].toString()
              : history["date"] + ' ' + history["time"] + ' UTC'.toString(),
          history["total_amount"].toString(),
          text(history["cash_account"], history["trade_account"]).toString(),
        ),
    ];
  }
  var data;
  final BuildContext context;
  List<_Row> _rows;
  int _selectedCount = 0;
  Size size;

  @override
  DataRow getRow(int index) {
    //  print("get row");
    assert(index >= 0);
    if (index >= _rows.length) return null;
    final row = _rows[index];
    return DataRow.byIndex(
      index: index,
      onSelectChanged: (value) {
        if (invoices.type == false) showDetails(context, index, data, size);
      },
      cells: [
        DataCell(MyText(
            // overflow: TextOverflow.ellipsis,
            text: isMobile == true
                ? row.valueA.replaceRange(15, row.valueA.length, '...')
                : row.valueA,
            style: TextStyle(
                color: Pallet.fontcolornew,
                fontSize: isMobile == true ? 10 : 16))),
        DataCell(MyText(
            // overflow: TextOverflow.ellipsis,
            text: row.valueB,
            style: TextStyle(
                color: Pallet.fontcolornew,
                fontSize: isMobile == true ? 10 : 16))),
        DataCell(MyText(
            // overflow: TextOverflow.ellipsis,
            text: row.valueC,
            style: TextStyle(
                color: Pallet.fontcolornew,
                fontSize: isMobile == true ? 10 : 16))),
        DataCell(MyText(
            overflow: TextOverflow.ellipsis,
            text: row.valueD,
            style: TextStyle(
                color: Pallet.fontcolornew,
                fontSize: isMobile == true ? 10 : 16))),
      ],
    );
  }

  showDetails(BuildContext c, int index, var valu, Size size) async =>
      await showDialog<bool>(
        context: c,
        builder: (_) => AlertDialog(
            // scrollable: true,

            title: Column(
              crossAxisAlignment: isMobile == true
                  ? CrossAxisAlignment.center
                  : CrossAxisAlignment.end,
              children: [
                Image.asset(
                  'final.png',
                  color: Pallet.fontcolornew,
                  // alignment: Alignment.topRight,
                ),
                SizedBox(
                  height: isMobile == true ? 5 : 0,
                ),
                Text(
                  isMobile == true
                      ? "CENTURION GLOBAL LIMITED \n(A Cayman's Company)"
                      : "CENTURION GLOBAL LIMITED (A Cayman's Company)",
                  style: TextStyle(
                    color: Pallet.fontcolornew,
                    fontSize:
                        isMobile == true ? Pallet.heading9 : Pallet.heading6,
                  ),
                  // textAlign: TextAlign.right,
                ),
              ],
            ),
            content: Container(
              child: Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  Container(
                      // flex: 1,
                      child: Column(
                    mainAxisSize: MainAxisSize.min,
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: [
                      SizedBox(
                        height: isMobile == true ? 5 : 10,
                      ),
                      Text(
                        "Invoice",
                        style: TextStyle(
                          color: Pallet.fontcolornew,
                          fontSize: Pallet.heading1,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      SizedBox(
                        height: isMobile == true ? 5 : 10,
                      ),
                      Text(
                        "Invoice ID:  " + valu[index]["invoice_id"],
                        style: TextStyle(
                          color: Pallet.fontcolornew,
                          fontSize: isMobile == true
                              ? Pallet.heading6
                              : Pallet.heading4,
                        ),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Text(
                        "Date:  " + valu[index]["date"],
                        style: TextStyle(
                          color: Pallet.fontcolornew,
                          fontSize: isMobile == true
                              ? Pallet.heading6
                              : Pallet.heading4,
                        ),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Text(
                        "Payment Method:  " +
                            text(
                              valu[index]["cash_account"],
                              valu[index]["trade_account"],
                            ),
                        style: TextStyle(
                          color: Pallet.fontcolornew,
                          fontSize: isMobile == true
                              ? Pallet.heading6
                              : Pallet.heading4,
                        ),
                      ),
                    ],
                  )),
                  SizedBox(
                    height: isMobile == true ? 10 : 20,
                  ),
                  MediaQuery.of(context).size.width > 600
                      ? Column(
                          mainAxisSize: MainAxisSize.min,
                          crossAxisAlignment: CrossAxisAlignment.stretch,
                          children: [
                            Padding(
                              padding: const EdgeInsets.all(10),
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceEvenly,
                                children: [
                                  Expanded(
                                    flex: 6,
                                    child: Center(
                                      child: Container(
                                        // width: 80,
                                        child: Text(
                                          "PACKAGE",
                                          style: TextStyle(
                                            fontSize: isMobile == true
                                                ? Pallet.heading8
                                                : Pallet.heading4,
                                            color: Pallet.fontcolornew,
                                            fontWeight: FontWeight.w600,
                                          ),
                                        ),
                                      ),
                                    ),
                                  ),
                                  Expanded(
                                    flex: 6,
                                    child: Center(
                                      child: Container(
                                        // width: 80,
                                        child: Text(
                                          "DSV/DUC",
                                          style: TextStyle(
                                            fontSize: isMobile == true
                                                ? Pallet.heading8
                                                : Pallet.heading4,
                                            color: Pallet.fontcolornew,
                                            fontWeight: FontWeight.w600,
                                          ),
                                        ),
                                      ),
                                    ),
                                  ),
                                  Expanded(
                                    flex: 6,
                                    child: Center(
                                      child: Container(
                                        // width: 130,
                                        child: Text(
                                          "BONUS CREDITS",
                                          style: TextStyle(
                                            fontSize: isMobile == true
                                                ? Pallet.heading8
                                                : Pallet.heading4,
                                            color: Pallet.fontcolornew,
                                            fontWeight: FontWeight.w600,
                                          ),
                                        ),
                                      ),
                                    ),
                                  ),
                                  Expanded(
                                    flex: 6,
                                    child: Center(
                                      child: Container(
                                        width: 80,
                                        child: Text(
                                          "QUANTITY",
                                          style: TextStyle(
                                            fontSize: isMobile == true
                                                ? Pallet.heading8
                                                : Pallet.heading4,
                                            color: Pallet.fontcolornew,
                                            fontWeight: FontWeight.w600,
                                          ),
                                        ),
                                      ),
                                    ),
                                  ),
                                  Expanded(
                                    flex: 6,
                                    child: Center(
                                      child: Container(
                                        // width: 80,
                                        child: Text(
                                          "AMOUNT",
                                          style: TextStyle(
                                            fontSize: isMobile == true
                                                ? Pallet.heading8
                                                : Pallet.heading4,
                                            color: Pallet.fontcolornew,
                                            fontWeight: FontWeight.w600,
                                          ),
                                        ),
                                      ),
                                    ),
                                  ),
                                  Expanded(
                                    flex: 6,
                                    child: Center(
                                      child: Container(
                                        // width: 50,
                                        child: Text(
                                          "TOTAL",
                                          style: TextStyle(
                                            fontSize: isMobile == true
                                                ? Pallet.heading8
                                                : Pallet.heading4,
                                            color: Pallet.fontcolornew,
                                            fontWeight: FontWeight.w600,
                                          ),
                                        ),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            SizedBox(
                              height: 8,
                            ),
                            for (int i = 0;
                                i < valu[index]["package_list"].length;
                                i++)
                              Padding(
                                padding: const EdgeInsets.all(10),
                                child: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceEvenly,
                                  children: [
                                    if (invoices.type == false)
                                      Expanded(
                                        flex: 6,
                                        child: Center(
                                          child: Container(
                                              // width: 70,
                                              child: Text(
                                            "DSV",
                                            style: TextStyle(
                                              fontSize: isMobile == true
                                                  ? Pallet.heading8
                                                  : Pallet.heading4,
                                              color: Pallet.fontcolornew,
                                              // fontWeight: FontWeight.w600,
                                            ),
                                          )),
                                        ),
                                      )
                                    else
                                      Expanded(
                                        flex: 6,
                                        child: Center(
                                          child: Container(
                                            // width: 70,
                                            child: Text(
                                              "DSV-P",
                                              style: TextStyle(
                                                fontSize: isMobile == true
                                                    ? Pallet.heading8
                                                    : Pallet.heading4,
                                                color: Pallet.fontcolornew,
                                                // fontWeight: FontWeight.w600,
                                              ),
                                            ),
                                          ),
                                        ),
                                      ),
                                    invoices.type == false
                                        ? Expanded(
                                            flex: 6,
                                            child: Center(
                                              child: Container(
                                                  // width: 80,
                                                  child: Text(
                                                valu[index]["package_list"][i]
                                                    ["name"],
                                                style: TextStyle(
                                                  fontSize: isMobile == true
                                                      ? Pallet.heading8
                                                      : Pallet.heading4,
                                                  color: Pallet.fontcolornew,
                                                  // fontWeight: FontWeight.w600,
                                                ),
                                              )),
                                            ),
                                          )
                                        : Expanded(
                                            flex: 6,
                                            child: Center(
                                              child: Container(
                                                // width: 80,
                                                child: Text(
                                                  valu[index]["package_list"][i]
                                                      ["name"],
                                                  style: TextStyle(
                                                    fontSize: isMobile == true
                                                        ? Pallet.heading8
                                                        : Pallet.heading4,
                                                    color: Pallet.fontcolornew,
                                                    // fontWeight: FontWeight.w600,
                                                  ),
                                                ),
                                              ),
                                            ),
                                          ),
                                    Expanded(
                                      flex: 6,
                                      child: Center(
                                        child: Container(
                                          // width: 100,
                                          child: Text(
                                            valu[index]["package_list"][i]
                                                        ["bonus_percentage"]
                                                    .toString() +
                                                " % " +
                                                "DUC",
                                            style: TextStyle(
                                              fontSize: isMobile == true
                                                  ? Pallet.heading8
                                                  : Pallet.heading4,
                                              color: Pallet.fontcolornew,
                                              // fontWeight: FontWeight.w600,
                                            ),
                                          ),
                                        ),
                                      ),
                                    ),
                                    Expanded(
                                      flex: 6,
                                      child: Center(
                                        child: Container(
                                          // width: 80,
                                          child: Text(
                                            valu[index]["package_list"][i]
                                                    ["quantity"]
                                                .toString(),
                                            style: TextStyle(
                                              fontSize: isMobile == true
                                                  ? Pallet.heading8
                                                  : Pallet.heading4,
                                              color: Pallet.fontcolornew,
                                              // fontWeight: FontWeight.w600,
                                            ),
                                          ),
                                        ),
                                      ),
                                    ),
                                    Expanded(
                                      flex: 6,
                                      child: Center(
                                        child: Container(
                                          // width: 60,
                                          child: Text(
                                            valu[index]["package_list"][i]
                                                    ["amount"]
                                                .toString(),
                                            style: TextStyle(
                                              fontSize: isMobile == true
                                                  ? Pallet.heading8
                                                  : Pallet.heading4,
                                              color: Pallet.fontcolornew,
                                              // fontWeight: FontWeight.w600,
                                            ),
                                          ),
                                        ),
                                      ),
                                    ),
                                    Expanded(
                                      flex: 6,
                                      child: Center(
                                        child: Container(
                                          // width: 60,
                                          child: Text(
                                            valu[index]["package_list"][i]
                                                    ["sub_total"]
                                                .toString(),
                                            style: TextStyle(
                                              fontSize: isMobile == true
                                                  ? Pallet.heading8
                                                  : Pallet.heading4,
                                              color: Pallet.fontcolornew,
                                              // fontWeight: FontWeight.w600,
                                            ),
                                          ),
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            SizedBox(
                              height: 8,
                            ),
                            Padding(
                              padding: const EdgeInsets.fromLTRB(0, 5, 20, 5),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.end,
                                children: [
                                  Text(
                                    "TOTAL",
                                    style: TextStyle(
                                      fontSize: isMobile == true
                                          ? Pallet.heading8
                                          : Pallet.heading3,
                                      color: Pallet.fontcolornew,
                                      fontWeight: FontWeight.w600,
                                    ),
                                  ),
                                  SizedBox(
                                    width: 10,
                                  ),
                                  Text(
                                    "\$ " + valu[index]["total_amount"],
                                    style: TextStyle(
                                      fontSize: isMobile == true
                                          ? Pallet.heading8
                                          : Pallet.heading3,
                                      color: Pallet.fontcolornew,
                                      fontWeight: FontWeight.w600,
                                    ),
                                  ),
                                  SizedBox(
                                    width: isMobile == true ? 10 : 30,
                                  ),
                                ],
                              ),
                            ),
                          ],
                        )
                      : SingleChildScrollView(
                          scrollDirection: Axis.horizontal,
                          child: Container(
                            width: 500,
                            child: Column(
                              mainAxisSize: MainAxisSize.min,
                              crossAxisAlignment: CrossAxisAlignment.stretch,
                              children: [
                                Padding(
                                  padding: const EdgeInsets.all(10),
                                  child: Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceEvenly,
                                    children: [
                                      Expanded(
                                        flex: 6,
                                        child: Center(
                                          child: Container(
                                            // width: 80,
                                            child: Text(
                                              "PACKAGE",
                                              style: TextStyle(
                                                fontSize: isMobile == true
                                                    ? Pallet.heading8
                                                    : Pallet.heading4,
                                                color: Pallet.fontcolornew,
                                                fontWeight: FontWeight.w600,
                                              ),
                                            ),
                                          ),
                                        ),
                                      ),
                                      Expanded(
                                        flex: 6,
                                        child: Center(
                                          child: Container(
                                            // width: 80,
                                            child: Text(
                                              "DSV/DUC",
                                              style: TextStyle(
                                                fontSize: isMobile == true
                                                    ? Pallet.heading8
                                                    : Pallet.heading4,
                                                color: Pallet.fontcolornew,
                                                fontWeight: FontWeight.w600,
                                              ),
                                            ),
                                          ),
                                        ),
                                      ),
                                      Expanded(
                                        flex: 6,
                                        child: Center(
                                          child: Container(
                                            // width: 130,
                                            child: Text(
                                              "BONUS CREDITS",
                                              style: TextStyle(
                                                fontSize: isMobile == true
                                                    ? Pallet.heading8
                                                    : Pallet.heading4,
                                                color: Pallet.fontcolornew,
                                                fontWeight: FontWeight.w600,
                                              ),
                                            ),
                                          ),
                                        ),
                                      ),
                                      Expanded(
                                        flex: 6,
                                        child: Center(
                                          child: Container(
                                            width: 80,
                                            child: Text(
                                              "QUANTITY",
                                              style: TextStyle(
                                                fontSize: isMobile == true
                                                    ? Pallet.heading8
                                                    : Pallet.heading4,
                                                color: Pallet.fontcolornew,
                                                fontWeight: FontWeight.w600,
                                              ),
                                            ),
                                          ),
                                        ),
                                      ),
                                      Expanded(
                                        flex: 6,
                                        child: Center(
                                          child: Container(
                                            // width: 80,
                                            child: Text(
                                              "AMOUNT",
                                              style: TextStyle(
                                                fontSize: isMobile == true
                                                    ? Pallet.heading8
                                                    : Pallet.heading4,
                                                color: Pallet.fontcolornew,
                                                fontWeight: FontWeight.w600,
                                              ),
                                            ),
                                          ),
                                        ),
                                      ),
                                      Expanded(
                                        flex: 6,
                                        child: Center(
                                          child: Container(
                                            // width: 50,
                                            child: Text(
                                              "TOTAL",
                                              style: TextStyle(
                                                fontSize: isMobile == true
                                                    ? Pallet.heading8
                                                    : Pallet.heading4,
                                                color: Pallet.fontcolornew,
                                                fontWeight: FontWeight.w600,
                                              ),
                                            ),
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                                SizedBox(
                                  height: 8,
                                ),
                                for (int i = 0;
                                    i < valu[index]["package_list"].length;
                                    i++)
                                  Padding(
                                    padding: const EdgeInsets.all(10),
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceEvenly,
                                      children: [
                                        if (invoices.type == false)
                                          Expanded(
                                            flex: 6,
                                            child: Center(
                                              child: Container(
                                                  // width: 70,
                                                  child: Text(
                                                "DSV",
                                                style: TextStyle(
                                                  fontSize: isMobile == true
                                                      ? Pallet.heading8
                                                      : Pallet.heading4,
                                                  color: Pallet.fontcolornew,
                                                  // fontWeight: FontWeight.w600,
                                                ),
                                              )),
                                            ),
                                          )
                                        else
                                          Expanded(
                                            flex: 6,
                                            child: Center(
                                              child: Container(
                                                // width: 70,
                                                child: Text(
                                                  "DSV-P",
                                                  style: TextStyle(
                                                    fontSize: isMobile == true
                                                        ? Pallet.heading8
                                                        : Pallet.heading4,
                                                    color: Pallet.fontcolornew,
                                                    // fontWeight: FontWeight.w600,
                                                  ),
                                                ),
                                              ),
                                            ),
                                          ),
                                        invoices.type == false
                                            ? Expanded(
                                                flex: 6,
                                                child: Center(
                                                  child: Container(
                                                      // width: 80,
                                                      child: Text(
                                                    valu[index]["package_list"]
                                                        [i]["name"],
                                                    style: TextStyle(
                                                      fontSize: isMobile == true
                                                          ? Pallet.heading8
                                                          : Pallet.heading4,
                                                      color:
                                                          Pallet.fontcolornew,
                                                      // fontWeight: FontWeight.w600,
                                                    ),
                                                  )),
                                                ),
                                              )
                                            : Expanded(
                                                flex: 6,
                                                child: Center(
                                                  child: Container(
                                                    // width: 80,
                                                    child: Text(
                                                      valu[index]
                                                              ["package_list"]
                                                          [i]["name"],
                                                      style: TextStyle(
                                                        fontSize:
                                                            Pallet.heading4,
                                                        color:
                                                            Pallet.fontcolornew,
                                                        // fontWeight: FontWeight.w600,
                                                      ),
                                                    ),
                                                  ),
                                                ),
                                              ),
                                        Expanded(
                                          flex: 6,
                                          child: Center(
                                            child: Container(
                                              // width: 100,
                                              child: Text(
                                                valu[index]["package_list"][i][
                                                                "bonus_percentage"]
                                                            .toString() ==
                                                        'null'
                                                    ? '0'
                                                    : valu[index]["package_list"]
                                                                    [i][
                                                                "bonus_percentage"]
                                                            .toString() +
                                                        " % " +
                                                        "DUC",
                                                style: TextStyle(
                                                  fontSize: Pallet.heading4,
                                                  color: Pallet.fontcolornew,
                                                  // fontWeight: FontWeight.w600,
                                                ),
                                              ),
                                            ),
                                          ),
                                        ),
                                        Expanded(
                                          flex: 6,
                                          child: Center(
                                            child: Container(
                                              // width: 80,
                                              child: Text(
                                                valu[index]["package_list"][i]
                                                        ["quantity"]
                                                    .toString(),
                                                style: TextStyle(
                                                  fontSize: Pallet.heading4,
                                                  color: Pallet.fontcolornew,
                                                  // fontWeight: FontWeight.w600,
                                                ),
                                              ),
                                            ),
                                          ),
                                        ),
                                        Expanded(
                                          flex: 6,
                                          child: Center(
                                            child: Container(
                                              // width: 60,
                                              child: Text(
                                                valu[index]["package_list"][i]
                                                        ["amount"]
                                                    .toString(),
                                                style: TextStyle(
                                                  fontSize: Pallet.heading4,
                                                  color: Pallet.fontcolornew,
                                                  // fontWeight: FontWeight.w600,
                                                ),
                                              ),
                                            ),
                                          ),
                                        ),
                                        Expanded(
                                          flex: 6,
                                          child: Center(
                                            child: Container(
                                              // width: 60,
                                              child: Text(
                                                valu[index]["package_list"][i]
                                                        ["sub_total"]
                                                    .toString(),
                                                style: TextStyle(
                                                  fontSize: Pallet.heading4,
                                                  color: Pallet.fontcolornew,
                                                  // fontWeight: FontWeight.w600,
                                                ),
                                              ),
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                SizedBox(
                                  height: 8,
                                ),
                                Padding(
                                  padding: const EdgeInsets.symmetric(
                                      vertical: 5, horizontal: 10),
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.end,
                                    children: [
                                      Text(
                                        "TOTAL",
                                        style: TextStyle(
                                          fontSize: isMobile == true
                                              ? Pallet.heading8
                                              : Pallet.heading3,
                                          color: Pallet.fontcolornew,
                                          fontWeight: FontWeight.w600,
                                        ),
                                      ),
                                      SizedBox(
                                        width: 10,
                                      ),
                                      Text(
                                        "\$ " + valu[index]["total_amount"],
                                        style: TextStyle(
                                          fontSize: isMobile == true
                                              ? Pallet.heading8
                                              : Pallet.heading3,
                                          color: Pallet.fontcolornew,
                                          fontWeight: FontWeight.w600,
                                        ),
                                      ),
                                      SizedBox(
                                        width: 30,
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                  SizedBox(
                    height: 20,
                  ),
                  MediaQuery.of(context).size.width > 600
                      ? Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: [
                            Row(
                              mainAxisSize: MainAxisSize.min,
                              children: [
                                Text(
                                  "TOTAL Cash Debit",
                                  style: TextStyle(
                                    fontSize: isMobile == true
                                        ? Pallet.heading8
                                        : Pallet.heading4,
                                    color: Pallet.fontcolornew,
                                    // fontWeight: FontWeight.w600,
                                  ),
                                ),
                                SizedBox(
                                  width: 10,
                                ),
                                Text(
                                  "\$" + valu[index]["cash_account"],
                                  style: TextStyle(
                                    fontSize: isMobile == true
                                        ? Pallet.heading8
                                        : Pallet.heading4,
                                    color: Pallet.fontcolornew,
                                    // fontWeight: FontWeight.w600,
                                  ),
                                ),
                              ],
                            ),
                            Row(
                              mainAxisSize: MainAxisSize.min,
                              children: [
                                Text(
                                  "TOTAL Trading Tokens Debit",
                                  style: TextStyle(
                                    fontSize: isMobile == true
                                        ? Pallet.heading8
                                        : Pallet.heading4,
                                    color: Pallet.fontcolornew,
                                    // fontWeight: FontWeight.w600,
                                  ),
                                ),
                                SizedBox(
                                  width: 10,
                                ),
                                Text(
                                  valu[index]["trade_account"],
                                  style: TextStyle(
                                    fontSize: isMobile == true
                                        ? Pallet.heading8
                                        : Pallet.heading4,
                                    color: Pallet.fontcolornew,
                                    // fontWeight: FontWeight.w600,
                                  ),
                                ),
                              ],
                            ),
                          ],
                        )
                      : Column(
                          // mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          mainAxisSize: MainAxisSize.min,
                          children: [
                            Container(
                              // color: Colors.amber,
                              child: Wrap(
                                // mainAxisSize: MainAxisSize.min,
                                children: [
                                  Text(
                                    "TOTAL Cash Debit",
                                    style: TextStyle(
                                      fontSize: isMobile == true
                                          ? Pallet.heading8
                                          : Pallet.heading4,
                                      color: Pallet.fontcolornew,
                                      // fontWeight: FontWeight.w600,
                                    ),
                                  ),
                                  SizedBox(
                                    width: 10,
                                  ),
                                  Text(
                                    "\$" + valu[index]["cash_account"],
                                    style: TextStyle(
                                      fontSize: isMobile == true
                                          ? Pallet.heading8
                                          : Pallet.heading4,
                                      color: Pallet.fontcolornew,
                                      // fontWeight: FontWeight.w600,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            SizedBox(
                              height: 5,
                            ),
                            Container(
                              // color: Colors.blue,
                              child: Wrap(
                                // mainAxisSize: MainAxisSize.min,
                                children: [
                                  Text(
                                    "TOTAL Trading Tokens Debit",
                                    style: TextStyle(
                                      fontSize: isMobile == true
                                          ? Pallet.heading8
                                          : Pallet.heading4,
                                      color: Pallet.fontcolornew,
                                      // fontWeight: FontWeight.w600,
                                    ),
                                  ),
                                  SizedBox(
                                    width: 10,
                                  ),
                                  Text(
                                    valu[index]["trade_account"],
                                    style: TextStyle(
                                      fontSize: isMobile == true
                                          ? Pallet.heading8
                                          : Pallet.heading4,
                                      color: Pallet.fontcolornew,
                                      // fontWeight: FontWeight.w600,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        )
                ],
              ),
            )),
      );

  @override
  int get rowCount => _rows.length;
  @override
  bool get isRowCountApproximate => false;
  @override
  int get selectedRowCount => _selectedCount;
}
