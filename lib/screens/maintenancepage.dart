import 'package:centurion/common.dart';
import 'package:flutter/material.dart';

class Maintenancepage extends StatefulWidget {
  const Maintenancepage({Key key}) : super(key: key);

  @override
  _MaintenancepageState createState() => _MaintenancepageState();
}

class _MaintenancepageState extends State<Maintenancepage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          Maintainance(),
          Expanded(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Center(
                  child: Container(
                    height: MediaQuery.of(context).size.height * 0.6,
                    width: MediaQuery.of(context).size.width * 0.7,
                    child: FittedBox(
                      child: Image.asset(
                        'under_construction.png',
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
