import 'package:flutter/material.dart';
import '../config.dart';
import '../home.dart';

// ignore: camel_case_types
class failure extends StatefulWidget {
  final String value;

  const failure({Key key, this.value}) : super(key: key);
  @override
  _failureState createState() => _failureState();
}

// ignore: camel_case_types
class _failureState extends State<failure> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    // ignore: unused_local_variable
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;
    return LayoutBuilder(
      builder: (context, constraints) {
        if (constraints.maxWidth <= ScreenSize.iphone) {
          return Scaffold(
            backgroundColor: Colors.white,
            body: Center(
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: failure(
                  wdgtWidth: width,
                ),
              ),
            ),
          );
        } else if (constraints.maxWidth > ScreenSize.iphone &&
            constraints.maxWidth < ScreenSize.ipad) {
          return Scaffold(
            backgroundColor: Colors.white,
            body: Padding(
              padding: EdgeInsets.all(Pallet.leftPadding),
              child: failure(
                wdgtWidth: width,
              ),
            ),
          );
        } else {
          return Scaffold(
            backgroundColor: Colors.white,
            body: Center(
              child: Padding(
                padding: EdgeInsets.all(Pallet.leftPadding),
                child: failure(
                  wdgtWidth: width,
                ),
              ),
            ),
          );
        }
      },
    );
  }

  Widget failure({double wdgtWidth, double wdgtHeight}) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Image.asset(
          "oops1.gif",
          //color: Colors.white.withOpacity(0.1),
          height: MediaQuery.of(context).size.height * 0.3,
          width: MediaQuery.of(context).size.width * 0.3,
        ),
        SizedBox(height: 15),
        InkWell(
          onTap: () {
            Navigator.pushReplacement(
              context,
              MaterialPageRoute(
                  builder: (BuildContext context) => Home(
                        route: 'add_funding',
                      )),
            );
          },
          child: Text(
            "Payment failed Click here to continue....",
            style: TextStyle(
              color: Pallet.dashcontainerback,
              fontSize: 16,
            ),
          ),
        )
      ],
    );
  }
}
