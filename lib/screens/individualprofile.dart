import 'dart:async';
import 'dart:convert';
// ignore: avoid_web_libraries_in_flutter
import 'package:universal_html/html.dart' hide HttpRequest, Navigator;
import 'dart:typed_data';
import '../common.dart';
import '../config/app_settings.dart';
import '../services/business/account.dart';
import '../services/business/cart.dart';
import '../utils/utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/services.dart';
// import 'package:intl/intl.dart';
import '../config.dart';
import '../data.dart';
import '../services/communication/http/index.dart' show HttpRequest;
// import 'package:get/get.dart';

class IndividualProfile extends StatefulWidget {
  IndividualProfile({
    Key key,
    this.wdgtWidth,
    this.wdgtHeight,
    this.setData,
    this.manageaddressonly,
    this.bank,
  }) : super(key: key);
  final double wdgtWidth, wdgtHeight;
  final dynamic setData;
  final bool manageaddressonly;
  final bool bank;

  @override
  _IndividualProfileState createState() => _IndividualProfileState();
}

class _IndividualProfileState extends State<IndividualProfile> {
  Image file;
  Uint8List profileimgurl;
  FileReader reader = FileReader();

  String firstname = '';
  var lastname = '';
  String dob = '';
  String screeningname = '';
  String nationality = '';
  String addressLine1 = '';
  String kycstatus = '';
  String addressLine2 = '';
  var phoneNumber = '';
  String stateString = '';
  String country = '';
  var zipCode = '';
  String cityString = '';
  String profileid = '';
  String gender = '';
  String accid = '';
  String emailString = '';
  bool isMobile = false;
  int index;
  Map<String, dynamic> pageDetails = {};
  String profileurl = '';
  String isAdd;
  bool isRequired = false;
  // ignore: unused_field
  Future<dynamic> _temp;
  Future<dynamic> _temp1;
  bool editscrname = false;
  bool editphone = false;

  Map<String, dynamic> getRegionsMap = {
    'system_product_id': 1,
    'region_level': 2,
    'parent_region': 'null'
  };

  //
  //
  //
  FocusNode submit = FocusNode();

  bool isAccountEdit = false;
  String beneficiaryval = '';
  var accnumberval = '';
  String banknameval = '';
  String swiftval = '';

  TextEditingController beneficiaryController = TextEditingController();
  FocusNode beneficiaryNamenode = FocusNode();
  String beneficiaryError;

  FocusNode accnumbernode = FocusNode();
  String accnumberError;
  TextEditingController accnumController = TextEditingController();

  String banknameError;
  FocusNode banknamenode = FocusNode();
  TextEditingController banknamecontroller = TextEditingController();

  String swiftError;
  FocusNode swiftnode = FocusNode();
  TextEditingController swiftcontroller = TextEditingController();

  String scrnameerror;
  TextEditingController scrnamecontroller = TextEditingController();
  FocusNode scrnamenode = FocusNode();

  String phoneerror;
  TextEditingController phonecontroller = TextEditingController();
  FocusNode phonenode = FocusNode();

// streambuilder  controller declaration

  uploadImage() {
    InputElement uploadInput = FileUploadInputElement()..accept = 'image/*';
    uploadInput.click();
    uploadInput.onChange.listen((event) {
      final file = uploadInput.files.first;
      reader.readAsArrayBuffer(file);
      reader.onLoadEnd.listen((event) async {
        setState(() {
          profileimgurl = reader.result;
          // companycer = true;
        });
        var imagebase64 = base64.encode(profileimgurl);
        uploadimg(imagebase64);
      });
    });
  }

  uploadimg(imagebase64) async {
    Map<String, dynamic> _map = {
      "product_name": screeningname,
      "image_name": "profile.png",
      'is_main': true,
      "folder_name": screeningname,
      "file_base64": imagebase64,
    };

    var _result =
        await HttpRequest.Post('profileupload', Utils.constructPayload(_map));
    print('xxxxxxxxxxxxcssssssssssss');
    print(_map);
    print(_result);
    if (Utils.isServerError(_result)) {
      print(_result['error']);
      snack.snack(title: pageDetails['text16']);
      // final snackBar = SnackBar(
      //     content: MyText(text:
      //   pageDetails['text16'],
      // ));
      // ScaffoldMessenger.of(context).showSnackBar(snackBar);
    } else {
      var map = {
        "profile_id": 0,
        "account_id": int.parse(accid),
        "screen_name": 'null',
        "profile_image": _result['response']['data']['file_path'],
        "first_name": 'null',
        "phone_no": 'null',
        "last_name": 'null',
        "date_of_birth": 'null',
        "gender": 'null',
        "address_line1": 'null',
        "address_line2": 'null',
        "nationality": 'null',
        "country": 'null',
        "region": 'null',
        "city": 'null',
        "postal_code": 'null',
        "account_number": "null",
        "beneficiary": "null",
        "bank_name": "null",
        "bank_identification_code": "null",
      };

      var res = await cart.profiledetails(map);
      print(res);
      if (res == true) {
        snack.snack(title: pageDetails['text17']);

        // ScaffoldMessenger.of(context).showSnackBar(
        //   SnackBar(
        //     content: MyText(text:pageDetails['text17']),
        //   ),
        // );
        dashboard.dashboard();
      } else {
        setState(() {
          profileimgurl = null;
        });
        snack.snack(title: pageDetails['text23']);

        // ScaffoldMessenger.of(context).showSnackBar(
        //   SnackBar(
        //     content: MyText(text:"Profile Picture Updation Failed"),
        //   ),
        // );
      }
      // var val = _result['response']['data'];
    }
  }

  void initState() {
    widget.bank == true ? isAccountEdit = true : isAccountEdit = false;

    fetchapi();
    dashboard.dashboard();
    _temp1 = cart.getAddressInfo(setState);
    setPageDetails();
    initialcall();
    super.initState();
  }

  initialcall() async {
    setSelectedRadio(cart.selectRadio);
    await cart.getAddressInfo(setState);
  }

  refresh() async {
    // print('lssskdjddn');
    initialcall();
  }

  setSelectedRadio(int val) async {
    // print("dddddddddddddddddd");
    setState(() {
      cart.selectRadio = val;
    });
    cart.country.text = await cart.address[val]["address_name"];
    await cart.setDefaultAddress(cart.country.text);
    cart.myCart(setState, widget.setData);
    print("exit-1");
  }

  void setPageDetails() async {
    String data = await Utils.getPageDetails('individualprofile');
    setState(() {
      pageDetails = Utils.fromJSONString(data);
    });
  }

  StreamController _streamController = StreamController.broadcast();
  StreamSink get stateSink => _streamController.sink;
  Stream get stateStream => _streamController.stream;

  @override
  void dispose() {
    _streamController.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return widget.manageaddressonly == false || widget.manageaddressonly == null
        ? StreamBuilder(
            stream: stateStream,
            // ignore: missing_return
            builder: (context, snapshot) {
              if (snapshot.hasData) {
                print("1111111111111111111111");
                return LayoutBuilder(
                  builder: (context, constraints) {
                    if (constraints.maxWidth <= 600) {
                      isMobile = true;
                      return Padding(
                        padding: EdgeInsets.fromLTRB(Pallet.defaultPadding,
                            Pallet.defaultPadding, Pallet.defaultPadding, 0),
                        child: SingleChildScrollView(
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              Padding(
                                padding:
                                    EdgeInsets.only(top: Pallet.leftPadding),
                                child: profile(
                                  width: widget.wdgtWidth,
                                ),
                              ),
                              Padding(
                                padding:
                                    EdgeInsets.only(top: Pallet.leftPadding),
                                child: profileimg(
                                  width: widget.wdgtWidth * .74,
                                ),
                              ),
                              Padding(
                                padding:
                                    EdgeInsets.only(top: Pallet.leftPadding),
                                child: accountDetails(
                                  width: widget.wdgtWidth,
                                ),
                              ),
                              Padding(
                                padding:
                                    EdgeInsets.only(top: Pallet.leftPadding),
                                child: contact(
                                  width: widget.wdgtWidth,
                                ),
                              ),
                              Padding(
                                padding:
                                    EdgeInsets.only(top: Pallet.leftPadding),
                                child: accountDetail(
                                  width: widget.wdgtWidth,
                                ),
                              ),
                              Padding(
                                padding:
                                    EdgeInsets.only(top: Pallet.leftPadding),
                                child: checkoutaddress(
                                  width: widget.wdgtWidth,
                                ),
                              ),
                              SizedBox(
                                height: 30,
                              ),
                            ],
                          ),
                        ),
                      );
                    } else {
                      return Padding(
                        padding: EdgeInsets.symmetric(
                          horizontal: Pallet.leftPadding,
                        ),
                        child: SingleChildScrollView(
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              Padding(
                                padding:
                                    EdgeInsets.only(top: Pallet.leftPadding),
                                child: profile(
                                  width: widget.wdgtWidth * .74,
                                ),
                              ),

                              Padding(
                                padding:
                                    EdgeInsets.only(top: Pallet.leftPadding),
                                child: profileimg(
                                  width: widget.wdgtWidth * .74,
                                ),
                              ),
                              Padding(
                                padding:
                                    EdgeInsets.only(top: Pallet.leftPadding),
                                child: accountDetails(
                                  width: widget.wdgtWidth * .74,
                                ),
                              ),

                              // Padding(
                              //   padding: EdgeInsets.only(top: Pallet.leftPadding),
                              //   child: shareHolders(
                              //     width: widget.wdgtWidth * .74,
                              //   ),
                              // ),
                              Padding(
                                padding:
                                    EdgeInsets.only(top: Pallet.leftPadding),
                                child: contact(
                                  width: widget.wdgtWidth * .74,
                                ),
                              ),
                              // Padding(
                              //   padding: EdgeInsets.only(top: Pallet.leftPadding),
                              //   child: finalsubmit(
                              //     width: widget.wdgtWidth * .74,
                              //   ),
                              // ),
                              // SizedBox(
                              //   height: 30,
                              // ),

                              Padding(
                                padding:
                                    EdgeInsets.only(top: Pallet.leftPadding),
                                child: accountDetail(
                                  width: widget.wdgtWidth * .74,
                                ),
                              ),
                              Padding(
                                padding:
                                    EdgeInsets.only(top: Pallet.leftPadding),
                                child: checkoutaddress(
                                  width: widget.wdgtWidth * .74,
                                ),
                              ),
                              SizedBox(
                                height: 30,
                              ),
                            ],
                          ),
                        ),
                      );
                    }
                  },
                );
              } else if (snapshot.hasError) {
                print(snapshot.error);
                print("22222222222222222222222");

                return SomethingWentWrongMessage();
              } else {
                return Loader();
              }
            },
          )
        : FutureBuilder(
            future: _temp1,
            // ignore: missing_return
            builder: (context, snapshot) {
              if (snapshot.hasData) {
                return LayoutBuilder(
                  builder: (context, constraints) {
                    if (constraints.maxWidth <= 600) {
                      isMobile = true;
                      return Padding(
                        padding: EdgeInsets.fromLTRB(Pallet.defaultPadding,
                            Pallet.defaultPadding, Pallet.defaultPadding, 0),
                        child: SingleChildScrollView(
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              Padding(
                                padding:
                                    EdgeInsets.only(top: Pallet.leftPadding),
                                child: address(
                                  width: widget.wdgtWidth,
                                ),
                              ),
                              // Padding(
                              //   padding:
                              //       EdgeInsets.only(top: Pallet.leftPadding),
                              //   child: profileimg(
                              //     width: widget.wdgtWidth * .74,
                              //   ),
                              // ),
                              // Padding(
                              //   padding:
                              //       EdgeInsets.only(top: Pallet.leftPadding),
                              //   child: accountDetails(
                              //     width: widget.wdgtWidth,
                              //   ),
                              // ),
                              // Padding(
                              //   padding:
                              //       EdgeInsets.only(top: Pallet.leftPadding),
                              //   child: contact(
                              //     width: widget.wdgtWidth,
                              //   ),
                              // ),

                              Padding(
                                padding:
                                    EdgeInsets.only(top: Pallet.leftPadding),
                                child: checkoutaddress(
                                  width: widget.wdgtWidth,
                                ),
                              ),
                              SizedBox(
                                height: 30,
                              ),
                            ],
                          ),
                        ),
                      );
                    } else {
                      return Padding(
                        padding: EdgeInsets.symmetric(
                          horizontal: Pallet.leftPadding,
                        ),
                        child: SingleChildScrollView(
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              Padding(
                                padding:
                                    EdgeInsets.only(top: Pallet.leftPadding),
                                child: address(
                                  width: widget.wdgtWidth * .74,
                                ),
                              ),

                              // Padding(
                              //   padding:
                              //       EdgeInsets.only(top: Pallet.leftPadding),
                              //   child: profileimg(
                              //     width: widget.wdgtWidth * .74,
                              //   ),
                              // ),
                              // Padding(
                              //   padding:
                              //       EdgeInsets.only(top: Pallet.leftPadding),
                              //   child: accountDetails(
                              //     width: widget.wdgtWidth * .74,
                              //   ),
                              // ),

                              // // Padding(
                              // //   padding: EdgeInsets.only(top: Pallet.leftPadding),
                              // //   child: shareHolders(
                              // //     width: widget.wdgtWidth * .74,
                              // //   ),
                              // // ),
                              // Padding(
                              //   padding:
                              //       EdgeInsets.only(top: Pallet.leftPadding),
                              //   child: contact(
                              //     width: widget.wdgtWidth * .74,
                              //   ),
                              // ),
                              // // Padding(
                              // //   padding: EdgeInsets.only(top: Pallet.leftPadding),
                              // //   child: finalsubmit(
                              // //     width: widget.wdgtWidth * .74,
                              // //   ),
                              // // ),
                              // // SizedBox(
                              // //   height: 30,
                              // // ),
                              Padding(
                                padding:
                                    EdgeInsets.only(top: Pallet.leftPadding),
                                child: checkoutaddress(
                                  width: widget.wdgtWidth * .74,
                                ),
                              ),
                              SizedBox(
                                height: 30,
                              ),
                            ],
                          ),
                        ),
                      );
                    }
                  },
                );
              } else if (snapshot.hasError) {
                return SomethingWentWrongMessage();
              }
              return Loader();
            },
          );
  }

  Widget accountDetail({double width}) {
    return Container(
      // color: Colors.pink,
      width: width,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          isAccountEdit == false ? showAccountDetail() : editAccountDetail()
        ],
      ),
    );
  }

  Widget editAccountDetail({double width}) {
    return Container(
      // width: width - 50,
      width: width,
      padding: EdgeInsets.all(10),
      decoration: BoxDecoration(
        color: Colors.white,
        boxShadow: [
          BoxShadow(
            color: Colors.black12,
            blurRadius: 3,
            spreadRadius: 8,
          )
        ],
        borderRadius: BorderRadius.circular(10),
      ),
      child: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Row(
                children: [
                  MyText(
                      text: pageDetails['text24'], style: Pallet.profiletext),
                  SizedBox(width: 3),
                  MyText(
                      text: pageDetails['text25'],
                      style: TextStyle(
                        color: Pallet.fontcolornew,
                        fontSize: Pallet.heading4,
                        fontWeight: Pallet.font500,
                      )),
                ],
              ),
              // MyText(text:'Bank Account Details (Optional)',
              //     style: Pallet.profiletext),
              InkWell(
                onTap: () {
                  setState(() {
                    beneficiaryError = null;
                    banknameError = null;
                    accnumberError = null;
                    swiftError = null;

                    isAccountEdit = !isAccountEdit;
                    beneficiaryController.text = beneficiaryval;
                    accnumController.text = accnumberval;
                    banknamecontroller.text = banknameval;
                    swiftcontroller.text = swiftval;
                  });
                },
                child: MyText(
                  text: isAccountEdit == false
                      ? pageDetails['text26']
                      : pageDetails['text27'],
                  style: TextStyle(
                      color: Pallet.fontcolornew,
                      decoration: TextDecoration.underline),
                ),
              )
            ],
          ),
          SizedBox(
            height: 30,
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      SizedBox(
                        height: 20,
                      ),
                      Container(
                        width: isMobile == true ? 250 : 300,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            MyText(
                                text: pageDetails['text28'],
                                style: TextStyle(
                                    fontSize: Pallet.normalfont,
                                    color: Pallet.fontcolornew,
                                    fontWeight: Pallet.font500)),
                            SizedBox(height: 10),
                            SignupTextBox(
                                tabpress: (event) {
                                  if (event
                                      .isKeyPressed(LogicalKeyboardKey.tab)) {
                                    FocusScope.of(context)
                                        .requestFocus(accnumbernode);
                                  }
                                },
                                regexallow: [
                                  FilteringTextInputFormatter.allow(
                                      RegExp(r'[a-zA-Z ]'))
                                ],
                                autofocus: widget.bank == true ? true : false,
                                errorText: beneficiaryError,
                                focusnode: beneficiaryNamenode,
                                onsubmit: () {
                                  FocusScope.of(context)
                                      .requestFocus(accnumbernode);
                                },
                                isPassword: false,
                                digitsOnlyPhone: false,
                                controller: beneficiaryController,
                                validation: (value) {
                                  setState(() {
                                    if (value.trim().isEmpty) {
                                      beneficiaryError = pageDetails['text22'];
                                    } else {
                                      beneficiaryError = null;
                                    }
                                  });
                                }),
                          ],
                        ),
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      Container(
                        width: isMobile == true ? 250 : 300,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            MyText(
                                text: pageDetails['text29'],
                                style: TextStyle(
                                  fontSize: Pallet.normalfont,
                                  color: Pallet.fontcolornew,
                                  fontWeight: Pallet.font500,
                                )),
                            SizedBox(height: 5),
                            SignupTextBox(
                                tabpress: (event) {
                                  if (event
                                      .isKeyPressed(LogicalKeyboardKey.tab)) {
                                    FocusScope.of(context)
                                        .requestFocus(banknamenode);
                                  }
                                },
                                regexallow: [
                                  FilteringTextInputFormatter.allow(
                                      RegExp(r'[0-9a-zA-Z]'))
                                ],
                                focusnode: accnumbernode,
                                onsubmit: () {
                                  FocusScope.of(context)
                                      .requestFocus(banknamenode);
                                },
                                errorText: accnumberError,
                                isPassword: false,
                                digitsOnlyPhone: false,
                                controller: accnumController,
                                validation: (value) {
                                  setState(() {
                                    if (value.trim().isEmpty) {
                                      accnumberError = pageDetails['text22'];
                                    } else {
                                      accnumberError = null;
                                    }
                                  });
                                }),
                          ],
                        ),
                      ),
                      isMobile == true
                          ? detailsOfEditAccountDetails()
                          : Container()
                    ],
                  ),
                  isMobile == false
                      ? detailsOfEditAccountDetails()
                      : Container(),
                ],
              ),
            ],
          ),
          SizedBox(height: 30),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              // ignore: deprecated_member_use

              // ignore: deprecated_member_use
              RaisedButton(
                color: Colors.grey[300],
                child: MyText(
                    text: pageDetails['text27'],
                    style: TextStyle(
                      color: Pallet.fontcolornew,
                    )),
                onPressed: () {
                  setState(() {
                    beneficiaryError = null;
                    banknameError = null;
                    accnumberError = null;
                    swiftError = null;
                    isAccountEdit = false;
                  });
                },
              ),
              // ignore: deprecated_member_use
              RaisedButton(
                focusNode: submit,
                child: Center(
                    child: MyText(
                  text: pageDetails['text30'],
                  style: TextStyle(color: Pallet.fontcolor),
                )),
                onPressed: () async {
                  if (beneficiaryController.text.trim().isEmpty ||
                      accnumController.text.trim().isEmpty ||
                      banknamecontroller.text.trim().isEmpty ||
                      swiftcontroller.text.trim().isEmpty) {
                    if (beneficiaryController.text.trim().length < 1) {
                      setState(() {
                        beneficiaryError = pageDetails['text22'];
                      });
                    }
                    if (accnumController.text.length < 1) {
                      setState(() {
                        accnumberError = pageDetails['text22'];
                      });
                    }
                    if (banknamecontroller.text.length < 1) {
                      setState(() {
                        banknameError = pageDetails['text22'];
                      });
                    }
                    if (swiftcontroller.text.length < 1) {
                      setState(() {
                        swiftError = pageDetails['text22'];
                      });
                    }
                  } else {
                    if (beneficiaryError == null &&
                        accnumberError == null &&
                        banknameError == null &&
                        swiftError == null) {
                      var map = {
                        "profile_id": int.parse(profileid),
                        "account_id": 0,
                        "screen_name": 'null',
                        "profile_image": 'null',
                        "phone_no": 'null',
                        "first_name": 'null',
                        "last_name": 'null',
                        "date_of_birth": 'null',
                        "gender": 'null',
                        "address_line1": 'null',
                        "address_line2": 'null',
                        "nationality": 'null',
                        "country": 'null',
                        "region": 'null',
                        "city": 'null',
                        "postal_code": 'null',
                        'beneficiary': beneficiaryController.text,
                        'account_number': accnumController.text,
                        'bank_name': banknamecontroller.text,
                        'bank_identification_code': swiftcontroller.text,
                        // 'beneficiary':
                        //     beneficiaryController.text.trim().isEmpty == true
                        //         ? 'null'
                        //         : beneficiaryController.text,
                        // 'account_number':
                        //     accnumController.text.trim().isEmpty == true
                        //         ? 'null'
                        //         : accnumController.text,
                        // 'bank_name': banknamecontroller.text.trim().isEmpty == true
                        //     ? 'null'
                        //     : banknamecontroller.text,
                        // 'bank_identification_code':
                        //     swiftcontroller.text.trim().isEmpty == true
                        //         ? 'null'
                        //         : swiftcontroller.text,
                      };
                      var result = await cart.profiledetails(map);
                      if (result == true) {
                        setState(() {
                          beneficiaryval = beneficiaryController.text;
                          accnumberval = accnumController.text;
                          banknameval = banknamecontroller.text;
                          swiftval = swiftcontroller.text;
                          isAccountEdit = false;
                        });
                        snack.snack(title: pageDetails['text31']);

                        // ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                        //   content: MyText(text:"Account detail Updation Success"),
                        // ));
                      } else {
                        snack.snack(title: pageDetails['text32']);

                        // ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                        //   content: MyText(text:"Account detail Updation Failed"),
                        // ));
                      }
                    } else {
                      snack.snack(title: pageDetails['text33']);
                    }
                  }
                },
                color: Pallet.fontcolornew,
              )
            ],
          ),
          SizedBox(height: 30),
        ],
      ),
    );
  }

  Column detailsOfEditAccountDetails() {
    return Column(
      mainAxisAlignment: MainAxisAlignment.end,
      children: [
        SizedBox(
          height: 20,
        ),
        Container(
          width: isMobile == true ? 250 : 300,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              MyText(
                  text: pageDetails['text34'],
                  style: TextStyle(
                      fontSize: Pallet.normalfont,
                      color: Pallet.fontcolornew,
                      fontWeight: Pallet.font500)),
              SizedBox(height: 10),
              SignupTextBox(
                  tabpress: (event) {
                    if (event.isKeyPressed(LogicalKeyboardKey.tab)) {
                      FocusScope.of(context).requestFocus(swiftnode);
                    }
                  },
                  focusnode: banknamenode,
                  onsubmit: () {
                    FocusScope.of(context).requestFocus(swiftnode);
                  },
                  regexallow: [
                    FilteringTextInputFormatter.allow(RegExp(r'[a-zA-Z0-9 ]'))
                  ],
                  errorText: banknameError,
                  isPassword: false,
                  digitsOnlyPhone: false,
                  controller: banknamecontroller,
                  validation: (value) {
                    setState(() {
                      if (value.trim().isEmpty) {
                        banknameError = pageDetails['text22'];
                      } else {
                        banknameError = null;
                      }
                    });
                  }),
            ],
          ),
        ),
        SizedBox(
          height: 20,
        ),
        Container(
          width: isMobile == true ? 250 : 300,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              MyText(
                  text: pageDetails['text35'],
                  style: TextStyle(
                    fontSize: Pallet.normalfont,
                    color: Pallet.fontcolornew,
                    fontWeight: Pallet.font500,
                  )),
              SizedBox(height: 5),
              SignupTextBox(
                  tabpress: (event) {
                    if (event.isKeyPressed(LogicalKeyboardKey.tab)) {
                      FocusScope.of(context).requestFocus(submit);
                    }
                  },
                  focusnode: swiftnode,
                  onsubmit: () {
                    FocusScope.of(context).requestFocus(submit);
                  },
                  regexallow: [
                    FilteringTextInputFormatter.allow(RegExp(r'[0-9a-zA-Z]'))
                  ],
                  errorText: swiftError,
                  isPassword: false,
                  // digitsOnlyPhone: false,
                  controller: swiftcontroller,
                  validation: (value) {
                    setState(() {
                      if (value.trim().isEmpty) {
                        swiftError = pageDetails['text22'];
                      } else {
                        swiftError = null;
                      }
                    });
                  }),
            ],
          ),
        ),
      ],
    );
  }

  Widget showAccountDetail({double width}) {
    return Container(
      width: width,
      padding: EdgeInsets.all(10),
      decoration: BoxDecoration(
        color: Colors.white,
        boxShadow: [
          BoxShadow(
            color: Colors.black12,
            blurRadius: 3,
            spreadRadius: 8,
          )
        ],
        borderRadius: BorderRadius.circular(10),
      ),
      child: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              // RichText(
              //   text: TextSpan(
              //     text: 'Bank Account Details ',
              //     style: TextStyle(
              //       color: Pallet.fontcolornew,
              //       fontSize: Pallet.heading3,
              //       fontWeight: Pallet.heading1wgt,
              //     ),
              //     children: <TextSpan>[
              //       TextSpan(
              //           text: '(Optional)',
              //           style: TextStyle(
              //             color: Pallet.fontcolornew,
              //             fontSize: Pallet.heading4,
              //           )),
              //     ],
              //   ),
              // ),

              Row(
                children: [
                  MyText(
                      text: pageDetails['text24'], style: Pallet.profiletext),
                  SizedBox(width: 3),
                  MyText(
                      text: pageDetails['text25'],
                      style: TextStyle(
                        color: Pallet.fontcolornew,
                        fontSize: Pallet.heading4,
                        fontWeight: Pallet.font500,
                      )),
                ],
              ),
              InkWell(
                onTap: () {
                  setState(() {
                    isAccountEdit = !isAccountEdit;
                    beneficiaryController.text =
                        beneficiaryval == pageDetails['text42']
                            ? ''
                            : beneficiaryval;
                    accnumController.text =
                        accnumberval == pageDetails['text42']
                            ? ''
                            : accnumberval;
                    banknamecontroller.text =
                        banknameval == pageDetails['text42'] ? '' : banknameval;
                    swiftcontroller.text =
                        swiftval == pageDetails['text42'] ? '' : swiftval;
                    // beneficiaryController.text = beneficiaryval;
                    // accnumController.text = accnumberval;
                    // banknamecontroller.text = banknameval;
                    // swiftcontroller.text = swiftval;
                  });
                },
                child: MyText(
                  text: isAccountEdit == false
                      ? pageDetails['text26']
                      : pageDetails['text27'],
                  style: TextStyle(
                      color: Pallet.fontcolornew,
                      decoration: TextDecoration.underline),
                ),
              )
            ],
          ),
          SizedBox(
            height: 30,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  // RichText(
                  //   text: TextSpan(
                  //     text: 'Name of beneficiary ',
                  //     style: TextStyle(
                  //       color: Pallet.fontcolornew,
                  //       fontSize: Pallet.heading3,
                  //     ),
                  //     children: <TextSpan>[
                  //       TextSpan(
                  //           text: '*',
                  //           style: TextStyle(
                  //             color: Pallet.failed,
                  //             fontWeight: Pallet.heading1wgt,
                  //           )),
                  //     ],
                  //   ),
                  // ),
                  MyText(
                    text: pageDetails['text28'],
                    style: TextStyle(
                      color: Pallet.fontcolornew,
                    ),
                  ),
                  SizedBox(
                    height: 5,
                  ),
                  MyText(
                    text: beneficiaryval,
                    style: TextStyle(
                      color: Pallet.fontcolornew,
                      fontWeight: Pallet.font600,
                      fontSize: Pallet.heading3,
                    ),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  MyText(
                    text: pageDetails['text29'],
                    style: TextStyle(
                      color: Pallet.fontcolornew,
                    ),
                  ),
                  SizedBox(
                    height: 5,
                  ),
                  MyText(
                    text: accnumberval,
                    style: TextStyle(
                      fontWeight: Pallet.font600,
                      color: Pallet.fontcolornew,
                      fontSize: Pallet.heading3,
                    ),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  isMobile == true
                      ? accountDetailsOfShowAccountDetail()
                      : Container()
                ],
              ),
              isMobile == false
                  ? accountDetailsOfShowAccountDetail()
                  : Container()
            ],
          ),
        ],
      ),
    );
  }

  Widget accountDetailsOfShowAccountDetail() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        MyText(
          text: pageDetails['text34'],
          style: TextStyle(
            color: Pallet.fontcolornew,
          ),
        ),
        SizedBox(
          height: 5,
        ),
        MyText(
          text: banknameval,
          style: TextStyle(
            fontWeight: Pallet.font600,
            color: Pallet.fontcolornew,
            fontSize: Pallet.heading3,
          ),
        ),
        SizedBox(
          height: 20,
        ),
        MyText(
          text: pageDetails['text35'],
          style: TextStyle(
            color: Pallet.fontcolornew,
          ),
        ),
        SizedBox(
          height: 5,
        ),
        MyText(
          text: swiftval,
          style: TextStyle(
            fontWeight: Pallet.font600,
            color: Pallet.fontcolornew,
            fontSize: Pallet.heading3,
          ),
        ),
        SizedBox(
          height: 20,
        ),
        // MyText(text:
        //   'Referal Link',
        //   style: TextStyle(
        //     color: Pallet.fontcolornew,
        //   ),
        // ),
        // SizedBox(
        //   height: 5,
        // ),
        // MyText(text:
        //   'test.ducatus.net/register/ccompany',
        //   style: TextStyle(
        //     fontWeight: Pallet.font600,
        //     color: Pallet.fontcolornew,
        //     fontSize: Pallet.heading3,
        //   ),
        // ),
      ],
    );
  }

  Widget profile({double width}) {
    return Container(
      width: width,
      child: MyText(
        text: pageDetails['text1'],
        style: TextStyle(
          color: Pallet.fontcolornew,
          fontSize: Pallet.heading1,
        ),
      ),
    );
  }

  Widget address({double width}) {
    return Container(
      width: width,
      child: MyText(
        text: pageDetails['text19'],
        style: TextStyle(
            color: Pallet.fontcolornew,
            fontSize: Pallet.heading1,
            fontWeight: Pallet.bold),
      ),
    );
  }

  Widget profileimg({double width}) {
    return Container(
      // color: Colors.pink,
      width: width,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Row(
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Container(
                // width: 100,
                child: Stack(
                  children: [
                    MouseRegion(
                      cursor: SystemMouseCursors.click,
                      child: GestureDetector(
                        onTap: () {
                          showDialog(
                              barrierDismissible: true,
                              // barrierColor: Colors.transparent,
                              context: context,
                              builder: (BuildContext context) {
                                return Hero(
                                  tag: 'profile',
                                  child: GestureDetector(
                                    onTap: () {
                                      Navigator.pop(context);
                                    },
                                    child: CircleAvatar(
                                      backgroundColor: Colors.transparent,
                                      child: Container(
                                        height: isMobile == true
                                            ? MediaQuery.of(context)
                                                    .size
                                                    .width /
                                                1.5
                                            : MediaQuery.of(context)
                                                    .size
                                                    .height /
                                                2,
                                        child: ClipOval(
                                          child: profileimgurl != null
                                              ? Image.memory(profileimgurl)
                                              : profileurl == null
                                                  ? Image.asset(
                                                      'temp-profile.png')
                                                  : Image.network(
                                                      appSettings[
                                                              'SERVER_URL'] +
                                                          '/' +
                                                          profileurl,
                                                      fit: BoxFit.fill,
                                                    ),
                                        ),
                                      ),
                                    ),
                                  ),
                                );
                                // Container(
                                //   decoration: BoxDecoration(
                                //     shape: BoxShape.circle,
                                //   ),
                                //   height: 150,
                                //   width: 130,
                                //   child: Image.network(
                                //     appSettings['SERVER_URL'] + '/' + profileurl,
                                //     fit: BoxFit.fill,
                                //   ),
                                // );
                              });
                          // setState(() {
                          //   zoomprofile = true;
                          // });
                        },
                        child: Hero(
                          tag: 'profile',
                          child: CircleAvatar(
                            radius: 35,
                            backgroundImage: profileimgurl != null
                                ? MemoryImage(profileimgurl)
                                : profileurl == null
                                    ? AssetImage('temp-profile.png')
                                    : NetworkImage(
                                        appSettings['SERVER_URL'] +
                                            '/' +
                                            profileurl,
                                      ),
                          ),
                        ),
                      ),
                    ),
                    Positioned(
                      top: -1.0,
                      right: -1.0,
                      child: MouseRegion(
                        cursor: SystemMouseCursors.click,
                        child: GestureDetector(
                          onTap: () {
                            uploadImage();
                          },
                          child: Container(
                            padding: EdgeInsets.all(2),
                            decoration: BoxDecoration(
                              shape: BoxShape.circle,
                              color: Colors.grey,
                            ),
                            child: Icon(
                              Icons.edit,
                              size: 20,
                              color: Colors.black,
                            ),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
          SizedBox(
            height: 10,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Container(
                child: editscrname == false
                    ? Container(
                        child: Row(
                          children: [
                            MyText(
                              text: screeningname,
                              style: TextStyle(
                                fontWeight: Pallet.font600,
                                color: Pallet.fontcolornew,
                                fontSize: Pallet.heading3,
                              ),
                            ),
                            SizedBox(
                              width: 5,
                            ),
                            IconButton(
                                icon: Icon(Icons.edit, color: Colors.black),
                                onPressed: () {
                                  setState(() {
                                    scrnamecontroller.text = screeningname;
                                    editscrname = true;
                                  });
                                })
                          ],
                        ),
                      )
                    : Container(
                        child: Row(
                          children: [
                            Container(
                              width: 250,
                              child: SignupTextBox(
                                  errorText: scrnameerror,
                                  focusnode: scrnamenode,
                                  isPassword: false,
                                  digitsOnlyPhone: false,
                                  controller: scrnamecontroller,
                                  validation: (value) {
                                    setState(() {
                                      checkUsername(value);
                                    });
                                  }),
                            ),
                            SizedBox(
                              width: 5,
                            ),
                            Container(
                              padding: EdgeInsets.all(3),
                              decoration: BoxDecoration(
                                color: Colors.green,
                                shape: BoxShape.circle,
                              ),
                              child: IconButton(
                                icon: Icon(
                                  Icons.save,
                                  color: Pallet.fontcolor,
                                ),
                                onPressed: () async {
                                  if (scrnamecontroller.text.isEmpty ||
                                      scrnamecontroller.text.trim() == '') {
                                    setState(() {
                                      scrnameerror = pageDetails['text22'];
                                    });
                                  } else if (scrnameerror == null) {
                                    setState(() {
                                      scrnameerror = null;
                                    });
                                    // var map = {
                                    //   "profile_id": 0,
                                    //   "account_id": int.parse(accid),
                                    //   "screen_name": scrnamecontroller.text,
                                    //   "profile_image": null,
                                    //   "first_name": null,
                                    //   "last_name": null,
                                    //   "date_of_birth": null,
                                    //   "gender": null,
                                    //   "address_line1": null,
                                    //   "address_line2": null,
                                    //   "nationality": null,
                                    //   "country": null,
                                    //   "region": null,
                                    //   "city": null,
                                    //   "postal_code": null,
                                    // };
                                    var map = {
                                      "profile_id": 0,
                                      "account_id": int.parse(accid),
                                      "screen_name": scrnamecontroller.text,
                                      "phone_no": 'null',
                                      "profile_image": 'null',
                                      "first_name": 'null',
                                      "last_name": 'null',
                                      "date_of_birth": 'null',
                                      "gender": 'null',
                                      "address_line1": 'null',
                                      "address_line2": 'null',
                                      "nationality": 'null',
                                      "country": 'null',
                                      "region": 'null',
                                      "city": 'null',
                                      "postal_code": 'null',
                                      "account_number": "null",
                                      "beneficiary": "null",
                                      "bank_name": "null",
                                      "bank_identification_code": "null",
                                    };

                                    var res = await cart.profiledetails(map);
                                    print(res);
                                    if (res == true) {
                                      snack.snack(title: pageDetails['text20']);
                                      // Get.snackbar('', '',
                                      //     colorText: Pallet.fontcolor,
                                      //     maxWidth: 600,
                                      //     boxShadows: [Pallet.shadowEffect],
                                      //     titleText: MyText(text:
                                      //       pageDetails['text20'],
                                      //       textAlign: TextAlign.center,
                                      //       style: TextStyle(
                                      //         color: Pallet.fontcolor,
                                      //       ),
                                      //     ),
                                      //     backgroundColor: Pallet.snackback,
                                      //     snackPosition: SnackPosition.TOP);

                                      dashboard.dashboard();
                                    } else {
                                      snack.snack(title: pageDetails['text21']);
                                    }
                                    setState(() {
                                      screeningname = scrnamecontroller.text;
                                      editscrname = false;
                                    });
                                  } else {}
                                },
                              ),
                            ),
                          ],
                        ),
                      ),
              ),
            ],
          )
        ],
      ),
    );
  }

  Widget contact({double width}) {
    return Padding(
      padding: const EdgeInsets.only(
        right: 8,
        left: 8,
        bottom: 10,
      ),
      child: Container(
        // color: Colors.pink,xx
        padding: EdgeInsets.all(10),
        decoration: BoxDecoration(
          color: Colors.white,
          boxShadow: [
            BoxShadow(
              color: Colors.black12,
              blurRadius: 3,
              spreadRadius: 8,
            )
          ],
          borderRadius: BorderRadius.circular(10),
        ),
        width: width,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                MyText(
                  text: pageDetails['text2'],
                  style: Pallet.profiletext,
                ),
                // InkWell(
                //   onTap: () {
                //     setState(() {
                //       isContactEdit = !isContactEdit;
                //       enablecity = false;
                //       enablezipcode = false;
                //       addressline1controller.text = addressLine1;
                //       addressline2controller.text = addressLine2;
                //       mobilecontroller.text = phoneNumber;
                //       zipcontroller.text = zipCode;
                //       dropDownCountry = country;
                //       emailcontroller.text = emailString;
                //       statecontroller.text = stateString;
                //       citycontroller.text = cityString;
                //     });
                //   },
                //   child: MyText(text:
                //     isContactEdit == false ? "Edit" : "Cancel",
                //     style: TextStyle(
                //         color: Pallet.fontcolornew,
                //         decoration: TextDecoration.underline),
                //   ),
                // )
              ],
            ),

            showContactDetails(),
            // isContactEdit == false ? showContactDetails() : contactForm(width)
          ],
        ),
      ),
    );
  }

  Widget accountDetails({double width}) {
    return Container(
      // color: Colors.pink,
      width: width,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          showAccountDetails(),
          // isAccountEdit == false ? showAccountDetails() : editAccountDetails()
        ],
      ),
    );
  }

  Widget checkoutaddress({double width}) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 8),
      child: Container(
        width: width,
        padding: EdgeInsets.all(10),
        decoration: BoxDecoration(
          color: Colors.white,
          boxShadow: [
            BoxShadow(
              color: Colors.black12,
              blurRadius: 3,
              spreadRadius: 8,
            )
          ],
          borderRadius: BorderRadius.circular(10),
        ),
        // height: width / 2,

        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                MyText(
                  text: pageDetails['text18'],
                  style: Pallet.profiletext,
                ),
                Row(
                  children: [
                    Icon(
                      Icons.add,
                      color: Pallet.dashcontainerback,
                    ),
                    SizedBox(width: 5),
                    InkWell(
                      onTap: () {
                        isAdd = 'true';
                        isRequired = false;
                        print('22222222222222222222222222222');

                        showDialog(
                            barrierDismissible: false,
                            context: context,
                            builder: (BuildContext context) {
                              return AddressProcessPopUp(
                                isAdd: isAdd,
                                // index: 0,
                                setData: setState,
                                shopContext: context,
                              );
                            });
                      },
                      child: MyText(
                          text: pageDetails['text36'],
                          style: TextStyle(
                              fontSize: Pallet.normalfont,
                              color: Pallet.fontcolornew,
                              fontWeight: Pallet.font500)),
                    ),
                  ],
                ),
              ],
            ),
            for (var index = 0; index < cart.address.length; index++)
              Padding(
                padding: const EdgeInsets.symmetric(
                  vertical: 5,
                  horizontal: 15,
                ),
                child: Container(
                  padding: EdgeInsets.all(Pallet.defaultPadding / 2),
                  decoration: BoxDecoration(
                      border: Border.all(color: Colors.grey[300]),
                      borderRadius: BorderRadius.circular(Pallet.radius),
                      color: Colors.grey[300]),
                  // height: 100,
                  margin: EdgeInsets.all(2),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      MyText(
                        text: cart.address[index]["address_name"],
                      ),
                      MyText(text: cart.address[index]["address_line1"]),
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.end,
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: [
                          Padding(
                            padding: EdgeInsets.symmetric(
                                horizontal: Pallet.defaultPadding),
                            child: IconButton(
                              icon: Icon(Icons.delete,
                                  color: Pallet.fontcolornew),
                              onPressed: () {
                                setState(() {
                                  Map<String, dynamic> _map = {
                                    "product_id": 1,
                                    "address_id": cart.address[index]
                                        ["address_id"],
                                    "address_name": "null",
                                    "address_line1": "null",
                                    "address_line2": "null",
                                    "country": "null",
                                    "region": "null",
                                    "city": "null",
                                    "postal_code": "null",
                                    "address_phone": "null",
                                    "delete": true
                                  };
                                  cart.deleteAddress(_map, setState);
                                });
                                setState(() {
                                  cart.myCart(setState, widget.setData);
                                });
                              },
                            ),
                          ),
                          Padding(
                            padding: EdgeInsets.symmetric(
                                horizontal: Pallet.defaultPadding),
                            child: IconButton(
                              icon:
                                  Icon(Icons.edit, color: Pallet.fontcolornew),
                              onPressed: () async {
                                isAdd = 'false';

                                await showDialog(
                                    barrierDismissible: false,
                                    context: context,
                                    builder: (BuildContext context) {
                                      return AddressProcessPopUp(
                                        isAdd: isAdd,
                                        index: index,
                                        setData: setState,
                                        shopContext: context,
                                        notifyParent: refresh,
                                      );
                                    });
                              },
                            ),
                          ),
                          Radio(
                            value: index,
                            activeColor: Pallet.fontcolornew,
                            // fillColor: Colors.green,
                            groupValue: cart.selectRadio,
                            onChanged: (int val) {
                              print("xccccccxxxxzzz");
                              setState(() {
                                setSelectedRadio(val);
                              });
                              print("bzvsfdkdefgd");
                            },
                          )
                        ],
                      ),
                    ],
                  ),
                ),
              ),
            SizedBox(
              height: 30,
            ),
          ],
        ),
      ),
    );
  }

  Widget showContactDetails() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: [
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            MyText(
              text: pageDetails['text3'],
              style: TextStyle(
                color: Pallet.fontcolornew,
              ),
            ),
            SizedBox(
              height: 5,
            ),
            MyText(
              text: addressLine1,
              style: TextStyle(
                color: Pallet.fontcolornew,
                fontWeight: Pallet.font600,
                fontSize: Pallet.heading3,
              ),
            ),
            SizedBox(
              height: 20,
            ),
            // MyText(text:
            //   'State *',
            //   style: TextStyle(
            //     color: Pallet.fontcolornew,
            //   ),
            // ),
            // SizedBox(
            //   height: 5,
            // ),
            // MyText(text:
            //   stateString,
            //   style: TextStyle(
            //     color: Pallet.fontcolornew,
            //     fontSize: Pallet.heading3,
            //   ),
            // ),
            // SizedBox(
            //   height: 20,
            // ),
            MyText(
              text: pageDetails['text4'],
              style: TextStyle(
                color: Pallet.fontcolornew,
              ),
            ),
            SizedBox(
              height: 5,
            ),
            MyText(
              text: country.substring(0, 1).toUpperCase() +
                  country.substring(1).toLowerCase(),
              style: TextStyle(
                color: Pallet.fontcolornew,
                fontWeight: Pallet.font600,
                fontSize: Pallet.heading3,
              ),
            ),
            SizedBox(
              height: 20,
            ),
            MyText(
              text: pageDetails['text5'],
              style: TextStyle(
                color: Pallet.fontcolornew,
              ),
            ),
            SizedBox(
              height: 5,
            ),
            //phone number editable
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Container(
                  child: editphone == false
                      ? Container(
                          child: Row(
                            children: [
                              MyText(
                                text: phoneNumber,
                                style: TextStyle(
                                  fontWeight: Pallet.font600,
                                  color: Pallet.fontcolornew,
                                  fontSize: Pallet.heading3,
                                ),
                              ),
                              SizedBox(
                                width: 5,
                              ),
                              IconButton(
                                  icon: Icon(Icons.edit, color: Colors.black),
                                  onPressed: () {
                                    setState(() {
                                      phonecontroller.text =
                                          phoneNumber == pageDetails['text42']
                                              ? ''
                                              : phoneNumber;
                                      editphone = true;
                                    });
                                  })
                            ],
                          ),
                        )
                      : Container(
                          child: Row(
                            children: [
                              Container(
                                width: 250,
                                child: SignupTextBox(
                                    errorText: phoneerror,
                                    focusnode: phonenode,
                                    isPassword: false,
                                    digitsOnlyPhone: true,
                                    regexallow: [
                                      FilteringTextInputFormatter.allow(
                                          RegExp('[0-9.]'))
                                    ],
                                    controller: phonecontroller,
                                    validation: (value) {
                                      setState(() {
                                        if (value.trim().isEmpty) {
                                          phoneerror = pageDetails['text22'];
                                        } else {
                                          phoneerror = null;
                                        }
                                      });
                                    }),
                              ),
                              SizedBox(
                                width: 5,
                              ),
                              Container(
                                padding: EdgeInsets.all(3),
                                decoration: BoxDecoration(
                                  color: Colors.green,
                                  shape: BoxShape.circle,
                                ),
                                child: IconButton(
                                  icon: Icon(
                                    Icons.save,
                                    color: Pallet.fontcolor,
                                  ),
                                  onPressed: () async {
                                    if (phonecontroller.text.isEmpty ||
                                        phonecontroller.text.trim() == '') {
                                      setState(() {
                                        phoneerror = pageDetails['text22'];
                                      });
                                    } else if (phoneerror == null) {
                                      setState(() {
                                        phoneerror = null;
                                      });
                                      // var map = {
                                      //   "profile_id": 0,
                                      //   "account_id": int.parse(accid),
                                      //   "screen_name": scrnamecontroller.text,
                                      //   "profile_image": null,
                                      //   "first_name": null,
                                      //   "last_name": null,
                                      //   "date_of_birth": null,
                                      //   "gender": null,
                                      //   "address_line1": null,
                                      //   "address_line2": null,
                                      //   "nationality": null,
                                      //   "country": null,
                                      //   "region": null,
                                      //   "city": null,
                                      //   "postal_code": null,
                                      // };
                                      var map = {
                                        "profile_id": int.parse(profileid),
                                        "account_id": 0,
                                        "screen_name": 'null',
                                        "phone_no": phonecontroller.text,
                                        "profile_image": 'null',
                                        "first_name": 'null',
                                        "last_name": 'null',
                                        "date_of_birth": 'null',
                                        "gender": 'null',
                                        "address_line1": 'null',
                                        "address_line2": 'null',
                                        "nationality": 'null',
                                        "country": 'null',
                                        "region": 'null',
                                        "city": 'null',
                                        "postal_code": 'null',
                                        "account_number": "null",
                                        "beneficiary": "null",
                                        "bank_name": "null",
                                        "bank_identification_code": "null",
                                      };

                                      var res = await cart.profiledetails(map);
                                      print(res);
                                      if (res == true) {
                                        snack.snack(
                                            title: pageDetails['text37']);
                                        // Get.snackbar('', '',
                                        //     colorText: Pallet.fontcolor,
                                        //     maxWidth: 600,
                                        //     boxShadows: [Pallet.shadowEffect],
                                        //     titleText: MyText(text:
                                        //       pageDetails['text20'],
                                        //       textAlign: TextAlign.center,
                                        //       style: TextStyle(
                                        //         color: Pallet.fontcolor,
                                        //       ),
                                        //     ),
                                        //     backgroundColor: Pallet.snackback,
                                        //     snackPosition: SnackPosition.TOP);

                                        dashboard.dashboard();
                                      } else {
                                        snack.snack(
                                            title: pageDetails['text38']);
                                      }
                                      setState(() {
                                        phoneNumber = phonecontroller.text;
                                        editphone = false;
                                      });
                                    } else {}
                                  },
                                ),
                              ),
                            ],
                          ),
                        ),
                ),
              ],
            ),
            // MyText(text:
            //   phoneNumber,
            //   style: TextStyle(
            //     color: Pallet.fontcolornew,
            //     fontWeight: Pallet.font600,
            //     fontSize: Pallet.heading3,
            //   ),
            // ),
            isMobile == true
                ? contactDetailsOfShowContactDetails()
                : Container()
          ],
        ),
        isMobile == false ? contactDetailsOfShowContactDetails() : Container()
      ],
    );
  }

  Widget showAccountDetails({double width}) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 8),
      child: Container(
        padding: EdgeInsets.symmetric(
          horizontal: 15,
          vertical: 10,
        ),
        decoration: BoxDecoration(
          color: Colors.white,
          boxShadow: [
            BoxShadow(
              color: Colors.black12,
              blurRadius: 3,
              spreadRadius: 8,
            )
          ],
          borderRadius: BorderRadius.circular(10),
        ),
        child: Column(
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                MyText(
                  text: pageDetails['text6'],
                  style: Pallet.profiletext,
                ),
                // InkWell(
                //   onTap: () {
                //     setState(() {
                //       isAccountEdit = !isAccountEdit;
                //       companyController.text = firstname;
                //       taxnumController.text = lastname;
                //       dateofIncorpcontroller.text = dob;
                //       fieldofbuisnesscontroller.text = phonenumber;
                //     });
                //   },
                //   child: MyText(text:
                //     isAccountEdit == false ? "Edit" : "Cancel",
                //     style: TextStyle(
                //         color: Pallet.fontcolornew,
                //         decoration: TextDecoration.underline),
                //   ),
                // )
              ],
            ),
            SizedBox(
              height: 30,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    MyText(
                      text: pageDetails['text7'],
                      style: TextStyle(
                        color: Pallet.fontcolornew,
                      ),
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    MyText(
                      text: screeningname,
                      style: TextStyle(
                        fontWeight: Pallet.font600,
                        color: Pallet.fontcolornew,
                        fontSize: Pallet.heading3,
                      ),
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    MyText(
                      text: pageDetails['text8'],
                      style: TextStyle(
                        color: Pallet.fontcolornew,
                      ),
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    MyText(
                      text: firstname.substring(0, 1).toUpperCase() +
                          firstname.substring(1).toLowerCase(),
                      style: TextStyle(
                        color: Pallet.fontcolornew,
                        fontWeight: Pallet.font600,
                        fontSize: Pallet.heading3,
                      ),
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    MyText(
                      text: pageDetails['text9'],
                      style: TextStyle(
                        color: Pallet.fontcolornew,
                      ),
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    MyText(
                      text: lastname.substring(0, 1).toUpperCase() +
                          lastname.substring(1).toLowerCase(),
                      style: TextStyle(
                        fontWeight: Pallet.font600,
                        color: Pallet.fontcolornew,
                        fontSize: Pallet.heading3,
                      ),
                    ),
                    isMobile == true
                        ? accountDetailsOfShowAccountDetails()
                        : Container()
                  ],
                ),
                isMobile == false
                    ? accountDetailsOfShowAccountDetails()
                    : Container()
              ],
            ),
          ],
        ),
      ),
    );
  }

  Widget contactDetailsOfShowContactDetails() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        MyText(
          text: pageDetails['text10'],
          style: TextStyle(
            color: Pallet.fontcolornew,
          ),
        ),
        SizedBox(
          height: 5,
        ),
        MyText(
          text: cityString.substring(0, 1).toUpperCase() +
              cityString.substring(1).toLowerCase(),
          style: TextStyle(
            color: Pallet.fontcolornew,
            fontWeight: Pallet.font600,
            fontSize: Pallet.heading3,
          ),
        ),
        SizedBox(
          height: 20,
        ),
        MyText(
          text: pageDetails['text11'],
          style: TextStyle(
            color: Pallet.fontcolornew,
          ),
        ),
        SizedBox(
          height: 5,
        ),
        MyText(
          text: zipCode,
          style: TextStyle(
            color: Pallet.fontcolornew,
            fontWeight: Pallet.font600,
            fontSize: Pallet.heading3,
          ),
        ),
        SizedBox(
          height: 20,
        ),
        MyText(
          text: pageDetails['text12'],
          style: TextStyle(
            color: Pallet.fontcolornew,
          ),
        ),
        SizedBox(
          height: 5,
        ),
        MyText(
          text: emailString,
          style: TextStyle(
            color: Pallet.fontcolornew,
            fontWeight: Pallet.font600,
            fontSize: Pallet.heading3,
          ),
        ),
      ],
    );
  }

  Widget accountDetailsOfShowAccountDetails() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        MyText(
          text: pageDetails['text13'],
          style: TextStyle(
            color: Pallet.fontcolornew,
          ),
        ),
        SizedBox(
          height: 5,
        ),
        MyText(
          text: dob,
          style: TextStyle(
            fontWeight: Pallet.font600,
            color: Pallet.fontcolornew,
            fontSize: Pallet.heading3,
          ),
        ),
        SizedBox(
          height: 20,
        ),
        MyText(
          text: pageDetails['text14'],
          style: TextStyle(
            color: Pallet.fontcolornew,
          ),
        ),
        SizedBox(
          height: 5,
        ),
        MyText(
          text: nationality.toUpperCase(),
          style: TextStyle(
            fontWeight: Pallet.font600,
            color: Pallet.fontcolornew,
            fontSize: Pallet.heading3,
          ),
        ),
        SizedBox(
          height: 20,
        ),
        if (dashboard.accountType == 2)
          Row(
            children: [
              Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
                MyText(
                  text: pageDetails['text15'],
                  style: TextStyle(
                    color: Pallet.fontcolornew,
                  ),
                ),
                SizedBox(
                  height: 5,
                ),
                MyText(
                  text: kycstatus == ''
                      ? ''
                      : kycstatus.substring(0, 1).toUpperCase() +
                          kycstatus.substring(1).toLowerCase(),
                  style: TextStyle(
                    fontWeight: Pallet.font600,
                    color: Pallet.fontcolornew,
                    fontSize: Pallet.heading3,
                  ),
                ),
              ]),
              SizedBox(
                width: 10,
              ),
              kycstatus == 'not_verified'
                  ? Container(
                      decoration: BoxDecoration(
                        color: Pallet.fontcolornew,
                        borderRadius: BorderRadius.circular(
                          5,
                        ),
                      ),
                      padding: EdgeInsets.all(2),
                      child: Center(
                        child: Icon(
                          Icons.arrow_forward,
                          color: Pallet.fontcolor,
                        ),
                      ))
                  : Container(),
            ],
          )
      ],
    );
  }

  checkUsername(value) {
    if (value.trim().isEmpty) {
      scrnameerror = pageDetails['text22'];
    } else {
      if (Utils.isUsername(value)) {
        print(value);
        print("lllllldddddddddddd");
        output(value) {
          print(value);
          print("jjjjjjjjjjjjjjjjjjjjj");
          if (value == "usernameexists") {
            print(pageDetails['text39']);
            setState(() {
              scrnameerror = pageDetails['text39'];
            });
          } else {
            setState(() {
              scrnameerror = null;
            });
          }
        }

        SampleClass().checkusername(value, (output));
      } else {
        scrnameerror = pageDetails['text40'];
      }
    }
  }

  fetchapi() async {
    Map<String, dynamic> _map = {
      'system_product_id': 1,
    };

    var _result = await HttpRequest.Post(
        'individualprofile', Utils.constructPayload(_map));

    print(_result);
    if (Utils.isServerError(_result)) {
      snack.snack(title: pageDetails['text41']);
    } else {
      var val = _result['response']['data'];
      setState(() {
        firstname = val['first_name'].toString();
        accid = val['account_id'].toString();
        profileurl = val['profile_image'].toString();
        screeningname = val['screen_name'].toString();
        lastname = val['last_name'].toString();
        profileid = val['profile_id'].toString();

        dob = nullremover.nullremove(
            text: val['date_of_birth'],
            ifnullreturnval: pageDetails['text42'],
            enableemptyvalcheck: true);

        addressLine2 = nullremover.nullremove(
            text: val['address_line2'],
            ifnullreturnval: '',
            enableemptyvalcheck: true);

        addressLine1 = nullremover.nullremove(
                text: val['address_line1'],
                ifnullreturnval: '',
                enableemptyvalcheck: true) +
            ',' +
            addressLine2.toString();

        nationality = nullremover.nullremove(
            text: val['nationality'],
            ifnullreturnval: '',
            enableemptyvalcheck: true);

        phoneNumber = nullremover.nullremove(
            text: val['phone_no'],
            ifnullreturnval: pageDetails['text42'],
            enableemptyvalcheck: true);

        stateString = nullremover.nullremove(
            text: val['region'],
            ifnullreturnval: pageDetails['text42'],
            enableemptyvalcheck: true);

        country = nullremover.nullremove(
            text: val['country'],
            ifnullreturnval: pageDetails['text42'],
            enableemptyvalcheck: true);

        zipCode = nullremover.nullremove(
            text: val['postal_code'],
            ifnullreturnval: pageDetails['text42'],
            enableemptyvalcheck: true);

        cityString = nullremover.nullremove(
            text: val['city'],
            ifnullreturnval: pageDetails['text42'],
            enableemptyvalcheck: true);

        emailString = nullremover.nullremove(
            text: val['email_id'],
            ifnullreturnval: pageDetails['text42'],
            enableemptyvalcheck: true);

        kycstatus = nullremover.nullremove(
            text: val['kyc_status'],
            ifnullreturnval: '',
            enableemptyvalcheck: true);

        gender = nullremover.nullremove(
            text: val['gender'],
            ifnullreturnval: pageDetails['text42'],
            enableemptyvalcheck: true);

        beneficiaryval = nullremover.nullremove(
            text: val['beneficiary'],
            ifnullreturnval: pageDetails['text42'],
            enableemptyvalcheck: true);

        accnumberval = nullremover.nullremove(
            text: val['account_number'],
            ifnullreturnval: pageDetails['text42'],
            enableemptyvalcheck: true);

        banknameval = nullremover.nullremove(
            text: val['bank_name'],
            ifnullreturnval: pageDetails['text42'],
            enableemptyvalcheck: true);

        swiftval = nullremover.nullremove(
            text: val['bank_identification_code'],
            ifnullreturnval: pageDetails['text42'],
            enableemptyvalcheck: true);
      });

      stateSink.add(_result['response']['data']);
    }
  }
}

// Widget contactForm2() {
//   return Column(
//     children: [
//       Container(
//         width: 300,
//         child: Column(
//           crossAxisAlignment: CrossAxisAlignment.start,
//           children: [
//             MyText(text:'State',
//                 style: TextStyle(
//                     fontSize: Pallet.normalfont,
//                     color: Pallet.fontcolornew,
//                     fontWeight: Pallet.font500)),
//             SizedBox(height: 5),
//             SignupTextBox(
//                 // tabpress: (event) {
//                 //   if (event.isKeyPressed(LogicalKeyboardKey.tab)) {
//                 //     FocusScope.of(context).requestFocus(countryfocus);
//                 //   }
//                 // },
//                 focusnode: state,
//                 // onsubmit: () {
//                 //   FocusScope.of(context).requestFocus(countryfocus);
//                 // },
//                 errorText: stateError,
//                 isPassword: false,
//                 digitsOnlyPhone: false,
//                 // header: 'llllllllll',
//                 // label: 'addressLine12',
//                 controller: statecontroller,
//                 validation: (value) {
//                   setState(() {
//                     if (value.trim().isEmpty) {
//                       stateError = 'Required';
//                     } else {
//                       stateError = null;
//                     }
//                   });
//                 }),
//           ],
//         ),
//       ),
//       SizedBox(height: 10),
//       Container(
//         width: 300,
//         child: Column(
//           crossAxisAlignment: CrossAxisAlignment.start,
//           children: [
//             MyText(text:'City',
//                 style: TextStyle(
//                     fontSize: Pallet.normalfont,
//                     color: Pallet.fontcolornew,
//                     fontWeight: Pallet.font500)),
//             SizedBox(height: 5),
//             AbsorbPointer(
//               absorbing: !enablecity ? true : false,
//               child: TypeAheadFormField(
//                 errortext: cityError,
//                 textFieldConfiguration: TextFieldConfiguration(
//                   cursorColor: Pallet.fontcolornew,
//                   style: TextStyle(
//                       color: Pallet.fontcolornew,
//                       fontSize: Pallet.heading3,
//                       fontWeight: Pallet.font500),
//                   controller: citycontroller,
//                 ),
//                 suggestionsCallback: (pattern) {
//                   return cart.cities;
//                 },
//                 itemBuilder: (context, suggestion) {
//                   return ListTile(
//                     title: MyText(text:
//                       suggestion,
//                       style: TextStyle(color: Pallet.fontcolornew),
//                     ),
//                   );
//                 },
//                 transitionBuilder: (context, suggestionsBox, controller) {
//                   return suggestionsBox;
//                 },
//                 onSuggestionSelected: (suggestion) {
//                   setState(() {
//                     print('lllllllllllllllllllllllllllll');
//                     cart.zipCode.clear();

//                     this.citycontroller.text = suggestion;
//                     cityError = null;
//                     zipcontroller.text = '';
//                     enablezipcode = true;
//                     // postalcodeError = "Required";
//                     getRegionsMap['region_level'] = 5;
//                     getRegionsMap['parent_region'] = citycontroller.text;
//                     // isRequired = false;

//                     // print(isRequired);
//                     print(
//                         'VVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVV');

//                     cart.getZipCode(getRegionsMap);
//                   });
//                 },
//                 // ignore: missing_return
//                 onchanged: (value) {
//                   if (value.isEmpty) {
//                     setState(() {
//                       cityError = 'Required';
//                     });
//                   } else {
//                     setState(() {
//                       enablezipcode = true;
//                       cityError = null;
//                     });
//                   }
//                 },
//                 onSaved: (value) {
//                   setState(() {
//                     if (value.trim().isEmpty) {
//                       cityError = 'Required';
//                     } else {
//                       cityError = null;
//                     }
//                   });
//                 },
//                 // ignore: missing_return
//                 validator: (value) {
//                   if (value.trim().isEmpty) {
//                     setState(() {
//                       cityError = 'Required';
//                     });
//                   } else {
//                     setState(() {
//                       cityError = null;
//                     });
//                   }
//                 },
//               ),
//             ),
//           ],
//         ),
//       ),
//       SizedBox(
//         height: 10,
//       ),
//       Container(
//         width: 300,
//         child: Column(
//           crossAxisAlignment: CrossAxisAlignment.start,
//           children: [
//             MyText(text:'E-Mail',
//                 style: TextStyle(
//                     fontSize: Pallet.normalfont,
//                     color: Pallet.fontcolornew,
//                     fontWeight: Pallet.font500)),
//             SizedBox(height: 5),
//             SignupTextBox(
//                 // tabpress: (event) {
//                 //   if (event.isKeyPressed(LogicalKeyboardKey.tab)) {
//                 //     FocusScope.of(context).requestFocus(countryfocus);
//                 //   }
//                 // },
//                 focusnode: email,
//                 // onsubmit: () {
//                 //   FocusScope.of(context).requestFocus(countryfocus);
//                 // },
//                 readonly: true,
//                 errorText: emailError,
//                 isPassword: false,
//                 digitsOnlyPhone: false,
//                 // header: 'llllllllll',
//                 // label: 'addressLine12',
//                 controller: emailcontroller,
//                 validation: (value) {
//                   setState(() {
//                     if (value.trim().isEmpty) {
//                       emailError = 'Required';
//                     } else {
//                       emailError = null;
//                     }
//                   });
//                 }),
//           ],
//         ),
//       ),
//     ],
//   );
// }

// Widget editAccountDetails({double width}) {
//   return Padding(
//     padding: const EdgeInsets.symmetric(horizontal: 8),
//     child: Container(
//       // width: width - 50,

//       padding: EdgeInsets.all(10),
//       decoration: BoxDecoration(
//         color: Colors.white,
//         boxShadow: [
//           BoxShadow(
//             color: Colors.black12,
//             blurRadius: 3,
//             spreadRadius: 8,
//           )
//         ],
//         borderRadius: BorderRadius.circular(10),
//       ),
//       child: Column(
//         children: [
//           Row(
//             mainAxisAlignment: MainAxisAlignment.spaceBetween,
//             children: [
//               MyText(text:'Account', style: Pallet.profiletext),
//               // InkWell(
//               //   onTap: () {
//               //     setState(() {
//               //       isAccountEdit = !isAccountEdit;
//               //       companyController.text = firstname;
//               //       taxnumController.text = lastname;
//               //       dateofIncorpcontroller.text = dob;
//               //       fieldofbuisnesscontroller.text = phonenumber;
//               //     });
//               //   },
//               //   child: MyText(text:
//               //     isAccountEdit == false ? "Edit" : "Cancel",
//               //     style: TextStyle(
//               //         color: Pallet.fontcolornew,
//               //         decoration: TextDecoration.underline),
//               //   ),
//               // )
//             ],
//           ),
//           SizedBox(
//             height: 30,
//           ),
//           Column(
//             crossAxisAlignment: CrossAxisAlignment.stretch,
//             children: [
//               MyText(text:
//                 'Username',
//                 style: Pallet.profiletext,
//               ),
//               Row(
//                 mainAxisAlignment: MainAxisAlignment.spaceEvenly,
//                 children: [
//                   Column(
//                     crossAxisAlignment: CrossAxisAlignment.start,
//                     children: [
//                       SizedBox(
//                         height: 20,
//                       ),
//                       Container(
//                         width: 300,
//                         child: Column(
//                           crossAxisAlignment: CrossAxisAlignment.start,
//                           children: [
//                             MyText(text:'User Name *',
//                                 style: TextStyle(
//                                     fontSize: Pallet.normalfont,
//                                     color: Pallet.fontcolornew,
//                                     fontWeight: Pallet.font500)),
//                             SizedBox(height: 10),
//                             SignupTextBox(
//                                 tabpress: (event) {
//                                   if (event
//                                       .isKeyPressed(LogicalKeyboardKey.tab)) {
//                                     FocusScope.of(context)
//                                         .requestFocus(taxnumber);
//                                   }
//                                 },
//                                 errorText: companyError,
//                                 focusnode: companyName,
//                                 onsubmit: () {
//                                   FocusScope.of(context)
//                                       .requestFocus(taxnumber);
//                                 },
//                                 isPassword: false,
//                                 digitsOnlyPhone: false,
//                                 controller: companyController,
//                                 validation: (value) {
//                                   setState(() {
//                                     if (value.trim().isEmpty) {
//                                       companyError = 'Required';
//                                     } else {
//                                       companyError = null;
//                                     }
//                                   });
//                                 }),
//                           ],
//                         ),
//                       ),
//                       SizedBox(
//                         height: 20,
//                       ),
//                       Container(
//                         width: 300,
//                         child: Column(
//                           crossAxisAlignment: CrossAxisAlignment.start,
//                           children: [
//                             MyText(text:'Last Name *',
//                                 style: TextStyle(
//                                     fontSize: Pallet.normalfont,
//                                     color: Pallet.fontcolornew,
//                                     fontWeight: Pallet.font500)),
//                             SizedBox(height: 10),
//                             SignupTextBox(
//                                 tabpress: (event) {
//                                   if (event
//                                       .isKeyPressed(LogicalKeyboardKey.tab)) {
//                                     FocusScope.of(context)
//                                         .requestFocus(dateofIncorp);
//                                   }
//                                 },
//                                 focusnode: taxnumber,
//                                 onsubmit: () {
//                                   FocusScope.of(context)
//                                       .requestFocus(dateofIncorp);
//                                 },
//                                 errorText: taxnumberError,
//                                 isPassword: false,
//                                 digitsOnlyPhone: false,
//                                 controller: taxnumController,
//                                 validation: (value) {
//                                   setState(() {
//                                     if (value.trim().isEmpty) {
//                                       taxnumberError = 'Required';
//                                     } else {
//                                       taxnumberError = null;
//                                     }
//                                   });
//                                 }),
//                           ],
//                         ),
//                       ),
//                       isMobile == true
//                           ? detailsOfEditAccountDetails()
//                           : Container()
//                     ],
//                   ),
//                   isMobile == false
//                       ? detailsOfEditAccountDetails()
//                       : Container(),
//                 ],
//               ),
//             ],
//           ),
//           SizedBox(height: 30),
//           Row(
//             mainAxisAlignment: MainAxisAlignment.spaceEvenly,
//             children: [
//               RaisedButton(
//                 color: Colors.grey[300],
//                 child: MyText(text:'Cancel',
//                     style: TextStyle(
//                       color: Pallet.fontcolornew,
//                     )),
//                 onPressed: () {
//                   setState(() {
//                     isAccountEdit = false;
//                     companyError = null;
//                     dateofIncorpError = null;
//                     taxnumberError = null;
//                     fieldofbuisnessError = null;
//                   });
//                 },
//               ),
//               RaisedButton(
//                 focusNode: submit,
//                 child: Center(
//                     child: MyText(text:
//                   'Submit',
//                   style: TextStyle(color: Pallet.fontcolor),
//                 )),
//                 onPressed: () {
//                   setState(() {
//                     if (companyController.text.trim().isEmpty ||
//                         taxnumController.text.trim().isEmpty ||
//                         dateofIncorpcontroller.text.trim().isEmpty ||
//                         fieldofbuisnesscontroller.text.trim().isEmpty) {
//                       if (companyController.text.trim().length < 1) {
//                         setState(() {
//                           companyError = 'Required';
//                         });
//                       }
//                       if (taxnumController.text.trim().length < 1) {
//                         setState(() {
//                           taxnumberError = 'Required';
//                         });
//                       }
//                       if (dateofIncorpcontroller.text.trim().length < 1) {
//                         setState(() {
//                           dateofIncorpError = 'Required';
//                         });
//                       }
//                       if (fieldofbuisnesscontroller.text.trim().length < 1) {
//                         setState(() {
//                           fieldofbuisnessError = 'Required';
//                         });
//                       }
//                     } else {
//                       if (companyError == null &&
//                           dateofIncorpError == null &&
//                           taxnumberError == null &&
//                           fieldofbuisnessError == null) {
//                         firstname = companyController.text.trim();
//                         lastname = taxnumController.text.trim();
//                         dob = dateofIncorpcontroller.text.trim();
//                         phonenumber = fieldofbuisnesscontroller.text.trim();
//                         isAccountEdit = false;
//                       }
//                     }
//                   });
//                 },
//                 color: Pallet.fontcolornew,
//               )
//             ],
//           ),
//           SizedBox(height: 30),
//         ],
//       ),
//     ),
//   );
// }

// Column detailsOfEditAccountDetails() {
//   return Column(
//     mainAxisAlignment: MainAxisAlignment.end,
//     children: [
//       SizedBox(
//         height: 20,
//       ),
//       Container(
//         width: 300,
//         child: Column(
//           crossAxisAlignment: CrossAxisAlignment.start,
//           children: [
//             MyText(text:"Date of incorporation *",
//                 style: TextStyle(
//                   fontSize: Pallet.normalfont,
//                   color: Pallet.fontcolornew,
//                   fontWeight: Pallet.font500,
//                 )),
//             SizedBox(height: 5),
//             TextFormField(
//               focusNode: dateofIncorp,
//               readOnly: true,
//               controller: dateofIncorpcontroller,
//               onTap: _handleDatePicker,
//               textAlign: TextAlign.left,
//               onFieldSubmitted: (value) {
//                 FocusScope.of(context).requestFocus(fieldofbuisness);
//               },
//               style: TextStyle(
//                   color: Pallet.fontcolornew,
//                   fontSize: Pallet.heading3,
//                   fontWeight: Pallet.font500),
//               cursorColor: Pallet.fontcolornew,
//               keyboardType: TextInputType.emailAddress,
//               decoration: InputDecoration(
//                 errorText: dateofIncorpError,
//                 errorStyle: TextStyle(
//                   fontSize: Pallet.heading4,
//                   color: Pallet.errortxt,
//                 ),
//                 border: OutlineInputBorder(
//                     borderSide: const BorderSide(
//                         color: Color(0XFF1034a6), width: 1.0),
//                     borderRadius: BorderRadius.circular(8.0)),
//                 hintText: "",
//                 hintStyle: TextStyle(
//                     fontSize: Pallet.heading4,
//                     fontWeight: Pallet.font500,
//                     color: Pallet.fontcolornew),
//                 fillColor: Pallet.fontcolor,
//                 focusedBorder: OutlineInputBorder(
//                     borderSide: const BorderSide(
//                         color: Color(0XFF1034a6), width: 1.0),
//                     borderRadius: BorderRadius.circular(8.0)),
//                 enabledBorder: OutlineInputBorder(
//                     borderSide:
//                         BorderSide(color: Color(0xFF4570ae), width: 0.0),
//                     borderRadius: BorderRadius.circular(8.0)),
//                 filled: true,
//                 prefixIcon: Padding(
//                     padding: EdgeInsets.only(left: 20, right: 10),
//                     child: Icon(Icons.calendar_today,
//                         size: 22, color: Pallet.fontcolornew)),
//               ),
//             ),
//           ],
//         ),
//       ),
//       SizedBox(
//         height: 20,
//       ),
//       Container(
//         width: 300,
//         child: Column(
//           crossAxisAlignment: CrossAxisAlignment.start,
//           children: [
//             MyText(text:"Field of business *",
//                 style: TextStyle(
//                   fontSize: Pallet.normalfont,
//                   color: Pallet.fontcolornew,
//                   fontWeight: Pallet.font500,
//                 )),
//             SizedBox(height: 5),
//             SignupTextBox(
//                 tabpress: (event) {
//                   if (event.isKeyPressed(LogicalKeyboardKey.tab)) {
//                     FocusScope.of(context).requestFocus(submit);
//                   }
//                 },
//                 focusnode: fieldofbuisness,
//                 onsubmit: () {
//                   FocusScope.of(context).requestFocus(submit);
//                 },
//                 errorText: fieldofbuisnessError,
//                 isPassword: false,
//                 digitsOnlyPhone: false,
//                 controller: fieldofbuisnesscontroller,
//                 validation: (value) {
//                   setState(() {
//                     if (value.trim().isEmpty) {
//                       fieldofbuisnessError = 'Required';
//                     } else {
//                       fieldofbuisnessError = null;
//                     }
//                   });
//                 }),
//           ],
//         ),
//       ),
//     ],
//   );
// }

// Widget contactForm(width) {
//   return Container(
//       child: Column(
//     children: [
//       Container(
//         width: isMobile == true ? width : width * .7,
//         child: Column(
//           crossAxisAlignment: CrossAxisAlignment.start,
//           children: [
//             MyText(text:'Address Line1',
//                 style: TextStyle(
//                     fontSize: Pallet.normalfont,
//                     color: Pallet.fontcolornew,
//                     fontWeight: Pallet.font500)),
//             SizedBox(height: 5),
//             SignupTextBox(
//                 tabpress: (event) {
//                   if (event.isKeyPressed(LogicalKeyboardKey.tab)) {
//                     FocusScope.of(context).requestFocus(addressline2);
//                   }
//                 },
//                 focusnode: addressline1,
//                 onsubmit: () {
//                   FocusScope.of(context).requestFocus(addressline2);
//                 },
//                 errorText: addressline1Error,
//                 isPassword: false,
//                 digitsOnlyPhone: false,

//                 // header: 'llllllllll',
//                 // label: ' Addressline1',
//                 controller: addressline1controller,
//                 validation: (value) {
//                   setState(() {
//                     if (value.trim().isEmpty) {
//                       addressline1Error = 'Required';
//                     } else {
//                       addressline1Error = null;
//                     }
//                   });
//                 }),
//           ],
//         ),
//       ),
//       SizedBox(
//         height: 10,
//       ),
//       Container(
//         width: isMobile == true ? width : width * .7,
//         child: Column(
//           crossAxisAlignment: CrossAxisAlignment.start,
//           children: [
//             MyText(text:'Address Line2',
//                 style: TextStyle(
//                     fontSize: Pallet.normalfont,
//                     color: Pallet.fontcolornew,
//                     fontWeight: Pallet.font500)),
//             SizedBox(height: 5),
//             SignupTextBox(
//                 tabpress: (event) {
//                   if (event.isKeyPressed(LogicalKeyboardKey.tab)) {
//                     FocusScope.of(context).requestFocus(mobile);
//                   }
//                 },
//                 focusnode: addressline2,
//                 onsubmit: () {
//                   FocusScope.of(context).requestFocus(mobile);
//                 },
//                 errorText: addressline2Error,
//                 isPassword: false,
//                 digitsOnlyPhone: false,

//                 // header: 'llllllllll',
//                 // label: ' Addressline1',
//                 controller: addressline2controller,
//                 validation: (value) {
//                   setState(() {
//                     if (value.trim().isEmpty) {
//                       addressline2Error = 'Required';
//                     } else {
//                       addressline2Error = null;
//                     }
//                   });
//                 }),
//           ],
//         ),
//       ),
//       SizedBox(
//         height: 20,
//       ),
//       Row(
//         mainAxisAlignment: MainAxisAlignment.spaceEvenly,
//         children: [
//           Column(
//             children: [
//               Container(
//                 width: 300,
//                 child: Column(
//                   crossAxisAlignment: CrossAxisAlignment.start,
//                   children: [
//                     MyText(text:'Phone Number',
//                         style: TextStyle(
//                             fontSize: Pallet.normalfont,
//                             color: Pallet.fontcolornew,
//                             fontWeight: Pallet.font500)),
//                     SizedBox(height: 5),
//                     SignupTextBox(
//                         // tabpress: (event) {
//                         //   if (event.isKeyPressed(LogicalKeyboardKey.tab)) {
//                         //     FocusScope.of(context).requestFocus(countryfocus);
//                         //   }
//                         // },
//                         focusnode: mobile,
//                         // onsubmit: () {
//                         //   FocusScope.of(context).requestFocus(countryfocus);
//                         // },
//                         errorText: mobileError,
//                         isPassword: false,
//                         digitsOnlyPhone: true,
//                         // header: 'llllllllll',
//                         // label: 'addressLine12',
//                         controller: mobilecontroller,
//                         validation: (value) {
//                           setState(() {
//                             if (value.trim().isEmpty) {
//                               mobileError = 'Required';
//                             } else {
//                               mobileError = null;
//                             }
//                           });
//                         }),
//                   ],
//                 ),
//               ),
//               SizedBox(
//                 height: 10,
//               ),
//               Container(
//                 width: 300,
//                 child: Column(
//                   crossAxisAlignment: CrossAxisAlignment.start,
//                   children: [
//                     MyText(text:'Country',
//                         style: TextStyle(
//                             fontSize: Pallet.normalfont,
//                             color: Pallet.fontcolornew,
//                             fontWeight: Pallet.font500)),
//                     SizedBox(height: 5),
//                     DropDownField(
//                         // hintText: 'Please choose country',
//                         required: isRequired,
//                         textStyle: TextStyle(color: Pallet.fontcolornew),
//                         value: dropDownCountry,
//                         onValueChanged: (newValue) {
//                           setState(() {
//                             cart.cities = [];
//                             cart.zipCode = [];
//                             citycontroller.text = '';
//                             zipcontroller.text = '';
//                             enablecity = true;
//                             dropDownCountry = newValue;
//                             getRegionsMap['region_level'] = 4;
//                             getRegionsMap['parent_region'] =
//                                 dropDownCountry.toUpperCase();

//                             cart.getCities(getRegionsMap);
//                           });
//                         },
//                         items: cart.countries),
//                   ],
//                 ),
//               ),
//               SizedBox(
//                 height: 10,
//               ),
//               Container(
//                 width: 300,
//                 child: Column(
//                   crossAxisAlignment: CrossAxisAlignment.start,
//                   children: [
//                     MyText(text:'Zip Code',
//                         style: TextStyle(
//                             fontSize: Pallet.normalfont,
//                             color: Pallet.fontcolornew,
//                             fontWeight: Pallet.font500)),
//                     SizedBox(height: 5),
//                     Container(
//                         // decoration: BoxDecoration(
//                         //     border:
//                         //         Border.all(color: Pallet.fontcolornew)),
//                         child: AbsorbPointer(
//                             absorbing: !enablezipcode ? true : false,
//                             child: TypeAheadFormField(
//                               // ignore: missing_return
//                               validator: (val) {
//                                 if (val.trim().isEmpty) {}
//                               },
//                               errortext: zipError,
//                               textFieldConfiguration: TextFieldConfiguration(
//                                   cursorColor: Pallet.fontcolornew,
//                                   style: TextStyle(
//                                       color: Pallet.fontcolornew,
//                                       fontSize: Pallet.heading3,
//                                       fontWeight: Pallet.font500),
//                                   controller: zipcontroller,
//                                   decoration: InputDecoration(
//                                     border: OutlineInputBorder(
//                                         borderSide: const BorderSide(
//                                             color: Color(0XFF1034a6),
//                                             width: 1.0),
//                                         borderRadius:
//                                             BorderRadius.circular(8.0)),
//                                     errorBorder: OutlineInputBorder(
//                                         borderSide: BorderSide(
//                                             color: Colors.red, width: 2),
//                                         borderRadius: BorderRadius.circular(
//                                             Pallet.radius)),
//                                     focusedErrorBorder: OutlineInputBorder(
//                                         borderSide: BorderSide(
//                                             color: Colors.red, width: 2),
//                                         borderRadius: BorderRadius.circular(
//                                             Pallet.radius)),
//                                     focusedBorder: OutlineInputBorder(
//                                         borderSide: const BorderSide(
//                                             color: Color(0XFF1034a6),
//                                             width: 1.0),
//                                         borderRadius:
//                                             BorderRadius.circular(8.0)),
//                                     enabledBorder: OutlineInputBorder(
//                                         borderSide: BorderSide(
//                                             color: Color(0xFF4570ae),
//                                             width: 0.0),
//                                         borderRadius:
//                                             BorderRadius.circular(8.0)),
//                                   )),
//                               suggestionsCallback: (pattern) {
//                                 return cart.zipCode;
//                               },

//                               itemBuilder: (context, suggestion) {
//                                 return ListTile(
//                                   title: MyText(text:
//                                     suggestion,
//                                     style:
//                                         TextStyle(color: Pallet.fontcolornew),
//                                   ),
//                                 );
//                               },
//                               transitionBuilder:
//                                   (context, suggestionsBox, controller) {
//                                 return suggestionsBox;
//                               },

//                               onSuggestionSelected: (suggestion) {
//                                 setState(() {
//                                   // isRequired = false;
//                                   zipError = null;

//                                   this.zipcontroller.text = suggestion;
//                                 });
//                               },
//                             )))
//                   ],
//                 ),
//               ),
//               isMobile == true ? contactForm2() : Container()
//             ],
//           ),
//           isMobile == false
//               ? Column(
//                   children: [contactForm2()],
//                 )
//               : Column(),
//         ],
//       ),
//       SizedBox(
//         height: 20,
//       ),
//       Row(
//         mainAxisAlignment: MainAxisAlignment.spaceEvenly,
//         children: [
//           RaisedButton(
//             onPressed: () {
//               setState(() {
//                 enablecity = false;
//                 enablezipcode = false;
//                 addressline1Error = null;
//                 addressline2Error = null;
//                 stateError = null;
//                 countryError = null;
//                 cityError = null;
//                 zipError = null;
//                 mobileError = null;
//                 isContactEdit = false;
//               });
//             },
//             color: Colors.grey[300],
//             child: MyText(text:'Cancel',
//                 style: TextStyle(
//                   color: Pallet.fontcolornew,
//                 )),
//           ),
//           RaisedButton(
//               color: Pallet.fontcolornew,
//               onPressed: () {
//                 isRequired = true;
//                 setState(() {
//                   if (addressline1controller.text.trim().isEmpty ||
//                       addressline2controller.text.trim().isEmpty ||
//                       mobilecontroller.text.trim().isEmpty ||
//                       zipcontroller.text.trim().isEmpty ||
//                       dropDownCountry.isEmpty ||
//                       emailcontroller.text.trim().isEmpty ||
//                       statecontroller.text.trim().isEmpty ||
//                       citycontroller.text.trim().isEmpty) {
//                     if (addressline1controller.text.trim().length < 1) {
//                       setState(() {
//                         addressline1Error = 'Required';
//                       });
//                     }
//                     if (addressline2controller.text.trim().length < 1) {
//                       setState(() {
//                         addressline2Error = 'Required';
//                       });
//                     }
//                     if (dropDownCountry.length < 1) {
//                       setState(() {
//                         countryError = 'Required';
//                       });
//                     }
//                     if (mobilecontroller.text.trim().length < 1) {
//                       setState(() {
//                         mobileError = 'Required';
//                       });
//                     }
//                     if (citycontroller.text.trim().length < 1) {
//                       setState(() {
//                         cityError = 'Required';
//                       });
//                     }
//                     if (zipcontroller.text.trim().length < 1) {
//                       setState(() {
//                         zipError = 'Required';
//                       });
//                     }
//                     if (emailcontroller.text.trim().length < 1) {
//                       setState(() {
//                         emailError = 'Required';
//                       });
//                     }
//                     if (statecontroller.text.trim().length < 1) {
//                       setState(() {
//                         stateError = 'Required';
//                       });
//                     }
//                   } else {
//                     enablecity = false;
//                     enablezipcode = false;
//                     addressLine1 =
//                         addressline1controller.text.trimRight().trimLeft();
//                     addressLine2 =
//                         addressline2controller.text.trimRight().trimLeft();
//                     phoneNumber =
//                         mobilecontroller.text.trimRight().trimLeft();
//                     stateString = statecontroller.text.trimRight().trimLeft();
//                     country = dropDownCountry;
//                     zipCode = zipcontroller.text.trimRight().trimLeft();
//                     cityString = citycontroller.text.trimRight().trimLeft();
//                     emailString = emailcontroller.text.trimRight().trimLeft();

//                     isContactEdit = false;

//                     // isAccountEdit = false;
//                   }
//                 });
//               },
//               child: MyText(text:
//                 'Submit',
//                 style: TextStyle(color: Pallet.fontcolor),
//               )),
//         ],
//       ),
//       SizedBox(
//         height: 30,
//       ),
//     ],
//   ));
// }
