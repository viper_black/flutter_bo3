import '../common.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:url_launcher/url_launcher.dart';
import '../config.dart';
import '../config/index.dart';
import '../utils/utils.dart';
import "../services/business/account.dart";

class Invitemember extends StatefulWidget {
  final double wdgtWidth, wdgtHeight;
  // String mynetworkinviter;
  Invitemember({
    Key key,
    this.wdgtWidth,
    this.wdgtHeight,
    // this.mynetworkinviter,
  }) : super(key: key);
  @override
  _InvitememberState createState() => _InvitememberState();
}

class _InvitememberState extends State<Invitemember> {
  // String heading = "Invite a Member";

  String link;
  String finallink;
  Future<String> temp;
  Map<String, dynamic> pageDetails = {};
  initState() {
    temp = dashboard.dashboard();
    // temp = Utils.getPageDetails('invitemember');
    setPageDetails();
    print("llllllllllllllaaaaaaaaaa");
    // print(widget.mynetworkinviter);
    // finallink = widget.mynetworkinviter != null
    //     ? widget.mynetworkinviter.toString()
    //     : dashboard.username.toString();
    // widget.mynetworkinviter = null;
    // String inviterlink = dashboard.accountType == 2
    //     ? "bmV0d29ya2Vy"
    //     : dashboard.accountType == 1
    //         ? "Q3VzdG9tZXI="
    //         : 'bm90Zm91bmQK';

    // link =
    //     appSettings["CLIENT_URL"] + '/invite/' + dashboard.username.toString();
    super.initState();
  }

  void setPageDetails() async {
    String data = await Utils.getPageDetails('invitemember');
    setState(() {
      pageDetails = Utils.fromJSONString(data);
    });
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
        future: temp,
        builder: (context, snapshot) {
          if (snapshot.hasError) {
            if (snapshot.error == 'Something Went Wrong') {
              print(snapshot.error);
            } else {
              return Error(
                message: snapshot.error != null ? snapshot.error : '',
              );
            }
          } else if (snapshot.hasData) {
            updateLink();
            return LayoutBuilder(
              builder: (context, constraints) {
                if (constraints.maxWidth <= ScreenSize.iphone) {
                  return SingleChildScrollView(
                    child: Padding(
                      padding: EdgeInsets.only(
                          left: Pallet.defaultPadding,
                          top: Pallet.topPadding1,
                          right: Pallet.defaultPadding),
                      child: Center(
                        child: invite(wdgtWidth: widget.wdgtWidth),
                      ),
                    ),
                  );
                } else if (constraints.maxWidth > ScreenSize.iphone &&
                    constraints.maxWidth < ScreenSize.ipad) {
                  return SingleChildScrollView(
                    child: Padding(
                      padding: EdgeInsets.only(top: Pallet.topPadding1),
                      child: Center(
                        child: invite(wdgtWidth: widget.wdgtWidth * 0.80),
                      ),
                    ),
                  );
                } else {
                  return SingleChildScrollView(
                    child: Padding(
                      padding: EdgeInsets.only(
                          left: Pallet.leftPadding, top: Pallet.topPadding1),
                      child: Center(
                        child: invite(wdgtWidth: widget.wdgtWidth),
                      ),
                    ),
                  );
                }
              },
            );
          }
          return Loader();
        });
  }

  updateLink() {
    print('called ***********');
    link =
        appSettings["CLIENT_URL"] + '/invite/' + dashboard.username.toString();
  }

  Widget invite({double wdgtWidth, double wdgtHeight}) {
    return Container(
      width: wdgtWidth,
      // height: wdgtHeight,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              MyText(text:pageDetails["text1"],
                  style: TextStyle(
                    color: Pallet.fontcolornew,
                    fontSize: Pallet.heading1,
                    // fontWeight: Pallet.font500,
                  )),
              SizedBox(height: 20),
              MyText(text:pageDetails["text4"],
                  style: TextStyle(
                    color: Pallet.fontcolornew,
                    fontSize: Pallet.heading3,
                  )),
              SizedBox(height: 20),
              MyText(
                text:pageDetails['text2'],
                style: TextStyle(fontSize: 14),
              ),
            ],
          ),
          SizedBox(height: 30),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Container(
                  // alignment: Alignment.center,
                  width: 600,
                  // height: widget.wdgtWidth * 0.30,
                  decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(5)),
                  child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Center(
                          child: SingleChildScrollView(
                            child: Container(
                                padding: EdgeInsets.all(15),
                                color: Colors.black12.withOpacity(0.1),
                                // margin: EdgeInsets.symmetric(
                                //   vertical: 25,
                                //   horizontal: wdgtWidth * 0.15,
                                // ),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    MyText(
                                      text:pageDetails['text3'],
                                      style: TextStyle(height: 1.5),
                                    ),
                                    SizedBox(height: 10),
                                    InkWell(
                                      onTap: () {
                                        launch(link);
                                      },
                                      child: MyText(
                                      text:  link,
                                        style: TextStyle(
                                          color: Pallet.dashcontainerback,
                                          fontWeight: FontWeight.w700,
                                          decoration: TextDecoration.underline,
                                        ),
                                      ),
                                    ),
                                  ],
                                )),
                          ),
                        ),
                        // SizedBox(height: 15),
                        // Container(
                        //   width: double.infinity,
                        //   // ignore: deprecated_member_use
                        //   child: RaisedButton(
                        //       shape: RoundedRectangleBorder(
                        //         borderRadius: BorderRadius.circular(8.0),
                        //       ),
                        //       padding: const EdgeInsets.fromLTRB(0, 18, 0, 18),
                        //       child: MyText(
                        //         pageDetails['text5'],
                        //         style: TextStyle(
                        //             fontSize: Pallet.heading3,
                        //             fontWeight: Pallet.font500),
                        //       ),
                        //       color: Pallet.dashcontainerback,
                        //       textColor: Pallet.fontcolor,
                        //       onPressed: () {
                        //         snack.snack(title: pageDetails['text6']);
                        //         // ScaffoldMessenger.of(context).showSnackBar(
                        //         //   SnackBar(
                        //         //     content: MyText(pageDetails['text6']),
                        //         //   ),
                        //         // );
                        //         String msg = pageDetails['text3'] + "$link";
                        //         Clipboard.setData(ClipboardData(MyText: "$msg"))
                        //             .then((result) {});
                        //       }),
                        // ),
                        SizedBox(height: 15),
                        Container(
                          width: double.infinity,
                          child: CustomButton(
                            onpress: () {
                              snack.snack(title: pageDetails['text6']);
                              String msg = pageDetails['text3'] + "$link";
                              Clipboard.setData(ClipboardData(text: "$msg"))
                                  .then((result) {});
                            },
                            vpadding: 10,
                            text: pageDetails['text5'],
                            buttoncolor: Pallet.fontcolornew,
                            textcolor: Pallet.fontcolor,
                            textsize: Pallet.heading4,
                          ),
                        ),
                      ])),
            ],
          ),
        ],
      ),
    );
  }
}
