import 'services/business/user.dart';

import 'utils/utils.dart';

class SampleClass {
  SampleClass();
  final user = User();
  void getSigninInit(Function next) {
    //print('asfhjadhsfkljhdasflkjdhasklfjhasdlkfjsdhalkjfh');
    user.loginPreferences().then((value) => next(value));
  }

  login(input, next) {
    user.login(input).then((value) => next(value));
  }

  checkpassword(input, next) {
    user.checkPassword(input).then((value) => next(value));
  }

  getCountries(input, next) {
    user.getCountires(input).then((value) => next(value));
  }

  dashboard(next) {
    user.account().dashboard().then((value) => next(value));
  }

  getDocuments(next) {
    user.getDocuments().then((value) => next(value));
  }

  getallproducts(next) {
    user.account().product().getAll({"product_id": 1, "category_id": 0}).then(
        (value) => next(value));
  }

  checkusername(input, next) {
    user.checkUsername(input).then((value) => next(value));
  }

  checkemail(input, next) {
    user.checkEmail(input).then((value) => next(value));
  }

  checksponsor(input, next) {
    user.isSponser(input).then((value) => next(value));
  }

  setup2fa(input, next) {
    user.setup2FA(input).then((value) => next(value));
  }

  check2fa(secret, next) async {
    var username = Utils.fromJSONString(
        await Utils.getSharedPrefereces('data'))['user_name'];
    user.check2FA(username, secret).then((value) => next(value));
  }
}
