import 'dart:ui';

import 'package:carousel_slider/carousel_slider.dart';
import 'package:centurion/animations/Bouncing_button.dart';
import 'package:centurion/remuspayment.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'config.dart';
import 'config/app_settings.dart';
import 'home.dart';
import 'utils/utils.dart';
import 'package:flutter/services.dart';
import 'package:local_auth/local_auth.dart';
import 'common.dart';
import 'utils/utils.dart' show Utils;
import 'services/communication/index.dart' show HttpRequest;
import 'services/communication/crypto/index.dart';

// ignore: must_be_immutable
class LoginRemus extends StatefulWidget {
  final String shopingtokenkey;
  LoginRemus({Key key, this.shopingtokenkey}) : super(key: key);

  @override
  _LoginRemusState createState() => _LoginRemusState();
}

class _LoginRemusState extends State<LoginRemus> {
  GlobalKey<FormState> _loginFormKey = GlobalKey<FormState>();

  Map<String, dynamic> pageDetails = {};
  _LoginRemusState() {
    setPageDetails();
  }
  void setPageDetails() async {
    String data = await Utils.getPageDetails('login');
    setState(() {
      pageDetails = Utils.fromJSONString(data);
    });
    // snack.snack(title: otpRegistered);
    otpRegistered = SnackBar(
      content: Text(pageDetails["text1"]),
    );
  }

  LocalAuthentication localAuth = LocalAuthentication();
  TextEditingController otp = TextEditingController();
  var _formKey2 = GlobalKey<FormState>();
  Future<bool> hasBiometric;
  bool fingerPrintAuth;
  bool isSetUp = false;
  bool ismobile = false;
  final username = TextEditingController();
  String usernameError;

  final password = TextEditingController();
  String passwordError;
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  SnackBar otpRegistered;

  String deviceId = 'null';
  String deviceModel;
  String macAddress;
  String otpError;

  Future<String> loaclStorage;
  Future<String> fingerPrintSet;
  // ignore: non_constant_identifier_names
  bool language_change;

  void initState() {
    loaclStorage = Utils.getSharedPrefereces('data');
    fingerPrintSet = Utils.getSharedPrefereces('fingerprint');
    language_change = false;
    print("llllllllllllaaaaaaaa");
    print(widget.shopingtokenkey);
    super.initState();
  }

  int _current = 1;
  int selectedoption = 1;
  IconData icon = Icons.arrow_drop_down;
  Future<String> temp;
  double totalAmount;
  CarouselController _carouselController = new CarouselController();
  @override
  Scaffold build(BuildContext context) {
    Size size = MediaQuery.of(context).size;

    return Scaffold(
      backgroundColor: Colors.transparent,
      body: LayoutBuilder(builder: (context, constraints) {
        if (constraints.maxWidth < ScreenSize.ipad) {
          return Container(
            padding: EdgeInsets.only(top: 15),
            width: size.width,
            height: size.height,
            color: Pallet.inner2,
            child: SingleChildScrollView(
              child: Column(
                children: [
                  SizedBox(
                    height: 60,
                  ),
                  Align(
                    alignment: Alignment.center,
                    child: Image.asset(
                      'mobile_logo.png',
                      height: 100,
                    ),
                  ),
                  SizedBox(
                    height: 30,
                  ),
                  Container(
                      padding: EdgeInsets.all(15),
                      width: size.width,
                      height: size.height - 205,
                      decoration: BoxDecoration(
                          color: Pallet.inner1,
                          borderRadius: BorderRadius.only(
                              topRight: Radius.circular(30),
                              topLeft: Radius.circular(30))),
                      child: signin1(wdgtWidth: 350)),
                ],
              ),
            ),
          );
        } else {
          return Row(
            children: [
              Container(
                padding: EdgeInsets.only(top: 15),
                width: 420,
                height: size.height,
                color: Pallet.inner1,
                child: Center(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      SizedBox(
                        height: 20,
                      ),
                      Image.asset(
                        'logo.png',
                        height: 100,
                      ),
                      SizedBox(
                        height: 30,
                      ),
                      Container(
                          // padding: EdgeInsets.all(15),
                          height: size.height - 180,
                          child: signin1(wdgtWidth: 350)),
                    ],
                  ),
                ),
              ),
              Center(
                child: Container(
                  width: size.width - 420,
                  height: size.height - 205,
                  color: Pallet.inner2,
                  child: Image.asset(
                    "centurion-logo.png",
                  ),
                ),
              )
            ],
          );
        }
      }),
    );
  }

  // FutureBuilder screen({double wdgtWidth, wdgtHeight}) {
  //   return FutureBuilder(
  //       future: loaclStorage,
  //       builder: (context, snapshot) {
  //         if (snapshot.hasError) {
  //           return SomethingWentWrongMessage();
  //         } else if (snapshot.hasData) {
  //           var ram = Utils.fromJSONString(snapshot.data);
  //           return signin2(
  //               wdgtWidth: wdgtWidth,
  //               wdgtHeight: wdgtHeight,
  //               username: ram["user_name"]);
  //         } else {
  //           return signin1(wdgtWidth: wdgtWidth, wdgtHeight: wdgtHeight);
  //         }
  //       });
  // }

  validateSignIn1() {
    _loginFormKey.currentState.validate();
  }

  submitSignIn1() {
    validateSignIn1();
    if (usernameError == null && passwordError == null) {
      language_change = true;
      perflogin();
    }
  }

  Container signin1({double wdgtWidth, wdgtHeight}) {
    return Container(
        width: wdgtWidth,
        child: Form(
            key: _loginFormKey,
            child: ListView(
              // crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(height: 40.0),
                Text(
                  pageDetails["text2"],
                  style: TextStyle(
                    // fontSize: getpr,
                    fontSize: Pallet.heading3,
                    color: Pallet.fontcolor,
                    fontWeight: Pallet.font500,
                  ),
                ),
                SizedBox(height: 10.0),
                Text(
                  pageDetails["text3"],
                  style: TextStyle(
                    fontSize: Pallet.heading1,
                    color: Pallet.fontcolor,
                    fontWeight: Pallet.font500,
                  ),
                ),
                SizedBox(height: 40.0),
                Text(
                  pageDetails["text4"],
                  style: TextStyle(
                    fontSize: Pallet.heading3,
                    color: Pallet.fontcolor,
                    fontWeight: Pallet.font500,
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
                SignupTextBox(
                    onFieldSubmitted: (value) => submitSignIn1(),
                    isPrefixIcon: true,
                    isSuffixIcon: false,
                    isPassword: false,
                    digitsOnlyPhone: false,
                    icon: Icons.mail,
                    label: pageDetails["text4"],
                    errorText: usernameError,
                    autofocus: true,
                    // onsubmit: () {
                    //   FocusScope.of(context).requestFocus(passnode);
                    // },
                    controller: username,
                    // onChanged: (value) => validateSignIn1(),
                    validation: (value) {
                      if (value.isEmpty) {
                        setState(() {
                          usernameError = pageDetails["text6"];
                        });
                      } else {
                        setState(() {
                          usernameError = null;
                        });
                      }
                    }),
                SizedBox(
                  height: 10,
                ),
                Text(
                  pageDetails["text7"],
                  style: TextStyle(
                    fontSize: Pallet.heading3,
                    color: Pallet.fontcolor,
                    fontWeight: Pallet.font500,
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
                SignupTextBox(
                    isPassword: true,
                    // tabpress: (event) {
                    // if (event.isKeyPressed(LogicalKeyboardKey.tab)) {
                    //   FocusScope.of(context).requestFocus(loginnode);
                    // }
                    // },
                    icon: Icons.lock,
                    isSuffixIcon: true,
                    // onChanged: (value) => validateSignIn1(),

                    // focusnode: passnode,
                    // onsubmit: () {
                    //   FocusScope.of(context).requestFocus(loginnode);
                    // },
                    onFieldSubmitted: (value) => submitSignIn1(),
                    isPrefixIcon: true,
                    label: pageDetails["text7"],
                    errorText: passwordError,
                    digitsOnlyPhone: false,
                    controller: password,
                    validation: (value) {
                      if (value.isEmpty) {
                        setState(() {
                          passwordError = pageDetails["text8"];
                        });
                        // return "Required";
                      } else {
                        setState(() {
                          passwordError = null;
                        });
                        // return null;
                      }
                    }),
                SizedBox(
                  height: 20,
                ),
                Bouncing(
                    onPress: () {
                      submitSignIn1();
                    },
                    child: Container(
                      width: 400,
                      height: 40,
                      decoration: BoxDecoration(
                        color: Pallet.fontcolor,
                        borderRadius: BorderRadius.circular(8.0),
                      ),
                      child: Center(
                        child: Text(
                          pageDetails["text9"],
                          style: TextStyle(
                              color: Pallet.fontcolornew,
                              fontSize: Pallet.heading2,
                              fontWeight: FontWeight.w700),
                        ),
                      ),
                      // ),
                      // ),
                    )),
                SizedBox(
                  height: 20,
                ),
              ],
            )));
  }

  validateSignIn2() {
    _formKey.currentState.validate();
  }

  submitSignIn2() async {
    validateSignIn2();

    if (passwordError == null) {
      if (isSetUp) {
        setUpFingerprint();
      } else {
        perflogin();
      }
    }
    Utils.removeSharedPrefrences('data');
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.remove("shopbannershow");
  }

  Container setUpContainer({double wdgtWidth}) {
    return Container(
      height: 130,
      width: wdgtWidth,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(18),
        color: Pallet.inner1,
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Text(
            pageDetails["text15"],
            style: TextStyle(
              fontSize: 16,
              color: Pallet.fontcolor,
            ),
          ),
          SizedBox(
            height: 10,
          ),
          Text(
            pageDetails["text16"],
            style: TextStyle(
              fontSize: 10,
              color: Pallet.fontcolor,
            ),
          ),
          SizedBox(
            height: 10,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Container(
                width: 20,
                child: Image.asset(
                  "fingerprint.png",
                  color: Pallet.fontcolor,
                ),
              ),
              SizedBox(
                width: 5,
              ),
              Text(
                pageDetails["text17"],
                style: TextStyle(
                  fontSize: 15,
                  color: Pallet.fontcolor,
                ),
              ),
            ],
          ),
          SizedBox(
            height: 15,
          ),
          Text(
            pageDetails["text18"],
            style: TextStyle(
              fontSize: 15,
              color: Pallet.fontcolor,
            ),
          ),
        ],
      ),
    );
  }

  void fsendOtp({String email}) async {
    Map<String, dynamic> map = {
      "email_id": email,
      "system_product_id": 1,
    };
    Map<String, dynamic> result =
        await HttpRequest.Post('sendOtp', Utils.constructPayload(map));
    if (Utils.isServerError(result)) {
      //print(result['response']['error']);
    } else {
      //print('fajsdfkjads;lkfja;sdklfjadlskfj');
    }
  }

  void check2Fa(String otp) async {
    Map<String, dynamic> _map = {
      "system_product_id": 1,
      "username": username.text,
      "password": password.text,
      "password_future": otp,
      "hardware_id": deviceId,
      "is_finger_print": false,
    };
    reqLogin(_map);
    _formKey2.currentState.validate();
  }

  setUpFingerprint() async {
    if (fingerPrintAuth == true) {
      var ram = Utils.fromJSONString(await loaclStorage);
      Map<String, dynamic> hashkey = {
        "username": ram["user_name"],
        "product_id": 1,
        "device_model": deviceModel,
        "hardware_id": deviceId,
        "mac_address": macAddress,
        "time_snap": DateTime.now().toString()
      };

      String key = Encryption().encode(Utils.toJSONString(hashkey));

      Map<String, dynamic> map = {
        "username": username.text,
        "password": password.text,
        "product_id": 1,
        "hash_key": key.toString()
      };
      Map<String, dynamic> result = await HttpRequest.Post(
          'c_fingerprint_setup', Utils.constructPayload(map));

      if (Utils.isServerError(result)) {
      } else {
        await Utils.setSharedPrefereces(
            result['response']['data']['auth'], 'fingerprint');
        //print(
        // '######################################fingerprint set ##################################################');
        //print(result['response']['data']);
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => LoginRemus()),
        );
      }
    } else {
      _authenticate();
    }
  }

  Future<void> _authenticate() async {
    try {
      fingerPrintAuth = await localAuth.authenticateWithBiometrics(
          localizedReason: "Scan your finger print to authenticate",
          useErrorDialogs: true,
          stickyAuth: false);
      if (!fingerPrintAuth) {
        setState(() {
          isSetUp = false;
        });
      }
      // ignore: unused_catch_clause
    } on PlatformException catch (e) {
      //print(e);
    }
  }

  perflogin() async {
    Map<String, dynamic> _map;
    //print(fingerPrintSet);
    //print('here');

    String oldPassword = await Utils.getSharedPrefereces('fingerprint');

    if (oldPassword != null) {
      if (password.text.length == 0) {
        await _authenticate();
        if (fingerPrintAuth == true) {
          //print(
          // '**************************************fingerprint**********************************************');
          Map<String, dynamic> hashkey = {
            "username": username.text,
            "system_product_id": 1,
            "device_model": deviceModel,
            "hardware_id": deviceId,
            "mac_address": macAddress,
            "is_finger_print": true,
            "time_snap": DateTime.now().toString()
          };
          String key = Encryption().encode(Utils.toJSONString(hashkey));
          //print("************************************" +
          // oldPassword.toString() +
          // "******************************************************");
          _map = {
            "username": username.text,
            "password": oldPassword,
            "password_future": key,
            "hardware_id": deviceId,
            "is_finger_print": false,
            "system_product_id": 1
          };
          reqLogin(_map);
        }
      } else {
        // print(
        // '**************************************normal*************************************');
        _map = {
          "username": username.text,
          "password": password.text,
          "password_future": "null",
          "hardware_id": "null",
          "is_finger_print": false,
          "system_product_id": 1
        };
        reqLogin(_map);
      }
    } else {
      //print(
      // '**************************************normal*******************************************');
      _map = {
        "username": username.text,
        "password": password.text,
        "password_future": "null",
        "hardware_id": "null",
        "is_finger_print": false,
        "system_product_id": 1
      };
      reqLogin(_map);
    }
  }

  reqLogin(Map<String, dynamic> map) async {
    if (map['username'] == "") {
      var ram = Utils.fromJSONString(await loaclStorage);
      map['username'] = ram['user_name'];
    }
    Map<String, dynamic> result =
        await HttpRequest.Post('login', Utils.constructPayload(map));

    if (Utils.isServerError(result)) {
      if (result['response']['error'] == "wrong_combination") {
        setState(() {
          passwordError = pageDetails["text21"];
        });
      } else if (result['response']['error'] == "auth_error3") {
        snack.snack(title: pageDetails["text64"]);

        // ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        //     content: Text(
        //   pageDetails['text64'],
        // )));
      } else {
        snack.snack(
            title:
                await Utils.getMessage(result['response']['error'].toString()));

        // String loginfailed = SnackBar(
        //   content: Text(
        //       await Utils.getMessage(result['response']['error'].toString())),
        // );

        // ScaffoldMessenger.of(context).showSnackBar(loginfailed);
      }
    } else {
      if (language_change == true) {
        print('language_deafault_english');
        Pallet.changelang('English');
      }
      print('jjjjjjjjjjjjjjj');
      print(result['response']['data']);
      if (result['response']['data']['email_verified'] == false) {
        showDialog(
            barrierDismissible: false,
            context: context,
            builder: (_) => AlertDialog(
                  elevation: 24.0,
                  backgroundColor: Pallet.inner1,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.all(Radius.circular(15.0)),
                  ),
                  title: Row(
                    children: [
                      Image.asset("c-logo.png", width: 40),
                      SizedBox(width: 10),
                      Text(pageDetails["text22"],
                          style: TextStyle(
                              fontSize: Pallet.heading3,
                              color: Pallet.fontcolor,
                              fontWeight: Pallet.font500)),
                    ],
                  ),
                  content: Container(
                    height: 130,
                    child: SingleChildScrollView(
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          SizedBox(height: 15),
                          Text("Your Email is Not Verfied",
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                  color: Pallet.fontcolor,
                                  fontSize: Pallet.heading2)),
                          SizedBox(height: 15),
                        ],
                      ),
                    ),
                  ),
                  actions: [
                    PopupButton(
                      text: "Ok",
                      onpress: () {
                        Navigator.of(context).pop();
                      },
                    ),
                  ],
                ));
      } else if (result['response']['data']['two_fa_enabled'] == true) {
        showDialog(
            context: context,
            builder: (_) => AlertDialog(
                  backgroundColor: Pallet.popupcontainerback,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(Pallet.radius)),
                  title: Row(
                    children: [
                      Image.asset(
                        'assets/c-logo.png',
                        width: 40,
                      ),
                      SizedBox(width: 10),
                      Align(
                        alignment: Alignment.topLeft,
                        child: Text(
                          pageDetails['text28'],
                          style: TextStyle(
                            fontSize: 15,
                            color: Pallet.fontcolor,
                          ),
                        ),
                      ),
                    ],
                  ),
                  content: Form(
                    key: _formKey2,
                    child: TextFormField(
                        style: TextStyle(color: Pallet.fontcolor),
                        controller: otp,
                        inputFormatters: <TextInputFormatter>[
                          FilteringTextInputFormatter.digitsOnly,
                          new LengthLimitingTextInputFormatter(8),
                        ],
                        cursorColor: Pallet.fontcolor,
                        decoration: InputDecoration(
                          border: OutlineInputBorder(
                              borderSide:
                                  BorderSide(color: Pallet.fontcolor, width: 2),
                              borderRadius:
                                  BorderRadius.circular(Pallet.radius)),
                          enabledBorder: OutlineInputBorder(
                              borderSide:
                                  BorderSide(color: Pallet.fontcolor, width: 2),
                              borderRadius:
                                  BorderRadius.circular(Pallet.radius)),
                          focusedBorder: OutlineInputBorder(
                              borderSide:
                                  BorderSide(color: Pallet.fontcolor, width: 2),
                              borderRadius:
                                  BorderRadius.circular(Pallet.radius)),
                          errorBorder: OutlineInputBorder(
                              borderSide:
                                  BorderSide(color: Colors.red, width: 2),
                              borderRadius:
                                  BorderRadius.circular(Pallet.radius)),
                          focusedErrorBorder: OutlineInputBorder(
                              borderSide:
                                  BorderSide(color: Colors.red, width: 2),
                              borderRadius:
                                  BorderRadius.circular(Pallet.radius)),
                          labelText: pageDetails["text29"],
                          labelStyle: TextStyle(
                            fontSize: 15,
                            color: Pallet.fontcolor,
                          ),
                          errorText: otpError,
                        ),
                        keyboardType: TextInputType.number,
                        validator: (value) {
                          if (value.isEmpty) {
                            return pageDetails["text30"];
                          } else {
                            return otpError;
                          }
                        }),
                  ),
                  actions: [
                    PopupButton(
                      text: pageDetails["text27"],
                      onpress: () {
                        _formKey2.currentState.validate();
                        check2Fa(otp.text);
                      },
                    ),
                  ],
                ));
      } else {
        if (result['response']['data']['accounts'].length > 1) {
          await showGeneralDialog(
              context: context,
              barrierDismissible: false,
              pageBuilder: (context, Animation animation,
                      Animation secondaryAnimation) =>
                  StatefulBuilder(builder: (context, setState1) {
                    return Material(
                      color: Colors.transparent,
                      child: BackdropFilter(
                        filter: ImageFilter.blur(
                          sigmaX: 3,
                          sigmaY: 3,
                        ),
                        child: Container(
                          color: Colors.transparent,
                          height: MediaQuery.of(context).size.width >= 768
                              ? MediaQuery.of(context).size.height
                              : MediaQuery.of(context).size.height * 0.6,
                          width: MediaQuery.of(context).size.width,
                          child: MediaQuery.of(context).size.width >= 768
                              ? Row(
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    IconButton(
                                        icon: Icon(
                                          Icons.keyboard_arrow_left,
                                          color: Pallet.fontcolor,
                                        ),
                                        onPressed: () {
                                          _carouselController.previousPage();
                                        }),
                                    Container(
                                      //color: Colors.red,
                                      width: MediaQuery.of(context).size.width *
                                          0.7,
                                      height: 200,
                                      child: CarouselSlider.builder(
                                          carouselController:
                                              _carouselController,
                                          options: CarouselOptions(
                                              autoPlay: false,
                                              initialPage: 0,
                                              enableInfiniteScroll: false,
                                              enlargeStrategy:
                                                  CenterPageEnlargeStrategy
                                                      .scale,
                                              reverse: false,
                                              scrollPhysics:
                                                  BouncingScrollPhysics(),
                                              viewportFraction: 0.3,
                                              enlargeCenterPage: true,
                                              scrollDirection: Axis.horizontal,
                                              onPageChanged: (index, reason) {
                                                setState1(() {
                                                  _current = index;
                                                });
                                              }),
                                          itemCount: result['response']['data']
                                                  ['accounts']
                                              .length,
                                          itemBuilder: (BuildContext context,
                                                  int index, _) =>
                                              GestureDetector(
                                                onTap: () async {
                                                  Utils.setSharedPrefereces(
                                                      result['response']['data']
                                                              ['token']
                                                          .toString(),
                                                      'for_acount');
                                                  Map<String, dynamic> _map = {
                                                    "system_product_id": 1,
                                                    "account_id":
                                                        result['response']
                                                                        ['data']
                                                                    ['accounts']
                                                                [index]
                                                            ['account_id'],
                                                    "token": (result['response']
                                                            ['data']['token'])
                                                        .toString(),
                                                  };
                                                  result =
                                                      await HttpRequest.Post(
                                                          'use_account',
                                                          Utils
                                                              .constructPayload(
                                                                  _map));
                                                  if (Utils.isServerError(
                                                      result)) {
                                                    snack.snack(
                                                        title: pageDetails[
                                                            "text65"]);

                                                    // ScaffoldMessenger.of(
                                                    //         context)
                                                    //     .showSnackBar(SnackBar(
                                                    //         content: Text(
                                                    //             pageDetails[
                                                    //                 'text65'])));
                                                    Navigator.of(context).pop();
                                                    //print(Utils.isServerError(
                                                    // result));
                                                  } else {
                                                    result['response']['data']
                                                        ['ismobile'] = ismobile;
                                                    await Utils
                                                        .setSharedPrefereces(
                                                            Utils.toJSONString(
                                                                result['response']
                                                                    ['data']),
                                                            'data');

                                                    //print(result['response']
                                                    // ['data']);
                                                    Navigator.of(context).pop();

                                                    Navigator.pushReplacement(
                                                      context,
                                                      MaterialPageRoute(
                                                          builder: (context) =>
                                                              RemusPayment(
                                                                shoppingtoken:
                                                                    widget
                                                                        .shopingtokenkey,
                                                              )),
                                                    );
                                                  }
                                                },
                                                child: Column(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment.center,
                                                  crossAxisAlignment:
                                                      CrossAxisAlignment.center,
                                                  children: [
                                                    CircleAvatar(
                                                      radius: index == _current
                                                          ? 70
                                                          : 30,
                                                      backgroundImage: result['response']
                                                                          ['data']
                                                                      ['accounts'][index]
                                                                  [
                                                                  'profile_image'] !=
                                                              null
                                                          ? NetworkImage((appSettings[
                                                                      'SERVER_URL'] +
                                                                  '/' +
                                                                  result['response']['data']
                                                                              ['accounts']
                                                                          [index]
                                                                      ['profile_image'])
                                                              .toString())
                                                          : AssetImage("individual.png"),
                                                    ),
                                                    SizedBox(height: 20),
                                                    Container(
                                                      child: Text(
                                                          (result['response'][
                                                                          'data']
                                                                      [
                                                                      'accounts'][index]
                                                                  [
                                                                  'screen_name'])
                                                              .toString(),
                                                          overflow: TextOverflow
                                                              .ellipsis,
                                                          maxLines: 4,
                                                          style: TextStyle(
                                                            height: 1.6,
                                                            fontSize: index ==
                                                                    _current
                                                                ? Pallet
                                                                    .heading3
                                                                : Pallet
                                                                    .heading7,
                                                            color: Pallet
                                                                .fontcolor,
                                                          )),
                                                    ),
                                                  ],
                                                ),
                                              )),
                                    ),
                                    IconButton(
                                        icon: Icon(
                                          Icons.keyboard_arrow_right,
                                          color: Pallet.fontcolor,
                                        ),
                                        onPressed: () {
                                          setState1(() {
                                            _carouselController.nextPage();
                                          });
                                        }),
                                  ],
                                )
                              : Container(
                                  //color: Colors.red,
                                  height: MediaQuery.of(context).size.height,
                                  width: MediaQuery.of(context).size.width,
                                  color: Colors.transparent,
                                  child: CarouselSlider.builder(
                                    carouselController: _carouselController,
                                    options: CarouselOptions(
                                        autoPlay: false,
                                        initialPage: 0,
                                        enableInfiniteScroll: false,
                                        viewportFraction: 0.28,
                                        enlargeCenterPage: true,
                                        reverse: false,
                                        scrollPhysics: BouncingScrollPhysics(),
                                        scrollDirection: Axis.horizontal,
                                        onPageChanged: (index, reason) {
                                          setState(() {
                                            setState(() {
                                              _current = index;
                                            });
                                          });
                                        }),
                                    itemCount: result['response']['data']
                                            ['accounts']
                                        .length,
                                    itemBuilder:
                                        (BuildContext context, int index, _) =>
                                            GestureDetector(
                                      onTap: () async {
                                        Map<String, dynamic> _map = {
                                          "system_product_id": 1,
                                          "account_id": result['response']
                                                  ['data']['accounts'][index]
                                              ['account_id'],
                                          "token": (result['response']['data']
                                                  ['token'])
                                              .toString(),
                                        };
                                        result = await HttpRequest.Post(
                                            'use_account',
                                            Utils.constructPayload(_map));
                                        if (Utils.isServerError(result)) {
                                          snack.snack(
                                              title: pageDetails["text65"]);

                                          // ScaffoldMessenger.of(context)
                                          //     .showSnackBar(SnackBar(
                                          //         content: Text(
                                          //             pageDetails['text65'])));
                                          Navigator.of(context).pop();
                                          //print(Utils.isServerError(result));
                                        } else {
                                          result['response']['data']
                                              ['ismobile'] = ismobile;
                                          await Utils.setSharedPrefereces(
                                              Utils.toJSONString(
                                                  result['response']['data']),
                                              'data');

                                          //print(result['response']['data']);
                                          Navigator.of(context).pop();

                                          Navigator.pushReplacement(
                                              context,
                                              MaterialPageRoute(
                                                  builder: (context) =>
                                                      RemusPayment(
                                                        shoppingtoken: widget
                                                            .shopingtokenkey,
                                                      )));
                                        }
                                      },
                                      child: Container(
                                        // height:
                                        //     MediaQuery.of(context).size.height *
                                        //         0.4,
                                        //padding: EdgeInsets.all(10),
                                        child: Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.center,
                                          children: [
                                            CircleAvatar(
                                              radius: 25,
                                              backgroundImage: result['response']
                                                                      ['data']
                                                                  ['accounts']
                                                              [index]
                                                          ['profile_image'] !=
                                                      null
                                                  ? NetworkImage((appSettings[
                                                              'SERVER_URL'] +
                                                          '/' +
                                                          result['response']
                                                                          ['data']
                                                                      ['accounts']
                                                                  [index]
                                                              ['profile_image'])
                                                      .toString())
                                                  : AssetImage("individual.png"),
                                            ),
                                            SizedBox(height: 15),
                                            Padding(
                                              padding: const EdgeInsets.only(
                                                  left: 8.0),
                                              child: Container(
                                                child: Text(
                                                    (result['response']['data']
                                                                    ['accounts']
                                                                [index]
                                                            ['screen_name'])
                                                        .toString(),
                                                    overflow: TextOverflow.fade,
                                                    //maxLines: 4,
                                                    style: TextStyle(
                                                      fontSize: Pallet.heading7,
                                                      color: Pallet.fontcolor,
                                                    )),
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ),
                                  ),
                                ),
                        ),
                      ),
                    );
                  }));
        } else {
          Map<String, dynamic> _map = {
            "system_product_id": 1,
            "account_id": result['response']['data']['accounts'][0]
                ['account_id'],
            "token": (result['response']['data']['token']).toString(),
          };
          result = await HttpRequest.Post(
              'use_account', Utils.constructPayload(_map));
          if (Utils.isServerError(result)) {
            snack.snack(title: pageDetails["text65"]);

            // ScaffoldMessenger.of(context)
            //     .showSnackBar(SnackBar(content: Text(pageDetails['text65'])));
            Navigator.of(context).pop();
            //print(Utils.isServerError(result));
          } else {
            await Utils.setSharedPrefereces(
                Utils.toJSONString(result['response']['data']), 'data');

            Navigator.pushReplacement(
              context,
              MaterialPageRoute(
                builder: (context) => RemusPayment(
                  shoppingtoken: widget.shopingtokenkey,
                ),
              ),
            );
          }
        }
      }
    }
  }
}
