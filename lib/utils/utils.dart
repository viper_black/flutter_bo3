import 'dart:convert';
import 'package:shared_preferences/shared_preferences.dart';
import '../services/communication/crypto/index.dart';
import '../services/common/index.dart';

import '../config/index.dart';

class Utils {
  static bool isEmail(String data) {
    return RegExp(r'''
^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,253}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,253}[a-zA-Z0-9])?)*$''')
        .hasMatch(data);
  }

  static bool isPassword(String data) {
    return RegExp(r'/(?=.*\d)(?=.*[a-z])(?=.*[!@#$%^&*])(?=.*[A-Z]).{8,}/')
        .hasMatch(data);
  }

  static bool isUsername(String data) {
    return RegExp(
            r'^(?=.{6,20}$)(?![_.0-9])(?!.*[_.]{2})[a-zA-Z0-9._]+(?<![_.])$')
        .hasMatch(data);
  }

  static String isRequiredPayload(String checkName, Map<String, dynamic> data) {
    if (data['payload']) {
      // ignore: unused_local_variable
      Map<String, dynamic> temp =
          requestConfig[checkName]['requestBody']['payload'];

      return 'output';
    } else {
      return 'Payload missing';
    }
  }

  static String toJSONString(Map<String, dynamic> data) {
    return data == null ? null : jsonEncode(data);
  }

  static Map<String, dynamic> fromJSONString(String data) {
    return data == null ? null : jsonDecode(data);
  }

  static bool isServerError(Map<String, dynamic> payload) {
    if (payload['response']['data'] == 'null' ||
        payload['response']['data'] == null) {
      return true;
    }
    return false;
  }

  // ignore: missing_return
  static Future<String> getMessage(
    String data,
  ) async {
    try {
      //print(data);
      String value = await getSharedPrefereces('language');
      String language;
      if (value == null || fromJSONString(value)['language'] == null) {
        language = 'English';
      } else {
        language = fromJSONString(value)['language'];
      }

      //print(value);
      //print(language);
      if (messageConfig[language][data] != null) {
        return messageConfig[language][data];
      } else {
        return data;
      }
      // ignore: unused_catch_stack
    } catch (e, trace) {
      //print(e);
      //print(trace);
    }
  }

  static Future<String> getPageDetails(
    String data,
  ) async {
    String value = await getSharedPrefereces('language');
    if (value == null) {
      return Utils.toJSONString(pageDetailsConfig['English'][data]);
    }
    String language = fromJSONString(value)['language'];
    if (language == null) {
      return Utils.toJSONString(pageDetailsConfig['English'][data]);
    } else if (pageDetailsConfig[language][data] != null) {
      return Utils.toJSONString(pageDetailsConfig[language][data]);
    } else {
      return data;
    }
  }

  static Future<String> getPageDetails1(
    String data,
  ) async {
    String value = await getSharedPrefereces('language');
    if (value == null) {
      return Utils.toJSONString(pageDetailsConfig['English'][data]);
    }
    String language = fromJSONString(value)['language'];
    if (language == null) {
      return Utils.toJSONString(pageDetailsConfig['English'][data]);
    } else if (pageDetailsConfig[language][data] != null) {
      return Utils.toJSONString(pageDetailsConfig[language][data]);
    } else {
      return data;
    }
  }

  static Future<Map<String, dynamic>> convertMessage(
      Map<String, dynamic> payload) async {
    String value = await getSharedPrefereces('language');
    String language;
    if (value == null || fromJSONString(value)['language'] == null) {
      language = 'English';
    } else {
      language = fromJSONString(value)['language'];
    }
    // ignore: omit_local_variable_types
    Map<String, dynamic> temp = {};
    for (var data_key in payload.keys) {
      if (messageConfig[language] == null) {
        temp[data_key] = payload[data_key];
      } else if (messageConfig[language][payload[data_key]] == null) {
        temp[data_key] = payload[data_key];
      } else if (messageConfig[language][payload[data_key]]
              .runtimeType
              .toString() !=
          'String') {
        temp[data_key] = payload[data_key];
      } else {
        temp[data_key] = messageConfig[language][payload[data_key]];
      }
    }
    return temp;
  }

  static bool isRequiredFieldsAvailable(
      String requestname, Map<String, dynamic> data) {
    for (var data_key in requestConfig[requestname]['payload'].keys) {
      if (data[data_key] == null || data[data_key] == '') {
        return false;
      }
    }
    return true;
  }

  static Map<String, dynamic> getSiteID() {
    return {
      'payload': {
        'product_id': appSettings['SITE_ID'],
      },
    };
  }

  static Map<String, dynamic> constructPayload(Map<String, dynamic> data) {
    return {'payload': data};
  }

  static Future<bool> setSharedPrefereces(String data, String key) async {
    try {
      final _prefs = await SharedPreferences.getInstance();
      _prefs.setString(key, Encryption().encode(data));
      // //print("${key}, ${data}");
      return true;
    } catch (e, trace) {
      Logger.error(e, trace);
      return false;
    }
  }

  static Future<bool> removeSharedPrefrences(String key) async {
    try {
      final _prefs = await SharedPreferences.getInstance();
      _prefs.remove(key);
      return true;
    } catch (e, trace) {
      Logger.error(e, trace);
      return false;
    }
  }

  static Future<String> getSharedPrefereces(String key) async {
    try {
      //print('$key,language chineese');
      final _prefs = await SharedPreferences.getInstance();
      String data = _prefs.getString(key);
      //print('$data,print_value12345');
      //print(data);
      //print(data != null ? Encryption().decode(data) : 'empty ');
      //print('lskdfjalsk;dfjasdfjkleninkuamr');
      if (data != null) {
        return Encryption().decode(data);
      } else {
        return null;
      }
    } catch (e, trace) {
      //print('flkjsadflkasjflasdkjfasdlkfjasdlkjasdfasdkjf');
      Logger.error(e, trace);
      return 'Something Went Wrong';
    }
  }
}
