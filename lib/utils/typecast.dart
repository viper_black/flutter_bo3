import '../config/index.dart';

class Typecast {
  static bool isValidFormat(
      Map<String, dynamic> temp, Map<String, dynamic> data) {
    for (var data_key in data.keys) {
      for (var config_key in temp.keys) {
        if (data_key == config_key) {
          if (data[data_key].runtimeType.toString() != temp[config_key]) {
            return false;
          }
        }
      }
    }
    return true;
  }

  static bool isValidPayloadFormat(
      String checkName, Map<String, dynamic> data) {
    Map<String, dynamic> temp =
        requestConfig[checkName]['requestBody']['payload'];
    return isValidFormat(temp, data);
  }

  static Map<String, dynamic> typecastFormat(
      Map<String, dynamic> temp, Map<String, dynamic> data) {
    for (var data_key in data.keys) {
      for (var config_key in temp.keys) {
        if (data_key == config_key) {
          if (data[data_key].runtimeType.toString() != temp[config_key]) {
            switch (temp[config_key]) {
              case 'int':
                data[data_key] = int.parse(data[data_key]);
                break;
              case 'double':
                data[data_key] = double.parse(data[data_key]);
                break;
              case 'String':
                data[data_key] = data[data_key].toString();
                break;
              case 'bool':
                if (data[data_key].toString().toLowerCase() == 'true' ||
                    data[data_key].toString().toLowerCase() == 'false') {
                  data[data_key] =
                      data[data_key].toString().toLowerCase() == 'true'
                          ? true
                          : false;
                }
                break;
              default:
            }
          }
        }
      }
    }
    return data;
  }

  static Map<String, dynamic> typecastPayloadFormat(
      String formatName, Map<String, dynamic> data) {
    Map<String, dynamic> temp =
        requestConfig[formatName]['requestBody']['payload'];
    return typecastFormat(temp, data);
  }
}
